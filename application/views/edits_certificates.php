<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <?php include 'assets/lib/cssscript.php'?>
        <style>
        	.dt-buttons{
                   	display:none !important;
                }
                .sweet-alert.showSweetAlert.visible{
                    z-index: 999999999 !important;
                    border:1px solid red;
                }

@media screen and (max-width:740px) {
				.ms-container {
    width:auto; }  }
        </style>
    </head>
    <!-- END HEAD -->

 <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header.php"?>
            <!-- END HEADER -->
            <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                            	<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<div class="portlet box dark">
									<div class="portlet-title">
										<div class="caption">
											<a class="btn btn-circle blue btn-outline" href="index.php?/controller_admin/certificate#portlet_tab2_2"><i class="fa fa-arrow-left"></i>Back</a>
											Edit Assessment </div>
										<div class="tools"> </div>
									</div>
									<div class="portlet-body">
										<div class="page-content-row">
											<div class="col-md-12 col-sm-12">
												<!-- BEGIN Portlet PORTLET-->
	                                             <div class="portlet box blue">
	                                                 <div class="portlet-title">
	                                                     <div class="caption">
	                                                     	Assessment Edit</div>
	                                                     <div class="tools">
	                                                     	<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
	                                                 	</div>
	                                             	</div>
	                                                <div class="portlet-body">
	                                                	<form class="form-horizontal" id="edit_certificate">
				                                            <div class="form-group" style="display:none">
				                                                <label class="control-label col-sm-4" for="email">Id:</label>
				                                                <div class="col-sm-5">
				                                                    <input type="text" class="form-control" id="id1" name="id1" readonly>
				                                                </div>
				                                            </div>
				                                            <div class="form-group" style="display:none">
				                                                <label class="control-label col-sm-4" for="email">Assessment ID:</label>
				                                                <div class="col-sm-5">
				                                                  <input type="text" class="form-control" id="assess_id1" name="assess_id1" readonly>

				                                                </div>
				                                            </div>
				                                            <div class="form-group">
				                                                <label class="control-label col-sm-4" for="email">Assessment Name:</label>
				                                                <div class="col-sm-5">
				                                                    <input type="text" class="form-control" id="certificate_name1" name="certificate_name1" placeholder="" readonly>
				                                                </div>
				                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-sm-4" for="email">Assessment Type:</label>
                                                                <div class="col-sm-5">
	                                                                <select class="form-control" id="assessment_type1"
	                                                                        name="assessment_type1" disabled>
	                                                                        <option selected disabled>Select Assessment Type</option>
	                                                                        <option value="generic">Generic Assessment</option>
	                                                                        <option value="technology">Technology Assessment</option>
	                                                                        <option value="product">Product Assessment</option>
	                                                                </select>
                                                                </div>
                                                            </div>
				                                            <div class="form-group assessment_change">
				                                                <label class="control-label col-sm-4" for="email">Product Category:</label>
				                                                <div class="col-sm-5">
				                                                    <select class="form-control" id="product_name1" name="product_name1" disabled>
                                                                                        <option selected disabled value="">Select Product Category</option>
				                                                        <?php
				                                                            foreach ($records->result() as $row) {
				                                                                ?>
				                                                                <option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
				                                                        <?php } ?>
				                                                    </select>
				                                                </div>
				                                            </div>
				                                            <div class="form-group assessment_change">
				                                                <label class="control-label col-sm-4" for="email">Sub Category:</label>
				                                                <div class="col-sm-5">
				                                                    <select class="form-control" id="category1" name="category1" disabled>
                                                                                        <option selected disabled value="">Select Sub Category</option>
				                                                    </select>
				                                                </div>
				                                            </div>

				                                            <div class="form-group">
				                                                <label class="control-label col-sm-4" for="email">Questions to Assessment:</label>
				                                                <div class="col-sm-5">
				                                                   	<select class="form-control" id="quiz_a" name="quiz_a">
                                                                        <option selected disabled value="">Select Questions to Assessment</option>
				                                                    </select>
				                                                </div>
				                                            </div>

				                                            <div class="form-group">
				                                                <label class="control-label col-sm-4" for="email">Threshold:</label>
				                                                <div class="col-sm-5">
				                                                   <select class="form-control" id="thrus_ha" name="thrus_ha">
                                                                        <option selected disabled value="">Select Threshold</option>
				                                                    </select>
				                                                </div>
				                                            </div>

				                                            <div class="form-group" style="display:none">
				                                                <label class="control-label col-sm-4" for="email">Title:</label>
				                                                <div class="col-sm-5">
				                                                    <input type="text" class="form-control" id="title1" name="title1" placeholder="Title">
				                                                </div>
				                                            </div>
				                                        </form>
	                                                </div>
	                                           </div>
	                                           <!-- END Portlet PORTLET-->
											</div>
                                        </div>

                                        <div class="page-content-row">
                                            <div class="col-md-12 col-sm-12">
                                                    <!-- BEGIN Portlet PORTLET-->
	                                             <div class="portlet box blue">
	                                                 <div class="portlet-title">
	                                                     <div class="caption">
	                                                     	Quiz</div>
	                                                     <div class="tools">
	                                                     	<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
	                                                 	</div>
	                                             	</div>
	                                                <div class="portlet-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="btn-group">
                                                                        <button id="sample_editable_1_new" class="btn btn-circle dark btn-outline" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"> Bulk Quiz
                                                                            <i class="fa fa-upload"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="btn-group">
                                                                        <button class="btn btn-circle blue btn-outline" onClick="add_quizs()"><i class="fa fa-plus"></i> Add Quiz</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                </div>
                                                            </div><span class="clearfix"></span><br>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered" id="sample_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center">Question</th>
                                                                            <th class="text-center">Choice 1</th>
                                                                            <th class="text-center">Choice 2</th>
                                                                            <th class="text-center">Choice 3</th>
                                                                            <th class="text-center">Choice 4</th>
                                                                            <th class="text-center">Correct Ans</th>
                                                                            <th class="text-center">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="quiz_show" align="center">
                                                                    </tbody>
                                                                </table>
                                                            </div>
	                                                </div>
	                                           	</div>
	                                           <!-- END Portlet PORTLET-->
											</div>
                                        </div>
                                        <div class="page-content-row">
											<div class="col-md-12 col-sm-12">
												<!-- BEGIN Portlet PORTLET-->
	                                             <div class="portlet box blue">
	                                                 <div class="portlet-title">
	                                                     <div class="caption">
	                                                     	<i class="fa fa-gift"></i>Refer Assessment</div>
	                                                     <div class="tools">
	                                                     	<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
	                                                 	</div>
	                                             	</div>
	                                                <div class="portlet-body">
	                                                	<div class="row">
	                                                		<div class="col-md-6 col-lg-offset-4">
                                                                <select id='pre-selected-options' multiple='multiple'>
                                                                    <!--<?php
                                                                        foreach ($rec->result() as $row) {
                                                                                ?>
                                                                                <option value="<?php echo $row->training_id; ?>"><?php echo $row->title; ?></option>
                                                                    <?php } ?>-->
                                                                  </select>
	                                                		</div>
														</div>
	                                                </div>
	                                           </div>
	                                           <!-- END Portlet PORTLET-->
											</div>
                                        </div>

                                                <div class="portlet-body">
                                                	<div class="row">
                                                		<div class="row text-center">
                                                			<button class="btn btn-circle blue btn-outline" onClick="update_edits()"><i class="fa fa-check"></i> Update</button>
                                                			<button class="btn btn-circle red btn-outline" onClick="cancel_edits()"><i class="fa fa-crass"></i> Cancel</button>
                                                		</div>
													</div>
                                                </div>
									</div>
								</div>
								<!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
					<!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                 </div>
 
       <!--Modal Starts-->

		<div id="edit_q" class="modal fade" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit Quiz</h4>
					</div>
					<div class="modal-body">
						<div class="error" style="display:none">
							<label id="rowdata"></label>
						</div>
						<form class="form-horizontal" id="edit_quiz">
							<div class="form-group" style="display:none">
									<label class="control-label col-sm-4" for="email">Id:</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="quiz_id_s" name="quiz_id_s" readonly>
									</div>
								</div>
							<div class="form-group" style="display:none">
								<label class="control-label col-sm-3" for="email">Assessment:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="trainings1" name="trainings1" readonly>
								</div>
							</div>
							<div class="rowform-group">
								<div class="row">
									<div class="col-md-12 col-sm-12" style="display:none">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Quiz ID:</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="quiz_id1" name="quiz_id1" readonly>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Question:</label>
											<div class="col-sm-9">
												<textarea class="form-control" id="question1" name="question1" style="resize:none"></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
											<div class="form-group">
													<label class="control-label col-sm-3" for="email">Option A:</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="option_a1" name="option_a1">
													</div>
												</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Option B:</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="option_b1" name="option_b1">
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Option C:</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="option_c1" name="option_c1">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Option D:</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="option_d1" name="option_d1">
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Correct Answer:</label>
											<div class="col-sm-9">
												<select class="form-control" id="c_answer1" name="c_answer1">
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-circle green btn-outline" onclick='submitquiz()'><i class="fa fa-check"></i> Submit</button>
						<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
					</div>
				</div>
			</div>
		</div>

                        <div id="myquiz" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Quiz</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="error" style="display:none">
                                            <label id="rowdata_1"></label>
                                        </div>
                                        <form class="form-horizontal" id="upload_quiz" name="">
                                            <!-- <div class="form-group">
	                                                <label class="control-label col-sm-4" for="email">Assessment Id:</label>
	                                                <div class="col-sm-8">
	                                                    <input type="text" class="form-control" id="quiz_id" name="quiz_id" readonly>
	                                                </div>
	                                            </div> -->
                                            <div class="form-group" style="display:none">
                                                <label class="control-label col-sm-3" for="email">Assessment:</label>
                                                <div class="col-sm-8">
                                                	<input type="text" class="form-control" id="trainings" name="trainings" readonly>
                                                    <!-- <select class="form-control" id="trainings" name="trainings">
                                                        <option selected disabled>Select Assessment</option>
                                                        <?php
                                                        	foreach ($record->result() as $row) {
                                                                ?>
                                                                <option value="<?php echo $row->certificate_id; ?>"><?php echo $row->certificate_name; ?></option>
                                                        <?php } ?>
                                                    </select>  -->
                                                </div>
                                            </div>
                                            <div class="rowform-group">
                                            	<div class="row">
                                            		<div class="col-md-6 col-sm-12" style="display:none">
	                                            		<div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Quiz ID:</label>
			                                                <div class="col-sm-9">
			                                                    <input type="text" class="form-control" id="quiz_id" name="fields[0][]" readonly>
			                                                </div>
			                                            </div>
                                            		</div>
                                            		<div class="col-md-6 col-sm-12">
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Question:</label>
			                                                <div class="col-sm-9">
                                                            <textarea class="form-control" id="question" name="fields[0][]" style='resize:none'></textarea>
			                                                </div>
			                                            </div>
                                            		</div>
                                            		<div class="col-md-6 col-sm-12">
                                            			<div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Option A:</label>
			                                                <div class="col-sm-9">
			                                                    <input type="text" class="form-control" id="option_a" name="fields[0][]">
			                                                </div>
			                                            </div>
                                            		</div>
                                            	</div>
                                            	<div class="row">
                                            		<div class="col-md-6 col-sm-12">
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Option B:</label>
			                                                <div class="col-sm-9">
			                                                    <input type="text" class="form-control" id="option_b" name="fields[0][]">
			                                                </div>
			                                            </div>
                                            		</div>
                                            		<div class="col-md-6 col-sm-12">
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Option C:</label>
			                                                <div class="col-sm-9">
			                                                    <input type="text" class="form-control" id="option_c" name="fields[0][]">
			                                                </div>
			                                            </div>
                                            		</div>
                                            	</div>
                                            	<div class="row">
                                            		<div class="col-md-6 col-sm-12">
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Option D:</label>
			                                                <div class="col-sm-9">
			                                                    <input type="text" class="form-control" id="option_d" name="fields[0][]">
			                                                </div>
			                                            </div>
                                            		</div>
                                            		<div class="col-md-6 col-sm-12">
			                                            <div class="form-group">
			                                                <label class="control-label col-sm-3" for="email">Correct Answer:</label>
			                                                <div class="col-sm-9">
			                                                    <select class="form-control" id="c_answer" name="fields[0][]">
			                                                    	<option selected diasbled value="">Select correct answer</option>
			                                                    </select>
			                                                </div>
			                                            </div>
			                                        </div>
                                            	</div>
												<div class="form-group" style="display:none">
													<label class="control-label col-sm-3" for="email">Assessment:</label>
													<div class="col-sm-8">
														<input type="text" class="form-control trainings" id="trainings" name="fields[0][]" readonly>
														<!-- <select class="form-control" id="trainings" name="trainings">
															<option selected disabled>Select Assessment</option>
															<?php
																foreach ($record->result() as $row) {
																	?>
																	<option value="<?php echo $row->certificate_id; ?>"><?php echo $row->certificate_name; ?></option>
															<?php } ?>
														</select>  -->
													</div>
												</div>
                                            </div>
                                        </form>
                                        <div class="form-group">
                                        	<div class="text-right">
                                                    <button type="submit" class="btn btn-circle dark btn-outline" id="add_buttons"><i class="fa fa-plus"></i> Add Quiz</button>
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-circle green btn-outline" id="addquiz"><i class="fa fa-check"></i> Submit</button>
                                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

						<div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Bulk Upload</h4>
                  </div>
                  <div class="modal-body">
                     <div class="container">
                       <form action="#" class="form-horizontal form-bordered">
                            <div class="form-body row">
                            	<div class="form-group col-md-2 col-sm-12 text-center">
                                	<?php $fname='bulk_quiz.xlsx'; ?>
                                    <a class="btn btn-circle purple-sharp btn-outline sbold uppercase" href="<?php echo base_url(); ?>index.php?/controller_admin/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="add_excel" id="add_excel"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            <div class="form-body row" style="display:none">
                            	<div class="form-group col-md-12 col-sm-12 text-center">
                                    <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                    <input type="text" class="form-control" id="a_value" name="a_value" readonly>
                                </div>
                              </div>
                            </form>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-circle green btn-outline" id="bulkupload" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
                     <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>

        <div id="sla_confirm" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <p id="sla_detail_confirm"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline" id="confirm_sla"><i class="fa fa-check"></i> Ok</button>
                        <!--<button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>-->
                    </div>
                </div>
            </div>
        </div>

     <!--loading model-->
     <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->
       <!--Modal Ends-->

        <!-- END QUICK SIDEBAR -->
        <?php include 'assets/lib/javascript.php'?>
        <script>
            $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#training_management').addClass('open');
        </script>
       <script>
        $(document).ready(function(){
                $('#assessment_type1').change(function(){
                    var product_type=$('#assessment_type1').val();
                    if(product_type=="product"){
                            $('.assessment_change').show();
                    }else{
                            $('.assessment_change').hide();
                    }
                })
        })
        $(window).load(function(){
            $('.assessment_change').hide();
            var sPageURL = window.location.search.substring(1);
            var sParameterName = sPageURL.split('=');
            sParameterNames = sParameterName[1];
            sParameterNames = sParameterNames.replace(/%20/g, " ");
            var ajaxResult=[];            
            $.ajax({
                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/certificate_s",
                type        :   "POST",
                data        :   {"sParameterNames":sParameterNames},// {action:'$funky'}
                datatype    :   "JSON",
                cache       :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
									//console.log(data['certificate_type']);
                                    //console.log(data);
                                    $('#id1').val(data['a'])
                                    $('#assess_id1').val(data['certificate_id']);
                                    $('#a_value').val(data['certificate_id']);
                                    $('#certificate_name1').val(data['certificate_name']);
                                    $('#title1').val(data['title']);
                                    $('#product_name1').val(data['product_id']);
                                    $('#assessment_type1').val(data['certificate_type']);
                                    //$('select[name="product_name1"] option[value="'+data['product_id']+'"]').attr("selected",true);
                                    //$('select[name="assessment_type1"] option[value="'+data['assessment_type']+'"]').attr("selected",true);
                                    if(data['certificate_type']=="product"){
                                       $('.assessment_change').show();
                                    }else{
                                       $('.assessment_change').hide();
                                    }
                                    $.ajax({
                                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/selected_training",
                                        type        :   "POST",
                                        data        :   {'id':data['certificate_type'],'prod_id':data['product_id'],'cat_id':data['cat_id'],'company_id':data['company_id']},// {action:'$funky'}
                                        datatype    :   "JSON",
                                        cache       :   false,
                                        success     :   function(data){
															//console.log(data);
                                                            var data=JSON.parse(data);
															$("#pre-selected-options").empty();
                                                            for(i=0;i<data.length;i++){
                                                                $("#pre-selected-options").append('<option value="'+data[i]['training_id']+'">'+data[i]['title']+'</option>');
                                                                $("#pre-selected-options").multiSelect('refresh');
                                                            }
                                                            $.ajax({
                                                                url         :   "<?php echo base_url(); ?>index.php?/controller_admin/training_sded",
                                                                type        :   "POST",
                                                                data        :   {"id":assess_id},// {action:'$funky'}
                                                                datatype    :   "JSON",
                                                                async       :   false,
                                                                success     :   function(data){
                                                                                    //console.log(data);
                                                                                    var data=JSON.parse(data);
                                                                                    //ajaxResult=[];
                                                                                    //ajaxResult=data;
                                                                                    for(i=0;i<data.length;i++){
                                                                                        $('#pre-selected-options').multiSelect().find(":checkbox[value='"+data[i]['training_id']+"']").attr("checked","checked");
                                                                                        $("#pre-selected-options option[value='" + data[i]['training_id'] + "']").attr("selected", 1);
                                                                                        $('#pre-selected-options').multiSelect("refresh");
                                                                                   }
                                                                                    //console.log(data[0]['training_id']);
                                                                                    //$("#pre-selected-options").multiselect("widget").find(":checkbox[value='"+data[0]['training_id']+"']").attr("checked","checked");
                                                                                    //$("#pre-selected-options option[value='" + data[0]['training_id'] + "']").attr("selected", 1);
                                                                                    //$("#pre-selected-options").multiselect("refresh");
                                                                                    //$('select[name="pre-selected-options"] option[value="'+data[0]['training_id']+'"]').attr("selected",true);
                                                                                    //$('#pre-selected-options').val(data[0]['training_id']);
                                                                                },
                                                            });
                                                        }
                                    });
									if(data['certificate_type']=='product'){
                                    	$.ajax({
	                                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category",
	                                        type        :   "POST",
	                                        data        :   {'id':data['cat_id']},// {action:'$funky'}
	                                        datatype    :   "JSON",
	                                        cache       :   false,
	                                        success     :   function(data){
	                                                            var data=JSON.parse(data);
	                                                            $('#category1').append('<option selected value="'+data['cat_id']+'">'+data['cat_name']+'</option>');
	                                                        }
	                                    });
                                    }
                    var assess_id=$('#assess_id1').val();
                    $.ajax({
		                  type 			: 		'POST',
		                  url 			: 		'<?php echo base_url(); ?>index.php?/controller_admin/get_count',
		                  data 			: 		{'assess_id1':assess_id},
		                  success 		: 		function (data) {
		                  							$('#quiz_a').empty();
		                  							$('#thrus_ha').empty();
		                  							if(data != ""){
		                  								$('#quiz_a').append('<option selected disabled>Select Questions to Assessment</option>');
		                  								$('#thrus_ha').append('<option selected disabled>Select Questions to Assessment</option>');
		                  								for(var i=1;i<=data;i++){
		                  									$('#quiz_a').append('<option value="'+i+'">'+i+'</option>');
		                  									$('#thrus_ha').append('<option value="'+i+'">'+i+'</option>');
		                  								}
		                  							}else{

		                  							}
		                  						}
		              	});
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/quiz_sded",
                        type        :   "POST",
                        data        :   {"id":assess_id},// {action:'$funky'}
                        datatype  	:   "JSON",
                        cache       :   false,
                        success     :   function(data){
                                            var data=JSON.parse(data);
                                            $('#quiz_show').empty();
                                            for(var i=0;i<data.length;i++){
                                                $('#quiz_show').append("<tr><td>"+data[i].question+"</td><td>"+data[i].choice1+"</td><td>"+data[i].choice2+"</td><td>"+data[i].choice3+"</td><td>"+data[i].choice4+"</td><td>"+data[i].correct_ans+"</td><td style='text-align:center'><button class='btn btn-circle blue btn-outline btn-icon-only' id="+data[i].id+" onClick='edit_quizz(this.id)'><i class='fa fa-edit'></i></button><button class='btn btn-circle red btn-outline btn-icon-only' id="+data[i].id+" onClick='deletes_quiz(this.id)'><i class='fa fa-trash'></i></button></td></tr>");
                                            }
                                        },
                    });
                    var node;
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/training_sded",
                        type        :   "POST",
                        data        :   {"id":assess_id},// {action:'$funky'}
                        datatype    :   "JSON",
                        async       :   false,
                        success     :   function(data){
                                            //console.log(data);
                                            var data=JSON.parse(data);
                                            ajaxResult=[];
                                            ajaxResult=data;
                                            //console.log(data[0]['training_id']);
                                            //$("#pre-selected-options").multiselect("widget").find(":checkbox[value='"+data[0]['training_id']+"']").attr("checked","checked");
                                            //$("#pre-selected-options option[value='" + data[0]['training_id'] + "']").attr("selected", 1);
                                            //$("#pre-selected-options").multiselect("refresh");
                                            //$('select[name="pre-selected-options"] option[value="'+data[0]['training_id']+'"]').attr("selected",true);
                                            //$('#pre-selected-options').val(data[0]['training_id']);
                                        },
                    });
                    var res=ajaxResult;
                    mulsel(ajaxResult);
                    },
            });
	});
        function  mulsel(ajaxResult){
           //console.log(ajaxResult);
           /*for(i=0;i<ajaxResult.length;i++){
                $('#pre-selected-options').multiSelect().find(":checkbox[value='"+ajaxResult[i]['training_id']+"']").attr("checked","checked");
                $("#pre-selected-options option[value='" + ajaxResult[i]['training_id'] + "']").attr("selected", 1);
                $('#pre-selected-options').multiSelect("refresh");
           }  */
        }
        function add_quizs(){
            $.ajax({
                type        :   "POST",
                url         :   "<?php echo site_url('controller_admin/check_assesment');?>",
                data        :   {'id':sParameterNames},
                datatype    :   "JSON",
                cache       :   false,
                success     :   function(data)
                            {
                                var data=JSON.parse(data);
                                //console.log(data);
                                 //$('#assessments').val(data['certificate_name']);
                                 $('.trainings').val(data['certificate_id']);
                                $.ajax({
                                    url         :   "<?php echo base_url();?>index.php?/Controller_admin/quiz_check",
                                    type        :   "POST",
                                    data        :   "",
                                    cache       :   false,
                                    success     :   function(data){
                                                        //console.log(data);
                                                        $('#quiz_id').val(data);
                                                    },
                                })
                                $('#myquiz').modal('show');
                            }
            });
        }
	var counter=0;
        $('#add_buttons').click(function(){
            counter += 1;
            var suffix=$('#quiz_id').val();
            var i = parseInt($('#quiz_id').val().replace(/\D/g, ''), 10);
            i=i+counter;
            $.strPad = function(i,l,s) {
                    var o = i.toString();
                    if (!s) { s = '0'; }
                    while (o.length < l) {
                            o = s + o;
                    }
                    return o;
            };
            var strr=$.strPad(i, 4);
            var str=('Quiz_'+strr);
            //$("#upload_quiz").append("<hr><div class='rowform-group fields_"+ counter + "'><div class='row'><div class='col-md-6 col-sm-12' style='display:none'><div class='form-group'><label class='control-label col-sm-3' for='email'>Quiz ID:</label><div class='col-sm-9'><input type='text' class='form-control' name='fields["+ counter + "][]' value="+str+" readonly></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Question:</label><div class='col-sm-9'><textarea class='form-control' id='question' name='fields["+ counter + "][]'></textarea></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option A:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_a' name='fields["+ counter + "][]'></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option B:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_b' name='fields["+ counter + "][]'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option C:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_c' name='fields["+ counter + "][]'></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option D:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_d' name='fields["+ counter + "][]'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Correct Answer:</label><div class='col-sm-9'><select class='form-control add_btn' id='fields_"+ counter + "' name='fields["+ counter +"][]'>><option selected diasbled>Select correct answer</option></select></div></div></div></div><div class='row'></div><div class='row' style='display:none'><div class='col-md-6 col-md-offset-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Assessment:</label><div class='col-sm-8'><input type='text' class='form-control trainings' id='trainings' name='fields["+ counter + "][]' readonly></div></div></div></div></div><hr>");
			$("#upload_quiz").append("<hr><div class='rowform-group fields_"+ counter + "'><div class='row'><div class='col-md-6 col-sm-12' style='display:none'><div class='form-group'><label class='control-label col-sm-3' for='email'>Quiz ID:</label><div class='col-sm-9'><input type='text' class='form-control' name='fields["+ counter + "][]' value="+str+" readonly></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Question:</label><div class='col-sm-9'><textarea class='form-control' id='question' name='fields["+ counter + "][]' style='resize:none'></textarea></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option A:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_a' name='fields["+ counter + "][]'></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option B:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_b' name='fields["+ counter + "][]'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option C:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_c' name='fields["+ counter + "][]'></div></div></div></div><div class='row'><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Option D:</label><div class='col-sm-9'><input type='text' class='form-control' id='option_d' name='fields["+ counter + "][]'></div></div></div><div class='col-md-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Correct Answer:</label><div class='col-sm-9'><select class='form-control add_btn' id='fields_"+ counter + "' name='fields["+ counter +"][]'>><option selected diasbled value=''>Select correct answer</option></select></div></div></div></div><div class='row'></div><div class='row' style='display:none'><div class='col-md-6 col-md-offset-6 col-sm-12'><div class='form-group'><label class='control-label col-sm-3' for='email'>Assessment:</label><div class='col-sm-8'><input type='text' class='form-control trainings' id='trainings' name='fields["+ counter + "][]' readonly></div></div></div></div></div><hr>");

            $.ajax({
                type        :"POST",
                url         :"<?php echo site_url('controller_admin/check_assesment');?>",
                data        :{'id':sParameterNames},
                datatype    :"JSON",
                cache       :false,
                success     :function(data)
                            {
                                var data=JSON.parse(data);
                                //console.log(data);
                                //$('#assessments').val(data['certificate_name']);
                                $('.trainings').val(data['certificate_id']);
                            }
            });
        $(".add_btn").focus(function(){
            var id=$(this).attr('id');
            var option_a=$('.'+id).find('#option_a').val();
            var option_b=$('.'+id).find('#option_b').val();
            var option_c=$('.'+id).find('#option_c').val();
            var option_d=$('.'+id).find('#option_d').val();
            if(option_a=="" || option_b=="" || option_c=="" || option_d==""){
                swal('All Options are Mandatory');
            }else{
                var myOptions = {
                   option_a : option_a,
                   option_b : option_b,
                   option_c : option_c,
                   option_d : option_d,
                };
                var mySelect =$("#"+id);
                mySelect.html('');
                mySelect.append('<option>Select correct answer</option>');
                $.each(myOptions, function(val, text) {
                   mySelect.append(
                       $('<option></option>').val(text).html(text)
                   );
               });
            }
        });

			});

       $('#addquiz').click(function(){
            $('#rowdata_1').empty();
            $('#Searching_Modal').modal('show');
            $.ajax({
               url         :   "<?php echo base_url();?>index.php?/Controller_admin/addquiz",
               type        :   "POST",
               data        :   $('#upload_quiz').serialize(),
               success     :   function(data){
                                $('#Searching_Modal').modal('hide');
                                if(data=="2"){
                                    swal({
										  title: 'All Fields are Mandatory',
										  type: "error",
										  showCancelButton: false,
										  confirmButtonClass: "btn-danger",
										  confirmButtonText: "Ok!",
										  cancelButtonText: "No, cancel plx!",
										  closeOnConfirm: false,
										  closeOnCancel: false
										},
										function(isConfirm) {
										  if (isConfirm) {
												window.location.reload();
										  }
										});
                                    /* $('#rowdata').append('All Fields are Mandatory');
                                      $('#sla_modal').animate({ scrollTop: 0 });
                                      $('.error').show(); */
								}
				                else{
									var data=JSON.parse(data);
									$('#myquiz').modal('hide');
								//	$('#sla_confirm').modal('show');
								//	for(i=0;i<data.length;i++){
										swal({
                                                          title: data,                                  
                                                          type: "success",
                                                          showCancelButton: false,
                                                          confirmButtonClass: "btn-danger",
                                                          confirmButtonText: "Ok!",
                                                          cancelButtonText: "No, cancel plx!",
                                                          closeOnConfirm: true,
                                                          closeOnCancel: false
                                                        },
                                                        function(isConfirm) {
                                                          if (isConfirm) {
                                                            window.location.reload();
                                                          }
                                                        });
								//	}
                                }
                            },
            });
       });
       function edit_quizz(id){
           $.ajax({
               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_quiz",
               type        :   "POST",
               data        :   {'id':id},// {action:'$funky'}
               datatype    :   "JSON",
               cache       :   false,
               success     :   function(data){
                                   var data=JSON.parse(data);
                                   //console.log(data);
                                   $('#quiz_id_s').val(data['id']);
                                   $('#quiz_id1').val(data['quiz_id']);
                                   //$('#trainings1').append('<option id="'+data['training_id']+'">'+data['title']+'</option>');
                                   $('select[name="trainings1"] option[value="'+data['certificate_id']+'"]').attr("selected",true);
                                   $('#question1').val(data['question']);
                                   $('#option_a1').val(data['choice1']);
                                   $('#option_b1').val(data['choice2']);
                                   $('#option_c1').val(data['choice3']);
                                   $('#option_d1').val(data['choice4']);
                                   $('#trainings1').val(data['certificate_id']);
									//$('#c_answer1').val(data['correct_ans']);
                                   $('#c_answer1').append('<option id="'+data['correct_ans']+'">'+data['correct_ans']+'</option>');
                                   //$('select[name="c_answer1"] option[value="'+data['correct_ans']+'"]').attr("selected",true);
                                   $('#edit_q').modal('show');
                               },
           });
       }
       function deletes_quiz(id){
    	   swal({
       		  title: "Are you sure?",
       		  text: "You will not be able to recover this Quiz!",
       		  type: "warning",
       		  showCancelButton: true,
       		  confirmButtonClass: "btn-danger",
       		  confirmButtonText: "Yes, Delete It!",
       		  cancelButtonText: "No, Keep It!",
       		  closeOnConfirm: false,
       		  closeOnCancel: false
       		},
       		function(isConfirm) {
       		  if (isConfirm) {
       			  $.ajax({
                         url         :   "<?php echo base_url(); ?>index.php?/controller_admin/deletequiz",
                         type        :   "POST",
                         data        :   {'id':id},// {action:'$funky'}
                         //datatype  :   "JSON",
                         cache       :   false,
                         success     :   function(data){
                                     		    if(data==1){
												   swal({
													  title: "Quiz Deleted successfully",
													  type: "success",
													  showCancelButton: false,
													  confirmButtonClass: "btn-danger",
													  confirmButtonText: "Ok!",
													  cancelButtonText: "No, keep it!",
													  closeOnConfirm: false,
													  closeOnCancel: false
													},
													function(isConfirm) {
													  if (isConfirm) {
														window.location.reload();
													  }
													});
											   }
                                             }
                         })
       		  }else{
				  swal.close();
				 }
       		});
       }
       $('#pre-selected-options').multiSelect();
       $('#c_answer').focus(function(){
	  	   var alerts=$('#c_answer option:selected').html();
	       var option_a=$('#option_a').val();
	       var option_b=$('#option_b').val();
	       var option_c=$('#option_c').val();
	       var option_d=$('#option_d').val();
	       if(option_a=="" || option_b=="" || option_c=="" || option_d==""){
	          swal('All Options are Mandatory');
	       }else{
	           var myOptions = {
	               option_a : option_a,
	               option_b : option_b,
	               option_c : option_c,
	               option_d : option_d,
	           };
	           var mySelect = $('#c_answer');
	           mySelect.html('');
	           mySelect.append('<option>Select correct answer</option>');
	           $.each(myOptions, function(val, text) {
	               mySelect.append(
	                   $('<option></option>').val(text).html(text)
	               );
	           });
	       }
       });
       $('#c_answer1').focus(function(){
            var alerts=$('#c_answer1 option:selected').html();
               var option_a=$('#option_a1').val();
               var option_b=$('#option_b1').val();
               var option_c=$('#option_c1').val();
               var option_d=$('#option_d1').val();
               if(option_a=="" || option_b=="" || option_c=="" || option_d==""){
                  swal('All Options are Mandatory');
               }else{
                   var myOptions = {
                       option_a : option_a,
                       option_b : option_b,
                       option_c : option_c,
                       option_d : option_d,
                   };
                   var mySelect = $('#c_answer1');
                   mySelect.html('');
                   mySelect.append('<option disabled>Select correct answer</option>');
                   $.each(myOptions, function(val, text) {
                       mySelect.append(
                           $('<option></option>').val(text).html(text)
                       );
                   });
               }
       });
       function submitquiz(){
       		var answers = [];
       	answers.push($("#option_a1").val());
       	answers.push($("#option_b1").val());
		answers.push($("#option_c1").val());
		answers.push($("#option_d1").val());
		var answercheck=answers.indexOf($("#c_answer1").val());
		if(answercheck>=0)
		{  
        $('#Searching_Modal').modal('show');
           $.ajax({
               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/submit_quiz",
               type        :   "POST",
               data        :   $('#edit_quiz').serialize(),// {action:'$funky'}
               datatype    :   "JSON",
               success     :   function(data){
                                   $('#Searching_Modal').modal('hide');
                                   data=$.trim(data);
                                   if(data=='Quiz updated successfully'){
                                	   swal({
                                 		  title: "Quiz updated successfully",
                                 		  type: "success",
                                 		  showCancelButton: false,
                                 		  confirmButtonClass: "btn-danger",
                                 		  confirmButtonText: "Ok!",
                                 		  cancelButtonText: "No, cancel plx!",
                                 		  closeOnConfirm: false,
                                 		  closeOnCancel: false
                                 		},
                                 		function(isConfirm) {
                                 		  if (isConfirm) {
                                 		    window.location.reload();
                                 		  }
                                 		});
                                   } else{
										swal(data);
                                  	}

                               },
           });
            }
   else
   {
swal("Error", "Answer should be one of the given options", "error")
   }
       };
       function update_edits(){
          var selecteds=$('#pre-selected-options').val();
          var id1=$('#id1').val();
          var assess_id1=$('#assess_id1').val();
          var assessment_type1=$('#assessment_type1').val();
          var certificate_name1=$('#certificate_name1').val();
          var product_name1=$('#product_name1').val();
          var category1=$('#category1').val();
          var title1=$('#title1').val();
          var quiz_a=$('#quiz_a option:selected').val();
          var thrus_ha=$('#thrus_ha option:selected').val();
          if( quiz_a == "Select Questions to Assessment" || thrus_ha == "Select Questions to Assessment" ){
          	swal('Questions to Assessment and Threshold is mandatory');
          }
          else if(quiz_a<thrus_ha){
          	//alert("incorrect");
          	swal("No. of Questions should be greater than equal to Threshold");
          }
         else{
    	   	$.ajax({
               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/updated_quizes",
               type        :   "POST",
               data        :   {'selecteds':selecteds,'id1':id1,'assess_id1':assess_id1,'certificate_name1':certificate_name1,'assessment_type1':assessment_type1,'product_name1':product_name1,'category1':category1,'title1':title1,'quiz_a':quiz_a,'thrus_ha':thrus_ha},// {action:'$funky'}
               datatype    :   "JSON",
               success     :   function(data){
                                   data=$.trim(data);
                                   if(data==1){
                                	   swal({
                                 		  title: "Assessment Updated Successfully",
                                 		  type: "success",
                                 		  showCancelButton: false,
                                 		  confirmButtonClass: "btn-danger",
                                 		  confirmButtonText: "Ok!",
                                 		  cancelButtonText: "No, cancel plx!",
                                 		  closeOnConfirm: false,
                                 		  closeOnCancel: false
                                 		},
                                 		function(isConfirm) {
                                 		  if (isConfirm) {
                                 		    window.location.href="<?php echo site_url('controller_admin/certificate/#portlet_tab2_2');?>";
                                 		  }
                                 		});
                                   } else{
										swal(data);
                                  	}
                               },
       		});
       	}
       };
       $("#product_name1").change(function () {
            $('#category1').empty();
            var product_id = $('#product_name1').val();
            $.ajax({
                url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                type        :   "POST",
                data        :   {'product_id' : product_id},
                datatype    :   "JSON",
                cache       :   false,
                process     :   false,
                success     :   function(data){
                                    var data=JSON.parse(data);
                                    if(data == 1){
                                    	 swal('No Sub Category are entered, kindly add category to this product');
                                       // bootbox.alert("No Category are entered, kindly add category to this product");
                                        $('#category1').empty();
                                    }else{
                                       //console.log(data);
                                       $('#category1').append('<option selected diabled>Select Sub category</option>');
                                        for(i=0; i<data.length;i++){
                                            $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                        }
                                    }
                                },
            })
        });
       </script>
	   <script>
           $('#bulkupload').click(function(){
            var a_value=$('#a_value').val();
            var c_id=$('#c_id').val();
            var ext = $('#add_excel').val().toString().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
               alert('Please upload Excel file');
               return false;
            }else {
               var file_data = $('#add_excel').prop('files')[0];
               var form_data = new FormData();
               form_data.append('a_value', a_value);
               form_data.append('c_id', c_id);
               form_data.append('add_excel', file_data);
               $('#Searching_Modal').modal('show');
               $.ajax({
                  type:'POST',
                  url:'<?php echo base_url(); ?>index.php?/controller_admin/bulk_quiz',
                  contentType:false,
                  processData: false,
                  cache:false,
                  data:form_data,
                  success: function (data) {
                    $('#Searching_Modal').modal('hide');
				 // data=JSON.parse(data);
				  for(i=0;i<data.length;i++)
				  {
                                if(data[i].response=="2"){
                                       /* swal({
                                                  title: 'All Fields are Mandatory',
                                                  type: "error",
                                                  showCancelButton: false,
                                                  confirmButtonClass: "btn-danger",
                                                  confirmButtonText: "Ok!",
                                                  cancelButtonText: "No, cancel plx!",
                                                  closeOnConfirm: false,
                                                  closeOnCancel: false
                                                },
                                                function(isConfirm) {
                                                  if (isConfirm) {
                                                        window.location.reload();
                                                  }
                                                }); */
                                         $('#rowdata').append('All Fields are Mandatory');
                                          $('#sla_modal').animate({ scrollTop: 0 });
                                          $('.error').show();
                           }else{
                                    var data=JSON.parse(data);
                                    $('#sla_modal').modal('hide');
                                    $('#sla_confirm').modal('show');
                                    for(i=0;i<data.length;i++){
                                            $('#sla_detail_confirm').append(data[i].response+"</br><hr>");
                                        }
                /* swal({
                          title: data,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ok!",
                          cancelButtonText: "No, cancel plx!",
                          closeOnConfirm: false,
                          closeOnCancel: false
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                                window.location.reload();
                          }
                        }); */
                  }
				  }
				  },
               });
            }
         })
          $('#confirm_sla').click(function(){
			location.reload();
		  });
	   		function cancel_edits(){
		   		window.location.href="<?php echo site_url('controller_admin/certificate/#portlet_tab2_2');?>";
	   		}
	   		$('#quiz_a').focus(function(){
	   			var assess_id1=$('#assess_id1').val();
	   			$.ajax({
                  type 			: 		'POST',
                  url 			: 		'<?php echo base_url(); ?>index.php?/controller_admin/get_count',
                  data 			: 		{'assess_id1':assess_id1},
                  success 		: 		function (data) {
                  							$('#quiz_a').empty();
                  							if(data != ""){
                  								$('#quiz_a').append('<option selected disabled>Select Questions to Assessment</option>');
                  								for(var i=1;i<=data;i++){
                  									$('#quiz_a').append('<option value="'+i+'">'+i+'</option>');
                  								}
                  							}else{

                  							}
                  						}
              	});
	   		})

	   		$('#thrus_ha').focus(function(){
	   			var assess_id1=$('#assess_id1').val();
	   			$.ajax({
                  type 			: 		'POST',
                  url 			: 		'<?php echo base_url(); ?>index.php?/controller_admin/get_count',
                  data 			: 		{'assess_id1':assess_id1},
                  success 		: 		function (data) {
                  							$('#thrus_ha').empty();
                  							if(data != ""){
                  								$('#thrus_ha').append('<option selected disabled>Select Questions to Assessment</option>');
                  								for(var i=1;i<=data;i++){
                  									$('#thrus_ha').append('<option value="'+i+'">'+i+'</option>');
                  								}
                  							}else{

                  							}
                  						}
              	});
	   		})
       </script>
    </body>
</html>