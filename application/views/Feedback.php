<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'assets/lib/cssscript.php'?>
    <style>		
		
	</style>
</head>

<body>	
	
    <div class="container">
        <div class="card card-container col-lg-6 col-lg-offset-3" style="background-color:#fff;margin-top:3%">            
            <p id="profile-name" class="profile-name-card"></p>	
  <?php if($ticketcurrentstatus!=12||$ticketcurrentstatus!='12'){?>	
            <form id="register" style="padding:2%">
				<span class="error_msg" style="color:red"><p id="error_msg"></p></span>
                <span id="reauth-email" class="reauth-email"></span>
            <div class="form-group">
                    <label for="username">Email:</label>
                    <input type="text" id="email" name="email" value="<?php echo $emailid;?>" class="form-control" readonly>
            	</div>				
                <!--<div class="form-group">
                    <label for="username">Company Name:</label>
                    <input type="text" id="c_name" name="c_name" class="form-control"readonly>
            	</div>	-->	
            	 <div class="form-group" style="display:none;">
                    <label for="username">Ticket ID:</label>
                    <input type="text" id="ticket_id" name="ticket_id" value="<?php echo $ticket_id;?>" class="form-control" readonly>
            	</div>					
                <div class="form-group" style="display:none;">
                    <label for="username">Ticket ID:</label>
                    <input type="text" id="primary_key" name="primary_key" value="<?php echo $primaryid;?>" class="form-control" readonly>
            	</div>	
              	<div class="form-group">
                    <label for="password">Customer Feedback:</label>
                    <!-- <input type="text" id="password" name="password" class="form-control" placeholder="Feedback" required autofocus> -->
					<select class="form-control" id="password"  name="password" required>
                        <option value="" >No Customer Feedback </option>
                        <option value="Excellent">Excellent</option>
                        <option value="Satisfied">Satisfied</option>
                        <option value="Very Good">Very Good</option>
                        <option value="Average">Average</option>
                        <option value="Bad">Bad</option>
                    </select>
					<span id="result"></span>
					<span id="results" style="color:red"><p id="error_password"></p></span>					
              	</div>				
              	<div class="form-group">
                    <label for="rating">Customer Rating (Rating upto 5):</label>
					<select class="form-control" name="c_password" id="c_password" required>
					<option value="1"> 1</option>
					<option value="2"> 2</option>
					<option value="3"> 3</option>
					<option value="4"> 4</option>
					<option value="5"> 5</option>
				   </select>
                    <!-- <input type="number" id="c_password"  min="0" max="5" onkeypress="restrictMinus(event);" name="c_password" class="form-control" placeholder="Rating upto 5" required> -->
					<span id="results" style="color:red"><p id="error_cpassword"></p></span>						
              	</div>
               	<button class="btn btn-lg btn-primary col-lg-offset-5" type="button" id="password_enter">Submit</button>
            </form>

              <?php }else {?>
              Thank You for your feedback
               <?php }?>
        </div>
    </div>
    <?php include 'assets/lib/javascript.php'?>	

	<script>
		/*function getParameterByName(name, url) {
			if (!url) {
			  url = window.location.href;
			}
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		var user = getParameterByName('user');		
		var ticket_id = getParameterByName('ticket_id');

		if(user=="" || ticket_id=="" ){
			alert("Url is not valid");
		}else{
			$('#email').val(user);
			$('#ticket_id').val(ticket_id);
		}*/
	</script>
	<script>
//window.history.forward();	
function restrictMinus(e) {
        var drpCatEarnning = document.getElementsByTagName('input[type=number]');
        var inputKeyCode = e.keyCode ? e.keyCode : e.which;
        if (inputKeyCode != null) {
            if (inputKeyCode == 45) e.preventDefault();
        }
      }	
		$('#password_enter').click(function(){
			var feedback=$("#password").val();
			var rating=$("#c_password").val();
		
			if(feedback !="" && rating != ""){
					$.ajax({
						url         :   "<?php echo base_url(); ?>index.php?/webservice/close_tkt",
						type        :   "POST",
						data        :   $('#register').serialize(),// {action:'$funky'}
						//datatype	:	"JSON",	
						cache       :   false,
						success    	: 	function(data){	

							
//if(data==1){
swal({
	 title: "",
						     text: "Thanks for your update",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
                           window.top.close();
									       }
					        }); 
         				//}else{
//$('#error_msg').html(data);
//}
},
										
					});
				
			}else if(feedback == ""){
				$('#error_password').html('Fields are mandatory');
			}else if(rating == ""){
				$('#error_cpassword').html('Fields are mandatory');
			}			
			$('#password').keyup(function(){
				$('#error_password').html("");
			});						
			$('#c_password').keyup(function(){
				$('#error_cpassword').html("");
			});
		});		
		
	</script>
   
</body>
</html>