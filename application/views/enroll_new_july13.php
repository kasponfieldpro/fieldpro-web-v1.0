<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include 'assets/lib/cssscript.php'?>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
	 <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header_superad.php"?>
            <!-- END HEADER -->
            <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/superad_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                   
                    <!-- END BREADCRUMBS -->
				   <div class="tab-pane" id="tab_2">
                                        <!--<div class="portlet box green">-->
										
                                        <div class="portlet box dark">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-address-card-o"></i>Create New Organisation</div>
                                                
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form id="parsing" class="form-horizontal">
                                                    <div class="form-body">
                                                        <h3 class="form-section">Company Info</h3>
                                                         <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Company Name</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" name="company_name" id="company_name">
                                                                       <!-- <span class="help-block"> This is inline help </span>-->
                                                                    </div>
                                                                </div>
                                                            </div>

												<div class="col-md-6">		
<div class="form-group">												
														<label class="control-label col-md-3">Upload Logo</label>
														<div class="col-md-9">
															
															<input type="text" class="form-control" id="fileUpload" name="fileUpload" style="display:none" />
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="input-group input-large">
																	<div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
																		<i class="fa fa-file fileinput-exists"></i>&nbsp;
																		<span class="fileinput-filename"> </span>
																	</div>
																	<span class="input-group-addon btn default btn-file" style="color: black !important;">
																		<span class="fileinput-new"> Select file </span>
																		<span class="fileinput-exists"> Change </span>
																		<input type="file" name="fileName" id="fileName"> </span>
																	<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
															</div>
														</div>
                                               <!--  <input type="file" class="form-control" id="product_image1" name="product_image1" /> -->
                                            </div>
                                        </div>
					</div>		
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Technicians</label>
                                                                    <div class="col-md-9">
                                                                         <input type="text" class="form-control" id="no_tech" name="no_tech" min="1" max="100" placeholder="Digits only">
                                                                        <!--<span class="help-block"> Select your gender. </span>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Service-Desks</label>
                                                                    <div class="col-md-9">
																	    <input type="text" class="form-control"  id="no_servd" name="no_servd" placeholder="Digits only">
                                                                        <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
																		</div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Contact Details</label>
                                                                    <div class="col-md-9">
																	 <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span>
                                                           <input type="text" class="form-control" placeholder="Contact Number" id="contact" name="contact"> </div>
																</div>
                                                                        <!--<select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                                            <option value="Category 1">Category 1</option>
                                                                            <option value="Category 2">Category 2</option>
                                                                            <option value="Category 3">Category 5</option>
                                                                            <option value="Category 4">Category 4</option>
                                                                        </select>-->
                                                                    </div>
                                                                </div>
                                                         
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Subscription Plan</label>
                                                                    <div class="col-md-8  col-md-offset-1">
                                                                        <div class="radio-list">
                                                                       <label class="radio-inline">
                                            <input type="radio" id="time_yr" name="time_yr" class="time_yr" value="category1"/> 1 month Trail </label>
                                                <label class="radio-inline">
                                  <input type="radio" id="time_yr" name="time_yr" class="time_yr" value="category2"/> Quarterly</label>
																			
                                   <label class="radio-inline">
                                     <input type="radio" id="time_yr" name="time_yr" class="time_yr" value="category3" /> Half-yearly</label>
																			
                                  <label class="radio-inline" style="padding-left: 11px !important;">
                                   <input type="radio" id="time_yr" name="time_yr" class="time_yr" value="category4" /> Annual</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                  <div class="row" style="display:none !important;">
                                                     <div class="form-group">
                                      <input type="text" class="form-control" id="renew_date" name="renew_date">
                                                         </div>
                                                   </div>
                                                        <h3 class="form-section">Address</h3>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Door/Plot No.</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" name="flat_no" id="flat_no"> </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                  <label class="control-label col-md-3">Street/ Locality</label>
                                                                   <div class="col-md-9">
                                                                     <input type="text" class="form-control" id="street" name="street"> 
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Town</label>
                                                                    <div class="col-md-9">
                                                                      <input type="text" class="form-control" name="company_town" id="company_town"> </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                  <label class="control-label col-md-3">Land mark</label>
                                                                   <div class="col-md-9">
                                                                     <input type="text" class="form-control" id="landmark" name="landmark"> 
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">City</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" id="comp_addr" name="comp_addr"> </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
															 <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Country</label>
                                                                   <div class="col-md-9">
																	 <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-globe"></i>
                                                                    </span>
                                                                   <!-- <input type="text" class="form-control" id="country" name="country"> -->
																		 <select class="form-control" id="country" name="country"> </select> 
																	</div>
																</div>
                                                                </div>
                                                            </div>
                                                           
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
															 <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">State</label>
                                                                    <div class="col-md-9">
                                                                       <!-- <input type="text" class="form-control" id="state" name="state"> -->
																		<select class="form-control" id="state" name="state"> </select>
																	 </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Post Code</label>
                                                                   <div class="col-md-9">
																	 <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-map-marker"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" id="post" name="post"> 
                                                                   </div>
                                                                  </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                           
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
														<!--New Section -->
														 <h3 class="form-section">Admin Info</h3>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">First Name</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" name="fname" id="fname" > </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Last Name</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" name="lname" id="lname"> </div>
                                                                </div>
                                                            </div>
                                                        </div>
														<!--/row-->
														 <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Contact Number</label>
                                                                    <div class="col-md-9">
                                                                        <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" id="contact_num" name="contact_num"> </div> 
																</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Email Address</label>
                                                                    <div class="col-md-9">
                                                                         <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" id="ad_email" name="ad_email"></div>
																	</div>
                                                                </div>
                                                            </div>
                                                        </div>
													
                                                    <div class="form-actions">
                                                        <div class="row">
                                                                <div class="col-md-12">  
								   <div class="text-center">
                                                                        <input type="button" class="btn green-haze btn-outline btn-circle btn-md" value="Submit" onClick="submitData();">
                                                                        <input type="reset" class="btn red-haze btn-outline btn-circle btn-md" value="Cancel">
                                                                    </div>
																</div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
		            </div>
				 
                    </div>
					</div>
					</div>
              <!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
        </div>
	<?php include 'assets/lib/javascript.php'?>    
 
        <script>                
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#focus_newsubs').addClass('open');
        </script>
   
 <script>
			/*document.getElementById("customButton").addEventListener("click", function(){
				document.getElementById("fileUpload").click();  // trigger the click of actual file upload button
			});
			document.getElementById("fileUpload").addEventListener("change", function(){
				var fullPath = document.getElementById('fileUpload').value;
				document.getElementById("fileName").style.visibility='visible';
				//var fileName = fullPath.split(/(\\|\/)/g).pop();  // fetch the file name
				//document.getElementById("fileName").innerHTML = fileName;  // display the file name
			}, false);*/
	</script>
    <script type="text/javascript">
	$('#no_tech').on('input', function (event) { 
			this.value = this.value.replace(/[^0-9]/g, '');
		});
	$('#no_servd').on('input', function (event) { 
			this.value = this.value.replace(/[^0-9]/g, '');
		});
      $(document).ready(function() {
	  
     // 	$('#parsing').parsley();
		  populateCountries("country", "state");
      	      
      $("#no_servd").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
       /*$('#post').on('input', function (event) { 
                 this.value = this.value.replace(/[^0-9]/g, '');
          }); */
   
     $("#no_tech").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
		// ^(?=.*?[1-9])[0-9()-]+$
		 
      	$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_superad/noti_count",
				    type: 'POST',
					success: function(data) {
						$('.noti-dot').html('');
					console.log(data);
					$('.noti-dot').html(data);
                            }							
		
			});
             
      	$.ajax({
				url: "<?php echo base_url();?>" + "index.php?/controller_superad/notification",
				    type: 'POST',
					success: function(data) {
						$('#content1').html('');
					console.log(data);
					$('#content1').append('<ul class="list-group list-no-border user-list">');							
							//data=JSON.parse(data);
						
							  if(data.length<=0){
			                         $('#content1').append(" <li class='list-group-item'><div class='avatar'>No Companies for Renewal</li>");
							  }
							  else {
							for(i=0;i<data.length;i++){							
							//var company_logo = data[i].company_logo.replace(new RegExp("\\\\", "g"), "");
var company_logo = data[i].company_logo;
				$('#content1').append(" <li class='list-group-item'><div class='avatar'><img src='"+company_logo+"' width='30px' height='30px' alt=''><span class='user-desc'><div class='name'>"+data[i].company_name+"</span></div><div class='time'>"+data[i].renewal_date+"</div></div></li>");
					}
				}
				$('#content1').append('</ul>');		
				}
			});
      });
      
      $('.time_yr').change(function(){
       var yr=$('input[name=time_yr]:checked').val();
     // alert(yr);
      $.ajax({
      	url: "<?php echo base_url();?>"+"index.php?/controller_superad/renewal_date1",
      	method: "POST",
      	data: {'yr': yr},
      	success:function(data){
           $('#renew_date').html('');
      	console.log(data);
    // alert(data);
      	  $('#renew_date').val(data);
      	}
      });
      
        });
		
		function cancel_butn() {
		sweetAlert("Oops...", "Something went wrong!", "error");
		}
		
      function validate() {
                 $inputVal= $("#no_servd").val();
                $characterReg = /^?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/
               if(!$characterReg.test($inputVal)) {
                    //$("#no_servd").after('<span class="error">Maximum 8 characters.</span>');
               } alert("Positive integers only"); $inputVal.val = "";
                     // this.focus(); 
     }
	function submitData(){
			/* var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
				//var email = getElementById("ad_email");
				var email = $('#ad_email').val();
				var emailValue = email.value;
				var testEmail = re.test(emailValue);
		 if(!testEmail){
				 swal("Please enter a valid email");
		 }*/ 
		var testReg = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]{7,13}$/;
		var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var pincodereg=/^[0-9]{6}$/;
                            var users_count=/^[1-9][0-9]?$|^100$/;
	 	var c_name= $("#company_name").val();
		var techo= $("#no_tech").val();
		var serv= $("#no_servd").val();
		var input = $("#contact").val();
		var period= $('input[name=time_yr]:checked').val();		
		var flat= $("#flat_no").val();
		var str= $("#street").val();
		var ctown= $("#company_town").val();
		var clandmark= $("#landmark").val();
		var caddr= $("#comp_addr").val();
		var ccountry= $("#country").val();
		var cstate= $("#state").val();
		var cpost= $("#post").val();
		var aname= $("#fname").val();
		var alname= $("#lname").val();
		var inputval = $("#contact_num").val();
		var emailinput = $('#ad_email').val();
		
		/* alert(c_name);
		alert(techo);
		alert(serv);
		alert(input); 
	if((c_name== "null" || c_name== '') && (techo== "null" || techo== '') && (serv== "null" || serv== '') && (input== "null" || input== '') && (period== "null" || period== '') && (flat== "null" || flat== '') && (str== "null" || str== '') && (ctown== "null" || ctown== '') && (clandmark== "null" || clandmark== '') && (caddr== "null" || caddr== '') && (ccountry== "null" || ccountry== '') && (cstate== "null" || cstate== '') && (cpost== "null" || cpost== '') && (aname== "null" || aname== '') && (alname== "null" || alname== '') && (inputval== "null" || inputval== '') && (emailinput== "null" || emailinput== '')) */
		//alert(ccountry.length);
       // alert(cstate);
       // return false;
		if(c_name == "" || techo == "" || serv == "" || input == "" || period == undefined || flat == "" || str == "" || ctown == "" || clandmark == "" || caddr == "" || ccountry == "-1" || cstate == null || cpost == "" || aname == "" || alname == "" || inputval == "" || emailinput == "")
			{
					swal("Fill form to Submit details.");
			}
			else if(!testReg.test(input))
			{
				  swal("Enter valid Contact Details");
			}
			else if(!testReg.test(inputval))
			{
				  swal("Enter valid Contact Number");
			}
    		else if (email_reg.test(emailinput) == false) {
       			 swal("Please enter a valid email");
		   }
           		else if(pincodereg.test(cpost)== false)
          		 {
           		    swal("Please enter a valid Postal code");
           		}
		else if(users_count.test(techo)== false)
           		 {
                	   swal("Technicians count should range from 1 to 100!");
            		}
                            else if(users_count.test(serv)== false)
           		 {
                	   swal("ServiceDesk count should range from 1 to 100!");
            		}
                          


		  else {
        //confirm("Is " + emailValue + " the email you want to use?");
  
      	    	var data = new FormData($('#parsing')[0]);
        var data1 = new FormData($('#parsing')[0]);
		var subscription_image = $('#fileName').prop('files')[0];
		
			if(subscription_image){
               //alert("1");
			    var ext = $('#fileName').val().toString().split('.').pop().toLowerCase();
				if ($.inArray(ext, ['jpeg', 'png', 'jpg']) == -1) {
					$("#myModal1").modal('hide');
					swal({
						title:"Upload an Image File",
						//text: "No file Chosen!",
						type:"warning",
						showCancelButton: false,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Ok.",
						cancelButtonText: "Cancel !",
						closeOnConfirm: false,
						closeOnCancel: false
			   },
				function(isConfirm){
					if (isConfirm) {
					    swal.close();
						
					} else {
					swal.close();
					}
			   });	
			}
				
		else {
			
      	 $.ajax({
      		type:"POST",
      		url: "<?php echo base_url();?>"+"index.php?/controller_superad/imageof",
      		data:data,
      		mimeType: "multipart/form-data",
      		contentType: false,
      		cache: false,
      		processData: false,
      		success:function(data)
      	 {
                  //alert(data);
		  //return false;
      		$.ajax({
      			type:"POST",
      			url: "<?php echo base_url();?>"+"index.php?/controller_superad/sendof",
      			data: $('#parsing').serialize() + '&data=' + data,
      			success:function(data){
					console.log(data);
                      data=$.trim(data); 
                                  
             if(data=="Provide Proper Mobile Number or/and Email Id")  {
                          // alert ("Provide Proper Mobile Number or/and Email Id")
                           swal("Provide Proper Contact Number");
			 }
              else if(data=="All Fields are Mandatory")  {
                              //alert("All Fields are Mandatory");
                                swal("All Field Are Mandatory")
                        }
              else if(data=="Duplication occured")  {
                      swal("Duplicate entry, Check Company Name & Admin Details");
               }
                   else 
                  {
                        $.ajax({
      					type:"POST",
      					url: "<?php echo base_url();?>"+"index.php?/controller_superad/send_mail",
      					data: $('#parsing').serialize() + '&data=' + data,
								success:function(data){
      						//swal("Good job!", "Successfully Registered", "success")
							console.log(data);
                                       swal({
	                                             title: "Good job!",
						     text: "Successfully Registered",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
										 location.reload();
									       }
					        }); 
                           }
      					
      			          });
      				  }
      		
      			   }
      		   });
      	    }
      
          });
	    }
	  }	
		
	else {
        //alert("2");
      		$.ajax({
      			type:"POST",
      			url: "<?php echo base_url();?>"+"index.php?/controller_superad/sendof1",
      			data: $('#parsing').serialize() ,
      			success:function(data){
					console.log(data);
                      data=$.trim(data);  
                                 
             if(data=="Provide Proper Mobile Number or/and Email Id")  {
                          // alert ("Provide Proper Mobile Number or/and Email Id")
                            swal("Provide Proper Contact Number");
                        }
              else if(data=="All Fields are Mandatory")  {
                              //alert("All Fields are Mandatory");
                                swal("All Field Are Mandatory")
                        }
              else if(data=="Duplication occured")  {
                      swal("Duplicate entry, Check Company Name & Admin Details");
               }
                   else 
                  {
                           $.ajax({
      					type:"POST",
      					url: "<?php echo base_url();?>"+"index.php?/controller_superad/send_mail",
      					data: $('#parsing').serialize() + '&data=' + data,
								success:function(data){
      						//swal("Good job!", "Successfully Registered", "success")
							console.log(data);
                                       swal({
	                                             title: "Good job!",
						     text: "Successfully Registered",
						     type: "success",
						     confirmButtonClass: "btn-primary",
						     confirmButtonText: "Ok.",
						     closeOnConfirm: false,
						},
						function(isConfirm) {
								if (isConfirm) {
										 location.reload();
									       }
					        }); 
                           }
      					
      			          });
      				  }
      		
      			   }
      		   });
      	    }
	}
    }
      
    </script>	
	</body>
</html>