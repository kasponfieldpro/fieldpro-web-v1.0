<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
    <![endif]-->
<!--[if IE 9]> 
    <html lang="en" class="ie9 no-js">
        <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <?php 
                    $company_id=$this->session->userdata('companyid');
                         $region=$user['region'];$area=$user['area'];$location=$user['location'];
                    include 'assets/lib/cssscript.php'?>
        <style>
            .dataTables_filter {
                text-align: right;
            }
            
            .dt-buttons {
                display: none;
            }
/*.uneditable-input {
min-width:auto !important;
}*/
@media screen and (max-width:740px) {
.knowledge-portlet {
padding: 7px 2px !important;
}
}
        </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/header_service.php"?>
            <!-- END HEADER -->
             <div class="page-container">
               <div class="page-sidebar-wrapper">
               <?php include "assets/lib/service_sidebar.php"?>
               </div>
               <div class="page-content-wrapper">
                  <div class="page-content">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box dark">
                                    <div class="portlet-title">
                                        <div class="caption"> KNOWLEDGE BASE </div>
                                        <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#unassigned" data-toggle="tab">Knowledge Base</a>
                                                    </li>
                                                    <li>
                                                        <a href="#assigned" data-toggle="tab">Awaiting Approval</a>
                                                    </li>
                                                    <li>
                                                        <a href="#accepted" data-toggle="tab">Add to Knowledge Base</a>
                                                    </li>
                                                </ul>
                                    </div>
                                    <div class="portlet-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="unassigned">
                                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                                <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="category" onChange="day();" name="role1">
                                                                    <option value="" selected>Select Sub-Category</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                                <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product" onChange="get_cat(this.value); day();" name="role1">
                                                                    <option value="" selected>All Product-Category</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="table=responsive" style="padding-top: 5%;">
                                                            <table class="table table-hover table-bordered datatable" id="">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Product-Category</th>
                                                                        <th class="text-center">Sub-Category</th>
                                                                        <th class="text-center">Problem Description</th>
                                                                        <th class="text-center">Solution</th>
                                                                        <th class="text-center">Image</th>
                                                                        <th class="text-center">Video</th>
                                                                        <th class="text-center" style="width: 12%;">Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="show_knowledge" align="center"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="assigned">
                                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                                <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="category1" onChange="day1();" name="role1">
                                                                    <option value="" selected>All Product-Category</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                                <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product1" onChange="get_cat1(this.value); day1();" name="role1">
                                                                    <option value="" selected>Select Sub-Category</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="table=responsive" style="padding-top: 5%;">
                                                            <table class="table table-hover table-bordered datatable1" id="">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="text-align:center;">Product-Category</th>
                                                                        <th style="text-align:center;">Sub-Category</th>
                                                                        <th style="text-align:center;">Problem Description</th>
                                                                        <th style="text-align:center;">Solution</th>
                                                                        <th style="text-align:center;">Image</th>
                                                                        <th style="text-align:center;">Video</th>
                                                                        <th style="text-align:center; width: 100px;">Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="knowledge" align="center"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="accepted">
                                                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                                            <div class="form-wizard">
                                                                <div class="form-body  col-lg-offset-1 col-lg-9">
                                                                    <div class="row">
                                                                        <div class="col-lg-offset-5 col-lg-6">
                                                                            <div class="input-group">
                                                                                <div class="icheck-inline">
                                                                                    <label>
                                                                                        <input type="radio" name="radio2" id="prod_base" onClick="check();" checked class="icheck"> Product-Base</label>
                                                                                    <label>
                                                                                        <input type="radio" name="radio2" id="generic" onClick="check();" class="icheck"> Generic</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row" id="product2_div">
                                                                        <label class="control-label col-lg-5">Product-Category</label>
                                                                        <input type="hidden" class="form-control" id="edit_id" name="edit_id">
                                                                        <div class="col-lg-5">
                                                                            <select class="form-control " id="product2" onchange='get_cat2(this.value);' name="role1">
                                                                                <option value="" selected disabled>Select Product-Category</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row" id="cat2_div" style="display:none;">
                                                                        <label class="control-label col-lg-5">Sub-Category</label>
                                                                        <div class="col-lg-5">
                                                                            <select class="form-control " id="category2" name="role1">
                                                                                <option value="" selected>Select Sub-Category</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row">
                                                                        <label class="control-label col-lg-5">Problem Description</label>
                                                                        <div class="col-lg-5">
                                                                            <textarea class="form-control" id="problem" style=" width: 100%;    height: 70%; resize: none;"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row">
                                                                        <label class="control-label col-lg-5">Solution</label>
                                                                        <div class="col-lg-5">
                                                                            <textarea class="form-control" id="solution" style=" width: 100%;    height: 70%; resize: none;"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row">
                                                                        <label class="control-label col-lg-5">Upload Image</label>
                                                                        <div class="col-lg-5">
                                                                            <div class=" fileinput fileinput-new" data-provides="fileinput">
                                                                                <div class="input-group input-large">
                                                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                        <span class="fileinput-filename"> </span>
                                                                                    </div>
                                                                                    <span class="input-group-addon btn default btn-file">
                                                                                            <span class="fileinput-new" id="span-button"> Select file </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" id="fileUpload_image"> </span>
                                                                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <div class="row">
                                                                        <label class="control-label col-lg-5">Upload Video</label>
                                                                        <div class="col-lg-5">
                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                <div class="input-group input-large">
                                                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                        <span class="fileinput-filename"> </span>
                                                                                    </div>
                                                                                    <span class="input-group-addon btn default btn-file">
                                                                                            <span class="fileinput-new" id="span-button"> Select file </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" id="fileUpload_video"> </span>
                                                                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                </div>
                                                                <!--<div class="form-actions">-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="text-center">
                                                                            <button type="button" class="btn blue btn-outline btn-md btn-circle " onClick="add_knowledge();">Add</button>
                                                                            <button type="reset" class="btn red btn-outline btn-md btn-circle " onClick="reset();">Reset</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--</div>-->
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>

            <!-- BEGIN FOOTER -->
            <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
    </div>
    <!--Modal Starts-->
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Media File</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id='modal_display'>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-circle red btn-outline btn-sm" data-dismiss="modal" onClick="stop();">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="edits" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Knowledge Base</h4>
                </div>
                <div class="modal-body">
                    <div class="error" style="display:none">
                        <label id="rowdata_1"></label>
                    </div>
                    <form class="form-horizontal" id="edit_technician">
                        <div class="form-group" id="dis_prod">
                            <label class="control-label col-sm-4" for="email">Product-Category</label>
                            <div class="col-sm-6">
                                <input type="hidden" class="form-control" id="edit_id" name="edit_id">
                                <select class="form-control m-top-md" id="display_product" onchange='display_cat(this.value);'>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="dis_cat">
                            <label class="control-label col-sm-4" for="email">Sub-Category</label>
                            <div class="col-sm-6">
                                <select class="form-control m-top-md" id="display_category"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Problem Description</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="edit_problem" name="edit_problem" style=" width: 100%;    height: 70%; resize: none;"></textarea>
                                <!-- <input type="text" class="form-control" id="edit_problem"  placeholder="First Name">-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Solution </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="edit_solution" name="edit_solution" style=" width: 100%;    height: 70%; resize: none;"></textarea>
                                <!--   <input type="text" class="form-control" id="edit_solution" name="edit_solution" placeholder="Last Name">-->
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <div class="col-sm-6">
                                <input type="hidden" class="form-control" id="hidden_prod" name="hidden_prod">
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <div class="col-sm-6">
                                <input type="hidden" class="form-control" id="hidden_cat" name="hidden_cat">
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <div class="col-sm-6">
                                <textarea class="form-control" id="hidden_problem" name="hidden_problem" style=" width: 100%;    height: 70%; resize: none;display:none;">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <div class="col-sm-6">
                                <textarea class="form-control" id="hidden_solution" name="hidden_solution" style=" width: 100%;    height: 70%; resize: none;display:none;"> </textarea>
                                <!--   <input type="text" class="form-control" id="edit_solution" name="edit_solution" placeholder="Last Name">-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Upload Image</label>
                            <div class="col-lg-5">
                                <div class=" fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new" id="span-button"> Select file </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" id="fileUpload_image1"> </span>
                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        <input type="hidden" class="form-control" id="edit_img" name="edit_img">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Upload Video</label>
                            <div class="col-lg-5">
                                <div class=" fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new" id="span-button"> Select file </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" id="fileUpload_video1"> </span>
                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        <input type="hidden" class="form-control" id="edit_video" name="edit_video">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue btn-outline btn-md btn-circle" onClick="submit_edit()"><i class="fa fa-check"></i> Submit</button>
                    <button type="button" class="btn red btn-outline btn-md btn-circle" data-dismiss="modal" onClick="close_edit()"><i class="fa fa-times"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal End-->
    <!-- END QUICK SIDEBAR -->
    <?php include 'assets/lib/javascript.php'?>
        <script>
            $('.nav.navbar-nav').find('.open').removeClass('open');
            $('#knowledge_b').addClass('open');
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                set_int();
                //$('.sample_2').DataTable();

                var company_id = "<?php echo $company_id;?>";
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product1",
                    type: 'POST',
                    data: {
                        'company_id': company_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#product').html('');
                        $('#product1').html('');
                        $('#product2').html('');
                        $('#display_product').html('');

                        $('#product').html('<option selected value="">All Product</option>');
                        $('#product1').html('<option selected value="">All Product</option>');
                        $('#product2').html('<option selected disabled>Select Product</option>');
                        //  $('#display_product').html('<option selected disabled>Select Product</option>');    
                        for (i = 0; i < data.length; i++) {
                            $('#product').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
                            $('#product1').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
                            $('#product2').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
                            $('#display_product').append('<option value="' + data[i].product_id + '">' + data[i].product_name + '</option>');
                        }
                    }
                });

                var product_id = $('#product1').val();
                var category_id = $('#category1').val();
                var region = "<?php echo $region;?>";
                var area = "<?php echo $area;?>";
                $('#knowledge').empty();
                $('.datatable1').DataTable().destroy();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_knowledge",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id,
                        'category_id': category_id,
                        'region': region,
                        'area': area
                    },
                    type: 'POST',
                    dataType: "json",
                    success: function(data) {
                        //$('#knowledge').html('');
                        if (data.length < 1) {
                            $('#knowledge').empty();
                            $('.datatable1').DataTable().destroy();
                        } else {
                            for (i = 0; i < data.length; i++) {
                                var product = data[i].product;
                                if (product == '') {
                                    data[i].product_name = "-";
                                    //var product= product.replace(/ /g, ":");
                                }
                                /*else
                                {
                                data[i].product_name="-";
                                }*/
                                var category = data[i].category;
                                if (category == '') {
                                    data[i].cat_name = "-";
                                    //var category= category.replace(/ /g, ":");
                                }
                                /* else
                                 {
                                 data[i].cat_name="-";
                                 }*/
                                if (data[i].image == "" && data[i].video != "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].problem + '</td><td>' + data[i].solution + '</td><td >-</td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td style="width: 100px;"><button class="btn green btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + '" onclick="accept(this.id);"><i class="fa fa-check" aria-hidden="true"></i></button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick="reject(this.id);"><i class="fa fa-times" aria-hidden="true"></i></button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video == "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].problem + '</td><td>' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td>-</td><td style="width: 100px;"><button class="btn green btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + '" onclick="accept(this.id);"><i class="fa fa-check" aria-hidden="true"></i></button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick="reject(this.id);"><i class="fa fa-times" aria-hidden="true"></i></button></td></tr>');
                                }
                                if (data[i].image == "" && data[i].video == "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].problem + '</td><td>' + data[i].solution + '</td><td>-</td><td>-</td><td style="width: 100px;"><button class="btn green btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + '" onclick="accept(this.id);"><i class="fa fa-check" aria-hidden="true"></i></button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick="reject(this.id);"><i class="fa fa-times" aria-hidden="true"></i></button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video != "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td>' + data[i].problem + '</td><td>' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td style="width: 100px;"><button class="btn green btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + '" onclick="accept(this.id);"><i class="fa fa-check" aria-hidden="true"></i></button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick="reject(this.id);"><i class="fa fa-times" aria-hidden="true"></i></button></td></tr>');
                                }
                            }

                        }
                        $('.datatable1').DataTable({"order": []});
                    }
                });

                var product_id = $('#product').val();
                var category_id = $('#category').val();
                var region = "<?php echo $region;?>";
                var area = "<?php echo $area;?>";
                $('#show_knowledge').empty();
                $('.datatable').DataTable().destroy();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/show_knowledge",
                    type: 'POST',
                    cache: false,
                    data: {
                        'company_id': company_id,
                        'product_id': product_id,
                        'category_id': category_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.length < 1) {
                            $('#show_knowledge').empty();
                            $('.datatable').DataTable().destroy();
                        } else {
                            for (i = 0; i < data.length; i++) {
                                var product = data[i].product;
                                if (product == '') {
                                    //var product= product.replace(/ /g, ":");
                                    data[i].product_name = "-";
                                    //alert('-');
                                } else {
                                    var prodname = data[i].product_name;
                                    //alert(prodname);
                                }
                                var category = data[i].category;
                                if (category == '') {
                                    data[i].cat_name = "-";
                                    //var category= category.replace(/ /g, ":");
                                }
                                /* else
                                 {
                                 data[i].cat_name="-";
                                 }*/
                                var replaceSpace = data[i].problem;
                                var problem = replaceSpace.replace(/ /g, ":");
                                var replaceSpace1 = data[i].solution;
                                var solution = replaceSpace1.replace(/ /g, ":");

                                if (data[i].image == "" && data[i].video != "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  >-</td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");><i class="fa fa-edit"></i></button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");><i class="fa fa-trash"></i></button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video == "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td>-</td><td><button class="btn blue btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");><i class="fa fa-edit"></i></button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");><i class="fa fa-trash"></i></button></td></tr>');
                                }
                                if (data[i].image == "" && data[i].video == "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td>-</td><td >-</td><td><button class="btn blue btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");><i class="fa fa-edit"></i></button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");><i class="fa fa-trash"></i></button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video != "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-icon-only btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");><i class="fa fa-edit"></i></button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-icon-only btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");><i class="fa fa-trash"></i></button></td></tr>');
                                }
                            }
                        }
                        $('.datatable').DataTable({"order": []});
                    },
                    error: function(error) {
                        alert("Network error");
                    }
                });

            });

            function reset() {
                $('#fileUpload_image').html('');
                $('#fileUpload_video').html('');

            }

            function close_edit() {
                //$('#fileUpload_image1').html('');
                $('#fileUpload_video1').clear('');
                $("#edit_technician")[0].reset();
                //  $('#edit_technician').trigger("reset");
                // var $el = $('#fileUpload_video1');
                //$el.wrap('<form>').closest('form').get(0).reset();
                //$el.unwrap();
            }

            function check() {

                if ($('#generic').prop("checked")) {
                    $("#product2_div").css({
                        "display": "none"
                    });
                    $("#cat2_div").css({
                        "display": "none"
                    });
                    $('#product2').val('');
                    $('#category2').val('');

                } else {
                    $("#product2_div").css({
                        "display": "block"
                    });
                     $("#product2").val($("#product2 option:first").val());
                }
            }

            function get_cat(product_id) {
                var company_id = "<?php echo $company_id;?>";
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_cat",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#category').html('');

                        $('#category').html('<option selected value="" >All Sub-Category</option>');
                        for (i = 0; i < data.length; i++) {
                            $('#category').append('<option value="' + data[i].cat_id + '">' + data[i].cat_name + '</option>');
                        }
                    }
                });
            }

            function get_cat1(product_id) {
                var company_id = "<?php echo $company_id;?>";
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_cat",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#category1').html('');
                        $('#category1').html('<option selected value="">All Sub-Category</option>');
                        for (i = 0; i < data.length; i++) {
                            $('#category1').append('<option value="' + data[i].cat_id + '">' + data[i].cat_name + '</option>');
                        }
                    }
                });
            }

            function get_cat2(product_id) {
                var company_id = "<?php echo $company_id;?>";
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_cat",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#category2').html('');

                        $('#category2').html('<option selected disabled value="">Select Sub-Category</option>');
                        for (i = 0; i < data.length; i++) {
                            $('#category2').append('<option value="' + data[i].cat_id + '">' + data[i].cat_name + '</option>');
                        }
                        $("#cat2_div").css({
                            "display": "block"
                        });
                    }
                });
            }

            function display_cat(product_id) {
                var company_id = "<?php echo $company_id;?>";
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_cat",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#display_category').html('');

                        //('#display_category').html('<option selected disabled>Select Sub-Category</option>'); 
                        for (i = 0; i < data.length; i++) {
                            $('#display_category').append('<option value="' + data[i].cat_id + '">' + data[i].cat_name + '</option>');
                        }
                    }
                });
            }

            function stop() {
                $("div#myModal").removeClass("show");
                $("#videoContainer").trigger('pause');
                $("#videoContainer").currentTime = 0;
                return false;
            }

            function add_knowledge() {
                var company_id = "<?php echo $company_id;?>";
                var region = "<?php echo $region;?>";
                var area = "<?php echo $area;?>";
                var product_id = $('#product2').val();
                var cat_id = $('#category2').val();
                var problem = $('#problem').val();
                var solution = $('#solution').val();
                var image1 = $('#fileUpload_image').val().toString().split('.').pop().toLowerCase();
                var video1 = $('#fileUpload_video').val().toString().split('.').pop().toLowerCase();
              //  alert($.inArray(video1, ['mp4']));
                if (image1 == "") {
                    var image = "";
                } else {
                    if ($.inArray(image1, ['gif', 'jpg', 'png']) != '-1' || $.inArray(image1, ['gif', 'jpg', 'png']) != -1) {
                        var image = $('#fileUpload_image').prop('files')[0];
                    } else {
                        swal({
                            title: "Image file should be in proper format!",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            cancelButtonText: "No, Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        });
                        return false;
                    }
                }
                if (video1 == "") {
                    var video = "";
                } else {
                    if ($.inArray(video1, ['mp4']) != '-1' || $.inArray(video1, ['mp4']) != -1) {
                        var video = $('#fileUpload_video').prop('files')[0];
                     //   alert(video);

                    } else {
                        swal({
                            title: "Video file should be in proper format!",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            cancelButtonText: "No, Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        });
                        return false;
                    }
                }
                var form_data = new FormData();
                form_data.append('product_id', product_id);
                form_data.append('cat_id', cat_id);
                form_data.append('company_id', company_id);
                form_data.append('region', region);
                form_data.append('area', area);
                form_data.append('problem', problem);
                form_data.append('solution', solution);
                form_data.append('fileUpload_image', image);
                form_data.append('fileUpload_video', video);
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/add_knowledge",
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function(data) {
                        if (data == 'Please provide problem/solution!!!') {
                            swal({
                                title: "Provide Problem & Solution",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                cancelButtonText: "No, Cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            });
                        } else if (data = "Added successfully") {
                            swal({
                                    title: data,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok",
                                    cancelButtonText: "No, Cancel",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                });
                        } else {
                            swal(data);
                        }
                    }
                });
            }

            function day1() {
                var company_id = "<?php echo $company_id;?>";
                var region = "<?php echo $region;?>";
                var area = "<?php echo $area;?>";
                var product_id = $('#product1').val();
                var category_id = $('#category1').val();
                $('#knowledge').empty();
                $('.datatable1').DataTable().destroy();

                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_knowledge",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id,
                        'category_id': category_id,
                        'region': region,
                        'area': area
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#knowledge').html('');
                        if (data.length < 1) {
                            $('#knowledge').empty();
                        } else {
                            for (i = 0; i < data.length; i++) {
                                if (data[i].image == "" && data[i].video != "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td style="  width: 16%;">' + data[i].problem + '</td><td style="  width: 26%;">' + data[i].solution + '</td><td >-</td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-sm btn-circle " id="' + data[i].k_id + '" onclick="accept(this.id);">Approve</button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-sm btn-circle " data-toggle="modal" onclick="reject(this.id);">Deny</button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video == "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td style="  width: 16%;">' + data[i].problem + '</td><td style="  width: 26%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td>-</td><td><button class="btn blue btn-outline btn-sm btn-circle " id="' + data[i].k_id + '" onclick="accept(this.id);">Approve</button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-sm btn-circle " data-toggle="modal" onclick="reject(this.id);">Deny</button></td></tr>');
                                }
                                if (data[i].image == "" && data[i].video == "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td style="  width: 16%;">' + data[i].problem + '</td><td style="  width: 26%;">' + data[i].solution + '</td><td>-</td><td>-</td><td><button class="btn blue btn-outline btn-sm btn-circle " id="' + data[i].k_id + '" onclick="accept(this.id);">Approve</button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-sm btn-circle " data-toggle="modal" onclick="reject(this.id);">Deny</button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video != "") {
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td style="  width: 16%;">' + data[i].problem + '</td><td style="  width: 26%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-sm btn-circle " id="' + data[i].k_id + '" onclick="accept(this.id);">Approve</button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-sm btn-circle " data-toggle="modal" onclick="reject(this.id);">Deny</button></td></tr>');
                                    $('#knowledge').append('<tr><td>' + data[i].product_name + '</td><td>' + data[i].cat_name + '</td><td style="  width: 16%;">' + data[i].problem + '</td><td style="  width: 26%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-sm btn-circle " id="' + data[i].k_id + '" onclick="accept(this.id);">Approve</button> <button id="' + data[i].k_id + '" class="btn red btn-outline btn-sm btn-circle " data-toggle="modal" onclick="reject(this.id);">Deny</button></td></tr>');
                                }
                            }
                        }
                        $('.datatable1').DataTable({"order": []});
                    }
                });
            }

            function day() {
                var company_id = "<?php echo $company_id;?>";
                var region = "<?php echo $region;?>";
                var area = "<?php echo $area;?>";
                var product_id = $('#product').val();
                var category_id = $('#category').val();
                $('#show_knowledge').empty();
                $('.datatable').DataTable().destroy();
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/show_knowledge",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id,
                        'category_id': category_id,
                        'region': region,
                        'area': area
                    },
                    cache: false,
                    dataType: "json",
                    success: function(data) {
                        $('#show_knowledge').html('');
                        //alert(data);
                        if (data.length < 1) {
                            $('#show_knowledge').empty();
                            $('.datatable').DataTable().destroy();
                        } else {
                            for (i = 0; i < data.length; i++) {
                                var product = data[i].product;
                                if (product == '') {
                                    // var product= product.replace(/ /g, ":");
                                    //}
                                    // else
                                    // {
                                    data[i].product_name = "-";
                                }
                                var category = data[i].category;
                                if (category == '') {
                                    // var category= category.replace(/ /g, ":");
                                    //}
                                    //else
                                    //{
                                    data[i].cat_name = "-";
                                }

                                var replaceSpace = data[i].problem;
                                var problem = replaceSpace.replace(/ /g, ":");
                                var replaceSpace1 = data[i].solution;
                                var solution = replaceSpace1.replace(/ /g, ":");
                                if (data[i].image == "" && data[i].video != "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  >-</td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-sm btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");>Edit</button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-sm btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");>Delete</button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video == "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td>-</td><td><button class="btn blue btn-outline btn-sm btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");>Edit</button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-sm btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");>Delete</button></td></tr>');
                                }
                                if (data[i].image == "" && data[i].video == "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td>-</td><td >-</td><td><button class="btn blue btn-outline btn-sm btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");>Edit</button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-sm btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");>Delete</button></td></tr>');
                                }
                                if (data[i].image != "" && data[i].video != "") {
                                    $('#show_knowledge').append('<tr><td id="' + data[i].product + i + '">' + data[i].product_name + '</td><td id="' + data[i].category + i + '">' + data[i].cat_name + '</td><td id="' + data[i].k_id + i + i + '" style="  width: 11%;">' + data[i].problem + '</td><td id="' + data[i].k_id + i + i + i + '" style="  width: 13%;">' + data[i].solution + '</td><td  onclick=start_image("' + data[i].image + '");><i class="fa fa-camera"></i></td><td  onclick=start_video("' + data[i].video + '");><i class="fa fa-video-camera"></i></td><td><button class="btn blue btn-outline btn-sm btn-circle" id="' + data[i].k_id + i + '" onclick=edit("' + data[i].k_id + '","' + product + '","' + data[i].product_id + '","' + category + '","' + problem + '","' + solution + '","' + data[i].image + '","' + data[i].video + '");>Edit</button> <button id="' + data[i].k_id + i + '" class="btn red btn-outline btn-sm btn-circle" data-toggle="modal" onclick=delet("' + data[i].k_id + '","' + i + '");>Delete</button></td></tr>');
                                }
                            }

                        }
                        $('.datatable').DataTable({"order": []});
                    }
                });
            }

            function start_image(image) {
                $('#modal_display').html('<picture ><source media="(width: 400)" srcset="' + image + '">  <img src="' + image + '"  width="550" height="400" class="img-responsive know-img"></picture>');
                $('#myModal').modal('show');
            }

            function start_video(video) {
                $('#modal_display').html('<video controls id="videoContainer"  width="100%" height="250"><source src="' + video + '" type="video/mp4"><source src="mov_bbb.ogg" type="video/ogg"></video>');
                $('#myModal').modal('show');
            }

            function accept(id) {
                swal({
                        title: "Are you sure? You want to accept it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Accept it!",
                        closeOnConfirm: false
                    },
                    function() {
                        $.ajax({
                            url: "<?php echo base_url();?>" + "index.php?/controller_service/accept_knowledge",
                            type: 'POST',
                            data: {
                                'id': id,
                                'status': 1
                            },
                            success: function(data) {
                                if (data = "Knowledge Request accepted") {
                                    swal({
                                            title: data,
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            cancelButtonText: "No,Cancel",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                window.location.reload();
                                                window.location.href = "<?php echo base_url();?>" + "index.php?/controller_service/knowledge_base#assigned";
                                            }
                                        });
                                } else {
                                    swal(data);
                                }
                            }
                        });
                    });
            }

            function reject(id) {
                swal({
                        title: "Are you sure? You want to Reject it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Reject it!",
                        closeOnConfirm: false
                    },
                    function() {
                        $.ajax({
                            url: "<?php echo base_url();?>" + "index.php?/controller_service/accept_knowledge",
                            type: 'POST',
                            data: {
                                'id': id,
                                'status': 2
                            },
                            success: function(data) {
                                swal("Knowledge Suggestion Rejected");
                                 setTimeout(function(){  window.location.reload(); }, 2000);
                            }
                        });
                    });
            }

            function edit(id, product, product_id, category, problem, solution, image, video)

            {
                var id = id;
                var company_id = "<?php echo $company_id;?>";
                var replaceSpace = problem;
                var problem1 = problem.replace(/\:/g, " ");
                var replaceSpace1 = solution;
                var solution1 = solution.replace(/\:/g, " ");
                if (product == "") {
                    var product = "";
                    var category = "";
                    var product_id = "";
                    $('#display_product').val("");
                    $('#dis_prod').css({
                        "display": "none"
                    });
                    $('#dis_cat').css({
                        "display": "none"
                    });
                } else {
                    $('#dis_prod').css({
                        "display": "block"
                    });
                    $('#dis_cat').css({
                        "display": "block"
                    });
                }
                var product = product;
                var product = product.replace(/\:/g, " ");
                var category = category;
                var category = category.replace(/\:/g, " ");
                $('#edit_id').val(id);
                $.ajax({
                    url: "<?php echo base_url();?>" + "index.php?/controller_service/load_cat",
                    type: 'POST',
                    data: {
                        'company_id': company_id,
                        'product_id': product_id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#display_category').html('');

                        //('#display_category').html('<option selected disabled>Select Sub-Category</option>'); 
                        for (i = 0; i < data.length; i++) {
                            $('#display_category').append('<option value="' + data[i].cat_id + '">' + data[i].cat_name + '</option>');
                            $('#display_category').val(category);
                        }
                    }
                });
                $('#display_product').val(product_id);
                $('#edit_problem').val(problem1);
                $('#edit_solution').val(solution1);
                $('#hidden_prod').val(product_id);
                $('#hidden_cat').val(category);
                $('#hidden_problem').val(problem1);
                $('#hidden_solution').val(solution1);
                $('#edit_img').val(image);
                $('#edit_video').val(video);
                $('#edits').modal('show');

            }

            function submit_edit() {
                var company_id = "<?php echo $company_id;?>";
                var id = $('#edit_id').val();
                var product = $('#display_product').val();
                var category = $('#display_category').val();
                var problem = $('#edit_problem').val();
                var solution = $('#edit_solution').val();
                var hidden_img = $('#edit_img').val();
                var hidden_video = $('#edit_video').val();
                var sam = $('#fileUpload_image1').val();
                var image1 = $('#fileUpload_image1').val().toString().split('.').pop().toLowerCase();
                var video1 = $('#fileUpload_video1').val().toString().split('.').pop().toLowerCase();
                var image = "";
                var video = "";
                if(image1 != "") {
                    $('#edits').modal('hide');
                    swal({
                            title: "Do you want to replace previous Image?",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            cancelButtonText: "No,Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                if ($.inArray(image1, ['gif', 'jpg', 'png']) != '-1') {
                                    image = $('#fileUpload_image1').prop('files')[0];

                                    if (video1 == "") {
                                        video = "";
                                           final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                    } else {
                                        $('#edits').modal('hide');
                                        swal({
                                                title: "Do you want to replace previous Video?",
                                                showCancelButton: true,
                                                confirmButtonClass: "btn-danger",
                                                confirmButtonText: "Ok",
                                                cancelButtonText: "No,Cancel",
                                                closeOnConfirm: false,
                                                //  closeOnCancel: false
                                            },
                                            function(isConfirm) {
                                                if (isConfirm) {
                                                    if ($.inArray(video1, ['mp4']) != '-1') {
                                                        video = $('#fileUpload_video1').prop('files')[0];
                                                            final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                                    } else {
                                                        $('#edits').modal('hide');
                                                        // alert('hiiii');
                                                        swal({
                                                                title: "Video file should be in proper format!",
                                                                showCancelButton: true,
                                                                confirmButtonClass: "btn-danger",
                                                                confirmButtonText: "Ok",
                                                                // closeOnConfirm: false,
                                                                // closeOnCancel: false
                                                            },
                                                            function(isConfirm) {
                                                                $('#edits').modal('show');
                                                            });

                                                        //return false;
                                                    }

                                                } else {
                                                      //   final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                                }
                                            });
                                    }
                                } else {
                                    swal({
                                            title: "Image file should be proper format!",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                $('#edits').modal('show');
                                            }
                                        });
                                }
                            } else {

                            }

                        });

                }
                if(image1 == ""){
                    image="";
                 if (video1 == "") {
                    video = "";
                       final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                } 
                else {
                    $('#edits').modal('hide');
                    swal({
                            title: "Do you want to replace previous Video?",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            cancelButtonText: "No,Cancel",
                            closeOnConfirm: false,
                              closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                if ($.inArray(video1, ['mp4']) != '-1') {
                                    video = $('#fileUpload_video1').prop('files')[0];
                    
                                    if (image1 == "") {
                                        image = "";
final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                    } else {
                                        $('#edits').modal('hide');
                                        swal({
                                                title: "Do you want to replace previous Image?",
                                                showCancelButton: true,
                                                confirmButtonClass: "btn-danger",
                                                confirmButtonText: "Ok",
                                                cancelButtonText: "No,Cancel",
                                                closeOnConfirm: false,
                                                closeOnCancel: false
                                            },
                                            function(isConfirm) {
                                                if (isConfirm) {
                                                    if ($.inArray(image1, ['gif', 'jpg', 'png']) != '-1') {
                                                        image = $('#fileUpload_image1').prop('files')[0];
final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                                    } else {
                                                        swal({
                                                                title: "Image file should be proper format!",
                                                                showCancelButton: true,
                                                                confirmButtonClass: "btn-danger",
                                                                confirmButtonText: "Ok",
                                                                closeOnConfirm: false,
                                                                closeOnCancel: false
                                                            },
                                                            function(isConfirm) {
                                                                if (isConfirm) {
                                                                    $('#edits').modal('show');
                                                                }
                                                            });
                                                    }
                                                } else {

                                                }

                                            });

                                    }
                                      //  final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                                } else {
                                    $('#edits').modal('hide');
                                    // alert('hiiii');
                                    swal({
                                            title: "Video file should be in proper format!",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            // closeOnConfirm: false,
                                            // closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                            $('#edits').modal('show');
                                        });

                                    //return false;
                                }

                            } else {
                             //        final_edit(id,product,category,problem,solution,company_id,image,video,hidden_img,hidden_video);
                            }
                        });
                }

                }
            }

            function delet(id) {
                var company_id = "<?php echo $company_id;?>";
                swal({
                        title: "Are you sure? You want to delete it",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        closeOnConfirm: false
                    },
                    function() {
                        $.ajax({
                            url: "<?php echo base_url();?>" + "index.php?/controller_service/delete_knowledge",
                            type: 'POST',
                            data: {
                                'id': id,
                                'company_id': company_id
                            },
                            success: function(data) {
                                swal({
                                        title: data,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        cancelButtonText: "No,Cancel",
                                        closeOnConfirm: false,
                                        closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.reload();
                                        }
                                    });
                            }
                        });
                    });
            }

            function final_edit(id, product, category, problem, solution, company_id, image, video, hidden_img, hidden_video) {
                if (image == "undefined") {
                    image = "";
                }
                if (video == "undefined") {
                    video = "";
                }
                var hp = $('#hidden_prod').val();
                var hc = $('#hidden_cat').val();
                var hprobs = $('#hidden_problem').val();
                var hs = $('#hidden_solution').val();
                if (hp == product && hc == category && hprobs == problem && hs == solution && image == "" && video == "") {
                    $("#edits").modal('hide');
                    swal({
                            title: "No Changes done!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Edit",
                            cancelButtonText: "Close",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                $('#edits').modal('show');
                                swal.close();
                            } else {
                                //alert("$().modal();");
                                swal.close();
                            }
                        });
                } else {
                    var form_data = new FormData();
                    form_data.append('id', id);
                    form_data.append('product', product);
                    form_data.append('category', category);
                    form_data.append('company_id', company_id);
                    form_data.append('problem', problem);
                    form_data.append('solution', solution);
                    form_data.append('fileUpload_image1', image);
                    form_data.append('fileUpload_video1', video);
                    form_data.append('hidden_img', hidden_img);
                    form_data.append('hidden_video', hidden_video);
                    $.ajax({
                        url: "<?php echo base_url();?>" + "index.php?/controller_service/edit_knowledge",
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $('#edits').modal('hide');
                            swal({
                                    title: data,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok",
                                    cancelButtonText: "No,Cancel",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                });
                        }
                    });
                }
            }
        </script>
</body>

</html>