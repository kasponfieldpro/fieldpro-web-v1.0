<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include 'assets/lib/cssscript.php'?>
          <style>
                .jstree-anchor{
                    min-height:110px !important;
                }
                .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
                    height:100px;
                    width:150px;
                }
                .jstree-default .jstree-anchor {
                    //line-height: 6 !important;
                    margin-bottom:6%;
                    text-align: center;
                }
                .jstree-icon.jstree-themeicon.jstree-themeicon-custom {
                    margin: 8px 0px !important;
                }
                .jstree-node{
                    margin:3px 0px;
                }
                #tree_5{
                    padding:2%;
                }
                .jstree-children{
                    margin:0% 2%;
                }
                .jstree-icon.jstree-themeicon.jstree-themeicon-custom{
                    display:block !important;
                }
                .fileinput-new,.fileinput-exists{
                    color:#000 !important;
                }
                .sweet-alert.showSweetAlert.visible{
                    z-index: 999999999 !important;
                    border: 1px solid red;
                }

          button.accordion2 {
              background-color: #eee;
              color: #444;
              cursor: pointer;
              padding: 18px;
              width: 100%;
              border: none;
              text-align: left;
              outline: none;
              font-size: 15px;
              transition: 0.4s;
              min-height: 140px !important;
              margin-bottom:1%;
          }

          button.accordion2.active, button.accordion2:hover {
              background-color: #ddd;
          }

          button.accordion2:after {
              content: '\002B';
              color: #777;
              font-size: 30px;
              float: right;
              margin-left: 5px;
              padding: 20px 0;
          }

          button.accordion2.active:after {
              content: "\2212";
          }

          div.panel2 {
              padding: 0 18px;
              background-color: white;
              max-height: 0;
              overflow: hidden;
              transition: max-height 0.2s ease-out;
              margin: 10px 0;
          }
          .sub-products img
          {
            border:1px solid #3A6C86;
            width: 100px;
            height: 100px;
			object-fit: contain;
          //border-radius: 80px !important;
          }
          .accordion2 img {
              //border-radius: 80px !important;
              width: 100px;
              height: 100px;
			  object-fit: contain;
          }
          .pdt-content {
              width: 65%;
              margin-top: -108px;
          }
          .pdt-title {
              font-size: 18px;
          }
          button.accordion2::after {
            margin-top: -85px;
          }
          .sub-content {
              padding: 3px 0;
          }
          .prroducts{
            //margin-bottom:1% !important;
          }
          .pen-icon2 {
              float: right;
              margin: -44px 0 0 0 !important;
              border: 1px solid #dddddd;
              padding: 8px;
              border-radius: 100px !important;
              background-color: #fff;
          }
          .pen-icon2 a .icon-pencil{
            color:#000 !important;
          }
          .side_animate{
            position: relative;
            float: right;
            margin-right: -13%;
            margin-top: 1%;
            padding: 1%;
            border-radius:100px !important;
            background: #fff;
            border: 1px solid #eee;
            opacity:0;
            transition: opacity 1s ease-in-out;
            -moz-transition: opacity 1s ease-in-out;
            -webkit-transition: opacity 1s ease-in-out;
          }
          .side_animate .first_fa{
            padding-right: 10px;

          }
          .pen-icon {
            float: right;
            margin: 0px -14px -39px 0 !important;
            position: relative;
            background: #fff;
            padding: 24px 10px 10px 18px;
            border-radius: 100% !important;
            border: 1px solid #eee;
            height: 70px;
            width: 70px;

              /*-webkit-transition-duration: 2s;
              -moz-transition-duration: 2s;
              -o-transition-duration: 2s;
              transition-duration: 2s;
              -webkit-transition-property: -webkit-transform;
              -moz-transition-property: -moz-transform;
              -o-transition-property: -o-transform;
              transition-property: transform;
              -webkit-transform:rotate(-180deg);
              -moz-transform:rotate(-180deg);
              -o-transform:rotate(-180deg);*/
          }
          .first_fa {
            margin-right:25%;
          }
          /*.pen-icon:hover{
            -webkit-transition-duration: 2s;
            -moz-transition-duration: 2s;
            -o-transition-duration: 2s;
            transition-duration: 2s;
            -webkit-transition-property: -webkit-transform;
            -moz-transition-property: -moz-transform;
            -o-transition-property: -o-transform;
            transition-property: transform;
            -webkit-transform:rotate(180deg);
            -moz-transform:rotate(180deg);
            -o-transform:rotate(180deg);
          }*/
          .pen-icon:hover + .side_animate{
            opacity: 1.0;
            transition: opacity 1s ease-in-out;
            -moz-transition: opacity 1s ease-in-out;
            -webkit-transition: opacity 1s ease-in-out;
          }
          </style>
    </head>
    <!-- END HEAD -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header.php"?>
            <!-- END HEADER -->
            <div class="page-container">
            <div class="page-sidebar-wrapper">
            <?php include "assets/lib/admin_sidebar.php"?>
            </div>
            <div class="page-content-wrapper">
                  <div class="page-content">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box dark">
                                    <div class="portlet-title">
                                        <div class="caption">Product Management</div>
                                        <div class="actions"> <a onclick='add_prod()' class='btn btn-circle green btn-sm btn-outline pull-right'><i class='fa fa-plus'></i> Add Product Category</a></div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="container-fluid" id="div_align">
                                                    <div class="portlet-body" id='product_append'>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
               <!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
                 </div>

        <!--Modal Starts-->
        <!-- Modal -->
        <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Product Category</h4>
                        <div class="error" style="display:none">
                            <label id="rowdata"></label>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="add_product">

                          <div class="form-group" style="display:none">
                              <label class="control-label col-md-3">Company Name
                                <span class="required" aria-required="true"> * </span>
                              </label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                              </div>
                          </div>
                          <div class="form-group" style="display:none">
                              <label class="col-md-3 control-label">Product_id</label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" id='product_id' name='product_id'>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Name<span class="required" aria-required="true"> * </span></label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" id='product_name' name='product_name' placeholder="Enter product name">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Model<span class="required" aria-required="true"> * </span></label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" id='product_modal' name='product_modal' placeholder="Enter product Model">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Description<span class="required" aria-required="true"> * </span></label>
                              <div class="col-md-9">
                                  <textarea class="form-control" rows="3" id='product_desc' name='product_desc' maxlength="140" style='resize:none'></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-3">Upload Image<span class="required" aria-required="true"> * </span></label>
                              <div class="col-md-9">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                      <div>
                                          <span class="btn btn-default btn-file">
                                              <span class="fileinput-new"> Select image </span>
                                              <span class="fileinput-exists"> Change </span>
                                              <input type="file" name="product_image" id='product_image'> </span>
                                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-circle green btn-outline" id="addproduct"><i class="fa fa-check"></i> Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- Modal -->
         <!-- Modal -->
         <div id="myModal2" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Sub Category</h4>
                            <div class="error" style="display:none">
                                <label id="rowdata_category"></label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="add_subproduct">
                                <div class="form-group" style='display:none'>
                                    <label class="control-label col-sm-4" for="email">Sub Category Id</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="subproduct_id" name="subproduct_id" readonly>
                                    </div>
                                </div>
                                <div class="form-group" style='display:none'>
                                    <label class="control-label col-sm-4" for="email">Product Category</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="product_names" name="product_names" readonly>
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="control-label col-sm-4" for="email">Product Category Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="product_value" name="product_value" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Sub Category Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="subproduct_name" name="subproduct_name" placeholder="Sub Category Name">
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Sub Category Modal No</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="subproduct_modal" name="subproduct_modal" placeholder="Sub Category Modal No">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Sub Category Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="5" id="subproduct_desc" name="subproduct_desc" style="resize:none"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">company id</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="c_id" name="c_id" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-md-4">Upload Image</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="subproduct_image" id="subproduct_image"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-circle green btn-outline" id="addsubproduct"><i class="fa fa-check"></i> Submit</button>
                            <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
        </div>
        <div id="edits" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update Product Category</h4>
                                    <div class="error" style="display:none">
                                        <label id="rowdata_1"></label>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" id="edit_product">
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-3" for="email">Id:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="id1" name="id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group" style='display:none'>
                                            <label class="control-label col-sm-3" for="email">Product Category Id:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_id1" name="product_id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_name1" name="product_name1" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Modal:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_modal1" name="product_modal1" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Description:</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="5" id="product_desc1" name="product_desc1" style="resize:none"></textarea>
                                            </div>
                                        </div>


                                           <div class="form-group">
                                            <label class="control-label col-md-3">Upload Image</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                                    <img src="" id="pro_edit_image" alt="Update Image" />
                                                    </div>
                                                    <div>
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="prd_image" id='prd_image'> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" id="product_image2" name="product_image2" style="display:none" />
                                            </div>
                                        </div>



                                        <!--<div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Product Category Image:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="product_image2" name="product_image2" style="display:none" />
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                            <span class="fileinput-filename"> </span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new"> Select file </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="product_image1" id="product_image1"> </span>
                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                               <!--  <input type="file" class="form-control" id="product_image1" name="product_image1" />
                                            </div>
                                        </div>-->
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-4" for="email">Company ID:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="companyid1" name="companyid1" placeholder="">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-circle green btn-outline" onClick="submit_product()"><i class="fa fa-check"></i> Submit</button>
                                    <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

        <div id="edits_category" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update Sub Category</h4>
                                    <div class="error" style="display:none">
                                        <label id="rowdata_category_1"></label>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" id="edit_product">
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-4" for="email">Id:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="id1" name="id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group" style='display:none'>
                                            <label class="control-label col-sm-3" for="email">Sub Category Id:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="subproduct_id1" name="subproduct_id1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Product Category Name:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="product_name_1" name="product_name_1">
                                                    <?php
                                                            foreach ($record->result() as $row) {
                                                                ?>
                                                        <option value="<?php echo $row->product_id; ?>">
                                                            <?php echo $row->product_name; ?>
                                                        </option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Sub Category Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="subproduct_name1" name="subproduct_name1" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label class="control-label col-sm-4" for="email">Sub Category Modal No:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="subproduct_modal1" name="subproduct_modal1" placeholder="Sub Category Modal No">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="email">Sub Category Description:</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="5" id="subproduct_desc1" name="subproduct_desc1" style="resize:none"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-md-3">Upload Image</label>
                                            <div class="col-md-9">
                                              <input type="text" class="form-control" id="subproduct_image2" name="subproduct_image2" style="display:none" />
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                                    <img src="" id="sub_pro_edit_image" alt="Update Image" />
                                                    </div>
                                                    <div>
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="subproduct_image1" id="subproduct_image1"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-circle green btn-outline" onClick="submit_subproduct()"><i class="fa fa-check"></i> Submit</button>
                                    <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
         <!-- Modal -->

         <!-- Modal -->

        <!--Modal End-->
         <!--loading model-->
         <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->

        <!-- END QUICK SIDEBAR -->
        <?php include 'assets/lib/javascript.php'?>

        <script>
            $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#product_management').addClass('open');
        </script>

    <script>

    $(document).ready(function(){
      var company_id="<?php echo $this->session->userdata('companyid');?>";
      $.ajax({
       url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_product_company1",
       type        :   "POST",
       data        :   {'company_id':company_id},// {action:'$funky'}
       datatype    :   "JSON",
       cache       :   false,
       success     :   function(data){
                        var data=JSON.parse(data);
                        //console.log(data);
                        /* $('#product_append').append("<a onclick='add_prod()' class='btn btn-circle green btn-sm pull-right'><i class='fa fa-plus'></i> Add Product Category</a><span class='clearfix'></span><br>"); */
                        for(i=0;i<data.length;i++){
                          $('#product_append').append("<p class='pen-icon'><a id='"+data[i].product_id+"' onclick='edit_prod(this.id)'><span class='first_fa'><i class='icon-pencil'></i></span></a><a id='"+data[i].product_id+"' onclick='deletes(this.id)'><span class=''><i class='fa fa-trash'></i></span></a></p><button class='accordion2' id='"+data[i].product_id+"'><img src='"+data[i].product_image+"' width='140'><div class='pdt-content center-block'><p class='pdt-title'>"+data[i].product_name+"</p><p>"+data[i].product_desc+"</p></div></button><div class='panel2 sub-products "+data[i].product_id+"'><a  id='"+data[i].product_id+"' onclick='add_sub(this.id)' class='btn btn-circle green btn-sm pull-right'><i class='fa fa-plus'></i> Add Sub Category</a><span class='clearfix'></span><br></div>");
                          
                          $.ajax({
                              url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_subproduct_company1",
                              type        :   "POST",
                              data        :   {'prod_id':data[i].product_id},// {action:'$funky'}
                              datatype    :   "JSON",
                              success     :   function(data){
                                                  var data=JSON.parse(data);
                                                    for(j=0;j<data.length;j++){
                                                      $('.'+data[j].prod_id).append("<p class='pen-icon'><a id='"+data[j].cat_id+"' onclick='edit_sub(this.id)'><span class='first_fa'><i class='icon-pencil'></i></span></a><a id='"+data[j].cat_id+"' onclick='deletes_category(this.id)'><span class=''><i class='fa fa-trash'></i></span></a></p><div class='col-sm-3'><img src='"+data[j].cat_image+"' ></div><div class='col-sm-9 sub-content'><p class='pdt-title'>"+data[j].cat_name+"</p>"+data[j].cat_desc+"</div><span class='clearfix'></span><br>");
                                                    }
                                              }
                          });

                          var acc = document.getElementsByClassName("accordion2");
                          var i;

                          for (j = 0; j < acc.length; j++) {
                            acc[j].onclick = function() {
                              this.classList.toggle("active");
                              var panel = this.nextElementSibling;
                              if (panel.style.maxHeight){
                                panel.style.maxHeight = null;
                              } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                              }
                            }
                          }

                          $(".pen-icon").hover(function () {
                            $(".design-nav").animate({
                                opacity: "1"
                            }, {
                                queue: false
                            });
                        }, function () {
                            $(".design-nav").animate({
                                opacity: "0"
                            }, {
                                queue: false
                            });
                        });

                        }
                      },
      });
    })

    </script>

        <script>
            function add_prod(){
            $("#add_product")[0].reset();
            $("#rowdata").empty();
            $('#company').val(parent);
            $.ajax({
            url         :   "<?php echo base_url();?>index.php?/controller_admin/productid_check",
            type        :   "POST",
            data        :   "",
            cache       :   false,
            success     :   function(data){
                                $('#product_id').val($.trim(data));
                                $('#myModal1').modal('show');
                            },
        })
    }
        $('#addproduct').click(function() {
            $('#rowdata').empty();
            var product_id = $("#product_id").val();
            var product_name = $("#product_name").val();
            var product_modal = $("#product_modal").val();
            var product_desc = $("#product_desc").val();
            var company = $("#c_id").val();
            var product_image = $('#product_image').val().toString().split('.').pop().toLowerCase();
            if ($.inArray(product_image, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                var product_image = $('#product_image').prop('files')[0];
                var form_data = new FormData();
                form_data.append('product_id', product_id);
                form_data.append('product_name', product_name);
                form_data.append('company', company);
                form_data.append('product_modal', product_modal);
                form_data.append('product_desc', product_desc);
                form_data.append('product_image', product_image);
                $('#Searching_Modal').modal('show');
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/insertproduct",
                    type: "POST",
                    data: form_data, // {action:'$funky'}
                    //datatype  :   "JSON",
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $('#Searching_Modal').modal('hide');
                        data = $.trim(data);
                        if (data == "product added Successfully") {
                            $('#myModal1').modal('hide');
                            swal({
                                  title: "Product added Successfully",
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                        } else {
                            $('#rowdata').append(data);
                            $('#myModal1').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
                $('#Searching_Modal').modal('hide');
                $('#rowdata').append('Product Image should be an Image file');
                $('#edits').animate({ scrollTop: 0 });
                $('.error').show();
                //swal('Product Image should be an Image file');
            }
        });
        function edit_prod(id){
             $("#edit_product")[0].reset();
             $('#Searching_Modal').modal('show');
            $.ajax({
                  url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_product",
                  type: "POST",
                  data: {
                      'id': id
                  }, // {action:'$funky'}
                  datatype: "JSON",
                  cache: false,
                  success: function(data) {
                    $('#Searching_Modal').modal('hide');
                      var data = JSON.parse(data);
                      //console.log(data);
                      //$('#product_image1').val(data['product_image']);
                      $('#id1').val(data['id']);
                      $('#product_id1').val(data['product_id']);
                      $('#product_name1').val(data['product_name']);
                      $('#product_modal1').val(data['product_modal']);
                      $('#product_desc1').val(data['product_desc']);
                      $('#product_image2').val(data['product_image']);
                      $('#companyid1').val(data['company_id']);
                      $('#pro_edit_image').attr("src",data['product_image']);
                      $('#edits').modal('show');
                  },
              });
        }
        function submit_product() {
            $('#rowdata_1').empty();
            var id1 = $("#id1").val();
            var product_id1 = $("#product_id1").val();
            var product_name1 = $("#product_name1").val();
            var companyid1 = $("#companyid1").val();
            var product_modal1 = $("#product_modal1").val();
            var product_desc1 = $("#product_desc1").val();
            var product_image2 = $("#product_image2").val();
            var product_image1 = $('#prd_image').val().toString().split('.').pop().toLowerCase();
            if (product_image1 == "") {
                product_image1 = product_image2;
                $('#Searching_Modal').modal('show');
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_product1",
                    type: "POST",
                    data: {
                        'id1': id1,
                        'product_id1': product_id1,
                        'product_name1': product_name1,
                        'product_modal1': product_modal1,
                        'product_desc1': product_desc1,
                        'companyid1': companyid1,
                        'product_image1': product_image1
                    }, // {action:'$funky'}
                    //datatype  :   "JSON",
                    success: function(data) {
                        $('#Searching_Modal').modal('hide');
                        data = $.trim(data);
                        if (data == "Product updated Successfully") {
                            $('#edits').modal('hide');
                            swal({
                                  title: "Product updated Successfully",
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                        } else {
                            $('#rowdata_1').append(data);
                            $('#edits').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
                if ($.inArray(product_image1, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                    var product_image1 = $('#prd_image').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('id1', id1);
                    form_data.append('product_id1', product_id1);
                    form_data.append('product_name1', product_name1);
                    form_data.append('product_modal1', product_modal1);
                    form_data.append('product_desc1', product_desc1);
                    form_data.append('companyid1', companyid1);
                    form_data.append('product_image1', product_image1);
                    $('#Searching_Modal').modal('show');
                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_product",
                        type: "POST",
                        data: form_data, // {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $('#Searching_Modal').modal('show');
                            data = $.trim(data);
                            if (data == "Product updated Successfully") {
                                $('#edits').modal('hide');
                                swal({
                                      title: "Product updated Successfully",
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                            } else {
                                $('#Searching_Modal').modal('hide');
                                $('#rowdata_1').append(data);
                                $('#edits').animate({ scrollTop: 0 });
                                $('.error').show();
                            }
                        },
                    });
                } else {
                    swal('product Category Image should be an Image file');
                    //bootbox.alert('Product image should be a image file');
                }
            }
        }
        function deletes(id) {
            swal({
                  title: "Are you sure to Delete?",
                  text: "You will not be able to recover this Product Category!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Delete It!",
                  cancelButtonText: "No, Keep It!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: "<?php echo base_url(); ?>index.php?/controller_admin/deleteproduct",
                          type: "POST",
                          data: {
                              'id': id
                          }, // {action:'$funky'}
                          //datatype  :   "JSON",
                          cache: false,
                          success: function(data) {
                              if (data == 1) {
                                  swal({
                                      title: "Product Category Deleted Successfully",
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                              } else {
                                  swal({
                                      title: "Product Category Not Deleted Successfully",
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                              }
                          },
                      });
                  } else {
                    swal.close();
                  }
                });
        }
        function add_sub(id){
            $('#rowdata_category').empty();
            $("#add_subproduct")[0].reset();
             $.ajax({
                 url: "<?php echo base_url();?>index.php?/controller_admin/subproductid_check",
                 type: "POST",
                 data: "",
                 cache: false,
                 success: function(data) {
                     $('#subproduct_id').val($.trim(data));
                     $.ajax({
                         url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_product",
                         type: "POST",
                         data: {
                             'id': id
                         }, // {action:'$funky'}
                         datatype: "JSON",
                         cache: false,
                         success: function(data) {
                             var data = JSON.parse(data);
                             //console.log(data);
                             $('#product_names').val(data['product_id']);
                             $('#product_value').val(data['product_name']);
                         },
                     });
                     $('#myModal2').modal('show');
                 },
             });
        }$('#addsubproduct').click(function() {
            $('#rowdata_category').empty();  
            var subproduct_id = $("#subproduct_id").val();
            var subproduct_name = $("#subproduct_name").val();
            var product_name = $("#product_names").val();
            var companyid = $("#c_id").val();
            var subproduct_modal = $("#subproduct_modal").val();
            var subproduct_desc = $("#subproduct_desc").val(); 
            $('#Searching_Modal').modal('show');         
            $.ajax({
                url         : "<?php echo base_url()?>index.php?/controller_admin/checksubcategory",
                type        : "POST",
                datatype    : "JSON",
                data        : {'name':subproduct_name, 'product_name':product_name, 'companyid':companyid},
                success     :  function(data){
                             $('#Searching_Modal').modal('hide');
                                if(data == 1){
                                    $('#rowdata_category').append("This product already had a Sub Product with same name!");
                                    $('#myModal2').animate({ scrollTop: 0 });
                                    $('.error').show();
                                }else{ 
                                    
                                    var subproduct_image = $('#subproduct_image').val().toString().split('.').pop().toLowerCase();
                                      
                                    if ($.inArray(subproduct_image, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                                        var subproduct_image = $('#subproduct_image').prop('files')[0];
                                        var form_data = new FormData();
                                        form_data.append('subproduct_id', subproduct_id);
                                        form_data.append('subproduct_name', subproduct_name);
                                        form_data.append('subproduct_modal', subproduct_modal);
                                        form_data.append('subproduct_desc', subproduct_desc);
                                        form_data.append('product_name', product_name);
                                        form_data.append('subproduct_image', subproduct_image);
                                        form_data.append('companyid', companyid);
                                        $('#Searching_Modal').modal('show');
                                        $.ajax({
                                            url: "<?php echo base_url(); ?>index.php?/controller_admin/insertsubproduct",
                                            type: "POST",
                                            data: form_data, // {action:'$funky'}
                                            //datatype  :   "JSON",
                                            contentType: false,
                                            processData: false,
                                            success: function(data) {
                                                $('#Searching_Modal').modal('hide');
                                                if (data == 1) {
                                                  $('#myModal2').hide();
                                                    swal({
                                                      title: "Sub category added successfully",
                                                      type: "success",
                                                      showCancelButton: false,
                                                      confirmButtonClass: "btn-danger",
                                                      confirmButtonText: "Ok!",
                                                      cancelButtonText: "No, cancel plx!",
                                                      closeOnConfirm: false,
                                                      closeOnCancel: false
                                                    },
                                                    function(isConfirm) {
                                                      if (isConfirm) {
                                                        window.location.reload();
                                                      }
                                                    });
                                                } else {
                                                    $('#rowdata_category').append(data);
                                                    $('#myModal2').animate({ scrollTop: 0 });
                                                    $('.error').show();
                                                }
                                            },
                                        });
                                    } else {
                                        $('#rowdata_category').append('Sub category image should be an image file');
                                        $('#myModal2').animate({ scrollTop: 0 });
                                        $('.error').show();
                                         //swal('Sub category image should be an image file');
                                    }
                                }
                            }
            });
        })
        function edit_sub(id){
            $("#edit_product")[0].reset();
            $.ajax({
                url: "<?php echo base_url(); ?>index.php?/controller_superad/getdetails_subproduct",
                type: "POST",
                data: {'id': id}, // {action:'$funky'}
                datatype: "JSON",
                cache: false,
                success: function(data) {
                    var data = JSON.parse(data);
                    //console.log(data);
                    $('#id1').val(data['id']);
                    $('#subproduct_id1').val(data['cat_id']);
                    $('#subproduct_name1').val(data['cat_name']);
                    $('#subproduct_modal1').val(data['cat_modal']);
                    $('#subproduct_desc1').val(data['cat_desc']);
                    //$('#product_name1').val(data['product_name']);
                    $('#subproduct_image2').val(data['cat_image']);
                    $('#sub_pro_edit_image').attr("src",data['cat_image']);
                    $('select[name="product_name_1"] option[value="' + data['prod_id'] + '"]').attr("selected", true);
                    $('#edits_category').modal('show');
                },
            });
        }
        function submit_subproduct() {
            $('#rowdata_1').empty();
            var id1 = $("#id1").val();
            var subproduct_id1 = $("#subproduct_id1").val();
            var subproduct_name1 = $('#subproduct_name1').val();
            var product_name1 = $("#product_name_1").val();
            var subproduct_modal1 = $("#subproduct_modal1").val();
            var subproduct_desc1 = $("#subproduct_desc1").val();
            var subproduct_image2 = $("#subproduct_image2").val();
            var subproduct_image1 = $('#subproduct_image1').val().toString().split('.').pop().toLowerCase();
            $('#Searching_Modal').modal('show');
            if (subproduct_image1 == "") {
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_subproducts1",
                    type: "POST",
                    data: {
                        'id1': id1,
                        'subproduct_id1': subproduct_id1,
                        'product_name1': product_name1,
                        'subproduct_modal1': subproduct_modal1,
                        'subproduct_desc1': subproduct_desc1,
                        'subproduct_name1': subproduct_name1
                    }, // {action:'$funky'}
                    //datatype  :   "JSON",
                    success: function(data) {
                        $('#Searching_Modal').modal('hide');
                        data = $.trim(data);
                        if (data == "Sub category updated Successfully") {
                            swal({
                                  title: "Sub category updated successfully",
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                        } else {
                            $('#rowdata_category_1').append(data);
                            $('#edits_category').animate({ scrollTop: 0 });
                            $('.error').show();
                        }
                    },
                });
            } else {
                if ($.inArray(subproduct_image1, ['gif', 'jpg', 'png', 'jpeg']) !== -1) {
                    var subproduct_image1 = $('#subproduct_image1').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('id1', id1);
                    form_data.append('subproduct_id1', subproduct_id1);
                    form_data.append('product_name1', product_name1);
                    form_data.append('subproduct_name1', subproduct_name1);
                    form_data.append('subproduct_modal1', subproduct_modal1);
                    form_data.append('subproduct_desc1', subproduct_desc1);
                    form_data.append('subproduct_image1', subproduct_image1);
                    $.ajax({
                        url: "<?php echo base_url(); ?>index.php?/controller_admin/edit_subproducts",
                        type: "POST",
                        data: form_data, // {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            data = $.trim(data);
                            if (data == "Sub category updated Successfully") {
                                swal({
                                  title: "Sub category updated successfully",
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                            } else {
                                $('#rowdata_category_1').append(data);
                                $('#edits_category').animate({ scrollTop: 0 });
                                $('.error').show();
                            }
                        },
                    });
                } else {
                    swal('Sub category image should be an image file');
                }
            }
        }
        function deletes_category(id) {
            swal({
                  title: "Are you sure to Delete?",
                  text: "You will not be able to recover this Sub Category!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes, Delete It!",
                  cancelButtonText: "No, Keep It!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm) {
                  if (isConfirm) {
                      $.ajax({
                          url: "<?php echo base_url(); ?>index.php?/controller_superad/deletesubproduct",
                          type: "POST",
                          data: {
                              'id': id
                          }, // {action:'$funky'}
                          //datatype  :   "JSON",
                          cache: false,
                          success: function(data) {
                              if (data == 1) {
                                  swal({
                                      title: "Sub Category Deleted Successfully",
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                              } else {
                                  swal({
                                      title: "Sub Category Not Deleted Successfully",
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                              }
                          },
                      });
                  } else {
                    swal.close();
                  }
                });
        }
        </script>


<div class="modal fade" id="add-products" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Add Products</h4>
          </div>
          <div class="modal-body">
          <form class="form-horizontal" role="form">
          <div class="form-group">
          <label class="col-md-3 control-label">Product_id</label>
          <div class="col-md-9">
              <input type="text" class="form-control" id='pro_id' name='pro_id'>
          </div>
      </div>
          <div class="form-group">
          <label class="col-md-3 control-label">Name</label>
          <div class="col-md-9">
              <input type="text" class="form-control" placeholder="Enter product name">
          </div>
      </div>
      <div class="form-group">
          <label class="col-md-3 control-label">Description</label>
          <div class="col-md-9">
              <textarea class="form-control" rows="3" maxlength="140" style='resize:none'></textarea>
          </div>
      </div>


      <div class="form-group ">
          <label class="control-label col-md-3">Upload Image</label>
          <div class="col-md-9">
              <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                  <div>
                      <span class="btn red btn-outline btn-file">
                          <span class="fileinput-new"> Select image </span>
                          <span class="fileinput-exists"> Change </span>
                          <input type="file" name="..."> </span>
                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
              </div>
          </div>
      </div>

          </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
              <button type="button" class="btn green">Save changes</button>
          </div>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="add-sub-products" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Products</h4>
            </div>
            <div class="modal-body">
            <form class="form-horizontal" role="form">
            <div class="form-group">
            <label class="col-md-3 control-label">Sub Product Name</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Enter sub product name">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Product Description</label>
            <div class="col-md-9">
                <textarea class="form-control" rows="3" maxlength="140" style='resize:none'></textarea>
            </div>
        </div>

        <div class="form-group ">
            <label class="control-label col-md-3">Upload Image</label>
            <div class="col-md-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                    <div>
                        <span class="btn red btn-outline btn-file">
                            <span class="fileinput-new"> Select image </span>
                            <span class="fileinput-exists"> Change </span>
                            <input type="file" name="..."> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    </div>
                </div>
            </div>
        </div>

            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

    </body>
</html>