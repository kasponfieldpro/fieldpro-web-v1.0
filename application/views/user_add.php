<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <?php include 'assets/lib/cssscript.php'?>
        <style>
            .fileinput-new, .fileinput-exists {
                    color: #000;
            }
            .dt-buttons{
                    display:none;
            }
            .sweet-alert.showSweetAlert.visible{
                    z-index: 999999999 !important;
                    border: 1px solid red;
            }
            label.showworkcategory {
    border: 0px !important;
            margin-top: 1px !important;
}
            /* .dataTables_filter
            {
              text- align-last: right;
            } */
.uneditable-input {
min-width:auto !important;
}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
.tableimg {
    height: 45px !important;
	width: 45px;
object-fit: cover;
}
        </style>
    </head>
    <!-- END HEAD -->

   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header.php"?>
            <!-- END HEADER -->
             <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box dark">
                                    <div class="portlet-title">
                                        <div class="caption">User Management</div>
                                        <div class="actions">
										 <div class="btn-group">
                                                        <button id="sample_editable_1_new" class="btn btn-circle green btn-outline" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"> Bulk Upload
                                                            <i class="fa fa-upload"></i>
                                                        </button>
                                                    </div>
                                                    <div class="btn-group">
                                                        <button id="sample_editable_1_new" class="btn btn-circle red btn-outline" onClick="model1_open()"> Add User
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
										 </div>
                                    </div>
                                    <div class="portlet-body">
                                        
                                        <div class="portlet light bordered">
                                            <div class="portlet-title tabbable-line">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_actions_pending" data-toggle="tab">Users</a>
                                                    </li>
                                                    <li>
                                                        <a href="#Accepted" data-toggle="tab">Technicians</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_actions_pending">
                                                    <div class="table=responsive">
                                                        <table class="table table-hover table-bordered sample_2" id="">
                                                            <thead>
                                                                <tr>
                                                                    <th class='text-center'>Emp ID</th>
                                                                    <th>Name</th>
                                                                    <th class='text-center'>Role</th>
                                                                    <th class='text-center'>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                   $result=json_decode($records,true);
                                                                   $res=(array)$result[0]['technician'];
                                                                   $res1=(array)$result[1]['user'];
                                                                   for($i=0;$i<count($res1);$i++) {
                                                                ?>
                                                                <tr>
                                                                    <td class='text-center'><a id="<?php echo $res1[$i]['user_id']; ?>" onclick='view_employee(this.id)'><?php echo $res1[$i]['employee_id']; ?></a></td>
                                                                    <td><?php echo $res1[$i]['first_name'] ." ".$res1[$i]['last_name']; ?></td>
                                                                    <td class='text-center'><?php echo $res1[$i]['role']; ?></td>
                                                                    <td style="text-align:center">
                                                                        <span>
                                                                            <button class="btn btn-circle blue btn-outline btn-sm btn-icon-only" id="<?php echo $res1[$i]['user_id']; ?>" onClick="edit(this.id)"><i class="fa fa-edit"></i></button>           
                                                                            <button class="btn btn-circle red btn-outline btn-sm btn-icon-only" id="<?php echo $res1[$i]['user_id']; ?>" onClick="deletes(this.id)"><i class="fa fa-trash"></i></button>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="Accepted">
                                                    <div class="table=responsive">
                                                        <table class="table table-hover table-bordered sample_2" id="">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Technician Image</th>
                                                                    <th class="text-center">Emp Id</th>
                                                                    <th>Name</th>
                                                                    <th class="text-center">Product Category</th>
                                                                    <!--<th>Sub Category</th>
                                                                    <th style="text-align:center">Skill Level</th>-->
                                                                    <th class="text-center">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                   $result=json_decode($records,true);
                                                                   $res=(array)$result[0]['technician'];
                                                                   for($i=0;$i<count($res);$i++) {
                                                                ?>
                                                                <tr>
                                                                    
                                                                    <td  class="text-center">

                                                                        <?php if($res[$i]['image']!='' && $res[$i]['image']!=null)
                                                                        {

                                                                             
                                                                            ?>
                                                                            <img src="<?php echo $res[$i]['image']; ?>" class="img-circle tableimg" alt="" title="" />   
                                                                            <?php

                                                                    }
                                                                    else
                                                                        {
                                                                            
                                                                            ?>
                                                                            <img src="<?php echo base_url(); ?>technician_img/user_avatar.png" class="img-circle tableimg" alt="" title="" />   
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                 </td>
                                                                    <td class="text-center"><a id="<?php echo $res[$i][ 'technician_id']; ?>" onclick='view_employee(this.id)'><?php echo $res[$i][ 'employee_id']; ?></a>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $res[$i][ 'first_name']." ".$res[$i]['last_name']; ?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <span>
                                                                            <button class="btn btn-circle dark btn-outline btn-sm" id="<?php echo $res[$i]['technician_id']; ?>" onClick="view_product(this.id)">View Product Details</button> 
                                                                        </span>
                                                                        <!--<?php echo $res[$i][ 'product_name']; ?>-->
                                                                    </td>
                                                                    <!--<td>
                                                                        <?php echo $res[$i]['cat_name']; ?>
                                                                    </td>
                                                                    <td style="text-align:center">
                                                                        <?php echo $res[$i][ 'skill_level']; ?>
                                                                    </td>-->
                                                                    <td style="text-align:center">
                                                                        <span>
                                                                            <button class="btn btn-circle blue btn-outline btn-sm btn-icon-only" id="<?php echo $res[$i]['technician_id']; ?>" onClick="edit(this.id)"><i class="fa fa-edit"></i></button>         
                                                                            <button class="btn btn-circle red btn-outline btn-sm btn-icon-only" id="<?php echo $res[$i]['technician_id']; ?>" onClick="deletes(this.id)"><i class="fa fa-trash"></i></button>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                <!-- END PAGE BASE CONTENT -->
				<!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                </div>

        <!--Modal Starts-->
        
        
        <div id="myModal1" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="border:0">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="error" style="display:none">
                            <label id="rowdata"></label>
                        </div>
                    </div>
                    <div class="modal-body">
                    <div class="portlet light bordered" id="form_wizard_2" style="margin-bottom:0">
                            <div class="portlet-body form">
                                <form class="form-horizontal" action="#" id="submit_form_1" method="POST" novalidate="novalidate">
                                    <div class="form-wizard">
                                        <div class="form-body">
                                            <ul class="nav nav-pills nav-justified steps">
                                                <li class="active">
                                                    <a href="#tab6" data-toggle="tab" class="step" aria-expanded="true">
                                                        <span class="number"> 1 </span>
                                                        <span class="desc">
                                                            <i class="fa fa-check"></i> Personal Profile </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab4" data-toggle="tab" class="step">
                                                        <span class="number"> 2 </span>
                                                        <span class="desc">
                                                            <i class="fa fa-check"></i> Work Profile </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab5" data-toggle="tab" class="step">
                                                        <span class="number"> 3 </span>
                                                        <span class="desc">
                                                            <i class="fa fa-check"></i> Confirm </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                                <div class="progress-bar progress-bar-success" style="width: 50%;"> </div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="alert alert-danger display-none">
                                                    <button class="close" data-dismiss="alert"></button> All fields are Mandatory. </div>
                                                <div class="alert alert-success display-none">
                                                    <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                <div class="tab-pane active" id="tab6">
                                                    <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Personal Profile</h3>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Employee Id
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="emp_id" name="emp_id" required />                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                            <label class="control-label col-md-5">Company Name
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="c_name" name="c_name" value="<?php echo $this->session->userdata('companyname');?>" readonly />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">First Name
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="first_name" name="first_name" required />                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Last Name
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="last_name" name="last_name" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Email ID
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="email" class="form-control form-control1" id="email_id" name="email_id" placeholder=""required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Contact Number
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="contact_no" name="contact_no" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                       <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Alternative Number
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="acontact_no" name="acontact_no" placeholder="" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                            <label class="control-label col-md-5">Company ID
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Address</h3>
                                                    <div class="row">
                                                       <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Door/Plot No
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="door_plotno" name="door_plotno" placeholder="Door/Plot/Flat No" required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Street/Locality
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="street_locality" name="street_locality" placeholder="ex: Mahathma Ganthi Street" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                       <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Town
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="town_name" name="town_name" placeholder="ex: Teachers Colony" required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Landmark
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="land_mark" name="land_mark" placeholder="ex: Bus Depot" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                       <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">City
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="city_name" name="city_name" placeholder="ex: Chennai" required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Country
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select class="form-control form-control1" id="country_name" name="country_name" required >
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">State
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select class="form-control form-control1" id="state_name" name="state_name" required >
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">PIN Code
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="pin_code" name="pin_code" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab4">
                                                    <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Work Profile</h3>
                                                    <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Role
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                 <select name="role" id="role" class="form-control form-control1" tabindex="-1" aria-hidden="true" required >
                                                                   <option value="" selected disabled>Select Role</option>
                                                                   <option value="Admin">Admin</option>
                                                                   <option value="Service_Manager">Service Manager</option>
                                                                   <option value="Call-coordinator">Call-coordinator</option>
                                                                   <option value="Service_Desk">Service Desk</option>
                                                                   <option value="Technician">Technician</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Region
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                 <select name="region" id="region" class="form-control form-control1" tabindex="-1" aria-hidden="true" required >
                                                                   <option value="" selected disabled>Select Region</option>
                                                                   <option value="all" id="regionall" style="display:none">All</option>
                                                                   <option value="north">North</option>
                                                                   <option value="south">South</option>
                                                                   <option value="east">East</option>
                                                                   <option value="west">West</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Location
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="location" name="location" placeholder="ex: Adyar" required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Area
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="area" name="area" placeholder="ex: Tamilnadu,Chennai" required />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12 products" style="display:none">
                                                            <label class="control-label col-md-5">Product Category
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select name="fields[0][0]" id="fields_0" class="form-control form-control1" tabindex="-1" aria-hidden="true" onChange="generatearray(this,0);load_subcategory(this);" required >
                                                                    <option value="" selected disabled>Select product Category</option>
                                                                        <?php
                                                                            foreach ($record->result() as $row) {
                                                                        ?>
                                                                   <option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                                                   <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12 products" style="display:none">
                                                            <label class="control-label col-md-5">Sub Category
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select name="fields[0][1]" id="category_0" class="form-control form-control1" tabindex="-1" aria-hidden="true" onChange="generatearray(this,1)" required >
                                                                    <option selected disabled>Select Sub Category</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12 products" style="display:none">
                                                            <label class="control-label col-md-5">Skill Level
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select name="fields[0][2]" id="skill_0" class="form-control form-control1" tabindex="-1" aria-hidden="true" onChange="generatearray(this,2)" required >
                                                                   <option selected disabled>Select Skill Level</option>
                                                                   <option value="L1">L1</option>
                                                                   <option value="L2">L2</option>
                                                                   <option value="L3">L3</option>
                                                                   <option value="L4">L4</option>
                                                               </select>
                                                            </div>
                                                        </div>
    <div class="form-group col-md-6 col-sm-12 products" style="display:none">
                                                            <label class="control-label col-md-5">Work Type
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                 
  <select name="fields[0][3]" id="work_type_0" class="form-control form-control1" tabindex="-1" aria-hidden="true" required onChange="generatearray(this,3)">
                                                                   <option value="" selected disabled>Select Work Type</option>
                                                                  
                                                                        <?php
	 // print_r($servicegroup);
                                                                            foreach ($servicegroup->result() as $row1) {
                                                                        ?>
                                                                   <option value="<?php echo $row1->service_group_id; ?>"><?php echo $row1->service_group; ?></option>
                                                                   <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row" id="insert_before">
                                                       <hr>
                                                        <div class="form-group col-md-12 col-sm-12 products text-right" style="display:none">
                                                            <button type="button" class="btn btn-circle dark btn-outline" id="add_products"><i class="fa fa-plus"></i> Add Product Category</button>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab5">
                                                    <h3 class="block">Confirm your Entries</h3>
                                                    <h4 class="form-section">Personal Profile</h4>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Employee ID:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="emp_id"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">First Name:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="first_name"> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Last Name:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="last_name"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Contact Number:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="contact_no"> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Door/Plot No:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="door_plotno"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Street/Locality:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="street_locality"> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Town:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="town_name"> </p>
                                                            </div>
                                                        </div>
                                                         <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">Landmark:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="land_mark"> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                       
                                                        <div class="form-group col-md-6 col-sm-12">
                                                            <label class="control-label col-md-5">City:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static h4" data-display="city_name" > </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="showservicegroup"></div>
                                                </div>
                                            </div>
                                            <div class="form-actions" style="border-top:1px solid #53ced9 !important">
                                                <div class="row">
                                                    <div class="col-md-offset-5 col-md-7">
                                                        <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                                            <i class="fa fa-angle-left"></i> Back
                                                        </a>
                                                        <a href="javascript:;" class="btn btn-circle blue btn-outline button-next" onClick="confirmshowtech()"> Continue
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                        <a class="btn btn-circle blue btn-outline button-submit" style="display: none;" id="adduser"> Submit
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Bulk Upload</h4>
                  </div>
                  <div class="modal-body">
                     <div class="container">
                       <form action="#" class="form-horizontal form-bordered">
                            <div class="form-body row">
                                <div class="form-group col-md-2 col-sm-12 text-center">
                                    <?php $fname='Useradd.xlsx'; ?>
                                    <a class="btn btn-circle purple-sharp btn-outline sbold uppercase" href="<?php echo base_url(); ?>index.php?/controller_admin/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="add_excel" id="add_excel"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </form>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-circle green btn-outline" id="bulkupload" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
                     <button type="button" class="btn btn-circle red btn-outline" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
         <!-- Modal -->

        <!-- Modal -->
        <div id="view_customer" class="modal fade" data-backdrop="static" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="">
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">First Name :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_first"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Last Name :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_last"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Email ID :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_email"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Contact Number :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_contact"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Door/Plot No :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_door"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Street/Locality :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_street"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Town :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_town"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Landmark :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_landmark"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">City :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_city"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Country :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_country"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">State :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_state"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">PIN Code :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="view_pin"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label class="control-label col-md-5">Service Group :
                                </label>
                                <div class="col-md-7">
                                    <label class="control-label" id="servicegroups"></label>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn circle red btn-outline" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

        
        
        <div id="sla_confirm" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="add_subproduct">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <p id="sla_detail_confirm"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-circle blue btn-outline"  onclick="confirm_sla()"><i class="fa fa-check"></i> OK !</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
         <div id="edits" class="modal fade" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header" style="border:0">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <div class="error" style="display:none">
                        <label id="rowdata_1"></label>
                     </div>
                  </div>
                  <div class="modal-body">
                    <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                          <div class="portlet-body form">
                            <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li class="active">
                                                <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                                    <span class="number"> 1 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i> Personal Profile </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                    <span class="number"> 2 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i> Work Profile </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab" class="step">
                                                    <span class="number"> 3 </span>
                                                    <span class="desc">
                                                        <i class="fa fa-check"></i> Confirm </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                            <div class="progress-bar progress-bar-success" style="width: 50%;"> </div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button> All fields are Mandatory. </div>
                                            <div class="alert alert-success display-none">
                                                <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                            <div class="tab-pane active" id="tab1">
                                                <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Personal Profile</h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Employee Id
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="emp_id1" name="emp_id1" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6 col-sm-12" style='display:none'>
                                                        <label class="control-label col-md-5">technician id
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="tech_id1" name="tech_id1">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                        <label class="control-label col-md-5">Id
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="id1" name="id1" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                        <label class="control-label col-md-5">Company Name
                                                                <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="c_name1" name="c_name1" value="<?php echo $this->session->userdata('companyname');?>" readonly>
                                                        </div>
                                                    </div>
													 <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                        <label class="control-label col-md-5">User image address
                                                                <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                                <input type="text" class="form-control form-control1" id="user_imageaddress" name="user_imageaddress" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">First Name
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="first_name1" name="first_name1" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Last Name
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="last_name1" name="last_name1" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Email ID
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="email_id1" name="email_id1" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contact Number
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="contact_no1" name="contact_no1" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Alternative Number
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="acontact_no1" name="acontact_no1" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                        <label class="control-label col-md-5">Company ID
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="c_id1" name="c_id1" value="<?php echo $this->session->userdata('companyid');?>" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Address</h3>
                                                <div class="row">
                                                   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Door/Plot No
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="door_plotno1" name="door_plotno1" placeholder="Door/Plot/Flat No" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Street/Locality
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="street_locality1" name="street_locality1" placeholder="ex: Mahathma Ganthi Street" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Town
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="town_name1" name="town_name1" placeholder="ex: Teachers Colony" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Landmark
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="land_mark1" name="land_mark1" placeholder="ex: Bus Depot" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                   <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">City
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="city_name1" name="city_name1" placeholder="ex: Chennai" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Countryedit
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <select class="form-control form-control1" id="country_name1" name="country_name1" required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">State
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <select class="form-control form-control1" id="state_name1" name="state_name1" required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">PIN Code
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="pin_code1" name="pin_code1" required />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2">
                                                <h3 class="block" style="border-bottom:1px solid #53ced9 !important">Work Profile</h3>
                                                <div class="row">
                                                <div class="form-group col-md-6 col-sm-12" style="display:none">
                                                        <label class="control-label col-md-5">roles
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="roles1" name="roles1" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Role
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                             <select name="role1" id="role1" class="form-control form-control1" tabindex="-1" aria-hidden="true" required>
                                                               <option value="Admin">Admin</option>
                                                               <option value="Service_Manager">Service Manager</option>
                                                               <option value="Call-coordinator">Call-coordinator</option>
                                                               <option value="Service_Desk">Service Desk</option>
                                                               <option value="Technician">Technician</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Region
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                             <select name="region1" id="region1" class="form-control form-control1" tabindex="-1" aria-hidden="true" required>
                                                               <option value="" selected disabled>Select Region</option>
                                                               <option value="all" id="regionall1" style="display:none">All</option>
                                                               <option value="north">North</option>
                                                               <option value="south">South</option>
                                                               <option value="east">East</option>
                                                               <option value="west">West</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                  
                
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Location
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="location1" name="location1" required>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Area
                                                            <span class="required" aria-required="true"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control form-control1" id="area1" name="area1" required>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div id='product_append' class="products1">
                                                </div>
                                                <div class='row products1' id="products2">
                                                    <div class="form-group col-md-12 col-sm-12 text-right">
                                                        <button type='button' class='btn btn-circle dark btn-outline' id='add_edit_products'><i class='fa fa-plus'></i> Add Product Category</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab3">
                                                    <h3 class="block">Confirm your Entries</h3>
                                                    <h4 class="form-section">Personal Profile</h4>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Employee ID:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="emp_id1"> </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">First Name:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="first_name1"> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Last Name:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="last_name1"> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Contact Number:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="contact_no1"> </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Door/Plot No:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="door_plotno1"> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Street/Locality:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="street_locality1"> </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Town:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="town_name1"> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">Landmark:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="land_mark1"> </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-sm-12">
                                                        <label class="control-label col-md-5">City:</label>
                                                        <div class="col-md-7">
                                                            <p class="form-control-static h4" data-display="city_name1"> </p>
                                                        </div>
                                                    </div>
													
                                                </div>      
												 <div id="editshowservicegroup"></div>
                                            </div>
                                        </div>
                                        <div class="form-actions" style="border-top:1px solid #53ced9 !important">
                                            <div class="row">
                                                <div class="col-md-offset-5 col-md-7">
                                                    <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                                        <i class="fa fa-angle-left"></i> Back </a>
                                                    <a href="javascript:;" class="btn btn-circle blue btn-outline button-next" onclick="editconfirmshowtech()"> Continue
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                    <a class="btn btn-circle blue btn-outline button-submit" style="display: none;" onClick="submit_user()"> Update
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Modal -->
    

        <div id="modal_contract" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
            <!-- Modal content-->
                <div class="modal-content">
                    <!--<div class="modal-header" style="border:0">
                    </div>-->
                    <div class="modal-body">
                        <div class="portlet box blue-hoki" id="form_wizard_1" style="margin-bottom:0">
                            <div class="portlet-title">
                                <div class="caption" style="color:#fff;">Product Category Details</div>
                                <div class="actions" style="padding-top: 14px;"><button type="button" class="close" data-dismiss="modal" style="color:#1e252b !important;">&times;</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                    <div class="form-wizard">
                                        <div class="form-body table-responsive">
                                            <table class="table table-hover table-bordered" id="contract_table">
                                                <thead>
                                                    <tr>
                                                        <th>Product Category</th>
                                                        <th>Sub Category</th>
                                                        <th style="text-align:center">Skill Level</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody_contract">
                                                </tbody>
                                                </table>
                                        </div>
                                        <div class="form-actions" style="border-top:1px solid #ddd !important">
                                                    <button type="button" class="btn btn-circle red btn-outline pull-right" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
        <!--Modal End-->
          <!--loading model-->
          <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->

        <!-- END QUICK SIDEBAR -->
        <?php include 'assets/lib/javascript.php'?>
        <script>
            $(window).load(function(){
                $('#user_management').addClass('open');
                populateCountries("country_name", "state_name");

                $('#country_name1').focus(function(){
                    $('#country_name1').empty();
                    populateCountries("country_name1", "state_name1");
                });
            })
        </script>
        <script type="text/javascript">
            var productelementname=[];
            var productelementvalue=[];
            var subcatelementname=[];
            var subcatelementvalue=[];
            var skillelementname=[];
            var skillelementvalue=[];
            var worktypeelementname=[];
            var worktypeelementvalue=[];
            var deletedarray=[];
           var finalproductelementvalue=[];
           var finalproductelementname=[];
           var finalsubcatelementvalue=[];
           var finalsubcatelementname=[];
           var finalskillelementvalue=[];
           var finalskillelementname=[];
           var finalworktypeelementvalue=[];
           var finalworktypeelementname=[];
          var counter=0;
			
			
			   var editproductelementname=[];
            var editproductelementvalue=[];
            var editsubcatelementname=[];
            var editsubcatelementvalue=[];
            var editskillelementname=[];
            var editskillelementvalue=[];
            var editworktypeelementname=[];
            var editworktypeelementvalue=[];
              var editdeletedarray=[];
           var editfinalproductelementvalue=[];
           var editfinalproductelementname=[];
           var editfinalsubcatelementvalue=[];
           var editfinalsubcatelementname=[];
           var editfinalskillelementvalue=[];
           var editfinalskillelementname=[];
           var editfinalworktypeelementvalue=[];
           var editfinalworktypeelementname=[];
            var editcounter=0;
            function model1_open(){
                counter=0;
                 productelementname=[];
            productelementvalue=[];
            subcatelementname=[];
             subcatelementvalue=[];
            skillelementname=[];
            skillelementvalue=[];
            worktypeelementname=[];
             worktypeelementvalue=[];
                $('#submit_form_1')[0].reset();
                $('#myModal1').modal('show');
            };
            //window.history.forward();
              $(document).ready(function() {
                  $('#datatable').DataTable({"order": []});
                      $('.sample_2').DataTable({"order": []});
              });
         $('#role').change(function(){
            var role=$('#role option:selected').val();

             if(role == "Service_Manager"){
                $('#regionall').show()
            }
            else{
                $('#regionall').hide();
            }

            if(role=="Technician"){
               $('.products').show();
            }else{
               $('.products').hide();
            }
         });

         $('#role1').change(function(){
            var role=$('#role1 option:selected').val();
            
            if(role == "Service_Manager"){
                $('#regionall1').show()
            }
            else{
                $('#regionall1').hide();
            }

            if(role=="Technician"){
               editcounter=0;
                $('#product_append').html('');
				//alert('hello1');
				$('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Sub Category</option></select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");
               //  $('#product_append').append("<hr><div class='row fields_0'><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields[0][0]' class='form-control form-control1 pro_2' id='product1' tabindex='-1' onchange='generatearray(this,0)' aria-hidden='true' required ><option value='' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields[0][1]' onchange='generatearray(this,1)' id='category1' class='form-control form-control1 category1' required><option value='' selected disabled>Select Sub Category</option></select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields[0][2]' onchange='generatearray(this,2)' id='skill_0' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option value='L0' selected>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div></div></div>");
                $("<div class='row' id='products1'><div class='form-group col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-circle dark btn-outline' id='add_edit_productss'><i class='fa fa-plus'></i> Add Product Category</button></div></div>").insertBefore($( "#products2" ));
                $('#add_edit_productss').click(function(){
                    editcounter += 1;
                    $('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><a class='pull-right' id='editcancel_"+ editcounter +"' onclick='editcancel_prod(this.id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required >"+textappend+"</select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");     					                
                    $('.pro_2').change(function(){
                        var id=$(this).attr('id');
                        var prod=$("#"+id).val();
                        var finds=$('.'+id).find('.category1');
                        $.ajax({
                           url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                           type     :  "POST",
                           data     :  {'product_id' : prod},
                           datatype :  "JSON",
                           cache    :  false,
                           process  :  false,
                           success  :  function(data){
                                          var data=JSON.parse(data);
                                            finds.empty();
                                          if(data == 1){
                                            swal({
                                                  title: "No Sub Category are entered, kindly add category to this product Category",
                                                  type: "warning",
                                                  showCancelButton: false,
                                                  confirmButtonClass: "btn-danger",
                                                  confirmButtonText: "Ok!",
                                                  cancelButtonText: "No, cancel plx!",
                                                  closeOnConfirm: false,
                                                  closeOnCancel: false
                                                },
                                                function(isConfirm) {
                                                  if (isConfirm) {
                                                    swal.close();
                                                  }
                                                });
                                            }else{
                                                finds.append("<option value='sel_sub' selected>Select Sub Category</option>");
                                                 for(i=0;i<data.length;i++){
                                                    finds.append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                                 }
                                            }
                                       },
                        })

                    });
                });
    $("#product1").change(function (){
            $('#category1').empty();
            var product_id = $('#product1').val();
            $.ajax({
               url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
               type     :  "POST",
               data     :  {'product_id' : product_id},
               datatype :  "JSON",
               cache    :  false,
               process     :  false,
               success     :  function(data){
                              var data=JSON.parse(data);
                              if(data == 1){
                                swal({
                                  title: "No Category are entered, kindly add category to this product",
                                  type: "warning",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    swal.close();
                                  }
                                });
                              }else{
  
                                 $('#category1').append("<option value='sel_sub' selected >Select Sub Category</option>");
                                 for(i=0; i<data.length;i++){
                                    $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                 }
                              }
                           },
            })
         });
               $('.products1').show();
                $('#products2').hide();
            }else{
               $('.products1').hide();
               $('#products1').hide();
            }
         });

        // $("#productselect").change(function (){
			 function editload_subcategory(selectedproductelement)
            {
                 var id=selectedproductelement.id;
                  var split=id.split("_");

                var length=split.length;
               
                var takenidcount=parseInt(length-1);

            $('#editcategory_'+split[takenidcount]).empty();
           
            var product_id = selectedproductelement.value;
           
            $.ajax({
               url         :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
               type     :  "POST",
               data     :  {'product_id' : product_id},
               datatype :  "JSON",
               cache    :  false,
               process     :  false,
               success     :  function(data){                             
                              var data=JSON.parse(data);
                              if(data == 1){
                                    swal({
                                  title: "No Category are entered, kindly add category to this product",
                                  type: "warning",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    swal.close();
                                  }
                                });
                              }else{
                                   
                                $('#editcategory_'+split[takenidcount]).append('<option selected disabled>Select Sub Category</option>');
                                 for(i=0; i<data.length;i++){                                   
                                    $('#editcategory_'+split[takenidcount]).append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                 }
                              }
                           },
            })
        }
			
			
			
            function load_subcategory(selectedproductelement)
            {
                 var id=selectedproductelement.id;
                  var split=id.split("_");

                var length=split.length;
              
                var takenidcount=parseInt(length-1);

            $('#category_'+split[takenidcount]).empty();
           
            var product_id = selectedproductelement.value;
           
            $.ajax({
               url         :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
               type     :  "POST",
               data     :  {'product_id' : product_id},
               datatype :  "JSON",
               cache    :  false,
               process     :  false,
               success     :  function(data){                             
                              var data=JSON.parse(data);
                              if(data == 1){
                                    swal({
                                  title: "No Category are entered, kindly add category to this product",
                                  type: "warning",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    swal.close();
                                  }
                                });
                              }else{
                                     
                                $('#category_'+split[takenidcount]).append('<option selected disabled>Select Sub Category</option>');
                                 for(i=0; i<data.length;i++){                                   
                                    $('#category_'+split[takenidcount]).append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                 }
                              }
                           },
            })
        }
        // });
         $("#product1").change(function (){
            $('#category1').empty();
            var product_id = $('#product1').val();
            $.ajax({
               url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
               type     :  "POST",
               data     :  {'product_id' : product_id},
               datatype :  "JSON",
               cache    :  false,
               process     :  false,
               success     :  function(data){
                              var data=JSON.parse(data);
                              if(data == 1){
                                swal({
                                  title: "No Category are entered, kindly add category to this product",
                                  type: "warning",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    swal.close();
                                  }
                                });
                              }else{
                                 $('#category1').append('<option selected disabled>Select Sub Category</option>');
                                 for(i=0; i<data.length;i++){
                                    $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                 }
                              }
                           },
            })
         });

$('#inver').change(function(){
         $('#tbdy_inventory').empty();
          var filter=$('#inver').val();
             $.ajax({
               url: "<?php echo base_url();?>index.php?/controller_admin/user2",
               type: 'POST',
               data: {'filter':filter},
               dataType: "json",
               success: function(data) {
                  $('#tbdy_inventory').html('');
                 
                  if(data.length<1)
                  {
                   
                     $('#tbdy_inventory').html('<tr><td colspan=8 style="text-align:center">No records found</td></tr>');
                  }
                  else
                  {
                     //data=JSON.parse(data);
                     for(i=0;i<data.length;i++)
                     {
                        if(data[i].role!="Technician"){
                           $('#tbdy_inventory').append('<tr><td><a id='+data[i].user_id+' onclick="edit(this.id)">'+data[i].employee_id+'</a></td><td>'+data[i].first_name+'</td><td>'+data[i].role+'</td><td>-</td><td>-</td><td style="text-align:center">-</td></tr>');
                        }else{
                           $('#tbdy_inventory').append('<tr><td><a id='+data[i].technician_id+' onclick="edit(this.id)">'+data[i].employee_id+'</a></td><td>'+data[i].first_name+'</td><td>'+data[i].role+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td style="text-align:center !important">'+data[i].skill_level+'</td></tr>');
                        }
                     }
                  }
               }
               });
       })
     
         $('#adduser').click(function(){
            $('#rowdata').empty();
            var role=$('#role').val();
            var c_id=$('#c_id').val();
            var c_name=$('#c_name').val();
            $('#Searching_Modal').modal('show');
            if(role == "Service_Desk"){
               $.ajax({
                  url         :   "<?php echo base_url(); ?>index.php?/controller_admin/checkservicedesk",
                  type        :   "POST",
                  data        :  {'c_name':c_name,'c_id':c_id},// {action:'$funky'}
                  datatype    :  "JSON",
                  success     :  function(data){
                                 var data=JSON.parse(data);
                                 var service_desk=parseInt(data['service_desk']);
                                 var service_added=parseInt(data['service_added']);
                                 if(service_added <service_desk ){

                                    addinguser();
                                 }
else{
swal("Warning!", "Service desk limit is crossed, contact Super admin");
$('#Searching_Modal').modal('hide');
                              //   alert(service_desk);
                               //  alert(service_added);
								  //swal({
                                    //      title: "Service desk limit is crossed, contact Super admin",
                                        //  type: "Warning",
                                      //    showCancelButton: false,
                                          //confirmButtonClass: "btn-danger",
                                          //confirmButtonText: "Ok!",
                                          //cancelButtonText: "No, cancel plx!",
                                          //closeOnConfirm: false,
                                          //closeOnCancel: false
                                        //},
                                        //function(isConfirm) {
                                          //if (isConfirm) {
                                            //    $('.error').show();
                                          //}
                                        //});
                                 //   $('#rowdata').append("");
                                 //   $('.error').show();
                                 }
                              },
               });
            }else if(role=="Technician"){
               $.ajax({
                  url         :   "<?php echo base_url(); ?>index.php?/controller_admin/checktech",
                  type        :   "POST",
                  data        :   {'c_name':c_name,'c_id':c_id},// {action:'$funky'}
                  datatype :  "JSON",
                  cache       :   false,
                  success     :  function(data){
                                 var data=JSON.parse(data);

                                 var technicians=parseInt(data['technicians']);
                                 var tech_added=parseInt(data['tech_added']);
                                 if(technicians <=tech_added){
swal("Warning!", "Technician limit is crossed, contact Super admin");
$('#Searching_Modal').modal('hide');
								 //swal({
                                          //title: "Technician limit is crossed, contact Super admin",
                                          //type: "Warning",
                                          //showCancelButton: false,
                                          //confirmButtonClass: "btn-danger",
                                          //confirmButtonText: "Ok!",
                                          //cancelButtonText: "No, cancel plx!",
                                          //closeOnConfirm: false,
                                          //closeOnCancel: false
                                        //},
                                        //function(isConfirm) {
                                          //if (isConfirm) {
                                         //       $('.error').show();
                                       //   }
                                     //   });
                                 //   $('#rowdata').append("Technician limit is crossed, contact Super admin");
                                 //   $('.error').show();
                                 }else{
                                    addinguser();
                                 }
                              },
               });
            }else{
               addinguser();
            }
            function addinguser(){
               $.ajax({
                  url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insertuser",
                  type        :   "POST",
                  data        :   $('#submit_form_1').serialize(),// {action:'$funky'}
                  datatype    :  "JSON",
                  cache       :   false,
                  success     :  function(data){
                                    data=JSON.parse(data);
                                 data=$.trim(data);
                                 $('#Searching_Modal').modal('hide');
                                 if(data=="User added Successfully"){
                                        $('#myModal1').modal('hide');
                                        counter=0;
                                          productelementname=[];
           productelementvalue=[];
             subcatelementname=[];
             subcatelementvalue=[];
             skillelementname=[];
             skillelementvalue=[];
            worktypeelementname=[];
             worktypeelementvalue=[];
               deletedarray=[];
          finalproductelementvalue=[];
           finalproductelementname=[];
           finalsubcatelementvalue=[];
           finalsubcatelementname=[];
          finalskillelementvalue=[];
           finalskillelementname=[];
          finalworktypeelementvalue=[];
           finalworktypeelementname=[];
                                        swal({
                                          title: data,
                                          type: "success",
                                          showCancelButton: false,
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok!",
                                          cancelButtonText: "No, cancel plx!",
                                          closeOnConfirm: false,
                                          closeOnCancel: false
                                        },
                                        function(isConfirm) {
                                          if (isConfirm) {
                                                location.reload();
                                          }
                                        });
                                 }else{
                                    $('#rowdata').append(data);
                                    $('#myModal1').animate({ scrollTop: 0 });
                                    $('.error').show();
                                 }
                              },
               });
            }
         })
         $('#bulkupload').click(function(){
            var role=$('#role').val();
            var c_id=$('#c_id').val();
            var c_name=$('#c_name').val();
            var inp= $('#add_excel');
            $('#Searching_Modal').modal('show');
          if(inp.val().length > 0)
          {
            var ext = $('#add_excel').val().toString().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                $('#Searching_Modal').modal('hide');
               swal('Please upload Excel file');
            }else {
               var file_data = $('#add_excel').prop('files')[0];
               var form_data = new FormData();
               form_data.append('add_excel', file_data);
               $.ajax({
                  type:'POST',
                  url:'<?php echo base_url(); ?>index.php?/controller_admin/bulk_user',
                  contentType:false,
                  processData: false,
                  cache:false,
                  data:form_data,
                  success: function (data) {
                    $('#Searching_Modal').modal('hide');
                            if(data=="2"){
                                swal({
                                      title: 'All Fields are Mandatory',
                                      type: "error",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                        window.location.reload();
                                      }
                                    });
                                /* $('#rowdata').append('All Fields are Mandatory');
                                  $('#sla_modal').animate({ scrollTop: 0 });
                                  $('.error').show(); */
                           }else{
                               var data=JSON.parse(data);
                                $('#sla_modal').modal('hide');
                                $('#sla_confirm').modal('show');
                                for(i=0;i<data.length;i++){
                                    $('#sla_detail_confirm').append(data[i]+"</br><hr>");
                                }
                    /* swal({
                          title: data,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ok!",
                          cancelButtonText: "No, cancel plx!",
                          closeOnConfirm: false,
                          closeOnCancel: false
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.reload();
                          }
                        }); */
                  }
                  },
               });
            }
        }
        else{
              $('#Searching_Modal').modal('hide');
         swal('upload File and then Submit!')
        }
         })
        

        function edit(id){
            $('#submit_form')[0].reset();
            $('#product_append').empty();
            $('#Searching_Modal').modal('show');
		editproductelementname=[];
           editproductelementvalue=[];
           editsubcatelementname=[];
           editsubcatelementvalue=[];
           editskillelementname=[];
           editskillelementvalue=[];
           editworktypeelementname=[];
           editworktypeelementvalue=[];
           editdeletedarray=[];
           editfinalproductelementvalue=[];
           editfinalproductelementname=[];
           editfinalsubcatelementvalue=[];
           editfinalsubcatelementname=[];
           editfinalskillelementvalue=[];
           editfinalskillelementname=[];
           editfinalworktypeelementvalue=[];
           editfinalworktypeelementname=[];
			var selectedcountry='';
			var selectedstate='';
            editcounter=0;
            $.ajax({
               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_user",
               type        :   "POST",
               data        :   {'id':id},// {action:'$funky'}
               datatype    :  "JSON",
               cache       :   false,
               success     :  function(data){
                             $('#Searching_Modal').modal('hide');
                              var dataarray=JSON.parse(data);
                              $('#id1').val(dataarray[0]['id']);
                              $('#emp_id1').val(dataarray[0]['employee_id']);
                              $('#first_name1').val(dataarray[0]['first_name']);
                              $('#last_name1').val(dataarray[0]['last_name']);
                              $('#email_id1').val(dataarray[0]['email_id']);
                              $('#contact_no1').val(dataarray[0]['contact_number']);
                              $('#acontact_no1').val(dataarray[0]['alternate_number']);
                              $('#door_plotno1').val(dataarray[0]['flat_no']);
                              $('#street_locality1').val(dataarray[0]['street']);
                              $('#town_name1').val(dataarray[0]['town']);
                              $('#land_mark1').val(dataarray[0]['landmark']);
                              $('#city_name1').val(dataarray[0]['city']);
                          
				              selectedcountry=dataarray[0]['country'];
				              selectedstate=dataarray[0]['state'];
                                                     
				  
                              $('#pin_code1').val(dataarray[0]['pincode']);
                              //$('select[name="region1"] option[value="'+data['region']+'"]').attr("selected",true);
                              $('#location1').val(dataarray[0]['location']);
                              $('#area1').val(dataarray[0]['area']);
                              $('#roles1').val(dataarray[0]['role']);
                              $('#user_image').attr('src',dataarray['image']);
				   			  $('#user_imageaddress').val(dataarray['image']);
                              $('#role1').val(dataarray[0]['role']);
                              $('#region1').val(dataarray[0]['region']);
                              $('#tech_id1').val(dataarray[0]['technician_id']);
                              //$('select[name="role1"] option[value="'+data['role']+'"]').attr("selected",true);
                              //$('select[name="skill1"] option[value="'+data['skill_level']+'"]').attr("selected",true);
                              //$('#skill1').val(data[0]['skill_level']);
                              $('#country_name1').append('<option value="'+ selectedcountry +'" selected >'+selectedcountry+'</option>');
                              $('#state_name1').append('<option value="'+ selectedstate +'" selected >'+selectedstate+'</option>');
                              if(dataarray[0]['role']=="Technician"){
                                $('#product_append').empty();
                                editcounter=0;
								
                                k=1;
								  var t_initial=0;
								
                                for(var p=0;p<dataarray.length;p++){
									
									var textappend='';
									 $.ajax({
                                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category_1",
                                        type        :   "POST",
                                        data        :   {'id':dataarray[p]['category'],'count_n':k},// {action:'$funky'}
                                        datatype    :  "JSON",
										  async: false,
                                        cache       :   false,
                                        success     :  function(data1){
                                                            
											//alert("#editcategory_"+ editcounter);
										
												
											editcounter=t_initial;
											var data1=JSON.parse(data1);
											//
											if (data1['user_detail'] == null){
    								
												}				
											else
											{
												  textappend="<option selected value='"+data1['user_detail']['cat_id']+"'>"+data1['user_detail']['cat_name']+"</option>";
											if(editcounter==0)
											   {
												    $('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required >"+textappend+"</select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");     					                
											   }
											   else
											   {
												    $('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><a class='pull-right' id='editcancel_"+ editcounter +"' onclick='editcancel_prod(this.id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required >"+textappend+"</select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");     					                
											   }
                                     
									
											t_initial=t_initial+1;
											}
										
                                    
											
									
										}
                                    });
									
									editproductelementname.push(dataarray[p]['product_name']);
           							editsubcatelementname.push(dataarray[p]['cat_name']);
           							editskillelementname.push(dataarray[p]['skill_level']);
           							editworktypeelementname.push(dataarray[p]['service_group']);
									
									editproductelementvalue.push(dataarray[p]['product']);
								   editsubcatelementvalue.push(dataarray[p]['category']);
								   editskillelementvalue.push(dataarray[p]['skill_level']);
								   editworktypeelementvalue.push(dataarray[p]['work_type']);
                                 
                                    k++;
								}
								    $('.products1').show();
								 
								  for(var g=0;g<editproductelementvalue.length;g++)
								  {
								  	$("#editfields_"+g).val(dataarray[g]['product']);
                                    $('#editskill_'+g).val(dataarray[g]['skill_level']);
                                    $('#editwork_type_'+g).val(dataarray[g]['work_type']);
									$('#editcategory_'+g).val(dataarray[g]['category']);
								  }
								 
								 //alert(selectedcountry);
								  // alert(selectedstate);
								/*  for(var g=0;g<editproductelementvalue.length;g++)
								  {
								     $.ajax({
                                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category_1",
                                        type        :   "POST",
                                        data        :   {'id':editsubcatelementvalue[g],'count_n':k},// {action:'$funky'}
                                        datatype    :  "JSON",
                                        cache       :   false,
                                        success     :  function(data){
                                                            
											//alert("#editcategory_"+ editcounter);
											console.log(data);
												
											
											var data=JSON.parse(data);
											
										
                                                            $("#editcategory_"+g).append('<option selected value="'+data['user_detail']['cat_id']+'">'+data['user_detail']['cat_name']+'</option>');
                                        					                
										}
                                    });
								  
								  }*/
								  
                              }else{
                                $('.products1').hide();
                              }
                              $('#edits').modal('show');
                           },
            });

         }
                                     $('.pro_2').change(function(){
                                        var id=$(this).attr('id');
                                        var prod=$("#"+id).val();
                                           var split=id.split("_");
                var length=split.length;
                var takenidcount=parseInt(length-1);
                   // $('#category_'+takenidcount).empty();
	//alert('change');
                                        var finds=$('#category_'+takenidcount);
                                        $.ajax({
                                           url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                                           type     :  "POST",
                                           data     :  {'product_id' : prod},
                                           datatype :  "JSON",
                                           cache    :  false,
                                           process  :  false,
                                           success  :  function(data){
                                                          var data=JSON.parse(data);
                                                            finds.empty();
                                                          if(data == 1){
                                                            swal({
                                                                  title: "No Sub Category are entered, kindly add Sub category to this product Category",
                                                                  type: "warning",
                                                                  showCancelButton: false,
                                                                  confirmButtonClass: "btn-danger",
                                                                  confirmButtonText: "Ok!",
                                                                  cancelButtonText: "No, cancel plx!",
                                                                  closeOnConfirm: false,
                                                                  closeOnCancel: false
                                                                },
                                                                function(isConfirm) {
                                                                  if (isConfirm) {
                                                                    swal.close();
                                                                  }
                                                                });
                                                            }else{
                                                                finds.append("<option value='sel_sub' selected disabled value='1'>Select Sub Category</option>");
                                                                 for(i=0; i<data.length;i++){
                                                                    finds.append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                                                 }
                                                            }
                                                       },
                                        })

                                    });
                                
								    
								 
                                $('#add_edit_products').click(function(){/////
                                    editcounter += 1;
								//	alert('hello')
                                  //  $('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><a class='pull-right' id='editcancel_"+ editcounter +"' onclick='editcancel_prod(this.id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required >"+textappend+"</select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");     					                
                                      $('#product_append').append("<hr><div class='row fields_"+ editcounter +"'><a class='pull-right' id='editcancel_"+ editcounter +"' onclick='editcancel_prod(this.id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][0]' onchange='editgeneratearray(this,0);editload_subcategory(this);' id='editfields_"+ editcounter + "' class='form-control form-control1 pro_2' tabindex='-1' aria-hidden='true' required ><option value='sel_sub' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][1]' id='editcategory_"+editcounter+"' onchange='editgeneratearray(this,1)' class='form-control form-control1 category1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Sub Category</option></select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][2]' onchange='editgeneratearray(this,2)' id='editskill_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='editfields["+ editcounter + "][3]' onchange='editgeneratearray(this,3)' id='editwork_type_"+ editcounter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>");
									$('.pro_2').change(function(){
										//alert('hola');
                                        var id=$(this).attr('id');
                                        var prod=$("#"+id).val();
                                        var finds=$('.'+id).find('.category1');
									//	alert(finds);
                                        $.ajax({
                                           url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                                           type     :  "POST",
                                           data     :  {'product_id' : prod},
                                           datatype :  "JSON",
                                           cache    :  false,
                                           process  :  false,
                                           success  :  function(data){
                                                          var data=JSON.parse(data);
                                                            finds.empty();
                                                          if(data == 1){
                                                            swal({
                                                                  title: "No Sub Category are entered, kindly add category to this product Category",
                                                                  type: "warning",
                                                                  showCancelButton: false,
                                                                  confirmButtonClass: "btn-danger",
                                                                  confirmButtonText: "Ok!",
                                                                  cancelButtonText: "No, cancel plx!",
                                                                  closeOnConfirm: false,
                                                                  closeOnCancel: false
                                                                },
                                                                function(isConfirm) {
                                                                  if (isConfirm) {
                                                                    swal.close();
                                                                  }
                                                                });
                                                            }else{
                                                                finds.append("<option value='sel_sub' selected>Select Sub Category</option>");
                                                                 for(i=0;i<data.length;i++){
                                                                    finds.append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                                                 }
                                                            }
                                                       },
                                        })

                                    });
                                });
                              

        function submit_user(){
            $('#rowdata_1').empty();
            $('#sla_detail_confirm').empty();
            $('#Searching_Modal').modal('show');
            $.ajax({
				/*	editproductelementvalue.push(dataarray[p]['product']);
								   editsubcatelementvalue.push(dataarray[p]['category']);
								   editskillelementvalue.push(dataarray[p]['skill_level']);
								   editworktypeelementvalue.push(dataarray[p]['work_type']);*/

               url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edituser",
               type        :   "POST",
               data        :   $('#submit_form').serialize()+ "&productfields="+editproductelementvalue+"&categoryfields="+editsubcatelementvalue+"&skillfields="+editskillelementvalue+"&worktypefields="+editworktypeelementvalue+"&deletedfields="+deletedarray,// {action:'$funky'}
               datatype    :  "JSON",
               cache       :   false,
               success     :  function(data){
                              //data=$.trim(data);
                              //var data = (data);
                              $('#Searching_Modal').modal('hide');
                                $('#edits').modal('hide');
                                $('#sla_detail_confirm').html(data);
                                $('#sla_confirm').modal('show');
                                /*if(data == "User Updated Successfully"){
                                  //alert('success');
                                 // return false;

                                    $('#edits').modal('hide');
                                    swal({
                                      title: data,
                                      type: "success",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                            location.reload();
                                      }
                                    });
                                }
                                else{
                                    // alert('not success');
                                    //return false;
                                    // var data=JSON.parse(data);
                                    $('#sla_modal').modal('hide');
                                    //$('#sla_confirm').modal('show');
                                     swal({
                                      title: data,
                                      type: "danger",
                                      showCancelButton: false,
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok!",
                                      cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                    },
                                    function(isConfirm) {
                                      if (isConfirm) {
                                      $('#edits').modal('show');
                                      }
                                    });
                                }*/
                           },
            });
         }
         function deletes(id){
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this User!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, keep it!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                  $.ajax({
                      url: "<?php echo base_url(); ?>index.php?/controller_admin/deleteuser",
                      type: "POST",
                      data: {
                          'id': id
                      },
                      cache: false,
                      success: function(data) {
                          if (data == 1) {
                              swal({
                                  title: "User Deleted successfully",
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                          } else {
                              swal({
                                  title: "User Not Deleted, Kindly try again!",
                                  type: "error",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok!",
                                  cancelButtonText: "No, cancel plx!",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
                          }
                      },
                  });
              } else {
                swal.close();
              }
            });
         }
         
         function confirm_sla(){
             if(data=""){
                 
             }
             else{
                location.reload();
             }
         }
     
            function view_employee(id){
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_user",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :  "JSON",
                    cache       :   false,
                    success     :  function(data){
                                  var data=JSON.parse(data);
                                
                                 
                                  $('#view_first').text(data[0]['first_name']);
                                  $('#view_last').text(data[0]['last_name']);
                                  $('#view_email').text(data[0]['email_id']);
                                  $('#view_contact').text(data[0]['contact_number']);
                                  $('#view_door').text(data[0]['flat_no']);
                                  $('#view_street').text(data[0]['street']);
                                  $('#view_town').text(data[0]['town']);
                                  if(data[0]['landmark']==""){
                                        $('#view_landmark').text('-');
                                  }else{
                                      $('#view_landmark').text(data[0]['landmark']);
                                  }
                                  $('#view_city').text(data[0]['city']);
                                  $('#view_country').text(data[0]['country']);
                                  $('#view_state').text(data[0]['state']);
                                  $('#view_pin').text(data[0]['pincode']);
                                  if(data[0]['role']=="Technician"){
                                       
                                        $('#servicegroups').text(data[0]['servicegroup']);
                                  }else{
                                        $('#servicegroups').text('-');
                                  }
                                  //$('#servicegroups').text(data[0]['servicegroup']);
                                  $('#view_customer').modal('show');
                                  
                                },
                });
            }
      
            
            $('#add_products').click(function(){
               counter += 1;
              $("<hr class='hr_"+ counter +"'><div class='row fields_"+ counter +"'><a class='pull-right' id='cancel_"+ counter +"' onclick='cancel_prod(this.id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][0]' onchange='generatearray(this,0);load_subcategory(this);' id='fields_"+ counter + "' class='form-control form-control1 pro_1' tabindex='-1' aria-hidden='true' required ><option value='' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][1]' id='category_"+ counter + "' class='form-control form-control1' tabindex='-1' aria-hidden='true' onchange='generatearray(this,1)' required ><option selected disabled>Select Sub Category</option></select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][2]' onchange='generatearray(this,2)' id='skill_"+ counter +"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div></div> <div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][3]' onchange='generatearray(this,3)' id='work_type_"+counter+"' class='form-control form-control1' tabindex='-1' aria-hidden='true' required > <option value='' selected disabled>Select Work Type</option><?php foreach ($servicegroup->result() as $row1) {?><option value='<?php echo $row1->service_group_id; ?>'><?php echo $row1->service_group; ?></option><?php } ?></select></div></div></div></div>").insertBefore("#insert_before" );
                



/*
                $("<hr class='hr_"+ counter +"'><div class='row fields_"+ counter +"'><a class='pull-right' id='cancel_"+ counter +"' onclick='cancel_prod(id)'><i class='fa fa-times'></i></a><div class='row'><div class='form-group col-md-6 col-sm-12 products'><label class='control-label col-md-5'>Product Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][0]' id='fields_"+ counter + "' class='form-control form-control1 pro_1' tabindex='-1' aria-hidden='true' required ><option value='' selected disabled>Select product Category</option><?php foreach ($record->result() as $row) {?><option value='<?php echo $row->product_id; ?>'><?php echo $row->product_name; ?></option><?php } ?></select></div></div><div class='form-group col-md-6 col-sm-12 products' ><label class='control-label col-md-5'>Sub Category<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select             name='fields["+ counter +"][1]' id='category' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Sub Category</option></select></div></div></div><div class='row'><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Skill Level<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][2]' id='skill' class='form-control form-control1' tabindex='-1' aria-hidden='true' required ><option selected disabled>Select Skill Level</option><option value='L1'>L1</option><option value='L2'>L2</option><option value='L3'>L3</option><option value='L4'>L4</option></select></div><div class='form-group col-md-6 col-sm-12 products' style=''><label class='control-label col-md-5'>Work Type<span class='required' aria-required='true'> * </span></label><div class='col-md-7'><select name='fields["+ counter + "][3]' id='work_type"+ counter + "' class='form-control form-control1' tabindex='-1' aria-hidden='true' required >
                    <option value='' selected disabled>Select Work Type</option><option value='1'>Installation</option><option value='2'>Maintenance</option><option value='3'>Break down</option>
                     </select></div></div></div></div></div>").insertBefore( "#insert_before" );*/
                $('.pro_1').change(function(){
                    var id=$(this).attr('id');
                    var prod=$("#"+id).val();
                   // alert(prod);
                    var finds=$('.'+id).find('#category');
                    $.ajax({
                       url      :  "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                       type     :  "POST",
                       data     :  {'product_id' : prod},
                       datatype :  "JSON",
                       cache    :  false,
                       process  :  false,
                       success  :  function(data){
                                      var data=JSON.parse(data);
                                        finds.empty();
                                      if(data == 1){
                                        swal({
                                              title: "No Category are entered, kindly add category to this product",
                                              type: "warning",
                                              showCancelButton: false,
                                              confirmButtonClass: "btn-danger",
                                              confirmButtonText: "Ok!",
                                              cancelButtonText: "No, cancel plx!",
                                              closeOnConfirm: false,
                                              closeOnCancel: false
                                            },
                                            function(isConfirm) {
                                              if (isConfirm) {
                                                swal.close();
                                              }
                                            });
                                        }else{
                                            finds.append('<option selected disabled>Select Sub Category</option>');
                                             for(i=0; i<data.length;i++){
                                                finds.append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                             }
                                        }
                                   },
                    })

                });
            });


            function cancel_prod(id){
         
          var id=id.split("_");

          $('.fields_'+id[1]).remove();
          $('.hr_'+id[1]).remove();
          var idtopush=parseInt(id[1]);
          deletedarray.push(parseInt(idtopush));
            }
            function editcancel_prod(id){
         
          var id=id.split("_");

          $('.fields_'+id[1]).remove();
          $('.hr_'+id[1]).remove();
          var idtopush=parseInt(id[1]);
          editdeletedarray.push(parseInt(idtopush));
            }
			
            function generatearray(changedelement,mode)
            {

                var id=changedelement.id;
                var split=id.split("_");
                var length=split.length;
                var takenidcount=parseInt(length-1);

               var text=$("#"+id+" option:selected").text();
              
                if(mode==0)
                {
                    
                    productelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    productelementname.splice(split[takenidcount], 1,String(text));

               
                }
                else if(mode==1)
                {
                    subcatelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    subcatelementname.splice(split[takenidcount], 1,String(text));
                  
                
                }
                else if(mode==2)
                {
                    skillelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    skillelementname.splice(split[takenidcount], 1,String(text));     
                }
                else
                {
                    worktypeelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    worktypeelementname.splice(split[takenidcount], 1,String(text));
                   
                  
                }
            }
            function editgeneratearray(changedelement,mode)
            {

                var id=changedelement.id;
                var split=id.split("_");
                var length=split.length;
                var takenidcount=parseInt(length-1);

               var text=$("#"+id+" option:selected").text();
              
                if(mode==0)
                {
                    
                    editproductelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    editproductelementname.splice(split[takenidcount], 1,String(text));

               
                }
                else if(mode==1)
                {
                    editsubcatelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    editsubcatelementname.splice(split[takenidcount], 1,String(text));
                  
                
                }
                else if(mode==2)
                {
                    editskillelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    editskillelementname.splice(split[takenidcount], 1,String(text));     
                }
                else
                {
                    editworktypeelementvalue.splice(split[takenidcount], 1,String(changedelement.value));
                    editworktypeelementname.splice(split[takenidcount], 1,String(text));
                   
                  
                }
            }
			function editconfirmshowtech()
			{
			editfinalproductelementvalue=editproductelementvalue;
           	editfinalproductelementname=editproductelementname;
           	editfinalsubcatelementvalue=editsubcatelementvalue;
            editfinalsubcatelementname=editsubcatelementname;
            editfinalskillelementvalue=editskillelementvalue;
            editfinalskillelementname=editskillelementname;
            editfinalworktypeelementvalue=editworktypeelementvalue;
            editfinalworktypeelementname=editworktypeelementname;
                if((editproductelementvalue.length>0) && (editdeletedarray.length>0))
                {
                   
                  for(var q=0;q<editdeletedarray.length;q++)
                  {
                   
                    var index=parseInt(editdeletedarray[q]);
                    delete editfinalproductelementvalue[index];
                    delete editfinalproductelementname[index];
                    delete editfinalsubcatelementvalue[index];
                    delete editfinalsubcatelementname[index];
                    delete editfinalskillelementvalue[index];
                    delete editfinalskillelementname[index];
                    delete editfinalworktypeelementvalue[index];
                    delete editfinalworktypeelementname[index];
                  }  
                }
                if(editproductelementvalue.length>0)
                {
                  // alert('hello');
                    $('#editshowservicegroup').html("");
                    var displaycode="<h4 class='form-section'>Skill details</h4>";
                    for(var t=0;t<editfinalproductelementvalue.length;t++)
                    {
                        if(editfinalproductelementvalue[t]!='' && editfinalproductelementvalue[t]!=null && editfinalproductelementvalue[t]!='undefined')
                        {
                        displaycode=displaycode+"<div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Product Category:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+editfinalproductelementname[t]+"</label></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Sub Category:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+editfinalsubcatelementname[t]+"</label></div></div></div>";
                        displaycode=displaycode+"<div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Skill Level:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+editfinalskillelementname[t]+"</label></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Work Type:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+editfinalworktypeelementname[t]+"</label></div></div></div>";
                        displaycode=displaycode+"<hr>";
                    }
                }
                    $('#editshowservicegroup').html(displaycode);
                    
                }
			}
            function confirmshowtech()
            {
               finalproductelementvalue=productelementvalue;
           finalproductelementname=productelementname;
           finalsubcatelementvalue=subcatelementvalue;
            finalsubcatelementname=subcatelementname;
            finalskillelementvalue=skillelementvalue;
            finalskillelementname=skillelementname;
            finalworktypeelementvalue=worktypeelementvalue;
            finalworktypeelementname=worktypeelementname;
                if((productelementvalue.length>0) && (deletedarray.length>0))
                {
                   
                  for(var q=0;q<deletedarray.length;q++)
                  {
                   
                    var index=parseInt(deletedarray[q]);
                    delete finalproductelementvalue[index];
                    delete finalproductelementname[index];
                    delete finalsubcatelementvalue[index];
                    delete finalsubcatelementname[index];
                    delete finalskillelementvalue[index];
                    delete finalskillelementname[index];
                    delete finalworktypeelementvalue[index];
                    delete finalworktypeelementname[index];
                  }  
                }
                if(productelementvalue.length>0)
                {
                   
                    $('#showservicegroup').html("");
                    var displaycode="<h4 class='form-section'>Skill details</h4>";
                    for(var t=0;t<finalproductelementvalue.length;t++)
                    for(var t=0;t<finalproductelementvalue.length;t++)
                    {
                        if(finalproductelementvalue[t]!='' && finalproductelementvalue[t]!=null && finalproductelementvalue[t]!='undefined')
                        {
                        displaycode=displaycode+"<div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Product Category:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+finalproductelementname[t]+"</label></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Sub Category:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+finalsubcatelementname[t]+"</label></div></div></div>";
                        displaycode=displaycode+"<div class='row'><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Skill Level:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+finalskillelementname[t]+"</label></div></div><div class='form-group col-md-6 col-sm-12'><label class='control-label col-md-5'>Work Type:</label><div class='col-md-7'><label class='form-control showworkcategory h4'>"+finalworktypeelementname[t]+"</label></div></div></div>";
                        displaycode=displaycode+"<hr>";
                    }
                }
                    $('#showservicegroup').html(displaycode);
                    
                }
              
            }
     
            function view_product(id){
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_productcategory",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                    var data=JSON.parse(data);
                                      
                                        $('#tbody_contract').html('');
                                        if(data.length<1){
                                            swal("Oops!", "No Current Product Category is Available.", "warning")
                                        }
                                        else {
                                            $('#modal_contract').modal('show');
                                            for(i=0;i<data.length;i++){
                                                $('#tbody_contract').append("<tr><td>"+data[i].product_name+"</td><td>"+data[i].cat_name+"</td><td style='text-align:center'>"+data[i].skill_level+"</td></tr>");
                                            }
                                        }
                                },
                });
            }
        </script>
    </body>

</html>
