<!DOCTYPE html>
<html lang="en">
   <!--<![endif]-->
   <!-- BEGIN HEAD -->
   <head>
      <?php
         $company_id = $this->session->userdata('companyid');
         ?>
      <?php
         $company_id=$this->session->userdata('companyid');
         $region=$user['region'];$area=$user['area'];$location=$user['location'];
         include 'assets/lib/cssscript.php';
         ?>   
      <style>
         #errmsg
         {
         color: red;
         }  
         #errmsg1
         {
         color: red;
         }
        .error{          
            color: red;      
         }    
         .fa-big{
         font-size: 16px !important;
         }
         #span-button:hover
         { cursor: pointer !important;}
         .ui-autocomplete{
         z-index:999999 !important;
         }span.fileinput-new {
         color: #000 !important;
         }
         .datepicker > .datepicker_header > .icon-home {
    display: none !important;
}
.datepicker > .datepicker_header > .icon-close {
     display: none !important;
}
      </style>
   </head>
   <!-- END HEAD -->
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
      <div class="page-wrapper">
         <!-- BEGIN HEADER -->
         <?php
            include "assets/lib/header_service.php";
            ?>
         <!-- END HEADER -->
         <div class="page-container">
         <div class="page-sidebar-wrapper">
         <?php include "assets/lib/service_sidebar.php"?>
         </div>
         <div class="page-content-wrapper">
                  <div class="page-content">
          
               <div class="tab-pane" id="tab_2">
                  <div class="portlet box dark">
                     <div class="portlet-title">
                        <div class="caption">
                           Raise Ticket
                        </div>
                        <div class="tools">
                           <a href="javascript:;" class="reload"> </a>
                           <!--<a href="javascript:;" class="remove"> </a>-->
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" id="form_sample_3">
                           <div class="form-body">
                              <h3 class="form-section">Customer Info</h3>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4" style="padding-left: 0px !important;">
                                       Contact Number
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-phone"></i>
                                             </span>
                                             <input type="text" name="contact_num" id="contact_num" data-required="1" class="form-control"  placeholder="Enter Contact Number to Search"/> 
                                             <!-- <span class="help-block"> This is inline help </span>-->
                                             <span class="input-group-btn">
                                             <button class="btn blue" type="button" id="search">Go!</button>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Customer ID
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" id="cust_id" name="cust_id" required readonly/>
                                          <!--<span class="help-block"> Select your gender. </span>-->
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Name
                                       <span class="required"> * </span></label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="cname" id="cname" data-required="1" readonly/>
                                          <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Email Address
                                       <span class="required" readonly> * </span></label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-envelope"></i>
                                             </span>
                                             <input type="text" class="form-control" name="e_mail" id="e_mail" placeholder="Email Address" readonly/>                                   
                                          </div>
                                          <!--<span class="help-block"> Select your gender. </span>-->
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <div class="row">
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4" style="padding-left: 0px !important;">
                                       Alternate Number
                                       </label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-phone"></i>
                                             </span>
                                             <input type="text" class="form-control" name="altnum" id="altnum" readonly/>
                                          </div>
                                          <!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">--> 
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <!-- New Section-->
                              <h3 class="form-section">Address</h3>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Door/ Plot no.</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="door" id="door" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Street/ Locality</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="street" id="street" data-required="1" readonly/>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Town</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="town" id="town" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Land mark</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="land_mark" id="land_mark" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">City</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="city" id="city" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">State</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="state" id="state" data-required="1"/ readonly>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Country</label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-globe"></i>
                                             </span>
                                             <input type="text" class="form-control" id="country" name="country"/ readonly>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-4">Post Code</label>
                                       <div class="col-md-8">
                                          <div class="input-group">
                                             <span class="input-group-addon">
                                             <i class="fa fa-map-marker"></i>
                                             </span>
                                             <input type="text" class="form-control" id="pin" name="pin"/ readonly>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--/span-->
                                 <!--/span-->
                              </div>
                              <!--/row-->
                              <h3 class="form-section" id="contract_div" style="display:none;">Contract Info</h3>
                              <div class="row">
                                 <div class="table-responsive" id="prod_table" style="display:none;">
                                    <table id="datatable11" class="table table-hover ">
                                       <thead>
                                          <tr>
                                             <th>Product Category</th>
                                             <th>Sub-Category</th>
                                             <th>Contract Type</th>
                                             <th>Expiry Date</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>
                                       <tbody id="dynamic_table">
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <!--/row-->
                           </div>
                           <div class="form-actions">
                              <div class="row">
                                 <div class="col-md-6 pull-right">
                                    <button type="reset" id="clear_contract" class="btn red-haze btn-outline btn-circle btn-md">Reset</button>
                                 </div>
                              </div>
                           </div>
                     </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
     </div>
         <!--container-->
         <!-- BEGIN FOOTER -->
         <?php
            include "assets/lib/footer.php";
            ?>
         <!-- END FOOTER -->
      </div>
      <!-- Modal Starts-->
      <div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header" style="border:0">
                  <div class="error" style="display:none">
                     <label id="rowdata"></label>
                  </div>
               </div>
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_1" style="margin-bottom:0">
                     <div class="portlet-title" style="margin-top: -27px !important;">
                        <div class="actions">
                           <button type="button" class="close" data-dismiss="modal" style="margin-top: 20px !important;">&times;</button>   
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                                 <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                       <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                       <span class="number"> 1 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i> Customer Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab2" data-toggle="tab" class="step">
                                       <span class="number"> 2 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Product Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab3" data-toggle="tab" class="step">
                                       <span class="number"> 3 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Confirm Details</span>
                                       </a>
                                    </li>
                                 </ul>
                                 <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                    <div class="progress-bar progress-bar-success" style="width: 20%;"> </div>
                                 </div>
                                 <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                       <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <!--<div class="alert alert-success display-none">
                                       <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>-->
                                    <div class="tab-pane active" id="tab1">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Ticket ID
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="tick_id" id="tick_id" class="form-control form-control1" readonly required /> 
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer ID
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cu_id" id="cu_id" class="form-control form-control1" required readonly/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Name
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="name" id="name" class="form-control form-control1" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Contact Number
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="number" id="number" class="form-control form-control1" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Email Id
                                                <span class="required" aria-required="true"> * </span>  </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="email" id="email" class="form-control form-control1" required readonly/> 
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">
                                                Alternate-Number </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="anum" id="anum" class="form-control form-control1" />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Door/ Plot no.
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="doornum" id="doornum" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Street/ Locality
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="address" id="address" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Town
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cus_town" id="cus_town" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Land mark 
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="land_mrk" id="land_mrk" class="form-control form-control1"/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">City
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="addr" id="addr" class="form-control form-control1" required />
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Country
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <!-- <input type="text" name="ccountry" id="ccountry" class="form-control form-control1" required /> -->
                                                   <select class="form-control form-control1" id="ccountry" name="ccountry" required>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <!--/row-->
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">State
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <!-- <input type="text" name="cstate" id="cstate" class="form-control form-control1" required />-->
                                                   <select class="form-control form-control1" id="cstate" name="cstate" required>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Post Code
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" id="postcode" name="postcode" class="form-control form-control1" required /> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Provide Product Info</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_prodname" id="cust_prodname" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_catname" id="cust_catname" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_prod" id="cust_prod" class="form-control form-control1" required /> 
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="cust_cat" id="cust_cat" class="form-control form-control1" required /> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Model No.
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="model_no" name="model_no" required readonly/>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contract Type
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" name="contract_type" id="contract_type" class="form-control form-control1" required readonly/> 
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Serial No.
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="s_no" name="s_no">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Call Category
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <!--<input type="text" name="call_tag" id="call_tag" class="form-control form-control1" required /> -->
                                                <select class="form-control" name="call_tag" id="call_tag" required>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5" style="padding-left: 0% !important;">Preferred Date-time
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-7">
                                                <input type="text" size="16" required class="form-control" id="preferred_date" name="preferred_date">
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Upload Image
                                             </label>
                                             <div class="col-md-3">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                   <div class="input-group input-small">
                                                      <div class="form-control uneditable-input1 input-fixed input-small2" data-trigger="fileinput">
                                                         <!--<i class="fa fa-file fileinput-exists"></i>&nbsp;-->
                                                         <span class="fileinput-filename"> </span>
                                                      </div>
                                                      <span class="input-group-addon btn default btn-file">
                                                      <span class="fileinput-new"> Select file </span>
                                                      <span class="fileinput-exists"> Change </span>
                                                      <input type="file" id="upload_image" name="upload_image"> </span>
                                                      <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--<div class="row">
                                          <div class="form-group col-md-12 col-sm-12">
                                                 <label class="control-label col-md-3" >Problem Description
                                                     <span class="required" aria-required="true"> * </span>
                                                 </label>
                                                 <div class="col-md-9" style="margin-left: -30px !important;">
                                                   <textarea id="prob_desc" name="prob_desc" rows="3" cols="73" style="resize:none;"></textarea>
                                                 </div>
                                             </div>
                                          </div>-->
                                          <div class="row">
                                          <div class="form-group col-md-6 col-sm-12 products">
                                                            <label class="control-label col-md-5">Work Type
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <div class="col-md-7">
                                                                 <select name="work_type" id="work_type" class="form-control form-control1" tabindex="-1" aria-hidden="true" required >
                                                                   <option value="" selected disabled>Select Work Type</option>
                                                                   <option value="1">Installation</option>
                                                                   <option value="2">Maintenance</option>
                                                                   <option value="3">Break down</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                          </div>
                                       <div class="row">
                                          <div class="form-group col-md-12 col-sm-12">
                                             <label class="control-label col-md-3" style="text-align: left;margin-left: -11px !important;">Problem Description
                                             <span class="required" aria-required="true"> * </span>
                                             </label>
                                             <div class="col-md-9" style="margin-left: -30px !important;">
                                                <textarea id="prob_desc" name="prob_desc" rows="3" cols="73" style="resize:none;" required></textarea>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php
                                                   echo $this->session->userdata('companyid');
                                                   ?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Customer details</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Ticket ID:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="tick_id"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Name:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="name"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Email ID:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="email"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contact Number:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="number"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Product Info</h3>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Product-Category:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="cust_prodname"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Sub-Category:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="cust_catname"> </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Model No:</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="model_no"> </p>
                                             </div>
                                          </div>
                                          <div class="form-group col-md-6 col-sm-12">
                                             <label class="control-label col-md-5">Contract Type :</label>
                                             <div class="col-md-7">
                                                <p class="form-control-static" data-display="contract_type"> </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-actions" style="border-top:1px solid #ddd !important">
                              <div class="row">
                                 <div class="col-md-offset-5 col-md-7">
                                    <a href="javascript:;" class="btn btn-circle red btn-outline button-previous disabled" style="display: none;">
                                    <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-circle blue btn-outline button-next"> Continue
                                    <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-circle green btn-outline button-submit" style="display: none;" id="raise_buttons"> Submit
                                    <i class="fa fa-check"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                     </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--/end Modal-->
      <div id="myModal3" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body">
                  <div class="portlet light bordered" id="form_wizard_3" style="margin-bottom:0">
                     <div class="error" style="display:none">
                        <label id="rowdata_c"></label>
                     </div>
                     <div class="portlet-title">
                        <!--<div class="caption">
                           <i class=" icon-layers font-red"></i>
                           <span class="caption-subject font-red bold uppercase">Add User
                               <span class="step-title"> Step 1 of 2 </span>
                           </span>
                           </div>-->
                        <div class="actions">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>   
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form class="form-horizontal" action="#" id="submit_form_2" method="POST" novalidate="novalidate">
                           <div class="form-wizard">
                              <div class="form-body">
                                 <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                       <a href="#tab7" data-toggle="tab" class="step" aria-expanded="true">
                                       <span class="number"> 1 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i> Customer Info</span>
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#tab8" data-toggle="tab" class="step">
                                       <span class="number"> 2 </span>
                                       <span class="desc">
                                       <i class="fa fa-check"></i>Product Info</span>
                                       </a>
                                    </li>
                                 </ul>
                                 <div id="bar" class="progress progress-striped" role="progressbar" style="height:10px !important">
                                    <div class="progress-bar progress-bar-success" style="width: 50%;"> </div>
                                 </div>
                                 <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                       <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-none">
                                       <button class="close" data-dismiss="alert"></button> Your form validation is successful! 
                                    </div>
                                    <div class="tab-pane active" id="tab7" style="font-size: 13px;">
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important"></h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer ID
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="custid" id="custid" data-required="1" class="form-control" required readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Customer Name
                                                <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="cusname" id="cusname" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Email Id
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="email" name="email" id="email" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">Contact Number
                                                <span class="required" aria-required="true"> * </span> </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="con_number" id="con_number" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4" style="padding-left: 0% !important;">
                                                Alternate-Number </label>
                                                <div class="col-md-8">
                                                   <input type="text" name="alter_num" id="alter_num" data-required="1" class="form-control" />
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <h3 class="block" style="border-bottom:1px solid #ddd !important">Address Details</h3>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Door/ Plot no.<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="plot_no" id="plot_no" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Street/ Locality<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="street_name" id="street_name" data-required="1" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Town<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="c_town" id="c_town" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Land mark</label>
                                                <div class="col-md-8">
                                                   <input type="text" name="c_landmark" id="c_landmark" data-required="1" class="form-control"/>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">City<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <input type="text" name="city_name" id="city_name" data-required="1" class="form-control" required/> 
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Country<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="fa fa-globe"></i>
                                                      </span>
                                                      <select name="country_name" id="country_name" class="form-control" required> 
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                       </div>
                                       <!--/row-->
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">State<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <select name="state_name" id="state_name" data-required="1" class="form-control" required></select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="control-label col-md-4">Post Code<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                   <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="fa fa-map-marker"></i>
                                                      </span>
                                                      <input type="text" name="pin_num" id="pin_num" class="form-control" required/> 
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--/span-->
                                          <!--/span-->
                                       </div>
                                       <div class="row" style="display:none">
                                          <div class="form-group col-md-6 col-sm-12">
                                             <div class="col-md-7">
                                                <input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php
                                                   echo $this->session->userdata('companyid');
                                                   ?>" readonly>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane" id="tab8">
                                       <div class="row" style="border-bottom:1px solid #e2e2e2;margin-bottom: 2%;line-height: 0;">
                                          <div class="form-group col-md-10">
                                             <h3 class="block" style=" margin-left: 2% !important;">Provide Product Info</h3>
                                          </div>
                                     