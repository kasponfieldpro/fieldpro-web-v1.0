<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include 'assets/lib/cssscript.php'?>
<style>
	#sample_3_filter{
		text-align:right;
	}
	.sweet-alert.showSweetAlert.visible {
		  z-index: 999999999 !important;
		  border:1px solid red;
	}
	.fileinput-new{
		 color:#000 !important;
	}
  /* .dataTables_filter, .dataTables_paginate.paging_bootstrap_number
  {
    text-align-last: right;
  } */
  .uneditable-input {
min-width:auto !important;
}
.portlet {
    margin-bottom: 10px;
	}
.form-control.uneditable-input.input-fixed {
    max-width: 233px !important;
}
@media screen and (max-width: 540px) {
.form-control.uneditable-input.input-fixed {
    max-width: 163px !important;
}
}
</style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <!-- BEGIN CONTAINER -->
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "assets/lib/header.php" ?>
            <!-- END HEADER -->
          <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/admin_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
						        <div class="portlet box dark">
									<div class="portlet-title">
										<div class="caption">Inventory Management</div>
									</div>
									<div class="portlet-body">
										<div class="portlet light bordered">
                                            <div class="">
                                                <div class="portlet light bordered">
                                                        <div class="table=responsive">
                                                            <form class="form-horizontal" id="add_location" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label col-sm-5" for="email">Spare Location :<span class="required">  </span></label>
                                                                    <div class="col-sm-3">
                                                                        <select class="form-control" id="spare_loc" name="spare_loc">
                                                                            <option selected disabled>Select Spare Location</option>
                                                                           <?php 
if($spare_loc){
                                                                                foreach($spare_loc->result() as $row){
                                                                            ?>
                                                                                <option value="<?php echo $row->spare_location; ?>"> <?php echo $row->spare_location; ?> </option>
                                                                            <?php }}?> 
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-sm-5" for="email">Delivery Lead Time:</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" class="form-control" id="day_del" name="day_del" >
                                                                    </div>
                                                                </div>  

                                                                <div class="form-group" style='display:none'>
                                                                    <label class="control-label col-sm-5" for="email">id:</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" class="form-control" id="location_id" name="location_id" >
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" style='display:none'>
                                                                    <label class="control-label col-sm-5" for="email">company id:</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" class="form-control" id="companyids" name="companyids" value="<?php echo $this->session->userdata('companyid');?>" >
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-md-6 col-md-offset-5">
                                                                        <div class="btn-group">
                                                                            <button  type="button" id="sample_editable_1_new" class="btn btn-circle blue btn-outline" onClick="location_update()" > Update
                                                                                <i class="fa fa-upload"></i>
                                                                            </button>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                            </form>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <div class="portlet light bordered">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption">
                                                    <span class="caption-subject font-dark bold uppercase"></span>
                                                </div>

                                                <div class="portlet-body">
                                                    <div class="table-toolbar">
                                                        <div class="row">
                                                            <div class="pull-right">
                                                                <div class="btn-group">
                                                                    <button id="sample_editable_1_new" class="btn btn-circle dark btn-outline" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"> Bulk Upload
                                                                        <i class="fa fa-upload"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="btn-group">
                                                                               <button id="sample_editable_1_new" class="btn btn-circle red btn-outline" onClick="spareid_check()"> Add Fresh Spare
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                           
                                                        </div>
                                                    </div>                                                    

                                                    <div class="portlet light bordered">
                                                        <div class="portlet-title tabbable-line">
                                                            <ul class="nav nav-tabs">
                                                                <li class="active">
                                                                    <a href="#Centralised_Spare" data-toggle="tab">Centralised Spare</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#Local_Spare" data-toggle="tab">Local Spare </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#Impereset_Spare" data-toggle="tab">Imprest Spare</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="Centralised_Spare">
                                                                <div class="table=responsive">
                                                                    <table class="table table-hover table-bordered sample_2" id="">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class='text-center'>Spare_code</th>
                                                                                <th class='text-center'>Spare_name</th>
                                                                                <th class='text-center'>Product Category</th>
                                                                                <th class='text-center'>Product Model No</th>
                                                                                <th class='text-center'>Spare Image</th>
                                                                                <th class='text-center'>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody align="center">
                                                                            <?php
                                                                                foreach ($records->result() as $row) {
                                                                                    if($row->spare_source == "Centralised_Spare"){
                                                                                        ?>
                                                                                        <tr>
                                                                                           <td class="text-center">
                                                                                                <a id="<?php echo $row->spare_code; ?>" onClick="view_contract(this.id)"><?php echo $row->spare_code; ?>
                                                                                                </a>
                                                                                            </td>
                                                                                            <td class="text-center"><?php echo $row->spare_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->product_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->spare_modal; ?></td>
                                                                                            <td class="text-center"  onclick=viewimage("<?php echo $row->spare_code; ?>")>
                                                                                                <i class="fa fa-camera"> </i>
                                                                                            </td>
                                                                                            <td class="text-center">
                                                                                                <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->a; ?>" onClick="edit(this.id)"><i class="fa fa-edit"></i></button>
                                                                                                <button class="btn btn-circle red btn-outline  btn-icon-only" id="<?php echo $row->a; ?>" onClick="deletes(this.id)"><i class="fa fa-trash"></i></button>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="Local_Spare">
                                                                <div class="table=responsive">
                                                                    <table class="table table-hover table-bordered sample_2" id="">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Spare_code</th>
                                                                                <th class="text-center">Spare_name</th>
                                                                                <th class="text-center">Product Category</th>
                                                                                <th class="text-center">Product Model No</th>
                                                                                <th class="text-center">Spare Image</th>
                                                                                <th class='text-center' style="width:160px;">Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody align="center">
                                                                            <?php
                                                                                foreach ($records->result() as $row) {
                                                                                    if($row->spare_source == "Local_Spare"){
                                                                                        ?>
                                                                                        <tr>
                                                                                           <td class="text-center">
                                                                                                <a id="<?php echo $row->spare_code; ?>" onClick="view_contract(this.id)"><?php echo $row->spare_code; ?>
                                                                                                </a>
                                                                                            </td>
                                                                                            <td class="text-center"><?php echo $row->spare_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->product_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->spare_modal; ?></td>
                                                                                            <td class="text-center"  onclick=viewimage("<?php echo $row->spare_code; ?>")>
                                                                                                <i class="fa fa-camera"> </i>
                                                                                            </td>
                                                                                            <td class="text-center">
                                                                                                <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->a; ?>" onClick="edit(this.id)"><i class="fa fa-edit"></i></button>
                                                                                                <button class="btn btn-circle red btn-outline  btn-icon-only" id="<?php echo $row->a; ?>" onClick="deletes(this.id)"><i class="fa fa-trash"></i></button>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="Impereset_Spare">
                                                                <div class="table=responsive">
                                                                    <table class="table table-hover table-bordered sample_2" id="">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Spare_code</th>
                                                                                <th class='text-center'>Spare_name</th>
                                                                                <th class='text-center'>Technician ID</th>
                                                                                <th class='text-center'>Product Category</th>
                                                                                <th class='text-center'>Product Model No</th>
                                                                                <th class="text-center">Spare Image</th>
                                                                                <th class='text-center' style="width:160px;">Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody align="center">
                                                                            <?php
                                                                                foreach ($records->result() as $row) {
                                                                                    if($row->spare_source == "Impreset_spare"){
                                                                                        ?>
                                                                                        <tr>
                                                                                           <td class="text-center">
                                                                                                <a id="<?php echo $row->spare_code; ?>" onClick="view_contract(this.id)"><?php echo $row->spare_code; ?>
                                                                                                </a>
                                                                                            </td>
                                                                                            <td class="text-center"><?php echo $row->spare_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->tech_id; ?></td>
                                                                                            <td class="text-center"><?php echo $row->product_name; ?></td>
                                                                                            <td class="text-center"><?php echo $row->spare_modal; ?></td>
                                                                                            <td class="text-center"  onclick=viewimage("<?php echo $row->spare_code; ?>")>
                                                                                                <i class="fa fa-camera"> </i>
                                                                                            </td>
                                                                                            <td class="text-center">
                                                                                                <button class="btn btn-circle blue btn-outline btn-icon-only" id="<?php echo $row->a; ?>" onClick="edit(this.id)"><i class="fa fa-edit"></i></button>
                                                                                                <button class="btn btn-circle red btn-outline  btn-icon-only" id="<?php echo $row->a; ?>" onClick="deletes(this.id)"><i class="fa fa-trash"></i></button>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                </div>
                                            </div>
                                        </div>   
                                        </div>
									</div>
								</div>
								<!-- END EXAMPLE TABLE PORTLET-->
                            </div>
							<!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
                        </div>
    

        <!--Modal Starts-->

        <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Spare</h4>
                            <div class="error" style="display:none">
                                <label id="rowdata"></label>
                            </div>
                        </div>
                        <div class="modal-body" style="margin-right:40px">
                            <form class="form-horizontal" id="add_spare">
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Spare ID:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spareid" name="spareid" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Code:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_code" name="spare_code" placeholder="Spare Code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Name:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_name" name="spare_name" placeholder="Spare Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Desc:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="5" id="spare_desc" name="spare_desc" style="resize:none"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Location:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="spare_source" name="spare_source" onChange="changesparesource(this.value);">
                                            <option selected disabled>Select Spare Location</option>
                                            <option value='Centralised_Spare'>Centralised Spare</option>
                                            <option value='Local_Spare'>Local Spare</option>
                                            <option value='Impreset_spare'>Imprest Spare</option>
                                        </select>
                                        <!--<input type="text" class="form-control" id="spare_source" name="spare_source" placeholder="Spare Source">-->
                                    </div>
                                </div>
                                <div class="form-group" id="show_spareregion" style='display:none'>
                                    <label class="control-label col-sm-4" for="email">Region:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="spare_region" name="spare_region">
                                            <option value="" selected disabled>Select Region</option>
                                                                   <option value="north">North</option>
                                                                   <option value="south">South</option>
                                                                   <option value="east">East</option>
                                                                   <option value="west">West</option>
                                        </select>
                                        <!--<input type="text" class="form-control" id="spare_source" name="spare_source" placeholder="Spare Source">-->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Product Category:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="product" name="product">
                                            <option value="" selected disabled>Select product</option>
                                            <?php
                                                foreach ($record->result() as $row) {
                                                    ?>
                                                    <option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Sub Category:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="category" name="category">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Product Model No:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_modal" name="spare_modal" placeholder="Product Model No">
                                    </div>
                                </div>
                                <div class="form-group">
                                 <label class="control-label col-sm-4" for="email">Purchase Date:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" id="p_date" name="p_date" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Expiry Date:</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" id="ep_date" name="ep_date" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Price:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Quantity Purchased:<span class="required"> * </span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="q_purchase" name="q_purchase" placeholder="Quantity Purchased">
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Quantity Used:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="q_used" name="q_used" placeholder="Quantity Used">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Product:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="product" name="product" placeholder="Product">
                                    </div>
                                </div>-->
                                <!--<div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Quantity:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                                    </div>
                                </div>-->
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Company ID:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="company_id" name="company_id" placeholder="Company ID" value="<?php echo $this->session->userdata('companyid');?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Image:</label>
                                    <div class="col-sm-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists" style="color: black;"> Change </span>
                                                    <input name="spare_image" id="spare_image" type="file">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    <!-- <input type="file" class="form-control" id="spare_image" name="spare_image" /> -->
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-circle green btn-outline" id="addspare"><i class="fa fa-check"></i> Submit</button>
                        <button type="button" class="btn btn-circle red btn-outline" id="cancel_add"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="edits" class="modal fade" role="dialog" data-backdrop="static">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Spare</h4>
                            <div class="error" style="display:none">
                                <label id="rowdata_1"></label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="edit_spare">
                                <div class="form-group" style="display:none">
                                                <label class="control-label col-sm-4" for="email">Id:</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="id1" name="id1" readonly>
                                                </div>
                                            </div>
                                <div class="form-group"  style="display:none">
                                    <label class="control-label col-sm-4" for="email">Spare id:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spareid1" name="spareid1" readonly>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Product:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="product1" name="product1">
                                            <?php
                                                foreach ($record->result() as $row) {
                                                    ?>
                                                    <option value="<?php echo $row->product_id; ?>"><?php echo $row->product_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Category:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="category1" name="category1">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Code:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_code1" name="spare_code1" placeholder="" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_name1" name="spare_name1" placeholder="" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Spare Location:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_source1" name="spare_source1" placeholder="" readonly>
                                    </div>
                                </div>
                                 <div class="form-group" id="editspareregionshow">
                                    <label class="control-label col-sm-4" for="email">Region:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="edit_spareregion" name="edit_spareregion" placeholder="" readonly>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Price:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="price1" name="price1" placeholder="Price">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Quantity Purchased:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="q_purchase1" name="q_purchase1" placeholder="Quantity Purchased">
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Quantity Used:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="q_used1" name="q_used1" placeholder="Quantity Used">
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Product Modal No:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_modal1" name="spare_modal1" placeholder="Spare Modal" readonly>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Spare Desc:</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="5" id="spare_desc1" name="spare_desc1" style="resize:none"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Company ID:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="company_id1" name="company_id1" placeholder="Company ID" value="<?php echo $this->session->userdata('companyid');?>">
                                    </div>
                                </div>
                                <!--<div class="form-group" style="display:none">
                                    <label class="control-label col-sm-4" for="email">Quantity:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="quantity1" name="quantity1" placeholder="Quantity">
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-md-4">Spare Image</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> 
                                            <img src="" id="spare_update_image" alt="Update Spare Image" /> 
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="spare_image1" id='spare_image1'> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        <input type="text" class="form-control" id="spare_image2" name="spare_image2" style="display:none" />
                                    </div>
                                </div>
                                <!-- <div class="form-group" style="">
                                    <label class="control-label col-sm-4" for="email">Spare Image:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="spare_image2" name="spare_image2" style="display:none"/>
                                        <input type="file" class="form-control" id="spare_image1" name="spare_image1" />
                                    </div>
                                </div> -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-circle blue btn-outline" onClick="submit_spare()"><i class="fa fa-check"></i> Update </button>
                            <button type="button" class="btn btn-circle red btn-outline" id="cancel_edit" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
		         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bulk Upload</h4>
                    </div>
                    <div class="modal-body">
                    	<form action="#" id="forbulk_upload" class="form-horizontal form-bordered">
                            <div class="form-body row">
                            	<div class="form-group col-md-4 col-sm-12 text-center">
                                	<?php $fname='spare_upload.xlsx'; ?>
                                    <a class="btn btn-circle purple-sharp btn-outline sbold uppercase" href="<?php echo base_url(); ?>index.php?/controller_admin/download_sampletemplate/<?php echo $fname;?>">Sample Template</a>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="add_excel" id="add_excel"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-circle green btn-outline" id="bulkupload" name="bulkupload"><i class="fa fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" id="dismiss_modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

       	         <div id="image_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Image</h4>
                    </div>
                    <div class="modal-body">
                    	<form action="#" id="foruploading_image" class="form-horizontal form-bordered">
                            <div class="form-body">
								<div class="row">
                            	<div class="form-group col-md-4 col-sm-12 text-center">
                                	<label class="control-label">upload Image</label>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="add_image" id="add_image"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
							<div class="row" style="display:none">
                            	<div class="form-group col-md-4 col-sm-12 text-center">
                                	<label class="control-label">upload Image</label>
                                </div>
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" id="sparecode_id" name="sparecode_id"/>
<input type="text" class="form-control" id="sparecoderow_id" name="sparecoderow_id"/>
                                </div>
                              </div>
							</div>
							</div>
                         </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-circle dark btn-outline" id="save_image" name="save_image"><i class="fa fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-circle red btn-outline" id="backto_result">Close</button>
                    </div>
                </div>
            </div>
        </div>
		  <div id="add_confirm" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">

					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="add_subproduct">
							<div class="form-group">
								<div class="col-sm-12">
									<p id="spare_confirm"></p>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-circle red btn-outline" id="closeandreload" data-dismiss="modal"><i class="fa fa-times">
                                       </i> Close</button>
					</div>
				</div>
			</div>
		</div>

    <div id="modal_viewspare" class="modal fade" role="dialog" data-backdrop="static">
         	<div class="modal-dialog modal-lg">

               <div class="modal-content">

				   <div class="modal-body">
                  	<div class="portlet box blue-hoki" id="form_wizard_1" style="margin-bottom:0">
                         <div class="portlet-title">
							<div class="caption" style="color:#fff;">Contract Details</div>
							<div class="actions" style="padding-top: 14px;"><button type="button" class="close" data-dismiss="modal" style="color:#1e252b !important;">&times;</button>
                              </div>
							</div>
						 <div class="portlet-body form">
                            <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
                                <div class="form-wizard">
                                    <div class="form-body table-responsive"">
						<table class="table table-hover table-bordered" id="spare_table">
						<thead>
                                                    <tr>

                                                        <th style="text-align: center;">Spare Code</th>
                                                        <th style="text-align: center;">Product Category</th>
                                                        <th style="text-align: center;">Sub Category</th>
                                                        <th style="text-align: center;">Quanity Purchased</th>
                                                        <th style="text-align: center;">Quantity Used</th>
														<th style="text-align: center;">Purchase Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody_spare">
												</tbody>

											</table>
									</div>
										<div class="form-actions" style="border-top:1px solid #ddd !important">
													<button type="button" class="btn btn-circle red btn-outline pull-right" data-dismiss="modal">Close</button>
										</div>
								</div>
							</form>
						 </div>
					</div>
					</div>
				</div>
                         </div>
                   </div>

         <div id="viewimage" class="modal fade" role="dialog">
			  <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
					    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View Image</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-dialog" id="view_imagetech">
                       </div>
					</div>
			</div>
		  </div>
        </div>
        <!--Modal End-->

        <!-- END QUICK SIDEBAR -->
		<?php include 'assets/lib/javascript.php'?>
    <script>
      window.history.forward();
        $(document).ready(function() {
               $('.sample_2').DataTable({"order": []});
        });
    </script>
		<script>
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#spare_management').addClass('open');
		</script>
		<script>
	$("#q_purchase").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
   $("#price").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
 $("#price1").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
   $("#q_purchase1").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });
   $("#day_del").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Numbers Only").show().fadeOut("slow");
               return false;
    }
   });

            $("#product").change(function () {
                $('#category').empty();
                var product_id = $('#product').val();
                $.ajax({
                    url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                    type        :   "POST",
                    data        :   {'product_id' : product_id},
                    datatype    :   "JSON",
                    cache       :   false,
                    process     :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        //console.log(data);
                                        if(data == 1){
                                            $.dialogbox({
                                                type:'msg',
                                                content: 'No Category are entered, kindly add category to this product',
                                                closeBtn:true,
                                                btn:['Ok.'],
                                                call:[
                                                    function(){
                                                        $.dialogbox.close();
                                                        //window.location.reload();
                                                    }
                                                ]
                                            });
                                            //bootbox.alert("No Category are entered, kindly add category to this product");
                                        }else{
                                            console.log(data);
                                            for(i=0; i<data.length;i++){
                                                $('#category').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                            }
                                        }
                                    },
                })
            });
            $("#product1").change(function () {
                $('#category1').empty();
                var product_id = $('#product1').val();
                $.ajax({
                    url         :   "<?php echo base_url();?>index.php?/controller_admin/getcategory",
                    type        :   "POST",
                    data        :   {'product_id' : product_id},
                    datatype    :   "JSON",
                    cache       :   false,
                    process     :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        if(data == 1){
                                            $.dialogbox({
                                                type:'msg',
                                                content: 'No Category are entered, kindly add category to this product',
                                                closeBtn:true,
                                                btn:['Ok.'],
                                                call:[
                                                    function(){
                                                        $.dialogbox.close();
                                                        //window.location.reload();
                                                    }
                                                ]
                                            });
                                            //bootbox.alert("No Category are entered, kindly add category to this product");
                                        }else{
                                            console.log(data);
                                            for(i=0; i<data.length;i++){
                                                $('#category1').append('<option value="'+data[i].cat_id+'">'+data[i].cat_name+'</option>');
                                            }
                                        }
                                    },
                })
            });
            function spareid_check(){
                $.ajax({
                    url         :   "<?php echo base_url();?>index.php?/controller_admin/spareid_check",
                    type        :   "POST",
                    data        :   "",
                    cache       :   false,
                    success     :   function(data){
                                        $('#spareid').val($.trim(data));
                                        $('#myModal1').modal('show');
                                    },
                })
            };
			$("#cancel_edit").click(function() {
				$('#edits').modal('hide');
				window.location.reload();
			});

			function view_contract(id){
				var company_id=$("#company_id").val();
			//alert(id);
				$.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_sparecode",
                    type        :   "POST",
                    data        :   {'id':id,'company_id':company_id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
								        var data=JSON.parse(data);
                                        console.log(data);
										$('#tbody_spare').html('');
										if(data.length<1){
											swal("Oops!", "No Current Contracts Available.", "warning")
										}
										else {
										$('#modal_viewspare').modal('show');
										for(i=0;i<data.length;i++){
										$('#tbody_spare').append("<tr><td style='text-align: center;'>"+data[i].spare_code+"</td><td style='text-align: center;'>"+data[i].product_name+"</td><td style='text-align: center;'>"+data[i].cat_name+"</td><td style='text-align: center;'>"+data[i].quantity_purchased+"</td><td style='text-align: center;'>"+data[i].quantity_used+"</td><td style='text-align: center;'>"+data[i].p_date+"</td></tr>");
										}
									}
					},
				});


			}

         //   $('#dob').datepicker();
         //   $('#dob_1').datepicker();
			$("#cancel_add").click(function() {
				$("#myModal1").modal('hide');
				window.location.reload();
			});
            
            function changesparesource(value)
            {
               // alert(value);
                if(value=="Local_Spare")
                {
                    $('#show_spareregion').show();
                }
                else
                {
                   // alert('hello');
                     $("#show_spareregion")[0].selectedIndex = 0;
                   // $('#show_spareregion').val('');

                   // $('#show_spareregion').prop('selectedIndex',0);
                     $('#show_spareregion').hide();
                }
            }
            $('#addspare').click(function(){
                $('#rowdata').empty();
                var spareid = $("#spareid").val();
                var spare_code = $("#spare_code").val();
                var spare_name = $("#spare_name").val();
                var spare_source = $("#spare_source").val();
                var spare_region='';
                if(spare_source=="Local_Spare")
                {

                     spare_region=$("#spare_region").val();
                }
                var spare_modal = $("#spare_modal").val();
				var p_date = $("#p_date").val();
				 var ep_date = $("#ep_date").val();
                var spare_desc = $("#spare_desc").val();
                var price = $("#price").val();
                var q_purchase = $("#q_purchase").val();
                var q_used = $("#q_used").val();
                var product = $("#product option:selected").val();
                var category = $("#category option:selected").val();
                var quantity = $("#quantity").val();
                var company_id = $("#company_id").val();
                var spare = $('#spare_image').prop('files')[0];
                var today = new Date();
				if(spareid!="" && spare_code!="" && spare_name!="" && spare_source!="" && spare_modal!="" && p_date!="" && spare_desc!="" && price!="" && q_purchase!="" && q_used!="" && product!="" && category!="" && quantity!="")
				{
                   
                    
				if(spare)
				{
                var spare_image = $('#spare_image').val().toString().split('.').pop().toLowerCase();
                if($.inArray(spare_image, ['gif','jpg','png','jpeg']) !== -1){
                    var spare_image = $('#spare_image').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('spareid',spareid);
                    form_data.append('spare_code',spare_code);
                    form_data.append('spare_name',spare_name);
                    form_data.append('spare_modal',spare_modal);
					form_data.append('p_date',p_date);
                    form_data.append('ep_date',ep_date);
					form_data.append('spare_desc',spare_desc);
                    form_data.append('spare_source',spare_source);
                    form_data.append('spare_region',spare_region);
                    form_data.append('price',price);
                    form_data.append('q_purchase',q_purchase);
                    form_data.append('q_used',q_used);
                    form_data.append('product',product);
                    form_data.append('category',category);
                    form_data.append('quantity',quantity);
                    form_data.append('company_id',company_id);
                    form_data.append('spare_image',spare_image);
                    var mypDate = new Date(p_date);
                    var myepDate = new Date(ep_date);
                    if (mypDate > today)
                    {         
                    swal("Purchase date can't be a future date");
                    return false;
                    }
                    else if (mypDate > myepDate)
                    { 
                    
                        swal('Invalid expiry date!')
                        return false;
                    }
                     if(spare_source=="Local_Spare" && (spare_region=='' || spare_region==null))
                    {
                        $('#rowdata').append("Fill all Mandatory Fields");
                    $('#myModal1').animate({ scrollTop: 0 });
                    $('.error').show();
                    return false;
                    }

                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insertspare",
                        type        :   "POST",
                        data        :   form_data,// {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType:false,
                        processData: false,
                        success     :   function(data){
                            data=$.trim(data);
                                            console.log(data);
                                            if(data == "Spare added Successfully"){
                                         swal({
									  title: "Good job!",
									  text: data,
									  type: "success",
									  confirmButtonClass: "btn-primary",
									  confirmButtonText: "Ok.",
									  closeOnConfirm: false,
									},
									function(isConfirm) {
									  if (isConfirm) {
										  $("#myModal1").modal('hide');
										  location.reload();
									 }
									});
                                          }
							    else{
                                                $('#rowdata').append(data);
                                                $('.error').show();
                                            }
                                        },
                    });
                }

				else{
					   //alert("no proper Image");
				 	  sweetAlert("Oops...", "Upload an Image file!", "warning");
                    }
			}
			else
				{

                    var form_data = new FormData();
                    form_data.append('spareid',spareid);
                    form_data.append('spare_code',spare_code);
                    form_data.append('spare_name',spare_name);
                    form_data.append('spare_modal',spare_modal);
					form_data.append('p_date',p_date);
                    form_data.append('ep_date',ep_date);
					form_data.append('spare_desc',spare_desc);
                    form_data.append('spare_source',spare_source);
                     form_data.append('spare_region',spare_region);
                    form_data.append('price',price);
                    form_data.append('q_purchase',q_purchase);
                    form_data.append('q_used',q_used);
                    form_data.append('product',product);
                    form_data.append('category',category);
                    form_data.append('quantity',quantity);
                    form_data.append('company_id',company_id);
                    form_data.append('spare_image',spare_image);
                    var mypDate = new Date(p_date);
                    var myepDate = new Date(ep_date);
                    if (mypDate > today)
                    { 
                 
                    swal("Purchase date can't be a future date");
                    return false;
                    }
                    else if (mypDate > myepDate)
                    { 
                    
                        swal('Invalid expiry date!')
                        return false;
                    }
                    if(spare_source=="Local_Spare" && (spare_region=='' || spare_region==null))
                    {
                        $('#rowdata').append("Fill all Mandatory Fields");
                    $('#myModal1').animate({ scrollTop: 0 });
                    $('.error').show();
                    return false;
                    }
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/insertspare1",
                        type        :   "POST",
                        data        :   form_data,// {action:'$funky'}
                        //datatype  :   "JSON",
                        contentType:false,
                        processData: false,
                        success     :   function(data){
                            data=$.trim(data);
                                            console.log(data);
                                            if(data == "Spare added Successfully"){
                                            swal({
									  title: "Good job!",
									  text: data,
									  type: "success",
									  confirmButtonClass: "btn-primary",
									  confirmButtonText: "Ok.",
									  closeOnConfirm: false,
									},
									function(isConfirm) {
									  if (isConfirm) {
										  $("#myModal1").modal('hide');
										  location.reload();
									 }
									});
                                   }
							 else{
                        $('#rowdata').append(data);
                        $('.error').show();
                  }
                                        },
                    });
                }

}
			

				else {
					$('#rowdata').append("Fill all Mandatory Fields");
			        $('#myModal1').animate({ scrollTop: 0 });
                    $('.error').show();
				}
         });

            $('#bulkupload').click(function(){
				var ext = $('#add_excel').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['xls', 'xlsx', 'csv']) == -1) {
                    swal('Please upload Excel file')

                }else {
                    var file_data = $('#add_excel').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('add_excel', file_data);
                    $.ajax({
                        type:'POST',
                        url:'<?php echo base_url(); ?>index.php?/controller_admin/bulk_spare',
                        contentType:false,
                        processData: false,
                        cache:false,
                        data:form_data,
                        success: function (data) {
                          //alert(data);
                           var result=JSON.parse(data);
							//var subStr;

								       $('#myModal').modal('hide');
								       $('#add_confirm').modal('show');
								       $('#spare_confirm').html('');
									console.log(result.length);
									for(i=0;i<result.length;i++){
										//if($("data:contains(successfully)"))
										var res=result[i].search("successfully");

										if(res==-1)
										{
									     	$('#spare_confirm').append("<div class='row' style='padding-left: 38px;'>"+result[i]+"</div><hr>");

										}
										else
										{
											var myStr=result[i];
											var subStr = myStr.match("Spare(.*)added successfully");
                                            //alert(subStr[1]);
											  var sdata=$.trim(subStr[1]);
									$('#spare_confirm').append("<div class='row'><div class='form-group col-md-8 col-sm-12' style='padding-left: 38px;'>"+result[i]+"</div><div class='form-group col-md-4 col-sm-12'id='"+sdata+"'><button type='button' id="+sdata+" class='btn blue btn-outline "+sdata+""+i+"' onclick=show_modal(this.id,"+i+");>Upload Image</button></div></div><hr>");
										}
									}

                       }
                });

			}
         })

		function show_modal(id,rowcount){
			                        var ans=id;
			                     $("#sparecode_id").val(ans);
								 $('#add_confirm').modal('hide');
								 $("#image_modal").modal('show');
                                 console.log("row "+rowcount);
		                        $("#sparecoderow_id").val(rowcount);
								 //alert(id);

							}
			$("#save_image").click(function (){
				var sp_image = $('#add_image').prop('files')[0];
				 var s_code=  $('#sparecode_id').val();
 var rowcountvalue=$('#sparecoderow_id').val();
				if(sp_image)
				{
				var ext = $('#add_image').val().toString().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['jpeg', 'jpg', 'png']) == -1) {
                    swal('Please upload Image file')

                }
				else {
                    var file_data = $('#add_image').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('add_image', file_data);
                    form_data.append('spar_code', s_code);
                    $.ajax({
                        type:'POST',
                        url:'<?php echo base_url(); ?>index.php?/controller_admin/insert_imageforspare',
                        contentType:false,
                        processData: false,
                        cache:false,
                        data:form_data,
                        success: function (data) {
                          //alert(data);
                          if(data=="Image added Successfully")
						  {
							  $("#image_modal").modal('hide');
							 swal({
											  title: data,
											  text: "Do you want to add image for other spares ?",
											  type: "success",
								 			  showCancelButton: true,
											  confirmButtonClass: "btn-primary",
											  confirmButtonText: "Yes",
								              cancelButtonText: " Cancel",
											  closeOnConfirm: false,
										},
										function(isConfirm) {
											  if (isConfirm) {
												  swal.close();
$('.'+s_code+''+rowcountvalue).prop("disabled", true);
												  $("#add_confirm").modal("show");
											 }
								              else {
												  window.location.reload();
											  }
										  });
						  }
					  }
				});
			}
		}
		else {
		   swal("No data to Submit!")
		}
		});

		$("#closeandreload").click(function() {
			window.location.reload();
		});

			$("#backto_result").click(function (){
				      $("#image_modal").modal('hide');
					  $('#add_confirm').modal('show');
				      $("#foruploading_image")[0].reset();

			});
			$("#dismiss_modal").click(function (){
				     // $("#image_modal").modal('hide');
					 window.location.reload();

			});

            function edit(id){
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/getdetails_spare",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        $('#id1').val(data['id']);
                                        $('#spareid1').val(data['spare_id']);
                                        $('#spare_code1').val(data['spare_code']);
                                        $('#spare_name1').val(data['spare_name']);
                                        $('#spare_modal1').val(data['spare_modal']);
                                        $('#spare_desc1').val(data['spare_desc']);
                                        $('#spare_source1').val(data['spare_source']);
                                        $('#price1').val(data['price']);
                                        $('#q_purchase1').val(data['quantity_purchased']);
                                        $('#company_id1').val(data['company_id']);
                                        //$('#quantity1').val(data['quantity']);
                                        $('#edit_spareregion').val(data['region']);
                                        if(data['spare_source']=="Local_Spare")
                                        {
                                            
                                            $('#editspareregionshow').show();
                                        }
                                        else
                                        {
                                            $('#editspareregionshow').hide();
                                        }
                                        $("#spare_image2").val(data['image']);
                                        $('#spare_update_image').attr("src",data['image']);
                                        $('select[name="product1"] option[value="'+data['product']+'"]').attr("selected",true);
                                        $.ajax({
                                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/data_category",
                                            type        :   "POST",
                                            data        :   {'id':data['category']},// {action:'$funky'}
                                            datatype    :   "JSON",
                                            cache       :   false,
                                            success     :   function(data){
                                                                var data=JSON.parse(data);
                                                                //console.log(data);
                                                                $('#category1').append('<option selected value="'+data['cat_id']+'">'+data['cat_name']+'</option>');
                                                                $('#edits').modal('show');
                                                            }
                                        });
                                    },
                });
            }
            function submit_spare(){
				var row_id=$('#id1').val();
            var sid= $('#spareid1').val();
            var pri= $('#price1').val();
            var qty= $('#q_purchase1').val();
            $.ajax({
            type:"POST",
            url:"<?php echo base_url();?>" + "index.php?/controller_admin/fetch_spareinfo",
            data:{'row_id':row_id,'sid':sid},
         	success:function(data){
         	 console.log(data);
               var ans=JSON.parse(data);
               //alert(ans['renewal_date']);
               //return false;
         if (pri ==ans['price'] && qty==ans['quantity_purchased'] && $('#spare_image1').val() == ""){
              swal("No Changes made!")
                  // alert ("No Changes");
                  //return false;
               }

		else
		{
                $('#rowdata_1').empty();
                var id1=$("#id1").val();
                var spareid1 = $("#spareid1").val();
                var spare_code1 = $("#spare_code1").val();
                var spare_name1 = $("#spare_name1").val();
                var spare_modal1 = $("#spare_modal1").val();
                var spare_desc1 = $("#spare_desc1").val();
                var spare_source1 = $("#spare_source1").val();
                var price1 = $("#price1").val();
                var q_purchase1 = $("#q_purchase1").val();
                var q_used1 = $("#q_used1").val();
                var product1 = $("#product1 option:selected").val();
                var category1 = $("#category1 option:selected").val();
                var quantity1 = $("#quantity1").val();
                var company_id1 = $("#company_id1").val();
                var spare_image2 = $("#spare_image2").val();
                var spare_image1 = $('#spare_image1').val().toString().split('.').pop().toLowerCase();
                if(spare_image1==""){
                    $.ajax({
                        url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edit_spare1",
                        type        :   "POST",
                        data        :   {'id1':id1,'spareid1':spareid1,'spare_code1':spare_code1,'spare_name1':spare_name1,'spare_modal1':spare_modal1,'spare_desc1':spare_desc1,'spare_source1':spare_source1,'price1':price1,'q_purchase1':q_purchase1,'product1':product1,'category1':category1,'quantity1':quantity1,'company_id1':company_id1},// {action:'$funky'}
                        //datatype  :   "JSON",
                        success     :   function(data){
                                            data=$.trim(data);
                                            if(data == "Spare updated Successfully"){
                                               //alert("Successfully");
											  swal({
												  title: data,
												  type: "success",
												  showCancelButton: false,
												  confirmButtonClass: "btn-danger",
												  confirmButtonText: "Ok!",
												  cancelButtonText: "No, cancel plx!",
												  closeOnConfirm: false,
												  closeOnCancel: false
												},
												function(isConfirm) {
												  if (isConfirm) {
													window.location.reload();
												  }
												});
                                            }else{
                                                $('#rowdata_1').append(data);
                                                $('.error').show();
                                            }
                                        },
                    });
                }else{
                    if($.inArray(spare_image1, ['gif','jpg','png']) !== -1){
                        var spare_image1 = $('#spare_image1').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('id1',id1);
                        form_data.append('spareid1',spareid1);
                        form_data.append('spare_code1',spare_code1);
                        form_data.append('spare_name1',spare_name1);
                        form_data.append('spare_modal1',spare_modal1);
                        form_data.append('spare_desc1',spare_desc1);
                        form_data.append('spare_source1',spare_source1);
                        form_data.append('price1',price1);
                        form_data.append('q_purchase1',q_purchase1);
                        form_data.append('product1',product1);
                        form_data.append('category1',category1);
                        form_data.append('quantity1',quantity1);
                        form_data.append('company_id1',company_id1);
                        form_data.append('spare_image1',spare_image1);
                        $.ajax({
                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/edit_spare",
                            type        :   "POST",
                            data        :   form_data,// {action:'$funky'}
                            //datatype  :   "JSON",
                            contentType:false,
                            processData: false,
                            success     :   function(data){
                                            data=$.trim(data);
                                            //console.log(data);
                                                if(data == "Spare updated Successfully"){
													swal({
														  title: data,
														  type: "success",
														  showCancelButton: false,
														  confirmButtonClass: "btn-danger",
														  confirmButtonText: "Ok!",
														  cancelButtonText: "No, cancel plx!",
														  closeOnConfirm: false,
														  closeOnCancel: false
														},
														function(isConfirm) {
														  if (isConfirm) {
															window.location.reload();
														  }
														});
                                                }else{
                                                    $('#rowdata_1').append(data);
                                                    $('.error').show();
                                                }
                                            },
                        });
                    }else{
                        alert("Image Should be in proper file");
                        //bootbox.alert('spare image should be a image file');
                    }
                }
		      }
            }
		});
	}
            function viewimage(id){
				// alert(id);
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/view_spare_images",
                    type        :   "POST",
                    data        :   {'id':id},// {action:'$funky'}
                    datatype    :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        console.log(data);
						               if(data['image']=="null")
									   {
										   $("#image_id").val("-");
										    //alert("empty");
										  $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<div id="textforimage"  style="height:200px;background: white;width:95%";><p style="text-align: center;padding-top: 82px;font-size: x-large;">No image available for this Spare</p></div>');
                                           $('#viewimage').modal('show');
									   }
						              else
									  {
										  //console.log(data['image']);
                                           $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<img src="'+data['image']+'" class="img-responsive" height="300px" width="95%">');
                                           $('#viewimage').modal('show');
									  }
                                    },
                });
            }

            function deletes(id){
            	swal({
            		  title: "Are you sure?",
            		  text: "You will not be able to recover this Spare Details",
            		  type: "warning",
            		  showCancelButton: true,
            		  confirmButtonClass: "btn-danger",
            		  confirmButtonText: "delete!",
            		  cancelButtonText: "No, Cancel!",
            		  closeOnConfirm: false,
            		  closeOnCancel: false
            		},
            		function(isConfirm) {
            		  if (isConfirm) {
            			  $.ajax({
                              url         :   "<?php echo base_url(); ?>index.php?/controller_admin/deletespare",
                              type        :   "POST",
                              data        :   {'id':id},// {action:'$funky'}
                              //datatype  :   "JSON",
                              cache       :   false,
                              success     :   function(data){
                                                  if(data == 1){
                                          		    //swal("Deleted!", "Your Spare has been deleted Successfully.", "success");
													  swal({
														  title: "Your Spare has been deleted Successfully",
														  type: "success",
														  showCancelButton: false,
														  confirmButtonClass: "btn-danger",
														  confirmButtonText: "Ok!",
														  cancelButtonText: "No, cancel plx!",
														  closeOnConfirm: false,
														  closeOnCancel: false
														},
														function(isConfirm) {
														  if (isConfirm) {
															window.location.reload();
														  }
														});
                                                  }else{
													   swal({
														  title: "Your Spare has not been deleted Successfully",
														  type: "success",
														  showCancelButton: false,
														  confirmButtonClass: "btn-danger",
														  confirmButtonText: "Ok!",
														  cancelButtonText: "No, cancel plx!",
														  closeOnConfirm: false,
														  closeOnCancel: false
														},
														function(isConfirm) {
														  if (isConfirm) {
															window.location.reload();
														  }
														});
                                                	  //swal("Cancelled", "Your Spare has not been deleted Successfully:", "error");
                                                  }
                              				}
                              })
            		  }else{
                    	  swal.close();
                      }
            		});
            }
            $('#spare_loc').change(function(){
                $('#day_del').val("");
                $('#location_id').val("");
                var selected_option=$("#spare_loc option:selected").val();
                $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_admin/get_spare_loc",
                    type        :   "POST",
                    data        :   {'id':selected_option},// {action:'$funky'}
                    datatype  :   "JSON",
                    cache       :   false,
                    success     :   function(data){
                                        //console.log(data);
                                        if(data != "null"){
                                            var data=JSON.parse(data);                                        
                                            $('#day_del').val(data.days_to_deliver);
                                            $('#location_id').val(data.id);
                                        }
                                    }
                });
            });
            function location_update(){
                var selected_option=$("#spare_loc option:selected").val();                
                var location_id = $('#location_id').val();
                var day_del = $('#day_del').val();
                var companyids = $('#companyids').val();
                if(selected_option == "Select Spare Location"){
                    swal('Spare location is mandatory to update');
                }else{
                    if(day_del == ""){
                        swal('Days to Deliver is mandatory to update');
                    }else{
                        $.ajax({
                            url         :   "<?php echo base_url(); ?>index.php?/controller_admin/update_spare_locations",
                            type        :   "POST",
                            data        :   {'select_option':selected_option,'location_id':location_id,'day_del':day_del,'companyids':companyids},// {action:'$funky'}
                            //datatype  :   "JSON",
                            cache       :   false,
                            success     :   function(data){
                                                console.log(data);
                                                if(data == '1'){
                                                    swal({
                                                        title: "Days to delivery is updated Successfully",
                                                        type: "success",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-danger",
                                                        confirmButtonText: "Ok!",
                                                        cancelButtonText: "No, cancel plx!",
                                                        closeOnConfirm: false,
                                                        closeOnCancel: false
                                                    },
                                                    function(isConfirm) {
                                                        if (isConfirm) {
                                                            window.location.reload();
                                                        }
                                                    });
                                                }else{
                                                    swal('Days to delivery is not updated, try again!');
                                                }
                                            }
                        });
                    }                    
                }                
            }
        </script>
    </body>
</html>