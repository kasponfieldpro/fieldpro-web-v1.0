<?php 
			$company_id=$this->session->userdata('companyid');
               $region=$user['region'];$area=$user['area'];$location=$user['location'];
			?>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal4" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Technician Details</h4>
                     </div>
                     <div class="modal-body">
                        <div class="portlet box blue">
                                 <div class="portlet-title">
                                    <div class="caption">
                                       ASSIGN TECHNICIAN
                                    </div>
                                 </div>
                                 <div class="portlet-body">
                                    <div class="table-responsive">
                                       <table class="table table-hover" id="sample_2">
                                          <thead>
                                             <tr>
                                                <th>Technician Id</th>
                                                <th>Technician Name</th>
                                                <th>Skill Level</th>
                                                <th>Tasks Present</th>
                                                <th>Current Location</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody id="tbody"></tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
						 <div class="form-group" style="display:none">
							 <input type="text" id="hidden_prod" name="hidden_prod">
							 <input type="text" id="hidden_cat" name="hidden_cat">
							 <input type="text" id="hidden_techid" name="hidden_techid">
                     </div>
					  </div>
                     <div class="modal-footer" style="padding: 15px !important;text-align: right !important;border-top: 1px solid #e5e5e5 !important;">
                       <button type="button" class="btn btn-default" onclick="reset_val()" data-dismiss="modal">Close</button>
                    </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
             <!--loading model-->
          <div class="modal" id="Searching_Modal" style="background: transparent !important;box-shadow: none !important;border: none !important;margin-top:8%;display: none;padding-right: 15px; position: fixed;" role="dialog" data-backdrop-limit="1">
     <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-sm vertical-align-center modal-dialog-loader">
<p class="text-center"> <img src="<?php echo base_url();?>/assets/global/img/37.gif" style="width: 100px;height:100px;" alt="Loading"></p>
    </div>
            </div>
  </div>
        <!-- end loading model-->
            <!-- END QUICK SIDEBAR -->
           
            <script type="text/javascript">
               
               function confirm1(technician_id,count,ticket_id,product_id,cat_id)
               {
$('#myModal4').modal('hide');
				   swal({
						  title: "Are you sure? You want to Assign",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Yes, Assign!",
						  cancelButtonText: "Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
						},
						function(isConfirm){
 if (isConfirm) {
							//var ticket_id=sessionStorage.getItem('ticket_id');
               var company_id="<?php echo $company_id;?>";
               $('#Searching_Modal').modal('show');
               $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_service/change_tech",
               type: 'POST',
               data: {'company_id':company_id,'technician_id':technician_id,'ticket_id':ticket_id,'count':count,'product_id':product_id,'cat_id':cat_id
               },
               success: function(data) {
                $('#Searching_Modal').modal('hide');

swal({
                                  title:  data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
									  var tech=$('#hidden_techid').val();
									  if(tech==''){
									  swal.close();
										  window.location.reload();
                                   	 // window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#tbody_unassigned"; 
									}
								  else{
										 swal.close();
									  window.location.reload();
                                    	window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#tbody_assigned"; 
									  }
								  }

                                });
						 // swal("Assigned!", data, "success");
               		}

					});
               }
		 else
			{
				swal.close();
				//$('#myModal4').modal('show');
			}
               });
               }
			 /* function confirm2(technician_id,count,ticket_id,product_id,cat_id)
               {
$('#myModal4').modal('hide');
				   swal({
						  title: "Are you sure? You want to Assign",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Yes, Assign him!",
						  cancelButtonText: "Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
						},
						function(isConfirm){
 if (isConfirm) {
							//var ticket_id=sessionStorage.getItem('ticket_id');
               var company_id="<?php echo $company_id;?>";
               $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_service/change_tech",
               type: 'POST',
               data: {'company_id':company_id,'technician_id':technician_id,'ticket_id':ticket_id,'count':count,'product_id':product_id,'cat_id':cat_id
               },
               success: function(data) {

swal({
                                  title:  data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
									  var tech=$('#hidden_techid').val();
									  if(tech==''){
									  swal.close();
										  window.location.reload();
                                   	 // window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#tbody_unassigned"; 
									}
								  else{
										 swal.close();
									  window.location.reload();
                                    	window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#tbody_assigned"; 
									  }
								  }

                                });
						 // swal("Assigned!", data, "success");
               		}

					});
               }
		 else
			{
				swal.close();
				//$('#myModal4').modal('show');
			}
               });
               } */
				
				function reset_val()
				{
					 $('#hidden_prod').val('');
				   $('#hidden_cat').val('');
				}
               
            </script>
