<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
               $company_id=$this->session->userdata('companyid');

include 'assets/lib/cssscript.php';?>
            <link href="assets/global/plugins/chart-graph/nv.d3.css" rel="stylesheet" type="text/css">
          
            <style>
               /* html, body, #chart1, svg {
               margin: 0px !important;
               padding: 0px !important;
               height: 100% !important;
               width: 100% !important;
               }*/
               .highcharts-button-symbol {
               display: none;
               }
               .highcharts-credits {
               display: none;
               }
               #preloader {
               position: fixed;
               top: 0;
               left: 0;
               right: 0;
               bottom: 0;
               background-color: #fff;
               /* change if the mask should have another color then white */
               z-index: 99;
               /* makes sure it stays on top */
               }
               #status {
               width: 200px;
               height: 200px;
               position: absolute;
               left: 50%;
               /* centers the loading animation horizontally one the screen */
               top: 50%;
               /* centers the loading animation vertically one the screen */
               background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
               /* path to your loading animation */
               background-repeat: no-repeat;
               background-position: center;
               margin: -100px 0 0 -100px;
               /* is width and height divided by two */
               }
               .highcharts-button {
               display: none;
               }
            </style>
            <style>
               .amcharts-chart-div a {
               display: none !important;
               }
               .piechart5 {
               margin: -12% 8%;
               }
               @media screen and (min-width:1600px) and (max-width:2100px)
               {
               .piechart5 {
               margin: -10% 20%;
               }	
               }
            </style>
         </head>
         <!-- END HEAD -->
         <body class="page-header-fixed page-sidebar-closed-hide-logo">
            <!-- BEGIN CONTAINER -->
            <div class="wrapper">
               <!-- BEGIN HEADER -->
              <?php 

    if($userrole =="Service_Manager")
      {
           include "assets/lib/manager_header.php";
         }
         else if ($userrole =="Service_Desk")
            {
               include "assets/lib/header_service.php";
            }
            else if($userrole =="Call-coordinator")
            {
               include "assets/lib/header_callcord.php";
            }
         else
            {
                include "assets/lib/header.php";
               }?>
               <!-- END HEADER -->
               <div class="container-fluid">
                  <div class="page-content">
    <div class="page-sidebar-wrapper">

            <?php 
if ($userrole =="Admin")
{
            include "assets/lib/admin_sidebar.php";
            }
            else if ($userrole =="Service_Desk")
            {
                include "assets/lib/service_sidebar.php";
            }
            ?>
            </div>
                     <!-- BEGIN BREADCRUMBS -->
                     <div class="breadcrumbs">
                     </div>
                     <!-- END BREADCRUMBS -->
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="portlet light bg-inverse col-md-12" style="height: 504px;">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">Current Tickets</span>
                                 </div>
                               
                              </div>
                              <!--<div style="height:360px">
                                 <svg id="test1" class="mypiechart"></svg>
                                 </div>-->
                                                        </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="portlet light bg-inverse col-md-12">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">Ticket Distribution</span>
                                 </div>
                                 <div class="actions">
                                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                    <i class="fa fa-pencil"></i> Export </a>
                                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                    <i class="fa fa-print"></i> Print </a>
                                 </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="call_distribution" onChange="call_distribution();" name="role1">
                                          <option value='today'>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' selected>This Year</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div id="chart1">
                                    <svg></svg>
                                 </div>
                                 <br><br><br>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="portlet light bg-inverse col-md-12">
                              <div class="portlet-title">
                                 <div class="caption font-black">
                                    <span class="caption-subject bold uppercase">C-SAT Insights(In Percentage)</span>
                                 </div>
                                 <div class="actions">
                                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                    <i class="fa fa-pencil"></i> Export </a>
                                    <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                    <i class="fa fa-print"></i> Print </a>
                                 </div>
                              </div>
                              <div class="portlet-body">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
                                       <select class="form-control" id="csat_insight" onChange="csat_insight();" name="role1">
                                          <option value='today'>Today</option>
                                          <option value='week'>This Week</option>
                                          <option value='month'>This Month</option>
                                          <option value='year' selected>This Year</option>
                                       </select>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-left">
                                       <select class="form-control" id="product_id" onChange="csat_insight();" name="role1">
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7  pull-right">
                                       <!--<div id="csat_insight_graph" style="height: 345px;  position: relative;" class="m-t-10"> </div>-->
                                       <div id="csat_insight_graph" style="min-width: 250px; max-width: 800px; height: 345px; margin: 0 auto"></div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5  pull-right">
                                       <div id="morris-rat-total" style="min-width: 250px; max-width: 800px; height: 345px; margin: 0 auto"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                      
                
                     </div>
                  </div>
                  <!-- BEGIN FOOTER -->
                  <?php include "assets/lib/footer.php"?>
                  <!-- END FOOTER -->
               </div>
            </div>
            <!--Modal Starts-->
            
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?> 
		
            
         </body>
      </html>
