<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
			$company_id=$this->session->userdata('companyid');
               $region=$user['region'];$area=$user['area'];$location=$user['location'];
include 'assign_tech.php';
include 'assets/lib/cssscript.php'?>
		<style>
		
        #errmsg1
        {
           color: red;
        }		
	.fa-big{
          font-size: 16px !important;
     }
    #span-button:hover
		{ 
			cursor: pointer !important;
		}
    
   .sweet-alert.showSweetAlert.visible{
	 	 z-index: 999999;
    	  border: 1px solid cadetblue;
	 	 margin-top: -118px !important;
    }
	 .dataTables_filter
 	 {
    	text-align: right;
 	 }
	.dt-buttons{
         display:none;
      }
     </style>
         </head>
         <!-- END HEAD -->
          <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_service.php"?>
               <!-- END HEADER -->
               <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/service_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->							  
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">NEW TICKETS </div>
									<ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#unassigned" data-toggle="tab">Unassigned Tickets</a>
                                             </li>
                                             <li>
                                                <a href="#assigned" data-toggle="tab">Assigned Tickets </a>
                                             </li>
                                             <li>
                                                <a href="#accepted" data-toggle="tab">Accepted Tickets </a>
                                             </li>
                                             <li>
                                                <a href="#deferred" data-toggle="tab">Deferred Tickets </a>
                                             </li>
                                          </ul>
                                 </div>
                                 <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="unassigned">
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location2" onChange="day2();" name="role1">
                                                         <option value="" selected>All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
												   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
        <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id2" onChange="day2();" name="role1">
                                                         <option value="" selected>All Product-Category</option>
                                                        
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">

                                                   <table class="table table-hover table-bordered datatable" id="">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Ticket Id</th>
							    <th style="text-align:center">Customer Name</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Raised Time</th>
                                                            <th style="text-align:center">Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_unassigned" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="assigned">
                                                
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location" onChange="day();" name="role1">
                                                         <option value="" selected>All Location</option>
                                                        
                                                      </select>
                                                   </div>
												   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id" onChange="day();" name="role1">
                                                         <option value="" selected>All Product-Category</option>
                                                        
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered datatable1" id="">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Ticket Id</th>
                                                            <th style="text-align:center">Customer Name</th>
                                                            <th style="text-align:center">Technician Id</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Assigned Time</th>
                                                            <th style="text-align:center">Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_assigned" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="accepted">
                                               
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location1"  onchange="day1();" name="role1">
                                                         <option value="" selected>All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id1" onChange="day1();" name="role1">
                                                         <option value="" selected>All Product-Category</option>
                                                        
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered datatable2" id="">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Ticket Id</th>
                                                            <th style="text-align:center">Customer Name</th>
                                                            <th style="text-align:center">Technician Id</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Accepted Time</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_accepted" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="deferred">
                                               
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location3"  onchange="day3();" name="role1">
                                                         <option value="" selected>All Location</option>
                                                       
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id3" onChange="day3();" name="role1">
                                                         <option value="" selected>All Product-Category</option>
                                                         
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered datatable3" id="">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Ticket Id</th>
							    <th style="text-align:center">Customer Name</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Preferred Date</th>
                                                            <th style="text-align:center">Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_deferred" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id='modal_head'>Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
						</form>
                     </div>
                     <div class="modal-footer" id="footer_formodal">
                       <!-- <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- END QUICK SIDEBAR -->
            
          <?php include 'assets/lib/javascript.php'?> 
  <script>                
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#new').addClass('open');
        </script>
			      <script type="text/javascript">
         $(document).ready(function() {
			//  $('.datatable').DataTable();
			 
		  $('.datatable').dataTable({
        "order": []
    }); $('.datatable1').dataTable({
        "order": []
    }); $('.datatable2').dataTable({
        "order": []
    }); $('.datatable3').dataTable({
        "order": []
    });
 var company_id="<?php echo $company_id;?>";
 var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";
  /*  $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'1'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0)
		 {
         $('#area').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
		 	$('#area').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'2'},
         dataType: "json",
         success: function(data) {
		  if(data.length>0)
		 {
         $('#area1').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
		 	$('#area1').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'0'},
         dataType: "json",
         success: function(data) {
		   if(data.length>0)
		 {
         $('#area2').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area2').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
		 	 $('#area2').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'0111'},
         dataType: "json",
         success: function(data) {
		   if(data.length>0) {
         $('#area3').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area3').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		  else{
		 	 $('#area3').html(' <option selected value="">No Results</option>');
		 }
		 }
         });*/

	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'1'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0){
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else
		 {
		 	$('#location').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'2'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0) {
         $('#location1').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		  else
		 {
		 	$('#location1').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0) {
         $('#location2').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location2').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		  else
		 {
		 	$('#location2').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0111'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0)
		 {
         $('#location3').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location3').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		  else
		 {
		 	$('#location3').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
			 
    $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'1'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0){
         	$('#product_id').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else{
		 	 $('#product_id').html('<option selected value="">No results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'2'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0){
         $('#product_id1').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#product_id1').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		else
		 {
		 	$('#product_id1').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'0'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0)
		 {
         $('#product_id2').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#product_id2').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else
		 {
		 	$('#product_id2').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'0111'},
         dataType: "json",
         success: function(data) {
		 if(data.length>0)
		 {
         $('#product_id3').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#product_id3').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else
		 {
		 	$('#product_id3').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
			 
	/*$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'1'},
         dataType: "json",
         success: function(data) {
		 var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0){
         $('#region').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		 else {
		 	$('#region').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
        $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'2'},
         dataType: "json",
         success: function(data) {
		  var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0){
         $('#region1').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#region1').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		  else {
		 	$('#region1').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'0'},
         dataType: "json",
         success: function(data) {
		  var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0){
         $('#region2').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#region2').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		  else {
		 	$('#region2').html(' <option selected value="">No Results</option>');
		 }
		 }
         });
		 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'0111'},
         dataType: "json",
         success: function(data) {
		  var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0){
         $('#region3').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#region3').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		  else {
		 	$('#region3').html(' <option selected value="">No Results</option>');
		 }
		 }
         });*/
			 
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var product_id1=$('#product_id1').val();
         var product_id2=$('#product_id2').val();
         var product_id3=$('#product_id3').val();
         var filter1=$('#day1').val();
         var filter2=$('#day2').val();
         var filter3=$('#day3').val();
		 var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
         var location=$('#location').val();
		 var region1="<?php echo $region;?>";
		var area1="<?php echo $area;?>";
         var location1=$('#location1').val();
		 var region2="<?php echo $region;?>";
		var area2="<?php echo $area;?>";
         var location2=$('#location2').val();
		 var region3="<?php echo $region;?>";
		var area3="<?php echo $area;?>";
         var location3=$('#location3').val();
			 
			 $("#tbody_unassigned").empty();
			  $('.datatable').DataTable().destroy();       
		
		$.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_unassigned",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter2,'product_id':product_id2,'region':region2,'area':area2,'location':location2},
         dataType: "json",
         success: function(data) {
        
         if(data.length<1)
         {
			  $("#tbody_unassigned").empty();
			  $('.datatable').DataTable().destroy();         
         //$('#tbody_unassigned').html('<trstyle="text-align:center"><td colspan=6>No records found</td></tr>');			 
         }
         else
         {
			  //$('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
          var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
           var work_type =work_type.replace(/ /g, ":");
           var replaceSpace=data[i].prob_desc;
           var problem = replaceSpace.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");		
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
           $('#tbody_unassigned').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].raised_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle " onclick=redirect(this.id,"'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","");>Assign</button></td></tr>');	
         
         }
			
			
         }
			  $('.datatable').DataTable({"order": []});
         }
         });
			 
	      $("#tbody_assigned").empty();
	      $('.datatable1').DataTable().destroy();
	 $.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_assigned",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
         $("#tbody_assigned").empty();
			   $('.datatable1').DataTable().destroy();
			 console.log(data);
         if(data.length<1)
         {
        $("#tbody_assigned").empty();
			  $('.datatable1').DataTable().destroy();
         }
			 
         else
         {
         for(i=0;i<data.length;i++)
         {
           var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");	
         	var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	 
			    var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");	
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");	
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");
         
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
			 
         	$('#tbody_assigned').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].assigned_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle " onclick=redirect(this.id,"'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","'+data[i].tech_id+'","'+data[i].work_type+'");>Re-assign</button></td></tr>');	
          
         }
       

         }
			   $('.datatable1').DataTable({"order": []});
         }
         });
		
          $("#tbody_accepted").empty();
			 $('.datatable2').DataTable().destroy(); 
	    $.ajax({
			 
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_accepted",
         type: 'POST',
		 data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region1,'area':area1,'location':location1},
         dataType: "json",
         success: function(data) {
         if(data.length<1)
         {
         //$('#tbody_accepted').html('<tr style="text-align:center"><td colspan=6>No records found</td></tr>');
			 $("#tbody_accepted").empty();
			 $('.datatable2').DataTable().destroy();
         }
         else
         {
         for(i=0;i<data.length;i++)
         {
           var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");	
          var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");	
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
          $('#tbody_accepted').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].accepted_time+'</td></tr>');        
         
         }		 

         }
			   $('.datatable2').DataTable({"order": []});
         }
         });
			 
		   $("#tbody_deferred").empty();
			 $('.datatable3').DataTable().destroy();

	$.ajax({
		
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_deferred",
         type: 'POST',
         data: {'company_id':company_id,'product_id':product_id3,'region':region3,'area':area3,'location':location3},
         dataType: "json",
         success: function(data) {
         if(data.length<1)
         {
			 $("#tbody_deferred").empty();
			 $('.datatable3').DataTable().destroy();         }
         else
         {
			  //$('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
          var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");
          var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
		    	var date= data[i].cust_preference_date.replace(/ /g, ",");	
         $('#tbody_deferred').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].cust_preference_date+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-circle btn-icon-only " onclick=modal(this.id,"'+date+'");><i class="fa fa-edit"></i></button><button id="'+data[i].ticket_id+'" class="btn red btn-outline btn-circle btn-icon-only" onclick=del(this.id);><i class="fa fa-times"></i></button></td></tr>');
         
         
         }
			 

         }
			  $('.datatable3').DataTable({"order": []});
         }
         });
         
            // $('.datatable').dataTable();
             $('#datatable-keytable').DataTable( { keys: true } );
             $('#datatable-responsive').DataTable();
             $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
             var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } ); 
         } );
         //TableManageButtons.init();
         function redirect(ticket_id,product_id,cat_id,location,tech_id,work_type)
         {
	var company_id="<?php echo $company_id;?>";
	var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
         $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_service/load_tech",
               type: 'POST',
               data: {'company_id':company_id,'product_id':product_id,'cat_id':cat_id,'region':region,'area':area,'location':location,'tech_id':tech_id,'work_type':work_type
               },
               dataType: "json",
               success: function(data) {
               $('#tbody').html('');
               console.log(data);
               if(data.length<1)
               {
               $('#tbody').html('<tr><td colspan=9>No Technicians Available</td></tr>');
               }
               else
               {
               if(tech_id==''){
               for(i=0;i<data.length;i++)
               {
               $('#tbody').append('<tr><td>'+data[i].employee_id+'</td><td>'+data[i].first_name+''+data[i].last_name+'</td><td>'+data[i].skill_level+'</td><td>'+data[i].today_task_count+'</td><td>'+data[i].current_location+'</td><td><button id="'+data[i].technician_id+'" class="btn blue btn-outline btn-circle btn-sm" onclick=confirm1(this.id,"'+data[i].today_task_count+'","'+ticket_id+'","'+product_id+'","'+cat_id+'");>Assign</button></td></tr>');	
				   $('#hidden_prod').val(product_id);
				   $('#hidden_cat').val(cat_id);
				   $('#hidden_techid').val(tech_id);
               }	
               }
               else{
               	
               for(i=0;i<data.length;i++)
               {
               $('#tbody').append('<tr><td>'+data[i].employee_id+'</td><td>'+data[i].first_name+''+data[i].last_name+'</td><td>'+data[i].skill_level+'</td><td>'+data[i].today_task_count+'</td><td>'+data[i].current_location+'</td><td><button id="'+data[i].technician_id+'" class="btn blue btn-outline btn-circle btn-sm" onclick=confirm1(this.id,"'+data[i].today_task_count+'","'+ticket_id+'","'+product_id+'","'+cat_id+'");>Re-assign</button></td></tr>');	
				   $('#hidden_prod').val(product_id);
				   $('#hidden_cat').val(cat_id);
				   $('#hidden_techid').val(tech_id);
               }
               }
               } 
			   $('#myModal4').modal('show');               }
});
         }
					  
         function hover(tech_id,technician_name,tech_email,contact_number,location,skill,task_count)
         {
location= location.replace(/\:/g," ");	
$('#modal_head').html('Technician Details');
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Technician ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+technician_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+tech_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Skill</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+skill+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Task Count</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+task_count+'</div></div></div>');
         $('#myModal').modal('show');
         }
					  
         function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority,call_tag,call_type,cust_contact,work_type)
         {
$('#modal_head').html('Ticket Details');
         var replaceSpace=problem;
         problem = problem.replace(/\:/g," ");	
         var product_name=product_name;
         product_name= product_name.replace(/\:/g," ");	
         var location=location;
         location= location.replace(/\:/g," ");	
         var cat_name=cat_name;
         cat_name= cat_name.replace(/\:/g," ");	
         var priority=priority;
         priority= priority.replace(/\:/g," ");
		call_tag= call_tag.replace(/\:/g," ");	
		call_type= call_type.replace(/\:/g," ");	
			 if(priority=='')
			 {
        $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Contact Number</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_contact+'</div></div><div class="form-group"> <label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"> <label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"> <label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div> <div class="form-group"> <label class="col-sm-3 col-sm-offset-2 control-label"for="worktype" >Work Type</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+work_type+'</div> </div>');
			 }
			 else{
        $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Contact Number</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_contact+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Priority</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+priority+'</div></div></div><div class="form-group"> <label class="col-sm-3 col-sm-offset-2 control-label"for="worktype" >Work Type</label> <div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+work_type+'</div> </div>');
			 }
         $('#myModal').modal('show');
				 
         }
					  
         function day()
         {
			  $('#tbody_assigned').empty();
			 $('.datatable1').DataTable().destroy();
         var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
         var location=$('#location').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_assigned",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
         if(data.length<1)
         {
       		  $('#tbody_assigned').empty();
			 $('.datatable1').DataTable().destroy();
		 }
         else
         {
			    $('#tbody_assigned').empty();
          //$('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
           var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
         	var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
			    var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");	
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");		
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         	$('#tbody_assigned').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].assigned_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle " onclick=redirect(this.id,"'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","'+data[i].tech_id+'","'+data[i].work_type+'");>Re-assign</button></td></tr>');	
          
         }


         } $('.datatable1').DataTable({"order": []});
         }
         });
         }
         function day1()
         {
			   $('#tbody_accepted').empty();
			   $('.datatable2').DataTable().destroy();
			 
         var company_id="<?php echo $company_id;?>";
         var filter1=$('#day1').val();
         var product_id=$('#product_id1').val();
		     var region="<?php echo $region;?>";
		     var area="<?php echo $area;?>";
         var location=$('#location1').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_accepted",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter1,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
       
         console.log(data);
         if(data.length<1)
         {
          $('#tbody_accepted').empty();
			 $('.datatable2').DataTable().destroy();
         }
         else
         {
			  $('#tbody_accepted').empty();
			 // $('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
          var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
         var customer_name = data[i].customer_name;
         var customer_name	= customer_name.replace(/ /g, ":");	
         var replaceSpace=data[i].prob_desc;
         var problem = replaceSpace.replace(/ /g, ":");	
         var product_name=data[i].product_name;
         var product_name= product_name.replace(/ /g, ":");	
         var cat_name=data[i].cat_name;
         var cat_name= cat_name.replace(/ /g, ":");	
         var location=data[i].location;
         var location= location.replace(/ /g, ":");
			   var priority=data[i].priority;
         var priority= priority.replace(/ /g, ":");	
         var tech_location=data[i].tech_loc;
         var tech_location= tech_location.replace(/ /g, ":");
         var call_tag=data[i].call_tag;
         var call_tag= call_tag.replace(/ /g, ":");
         var call_type=data[i].call_type;
         var call_type= call_type.replace(/ /g, ":");	
         var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         $('#tbody_accepted').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].accepted_time+'</td></tr>');	
         
         
         }
         }  $('.datatable2').DataTable({"order": []});
         }
         });
         }
		 function day2()
		 {
			 $('#tbody_unassigned').empty();
				  $('.datatable').DataTable().destroy();
			var company_id="<?php echo $company_id;?>";
			var filter2=$('#day2').val();
			var product_id2=$('#product_id2').val();
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			// alert(area);
			var location=$('#location2').val();
			 $.ajax({
			 url: "<?php echo base_url();?>" + "index.php?/controller_service/load_unassigned",
			 type: 'POST',
			 data: {'company_id':company_id,'filter':filter2,'product_id':product_id2,'region':region,'area':area,'location':location},
			 dataType: "json",
			 success: function(data) {
				// alert(data);
			 
			 console.log(data);
			 if(data.length<1)
			 {
				 $('#tbody_unassigned').empty();
				  $('.datatable').DataTable().destroy();
			 }
			 else
			 {
				  $('#tbody_unassigned').empty();
				  $('.datatable').DataTable().destroy();
			 for(i=0;i<data.length;i++)
			 {
        var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
        var work_type =work_type.replace(/ /g, ":");
			  var replaceSpace=data[i].prob_desc;
				var problem = replaceSpace.replace(/ /g, ":");	
			  var product_name=data[i].product_name;
				var product_name= product_name.replace(/ /g, ":");	
			  var cat_name=data[i].cat_name;
				var cat_name= cat_name.replace(/ /g, ":");	
			  var location=data[i].location;
				var location= location.replace(/ /g, ":");	
				var priority=data[i].priority;
				var priority= priority.replace(/ /g, ":");
        var call_tag=data[i].call_tag;
        var call_tag= call_tag.replace(/ /g, ":");
        var call_type=data[i].call_type;
        var call_type= call_type.replace(/ /g, ":");
        var customer_name = data[i].customer_name;
        var customer_name	= customer_name.replace(/ /g, ":");		
				var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
        $('#tbody_unassigned').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].raised_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle " onclick=redirect(this.id,"'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","");>Assign</button></td></tr>');	
			 
			 }
				
			 }
				  $('.datatable').DataTable({"order": []});
				  
			 }
			});
		 }
		 function day3()
		 {
			$("#tbody_deferred").empty();
			$('.datatable3').DataTable().destroy();
			var company_id="<?php echo $company_id;?>";
			var product_id3=$('#product_id3').val();
			var region="<?php echo $region;?>";
		  var area="<?php echo $area;?>";
			var location=$('#location3').val();			
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_deferred",
         type: 'POST',
         data: {'company_id':company_id,'product_id':product_id3,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
         if(data.length<1)
         {
		   	$("#tbody_deferred").empty();
			  $('.datatable3').DataTable().destroy();
         }
         else
         {
		   	$("#tbody_deferred").empty();
         for(i=0;i<data.length;i++)
         {
           var work_type = data[i].work_type;
           if(work_type == 1)
           {
             var work_type = "Installation";
           }
           else if(work_type ==2)
           {
             var work_type = "Maintanance";
           }
           else{
             var work_type = "Breakdown";
           }
          var customer_name = data[i].customer_name;
          var customer_name	= customer_name.replace(/ /g, ":");
          var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");	
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
			    var date= data[i].cust_preference_date.replace(/ /g, ",");
			 
          $('#tbody_deferred').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'","'+work_type+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].cust_preference_date+'</td><td style="text-align:center"><span class="btn-group btn-group-circle"> <button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle" onclick=modal(this.id,"'+date+'");>Update</button><button id="'+data[i].ticket_id+'" class="btn red btn-outline btn-sm btn-circle" onclick=del(this.id);>Delete</button></span></td></tr>');
         
         
         }
         }
			   $('.datatable3').DataTable({"order": []});
         }
         });
		 }
					  
		 function region()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'1'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
			 }
			 else{
				  $('#area').html(' <option selected value="">No results</option>');
			 }
         }
         });
		 }
		 function area()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'1'},
         dataType: "json",
         success: function(data) {
		if(data.length>0){
			$('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
		}
			 else{
				 $('#location').html('<option selected value="">No results</option>');
			 }
         }
         });
		 }
		 function region1()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'2'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area1').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
			 else{
				 $('#area1').html(' <option selected value="">No results</option>');
			 }
		 }
         });
		 }
		 function area1()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'2'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location1').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
			 else{
				  $('#location1').html(' <option selected value="">No results</option>');
			 }
		 }
         });
		 }
		 function region2()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area2').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area2').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
			 else{
				$('#area2').html(' <option selected value="">No results</option>'); 
			 }
		 }
         });
		 }
		 function area2()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location2').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location2').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
			 else{
				  $('#location2').html(' <option selected value="">No results</option>');
			 }
		 }
         });
		 }
		 function region3()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0111'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area3').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area3').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
			 else{
				  $('#area3').html(' <option selected value="">No results</option>');
			 }
		 }
         });
		 }
		 function area3()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
		var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'0111'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location3').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location3').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
			 else{
				  $('#location3').html(' <option selected value="">No results</option>');
			 }
		 }
         });
		 }
		 function modal(ticket_id,date)
         {
			date= date.replace(/\,/g," ");// date=new Date(date);
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-4 control-label"for="inputEmail3" readonly>Ticket ID</label><div class=" col-lg-offset-4 col-sm-4">'+ticket_id+'</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Preferred datetime Of Visit</label><div class=" col-sm-offset-2 col-sm-4"><input class="form-control" type="text" name="serv_date" parsley-trigger="change" required id="serv_date" style="margin-bottom:3%;" value="'+date+'"></div></div><div class="form-group" style="display:none;"><label  class="col-sm-5 control-label"for="inputEmail3">Preferred datetime Of Visit</label><div class=" col-sm-offset-2 col-sm-4"><input class="form-control" type="text" name="copy_date" parsley-trigger="change" required id="copy_date" style="margin-bottom:3%;" value="'+date+'"></div></div>');
         
			 $('#serv_date').appendDtpicker({
"inline": false,
"dateFormat": "YYYY-MM-DD hh:mm",
"current": date,
"todayButton": false,
//"futureOnly": true,
"closeOnSelected": true
});
	$("#footer_formodal").empty();
$("#footer_formodal").html('<button type="button" class="btn circle blue btn-outline btn-sm btn-default" onclick=edit_modal("'+ticket_id+'");>OK</button><button type="button" class="btn circle red btn-outline btn-sm btn-default" data-dismiss="modal" data-dismiss="modal">Cancel</button>');
		 $('#myModal').modal('show');
         }
		 function del(ticket_id)
		 {
			 var company_id="<?php echo $company_id;?>";	  
           swal({
						  title: "Are you sure? You want to delete",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Yes, Delete it!",
						  closeOnConfirm: false
						},
						function(){
			   $.ajax({
         			url: "<?php echo base_url();?>" + "index.php?/controller_service/delete_tick",
         			type: 'POST',
         			data: {'company_id':company_id,'ticket_id':ticket_id
         			},
         			success: function(data) {
         				swal({
                                  title: data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "No, Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
									  window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#deferred"; 
                                  }
                                });
					}
			   });
		   });
						}
		 function edit_modal(ticket_id)
		 {	
		 var date=$('#serv_date').val();
		 var cdate=$('#copy_date').val();
			 var date=date+":00";
		//	alert(date);
		//	alert(cdate);
			 if(date==cdate){
			 swal("No Changes done!");
				 //return false;
			 }
		else {
		 var company_id="<?php echo $company_id;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/update_tkt",
         type: 'POST',
         data: {'company_id':company_id,'ticket_id':ticket_id,'date':date,'cdate':cdate},
         success: function(data) {
			 //alert(data);
			 if(data="Ticket Date updated"){
				 $('#myModal').modal('hide');
			swal({
                                  title: data,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "No,Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                   swal.close();
									  window.location.reload();
									  window.location.href="<?php echo base_url();?>"+"index.php?/controller_service/assigned_tickets#tbody_deferred";
                                  }
                                });
					 }
			 	else{
				 		swal(data,"warning");
					 }
        		 }
        	 });
		}
		 }
      </script>
         </body>
      </html>