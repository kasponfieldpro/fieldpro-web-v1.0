<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
			 $company_id=$this->session->userdata('companyid');
			$region=$user['region'];$area=$user['area'];$location=$user['location'];
			include 'assets/lib/cssscript.php';
include 'assign_tech.php'?>
         </head>
         <!-- END HEAD -->
       <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/manager_header.php"?>
               <!-- END HEADER -->
              <div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->							  
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">ONGOING TICKETS</div>
									<ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#unassigned" data-toggle="tab">Work-In-Progress Tickets</a>
                                             </li>
                                             <li>
                                                <a href="#assigned" data-toggle="tab">Escalated Tickets </a>
                                             </li>
                                             <li>
                                                <a href="#accepted" data-toggle="tab">Spare Requested Tickets </a>
                                             </li>
                                          </ul>
                                 </div>
                                 <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="unassigned">
                
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                              
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location" id="location" onchange="day();" name="role1">
                                                         <!--<option value="" selected disabled>All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id" onchange="day();">
                                                       <!--  <option value="" selected disabled>All Product-Category</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right" id="regionall">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="region" onchange="day();" name="">
                                                      <option value="" selected>All Region</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <span class="clearfix"></span>
                                                <div class="table=responsive" style="margin-top:10px;">
                                                   <table class="table table-hover table-bordered datatable1" id="">
                                                      <thead>
                                                         <tr>
                                                            <th class="text-center">Ticket Id</th>
                                                            <th class="text-center">Customer Name</th>
                                                            <th class="text-center">Technician Id</th>
                                                            <th class="text-center">Next Visit</th>
                                                            <th class="text-center">Plan of Action</th>
                                                            <th class="text-center">Reason</th>
                                                            <th class="text-center">Start Time</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_inprogress" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="assigned">
      
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                

                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location1"  onchange="day1();" name="role1">
                                                        <!-- <option value="" selected disabled>All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id1" onchange="day1();" name="role1">
                                                       <!--  <option value="" selected disabled>All Product-Category</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right" id="regionall1">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="region1" onchange="day1();" name="">
                                                      <option value="" selected>All Region</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <span class="clearfix"></span>
                                                <div class="table-responsive" style="margin-top:10px;">
                                                   <table class="table table-hover table-bordered datatable2" id="">
                                                      <thead>
                                                         <tr>
                                                            <th class="text-center">Ticket Id</th>
                                                            <th class="text-center">Customer Name</th>
                                                            <th class="text-center">Escalated Technician Id</th>
                                                            <th class="text-center">Previous Technician Id</th>
                                                            <th class="text-center">Product-Category</th>
                                                            <th class="text-center">Sub-Category</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_escalate" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="accepted">
                            
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                                  

                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location2"  onchange="day2();" name="role1">
                                                        <!-- <option value="" selected disabled>All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id2"  onchange="day2();" name="role1">
                                                       <!--  <option value="" selected disabled>All Product-Category</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>-->
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right" id="regionall2">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="region2" onchange="day2();" name="">
                                                      <option value="" selected>All Region</option>
                                                      </select>
                                                   </div>
                                                </div><span class="clearfix"></span>
                                                <div class="table-responsive" style="margin-top:10px;">
                                                   <table class="table table-hover datatable3" id="">
                                                      <thead>
                                                         <tr>
                                                            <th class="text-center">Ticket Id</th>
                                                            <th class="text-center">Customer Name</th>
                                                            <th class="text-center">Technician Id</th>
                                                            <th class="text-center">Spare Requested</th>
                                                            <th class="text-center">Lead Time</th>
                                                            <th class="text-center">Status</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_spare" align="center" ></tbody>
                                                   </table>
                                                </div>
                                             </div>
											</div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>

<!-- BEGIN FOOTER -->
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>

            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modal_head">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
						</form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>
                     </div>
                  </div>
               </div>
            </div>
           
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?> 
<script>        		
       		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
        	$('#man_spare').addClass('open active');
        	$('#man_ongoing').addClass('active');
		</script>
	<script type="text/javascript">
         $(document).ready(function() {
			 $('.datatable1').DataTable({"order": []});
			 $('.datatable2').DataTable({"order": []});
			 $('.datatable3').DataTable({"order": []});
	
	var company_id="<?php echo $company_id;?>";		 
	var region="<?php echo $region;?>";		 
	var area="<?php echo $area;?>";	

         if(region =="all"){
           $('#regionall').show();
           $('#regionall1').show();
           $('#regionall2').show();

         }
         else{
           $('#regionall').hide();
           $('#regionall1').hide();
           $('#regionall2').hide();
         }	 



	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'10'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location').html(' <option selected value="">All Location</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
 	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'9'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location1').html(' <option selected value="">All Location</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location1').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
 	$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'11'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location2').html(' <option selected value="">All Location</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#location2').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location2').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
			 
    $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'10'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#product_id').html(' <option selected value="">All Product-Category</option>');
         
         for(i=0;i<data.length;i++)
         {
         	$('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else{
			 $('#product_id').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'9'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#product_id1').html(' <option selected value="">All Product-Category</option>');
         
         for(i=0;i<data.length;i++)
         {
         	$('#product_id1').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else{
			 $('#product_id1').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'11'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#product_id2').html(' <option selected value="">All Product-Category</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#product_id2').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
		 else{
			 $('#product_id2').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
			 
		 var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var product_id1=$('#product_id1').val();
         var product_id2=$('#product_id2').val();
         var product_id3=$('#product_id3').val();
         var filter1=$('#day1').val();
         var filter2=$('#day2').val();
         var filter3=$('#day3').val();
		     var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
         var location=$('#location').val();
		     var region1="<?php echo $region;?>";
         var area1="<?php echo $area;?>";
         var location1=$('#location1').val();
		     var region2="<?php echo $region;?>";
         var area2="<?php echo $area;?>";
         var location2=$('#location2').val();
		     var region3="<?php echo $region;?>";
         var area3="<?php echo $area;?>";
         var location3=$('#location3').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_ongoing",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
         $('#tbody_inprogress').empty();
			  $('.datatable1').DataTable().destroy();
        
         if(data.length<1)
         {
         $('#tbody_inprogress').html('<tr><td colspan=9>No data available</td></tr>');
         }
         else
         {
           $('#tbody_inprogress').empty();
         for(i=0;i<data.length;i++)
         {
         	var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");
          var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");	
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");		
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");
          var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");	
          data[i].schedule_next=data[i].schedule_next.split(' ')[0];
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         	$('#tbody_inprogress').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].schedule_next+'</td><td>'+data[i].poa+'</td><td>'+data[i].reason+'</td><td>'+data[i].assigned_time+'</td></tr>');	
         }

         } $('.datatable1').DataTable({"order": []});
         }
         });
			 
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_escalated",
         type: 'POST',
		 data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region1,'area':area1,'location':location1},
         dataType: "json",
         success: function(data) {
         $('#tbody_escalate').empty();
			   $('.datatable2').DataTable().destroy();
         
         if(data.length<1)
         {
         $('#tbody_escalate').html('<tr><td colspan=9>No records found</td></tr>');
         }
         else
         {
			  $('#tbody_escalate').empty();
			 for(i=0;i<data.length;i++)
         {
         var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
          var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");
         var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
         var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
         var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
         var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");
          var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");		
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         $('#tbody_escalate').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].previous_emp+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td></tr>');	
         
         }
         }  $('.datatable2').DataTable({"order": []});
         }
         });
			 
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'10'},
         dataType: "json",
         success: function(data) {
			  var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0){
         $('#region').html(' <option selected value="">All Region</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		 else{
			 $('#region').html(' <option selected value="">No regions</option>'); 
		 }
		 }
         });
	    $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'9'},
         dataType: "json",
         success: function(data) {
		 var res=$.trim(data['region']);
		 if(res.length>0)
	   {
         $('#region1').html(' <option selected value="">All Region</option>');
        
         for(i=0;i<data.length;i++)
         {
         	$('#region1').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		 else{
			 $('#region1').html(' <option selected value="">No regions</option>'); 
		 }
		 }
         });
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'11'},
         dataType: "json",
         success: function(data) {
		     var res=$.trim(data['region']);
			 //alert(res);
		 if(res.length>0)
		 { 
         $('#region2').html(' <option selected value="">All Region</option>');
         
         for(i=0;i<data.length;i++)
         {
         	$('#region2').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
		 else{
			 $('#region2').html(' <option selected value="">No regions</option>'); 
		 }
		 }
         });
			 
		$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_spare_requested",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter2,'product_id':product_id2,'region':region2,'area':area2,'location':location2},
         dataType: "json",
         success: function(data) {
         $('#tbody_spare').empty();
			  $('.datatable3').DataTable().destroy();
        
         if(data.length<1)
         {
         $('#tbody_spare').html('<tr><td colspan=9>No records found</td></tr>');
         }
         else
         {
			   $('#tbody_spare').empty();
         for(i=0;i<data.length;i++)
         {
         var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");
         var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
         var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
         var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
         var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");
var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
 var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");	
var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
if(data[i].current_status==11)
{
var spare_status='Approved';
}
else if (data[i].current_status==18)
{
var spare_status='Delievered';
}
         $('#tbody_spare').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td class="text-center"><button class="btn blue btn-outline btn-icon-only btn-circle" onclick=show_spare("'+data[i].spare_code+'","'+data[i].quantity+'","'+data[i].spare_source+'");><i class="fa fa-eye"></i></button></td><td>'+data[i].lead_time+'</td><td><span class="label label-sm label-info ">'+spare_status+'</span></td></tr>');	
         
         }
         }  $('.datatable3').DataTable({"order": []});
         }
         });
         });
         function redirect(ticket_id,product_id,cat_id,location,tech_id)
         {
         var company_id="<?php echo $company_id;?>";
         $.ajax({
               url: "<?php echo base_url();?>" + "index.php?/controller_service/load_tech",
               type: 'POST',
               data: {'company_id':company_id,'product_id':product_id,'cat_id':cat_id,'location':location,'tech_id':tech_id
               },
               dataType: "json",
               success: function(data) {
               $('#tbody').html('');
               console.log(data);
               if(data.length<1)
               {
               $('#tbody').html('<tr><td colspan=9>No records found</td></tr>');
               }
               else
               {
               if(tech_id==''){
               for(i=0;i<data.length;i++)
               {
               $('#tbody').append('<tr><td>'+data[i].technician_id+'</td><td>'+data[i].first_name+''+data[i].last_name+'</td><td>'+data[i].skill_level+'</td><td>'+data[i].today_task_count+'</td><td>'+data[i].current_location+'</td><td><button id="'+data[i].technician_id+'" class="btn blue btn-outline btn-circle btn-sm" onclick=confirm2(this.id,"'+data[i].today_task_count+'","'+ticket_id+'");>Assign</button></td></tr>');	
               }	
               }
               else{
               	
               for(i=0;i<data.length;i++)
               {
               $('#tbody').append('<tr><td>'+data[i].technician_id+'</td><td>'+data[i].first_name+''+data[i].last_name+'</td><td>'+data[i].skill_level+'</td><td>'+data[i].today_task_count+'</td><td>'+data[i].current_location+'</td><td><button id="'+data[i].technician_id+'" class="btn blue btn-outline btn-circle btn-sm" onclick=confirm2(this.id,"'+data[i].today_task_count+'","'+ticket_id+'");>Re-assign</button></td></tr>');	
               }
               }
               } 
			   $('#myModal4').modal('show');               }
});
         }
         function hover(tech_id,technician_name,tech_email,contact_number,location,skill,task_count)
         {
$('#modal_head').html('Technician Details');
location= location.replace(/\:/g," ");	
technician_name= technician_name.replace(/\:/g," ");	
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Technician ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+technician_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+tech_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Skill</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+skill+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Tickets Count</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+task_count+'</div></div></div>');
         $('#myModal').modal('show');
         }
         function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority,call_tag,call_type,cust_contact)
         {
$('#modal_head').html('Ticket Details');
         var replaceSpace=problem;
         problem = problem.replace(/\:/g," ");	
         var product_name=product_name;
         product_name= product_name.replace(/\:/g," ");	
         var location=location;
         location= location.replace(/\:/g," ");	
         var cat_name=cat_name;
         cat_name= cat_name.replace(/\:/g," ");	
         var priority=priority;
         priority= priority.replace(/\:/g," ");	call_tag= call_tag.replace(/\:/g," ");	call_type= call_type.replace(/\:/g," ");	
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Contact Number</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_contact+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Priority</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+priority+'</div></div></div>');

         $('#myModal').modal('show');
         }
		 function show_spare(spare_code,quantity,spare_source)
		 {
			 var spare_codes = spare_code.split(',');
			 var spare_quantity = quantity.split(',');
			 var spare_sources = spare_source.split(',');
			 $('#modal_display').html('');
			 $('#modal_display').html('<table class="table table-hover" id=""><thead><tr><th>Spare Code</th><th>Spare Quantity</th><th>Spare Location</th></tr></thead><tbody id="spare_body">');console.log(spare_quantity);
			 for(i=0;i<spare_codes.length;i++)
			 {
				 
			$('#spare_body').append('<tr><td>' + spare_codes[i] + '</td><td>' + spare_quantity[i]+ '</td><td>' + spare_sources[i] + '</td></tr>');
			 }
			 $('#modal_display').append('</tbody></table>');
			$('#myModal').modal('show');
		 }
         function day()
         {
         var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
         var location=$('#location').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_ongoing",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location},
         dataType: "json",
         success: function(data) {
         $('#tbody_inprogress').empty();
			  $('.datatable1').DataTable().destroy();
         console.log(data);
         if(data.length<1)
         {
         $('#tbody_inprogress').html('<tr><td colspan=9>No records found</td></tr>');
		 }
         else
         {
         $('#tbody_inprogress').empty();
         for(i=0;i<data.length;i++)
         {
         	var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");
          var location=data[i].location;
         	var location= location.replace(/ /g, ":");	
          var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
          var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");		
          var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");	
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");	
          var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");
          data[i].schedule_next=data[i].schedule_next.split(' ')[0];
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         	$('#tbody_inprogress').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].tech_id+'</a></td><td>'+data[i].schedule_next+'</td><td>'+data[i].poa+'</td><td>'+data[i].reason+'</td><td>'+data[i].assigned_time+'</td></tr>');	
         }
         }  $('.datatable1').DataTable({"order": []});
         }
         });
         }
         function day1()
         {
         var company_id="<?php echo $company_id;?>";
         var filter1=$('#day1').val();
         var product_id=$('#product_id1').val();
		var region1="<?php echo $region;?>";
         var area1="<?php echo $area;?>";
         var location1=$('#location1').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_escalated",
         type: 'POST',
		 data: {'company_id':company_id,'filter':filter1,'product_id':product_id,'region':region1,'area':area1,'location':location1},
         dataType: "json",
         success: function(data) {
         $('#tbody_escalate').empty();
			  $('.datatable2').DataTable().destroy();
         console.log(data);
         if(data.length<1)
         {
         $('#tbody_escalate').html('<tr><td colspan=9>No records found</td></tr>');$('#datatable2').datatable();
         }
         else
         {
			   $('#tbody_escalate').empty();
         for(i=0;i<data.length;i++)
         {
         var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
         var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");
         var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
         var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
         var location=data[i].location;
         	var location= location.replace(/ /g, ":");		
         var priority=data[i].priority;
         	var priority= priority.replace(/ /g, ":");	
          var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
          var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
          var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");
          var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");	
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
         $('#tbody_escalate').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+customer_name+'</td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].previous_emp+'</td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td></tr>');	
         
         }
         }  $('.datatable2').DataTable({"order": []});
         }
         });
         }
		 function day2()
		 {
			var company_id="<?php echo $company_id;?>";
			var filter2=$('#day2').val();
			var product_id2=$('#product_id2').val();
			var region2="<?php echo $region;?>";
         var area2="<?php echo $area;?>";
			var location2=$('#location2').val();
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_spare_requested",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter2,'product_id':product_id2,'region':region2,'area':area2,'location':location2},
         dataType: "json",
         success: function(data) {
         $('#tbody_spare').empty();
			  $('.datatable3').DataTable().destroy();
         console.log(data);
         if(data.length<1)
         {
         $('#tbody_spare').html('<tr><td colspan=9>No records found</td></tr>');$('#datatable3').datatable();
         }
         else
         {
			  $('#tbody_spare').empty();
         for(i=0;i<data.length;i++)
         {
         var replaceSpace=data[i].prob_desc;
         	var problem = replaceSpace.replace(/ /g, ":");	
var customer_name=data[i].customer_name;
         	var customer_name= customer_name.replace(/ /g, ":");
         var product_name=data[i].product_name;
         	var product_name= product_name.replace(/ /g, ":");	
         var cat_name=data[i].cat_name;
         	var cat_name= cat_name.replace(/ /g, ":");	
         var location=data[i].location;
         	var location= location.replace(/ /g, ":");		
         var priority=data[i].priority;
var call_tag=data[i].call_tag;
         	var call_tag= call_tag.replace(/ /g, ":");
 var call_type=data[i].call_type;
         	var call_type= call_type.replace(/ /g, ":");	
         	var priority= priority.replace(/ /g, ":");var tech_location=data[i].tech_loc;
         	var tech_location= tech_location.replace(/ /g, ":");
var first_name=data[i].first_name;
         	var first_name= first_name.replace(/ /g, ":");		
         	var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
if(data[i].current_status==11)
{
var spare_status='Approved';
}
else if (data[i].current_status==18)
{
var spare_status='Delievered';
}
         $('#tbody_spare').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td>'+data[i].customer_name+'</td><td id="'+data[i].tech_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+tech_location+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].tech_id+'</a></td><td class="text-center"><button class="btn dark btn-outline btn-sm btn-circle" onclick=show_spare("'+data[i].spare_code+'","'+data[i].quantity+'","'+data[i].spare_source+'");>View</button></td><td>'+data[i].lead_time+'</td><td ><span class="label label-sm label-info ">'+spare_status+'</span></td></tr>');	
         
         }

         }  $('.datatable3').DataTable({"order": []});
         }
         });
		 }
		 function region()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'10'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
			 $('#area').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
		 function area()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'10'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
		 function region1()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'9'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area1').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
			 $('#area1').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
		 function area1()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'9'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location1').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location1').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
		 function region2()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'11'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#area2').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#area2').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
		 else{
			 $('#area2').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
		 function area2()
		 {
			 var company_id="<?php echo $company_id;?>";
			var region="<?php echo $region;?>";
         var area="<?php echo $area;?>";
			 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'11'},
         dataType: "json",
         success: function(data) {
			 if(data.length>0){
         $('#location2').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
         	$('#location2').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
		 else{
			 $('#location2').html(' <option selected value="">No results</option>'); 
		 }
		 }
         });
		 }
      </script>
         </body>
      </html>