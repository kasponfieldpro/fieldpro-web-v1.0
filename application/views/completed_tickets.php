<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <?php 
      $company_id=$this->session->userdata('companyid');
               $region=$user['region'];$area=$user['area'];$location=$user['location'];
      include 'assets/lib/cssscript.php'?>
       <style>
      .dt-buttons{
                 display:none;
            }
       .dataTables_filter
            {
              text-align: right;
            }
       </style>
         </head>
         <!-- END HEAD -->
 <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
            <!-- BEGIN CONTAINER -->
            <div class="page-wrapper">
               <!-- BEGIN HEADER -->
               <?php include "assets/lib/header_service.php"?>
               <!-- END HEADER -->
              <div class="page-container">
         <div class="page-sidebar-wrapper">
         <?php include "assets/lib/service_sidebar.php"?>
         </div>
         <div class="page-content-wrapper">
                  <div class="page-content">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->               
                              <div class="portlet box dark">
                                 <div class="portlet-title">
                                    <div class="caption">COMPLETED TICKETS</div>
                  <ul class="nav nav-tabs">
                                             <li class="active">
                                                <a href="#unassigned" data-toggle="tab">Completed Tickets</a>
                                             </li>
                                             <li>
                                                <a href="#assigned" data-toggle="tab">Closed Tickets </a>
                                             </li>
                                          </ul>
                                 </div>
                                 <div class="portlet-body">
                                          <div class="tab-content">
                                             <div class="tab-pane active" id="unassigned">
                                                
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location" onChange=" day();" name="role1">
                                                         <option value="" selected >All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
                                                    <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id" onChange="day();" name="role1">
                                                         <option value="" selected >All Product-Category</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="day" onChange="day();" name="role1">
                                                          <option value='today'>Today</option>
                               <option value='week'>This Week</option>
                               <option value='month'>This Month</option>
                               <option value='year'>This Year</option>
                               <option value='all' selected>All Tickets</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered datatable" id="">
                                                      <thead>
                                                         <tr>
                                                            <th style="text-align:center">Ticket Id</th>
                                                            <th style="text-align:center">Technician Id</th>
                                                            <th style="text-align:center">Product-Category</th>
                                                            <th style="text-align:center">Sub-Category</th>
                                                            <th style="text-align:center">Completed Time</th>
                                                            <th style="text-align:center">Action</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_completed" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div class="tab-pane" id="assigned">
                                               
                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="location1" onChange="day1();" name="role1">
                                                         <option value="" selected >All Location</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="product_id1" onChange="day1();" name="role1">
                                                         <option value="" selected >All Product-Category</option>
                                                         <option value="Admin">Admin</option>
                                                         <option value="Manager">Manager</option>
                                                         <option value="Call-coordinator">Call-coordinator</option>
                                                         <option value="Service Desk">Service Desk</option>
                                                         <option value="Technician">Technician</option>
                                                      </select>
                                                   </div>
                                                   <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12  pull-right">
                                                      <select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="day1" onChange="day1();" name="role1">
                              <option value='today'>Today</option>
                               <option value='week'>This Week</option>
                               <option value='month'>This Month</option>
                               <option value='year'>This Year</option>
                               <option value='all' selected>All Tickets</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="table=responsive" style="padding-top: 5%;">
                                                   <table class="table table-hover table-bordered datatable1" id="">
                                                      <thead>
                                                         <tr>
                                                            <th class="text-center">Ticket Id</th>
                                                            <th class="text-center">Technician Id</th>
                                                            <th class="text-center">Product-Category</th>
                                                            <th class="text-center">Sub-Category</th>
                                                            <th class="text-center">Closed Time</th>
                                                            <th class="text-center">Customer Feedback</th>
                                                            <th class="text-center">Customer Rating</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="tbody_closed" align="center"></tbody>
                                                   </table>
                                                </div>
                                             </div>
                      </div>
                                 </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                           </div>
                        </div>
                     </div>
                <?php include "assets/lib/footer.php"?>
                <!-- END FOOTER -->
            </div>
            <!--Modal Starts-->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
               <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modal_head">Details</h4>
                     </div>
                     <div class="modal-body">
                        <form class="form-horizontal" role="form" id='modal_display'>
            </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-circle blue btn-outline btn-sm" data-dismiss="modal">OK</button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
           <div class="modal fade" id="smallModal">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Customer Feedback</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" role="form">
                  <div class="form-group">
            <div class="col-lg-4"><label for="users">Customer ID</label></div>
                    <div class="col-lg-8"><input type="text" class="form-control" readonly id="cust_id_modal" placeholder="Customer ID"></div>
                  </div>
          <br>
                  <div class="form-group">
          <div class="col-lg-4"><label for="users">Customer Name</label></div>
                    <div class="col-lg-8"><input type="text" class="form-control"  readonly id="cust_name_modal" placeholder="Customer Name"></div>
                  </div>
          <br>
          <div class="form-group">
          <div class="col-lg-4"><label for="users">Contact Number</label></div>
                    <div class="col-lg-8"><input type="text" class="form-control"  readonly id="cust_contact_modal" placeholder="Contact Number"></div>
                  </div>
          <br>
           <div class="form-group">
          <div class="col-lg-4"><label for="users">E-mail ID</label></div>
                    <div class="col-lg-8"><input type="text" class="form-control"  readonly id="cust_email_modal"></div>
                  </div>
          <br>
        
          <div class="form-group">
            <div class="col-lg-4"><label for="users">Customer Feedback</label></div>
                    <div class="col-lg-8"><select class="form-control col-lg-2 col-lg-offset-1 pull-right" id="feedback_modal"  name="feedback_modal">
                                                         <option value="" >No Customer Feedback </option>
                                                         <option value="Excellent">Excellent</option>
                                                         <option value="Satisfied">Satisfied</option>
                                                         <option value="Very Good">Very Good</option>
                                                         <option value="Average">Average</option>
                                                         <option value="Bad">Bad</option>
                                                      </select></div>
                  </div>
          <br>
                  <div class="form-group">  
          <div class="col-lg-4"><label for="users">Customer Rating</label></div>
                    <div class="col-lg-8"><input type="text" class="form-control" id="rating_modal" placeholder="give your rating between 1 to 5"></div>
                  </div>
          <br>
                  <div class="form-group">  
          <div class="col-lg-4"><label for="users">Customer Comments</label></div>
                    <div class="col-lg-8"><textarea rows="4" cols="50" class="form-control" id="reason_modal" placeholder="Customer Comments" style="resize:none;">                          </textarea></div>
                  </div>
                </form>
              </div>
              <div class="modal-footer" id='button_modal'>
                
              </div>
            </div>
          </div>
        </div>
           
            <!-- END QUICK SIDEBAR -->
            <?php include 'assets/lib/javascript.php'?> 
      <script>                
               $('.nav.navbar-nav').find('.open').removeClass( 'open' );
            $('#completed').addClass('open');
        </script>
        <script type="text/javascript">
         $(document).ready(function() {set_int();
         // $('.sample_2').DataTable();
         $('[data-toggle="tooltip"]').tooltip(); 
                       
  /* $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'8'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#area').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#area').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
     else
     {
      $('#area').html(' <option selected value="">No Results</option>');
     }
     }
         });
    $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_area",
         type: 'POST',
         data: {'company_id':company_id,'status':'12'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#area1').html(' <option selected value="">All Area</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#area1').append('<option value="'+data[i].town+'">'+data[i].town+'</option>');
         }
         }
      else
     {
      $('#area1').html(' <option selected value="">No Results</option>');
     }
     }
         });*/
var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";
     $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'8'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
      else
     {
      $('#location').html(' <option selected value="">No Results</option>');
     }
     }
         });
  $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'12'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#location1').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
      else
     {
      $('#location1').html(' <option selected value="">No Results</option>');
     }
     }
         });

         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'8'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#product_id').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#product_id').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
      else
     {
      $('#product_id').html(' <option selected value="">No Results</option>');
     }
     }
         });
$.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_product",
         type: 'POST',
         data: {'company_id':company_id,'status':'12'},
         dataType: "json",
         success: function(data) {
     if(data.length>0){
         $('#product_id1').html(' <option selected value="">All Product-Category</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#product_id1').append('<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>');
         }
         }
      else
     {
      $('#product_id1').html(' <option selected value="">No Results</option>');
     }
     }
         });
    var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";                 
     $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_technician",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'8'},
         dataType: "json",
         success: function(data) {
         $('#tech').html(' <option selected value="">All Technician</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#tech').append('<option value="'+data[i].technician_id+'">'+data[i].first_name+'</option>');
         }
         }
         });
 $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_technician",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'12'},
         dataType: "json",
         success: function(data) {
         $('#tech1').html(' <option selected value="">All Technician</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#tech1').append('<option value="'+data[i].technician_id+'">'+data[i].first_name+'</option>');
         }
         }
         });
                     
  /*  $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'8'},
         dataType: "json",
         success: function(data) {
      var res=$.trim(data['region']);
      // alert(res);
     if(res.length>0){
         $('#region').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#region').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
     else{
        $('#region').html(' <option selected value="">No Results</option>');
     }
     }
         });     
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_region",
         type: 'POST',
         data: {'company_id':company_id,'status':'12'},
         dataType: "json",
         success: function(data) {
      var res=$.trim(data['region']);
      // alert(res);
     if(res.length>0){
         $('#region1').html(' <option selected value="">All Region</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#region1').append('<option value="'+data[i].region+'">'+data[i].region+'</option>');
         }
         }
      else{
        $('#region1').html(' <option selected value="">No Results</option>');
     }
     }
         });
             
    */                 
     var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var product_id1=$('#product_id1').val();
         var filter1=$('#day1').val();
     var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";    
         var location=$('#location').val();
     var region1="<?php echo $region;?>";
 var area1="<?php echo $area;?>";   
         var location1=$('#location1').val();
var tech=$('#tech').val();
         var tech1=$('#tech1').val();
                     
            $('#tbody_completed').empty();
         $('.datatable').DataTable().destroy();
   $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_completed",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location,'tech':tech},
         dataType: "json",
         success: function(data) {
       //alert(data);
         console.log(data);
         if(data.length<1)
         {
            //$('#tbody_completed').html('<tr><td colspan=3>No data available</td></tr>');
               $('#tbody_completed').empty();
         $('.datatable').DataTable().destroy();
         }
         else
         {
          // $('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
          var replaceSpace=data[i].prob_desc;
          var problem = replaceSpace.replace(/ /g, ":");  
            var location=data[i].location;
          var location= location.replace(/ /g, ":");  
            var product_name=data[i].product_name;
          var product_name= product_name.replace(/ /g, ":");  
            var cat_name=data[i].cat_name;
          var cat_name= cat_name.replace(/ /g, ":");
      var cust_reason=data[i].cust_reason;
          var cust_reason= cust_reason.replace(/ /g, ":");
      var priority=data[i].priority;
          var priority= priority.replace(/ /g, ":");  
      var cust_feedback=data[i].cust_feedback;
               cust_feedback=cust_feedback.trim().replace(/ /g, '%20');
var customer_name=data[i].customer_name;
               customer_name=customer_name.replace(/ /g, ':');
var call_tag=data[i].call_tag;
          var call_tag= call_tag.replace(/ /g, ":");
var call_type=data[i].call_type;
          var call_type= call_type.replace(/ /g, ":");
          var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
       
          $('#tbody_completed').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].ticket_end_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn blue btn-outline btn-sm btn-circle" onclick=get_feedback(this.id,"'+data[i].customer_id+'","'+customer_name+'","'+cust_feedback+'","'+data[i].cust_rating+'","'+cust_reason+'","'+data[i].cust_email+'","'+data[i].cust_contact+'","'+data[i].cust_alt_num+'","'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","'+data[i].tech_id+'");>Get Feedback</button></td></tr>');  
         }

         }
      $('.datatable').DataTable({"order": []});
         }
         });
    
        $('#tbody_closed').empty();
      $('.datatable1').DataTable().destroy();
                     
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_closed",
         type: 'POST',
     data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region1,'area':area1,'location':location1,'tech':tech1},
         dataType: "json",
         success: function(data) {
         console.log(data);
         if(data.length<1)
         {
            //$('#tbody_closed').html('<tr><td colspan=6>No records found</td></tr>');
          $('#tbody_closed').empty();
        $('.datatable1').DataTable().destroy();
         }
         else
         {
      // $('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
      // var cust_rating='';
         var replaceSpace=data[i].prob_desc;
          var problem = replaceSpace.replace(/ /g, ":");  
         var product_name=data[i].product_name;
          var product_name= product_name.replace(/ /g, ":");  
         var cat_name=data[i].cat_name;
          var cat_name= cat_name.replace(/ /g, ":");  
         var location=data[i].location;
          var location= location.replace(/ /g, ":");  
         var priority=data[i].priority;
          var priority= priority.replace(/ /g, ":");
if(data[i].cust_feedback=='')
{
data[i].cust_feedback='-';
}
    if(data[i].cust_rating==0)
              {
                data[i].cust_rating='-';
              }
              else if(data[i].cust_rating==1){
                data[i].cust_rating='*';
              }
              else if(data[i].cust_rating==2){
                data[i].cust_rating='* *';
              }
              else if(data[i].cust_rating==3){
                data[i].cust_rating='* * *';
              }
              else if(data[i].cust_rating==4){
                data[i].cust_rating='* * * *';
              }
              else if(data[i].cust_rating==5){
                data[i].cust_rating='* * * * *';
              }   
var call_tag=data[i].call_tag;
          var call_tag= call_tag.replace(/ /g, ":");
var call_type=data[i].call_type;
          var call_type= call_type.replace(/ /g, ":");  
          var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
      $('#tbody_closed').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].accepted_time+'</td><td align="center"><span >'+data[i].cust_feedback+'</span></td><td align="center"><span>'+data[i].cust_rating+'</span></td></tr>');          
         }
         }
        $('.datatable1').DataTable({"order": []});
         }
         });
                     
    
         } );
        // TableManageButtons.init();
    function close_tkt(ticket_id,customer_name,email_id)
    {
     var rating= $('#rating_modal').val();
     
      if(rating=='0' || rating=='1' || rating=='2' || rating=='3' || rating=='4'|| rating=='5'){
    var cust_feedback= $('#feedback_modal').val();
    var cust_rating= $('#rating_modal').val();
    var cust_reason= $('#reason_modal').val();

var company_id="<?php echo $company_id;?>";
       $.ajax({
        url: "<?php echo base_url();?>" + "index.php?/controller_service/close_tkt",
        type: 'POST',
        data: {'company_id':company_id,'ticket_id':ticket_id,'cust_feedback':cust_feedback,'cust_rating':cust_rating,'cust_reason':cust_reason},
        success: function(data) {
      var val=data; 
        $.ajax({
        url: "<?php echo base_url();?>" + "index.php?/controller_service/send_mail",
        type: 'POST',
        data: {'company_id':company_id,'ticket_id':ticket_id,'customer_name':customer_name,'cust_feedback':cust_feedback,'cust_rating':cust_rating,'cust_reason':cust_reason,'email_id':email_id},
        success: function(data) { 
          //alert(data);
          //return false();
          swal({
                                  title: val,                                  
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Ok",
                                  cancelButtonText: "No, Cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                                function(isConfirm) {
                                  if (isConfirm) {
                                    window.location.reload();
                                  }
                                });
              }
            });
        }
        });
      }
     else{
      swal({
              title: "Rating should be between 0 to 5",  
              type: "warning",
              showCancelButton: false,
              confirmButtonClass: "btn-default",
              confirmButtonText: "Ok",
              cancelButtonText: "No, Cancel",
                 confirmButtonColor: '#337ab7',
              closeOnConfirm: false,
              closeOnCancel: false
            },
        function(isConfirm) {
            if (isConfirm) {
             $('#smallModal').modal('show');
              swal.close();
          }
        });
        
      }
    }
         function hover(tech_id,technician_name,tech_email,contact_number,skill,task_count)
         {
$('#modal_head').html('Technician Details');
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Technician ID</label><div class="col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+tech_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Name</label><div class=" col-lg-offset-1 col-sm-4 control-label" style=" text-align: left;">'+technician_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Email ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+tech_email+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Mobile </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+contact_number+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Skill</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+skill+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Tickets Count</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+task_count+'</div></div></div>');
         $('#myModal').modal('show');
         }
         function hover_ticket(ticket_id,customer_name,location,product_name,cat_name,problem,priority,call_tag,call_type,cust_contact)
         {
$('#modal_head').html('Ticket Details');
         var replaceSpace=problem;
         problem = problem.replace(/\:/g," ");  
         var product_name=product_name;
         product_name= product_name.replace(/\:/g," "); 
         var location=location;
         location= location.replace(/\:/g," "); 
         var cat_name=cat_name;
         cat_name= cat_name.replace(/\:/g," "); 
         var priority=priority;
         priority= priority.replace(/\:/g," "); call_tag= call_tag.replace(/\:/g," ");  call_type= call_type.replace(/\:/g," ");  
         $('#modal_display').html('<div class="form-group"><label  class="col-sm-3 col-sm-offset-2 control-label"for="inputEmail3">Ticket ID</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+ticket_id+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Customer Name</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+customer_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Contact Number</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cust_contact+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Location</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+location+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Product-Category </label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+product_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Sub-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+cat_name+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Call-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_tag+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Service-Category</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+call_type+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Problem</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;" style=" text-align: left;">'+problem+'</div></div><div class="form-group"><label class="col-sm-3 col-sm-offset-2 control-label"for="inputPassword3" >Priority</label><div class="col-lg-offset-1 col-sm-4 control-label text-left" style=" text-align: left;">'+priority+'</div></div></div>');

         $('#myModal').modal('show');
         }
          
  function get_feedback(ticket_id,customer_id,customer_name,cust_feedback,cust_rating,cust_reason,email_id,cust_contact,alternate_number,product_id,cat_id,location,tech_id)
    {   
      $('#cust_id_modal').val(customer_id);
customer_name=customer_name.trim().replace(/\:/g," ");  
      $('#cust_name_modal').val(customer_name); 
      $('#cust_email_modal').val(email_id); 
      $('#cust_contact_modal').val(cust_contact); 
      var cust_feedback=cust_feedback;
      cust_feedback=cust_feedback.trim().replace('%20',' ');
      $('select[name="feedback_modal"] option[value="' + cust_feedback+ '"]').attr("selected", true);
     // $('#rating_modal').val(cust_rating);
      $('#reason_modal').val(cust_reason);
          var cust_feedback= $('#feedback_modal').val();
var cust_reason= $('#reason_modal').val();
          var cust_reason= cust_reason.replace(/ /g, ":");
var customer_name= customer_name.replace(/ /g, ":");
      $('#button_modal').html('<button id="'+ticket_id+'" class="btn red btn-outline btn-sm btn-circle" data-dismiss="modal">Cancel</button><button type="button" id="'+ticket_id+'" class="btn blue btn-outline btn-sm btn-circle" onclick=close_tkt(this.id,"'+customer_name+'","'+email_id+'"); data-dismiss="modal">Close Ticket</button>');
      $('#smallModal').modal('show');
    }
    function redirect(ticket_id,product_id,cat_id,location,tech_id)
         {
         sessionStorage.setItem('ticket_id', ticket_id);
         sessionStorage.setItem('product_id', product_id);
         sessionStorage.setItem('cat_id', cat_id);
         sessionStorage.setItem('location', location);
         sessionStorage.setItem('tech_id', tech_id);
         window.location.href = "<?php echo site_url('controller_service/assign_tech');?>";
         }
         function day()
         {
         var company_id="<?php echo $company_id;?>";
         var filter=$('#day').val();
         var product_id=$('#product_id').val();
         var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";    
         var location=$('#location').val();
         var tech=$('#tech').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_completed",
         type: 'POST',
         data: {'company_id':company_id,'filter':filter,'product_id':product_id,'region':region,'area':area,'location':location,'tech':tech},
         dataType: "json",
         success: function(data) {
         $('#tbody_completed').empty();
        $('.sample_2').DataTable().destroy();
         console.log(data);
         if(data.length<1)
         {
         $('#tbody_completed').html('<tr><td colspan=9>No records found</td></tr>');
       // $('.sample_2').DataTable().destroy();
 $('.sample_2').DataTable();

         }
         else
         {
        $('#tbody_completed').empty();
          $('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
          var replaceSpace=data[i].prob_desc;
              var problem = replaceSpace.replace(/ /g, ":");  
            var location=data[i].location;
              var location= location.replace(/ /g, ":");  
       var product_name=data[i].product_name;
              var product_name= product_name.replace(/ /g, ":");  
       var cat_name=data[i].cat_name;
              var cat_name= cat_name.replace(/ /g, ":");   
       var cust_reason=data[i].cust_reason;
              var cust_reason= cust_reason.replace(/ /g, ":");
       var customer_name=data[i].customer_name;
              var customer_name= customer_name.replace(/ /g, ":");  
       var priority=data[i].priority;
              var priority= priority.replace(/ /g, ":");  
      var cust_feedback=data[i].cust_feedback;
           cust_feedback=cust_feedback.trim().replace(/ /g, '%20');
      var call_tag=data[i].call_tag;
              var call_tag= call_tag.replace(/ /g, ":");
      var call_type=data[i].call_type;
              var call_type= call_type.replace(/ /g, ":");
          var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
       
          $('#tbody_completed').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].ticket_end_time+'</td><td style="text-align:center"><button id="'+data[i].ticket_id+'" class="btn red btn-outline btn-sm btn-circle" onclick=get_feedback(this.id,"'+data[i].customer_id+'","'+customer_name+'","'+cust_feedback+'","'+cust_reason+'","'+data[i].cust_email+'","'+data[i].cust_contact+'","'+data[i].cust_alt_num+'","'+data[i].product_id+'","'+data[i].cat_id+'","'+location+'","'+data[i].tech_id+'");>Get Feedback</button></td></tr>'); 
         }
//$('#datatable2').dataTable();

         }
       $('.sample_2').DataTable({"order": []});
         }
         });
         }
         function day1()
         {
         var company_id="<?php echo $company_id;?>";
         var filter1=$('#day1').val();
         var product_id=$('#product_id1').val();
     var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";    
         var location=$('#location1').val();
         var tech1=$('#tech1').val();
         $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_closed",
         type: 'POST',
     data: {'company_id':company_id,'filter':filter1,'product_id':product_id,'region':region,'area':area,'location':location,'tech':tech1},
         dataType: "json",
         success: function(data) {
         $('#tbody_closed').empty();
        $('.sample_2').DataTable().destroy();
         console.log(data);
         if(data.length<1)
         {
         $('#tbody_closed').html('<tr><td colspan=6>No records found</td></tr>');
//$('#datatable1').dataTable();
         }
         else
         {
        $('.sample_2').DataTable().destroy();
         for(i=0;i<data.length;i++)
         {
         var replaceSpace=data[i].prob_desc;
          var problem = replaceSpace.replace(/ /g, ":");  
         var product_name=data[i].product_name;
          var product_name= product_name.replace(/ /g, ":");  
         var cat_name=data[i].cat_name;
          var cat_name= cat_name.replace(/ /g, ":");  
         var location=data[i].location;
          var location= location.replace(/ /g, ":");  
         var priority=data[i].priority;
          var priority= priority.replace(/ /g, ":");if(data[i].cust_feedback=='')
{
data[i].cust_feedback='-';
}
       var cust_rating='';
       if(data[i].cust_rating==0)
              {
                cust_rating='No Rating';
              }
              else if(data[i].cust_rating==1){
                cust_rating='*';
              }
              else if(data[i].cust_rating==2){
                cust_rating='* *';
              }
              else if(data[i].cust_rating==3){
                cust_rating='* * *';
              }
              else if(data[i].cust_rating==4){
                cust_rating='* * * *';
              }
              else if(data[i].cust_rating==5){
                cust_rating='* * * * *';
              }     
          var param2=data[i].customer_name+'&'+data[i].location+'&'+data[i].product_name+'&'+data[i].cat_name+'&'+problem;
var call_tag=data[i].call_tag;
          var call_tag= call_tag.replace(/ /g, ":");
var call_type=data[i].call_type;
          var call_type= call_type.replace(/ /g, ":");
      $('#tbody_closed').append('<tr><td id="'+data[i].ticket_id+'" style="cursor: pointer;" onclick=hover_ticket(this.id,"'+data[i].customer_name+'","'+location+'","'+product_name+'","'+cat_name+'","'+problem+'","'+priority+'","'+call_tag+'","'+call_type+'","'+data[i].cust_contact+'");><a>'+data[i].ticket_id+'</a></td><td id="'+data[i].employee_id+'" style="cursor: pointer;" onclick=hover(this.id,"'+data[i].first_name+'","'+data[i].email_id+'","'+data[i].contact_number+'","'+data[i].skill_level+'","'+data[i].today_task_count+'");><a>'+data[i].employee_id+'</a></td><td>'+data[i].product_name+'</td><td>'+data[i].cat_name+'</td><td>'+data[i].accepted_time+'</td><td align="center"><span >'+data[i].cust_feedback+'</span></td><td align="center"><span>'+cust_rating+'</span></td></tr>');          
         }
         }
        $('.sample_2').DataTable({"order": []});
         }
         });
         }
     
     function area()
     {
       var company_id="<?php echo $company_id;?>";
      var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";    
       $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'8'},
         dataType: "json",
         success: function(data) {
       if(data.length>0)
       {
         $('#location').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#location').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
       else{
          $('#location').html(' <option selected value="">No results</option>');
       }
     }
         });
     }
    
     function area1()
     {
       var company_id="<?php echo $company_id;?>";
      var region="<?php echo $region;?>";
 var area="<?php echo $area;?>";    
       $.ajax({
         url: "<?php echo base_url();?>" + "index.php?/controller_service/load_location",
         type: 'POST',
         data: {'company_id':company_id,'region':region,'area':area,'status':'12'},
         dataType: "json",
         success: function(data) {
       if(data.length>0)
       {
         $('#location1').html(' <option selected value="">All Location</option>');
         console.log(data);
         for(i=0;i<data.length;i++)
         {
          $('#location1').append('<option value="'+data[i].location+'">'+data[i].location+'</option>');
         }
         }
       else{
          $('#location1').html(' <option selected value="">No results</option>');
       }
     }
         });
     }
      </script>
         </body>
      </html>