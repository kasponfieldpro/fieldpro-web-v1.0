<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<style>
	.dt_buttons{
		display:none;
	}
  .dataTables_filter{
		text-align: right;
	}
/*	.pagination li.prev a, .pagination li.next a {
    padding: 9px 8px !important;
}*/
</style>
<?php $company_id=$this->session->userdata('companyid');
			$region=$user['region'];$area=$user['area'];$location=$user['location'];
         include 'assets/lib/cssscript.php'?>	 
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
    <!-- BEGIN CONTAINER -->
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "assets/lib/manager_header.php"?>
        <!-- END HEADER -->
		<div class="page-container">
			   <div class="page-sidebar-wrapper">
			   <?php include "assets/lib/manager_sidebar.php"?>
			   </div>
			   <div class="page-content-wrapper">
                  <div class="page-content">
				  
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box dark">
                                <div class="portlet-title">
                                            <div class="caption">
                                       <i class=""></i>Contract
                                    </div>
                                </div>
                                  
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                    </div>
									 <div class="row" style="display:none">
									   <div class="form-group col-md-6 col-sm-12">
										   <div class="col-md-7">
													<input type="text" class="form-control form-control1" id="c_id" name="c_id" value="<?php echo $this->session->userdata('companyid');?>" readonly>
											 </div>
										</div>
									 </div>
                                        <div class="tab-pane active" id="tab_actions_pending">
                                           
                                                <table class="table table-hover table-bordered datatable1" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center">Customer ID</th>
                                                            <th style="text-align:center">Ticket ID</th>
                                                            <th style="text-align:center">Product Category</th>
                                                            <th style="text-align:center">Product Model No.</th>
															<th style="text-align:center">Serial No.</th>
                                                            <th style="text-align:center">Contract Type</th>
                                                            <th style="text-align:center">Mode of Payment</th>
                                                            <th style="text-align:center">Contract Amount</th>
                                                            <th style="text-align:center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="display_contract" align="center">
                                                        <?php foreach ($record as $row) { ?>
                                                        <tr>
                                                            <td style="text-align:center" id="<?php echo $row['cust_id']; ?>" onClick="hover_tech(this.id,'<?php echo $row['customer_name']; ?>','<?php echo $row['contact_number']; ?>','<?php echo $row['alternate_number']; ?>','<?php echo $row['email_id']; ?>')">
                                                                <a>
                                                                    <?php echo $row[ 'cust_id']; ?>
                                                                </a>
                                                            </td>
															<td style="text-align:center" id="<?php echo $row['ticket_id']; ?>" onClick="hover_ticket(this.id,'<?php echo $row['customer_name']; ?>','<?php echo $row['priority']; ?>','<?php echo $row['product_name']; ?>','<?php echo $row['cat_name']; ?>','<?php echo $row['call_type']; ?>')">
																<a>
                                                               	 	<?php echo $row[ 'ticket_id']; ?>
																</a>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['product_name']; ?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['model']; ?>
                                                            </td>
															<td style="text-align:center">
															<?php  			 	                         																			    if(!empty($row['serial_no'])){
																    echo $row['serial_no'];
															    }
																else{
																      echo '-';
															    }   
															?>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <?php echo $row['call_type']; ?>
															</td>
															<td style="text-align:center">
                                                                <?php echo $row['mode_of_payment']; ?>
															</td>
 															<td style="text-align:center">
                                                                <?php echo $row['total_amount']; ?>
                                                            </td>															
                                                            <td style="text-align:center !important">
		<button class="btn btn-circle green btn-outline btn-sm btn-icon-only" id="<?php echo $row['ticket_id']; ?>" onClick="accepts(this.id)" title="Activate"><i class="fa fa-check" aria-hidden="true"></i></button>
		<!--button class="btn btn-circle red btn-outline btn-sm" id="<?php echo $row['ticket_id']; ?>" onclick="Rejects(this.id)">Hold</button-->       
															</td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            
                                      
                                    </div><!--end tab content-->
								  
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
						<!-- BEGIN FOOTER -->
        <?php include "assets/lib/footer.php"?>
        <!-- END FOOTER -->							
        </div>
		  <div id="viewimage" class="modal fade" role="dialog">
			  <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
					    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View Image</h4>
                    </div>
                    <div class="modal-body">  
                        <div class="modal-dialog" id="view_imagetech">  
                       </div>
					</div>
			</div>
		  </div>
        </div>
		<div id="myModal1" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Details</h5>
               </div>
               <div class="modal-body"id='modal_tech'>
                  <form class="form-horizontal" role="form" >
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
               </div>
            </div>
         </div>
      </div>

 
    
    <!-- END QUICK SIDEBAR -->
    <?php include 'assets/lib/javascript.php'?>
	<script>   
		$('.nav.navbar-nav').find('.open').removeClass( 'open' );
		$('#man_contract').addClass('open');
			$(document).ready(function() {
				$('.datatable1').DataTable({"order": []});
		});
	</script>
    <script>
			
        var company_id="<?php echo $this->session->userdata('companyid');?>";
		function viewbillimage(id)
		{
			  $.ajax({
                    url         :   "<?php echo base_url(); ?>index.php?/controller_manager/view_billimages",
                    type        :   "POST",
                    data        :   {'id':id,'company':company_id},
                    datatype    :   "JSON", 
                    cache       :   false,
                    success     :   function(data){
                                        var data=JSON.parse(data);
                                        console.log(data['bill_image']);
						               if(data['bill_image']=="" ||typeof data['bill_image']=="undefined" )
									   {
										  
										  $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<div id="textforimage"  style="height:200px;background: white;width:95%";><p style="text-align: center;padding-top: 82px;font-size: x-large;">No image available for this Ticket</p></div>');
                                           $('#viewimage').modal('show');
									   }
						              else 
									  {
										  //console.log(data['image']);
                                           $('#view_imagetech').empty();
                                           $('#view_imagetech').append('<img src="'+data['bill_image']+'" height="300px" width="95%">');
                                           $('#viewimage').modal('show');
									  }
                                    },
                });
		}
        function accepts(id) {
			
			swal({
          		  title: "Are you sure?",
          		  text: "You Want To Accept This  Contract Details!",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, Accept",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
function(isConfirm) {
if (isConfirm){
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/contract_status",
                type: 'POST',
                data: {'id': id,'company_id':company_id},
                success: function(data) {
							   			if(data == "Something went Wrong"){
                                              	  swal("Cancelled", "Something Went Wrong!", "error");
                                             }
							   		  else {
										 swal({
											  title: "Approved",
											  text: data,
											  type: "success",
											  showCancelButton: true,
											  confirmButtonClass: "btn-danger",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										}); 
									  }
                            }
			});
}
     else{							
                   swal.close();
		}
          		});  

}
        function Rejects(id) {
			var company_id="<?php echo $this->session->userdata('companyid');?>";
           /* $.ajax({
                url: "<!--?php echo base_url();?>" + "index.php?/controller_manager/contract_reject",
                type: 'POST',
                data: {'id': id,'company_id':company_id},
                //dataType: "json",
                success: function(data) {

                    alert(data);
                   // location.reload();
                }
            });*/
			swal({
          		  title: "Are you sure?",
          		  text: "You will not be able to recover the Contract Details!",
          		  type: "warning",
          		  showCancelButton: true,
          		  confirmButtonClass: "btn-danger",
          		  confirmButtonText: "Yes, delete",
          		  cancelButtonText: "No, cancel",
          		  closeOnConfirm: false,
          		  closeOnCancel: false
          		},
          		function(isConfirm) {
          		  if (isConfirm) {
          			   $.ajax({
                			url   : "<?php echo base_url();?>" + "index.php?/controller_manager/contract_reject",
                			type  : 'POST',
                			data  : {'id': id,'company_id':company_id},
                			//dataType: "json",
               			   success: function(data) {
							   			if(data == "Something went Wrong"){
                                              	  swal("Cancelled", "Something Went Wrong:)", "error");
                                             }
							   		  else {
										 swal({
											  title: "Deleted",
											  text: data,
											  type: "success",
											  showCancelButton: false,
											  confirmButtonClass: "btn-danger",
											  confirmButtonText: "Ok",
											  cancelButtonText: "Cancel",
											  closeOnConfirm: false,
											  closeOnCancel: false
										},
										function(isConfirm) {
											if (isConfirm) {
												swal.close();
												location.reload();
											}
										}); 
									  }
                            }
                       });
          		  }
										    else{
												
                                                                                          swal.close();
											}
          		});    
        }

        function att_day() {
            $('#tbl_accept').empty();
            var filter = $('#att_day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/reimb_accept",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbl_accept').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbl_accept').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);

                        for (i = 0; i < data.length; i++) {
                            $('#tbl_accept').append('<tr><td style="text-align:center" id="' + data[i].technician_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + data[i].cat_name + '");>' + data[i].technician_id + '</td><td style="text-align:center">' + data[i].first_name + '</td><td style="text-align:center">' + data[i].start_time + '</td><td style="text-align:center">' + data[i].end_time + '</td><td style="text-align:center">' + data[i].travelling_charges + '</td><td style="text-align:center"><button class="btn btn-default red-stripe" id="' + data[i].technician_id + '" onclick=viewimage(this.id);>View</button></td></tr>');
                        }
                    }
                }
            });
        }

        function rej_day() {
            $('#tbl_reject').empty();
            var filter = $('#rej_day').val();
            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/reimb_reject",
                type: 'POST',
                data: {
                    'filter': filter
                },
                dataType: "json",
                success: function(data) {
                    $('#tbl_reject').html('');
                    console.log(data);
                    if (data.length < 1) {
                        console.log(data);
                        $('#tbl_reject').html('<tr><td colspan=8><b><i>No records found</i></b></td></tr>');
                    } else {
                        console.log(data);

                        for (i = 0; i < data.length; i++) {
                            $('#tbl_reject').append('<tr><td style="text-align:center" id="' + data[i].technician_id + '" style="cursor: pointer;" onclick=hover_tech(this.id,"' + data[i].first_name + '","' + data[i].skill_level + '","' + data[i].contact_number + '","' + data[i].location + '","' + data[i].product_name + '","' + data[i].cat_name + '");>' + data[i].technician_id + '</td><td style="text-align:center">' + data[i].first_name + '</td><td style="text-align:center">' + data[i].start_time + '</td><td style="text-align:center">' + data[i].end_time + '</td><td style="text-align:center">' + data[i].travelling_charges + '</td><td style="text-align:center"><button class="btn btn-default red-stripe" id="' + data[i].technician_id + '" onclick=viewimage(this.id);>View</button></td></tr>');
                        }
                    }
                }
            });
        }

        function viewimage(id) {

            $.ajax({
                url: "<?php echo base_url();?>" + "index.php?/controller_manager/view",
                type: 'POST',
                data: {
                    'id': id
                },
                //dataType: "JSON",
                success: function(data) {

                    var data = JSON.parse(data);
                    //console.log(data['view']);  
                    $("#token").attr('src', data['view']);
                    if (data['view'] == "") {
                        $.dialogbox({
                            type: 'msg',
                            content: 'No Data',
                            closeBtn: true,
                            btn: ['Ok.'],
                            call: [
                                function() {
                                    $.dialogbox.close();
                                }
                            ]

                        });
                    } else {
                        $("#myModal").modal('show');
                    }

                }
            });
        }

  function hover_tech(cust_id,customer_name, contact_number,alternate_number, email_id) {

            $('#modal_tech form').html('<div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Customer ID</label><div class="col-sm-6 control-label">' + cust_id + '</div></div><div class="form-group"><label  class="col-sm-5 control-label"for="inputEmail3">Customer Name</label><div class="col-sm-6 control-label">' + customer_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contact Number </label><div class="col-sm-6 control-label">' + contact_number + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Alternate Number</label><div class="col-sm-6 control-label">' + alternate_number + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Email ID </label><div class="col-sm-6 control-label">' + email_id + '</div></div>');
            $('#myModal1').modal('show');
        }
	function hover_ticket(ticket_id,customer_name,priority,product,cat,call_type) {
		
            $('#modal_tech form').html('<div class="form-group"><label class="col-sm-5 control-label"for="inputEmail3">Ticket ID</label><div class="col-sm-6 control-label">' + ticket_id + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputEmail3">Customer Name</label><div class="col-sm-6 control-label">' + customer_name + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Priority Level </label><div class="col-sm-6 control-label">' + priority + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Product Name</label><div class="col-sm-6 control-label">' + product + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Subcategory Name </label><div class="col-sm-6 control-label">' + cat + '</div></div><div class="form-group"><label class="col-sm-5 control-label"for="inputPassword3" >Contract Type</label><div class="col-sm-6 control-label">' + call_type + '</div></div>');
            $('#myModal1').modal('show');
        }
    </script>

</body>

</html>