<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class Webservice extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->view('exam');
    }
    public function test_mail()
    {
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->database();
        echo $to='sahithya.s@kaspontech.com';
$ticket_id="Tk_0074";
        echo $username = base_url() . "/index.php?/Webservice/Feedback/?user=$to&ticket_id=$ticket_id";
        $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'us2.smtp.mailhostbox.com',
            'smtp_port' => 25,
            'smtp_user' => 'Fieldpro@kaspontech.com',
            'smtp_pass' => 'Field@2012',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('Fieldpro@kaspontech.com', 'Fieldpro');
        $this->email->to($to);
        $this->email->subject('Login Credentials');
        $this->email->message($username);
        $this->email->set_newline("\r\n");
        $this->email->send();
        if($this->email->send())
        {
        echo 'Email sent successfully';
        }
        else
        {
            echo "Not delivered";
        //show_error($this->email->print_debugger());
        }
    }
    public function display_reimbursement()
    {
        
        $this->load->helper('download');
        $this->load->library('cezpdf');
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id != "") {
            $result = $this->Punch_in->display_reimbursement($tech_id);
            $json   = array();
            foreach ($result as $row) {
                array_push($json, array(
                    'Ticket_Id' => $row['ticket_id'],
                    'source_address' => $row['source_Address'],
                    'destination_address' => $row['destin_Address'],
                    'mode' => $row['mode_of_travel'],
                    'km' => $row['no_of_km_travelled'],
                    'Travel_charge' => $row['travelling_charges']
                ));
                $img_dir  = 'assets/images/pdf/' . $row['ticket_id'] . $row['technician_id'] . '.pdf';
                $img_dir1 = stripslashes($img_dir);
                file_put_contents($img_dir1, $this->cezpdf->ezStream());
            }
            $col_names = array(
                'Ticket_Id' => 'Ticket ID',
                'source_address' => 'Source Address',
                'destination_address' => 'Destination Address',
                'mode' => 'Mode of Travel',
                'km' => 'No of Kilometers',
                'Travel_charge' => 'Ticket Charge'
            );
            
            $data = $this->cezpdf->ezTable($json, $col_names, 'Reimbursement List', array(
                'width' => 550
            ));
            $this->cezpdf->ezStream();
            
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician ID"
            );
        }
        echo json_encode($json);
    }
    
    public function home()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->home($tech_id);
        return $result;
    }
    public function chat_list()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $json = '';
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician ID"
            );
        } else {
            $result = $this->Punch_in->chat_list($tech_id);
            $json   = array(
                "status" => 1,
                "result" => $result
            );
        }
        echo json_encode($json);
    }
    public function spare_view()
    {
        $this->load->helper('url');
        $this->load->database();
        $spare_code = $this->input->post('spare_code');
        $this->load->model('Punch_in');
        if ($spare_code != "") {
            $result = $this->Punch_in->spare_view($spare_code);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        } else {
            $json = array(
                "status" => 0,
                "result" => "Please Provide Spare Code"
            );
            echo json_encode($json);
        }
    }
    public function spare_search()
    {
        $this->load->helper('url');
        $this->load->database();
        $spare_code = $this->input->post('spare_code');
        $this->load->model('Punch_in');
        if ($spare_code == '') {
            $json = array(
                "status" => 0,
                "result" => "Please provide spare code"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->spare_search($spare_code);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function get_category()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_category($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function get_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_spare($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function get_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_amc($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function product()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->product($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function category()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id    = $this->input->post('tech_id');
        $product_id = $this->input->post('product_id');
        $this->load->model('Punch_in');
        if ($tech_id == '' || $product_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->category($tech_id, $product_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 1,
                    "result" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }
    public function upload()
    {
        $this->load->helper('url');
        $this->load->database();
        $string_prob = preg_replace('/\s+/', '', $this->input->post('problem_desc'));
        $this->load->model('Punch_in');
        $r = $this->Punch_in->get_tech_product($this->input->post('tech_id'));
      
        foreach ($r as $row) {
            $product    = $row['product_id'];
            $company_id = $row['company_id'];
        }
        $baseurl = base_url() . 'assets/images/knowledge_base/';
        if (isset($_FILES["fileUpload_image"]['name'])) {
            $image      = $_FILES["fileUpload_image"];
            $output_dir = "./assets/images/knowledge_base/";
            $img_dir    = $baseurl . 'image' . $string_prob . '.jpg';
        } else {
            $img_dir = "";
        }
        if (isset($_FILES["fileUpload_video"]['name'])) {
            
            $video      = $_FILES["fileUpload_video"];
            $output_dir = "./assets/images/knowledge_base/";
            $video_dir  = $baseurl . 'video' . $string_prob . '.mp4';
        } else {
            $video_dir = "";
        }
        if (isset($_FILES["fileUpload_image"]['name'])) {
            move_uploaded_file($_FILES["fileUpload_image"]["tmp_name"], $output_dir . 'image' . $string_prob . '.jpg');
        }
        if (isset($_FILES["fileUpload_video"]['name'])) {
            move_uploaded_file($_FILES["fileUpload_video"]["tmp_name"], $output_dir . 'video' . $string_prob . '.mp4');
        }
        //$prod_cat = $this->Punch_in->get_prod_cat($product, $this->input->post('category'));
        $prod_cat = $this->Punch_in->get_prod_cat($product, $this->input->post('sub_category'));
        //print_r($prod_cat);
        if (!empty($prod_cat)) {
            $regioncitydata=$this->Punch_in->gettechdetails($this->input->post('tech_id'));
            $region=$regioncitydata['region'];
            $area=$regioncitydata['area'];
           
            $data   = array(
                'technician_id' => $this->input->post('tech_id'),
                'ticket_id' => $this->input->post('ticket_id'),
                'product' => $prod_cat[0]['prod_id'],
                'company_id' => $company_id,
                'category' => $prod_cat[0]['cat_id'],
                'sub_category' => $this->input->post('sub_category'),
                'problem' => $this->input->post('problem_desc'),
                'solution' => $this->input->post('solution'),
                'image' => $img_dir,
                'video' => $video_dir,
                'region'=>$region,
                'area'=>$area
            );
            $result = $this->Punch_in->upload($data);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
                $notify      = 'New Knowledge base request raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $this->input->post('tech_id'));
                $json = '';
                $json = array(
                    "status" => "1",
                    "msg" => "Uploaded Successfully!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => "0",
                    "msg" => "Error during Upload!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => "0",
                "msg" => "Error in Category!!!"
            );
        }
        echo json_encode($json);
    }
    public function search()
    {
        $this->load->helper('url');
        $this->load->database();
        $key = $this->input->post('key');
        $sub = $this->input->post('sub');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->search($key, $sub);
        if (!empty($result)) {
            $json = '';
            $json = array(
                "status" => 1,
                "result" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "No result found!!!"
            );
        }
        echo json_encode($json);
    }
    public function punch_in()
    {
        $this->load->helper('url');
        $latitude  = 0;
        $longitude = 0;
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $type    = $this->input->post('punch_type');
        
        if ($tech_id != "" || $type != "" || $current_location != "") {
            $this->load->model('Punch_in');
            $tech_check = $this->Punch_in->check_tech($tech_id);
            if ($tech_check > 0) {
                $latitude  = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                //$latitude="26.754347";
                //$longitude="81.001640";
                //sleep(1);
                
                // $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false';
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $output1 = json_decode($response);
                
                $status1 = $output1->status;
                if ($status1 == 'OK' && !empty($output1->results[1])) {
                    //Get address from json data
                    $current_location = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';
                    
                    if ($type == 'punch_in') {
                        $result = $this->Punch_in->check_punch_in($tech_id);
                        if ($result == 0) {
                            $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
                            $res        = $this->Punch_in->punch_in($tech_id, $company_id);
                            if ($res == 1) {
                                $res3 = $this->Punch_in->available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 1,
                                    "msg" => "Punched in Successfully!!!"
                                );
                            }
                        } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch in!!!"
                            );
                        }
                    } else if ($type == 'punch_out') {
                        $result = $this->Punch_in->check_punch_out($tech_id);
                        if ($result != 0) {
                            $res = $this->Punch_in->punch_out($tech_id, $result);
                            if ($res == 1) {
                                $res3 = $this->Punch_in->nt_available($tech_id, $current_location);
                                $json = '';
                                $json = array(
                                    "status" => 1,
                                    "msg" => "Punched out Successfully!!!"
                                );
                            }
                        } else {
                            $json = '';
                            $json = array(
                                "status" => 0,
                                "msg" => "Error during Punch out!!!"
                            );
                        }
                    }
                }
                
                else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Unable to fetch Current Location!!!"
                    );
                }
            }
            
            else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Provide proper technician id!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all credentials!!!"
            );
        }
        
        echo json_encode($json);
    }
    
    public function get_new_tkts()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->get_new_tkts($tech_id);
        return $result;
    }
    public function android_get_new_tkts()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->android_get_new_tkts($tech_id);
        return $result;
    }

    public function tickets_forthe_day()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->tickets_forthe_day($tech_id);
        return $result;
    }

    public function ongoing_tickets()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->ongoing_tickets($tech_id);
        return $result;
    }
    public function reschedule_tickets()

    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $company_id = $this->input->post('company_id');
        $new_date = $this->input->post('reschedule_date');
        $this->load->model('Punch_in');
        if ($tech_id != "" && $ticket_id != "" && $company_id != "" && $new_date!="") {
        $result = $this->Punch_in->reschedule_tickets($tech_id,$company_id,$ticket_id,$new_date);
       
        if($result==1){
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Ticket rescheduled"
            );
        }  
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error in rescheduling!"
            );
        }
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Provide all details!"
        );
    }
    echo json_encode($json);
}
      
    

    public function accept_ticket()
    {
        //$this->load->helper('url');
        $this->load->database();
        $tech_id   = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $reason    = $this->input->post('reason');
        $this->load->model('Punch_in');
        if ((!empty($ticket_id)) || (!empty($ticket_id))) {
            $result      = $this->Punch_in->accept_ticket($tech_id, $ticket_id, $reason);
            $company_id  = $this->Punch_in->get_company($this->input->post('tech_id'));
            $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
            
            if ($reason == 'Accept') {
                $notify = $ticket_id . ' is accepted by ' . $employee_id;
            } else {
                $notify = $ticket_id . ' is rejected by ' . $employee_id;
            }
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            if (!empty($result)) {
                $json = '';
                $json = $result;
                echo $json;
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error !!!"
            );
            echo json_encode($json);
        }
    }
    public function start_travel()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('Punch_in');
        
        $start_time = date("Y-m-d");
        $company_id = $this->Punch_in->get_company($this->input->post('technician_id'));
        $data       = array(
            'technician_id' => $this->input->post('technician_id'),
            'ticket_id' => $this->input->post('ticket_id'),
            'source_lat' => $this->input->post('source_lat'),
            'source_long' => $this->input->post('source_long'),
            'no_of_km_travelled' => $this->input->post('no_of_km_travelled'),
            'estimated_time' => $this->input->post('estimated_time'),
            'mode_of_travel' => $this->input->post('mode_of_travel'),
            'start_time' => $start_time,
            'status' => 5,
            'company_id' => $company_id
        );
        $this->load->model('Punch_in');
        if ($this->input->post('technician_id') != '' || $this->input->post('ticket_id') != '' || $this->input->post('source_lat') != '' || $this->input->post('source_long') != '' || $this->input->post('no_of_km_travelled') != '' || $this->input->post('estimated_time') != '' || $this->input->post('mode_of_travel') != '' || $this->input->post('start_time') != '') {
            $result = $this->Punch_in->start_travel($data, $this->input->post('technician_id'));
            if ($result == 1) {
                $this->load->model('Punch_in');
                $travel_status = array(
                    'current_status' => 5,
                    'ticket_start_time' => date('Y-m-d H:i:s')
                );
                $res           = $this->Punch_in->change_travel_status($travel_status, $ticket_id);
                if ($res == 1) {
                    $json = '';
                    $json = array(
                        "status" => 1,
                        "msg" => "Travel Successfully started!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        }
        echo json_encode($json);
        
    }
    public function end_travel()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id     = $this->input->post('ticket_id');
        $technician_id = $this->input->post('technician_id');
        $end_time      = date("Y-m-d");
        $data          = array(
            'end_time' => $end_time,
            'dest_lat' => $this->input->post('dest_lat'),
            'dest_long' => $this->input->post('dest_long')
        );
        $this->load->model('Punch_in');
        if ($this->input->post('technician_id') != '' || $this->input->post('ticket_id') != '' || $this->input->post('start_time') != '') {
            $result = $this->Punch_in->end_travel($data, $ticket_id, $technician_id);
            if ($result == 1) {
                $this->load->model('Punch_in');
                $travel_status = array(
                    'current_status' => 6,
                    'ticket_end_time' => date('Y-m-d H:i:s')
                );
                $res           = $this->Punch_in->change_travel_status($travel_status, $ticket_id);
                if ($res == 1) {
                    $json = '';
                    $json = array(
                        "status" => 1,
                        "msg" => "Reached Successfully!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        }
        echo json_encode($json);
        
    }
    public function bill()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id      = $this->input->post('ticket_id');
        $technician_id  = $this->input->post('technician_id');
        $travel_charges = $this->input->post('travel_charges');
        $file           = '';
        $img_dir        = "";
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/bill/' . $ticket_id . '.' . $filename;
            $img_dir1  = stripslashes($img_dir);
            move_uploaded_file($temp_name, $img_dir1);
        }
        
        $this->load->model('Punch_in');
        if ($technician_id == '' || $ticket_id == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $company_id = $this->Punch_in->get_company($this->input->post('technician_id'));
            $res        = $this->Punch_in->bill($technician_id, $ticket_id, $img_dir, $travel_charges, $company_id);
            $json       = '';
            if ($res == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Reimbursement Request done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }
    
    public function reimbursement_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $technician_id = $this->input->post('tech_id');
        $start_date    = $this->input->post('start_date');
        $start_date    = date("Y-m-d", strtotime($start_date));
        $end_date      = $this->input->post('end_date');
        $end_date      = date("Y-m-d ", strtotime($end_date));
        $comment       = "";
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($technician_id == '' || $start_date == '' || $end_date == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $this->load->model('Punch_in');
            $res  = $this->Punch_in->reimbursement_request($technician_id, $start_date, $end_date, $comment, $company_id);
            $json = '';
            if ($res == 1) {
                $employee_id = $this->Punch_in->get_emp_id($this->input->post('tech_id'));
                $notify      = $employee_id . ' applied new reimbursement claim request!!!';
                $role        = "Manager";
                $key         = "";
                $this->load->model('Model_service');
                $this->Model_service->update_notify($notify, $company_id, $role, $technician_id, $key);
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Reimbursement Request done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }
    public function ticket_start()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id    = $this->input->post('tech_id');
        $ticket_id  = $this->input->post('ticket_id');
        $start_time = $this->input->post('start_time');
        $this->load->model('Punch_in');
        if ($tech_id == '' || $ticket_id == '' || $start_time == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        } else {
            $result = $this->Punch_in->ticket_start($tech_id, $ticket_id, $start_time);
            if ($result == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Ticket Started!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
    }
    public function complete_ticket()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id   = $this->input->post('tech_id');
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($tech_id);
        
        if ($tech_id == '' || $ticket_id == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->complete_ticket($tech_id, $ticket_id, $company_id);
            if (!empty($result)) {
                return $result;
            }
        }
        
    }
    public function show_skill()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $result = $this->Punch_in->show_skill();
        return $result;
    }
    public function submit_escalate()
    {
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $img_dir1  = '';
        $audio1    = '';
        $video1    = '';
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/escalated_image/' . $ticket_id .'.'. $filename;
            $img_dir1  = stripslashes($img_dir);
            move_uploaded_file($temp_name, $img_dir1);
        }
        if (!empty($_FILES["myAudio"])) {
            $file_name1 = $_FILES["myAudio"]["name"];
            $temp_name1 = $_FILES['myAudio']['tmp_name'];
            $filename1  = end((explode(".", $file_name1)));
            $audio      = 'assets/images/escalated_audio/' . $ticket_id . '.'. $filename1;
            $audio1     = stripslashes($audio);
            move_uploaded_file($temp_name1, $audio1);
        }
        if (!empty($_FILES["myVideo"])) {
            $file_name2 = $_FILES["myVideo"]["name"];
            $temp_name2 = $_FILES['myVideo']['tmp_name'];
            $filename2  = end((explode(".", $file_name2)));
            $video      = 'assets/images/escalated_video/' . $ticket_id . '.'. $filename2;
            $video1     = stripslashes($video);
            move_uploaded_file($temp_name2, $video1);
        }
        if ($img_dir1 != '' && $audio1 != '' && $video1 != '') {
            $data = array(
                'image' => $img_dir1,
                'audio' => $audio1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 != '' && $video1 != '') {
            $data = array(
                'audio' => $audio1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 == '' && $video1 != '') {
            $data = array(
                'image' => $img_dir1,
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 != '' && $video1 == '') {
            $data = array(
                'image' => $img_dir1,
                'audio' => $audio1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 != '' && $audio1 == '' && $video1 == '') {
            $data = array(
                'image' => $img_dir1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 != '' && $video1 == '') {
            $data = array(
                'audio' => $audio1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 == '' && $video1 != '') {
            $data = array(
                'video' => $video1,
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        } else if ($img_dir1 == '' && $audio1 == '' && $video1 == '') {
            $data = array(
                'escalated_description' => $this->input->post('escalated_description'),
                'suggested_skill_level' => $this->input->post('suggested_skill_level'),
                'sug_tech_name' => $this->input->post('sug_tech_name'),
                'prev_tech_id' => $tech_id,
                'current_status' => 15,
                'previous_status' => 15
            );
        }
        $result = $this->Punch_in->submit_escalate($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $company_id  = $this->Punch_in->get_company($tech_id);
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            
            $notify = $ticket_id . ' is escalated by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'MAnager', $tech_id);
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Escalation Request Successfully done!!!"
            );
            
            $res1=$this->Punch_in->update_tech_task($tech_id);  
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        }
        
        echo json_encode($json);
    }
    public function work_in_progress()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $data      = array(
            'prev_tech_id' => $this->input->post('tech_id'),
            'reason' => $this->input->post('reason'),
            'poa' => $this->input->post('poa'),
            'schedule_next' => $this->input->post('schedule_next'),
            'cust_acceptance' => $this->input->post('cust_acceptance'),
            'estimation_time' => $this->input->post('estimate_time'),
            'current_status' => 10,
            'requested_spare' => $this->input->post('spare'),
            'spare_array' => $this->input->post('spare'),
            'cust_preference_date' => $this->input->post('schedule_next'),
            'lead_time' => $this->input->post('schedule_next')
        );
        $this->load->model('Punch_in');
        if ($this->input->post('tech_id') == '' || $this->input->post('ticket_id') == '' || $this->input->post('reason') == '' || $this->input->post('poa') == '' || $this->input->post('schedule_next') == '' || $this->input->post('cust_acceptance') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all fields!!!"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->work_in_progress($data, $ticket_id);
            if ($result == 1) {
                $company_id  = $this->Punch_in->get_company($tech_id);
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = $ticket_id . ' is made  inprogress  by ' . $employee_id . ' on ' . $this->input->post('schedule_next');
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = '';
                $json = array(
                    "status" => "1",
                    "msg" => "Work in Progress Request Successfully done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => "0",
                    "msg" => "Error!!!"
                );
            }
            
            $res1=$this->Punch_in->update_tech_task($tech_id);  
            echo json_encode($json);
        }
    }
    
    public function Feedback()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->view('Feedback');
    }
    
    public function close_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('model_service');
        $this->load->model('Punch_in');
        $ticket_id=$this->input->post('ticket_id');
        $company_id=$this->Punch_in->get_company_tkt($ticket_id);
        $cust_feedback=$this->input->post('password');
        $cust_Rating=$this->input->post('c_password');
       
        $cust_reason="";
        $result=$this->model_service->close_tkt($company_id, $ticket_id,$cust_feedback,$cust_Rating,$cust_reason);
        if($result==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function submit_complete()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');
        $service_charge = $this->input->post('service_charge');
        $spare_charge   = $this->input->post('spare_charge');
        $tax            = $this->input->post('tax');
        $total_charge   = $this->input->post('total_charge');
        $signature      = '';

        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $tmp = explode('.', $file_name);
            $filename  =end($tmp);
            $img_dir   = 'assets/images/' . 'completed_' . $ticket_id . '.'. $filename;
            $signature = stripslashes($img_dir);
            move_uploaded_file($temp_name, $signature);
        }

        $frm              = $this->input->post('frm');
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d H:i:s", strtotime($drop_of_date));
        $drop_of_location = $this->input->post('drop_of_location');
        $ticket_end_time  = date("Y-m-d H:i:s");
        $company_id       = $this->Punch_in->get_company($tech_id);

        if ($signature != '') {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'signature' => $signature,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        } else {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        }

        $result = $this->Punch_in->submit_complete($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $product     = $this->Punch_in->get_tech_product($tech_id);
            $company_id  = $product[0]['company_id'];
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = $ticket_id . ' is completed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            $json = '';
            $code = mt_rand(10001,99999);
            $data = array(
                'service_charge' => $service_charge,
                'spare_cost' => $spare_charge,
                'tax' => $tax,
                'total_amount' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time,
                //'current_status' => 8,
                'otp_code'=> $code
            );
            $cus_mo  = $this->Punch_in->get_det_cust($ticket_id);
            $cust_mobile = $cus_mo['contact_number'];
            $res  = $this->Punch_in->submit_complete1($ticket_id, $tech_id, $data,$cust_mobile,$code);
            if ($res == 1) {
                $json = array(
                    "status" => 1,
                    "OTP" => $code,
                    "msg" => "Completed Successfully done!!!"
                );
                $cst  = $this->Punch_in->get_det_cust($ticket_id);
                $cust_id=$cst['customer_id'];
                 $to=$cst['email_id'];
                 
       $emailcontent = "Dear Customer if you satisfy with our service please share this OTP with technician - ".$code;
          $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
        $this->email->to($to);
        $this->email->subject('FieldPro Ticket Status');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
        $this->email->send();
       
            }
            
            $this->load->model('Punch_in');
            $res1=$this->Punch_in->update_tech_task($tech_id);
          
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error!!!"
            );
        }
        
        echo json_encode($json);
    }

    public function frm_complete_ticket(){
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');      
        $frm            = $this->input->post('frm');
        $frm_status     = $this->input->post('frm_status');
   
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d H:i:s", strtotime($drop_of_date));
        $drop_spare       = $this->input->post('drop_spare');
        $drop_of_location = $this->input->post('drop_of_location');
        $company_id       = $this->Punch_in->get_company($tech_id);
        
        if($frm_status == 'now'){
            $fstatus = "RETURNED";
        }
        elseif($frm_status == 'later'){
            $fstatus = "PENDING";
        }
        else{
            $fstatus = "NO FRM";
        }

        $billing_data = array(
            'frm' => $frm,
            'drop_spare' => $drop_spare,
            'drop_of_date' => $drop_of_date,
            'drop_of_location' => $drop_of_location,
            'frm_status'    => $fstatus
           
        );

        $ticket_data = array(
            'frm' => $frm,
            'drop_spare' => $drop_spare,
            'drop_of_date' => $drop_of_date,
            'drop_of_location' => $drop_of_location,
            'current_status' =>8
        );

        $result = $this->Punch_in->frm_complete_ticket($ticket_id, $tech_id, $company_id, $billing_data, $ticket_data);
      
        if($result == 1){
            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Success"
            );
        }
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Failure"
            );
        }

        echo json_encode($json);
    }

    public function get_tech_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $start   = $this->input->post('start');
        $end     = $this->input->post('end');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->get_tech_tkt($tech_id, $start, $end);
        $json   = '';
        $json   = array(
            "status" => 1,
            "result" => $result
        );
        echo json_encode($json);
    }
    public function pdf_report()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $tech_id    = $this->input->post('tech_id');
        $from       = $this->input->post('from');
        $to         = $this->input->post('to');
        $result     = $this->Punch_in->pdf_report($tech_id, $company_id, $from, $to);
        $json       = '';
        $json1      = '';
           
        if (!empty($result)) {
          if($result[0]['status']==10 )
          {
              $status='Work in progress';
          }
          elseif($result[0]['status']==11)
          {
              $status="Spare accepted";
          }
          elseif($result[0]['status']==12)
          {
              $status ="Closed";
          }
          elseif($result[0]['status']==14)
          {
              $status="Spare requested";
          }
          elseif($result[0]['status']==15)
          {
              $status="Spare requested";
          }
          elseif($result[0]['status']==18)
          {
              $status="Spare requested";
          }
          elseif($result[0]['status']==8)
          {
              $status="Completed";
          }
            
          if (!empty($result)) {
            

           $table ='<html>
           <head>
           <style>
           table.bottomBorder { 
            border-collapse: collapse; 
          }
          table.bottomBorder td, 
          table.bottomBorder th { 
            border-bottom: 1px solid yellowgreen; 
            padding: 10px; 
            text-align: left;
          }

           </style>
           </head> 
           <strong>FieldPro - Service Report</strong>
           <br/>
           <table>         
           <tr>
           <td>
           Employee ID:
           </td>
           <td>
           <strong>'.$result[0]['employee_id'].'</strong>
           </td>
           </tr>
           <tr>
           <td>
           Technician name:
           </td>
           <td>
           <strong>'.$result[0]['first_name'].'</strong>
           </td>
           </tr>
           </table>
           <br/>

           <table class="bottomBorder" align="center">
         
           <thead>
             <tr style="border: 1px solid black; background-color: #01B6AD">
               <th style="border: 1px solid black">Date</th>
               <th style="border: 1px solid black">Ticket id</th>
               <th style="border: 1px solid black">Customer name</th>
               <th style="border: 1px solid black">Status</th>
             </tr>
           </thead>
           <tbody>';
           foreach ($result as $row) {
                
            $table = $table . ' <tr>
                                <td style="border: 1px solid black">'.$result[0]['date'].'</td>
                                <td style="border: 1px solid black">'.$row['ticket_id'].'</td>
                                <td style="border: 1px solid black">'.$row['customer_name'].'</td>
                                <td style="border: 1px solid black">'.$status.'</td>
                               </tr>';
            
           }
           $table = $table . '</tbody></table></html>';

            $json = array(
                "status" => 1,
                "result" => $table
            );
        }
           
        }
        else{
            $table = '<html><body><form><p>Employee ID:'.$result[0]['employee_id'].'</p><p> Technician name:'.$result[0]['first_name'].'</p><br/><br/></form><table style="width:100%;border: 0px solid black; border-collapse: collapse;"><tr style="border: 1px solid black; background-color: #eee;"><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Customer Name</th><th style="border: 1px solid black"> Call Status</th><th style="border: 1px solid black">Date</th></tr>';
                
            $table = $table.'<tr style="border: 1px solid black; background-color: #eee;"><td colspan="4" style="border: 1px solid black">No data found</td></tr>';
                          
            $table = $table.'</table></body> </html>';
        }

      
        $json = array(
            "status" => 1,
            "result" => $table
        );
        echo json_encode($json);
    }

    public function reimbursement_report()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $tech_id    = $this->input->post('tech_id');
        $from       = $this->input->post('from');
        $to         = $this->input->post('to');
        $result     = $this->Punch_in->reimbursement_report($tech_id, $company_id, $from, $to);
        $json       = '';
        $json1      = '';
        if (!empty($result)) {
            
            $json1 = '<html><body><form><p>Employee ID:'. $result[0]['employee_id'] .'</p><p> Technician name:' . $result[0]['first_name'] . '</p><br/><br/> </form> <table style="width:100%;border: 0px solid black; border-collapse: collapse;"> <tr style="border: 1px solid black; background-color: #eee;"> <th style="border: 1px solid black">S.No</th><th style="border: 1px solid black">Date</th><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Customer Name</th> <th style="border: 1px solid black">From</th> <th style="border: 1px solid black">To</th>          <th style="border: 1px solid black">Distance</th>          <th style="border: 1px solid black">Mode of Travel</th>          <th style="border: 1px solid black">Amount</th>         </tr>   ';
            
            // foreach ($result as $row) {
                
            //     $json1 = $json1 . '    <tr style="border: 1px solid black; background-color: #eee;"><td style="border: 1px solid black">' . $row['reim_id'] . '</td> <td style="border: 1px solid black">' . $row['date'] . '</td><td style="border: 1px solid black">' . $row['ticket_id'] . '</td> <td style="border: 1px solid black">' . $row['customer_name'] . '</td> <td style="border: 1px solid black">' . $row['source_Address'] . '</td> <td style="border: 1px solid black">' . $row['destin_Address'] . '</td> <td style="border: 1px solid black">' . $row['no_of_km_travelled'] . '</td> <td style="border: 1px solid black">' . $row['mode_of_travel'] . '</td> <td style="border: 1px solid black">' . $row['travelling_charges'] . '</td></tr>  ';
                
            // }
            // $json1 = $json1 . '</table> </body> </html>';


            $table ='<html>
            <head>
            <style>
           table.bottomBorder { 
            border-collapse: collapse; 
          }
          table.bottomBorder td, 
          table.bottomBorder th { 
            border-bottom: 1px solid yellowgreen; 
            padding: 10px; 
            text-align: left;
          }

           </style>

            </head> 

            <strong>FieldPro - Reimbursement Report</strong>
            <br/>
            <table>         
            <tr>
            <td>
            Employee ID:
            </td>
            <td>
            <strong>'.$result[0]['employee_id'].'</strong>
            </td>
            </tr>
            <tr>
            <td>
            Technician name:
            </td>
            <td>
            <strong>'.$result[0]['first_name'].'</strong>
            </td>
            </tr>
            </table>
            <br/>
            <table style="table-layout:fixed;border-collapse:collapse;">
            <thead>
            <tr style="border: 1px solid black; background-color: #01B6AD">
            <th style="border: 1px solid black">S.No</th>
            <th style="border: 1px solid black">Date</th>
            <th style="border: 1px solid black">Ticket ID</th>
            <th style="border: 1px solid black">Customer Name</th>
            <th style="border: 1px solid black">From</th>
            <th style="border: 1px solid black">To</th>
            <th style="border: 1px solid black">Distance</th>
            <th style="border: 1px solid black">Mode of travel</th>
            <th style="border: 1px solid black">Amount</th>
             </tr>
            </thead>
            <tbody>';
            $i=1;
            $sum =0;
            foreach ($result as $row) {
                 
             $table = $table.' <tr>
                                 <td style="border: 1px solid black">'.$i.'</td>
                                 <td style="border: 1px solid black">'.$row['date'].'</td>
                                 <td style="border: 1px solid black; width: 80px">'.$row['ticket_id'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word;width: 80px">'.$row['customer_name'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 100px">'.$row['source_Address'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 100px">'.$row['destin_Address'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word">'.$row['no_of_km_travelled'].'</td>
                                 <td style="border: 1px solid black; word-wrap:break-word; width: 80px">'.$row['mode_of_travel'].'</td>
                                 <td style="border: 1px solid black">'.$row['travelling_charges'].'</td>                               
                                </tr>';
                                 $sum+= $row['travelling_charges'];
                                $i++;
             
            }
            $table = $table . '</tbody></table>
            <p id="total" style="text-align: right"><strong>Total Amount(INR):</strong>  '.$sum.' </p>
            </html>';
        }
        else{
            $table = '<html><body><table style="width:100%;border: 0px solid black; border-collapse: collapse;"> <tr style="border: 1px solid black; background-color: #eee;"> <th style="border: 1px solid black">S.No</th><th style="border: 1px solid black">Date</th><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Customer Name</th> <th style="border: 1px solid black">From</th> <th style="border: 1px solid black">To</th>          <th style="border: 1px solid black">Distance</th>          <th style="border: 1px solid black">Mode of Travel</th><th style="border: 1px solid black">Amount</th></tr>';
                
            $table = $table.'<tr style="border: 1px solid black; background-color: #eee;"><td colspan="4" style="border: 1px solid black">No data found</td></tr>';
                          
            $table = $table.'</table></body> </html>';
        }
        $json = array(
            "status" => 1,
            "result" => $table
        );
        echo json_encode($json);
    }
    
    public function amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $amc_id     = $this->Punch_in->select_amc();
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $product_id = $ids[0]['product_id'];
        $cat_id     = $ids[0]['cat_id'];
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $myStr      = $this->input->post('amc_type');
        $tic_id=$this->input->post('ticket_id');
        $result     = substr($myStr, 0, 4);
        $gen_amc_id = $result . '_' . $amc_id;
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($this->input->post('tech_id') == '' || $this->input->post('amc_type') == '' || $this->input->post('contract_period') == '' || $this->input->post('product') == '' || $this->input->post('category') == '' || $this->input->post('quantity') == '' || $this->input->post('total_amount') == '' || $this->input->post('mode_of_payment') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details!!!"
            );
        } else {
            if ($this->input->post('cust_id') == '') {
                $this->load->model('model_man');
                $userid = $this->model_man->get_userid($this->input->post('email_id'));
                if (count($userid) == 0) {
                    $cutomer = array(
                        'customer_name' => $this->input->post('cust_name'),
                        'email_id' => $this->input->post('email_id'),
                        'contact_number' => $this->input->post('contact_number'),
                        'address' => $this->input->post('street'),
                        'product_serial_no' => $product_id,
                        'component_serial_no' => $cat_id,
                        'serial_no' => $this->input->post('serial_no'),
                        'model_no' => $this->input->post('model_no'),
                        'door_num' => $this->input->post('door_no'),
                        'cust_town' => $this->input->post('city'),
                        'cust_country' => $this->input->post('country'),
                        'company_id' => $company_id
                    );
                    
                    $this->load->model('Punch_in');
                    $gen_cust = $this->Punch_in->gen_customer($cutomer);
                    $amc      = $this->Punch_in->select_amc();
                    $this->load->model('New_amc');
                    if($tic_id=="")
                    {
                        $ticket_id  = $this->New_amc->tick_id($company_id);
                    }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                    $check_date = date("Y-m-d H:i:s", strtotime($this->input->post('check_date')));
                    $data       = array(
                        'ticket_id' => $ticket_id,
                        'amc_id' => $gen_amc_id,
                        'tech_id' => $this->input->post('tech_id'),
                        'call_type' => $this->input->post('amc_type'),
                        'contact_no' => $this->input->post('contact_number'),
                        'cust_id' => $gen_cust,
                        'address' => $this->input->post('street'),
                        'door_no' => $this->input->post('door_no'),
                        'town' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'contract_period' => $this->input->post('contract_period'),
                        'product_id' => $product_id,
                        'cat_id' => $cat_id,
                        'quantity' => $this->input->post('quantity'),
                        'total_amount' => $this->input->post('total_amount'),
                        'mode_of_payment' => $this->input->post('mode_of_payment'),
                        'check_no' => $this->input->post('check_no'),
                        'check_date' => $check_date,
                        'transaction_id' => $this->input->post('transaction_id'),
                        'serial_no' => $this->input->post('serial_no'),
                        'model' => $this->input->post('model_no'),
                        'current_status' => 12,
                        'company_id' => $company_id
                    );
                    
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Email ID already registered!!!"
                    );
                }
            } else {
                $this->load->model('Punch_in');
                $amc = $this->Punch_in->select_amc();
                $this->load->model('New_amc');
                if($tic_id=="")
                {
                    $ticket_id  = $this->New_amc->tick_id($company_id);
                }
                    else
                    {
                        $ticket_id=$tic_id;
                    }
                $data      = array(
                    'ticket_id' => $ticket_id,
                    'amc_id' => $gen_amc_id,
                    'tech_id' => $this->input->post('tech_id'),
                    'call_type' => $this->input->post('amc_type'),
                    'contact_no' => $this->input->post('contact_number'),
                    'cust_id' => $this->input->post('cust_id'),
                    'address' => $this->input->post('street'),
                    'door_no' => $this->input->post('door_no'),
                    'town' => $this->input->post('city'),
                    'country' => $this->input->post('country'),
                    'contract_period' => $this->input->post('contract_period'),
                    'product_id' => $product_id,
                    'cat_id' => $cat_id,
                    'quantity' => $this->input->post('quantity'),
                    'total_amount' => $this->input->post('total_amount'),
                    'mode_of_payment' => $this->input->post('mode_of_payment'),
                    'check_no' => $this->input->post('check_no'),
                    'check_date' => $this->input->post('check_date'),
                    'transaction_id' => $this->input->post('transaction_id'),
                    'serial_no' => $this->input->post('serial_no'),
                    'model' => $this->input->post('model_no'),
                    'current_status' => 12,
                    'company_id' => $company_id
                );
            }
            $result = $this->Punch_in->amc($data,$ticket_id);
            if ($result == 1) {
                
                $result=$this->Punch_in->updte_amc($tic_id);
                $tech_id     = $this->input->post('tech_id');
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = 'New Contract raised  by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = array(
                    "status" => 1,
                    "msg" => "AMC Successfully done!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
            
        }
        echo json_encode($json);
    }
    public function search_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $search_var = $this->input->post('amc_id');
        //$search_var='Che/Part/sep/2017/0001';
        $this->load->model('Punch_in');
        $result = $this->Punch_in->search_amc($search_var);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "result" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
    }

public function find_amc(){
    $this->load->helper('url');
    $this->load->database();
    $search_var = $this->input->post('contact_number');
    $this->load->model('Punch_in');
    $result = $this->Punch_in->find_amc($search_var);
    if($result == 1){
        $json = array(
            "status" => 1,
            "result" => "AMC already exist"
        );
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Not exist"
        );
    }
    echo json_encode($json);
}

    public function amc_price()
    {
        $this->load->helper('url');
        $this->load->database();
        $cat_name = $this->input->post('cat_name');
        $quantity = $this->input->post('quantity');
        $tech_id  = $this->input->post('tech_id');
        $amc_type = $this->input->post('amc_type');
        $this->load->model('Punch_in');
        $result = $this->Punch_in->amc_price($cat_name, $tech_id, $amc_type, $quantity);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "total_amount" => $result
            );
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid category!!!"
            );
        }
        echo json_encode($json);
    }
    public function update_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $search_var      = $this->input->post('amc_id');
        $tech_id         = $this->input->post('tech_id');
        $contract_period = $this->input->post('contract_period');
        $type_cont       = $this->input->post('type_cont');
        $total_amount    = $this->input->post('total_amount');
        $mode_of_payment = $this->input->post('mode_of_payment');
        $check_no        = $this->input->post('check_no');
        $check_date      = $this->input->post('check_date');
        $search_var      = $this->input->post('amc_id');
        $this->load->model('Punch_in');
        $resu       = $this->Punch_in->search_amc($search_var);
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $product_id = $ids[0]['product_id'];
        $cat_id     = $ids[0]['cat_id'];
        if (!empty($resu)) {
            foreach ($resu as $row) {
                $data   = array(
                    'tech_id' => $tech_id,
                    'call_type' => $row['type_cont'],
                    'contact_no' => $row['contact_num'],
                    'address' => $row['street'],
                    'door_no' => $row['door_no'],
                    'town' => $row['town'],
                    'country' => $row['country'],
                    'contract_period' => $contract_period,
                    'product_id' => $product_id,
                    'cat_id' => $cat_id,
                    'quantity' => $row['quantity'],
                    'total_amount' => $total_amount,
                    'bill_no' => $row['bill_no'],
                    'mode_of_payment' => $mode_of_payment,
                    'check_no' => $check_no,
                    'check_date' => $check_date,
                    'transaction_id' => $this->input->post('transaction_id'),
                    'serial_no' => $this->input->post('serial_no'),
                    'model' => $this->input->post('model_no'),
                    'current_status' => 12
                );
                $result = $this->Punch_in->update_amc($data, $search_var);
                if ($result == 1) {
                    $tech_id     = $this->input->post('tech_id');
                    $company_id  = $this->Punch_in->get_company($tech_id);
                    $employee_id = $this->Punch_in->get_emp_id($tech_id);
                    $notify      = ' Contract updated  by ' . $employee_id;
                    $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                    $json = array(
                        "status" => 1,
                        "msg" => "AMC Successfully done!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
        
    }
    public function renew_amc()
    {
        $this->load->helper('url');
        $this->load->database();
        $search_var      = $this->input->post('amc_id');
        $tech_id         = $this->input->post('tech_id');
        $contract_period = $this->input->post('contract_period');
        $type_cont       = $this->input->post('type_cont');
        $total_amount    = $this->input->post('total_amount');
        $mode_of_payment = $this->input->post('mode_of_payment');
        $check_no        = $this->input->post('check_no');
        $check_date      = $this->input->post('check_date');
        $search_var      = $this->input->post('amc_id');
        $this->load->model('Punch_in');
        $new_amc_id = $this->Punch_in->select_amc();
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $product_id = $ids[0]['product_id'];
        $cat_id     = $ids[0]['cat_id'];
        $ids        = $this->Punch_in->get_ids($this->input->post('product'), $this->input->post('category'));
        $resu       = $this->Punch_in->search_amc($search_var);
        if (!empty($resu)) {
            foreach ($resu as $row) {
                $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
                $this->load->model('New_amc');
                $ticket_id = $this->New_amc->tick_id($company_id);
                $data      = array(
                    'ticket_id' => $ticket_id,
                    'amc_id' => $search_var,
                    'tech_id' => $tech_id,
                    'cust_id' => $row['customer_id'],
                    'call_type' => $row['type_cont'],
                    'contact_no' => $row['contact_num'],
                    'address' => $row['street'],
                    'door_no' => $row['door_no'],
                    'town' => $row['town'],
                    'country' => $row['country'],
                    'contract_period' => $contract_period,
                    'product_id' => $product_id,
                    'cat_id' => $cat_id,
                    'quantity' => $row['quantity'],
                    'total_amount' => $total_amount,
                    'mode_of_payment' => $mode_of_payment,
                    'check_no' => $check_no,
                    'check_date' => $check_date,
                    'transaction_id' => $this->input->post('transaction_id'),
                    'serial_no' => $this->input->post('serial_no'),
                    'model' => $this->input->post('model_no'),
                    'previous_status' => 16,
                    'current_status' => 12,
                    'company_id' => $company_id
                );
                
                $result = $this->Punch_in->amc($data,$ticket_id);
                if ($result == 1) {
                    $tech_id     = $this->input->post('tech_id');
                    $company_id  = $this->Punch_in->get_company($tech_id);
                    $employee_id = $this->Punch_in->get_emp_id($tech_id);
                    $notify      = ' Contract renewed  by ' . $employee_id;
                    $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                    $json = array(
                        "status" => 1,
                        "msg" => "AMC Successfully done!!!"
                    );
                } else {
                    $json = '';
                    $json = array(
                        "status" => 0,
                        "msg" => "Error!!!"
                    );
                }
            }
        } else {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Invalid AMC id!!!"
            );
        }
        echo json_encode($json);
        
    }
    public function training()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->training($tech_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    public function certificate()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->certificate($tech_id);
            

            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    public function request_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id = $this->input->post('ticket_id');
        $tech_id   = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        //$get_availability=$this->Punch_in->get_availability($data,$ticket_id);
        $data = array(
            'requested_spare' => $this->input->post('spare'),
            'spare_array' => $this->input->post('spare'),
            'avalability' => $this->input->post('avalability'),
            'lead_time' => $this->input->post('lead_time'),
            'schedule_next' => date("Y-m-d",strtotime($this->input->post('lead_time'))),
            'cust_preference_date' => date("Y-m-d",strtotime($this->input->post('lead_time'))),
            'spare_cost' => $this->input->post('price'),
            'cust_acceptance' => $this->input->post('cust_acceptance'),
            'is_it_chargeable' => $this->input->post('is_it_chargeable'),
            'current_status' => 14
        );
        $this->load->model('Punch_in');
        if ($ticket_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please provide ticket id!!!"
            );
        } else {
            $result = $this->Punch_in->request_spare($data, $ticket_id);
            if ($result == 1) {
                $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
                $notify     = 'New Spare Request raised for the ticket ' . $ticket_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "msg" => "Spare Requested!!!"
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        
            $res1=$this->Punch_in->update_tech_task($tech_id);  
            echo json_encode($json1);
    }

    public function submit_quiz()
    { 
        $this->load->helper('url');
        $this->load->database();
        //$company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
       
        $data = array(
            'certificate_id' => $this->input->post('quiz_id'),
            'tech_id' => $this->input->post('tech_id'),
            'score' => $this->input->post('score'),
            'status' => $this->input->post('status'),
            'company_id'=> $this->input->post('company_id')
        );
        $this->load->model('Punch_in');
        if ($this->input->post('quiz_id') == '' || $this->input->post('tech_id') == '' || $this->input->post('score') == ''  || $this->input->post('company_id') == '') {
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Provide all required details!!!"
            );
        } else {
            $result = $this->Punch_in->submit_quiz($data);
            if ($result == 1) {
                $json = '';
                $json = array(
                    "status" => 1,
                    "msg" => "Submitted!!!"
                );
            } else {
                $json = '';
                $json = array(
                    "status" => 0,
                    "msg" => "Error!!!"
                );
            }
        }
        echo json_encode($json);
        
    }
    
    public function get_notify()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide technician id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->get_notify($tech_id);
            if (!empty($result)) {
                $json = array(
                    "status" => 1,
                    "result" => $result
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "No Records found"
                );
                echo json_encode($json);
            }
        }
    }

    public function personal_spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id     = $this->input->post('tech_id');
        $spare_array = $this->input->post('spare_array');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '' || empty($spare_array)) {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_request($tech_id, $spare_array, $company_id);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($tech_id);
                $notify      = 'New Imprest Spare Request raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
                $json = array(
                    "status" => 1,
                    "result" => "Request Done"
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }
    public function personal_spare_update()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $id      = $this->input->post('id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '' || $id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_update($tech_id, $id, $company_id);
            if ($result == 1) {
                $json = array(
                    "status" => 1,
                    "result" => "Updated "
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }
    public function onhand()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
       
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->onhand($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    
    public function requested_personal()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->requested_personal($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    public function spare_array_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->spare_array_tkt($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    public function submitted_claims()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        if ($tech_id == '') {
            $json1 = '';
            $json1 = array(
                "status" => 0,
                "msg" => "Please Provide Technician Id!!!"
            );
        } else {
            $result = $this->Punch_in->submitted_claims($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
        }
        echo json_encode($json1);
    }
    
    public function personal_spare_transfer()
    {
        $this->load->helper('url');
        $this->load->database();
        $from_tech_id     = $this->input->post('from_tech_id');
        $to_tech_id = $this->input->post('to_tech_id');
        $spare_array = $this->input->post('spare_array');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('from_tech_id'));
        if ($from_tech_id == '' || $to_tech_id == '' || empty($spare_array)) {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->personal_spare_transfer($from_tech_id,$to_tech_id, $spare_array, $company_id);
            if ($result == 1) {
                $employee_id = $this->Punch_in->get_emp_id($from_tech_id);
                $notify      = 'Imprest Spare Transfer raised by ' . $employee_id;
                $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $from_tech_id);
                $this->Punch_in->update_notify($notify, $company_id, 'Manager', $from_tech_id);
                $json = array(
                    "status" => 1,
                    "result" => "Request Done"
                );
                echo json_encode($json);
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Error"
                );
                echo json_encode($json);
            }
        }
    }
    public function tech_select()
    {
        $this->load->helper('url');
        $this->load->database();
        $tech_id     = $this->input->post('tech_id');
        $this->load->model('Punch_in');
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
            $product=$this->Punch_in->get_tech_product($tech_id);
            $product_id=$product[0]['product_id'];
            $cat=$this->Punch_in->get_tech_cat($tech_id);
            $cat_id=$cat[0]['cat_id'];
        if ($tech_id == '') {
            $json = array(
                "status" => 0,
                "msg" => "Please provide tech_id"
            );
            echo json_encode($json);
        } else {
            $result = $this->Punch_in->tech_select($tech_id, $company_id, $product_id, $cat_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No records found!!!"
                );
            }
            echo json_encode($json1);
        }
    }
    
    public function verify_otp()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id'); 
        $otp_mobile     = $this->input->post('otp');
        $company_id     = $this->Punch_in->get_company($tech_id);
   
        $res = $this->Punch_in->verify_otp($ticket_id,$tech_id,$otp_mobile);
      
        if ($res == 1)
         {

            $keytext=uniqid();
$key = $ticket_id."".md5($keytext)."".time();
$getdetails=$this->Punch_in->storeratingkey($ticket_id,$company_id,$key);
         $to=$getdetails['email_id'];
         $tonumber=$getdetails['contact_number'];
        $emailcontent = "Dear Customer,";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."please take some time to rate our service in the following link";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."".base_url() . "/index.php?/Controller_service/Feedback/?token=".$key;

        $mobilemessage="Dear Customer,";
        $mobilemessage=$mobilemessage."please take some time to rate our service in the following link";
        $mobilemessage=$mobilemessage.'<br>';
        $mobilemessage=$mobilemessage."".base_url() . "/index.php?/Controller_service/Feedback/?token=".$key;

        $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
        $this->email->to($to);
        $this->email->subject('FieldPro Ticket Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
      //  $this->email->send();
        $this->email->send();
      $apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
       // $numbers = array($cust_mobile);
        $sender = urlencode('TXTLCL');
        //$message = rawurlencode($array['string']);
       // $message = rawurlencode("Dear Customer if you satisfy with our service please share this OTP with technician - ".$code);
     //   $tonumber = implode(',', $tonumber);
       
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $tonumber, "sender" => $sender, "message" => rawurlencode($mobilemessage));
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);



            $json = array(
                "status" => 1,
                "msg" => "OTP verified successfully"
            );
         }
         else
         {
            $json = array(
                "status" => 0,
                "msg" => "Error in OTP Verification"
            );  
         }
         echo json_encode($json);
   }
 public function submit_completetest()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $ticket_id      = $this->input->post('ticket_id');
        $tech_id        = $this->input->post('tech_id');
        $service_charge = $this->input->post('service_charge');
        $spare_charge   = $this->input->post('spare_charge');
        $tax            = $this->input->post('tax');
        $total_charge   = $this->input->post('total_charge');
        $signature      = '';
        if (!empty($_FILES["myFile"])) {
            $file_name = $_FILES["myFile"]["name"];
            $temp_name = $_FILES['myFile']['tmp_name'];
            $filename  = end((explode(".", $file_name)));
            $img_dir   = 'assets/images/' . 'completed_' . $ticket_id . '.'. $filename;
            $signature = stripslashes($img_dir);
            move_uploaded_file($temp_name, $signature);
        }
        $frm              = $this->input->post('frm');
        $drop_of_date     = $this->input->post('drop_of_date');
        $drop_of_date     = date("Y-m-d H:i:s", strtotime($drop_of_date));
        $drop_of_location = $this->input->post('drop_of_location');
        $ticket_end_time  = date("Y-m-d H:i:s");
        $company_id       = $this->Punch_in->get_company($tech_id);
        if ($signature != '') {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'signature' => $signature,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        } else {
            $data = array(
                'company_id' => $company_id,
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id,
                'service_charge' => $service_charge,
                'spare_amount' => $spare_charge,
                'tax' => $tax,
                'total' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time
            );
        }
        $result = $this->Punch_in->submit_complete($ticket_id, $tech_id, $data);
        if ($result == 1) {
            $product     = $this->Punch_in->get_tech_product($tech_id);
            $company_id  = $product[0]['company_id'];
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = $ticket_id . ' is completed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            $json = '';
            $code = mt_rand(1001,9999);
            $data = array(
                'service_charge' => $service_charge,
                'spare_cost' => $spare_charge,
                'tax' => $tax,
                'total_amount' => $total_charge,
                'frm' => $frm,
                'drop_spare' => $this->input->post('drop_spare'),
                'drop_of_date' => $drop_of_date,
                'drop_of_location' => $drop_of_location,
                'mode_of_payment' => $this->input->post('mode_of_payment'),
                'check_no' => $this->input->post('check_no'),
                'check_date' => $this->input->post('check_date'),
                'transaction_id' => $this->input->post('transaction_id'),
                'ticket_end_time' => $ticket_end_time,
                //'current_status' => 8,
                'otp_code'=> $code
            );
            $cus_mo  = $this->Punch_in->get_det_cust($ticket_id);
            $cust_mobile = $cus_mo['contact_number'];
            $res  = $this->Punch_in->submit_complete1($ticket_id, $tech_id, $data,$cust_mobile,$code);
            if ($res == 1) {
                $json = array(
                    "status" => 1,
                    "OTP" => $code,
                    "msg" => "Completed Successfully done!!!"
                );
                $cst  = $this->Punch_in->get_det_cust($ticket_id);
                $cust_id=$cst['customer_id'];
                 $to=$cst['email_id'];
//$to="srihari.s@kaspontech.com";




        $emailcontent = "Dear Customer if you satisfy with our service please share this OTP with technician - ".$code;
         $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
        $this->email->to($to);
        $this->email->subject('FieldPro OTP Verification');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
        echo "to ".$to."  ".$this->email->send();
        
            }
          
        } 
        
      //  echo json_encode($json);
    }
public function testencryption()
{
     $ticket_id="Tk_0828";
    $company_id="company1";
    $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        //  $this->load->library('email');
   
$keytext=uniqid();
$key = $ticket_id."".md5($keytext)."".time();
$getdetails=$this->Punch_in->storeratingkey($ticket_id,$company_id,$key);
         $to=$getdetails['email_id'];
      //   $tonumber=$getdetails['contact_number'];
        // $to="srihari.s@kaspontech.com";
        // echo $to;
         $tonumber=$getdetails['contact_number'];
        $emailcontent = "Dear Customer,";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."please take some time to rate our service in the following link";
        $emailcontent=$emailcontent.'<br>';
        $emailcontent=$emailcontent."".base_url() . "/index.php?/Controller_service/Feedback/?token=".$key;
        $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
        $this->email->to($to);
        $this->email->subject('FieldPro Ticket Feedback');
        $this->email->message($emailcontent);
        $this->email->set_newline("\r\n");
      //  $this->email->send();
        $this->email->send();
$apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
       // $numbers = array($cust_mobile);
        $sender = urlencode('TXTLCL');
        //$message = rawurlencode($array['string']);
       // $message = rawurlencode("Dear Customer if you satisfy with our service please share this OTP with technician - ".$code);
     //   $tonumber = implode(',', $tonumber);
       
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $tonumber, "sender" => $sender, "message" => $emailcontent);
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
// Will only print the email headers, excluding the message subject and body
       echo  $this->email->print_debugger(array('headers'));
        echo "  sent to  ".$to;
}
public function remainder_mail(){
            $today_date = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('admin_user');
            $this->db->group_by('admin_user.company_id');
            $query = $this->db->get();
            $answer = $query->result_array();
            //print_r($answer);
            foreach($answer as $row){
                $date1_ts = strtotime($today_date);
                $date2_ts = strtotime($row['renewal_date']);
                $diff = $date2_ts - $date1_ts;
                $result = round($diff / 86400);
                //echo "for";
                  if($result>="0" && $result<="5"){
                                    //echo "if";    
                                    // echo $row['ad_mailid'];
                        //echo $row['company_id'];
                      $mail_id = $row['ad_mailid'];
                            $company_name = $row['company_name'];
                      $renewal_date = $row['renewal_date'];
                      
                        $this->load->helper('url');
                        $this->load->database();
                        $username = "
                            <!doctype html>
                            <html>
                            <head>
                            <meta name='viewport' content='width=device-width' />
                            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                            <style>
                            p,
                            ul,
                            ol {
                            font-family: sans-serif;
                            font-size: 14px;
                            font-weight: normal;
                            margin: 0;
                            Margin-bottom: 15px; }
                            p li,
                            ul li,
                            ol li {
                            list-style-position: inside;
                            margin-left: 5px; }
                            a {
                            color: #3498db;
                            text-decoration: underline; }
                            </style>
                            <body>
                            <p>Dear <b>".$company_name."</b> & Team,</p>
                            <p>Warning mail!!!</b></p>
                            <p>Your Subscription expires soon!! </p> 
                            <p>Your Company's subscription expires on <strong>".$renewal_date."</strong></p>
                            <p>Only ".$result ." Days left for expiry</p>
                            <p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.
                            
                            </body>
                            </html>";
                    $config   = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'kaspondevelopers@gmail.com',
            'smtp_pass' => 'Kaspon@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
                        $this->email->to("ramyariotz22@gmail.com");
                        $this->email->subject('Expiry of Subscription regd');
                        $this->email->message($username);
                        $this->email->set_newline("\r\n");
                        //$this->email->send();
                      $this->email->send();
//echo "mail sent";
// Will only print the email headers, excluding the message subject and body
       //echo  $this->email->print_debugger(array('headers'));
                      
                  }                     
            }
            return true;
            //return $result.' Days';
        }

        public function ForgotPassword_Customer()
        {   $this->load->helper('url');
            $this->load->database(); 
            $this->load->model('Punch_in');
            //$json1 = array();
              $email = $this->input->post('email');      
              $findemail = $this->Punch_in->ForgotPassword_cus($email);  
              if($findemail){
               $this->Punch_in->sendpassword($findemail);        
                }else{
                    $json1 = array(
                        "status" => 0,
                        "msg" => "No Email Found!!!"
                    );

                    echo json_encode($json1);
           }
          
        }

        public function change_password_cus()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Punch_in');
        $json1 = array();
        $old_pass = $this->input->post('oldpass');
        $new_pass = $this->input->post('newpass');
        $cus_email = $this->input->post('email');
        $check_old_email = $this->Punch_in->checkOldPass($cus_email,$old_pass);

        if($check_old_email == 1)
        {
         $save_new_pass = $this->Punch_in->save_new_pass($cus_email,$new_pass);  
         if($save_new_pass == 1) {
            $json1 = array(
                "status" => 1,
                "msg" => "Password changed successfully"
            ); 
         }
         else{
            $json1 = array(
                "status" => 0,
                "msg" => "Error in update password"
            ); 
         }
         
        }
        
        else{
            $json1 = array(
                "status" => 0,
                "msg" => "No Email Found"
            );  
        }
        echo json_encode($json1);
    
    }

    
public function testunion()
{
     $this->load->helper('url');
   $this->load->library('email');
    $data['ticket_data']=array(
        "ticket_id"=>"Ticket",
        "AMCID"=>"AMC",
        "Contracttype"=>"Labour"
    );

    $baseurl = base_url() .'assets/layouts/layout/img/logos.png';
   $emailTemplate='<html>
<head>
    <style>
    table {
    border-collapse: collapse;
    width: 70%;
}
th{
   
    text-align: center;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
 td {
    padding: 8px;
    text-align: left;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
    </style>
    
</head>
<body>
    <table  border="0" width="70%" >
 <tr>
<td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$baseurl.'"></td>
</tr>

<tr>
<td colspan="2">Hi Hari</td>
</tr>
<tr>
<td colspan="2">Your request for renewal of the contract is under process.
  Kindly find the details below. </td>
</tr>
 
                  <tr>
                    <td width="50%" align="left" >Ticket Id:</td>
                    <td width="50%" align="left" >TK_0900</td>
                  </tr>
                  <tr>
                    <td width="50%" align="left" >AMC contract Id</td>
                    <td width="50%" align="left" >TK_0900</td>
                    
                  </tr>
                  <tr>
                    <td width="50%" align="left" >Contract Type:</td>
                    <td width="50%" align="left" >Labour</td>
                  </tr>

<tr>
<td colspan="2">Thanks and Regards,</td>
</tr>

<tr>
<td colspan="2">Field Pro Team</td>
</tr>
</table>
</body>
</html>';
  
$config['protocol']='smtp';
$config['smtp_host']='ssl://smtp.gmail.com';
$config['smtp_port']='465';
$config['smtp_timeout']='300';
$config['smtp_user']='kaspondevelopers@gmail.com';
$config['smtp_pass']='Kaspon@123';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
$this->email->clear(TRUE);
$this->email->initialize($config);
$this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
$this->email->to("srihariram777@hotmail.com");
$this->email->subject('Notification Mail');
$this->email->set_mailtype("html");
$this->email->message($emailTemplate);

$this->email->send();
}

public function getDistance() {  
    $latitude1 = $this->input->post('latitude1');
    $longitude1 = $this->input->post('longitude1');
    $latitude2 = $this->input->post('latitude2');
    $longitude2 = $this->input->post('longitude2');
   
    $earth_radius = 6371;

    $dLat = deg2rad( $latitude2 - $latitude1 );  
    $dLon = deg2rad( $longitude2 - $longitude1 );  

    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
    $c = 2 * asin(sqrt($a));  
    $d = $earth_radius * $c;  
    $meters = $d * 1000;
    if( $meters < 100 ) {
        
        $json2 = '';
        $json2 = array(
            "status" => 1,
            "msg" => "Within 100 meters radius"
        );
        echo json_encode($json2); 
        
    } else {
      
        $json2 = '';
        $json2 = array(
            "status" => 0,
            "msg" => "Outside 100 meters radius"
        );
        echo json_encode($json2);
    }  
}


public function imprest_spare_track()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id     = $this->input->post('tech_id');
    $ticket_id   = $this->input->post('ticket_id');
    $spare_array = $this->input->post('spare_array');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '' || empty($spare_array)) {
        $json = array(
            "status" => 0,
            "msg" => "Please provide all details"
        );
        echo json_encode($json);
    } else {
        $result = $this->Punch_in->imprest_spare_track($tech_id,$ticket_id, $spare_array, $company_id);
        if ($result == 1) {
            $employee_id = $this->Punch_in->get_emp_id($tech_id);
            $notify      = 'Imprest Spare consumed by ' . $employee_id;
            $this->Punch_in->update_notify($notify, $company_id, 'Service Desk', $tech_id);
            $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
            $json = array(
                "status" => 1,
                "result" => "Success"
            );
            echo json_encode($json);
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Error"
            );
            echo json_encode($json);
        }
    }
}

public function spare_transfer_list()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id     = $this->input->post('tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '') {
        $json = array(
            "status" => 0,
            "msg" => "Please provide technician id"
        );
        echo json_encode($json);
    }
    else {
        $result = $this->Punch_in->spare_transfer_list($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No data found"
                );
            }
            echo json_encode($json1);
    }
}

public function spare_received_list()
{
    $this->load->helper('url');
    $this->load->database();
    $tech_id     = $this->input->post('tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    if ($tech_id == '') {
        $json = array(
            "status" => 0,
            "msg" => "Please provide technician id"
        );
        echo json_encode($json);
    }
    else {
        $result = $this->Punch_in->spare_received_list($tech_id, $company_id);
            if (!empty($result)) {
                $json1 = '';
                $json1 = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json1 = '';
                $json1 = array(
                    "status" => 0,
                    "msg" => "No data found"
                );
            }
            echo json_encode($json1);
    }
}

public function accept_transfered_spare(){

    $this->load->helper('url');
    $this->load->database();
    $spare_id   =$this->input->post('spare_id');
    $tech_id     = $this->input->post('tech_id');
    $to_tech_id = $this->input->post('to_tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    
 
    $result = $this->Punch_in->accept_transfered_spare($spare_id,$tech_id, $to_tech_id,$company_id);
    if ($result == 1) {
        $tech_id     = $this->input->post('tech_id');
        $employee_id = $this->Punch_in->get_emp_id($tech_id);
        $notify      = 'Spare accepted  by ' . $employee_id;
        $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
        $this->Punch_in->update_notify($notify, $company_id, 'Technician', $tech_id);
        $json = array(
            "status" => 1,
            "msg" => "Spare accepted"
        );
    } else {
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Error"
        );
    }
    echo json_encode($json);

}

public function accept_received_spare(){

    $this->load->helper('url');
    $this->load->database();
    $spare_id   =$this->input->post('spare_id');
    $tech_id     = $this->input->post('tech_id');
    $from_tech_id = $this->input->post('from_tech_id');
    $this->load->model('Punch_in');
    $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
    
 
    $result = $this->Punch_in->accept_received_spare($spare_id,$tech_id, $from_tech_id,$company_id);
    if ($result == 1) {
        $tech_id     = $this->input->post('tech_id');
        $employee_id = $this->Punch_in->get_emp_id($tech_id);
        $notify      = 'Spare received  by ' . $employee_id;
        $this->Punch_in->update_notify($notify, $company_id, 'Manager', $tech_id);
        $this->Punch_in->update_notify($notify, $company_id, 'Technician', $tech_id);
        $json = array(
            "status" => 1,
            "msg" => "Spare received"
        );
    } else {
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Error"
        );
    }
    echo json_encode($json);

}



public function profile_image_update()
{  
   
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->database();
    $this->load->model('Punch_in');
   
    $company_id = $this->input->post('company_id');
    $tech_id   = $this->input->post('tech_id');
   
    if (!empty($_FILES["myimage"])) {
        $file_name = $_FILES["myimage"]["name"];
        $temp_name = $_FILES['myimage']['tmp_name'];
        $filename  = end((explode(".", $file_name)));
        $img_dir   = 'technician_img/' . $tech_id .'.'. $filename;
        $img_dir1  = stripslashes($img_dir);
        if(move_uploaded_file($temp_name, $img_dir1)){
            
         $result = $this->Punch_in->profile_image_upload($company_id,$tech_id,$img_dir1);
         if ($result == 1) {
           
           $image_url = $this->Punch_in->profile_image_link($company_id, $tech_id); 

            $json = '';
            $json = array(
                "status" => 1,
                "msg" => "Profile image updated successfully",
                "updated_image" => base_url().$image_url
            );
         }else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Error in insert image data"
            );
         }

          
        }
        else{
            $json = '';
            $json = array(
                "status" => 0,
                "msg" => "Something error in image upload, try again"
            );
        }
    }
    else{
        $json = '';
        $json = array(
            "status" => 0,
            "msg" => "Image may empty"
        );
    }

    echo json_encode($json);
    
}

public function pending_frm()
{
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->database();
    $this->load->model('Punch_in');  
    $json   = array();      
    $tech_id   = $this->input->post('tech_id');

    if ($tech_id != "") {
        $company_id = $this->Punch_in->get_company($this->input->post('tech_id'));
        $result = $this->Punch_in->pending_frm($tech_id,$company_id);
        if (!empty($result)) {
            $json = array(
                "status" => 1,
                "result" => $result
            );
        }
        
    } else {
        $json = array(
            "status" => 0,
            "result" => "Please provide technician ID"
        );
    }
    echo json_encode($json);
    
    
}

public function load_location()
{
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Punch_in');  
    $json   = array();    
    $tech_id =  $this->input->post('tech_id');
    if ($tech_id != "") {
    $company_id = $this->Punch_in->get_company($tech_id);
    $tech_data=$this->Punch_in->gettechdetails($tech_id);
    $region=$tech_data['region'];
    $area=$tech_data['area'];
    
    $result = $this->Punch_in->load_location($company_id,$region,$area);
    if (!empty($result)) {
        $json = array(
            "status" => 1,
            "result" => $result
        );
    }
} else {
    $json = array(
        "status" => 0,
        "result" => "Please provide technician ID"
    );
}

    echo json_encode($json);
}

}