<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class controller_service extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper(array(
            'url',
            'cookie'
        ));
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->model('Punch_in');
    }
    
    public function index()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->db->join('user', 'user.email_id = login.username');
            $this->load->view('service_desk', $data);
        } else {
            redirect('login/login');
        }
    }
    public function load_notify()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('model_service');
        $datass       = $this->session->userdata('session_username');
        $datas        = $this->session->userdata('companyid');
        $user         = $this->Admin_model->get_details_user($datass, $datas);
        $region       = $user['region'];
        $area         = $user['area'];
        $company_id = $this->input->post('company_id');
        //$company_id='company1';
        $data       = $this->model_service->load_notify($company_id, $region, $area);
        echo json_encode($data);
    }
    public function manager()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('manager', $data);
        } else {
            redirect('login/login');
        }
    }
    /*Display assigned tickets to service desk login */
    public function assigned_tickets()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('assigned_tickets', $data);
        } else {
            redirect('login/login');
        }
    }
    public function service_raise_tkt()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
			$data['servicegroup'] = $this->Admin_model->get_servicegroup_ticket($datas);
            $this->load->view('service_raise_tkt', $data);
        } else {
            redirect('login/login');
        }
    }
    public function service_amc()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('service_amc', $data);
        } else {
            redirect('login/login');
        }
    }
    
    public function customer_req()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->model('Model_service');
            $data['record'] = $this->Model_service->disply_contractreq($datas, $region, $area);
            $this->load->view('customer_req', $data);
        } else {
            redirect('login/login');
        }
    }
    
    public function contract_status()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Model_service');
        $action     = $this->input->post('id');
        $company_id = $this->input->post('company_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $exist      = $this->Model_service->existing($action, $company_id);
        
        //$datas = $this->reimbursement->contract_change($data,$action,$company_id);
        if ($exist == "accepted") {
            echo 'You have Approved the Contract';
        } else if ($exist == "rejected") {
            echo 'This Contract alrdy exists.';
        } 
         else if($exist=="no data")
        {
            echo 'No data found.';
        }
        else {
            echo 'Something went wrong';
        }
    }
    
    
    public function contract_reject()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Model_service');
        $action     = $this->input->post('id');
        $company_id = $this->input->post('company_id');
        $changes    = array(
            'status' => 2
        );
        $data1      = array(
            'id' => $action,
            'company_id' => $company_id
        );
        $datas      = $this->Model_service->contract_change1($data1, $changes);
        if ($datas = 1) {
            echo 'You have rejected this Contract';
        } else {
            echo 'sorry try again';
        }
    }
    
    public function bulk_impreset()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Model_service');
        $this->load->library('PHPExcel');
        $handle   = $_FILES["add_excel"]['tmp_name'];
        $filename = $_FILES["add_excel"]["name"];
        $pieces   = explode(".", $filename);
       
        if ($pieces[1] == "xlsx") {
            $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel   = $objReader->load($handle);
            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest)
                    return ord(strtolower($dest)) - 96;
                else
                    return 0;
            }
            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }
            for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }
            $e                  = 2;
            $result             = array();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $company_id = $this->session->userdata('companyid');
            if ($highestColumnIndex == 2) {
                for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                    $sheetData  = $objPHPExcel->getActiveSheet();


                    $data_array  = myFunction($sheetData, '0', $e);
                    $tech_id  = myFunction($sheetData, 'A', $e);
                    if(!empty($data_array))
                    {
                    if($tech_id!="")
                    {
                    $ch_tech        = $this->Model_service->check_tech($tech_id, $company_id);
                    $s=$e-1;
                    if ($ch_tech != "") {
                        $update_req_spare = $this->Model_service->update_imprest_spare( $data_array,$ch_tech, $company_id);
                        if($update_req_spare==1)
                        {
                            $notify = "Requested Imprest Spare is available ";
                            $role   = "Technician";
                            $key    = "imprest_spare";
                            $this->Model_service->update_notify($notify, $company_id, $role, $ch_tech, $key);
                            array_push($result, array(
                                        "Updated successfully for record  " . $s
                                    ));
                        }
                        else
                        {
                            array_push($result, array(
                                        "Mismatch in spare array/technician id for record  " .$s
                                    ));
                        }
                    }
                    else {
                        array_push($result, array(
                            "Provide proper technician id for record " .$s
                        ));
                    }
                    }
                    else {
                        array_push($result, array(
                            "Provide  technician id for record " . $s
                        ));
                    }
                    }
                    else
                    {
                        array_push($result, array(
                            "Provide  spare array  for record " . $s
                        ));
                    }
                    $e++;
                }
            } else {
                array_push($result, array(
                    "Error Data!!! .. Please do verfity with Sample "
                ));
            }
            echo json_encode($result);
        }
       
    }
    public function ongoing()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('ongoing', $data);
        } else {
            redirect('login/login');
        }
    }
    public function completed()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('completed_tickets', $data);
        } else {
            redirect('login/login');
        }
    }
    public function spare()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('requested_spare', $data);
        } else {
            redirect('login/login');
        }
    }
    public function bulk_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Model_service');
        $this->load->library('PHPExcel');
        $handle     = $_FILES["add_spare"]['tmp_name'];
        $filename   = $_FILES["add_spare"]["name"];
        $company_id = $this->session->userdata('companyid');
        $pieces     = explode(".", $filename);
        if ($pieces[1] == "xlsx" || $pieces[1] == "ods") {
            $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel   = $objReader->load($handle);
            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest)
                    return ord(strtolower($dest)) - 96;
                else
                    return 0;
            }
            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }
            for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }
            $e                  = 2;
            $result             = array();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            if ($highestColumnIndex == 4) {
                for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                    $sheetData      = $objPHPExcel->getActiveSheet();
                    $ticket_id      = myFunction($sheetData, '0', $e);
                    $tech_id        = myFunction($sheetData, 'A', $e);
                    $spare_id       = myFunction($sheetData, 'B', $e);
                    $spare_quantity = myFunction($sheetData, 'C', $e);
                    $ch_tech        = $this->Model_service->check_tk_tech($ticket_id, $tech_id, $company_id);
                    if ($ch_tech != "") {
                        $datass = $this->Model_service->get_req_spare($ticket_id, $company_id);
                        if ($datass != 'Invalid Ticket ID') {
                            foreach ($datass as $row) {
                                $spare_array = json_decode($row['spare_array'], true);
                                $spare       = array();
                                if (empty($spare_array)) {
                                    array_push($result, array(
                                        "No Spare array to update for " . $ticket_id
                                    ));
                                } else {
                                    
                                    foreach ($spare_array as $sp_arr) {
                                        if ($sp_arr['Spare_code'] == $spare_id) {
                                            $sp_arr['quantity'] = $sp_arr['quantity'] - $spare_quantity;
                                            array_push($spare, array(
                                                'Spare_code' => $sp_arr['Spare_code'],
                                                'quantity' => $sp_arr['quantity'],
                                                'spare_source' => $sp_arr['spare_source']
                                            ));
                                        } else {
                                            array_push($spare, array(
                                                'Spare_code' => $sp_arr['Spare_code'],
                                                'quantity' => $sp_arr['quantity'],
                                                'spare_source' => $sp_arr['spare_source']
                                            ));
                                        }
                                    }
                                    $update_req_spare = $this->Model_service->update_req_spare($ticket_id, $spare, $company_id);
                                    
                                    $spare_array1 = json_decode($row['spare_array'], true);
                                    $spare1       = array();
                                    foreach ($spare_array1 as $sp_arr1) {
                                        array_push($spare1, array(
                                            'Spare_code' => $sp_arr1['Spare_code'],
                                            'quantity' => 0,
                                            'spare_source' => $sp_arr1['spare_source']
                                        ));
                                    }
                                    if ($spare_array1 == $spare1) {
                                        $this->Model_service->update_tik($ticket_id, $company_id);
                                        $notify = "Spares available for ticket " . $ticket_id;
                                        $role   = "Technician";
                                        $key    = "spare_request";
                                        $this->Model_service->update_notify($notify, $company_id, $role, $ch_tech, $key);
                                    }
                                    array_push($result, array(
                                        "Updated successfully for Ticket  " . $ticket_id
                                    ));
                                }
                            }
                        } else {
                            array_push($result, array(
                                "Invalid Ticket  " . $ticket_id
                            ));
                        }
                        $e++;
                    } else {
                        array_push($result, array(
                            "Provide proper technician id for Ticket " . $ticket_id
                        ));
                    }
                }
                
            } else {
                array_push($result, array(
                    "Error Data!!! .. Please do verfity with Sample "
                ));
            }
            echo json_encode($result);
        }
    }
    public function spare_form()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->model('model_service');
            $data['records'] = $this->model_service->display_accept_Spare($datas, $region, $area);
            $data['imprest'] = $this->model_service->impressed_accept_Spare($datas);
            $this->load->view('spare_form', $data);
        } else {
            redirect('login/login');
        }
    }
    public function imprest()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->model('model_service');
            $data['records'] = $this->model_service->impressed_accept_Spare($datas);
            $this->load->view('imprest_spare', $data);
        } else {
            redirect('login/login');
        }
    }
    public function knowledge_base()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('knowledge_base', $data);
        } else {
            redirect('login/login');
        }
    }
    
    public function assign_tech()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyid');
            $user         = $this->Admin_model->get_details_user($datass, $datas);
            $region       = $user['region'];
            $area         = $user['area'];
            $location     = $user['location'];
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas, $region, $area);
            $data['user'] = $this->Admin_model->get_details_user($datass, $datas, $region, $area);
            $this->load->view('assign_tech', $data);
        } else {
            redirect('login/login');
        }
    }
    

    /*Displaying the assigned tickets based on company,product, region, area, location*/
    public function load_assigned()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_assigned($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

  /*Displaying assigned ticket based on fiter categories */
    public function load_unassigned()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_unassigned($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

    /*Displaying the deferred tickets based on company and other filter scenaria */
    public function load_deferred()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_deferred($company_id, $product_id, $region, $area, $location);
        return $result;
    }

    /*Displaying ongoing ticekts based on companyid */
    public function load_ongoing()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_ongoing($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

    /* Displaying escalated ticket list */
    public function load_escalated()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
       // $filter     = $this->input->post('filter'); commented by muthu
        $filter     = "";
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_escalated($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

/*Displaying spare requested ticket list */
    public function load_spare_requested()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_spare_requested($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }


    public function manload_spare_requested()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->manload_spare_requested($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }


    public function load_req()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_req($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

    /*Displaying the accepted ticket list */
    public function load_accepted()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_accepted($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }

    /*Load completed tickets */
    public function load_completed()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $tech       = $this->input->post('tech');
        $filter     = $this->input->post('filter');
        $this->load->model('model_service');
        $result = $this->model_service->load_completed($company_id, $filter, $product_id, $region, $area, $location, $tech);
        return $result;
    }

    /*Displaying closed tickets */
    public function load_closed()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $tech       = $this->input->post('tech');
        $filter     = $this->input->post('filter');
        $this->load->model('model_service');
        $result = $this->model_service->load_closed($company_id, $filter, $product_id, $region, $area, $location, $tech);
        return $result;
    }
    public function get_count()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $this->load->model('model_service');
        $get_without_feedback = $this->model_service->get_without_feedback($company_id, $region, $area);
        $this->load->model('model_service');
        $get_with_feedback = $this->model_service->get_with_feedback($company_id, $region, $area);
        $this->load->model('model_service');
        $get_closed = $this->model_service->get_closed($company_id, $region, $area);
        $json       = array();
        array_push($json, array(
            'get_without_feedback' => $get_without_feedback,
            'get_with_feedback' => $get_with_feedback,
            'get_closed' => $get_closed
        ));
        echo json_encode($json, true);
    }

    /*Change ticket status to close */
    public function close_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $this->load->model('model_service');
        $ticket_id     = $this->input->post('ticket_id');
        $cust_feedback = $this->input->post('cust_feedback');
        $cust_Rating   = $this->input->post('cust_rating');
        $cust_reason   = $this->input->post('cust_reason');
        $result        = $this->model_service->close_tkt($company_id, $ticket_id, $cust_feedback, $cust_Rating, $cust_reason);
        if ($result == 1) {
            echo "Ticket Closed!!!";
        } else {
            echo "Error while closing!!!";
        }
    }

    /*Assign ticket to anyother technician */
    public function change_tech()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id    = $this->input->post('company_id');
        $technician_id = $this->input->post('technician_id');
        $ticket_id     = $this->input->post('ticket_id');
        $count         = $this->input->post('count');
        $prod          = $this->input->post('product_id');
        $cat           = $this->input->post('cat_id');
        $this->load->model('model_service');
        $result = $this->model_service->change_tech($company_id, $technician_id, $ticket_id, $count);
        if ($result == 1) {
            $re = $this->model_service->tkt_ass($company_id, $technician_id, $ticket_id, $count, $prod, $cat);
            if ($re == 1) {
                $tech_data = $this->model_service->get_technican_data($technician_id);
                $tech_email=$tech_data['email_id'];
                $tech_name = $tech_data['first_name'];
                echo $ticket_id . " is assigned to " . $tech_name;
                $notify = $ticket_id . " is assigned to " . $tech_name;
                $role   = "Technician";
                $key    = "new";
                $notify_tech = $this->model_service->update_notify($notify,$company_id,$role,$technician_id,$key);
                $this->load->model('Pushnotify');
                $tech_id      = $technician_id;
                $token        = $this->Pushnotify->get_tech_token($tech_id);
                $device_token = $token['device_token'];
                $reg_id       = $token['reg_id'];
                
                 //email to technician
              
               $logo = base_url() .'assets/layouts/layout/img/logos.png';
              

               $emailTemplate='<html>
              <head>
                  <style>
                  table {
                  border-collapse: collapse;
                  width: 70%;
              }
              th{
                 
                  text-align: center;
                  border-bottom: 0px solid #ddd;
                  border-top: 0px solid #ddd;
                  border-left: 0px solid #ddd;
                  border-right: 0px solid #ddd;
              }
               td {
                  padding: 8px;
                  text-align: left;
                  border-bottom: 0px solid #ddd;
                  border-top: 0px solid #ddd;
                  border-left: 0px solid #ddd;
                  border-right: 0px solid #ddd;
              }
                  </style>
                  
              </head>
              <body>
                  <table  border="0" width="70%" >
               <tr>
              <td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$logo.'"></td>
              </tr>
              
              <tr>
              <td colspan="2">Hi '.$tech_name.'</td>
              </tr>
              <tr>
              <td colspan="2">New ticket is assigned, 
                Kindly find the details below. </td>
              </tr>
               
               <tr>
                 <td width="50%" align="left" >Ticket Id:</td>
                 <td width="50%" align="left" >'.$ticket_id.'</td>
               </tr>
               <tr>
                 <td width="50%" align="left" >Product</td>
                 <td width="50%" align="left" >'.$prod.'</td>
                 
               </tr>
               <tr>
                 <td width="50%" align="left" >Category:</td>
                 <td width="50%" align="left" >'.$cat.'</td>
               </tr>
              
              <tr>
              <td colspan="2">Thanks and Regards,</td>
              </tr>
              
              <tr>
              <td colspan="2">Field Pro Team</td>
              </tr>
              </table>
              </body>
              </html>';
              
                                                      
              $this->load->library('email');
              $config['protocol']='smtp';
              $config['smtp_host']='ssl://smtp.gmail.com';
              $config['smtp_port']='465';
              $config['smtp_timeout']='300';
              $config['smtp_user']='kaspondevelopers@gmail.com';
              $config['smtp_pass']='Kaspon@123';
              $config['charset']='utf-8';
              $config['newline']="\r\n";
              $config['wordwrap'] = TRUE;
              $config['mailtype'] = 'html';
              $this->email->initialize($config);
              $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
              $this->email->to($tech_email);
              $this->email->subject('New Ticket Assigned -' .$ticket_id );
              $this->email->message($emailTemplate);
              $this->email->send();

               //end email

                if ($device_token == "") {
                    
                    $this->Pushnotify->send_notification($reg_id, $notify, "New Ticket Assigned");
                } else {
                    $this->Pushnotify->iOS($notify, "New Ticket Assigned", $device_token);
                }
            } else {
                echo "Error while assigning!!!";
            }
        } else {
            echo "Error while assigning!!!";
        }
    }

    /*Dislaying technicians list based on company, worktype, etc */
    public function load_tech()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $cat_id     = $this->input->post('cat_id');
        $tech_id    = $this->input->post('tech_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $address    = $this->input->post('location');
        $work_type=$this->input->post('work_type');
        $this->load->model('model_service');
        //if($tech_id!='')
        //{
        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
        
        // Convert the JSON to an array
        $geo = json_decode($geo, true);
        
        if ($geo['status'] == 'OK') {
            // Get Lat & Long
            $latitude  = $geo['results'][0]['geometry']['location']['lat'];
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
            $this->load->model('model_service');
            $tech = $this->model_service->get_tech($company_id, $region, $area, $latitude, $longitude, $product_id, $cat_id, $tech_id,$work_type);
            return $tech;
        } else {
            $latitude  = 0;
            $longitude = 0;
            $this->load->model('model_service');
            $tech = $this->model_service->get_tech($company_id, $region, $area, $latitude, $longitude, $product_id, $cat_id, $tech_id,$work_type);
            return $tech;
        }
        // }
        
    }
    public function load_technician()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $this->load->model('model_service');
        $status  = '';
        $product = $this->model_service->load_technician($company_id, $status, $region, $area);
        return $product;
    }

    /*Diplaying the product list based on companyid */
    public function load_product()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $status     = $this->input->post('status');
        $this->load->model('model_service');
        $product = $this->model_service->load_product($company_id, $status);
        return $product;
    }

    public function load_product1()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $status     = $this->input->post('status');
        $this->load->model('model_service');
        $product = $this->model_service->load_product1($company_id);
        return $product;
    }

/*Displaying the region list based on companyid */
    public function load_region()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $status     = $this->input->post('status');
        $this->load->model('model_service');
        $product = $this->model_service->load_region($company_id, $status);
        return $product;
    }


    /*Displaying the area list based on company */
    public function load_area()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $status     = $this->input->post('status');
        $this->load->model('model_service');
        $product = $this->model_service->load_area($company_id, $region, $area, $status);
        return $product;
    }
   /*Displaying the location list from all_tickets table */

    public function load_location()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $status     = $this->input->post('status');
        $this->load->model('model_service');
        $product = $this->model_service->load_location($company_id, $region, $area, $status);
        return $product;
    }


    public function load_cat()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $this->load->model('model_service');
        $product = $this->model_service->load_cat($company_id, $product_id);
        return $product;
    }

    /* Displaying knowledge question */
    public function load_knowledge()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id  = $this->input->post('company_id');
        $product_id  = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $region      = $this->input->post('region');
        $area        = $this->input->post('area');
        $this->load->model('model_service');
        $result = $this->model_service->load_knowledge($company_id, 0, $product_id, $category_id, $region, $area);
        return $result;
    }

    /*displaying the approved knowledgebase */
    public function show_knowledge()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id  = $this->input->post('company_id');
        $product_id  = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $region      = $this->input->post('region');
        $area        = $this->input->post('area');
        $this->load->model('model_service');
        $result = $this->model_service->load_knowledge($company_id, 1, $product_id, $category_id, $region, $area);
        //print_r($result);
        return $result;
    }
    public function edit_knowledge()
    {
        
        $this->load->helper('url');
        $this->load->database();
        
        $string_prob = preg_replace('/\s+/', '', $this->input->post('problem'));
        $this->load->helper("file");
        $company_id = $this->input->post('company_id');
        $id         = $this->input->post('id');
        $baseurl    = base_url() . 'assets/images/knowledge_base/';
        if (isset($_FILES["fileUpload_image1"]['name'])) {
            $image      = $_FILES["fileUpload_image1"];
            $output_dir = "./assets/images/knowledge_base/";
            $img_dir    = $baseurl . 'image/' . $string_prob . '.jpg';
        } else {
            $img_dir = $this->input->post('hidden_img');
        }
        if (isset($_FILES["fileUpload_video1"]['name'])) {
            
            $video      = $_FILES["fileUpload_video1"];
            $output_dir = "./assets/images/knowledge_base/";
            $video_dir  = $baseurl . 'video/' . $string_prob . '.mp4';
        } else {
            $video_dir = $this->input->post('hidden_video');
        }
        
        $product = $this->input->post('product');
        if ($this->input->post('product') == '' || $this->input->post('product') == 'null') {
            $product = '';
        }
        $category = $this->input->post('category');
        if ($this->input->post('category') == '' || $this->input->post('category') == 'null') {
            $category = '';
        }
        if (isset($_FILES["fileUpload_image1"]['name'])) {
            if (file_exists($baseurl . $output_dir . 'image/' . $string_prob . '.jpg')) {
                chmod($baseurl . $output_dir . 'image/' . $string_prob . '.jpg', 0755); //Change the file permissions if allowed
                unlink($baseurl . $output_dir . 'image/' . $string_prob . '.jpg'); //remove the file
                
            }
            move_uploaded_file($_FILES["fileUpload_image1"]["tmp_name"], $output_dir . 'image/' . $string_prob . '.jpg');
        }
        if (isset($_FILES["fileUpload_video1"]['name'])) {
            if (file_exists($output_dir . 'video/' . $string_prob . '.mp4')) {
                chmod($output_dir . 'video/' . $string_prob . '.mp4', 0755); //Change the file permissions if allowed
                unlink($output_dir . 'video/' . $string_prob . '.mp4'); //remove the file
            }
            move_uploaded_file($_FILES["fileUpload_video1"]["tmp_name"], $output_dir . 'video/' . $string_prob . '.mp4');
        }
        
        $data = array(
            'product' => $product,
            'category' => $category,
            'problem' => $this->input->post('problem'),
            'solution' => $this->input->post('solution'),
            'image' => $img_dir,
            'video' => $video_dir
        );
        $this->load->model('model_service');
        $result = $this->model_service->edit_knowledge($company_id, $id, $data);
        if ($result == 1) {
            echo "Edited successfully";
        } else {
            echo "Error!!! Please recheck and edit";
        }
    }
    public function delete_knowledge()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $id         = $this->input->post('id');
        $this->load->model('model_service');
        $result = $this->model_service->delete_knowledge($company_id, $id);
        if ($result == 1) {
            echo "Deleted successfully";
        } else {
            echo "Error!!! Please recheck and delete";
        }
    }

    /* Removing the ticekt based on company id and ticket id */
    public function delete_tick()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $id         = $this->input->post('ticket_id');
        $this->load->model('model_service');
        $result = $this->model_service->delete_tick($company_id, $id);
        if ($result == 1) {
            echo "Deleted successfully";
        } else {
            echo "Error!!! Please recheck and delete";
        }
    }
    public function add_knowledge()
    {
        $this->load->helper('url');
        $this->load->database();
        $string_prob = preg_replace('/\s+/', '', $this->input->post('problem'));
        $baseurl     = base_url() . 'assets/images/knowledge_base/';
        if (isset($_FILES["fileUpload_image"]['name'])) {
            $image      = $_FILES["fileUpload_image"];
            $output_dir = "./assets/images/knowledge_base/";
            $img_dir    = $baseurl . 'image/' . $string_prob . '.jpg';
        } else {
            $img_dir = "";
        }
        if (isset($_FILES["fileUpload_video"]['name'])) {
            
            $video      = $_FILES["fileUpload_video"];
            $output_dir = "./assets/images/knowledge_base/";
            $video_dir  = $baseurl . 'video/' . $string_prob . '.mp4';
        } else {
            $video_dir = "";
        }
        
        $product = $this->input->post('product_id');
        if ($this->input->post('product_id') == '' || $this->input->post('product_id') == 'null') {
            $product = '';
        }
        $cat_id = $this->input->post('cat_id');
        if ($this->input->post('cat_id') == '' || $this->input->post('cat_id') == 'null') {
            $cat_id = '';
        }
        $data = array(
            'company_id' => $this->input->post('company_id'),
            'product' => $product,
            'category' => $cat_id,
            'region' => $this->input->post('region'),
            'area' => $this->input->post('area'),
            'problem' => $this->input->post('problem'),
            'solution' => $this->input->post('solution'),
            'image' => $img_dir,
            'video' => $video_dir,
            'status' => 1
        );
        $this->load->model('model_service');
        if ($this->input->post('solution') == '' || $this->input->post('problem') == '') {
            echo "Please provide problem/solution!!!";
        } else {
            if (isset($_FILES["fileUpload_image"]['name'])) {
                $moved= move_uploaded_file($_FILES["fileUpload_image"]["tmp_name"], $output_dir . 'image/' . $string_prob . '.jpg');
                // if($moved){  echo "Saved Successfully";}
                // else{
                //     echo "Not uploaded because of error #".$_FILES["fileUpload_image"]["error"];
                // }
              
            }
            if (isset($_FILES["fileUpload_video"]['name'])) {
                move_uploaded_file($_FILES["fileUpload_video"]["tmp_name"], $output_dir . 'video/' . $string_prob . '.mp4');
                
            }
            $result = $this->model_service->add_knowledge($data);
            if ($result == 1) {
                echo "Added successfully";
            } else {
                echo "Error!!! Please recheck and add";
            }
        }
    }
    public function accept_knowledge()
    {
        $this->load->helper('url');
        $this->load->database();
        $id     = $this->input->post('id');
        $status = $this->input->post('status');
        $this->load->model('model_service');
        $result = $this->model_service->accept_knowledge($id, $status);
        if ($result == 1) {
            echo "Knowledge Request accepted";
        } else {
            echo "Error while accepting!!!";
        }
    }

    /*Update ticket details */
    public function update_tkt()
    {
        $this->load->helper('url');
        $this->load->database();
        $ticket_id  = $this->input->post('ticket_id');
        $company_id = $this->input->post('company_id');
        $date       = $this->input->post('date');
        $cdate      = $this->input->post('cdate');
        /*$string1 = preg_replace('/\s+/', '', $date);
        $string2 = preg_replace('/\s+/', '', $cdate);
        $d1 = new DateTime($date);
        $d2 = new DateTime($cdate);
        if($d1==$d2)
        {
        echo "No Changes done!";    
        }
        else {*/
        $date_time2 = $date;
        $date_time1 = date("Y-m-d H:i:s", strtotime((string) $date_time2));
        $this->load->model('model_service');
        $result = $this->model_service->update_tkt($ticket_id, $company_id, $date_time1);
        if ($result == 1) {
            echo "Ticket Date updated";
        } else {
            echo "Error while updation!!!";
        }
        //}
    }
    public function tickets_count()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->tickets_count($company_id, $filter, $region, $area, $location);
        return $result;
    }
    public function ticket_distribution()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->ticket_distribution($company_id, $filter, $region, $area, $location);
        return $result;
    }
    public function ticket_insight()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->ticket_insight($company_id, $filter, $region, $area, $location);
        return $result;
    }
    public function revenue_insight()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->revenue_insight($company_id, $filter, $region, $area, $location);
        return $result;
    }
    public function csat_insight()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $product    = $this->input->post('product_id');
        $this->load->model('model_service');
        $result = $this->model_service->csat_insight($company_id, $filter, $product, $region, $area, $location);
        return $result;
    }
    public function spare_insight()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->spare_insight($company_id, $filter, $region, $area, $location);
        return $result;
    }
    
    public function send_mail()
    {
        $this->load->helper('url');
        $this->load->database();
        //$from_email= 'ramyariotz22@gmail.com';
        $to_mail  = $this->input->post('email_id');
        $name     = $this->input->post('customer_name');
        $feedback = $this->input->post('cust_feedback');
        $rating   = $this->input->post('cust_rating');
        $reason   = $this->input->post('cust_reason');
        $subject  = "Field Pro Feedback -reg.";
        /*  $config = Array(
        'mailtype'  => 'html', 
        'charset'   => 'utf8'
        );
        
        $message = "
        <!doctype html>
        <html>
        <head>
        <meta name='viewport' content='width=device-width' />
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <style>
        p,
        ul,
        ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
        list-style-position: inside;
        margin-left: 5px; }
        a {
        color: #3498db;
        text-decoration: underline; }
        </style>
        <body>
        <p>Dear <b>$name </b> & Team,</p>
        <p>   Welcome to FieldPro</b></p>
        <p>  Thank you for being our valued customers. We are grateful for the pleasure of serving you.</p> 
        <p></p>
        <p> Your Feedback is <strong>$feedback</strong></p>
        <p> Your Rating is <strong>$rating</strong></p>
        <p> Your Reason is<strong>$reason</strong></p>
        <p></p>
        <p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.
        
        
        </body>
        </html>
        ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // More headers
        $headers .= 'From: FieldPro Team- Work Feedback <fieldpro@kaspontech.com>' . "\r\n";
        $headers .= 'Cc: sahithyasahi93@gmail.com' . "\r\n";
        
        //mail($to,$subject,$message,$headers);
        if (mail($to_mail , $subject, $message, $headers)) {
        echo "Your Feedback has been sent to your mail";
        }
        else {
        echo "Error ";
        } */
        
        
        $message = "
                        <!doctype html>
                        <html>
                        <head>
                        <meta name='viewport' content='width=device-width' />
                        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                        <style>
                        p,
                        ul,
                        ol {
                        font-family: sans-serif;
                        font-size: 14px;
                        font-weight: normal;
                        margin: 0;
                        Margin-bottom: 15px; }
                        p li,
                        ul li,
                        ol li {
                        list-style-position: inside;
                        margin-left: 5px; }
                        a {
                        color: #3498db;
                        text-decoration: underline; }
                        </style>
                        <body>
                        <p>Dear <b>$name</b> ,</p>
                        <p> Thank you for being our valued customers. We are grateful for the pleasure of serving you.</p>
                        <p>This email is to inform you that the ticket you have raised is monitored by our field technician(s) and it is closed with your valuable feedback. </p> 
                        <p> Your Feedback is <strong>$feedback</strong></p>
                        <p> Your Rating is <strong>$rating</strong></p>
                        <p> Your Reason is<strong>$reason</strong></p>
                        <p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.

                        </body>
                        </html>
                        ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // More headers
        $headers .= 'From: FieldPro Team- New Subscription <fieldpro@kaspontech.com>' . "\r\n";
        $headers .= 'Cc: ramyariotz22@gmail.com' . "\r\n";
        
        //mail($to,$subject,$message,$headers);
        if (mail($to_mail, $subject, $message, $headers)) {
            echo "Ticket closed";
        } else {
            echo "mail not sent";
        }
    }
    
    public function download_sampletemplate($filename = NULL)
    {
        $this->load->helper('url');
        // load download helder
        $this->load->helper('download');
        // read file contents
        $data = file_get_contents(base_url() . 'assets/templates/'.$filename);
        force_download($filename, $data);
    }
    public function imprest_download_sampletemplate($filename = NULL)
    {
        $this->load->helper('url');
        // load download helder
        $this->load->helper('download');
        // read file contents
        $data = file_get_contents(base_url() . 'assets/templates/'.$filename); 
        force_download($filename, $data);
    }
    /* public function download_spareform(){
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('model_service');
    $company_id=$this->input->post('company_id');
    //$company_id='company1';
    $data=$this->model_service->download_spareform($company_id);
    print_r(json_encode($data));
    } */
    
    public function download_spareform()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $filter     = $this->input->post('filter');
        $product_id = $this->input->post('product_id');
        $region     = $this->input->post('region');
        $area       = $this->input->post('area');
        $location   = $this->input->post('location');
        $this->load->model('model_service');
        $result = $this->model_service->load_req($company_id, $filter, $product_id, $region, $area, $location);
        return $result;
    }
    public function imprest_download_spareform()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('model_service');
        $company_id = $this->input->post('company_id');

        // $region     = $this->input->post('region');
        // $area       = $this->input->post('area');
        //$company_id='company1';

        $data       = $this->model_service->imprest_download_spareform($company_id);
        //return $data;
       echo json_encode($data);
    }
 public function Feedback()
    {
        $this->load->helper('url');
        $this->load->database();
              $tokenid=$this->input->get('token');
        $arraydata=$this->Punch_in->getfeedbackcustomerdata($tokenid);
       // print_r($arraydata);
        $data['ticket_id']=$arraydata['ticket_id'];
        $data['primaryid']=$arraydata['id'];
          $data['emailid']=$arraydata['email_id'];
$data['ticketcurrentstatus']=$arraydata['current_status'];
        //$data['token'] = $tokenid;
        $this->load->view('Feedback',$data);
    }
}