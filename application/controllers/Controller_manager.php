<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class controller_manager extends CI_Controller
{
    public  function __construct()
        {
         parent:: __construct();         
         $this->load->library('encrypt');
         $this->load->library('email');
         $this->load->library('session');
         $this->load->helper(array('url','cookie'));         
         $this->load->database();
         $this->load->model('Admin_model');  
        }
    public function load_notify(){
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $company_id=$this->input->post('company_id');
        //$company_id='company1';
        $data=$this->reimbursement->load_notify($company_id);
        echo json_encode($data);
    }

    /* Displaying the employee id based on technician_id */
    public function onload_prev_tech(){
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $tech_id=$this->input->post('tech_id');
        //$company_id='company1';
        $data=$this->reimbursement->onload_prev_tech($tech_id);
        echo json_encode($data);
    }
    public function index()
        {   
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('reimbursement');
            $this->load->model('Model_service');
            $datass=$this->session->userdata('session_username');
            $company_id= $this->session->userdata('companyid');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $spare=$this->reimbursement->spare_expired($company_id);
            foreach($spare as $sp)
            {
            $notify=$sp['data'];
            $role="Manager";
            $key="";
            $tech_id="";
            $this->Model_service->update_notify($notify,$company_id,$role,$tech_id,$key);
            $this->Model_service->update_notify($notify,$company_id,"Admin",$tech_id,$key);
            }   
            $spare=$this->reimbursement->spare_quan($company_id);
            foreach($spare as $sp)
            {
            $notify=$sp['data'];
            $role="Manager";
            $key="";
            $tech_id="";
            $this->Model_service->update_notify($notify,$company_id,$role,$tech_id,$key);
            $this->Model_service->update_notify($notify,$company_id,"Admin",$tech_id,$key);
            }
            $renewal_date=$this->reimbursement->renewal_date($company_id);
            foreach($renewal_date as $rd)
            {
            $notify=$rd['data'];
            $role="Manager";
            $key="";
            $tech_id="";
            $this->Model_service->update_notify($notify,$company_id,$role,$tech_id,$key);
            $this->Model_service->update_notify($notify,$company_id,"Admin",$tech_id,$key);
            }
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            $data['record']     = $this->reimbursement->get_count($datas,$region,$area);
            $data['tickets']    = $this->reimbursement->total_tickets($datas,$region,$area);
            $data['escallated'] = $this->reimbursement->escallated($datas,$region,$area);
            $data['completed']  = $this->reimbursement->completed($datas,$region,$area);
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $this->load->view('manager', $data);      
        }
 

        /*Displaying the technician details with image, core, level */
        public function leaderboard()
        {               
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('reimbursement');
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas);
            $leader_calc = $this->leaderboard_display($datas,$region,$area);
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            $data['escallated'] = $this->reimbursement->leader($datas,$region,$area);
            $data['result2']=$this->reimbursement->rewardrank($datas,$region,$area);
            $data['result3']=$this->reimbursement->rewardrank2($datas,$region,$area);
            $data['result4']=$this->reimbursement->rewardrank3($datas,$region,$area);
            $this->load->view('manager_reward', $data);  
        }

          /*Displaying spare transfer information like from_technician, to_technicians, etc */
            public function spare_transfer()
            {               
            $this->load->helper('url');
            $this->load->database();
            $company_id = $this->session->userdata('companyid');
            $this->load->model('reimbursement');
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
        //  $data['pass']=$this->Admin_model->get_employee($datass,$datas);
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['record']= $this->reimbursement->spare_transfer($company_id,$region,$area);
            $data['records']= $this->reimbursement->spare_transfer_accept($company_id,$region,$area);
            $data['recordss']= $this->reimbursement->Spare_transfer_reject($company_id,$region,$area);
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
           // $data['recorded']= $this->reimbursement->Spare_transfer_reject('company1_Tech_0010');
            $this->load->view('spare_transfer', $data);
        }
        
public function leader_region()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $datas= $this->session->userdata('companyid');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->leader_region($datas,$filter);
        echo $data;
        return $data;
    }
      public function leaderboard_display($datas,$region,$area)
    {
        $company_id =$this->session->userdata('companyid');
        $company_id =$datas;
        $this->load->helper('url');
        $this->load->database();
        //$company_id = $this->input->post('company_id');
        $this->load->model('reimbursement');
        $data1      = $this->reimbursement->daysDifference($company_id,$region,$area);
        $technician = $this->reimbursement->technician($company_id,$region,$area);
        $data2      = $this->reimbursement->first_time_fix($company_id,$region,$area);
        $data3      = $this->reimbursement->average_call($company_id,$region,$area);
        $data8      = $this->reimbursement->Customer_feedback($company_id,$region,$area);
        $data4      = $this->reimbursement->Customer_Rating($company_id,$region,$area);
        $data5      = $this->reimbursement->Knowledge($company_id,$region,$area);
        $data6      = $this->reimbursement->Training($company_id,$region,$area);
        $data7      = $this->reimbursement->contract($company_id,$region,$area);
        $data9      = $this->reimbursement->mttr($company_id,$region,$area);
        $result     = json_decode($technician, true);
        $data1      = json_decode($data1, true);
        $data2      = json_decode($data2, true);
        $data3      = json_decode($data3, true);
        $data4      = json_decode($data4, true);
        $data5      = json_decode($data5, true);
        $data6      = json_decode($data6, true);
        $data7      = json_decode($data7, true);
        $data8      = json_decode($data8, true);
        $data9      = json_decode($data9, true);
        $c          = 0;
        $json       = array();
        foreach ($result as $re) {
            $total =0;
            $i     = $re['tech_id'];
            $val1  = $data1[$c][$i];
            $val1 =  $val/100; //sla ath
            $val2  = $data2[$c][$i];
            $val2  = $val2/100; 
            $val3  = $data3[$c][$i];
            $val4  = $data4[$c][$i];
            $val5  = $data5[$c][$i];
            $val6  = $data6[$c][$i];
            $val7  = $data7[$c][$i];
            $val8  = $data8[$c][$i];
            $val8  = $val8/100;
            $val9  = $data9[$c][$i];
            $val9  = $val9/100;
            //  $total=($data1[$c][$i]+$data2[$c][$i]+$data3[$c][$i]+$data4[$c][$i]+$data5[$c][$i]+$data6[$c][$i]+$data7[$c][$i])/7;
             $total = ($val1 + $val2 + $val3 + $val4 + $val5 + $val6 + $val7+ $val8+ $val9);
            array_push($json, array(
                "tech_id" => $i,
                "tech_name" => $re['tech_name'],
                "ticket_count" => $re['ticket_count'],
                "technician_level" => $re['skill_level'],
                "year" => date('Y'),
                "month" => date('m'),
                "company_id" => $company_id,
                "tech_reward_point" => $total
            ));
            $c++;
            
        }
         $sethu      = $this->reimbursement->leaderboard_newdata($json,$company_id);
        
    }


/* Displaying the report of c-sat, attendance, revenue, spare, productivity, FRM */
  public function report()
       {    
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        //$company_id=$this->input->post('company_id');
        $filter="year";
        $product="";
        $datass=$this->session->userdata('session_username');
        $datas= $this->session->userdata('companyid');
        //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
          $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
                $data['overall'] = $this->reimbursement->overall_claims($datas,$region,$area);          
                $data['reject']  = $this->reimbursement->Count_reject($datas,$region,$area);
                $data['rec']     = $this->reimbursement->disply_attendance($datas,$region,$area);
                $data['billing']    = $this->reimbursement->disply_billing($datas,$region,$area);
                $data['escallated'] = $this->reimbursement->disply_escallated1($datas,$region,$area);
                $data['completed']  = $this->reimbursement->disply_completed($datas,$region,$area);
                $data['inven']  =$this->reimbursement->disply_inventory($datas,$region,$area);
                $data['frminven']  =$this->reimbursement->disply_frminventory($datas,$region,$area);
                $this->load->model('model_service');
                $da=$this->reimbursement->csat_insights($datas, $filter,$product,$region,$area);
        //for($i=0;$i<=count($da);$i++) {
           if($da[2]['value']!=0)
           {
               $data['result4']=$da[2]['value'];
           }
           else
           {
               $data['result4']=0;
           }
           if($da[3]['value']!=0)
           {
               $data['result1']=$da[3]['value'];
           }
           else
           {
              $data['result1']=0;  
           }
    //  }
                $da1=$this->reimbursement->sla_complaince($datas,$region,$area);
                $data['result1']=$da1[0]['percent'];
                $data['result2']=$this->reimbursement->callclosed($datas,$region,$area);
                $data['result3']=$this->reimbursement->escallatedcalls($datas,$region,$area);
           $this->load->view('manager_report', $data); 
        }

     /* Displaying the bill image of reimbursement claim*/   
     public function view_image()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $datass=$this->session->userdata('session_username');
        $datas= $this->session->userdata('companyid');
        //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
        $status = $this->input->post('id');
        $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
        $data  = $this->reimbursement->view($status,$datas);
        print_r(json_encode($data));
    }
        
    /*Change technician in spare_transfer table */
    public function to_tech_id()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        //$datass=$this->session->userdata('session_username');
        $datas= $this->session->userdata('companyid');
        //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
        $status = $this->input->post('id');
        $data  = $this->reimbursement->to_tech_id($status);
        print_r(json_encode($data));
    }

/* Displaying the reimbursement details based on tickets */
public function view_form()
    {
        $this->load->helper('url');
        $this->load->database();
              $this->load->model('reimbursement');
        $company_id = $this->session->userdata('companyid');
        $tech_id = $this->input->post('tech_id');
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $data= $this->reimbursement->view_form($tech_id,$company_id,$from,$to);
        print_r(json_encode($data));
    }

/* Display customer satisfaction report */
     public function display_csat()
    {
        if($this->session->userdata('user_logged_in'))
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $day = $this->input->post('day');
        $region = $this->input->post('region');
        $area = $this->input->post('area');
        $location = $this->input->post('location');
        $this->load->model('reimbursement');
        $data=$this->reimbursement->display_csat($company_id,$day,$region,$area,$location,$product_id);
        return $data;
    }
    else
        {
            redirect('login/login');
        }
    }


        public function assigned_tickets()
    {
        if($this->session->userdata('user_logged_in'))
    {   
            $this->load->helper('url');
            $this->load->database();
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            
            $this->load->view('manager_assigned',$data);   
        }
        else
        {
            redirect('login/login');
        }
    }

    /*Displaying ongoing ticket list */
        public function ongoing()
    {
    if($this->session->userdata('user_logged_in'))
        {   
            $this->load->helper('url');
            $this->load->database();
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            $this->load->view('manager_ongoing',$data);   
        }
        else
        {
            redirect('login/login');
        }
    }


    public function completed()
    {
        if($this->session->userdata('user_logged_in'))
        {   
            $this->load->helper('url');
            $this->load->database();
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            $this->load->view('manager_completed',$data);   
        }
        else
        {
            redirect('login/login');
        }
    }

    /*Displaying the contract details in manager login */
    public function Manager_Contract()
        {   
            if($this->session->userdata('user_logged_in'))
            {
                $this->load->helper('url');
                $this->load->database();
                $datass=$this->session->userdata('session_username');
                $datas= $this->session->userdata('companyid');
                $this->load->model('reimbursement');
                $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
                $data['record']   = $this->reimbursement->disply_contract($datas,$region,$area);
                $data['accepted'] = $this->reimbursement->disply_accept_contract($datas,$region,$area);
                $this->load->view('manager_contract', $data);    
            }
            else {
                redirect('login/login');
            }
       }
      /*Displaying reimbursement details*/ 
    public function reimbursement()
        {   
            $this->load->helper('url');
            $this->load->database();
                $datass=$this->session->userdata('session_username');
                $datas= $this->session->userdata('companyid');
            $company_id = $this->session->userdata('companyid');
            $this->load->model('reimbursement');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['record']   = $this->reimbursement->disply_employee($company_id,$region,$area);
            $data['accepted'] = $this->reimbursement->accepted($company_id,$region,$area);
            $data['rejected'] = $this->reimbursement->rejected($company_id,$region,$area);
            //$data['overall'] = $this->reimbursement->overall_claims($company_id);
            //$data['accept']  = $this->reimbursement->Count_accept($company_id);
            //$data['reject']  = $this->reimbursement->Count_reject($company_id);
            $this->load->view('manager_reimb', $data);    
        }


        /* Displaying the spare request details */
    public function Spare_request()
  { 
            $this->load->helper('url');
            $this->load->database();
                $datass=$this->session->userdata('session_username');
                $datas= $this->session->userdata('companyid');
            $company_id = $this->session->userdata('companyid');
            $this->load->model('reimbursement');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['record']= $this->reimbursement->disply_Spare($company_id,$region,$area);
            $data['records']= $this->reimbursement->disply_accept_Spare($company_id,$region,$area);
            $data['recorded']= $this->reimbursement->disply_reject_Spare($company_id,$region,$area);
            $this->load->view('manager_Spare', $data);
        }


       /* It is changing spare requested status to accepted*/ 
      public function spare_accept()
    {
        $this->load->helper('url');
        $this->load->database();
        //$company_id=$this->input->post('company_id');
        $company_id = $this->session->userdata('companyid');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->accept_spare_request($company_id,$filter);
        echo $data;
        return $data;
    }


    /* It is changing the spare request status to reject */
 public function spare_reject()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->session->userdata('companyid');
        //$company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->reject_spare_request($company_id,$filter);
        echo $data;
        return $data;
    }


    /*Displaying the billing image from billing table, which is sent by technician mobile application */
    public function view_billimages()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $id=$this->input->post('id');
        $c=$this->input->post('company');
        $data=$this->reimbursement->view_billimages($id,$c);
        echo (json_encode(array("bill_images"=>$data)));
    }

   /*Displaying permarmance metrix configuration */
    public function service_desk ()
    {
            $this->load->helper('url');
            $this->load->database();
                $datass=$this->session->userdata('session_username');
                $datas= $this->session->userdata('companyid');
            $company_id = $this->session->userdata('companyid');
            $this->load->model('reimbursement');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
        
            $data['record']= $this->reimbursement->disply_servicedesk($company_id);

            $data['record1']= $this->reimbursement->disply_servicedesk1($company_id);
            $data['record2']= $this->reimbursement->disply_servicedesk2($company_id);
            $data['record3']= $this->reimbursement->disply_servicedesk3($company_id);
            $data['record4']= $this->reimbursement->disply_rewarddesk($company_id);
            $data['record5']= $this->reimbursement->disply_rewarddesk2($company_id);
            $data['record6']= $this->reimbursement->disply_rewarddesk3($company_id);
            $data['record7']= $this->reimbursement->disply_rewarddesk4($company_id);
            $this->load->view('manager_service_disc', $data);
        }

/*Updating the servie tax and GST for products */
public function Service_tax()
            {
                $this->load->helper('url');
                $this->load->database();                
                $data=array(
                'pro_gst'=>$this->input->post('prod'),
                'ser_gst'=>$this->input->post('serv'),
                'company_id'=>$this->input->post('cid')
                );
                    $company_id=$this->session->userdata('companyid');
                    $this->load->model('Reimbursement');
                    $res = $this->Reimbursement->isservice_disc($company_id);
                    print_r($res);
                    if(!empty($res))
                    {
                        $result = $this->Reimbursement->update_disc($data,$company_id);
                    }
                    else
                    {
                        $result = $this->Reimbursement->service_disc($data);
            if($result==1)
            {
                echo "Added successfully";
            }
            else{
                echo "Error!!! Please recheck and add";
            }
            }
            }
 public function leader_stat()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->leaderboard_static($company_id,$filter);
        echo $data;
        return $data;
    }

    /* It is changing reimbursement status to accepted or not based on ticket */
    public function statuschange()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $company_id = $this->session->userdata('companyid');
        $changes = 11;
        $data    = array(
            'current_status' => $changes
        );
        $where= array(
             'all_tickets.ticket_id' => $status,
             'all_tickets.company_id'=>$company_id
            );
        $datas   = $this->reimbursement->statuschange($data, $where);
        
        if ($datas == 1) {
            echo 'You have accepted Spare(s) for ticket '. $status;
        } else {
            echo 'sorry try again';
        }
    }

    /*Displaying technician score board based on level */
public function load_closed()
    {
         if($this->session->userdata('user_logged_in'))
            { 
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $area=$this->input->post('area');
        $location=$this->input->post('location');
        $region=$this->input->post('region');
        $level=$this->input->post('level');
        $this->load->model('Reimbursement');
        $result=$this->Reimbursement->load_level_based($company_id,$area,$region,$location,$level);
      //  print_r($result);die();
        echo json_encode($result);
    }
    else
            {
                redirect('login/login');
            }
            
        }

/*Change the contract status to approved */
  public function contract_status()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $action  = $this->input->post('id');
        $company_id  = $this->input->post('company_id');
      
        
        $changes = 20;
        $data    = array(
                  'current_status' => $changes
        );
     
          $datas = $this->reimbursement->contract_change($data,$action,$company_id);
      
        if ($datas = 1) {
            echo 'You have Approved the Contract for Ticket ' . $action;
        } 
        else {
            echo 'Something went Wrong';
        }
    }

 /*Change the contract status to rejected */
    public function contract_reject()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $action  = $this->input->post('id');
        $company_id  = $this->input->post('company_id');
        $changes = 19;
        $data1    = array(
            'current_status' => $changes
        );
        $datas   = $this->reimbursement->contract_change1($data1, $action,$company_id);
        if ($datas = 1) {
            echo 'You have rejected Contract for the Ticket ' . $action;
        } else {
            echo 'sorry try again';
        }
    }

    /*It is changing status to reject */
    public function rejectstatus()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $company_id = $this->session->userdata('companyid');
        $changes = 17;
        $data    = array(
            'current_status' => $changes
        );
        $where= array(
            'all_tickets.ticket_id' => $status,
             'all_tickets.company_id'=>$company_id
            );
        $datas   = $this->reimbursement->statuschange($data, $where);
        if ($datas == 1) {
            echo 'You have rejected spare(s) for the Ticket  ' . $status;
        } else {
            echo 'sorry try again';
        }
    }
    public function view()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        //$datass=$this->session->userdata('session_username');
        //$datas= $this->session->userdata('companyid');
        //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
        $status = $this->input->post('id');
        $datas  = $this->reimbursement->view($status);
        print_r(json_encode($datas));
    }
   public function status()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $from  = $this->input->post('from');
        $to  = $this->input->post('to');
        $comp  = $this->session->userdata('companyid');
        $changes = 1;
        $data    = array(
            'action' => $changes,'status'=>13
        );
        $where=array(
            'technician_id'=>$status,
            'start_date'=>$from,
            'end_date'=>$to,
            'company_id'=>$comp
            );
        $datas   = $this->reimbursement->claimchange($data,$where);
        if ($datas = 1) {
            echo ' Reimbursement accepted Successfully';
        } else {
            echo 'sorry try again';
        }
    }
public function Service_call()
            {
                if($this->session->userdata('user_logged_in'))
            {
                $this->load->helper('url');
                $this->load->database();
                $c_id=$this->input->post('c_id');
                $call=$this->input->post('call');               
                $data=array(
                'company_id'=>$this->input->post('c_id'),
                'call'=>$this->input->post('call'),
                'mttr'=>$this->input->post('mttr')
                );
                    $this->load->model('Reimbursement');
                    $result = $this->Reimbursement->service_call($data,$call);
            if($result==1)
            {
                //echo "Added successfully";
            }
            else{
                echo "Error!!! Please recheck and add";
            }
            }
            else {
                redirect('login/login');
            }
       }


    public function reject()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $from  = $this->input->post('from');
        $to  = $this->input->post('to');
         $comp  = $this->session->userdata('companyid');
        $changes = 2;
          $data    = array(
            'action' => $changes,'status'=>13
        );
        $where=array(
            'technician_id'=>$status,
            'start_date'=>$from,
            'end_date'=>$to,
            'company_id'=>$comp
            );
        $datas   = $this->reimbursement->claimchange($data, $where);
        if ($datas = 1) {
            echo 'Reimbursement rejected successfully';
        } else {
            echo 'sorry try again';
        }
    }

    /*Displaying impressed spare details */
 public function impressed_spare()
    { 
            $this->load->helper('url');
            $this->load->database();
            $company_id = $this->session->userdata('companyid');
            $this->load->model('reimbursement');
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
        //  $data['pass']=$this->Admin_model->get_employee($datass,$datas);
        $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['record']= $this->reimbursement->impressed_Spare($company_id,$region,$area);
            $data['records']= $this->reimbursement->impressed_accept_Spare($company_id,$region,$area);
           // $data['recorded']= $this->reimbursement->disply_reject_Spare();
            $this->load->view('manager_personal Spare', $data);
        }
    
   /* Display amc details based on date */
        public function amc_day()
    {
        if($this->session->userdata('user_logged_in'))
            {
        $this->load->helper('url');
        $this->load->database();
        $company_id =$this->session->userdata('companyid');
        $filter=$this->input->post('filter');
        $revenue=$this->input->post('revenue');
        $product_id=$this->input->post('product_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $location=$this->input->post('location');
         $this->load->model('reimbursement');
        $data=array();
        $data = $this->reimbursement->disply_amc_bling($company_id,$filter,$revenue,$product_id,$region,$area,$location);        
        return $data;       
    }
    else {
                redirect('login/login');
            }
       }

       /* Display escallated tickets*/
    public function escallated2()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id =$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->disply_escallated2($company_id,$filter);
        return $data;
    }

    /* Change FRM status in billing table */
     public function change_frm()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $company_id =$this->input->post('comp');
        $filter=$this->input->post('filter');
        $data= $this->reimbursement->change_frm($company_id,$filter);
        return $data;
    }
 /*Display spare details */
    public function inventory2()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->session->userdata('companyid');
         //$company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $product_id=$this->input->post('product_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $location=$this->input->post('location');
        $filter=$this->input->post('filter');
        $spare_bill=$this->input->post('spare_bill');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->disply_inventory2($company_id,$filter,$spare_bill,$product_id,$region,$area,$location);
        return $data;
    }
/*Displaying attendance details */
public function controll_attendance()
    {
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $product_id=$this->input->post('product_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $location=$this->input->post('location');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->load_attendance($company_id,$product_id,$region,$area,$location);
        return $result;
    }
    
   

    /* Display complete ticket's information */
    public function completed2()
    {
        $this->load->helper('url');
        $this->load->database();
      
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $product_id=$this->input->post('product_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $location=$this->input->post('location');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->disply_completed2($company_id,$filter,$product_id,$region,$area,$location);
        return $data;
    }
    public function productivity_trends()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->productivity_trends($company_id,$filter);
        return $result;
    }
    public function customer_status()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->customer_status($company_id,$filter);
        return $result;
    }
    public function field_force()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->field_force($company_id,$filter);
        return $result;
    }

/* Change the status to reimbursement acceptance*/
   public function reimb_accept()
    {
        $this->load->helper('url');
        $this->load->database();
        //$company_id=$this->input->post('company_id');
        $company_id=$this->session->userdata('companyid');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->reimb_accept($company_id,$filter);
        echo $data;
        return $data;
    }
/* Change the status to reimbursement reject */
  public function reimb_reject()
    {
        $this->load->helper('url');
        $this->load->database();
        //$company_id=$this->input->post('company_id');
        $company_id=$this->session->userdata('companyid');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $data= $this->reimbursement->reimb_reject($company_id,$filter);
        echo $data;
        return $data;
    }
    public function revenue_status()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->revenue_status($company_id,$filter);
        return $result;
    }
    public function tech_count()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $filter=$this->input->post('filter');
        $this->load->model('reimbursement');
        $result=$this->reimbursement->tech_count($company_id,$filter);
        return $result;
    }
//<!-- Sla Contract -->
    public function score()
    {
        if($this->session->userdata('user_logged_in'))
        {
            $this->load->helper('url');
            $this->load->model('Reimbursement');
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
            $data['record']= $this->Reimbursement->display_score($datas);
            $data['wheels']= $this->Reimbursement->select_service_disc($datas);//print_r($data['wheels']);
            $this->load->view('score_man',$data);
        }
        else {
                redirect('login/login');
            }
       }

       /*Displaying the sla details for manager login */
    public function addsla()
      {
             if($this->session->userdata('user_logged_in'))
            {      
                $datass=$this->session->userdata('session_username');
                $datas= $this->session->userdata('companyname');             
                $datas= $this->session->userdata('companyid');
                $this->load->model('Admin_model');
                $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
                $data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
                $data['details']= $this->Admin_model->disply($datas);
                $data['priority_level']=$this->Admin_model->priority_level();
$data['get_sla']=$this->Admin_model->sla_get($datas);
                $this->load->view('sla_man',$data);
            }
            else
            {
                redirect('login/login');
            }
            
        }


    public function load_techregion()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_techregion($company_id);
        return $product;
    }

    /* Display location list from technician table */
    public function load_techlocation()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_techlocation($company_id);
        return $product;
    }

    /*Display area list from technician table*/
    public function load_techarea()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_techarea($company_id);
        return $product;
    }

    /* Displaying products of relevant technicians */
    public function load_techproduct()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_techproduct($company_id);
        return $product;
    }
    
    public function load_region()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $datas= $this->session->userdata('companyid');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_region($datas);
        return $product;
    }

    /* Displaying location list of technicians */
    public function load_location()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_location($company_id,$region,$area);
        return $product;
    }

    /* Displaying town from all tickets table */
    public function load_area()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_area($company_id,$region,$area);
        return $product;
    }
    public function load_product()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_product($company_id);
        return $product;
    }

    /*Displaying area list of technicians */
    public function load_area_tech()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_area_tech($company_id,$region);
        return $product;
    }



    /*Displaying location list of technicians */
    public function load_location_tech()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_location_tech($company_id,$region,$area);
        return $product;
    }


    public function load_cat()
    {
        
        $this->load->helper('url');
        $this->load->database();
        $company_id=$this->input->post('company_id');
        $product_id=$this->input->post('product_id');
        $this->load->model('Reimbursement');
        $product=$this->Reimbursement->load_cat($company_id,$product_id);
        return $product;
    }
    public function contract()
    {
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Reimbursement');
            //$datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $region=$user['region'];
            $area=$user['area'];
            $location=$user['location'];
            $data['user']=$this->Admin_model->get_details_user($datass,$datas,$region,$area);
            //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
            $data['record']= $this->Reimbursement->disply_invento($datas);
            $data['amc']= $this->Reimbursement->disply_inven($datas);
            $this->load->view('contract_amc',$data);
        }
 /*Update the new score target */
    public function add_newtarget(){
        $this->load->helper('url');
        $this->load->database();
        $c_id = $this->session->userdata('companyid');
        $this->load->model('Reimbursement');
        $id1=
        $data=array(
        'technician_level'=>$this->input->post('id1'),
        'average_calls'=>$this->input->post('avgcall'),
        'first_time'=>$this->input->post('first_time'),
        'cust_feed'=>$this->input->post('cus_feed'),
        'contract_revenue'=>$this->input->post('service_revev'),
        'customer_rating'=>$this->input->post('custo_rat'),
        'contribution_to_kb'=>$this->input->post('cont_kb'),
        'training_certification'=>$this->input->post('training'),
        'sla_adher'=>$this->input->post('sla_adher'),
        'mttr'=>$this->input->post('mttr'),
        'company_id'=>$this->input->post('company_id')
        );
        $result = $this->Reimbursement->add_newscore($data);
        
    }
  /*Save new score reward */
    public function add_newreward(){
        $this->load->helper('url');
        $this->load->database();
        $c_id = $this->session->userdata('companyid');
        $this->load->model('Reimbursement');
        $id1=
        $data=array(
        'technician_level'=>$this->input->post('id1'),
        'average_calls'=>$this->input->post('avgcall'),
        'first_time'=>$this->input->post('first_time'),
        'cust_feed'=>$this->input->post('cus_feed'),
        'contract_revenue'=>$this->input->post('service_revev'),
        'customer_rating'=>$this->input->post('custo_rat'),
        'contribution_to_kb'=>$this->input->post('cont_kb'),
        'training_certification'=>$this->input->post('training'),
        'sla_adher'=>$this->input->post('sla_adher'),
        'mttr'=>$this->input->post('mttr'),
        'company_id'=>$this->input->post('company_id')
        );
        $result = $this->Reimbursement->add_newreward($data);
        
    }

 /* Updating the score */
    public function add_lead()
    {
        $this->load->helper('url');
        $this->load->database();
        $c_id = $this->session->userdata('companyid');
        $this->load->model('Reimbursement');
        $id1=$this->input->post('id1');
        $data=array(
        'average_calls'=>$this->input->post('avgcall'),
        'first_time'=>$this->input->post('first_time'),
        'cust_feed'=>$this->input->post('cus_feed'),
        'contract_revenue'=>$this->input->post('service_revev'),
        'customer_rating'=>$this->input->post('custo_rat'),
        'contribution_to_kb'=>$this->input->post('cont_kb'),
        'training_certification'=>$this->input->post('training'),
        'sla_adher'=>$this->input->post('sla_adher'),
        'mttr'=>$this->input->post('mttr'));
        $result = $this->Reimbursement->add_score($data,$id1,$c_id);
    }
/*Add new score reward */
    public function add_reward()
    {
        $this->load->helper('url');
        $this->load->database(); 
        $company_id=$this->input->post('company_id');
        $id2=$this->input->post('id2');
       
        $data=array(
        'average_calls'=>$this->input->post('avgcall1'),
        'first_time'=>$this->input->post('first_time1'),
        'cust_feed'=>$this->input->post('cus_feed1'),
        'contract_revenue'=>$this->input->post('service_revev1'),
        'customer_rating'=>$this->input->post('custo_rat1'),
        'contribution_to_kb'=>$this->input->post('cont_kb1'),
        'training_certification'=>$this->input->post('training1'),
        'sla_adher'=>$this->input->post('sla_adher1'),
        'mttr'=>$this->input->post('mttr1')
        );
        
            $this->load->model('Reimbursement');
            $result = $this->Reimbursement->add_reward($data,$id2);
    
    }

    /* SLA compination information with product management table */
 public function sla_complaince()
    {
        $result = array();
        $this->load->helper('url');
        $this->load->database();
        $company_id = $this->input->post('company_id');
        $this->load->model('reimbursement');
        $sla    = $this->reimbursement->sla_complaince($company_id);
        $feed   = $this->reimbursement->custfeed($company_id);
        $rating = $this->reimbursement->custrat($company_id);
        array_push($result, array(
            'sla' => $sla,
            'feed' => $feed,
            'rating' => $rating
        ));
        echo json_encode($result);
    }
    public function sla_adh()
    {
                $this->load->helper('url');
                $this->load->database(); 
                
                $data=array(
                'priority_level'=>$this->input->post('avgcall'),
                'response_time'=>$this->input->post('first_time'),
                'resolution_time'=>$this->input->post('cus_feed'),
                'SLA_Compliance_Target'=>$this->input->post('service_revev'));
                
                    $this->load->model('Reimbursement');
                    $result = $this->Reimbursement->sla_adh($data);
            if($result==1)
            {
                echo "Added successfully";
            }
            else{
                echo "Error!!! Please recheck and add";
            }
      }

/*Updating the allowance details for two wheelor and four wheelor */
    public function Service_disc()
            {
                $this->load->helper('url');
                $this->load->database();
                $data=array(
                'two_whel'=>$this->input->post('two'),
                'four_whel'=>$this->input->post('four'),
                'company_id'=>$this->session->userdata('companyid')
                );
                        $company_id=$this->session->userdata('companyid');
                    $this->load->model('Reimbursement');
                    $res = $this->Reimbursement->isservice_disc($company_id);
                    print_r($res);
                    if(!empty($res))
                    {
                        $result = $this->Reimbursement->update_disc($data,$company_id);
                    }
                    else
                    {
                        $result = $this->Reimbursement->service_disc($data);
                        if($result==1)
                            {
                                //echo "Added successfully";
                            }
                        else{
                                echo "Error!!! Please recheck and add";
                            }
                    }
                    
            }


public function update_service_disc()
            {
                $this->load->helper('url');
                $this->load->database();    
                $id=$this->input->post('id_1');             
                $data=array(
                'response_time'=>$this->input->post('res_time'),
                'resolution_time'=>$this->input->post('price'),
                );
                    $this->load->model('Reimbursement');
                    $result = $this->Reimbursement->update_service_disc($data,$id);
                    print_r($result);
            }


    public function view_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Reimbursement');
        $data=$this->input->post('id');
        $data=$this->Reimbursement->getdetails_sla($data);
        echo json_encode($data);
    }


    public function add_contract()
            {
                $this->load->helper('url');
                $this->load->database(); 
                
                $data=array(
                'amc_type'=>$this->input->post('avgcall'),
                'company_id'=>$this->input->post('company'));
                
                    $this->load->model('Reimbursement');
                    $result = $this->Reimbursement->add_contract($data);
            if($result==1)
            {
                echo "Added successfully";
            }
            else{
                echo "Error!!! Please recheck and add";
            }
            }
    public function add_price()
            {
                $this->load->helper('url');
                $this->load->database(); 
            
                $data=array(
                'cat_id'=>$this->input->post('contr'),  
                'contract_type'=>$this->input->post('amc'),
                'price'=>$this->input->post('price'),
                'company_id'=>$this->input->post('company'));
                
                    $this->load->model('Reimbursement');
                    $result = $this->Reimbursement->add_price($data);
            if($result==1)
            {
                echo "Added successfully";
            }
            else{
                echo "Error!!! Please recheck and add";
            }
            }
    public function deletesla()
        {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Reimbursement');
        $id=$this->input->post('id');
        $data=$this->Reimbursement->deletesla($id);
        echo $data;
        }
    public function edit_sla()
        { 
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Reimbursement');
            $this->load->library('form_validation');    
            $priority_level=$this->input->post('avgcall1');         
                    $data=array(
                            'response_time'=>$this->input->post('first_time1'),
                            'resolution_time'=>$this->input->post('cus_feed1'),
                            'SLA_Compliance_Target'=>$this->input->post('service_revev1')
                        );
                         $data =$this->Reimbursement->edit_sla($data,$priority_level);
                        if ($data ==1){
                        echo "User updated Successfully";}
        }
    public function choose_company() {
                //calculates next coimng monday(ie) starting day of week
            /* $date=date('Y-m-d', strtotime('2016-12-30'));
            echo $monday = date('Y-m-d',strtotime('monday', strtotime($date))); */
            $this->load->helper('url');
            $this->load->database();                
            $this->load->model('Reimbursement');
            $company =$this->Reimbursement->choose_company();
            return $company; 
        
        }
public function impresed_status()
     {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $tech_id  = $this->input->post('tech_id');
        $cid= $this->session->userdata('companyid');
        $changes = 1;
        $data    = array(
            'status' => $changes
        );
         $where= array(
            'id'=>$status,
            'company_id'=>$cid,'tech_id'=>$tech_id
             );
        $datas   = $this->reimbursement->impressed_accept($where,$tech_id,$cid);
        if ($datas == 1) {
        $datas1   = $this->reimbursement->impressed_accept_status($data, $where);
        if ($datas1 == 1) {
            echo 'You have accepted the Spare request';
        }
        else
        {
             echo 'Sorry try again';
        }
        } else {
            echo 'Sorry try again';
        }
    }

   /* Accepting spare transfer request */
    public function transfer_accept()
     {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $tech_id  = $this->input->post('tech_id');
        $cid= $this->session->userdata('companyid');
        $changes = 1;
        $data    = array(
            'status' => $changes
        );
         $where= array(
            'id'=>$status,
            'company_id'=>$cid,'from_tech_id'=>$tech_id
             );
        $datas   = $this->reimbursement->transfer_accept($where,$data);
        if ($datas == 1) {
        
            echo 'You have accepted the Spare transfer request';
        
        } else {
            echo 'Sorry try again';
        }
    }

   /*Rejecting spare transfer request by manager */
    public function transfer_reject()
     {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
        $cid= $this->session->userdata('companyid');
        $changes = 2;
        $data    = array(
            'status' => $changes
        );
         $where= array(
            'id'=>$status,
            'company_id'=>$cid
             );
        $datas   = $this->reimbursement->transfer_accept($where,$data);
        if ($datas == 1) {
        
            echo 'You have rejected the Spare transfer request';
        
        } else {
            echo 'Sorry try again';
        }
    }
     public function impresed_reject_status()
     {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $status  = $this->input->post('id');
         $cid= $this->session->userdata('companyid');
        $changes = 2;
        $data    = array(
            'status' => $changes
        );
          $where= array(
            'id'=>$status,
            'company_id'=>$cid
             );
        $datas   = $this->reimbursement->impressed_accept_status($data, $where);
        if ($datas = 1) {
            echo 'You have rejected the spare request';
        } else {
            echo 'sorry try again';
        }
    }
    public function check()
        {
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Reimbursement');
            /* $product = $this->input->post('product');
                $category = $this->input->post('category'); 
                $customer = $this->input->post('customer');
                $contract = $this->input->post('contract');
                $call_cat = $this->input->post('txtPassportNumber0');
                echo $call_cat;
                $keywords = explode(',', $call_cat); */
                $company_name =  $this->input->post('company_name');
                $insert1 = $this->Reimbursement->check($company_name);
                return $insert1;
        }
    public function add_sla()
        {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Reimbursement');
                $company_name =  $this->input->post('company_name');
                $priority = $this->input->post('priority');
                $rhours = $this->input->post('rhours'); 
                $rminute = $this->input->post('rminute');   
                $rseconds = $this->input->post('rseconds');
                $resp_time=$rhours.':'.$rminute.':'.$rseconds;
                //$resp_time=date('',strtotime($restime));
                $hours = $this->input->post('hours');   
                $minute = $this->input->post('minute'); 
                $seconds = $this->input->post('seconds');
                $resolution=$hours.':'.$minute.':'.$seconds;
                //$resp_time=date('H:i:s',strtotime($restime));
                //$resolution=date('H:i:s',strtotime($resoltime));
                $mttr_target = $this->input->post('mttr_target');
                $sla_target = $this->input->post('service_reven');
        if( empty($company_name) && !empty($priority) && !empty($resp_time) && !empty($resolution) && !empty($mttr_target) && !empty($sla_target))
        {
            echo "Select Company to Map SLA Compliance";
        }
        else if( !empty($company_name) && empty($priority) && !empty($resp_time) && !empty($resolution) && !empty($mttr_target) && !empty($sla_target))
        {
            echo "Set Priority to Map SLA Compliance";
        }
        else if( !empty($company_name) && !empty($priority) && empty($resp_time) && !empty($resolution) && !empty($mttr_target) && !empty($sla_target))
        {
            echo "Set Response Time ";
        }
        else if( !empty($company_name) && !empty($priority) && !empty($resp_time) && empty($resolution) && !empty($mttr_target) && !empty($sla_target))
        {
            echo "Set Resolution Time ";
        }
        else if( !empty($company_name) && !empty($priority) && !empty($resp_time) && !empty($resolution) && empty($mttr_target) && !empty($sla_target))
        {
            echo "Set MTTR Target ";
        }
        else if( !empty($company_name) && !empty($priority) && !empty($resp_time) && !empty($resolution) && !empty($mttr_target) && empty($sla_target))
        {
            echo "Set SLA Compliance Target ";
        }
        else 
        {
            $product = $this->input->post('for_prod');
            $category = $this->input->post('for_cat');  
            $customer = $this->input->post('for_cust');
            $contract = $this->input->post('for_cont_type');
            $call_cat = $this->input->post('txtPassportNumber0');
            echo $call_cat;
            $keywords = explode(',', $call_cat);
            // Build query
            // foreach ($keywords as $keyword)
            // {
                // $keyword = trim($keyword);
                // $this->db->or_where("`terms` LIKE '%$keyword%'");
            // }
            // $query = $this->db->get('filter_tbl');
            // return $query->num_rows();

            if( empty($call_cat))
            {
            if($product=='')
                {
                $product = 'all';
                }
                if($category=='')
                {
                $category = 'all';
                }
                if($customer=='')
                {
                $customer = 'all';
                }
                if($contract=='')
                {
                $contract = 'all';
                }
                if($call_cat=='')
                {
                $call_cat = 'all';
                }
                $ref_id = $this->Reimbursement->get_reference_id($company_name);
                $adata1=array(
                        'company_id'=>$company_name,
                        'priority_level'=>$priority,
                        'response_time'=>$resp_time,
                        'resolution_time'=>$resolution,
                        'mttr_target'=> $mttr_target,
                        'SLA_Compliance_Target'=>$sla_target,
                        'ref_id'=>$ref_id
                        );
                $adata2=array(
                        'company_id'=>$company_name,
                        'product'=>$product,
                        'category'=>$category,
                        'cust_category'=>$customer,
                        'call_category'=> $call_cat,
                        'service_category'=>$contract,
                        'ref_id'=>$ref_id
                        );
            
                    $insert1 = $this->Reimbursement->add_sla1($adata2);
                    if($insert1==1){
                                $ins = $this->Reimbursement->add_sla($adata1);
                                if($ins==1)
                                {
                                echo "Inserted.";
                                }
                            }
                            else
                                {
                                echo "This combination already Already Exist";
                                }
            }
            else
            {
                if(empty($product)){
                    $product = 'all';
                }
                if(empty($category)){
                    $category = 'all';
                }
                if(empty($customer)){
                    $customer = 'all';
                }
                if(empty($contract)){
                    $contract = 'all';
                }
                if(empty($call_cat)){
                    $call_cat = 'all';
                }
                foreach($keywords as $key){
                $ref_id = $this->Reimbursement->get_reference_id($company_name);
                $adata1=array(
                        'company_id'=>$company_name,
                        'priority_level'=>$priority,
                        'response_time'=>$resp_time,
                        'resolution_time'=>$resolution,
                        'mttr_target'=> $mttr_target,
                        'SLA_Compliance_Target'=>$sla_target,
                        'ref_id'=>$ref_id
                        );
                $adata2=array(
                        'company_id'=>$company_name,
                        'product'=>$product,
                        'category'=>$category,
                        'cust_category'=>$customer,
                        'call_category'=> $key,
                        'service_category'=>$contract,
                        'ref_id'=>$ref_id
                        );
            
                    $insert1 = $this->Reimbursement->add_sla1($adata2);
                    if($insert1==1){
                                $ins = $this->Reimbursement->add_sla($adata1);
                                if($ins==1)
                                {
                                echo "Inserted.";
                                }
                            }
                            else
                                {
                                echo "This combination already Already Exist";
                                }
                }   
        
            }   
        }
    }   
        public function getproduct(){
            $this->load->database();
            $this->load->model('Reimbursement');
            $data=$this->Reimbursement->getproduct_s();
            $nodes = array();
            foreach($data as $val)
            {                               
                array_push($nodes,array('id'=>$val['product_id'],'parent'=>"#",'text'=>$val['product_name'],'icon'=>$val['product_image']));
                $prod_id_1=$val['product_id'];
                $data_category=$this->Reimbursement->getcategory_s($prod_id_1); 
                foreach($data_category as $val_category){
                    array_push($nodes,array('id'=>$val_category['cat_id'],'parent'=>$val_category['prod_id'],'text'=>$val_category['cat_name'],'icon'=>$val_category['cat_image']));
                }
            }
            echo json_encode($nodes);
        }

        /*Displaying the notifications of new tickets, spare details, training and sla details */
        public function manager_notifications(){ //added
            $this->load->helper('url');
            $this->load->database();
            $datass=$this->session->userdata('session_username');
            $datas= $this->session->userdata('companyid');
            
            $user = $this->Admin_model->get_details_user($datass,$datas); 
            $area=$user['area'];
            $region=$user['region'];
            $this->load->model('pushnotify');
            $data['new_tickets']=$this->pushnotify->manager_notifications_new_tickets($datass,$datas,$area,$region);
            $data['escalated_tickets']=$this->pushnotify->manager_notifications_escalated_tickets($datass,$datas,$area,$region);
            $data['completed_tickets']=$this->pushnotify->manager_notifications_completed_tickets($datass,$datas,$area,$region);
            $data['spare_request']=$this->pushnotify->manager_notifications_spare_requests($datass,$datas,$area,$region);
            $data['spares']=$this->pushnotify->manager_notifications_spares($datass,$datas,$area,$region);
            $data['training']=$this->pushnotify->manager_notifications_training($datass,$datas,$area,$region);
             $data['slamapping']=$this->pushnotify->manager_notifications_slamapping($datass,$datas,$area,$region);
            $notifications=$this->pushnotify->manager_notifications_users($datass,$datas,$area,$region);
            echo sizeof($notifications);
            
            $resultarray=array();
            for($i=0;$i<sizeof($notifications);$i++)
            {
                $resultarray[]=get_object_vars($notifications[$i]);
            }
    
    
            $data['new_users'] =$resultarray;
           
    
            $this->load->view('manager_notification', $data);    
        }
/* displaying call tags */
        public function display_call_category(){
            $this->load->database();
            $this->load->model('Reimbursement');
            $cat_id  = $this->input->post('id');
            $cid= $this->session->userdata('companyid');
            $result = $this->Reimbursement->display_call_category($cat_id,$cid);
            echo json_encode($result);
        }
/*Update the call tags based on company */
        public function update_cal_cat(){
            $this->load->database();
            $this->load->model('Reimbursement');
            $companyid = $this->session->userdata('companyid');
            $id = $this->input->post('id');
           
            $data = array(
                'call' => $this->input->post('call'),
                'mttr' => $this->input->post('mttr')
            );
            
            $update = $this->Reimbursement->update_call_cat($id,$data,$companyid);
            if($update == 1){
                echo "Call category updated";
            }
            else{
                echo "Error in updating";
            }
        }


        /* Remove call category */
        public function delete_call_cat()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Reimbursement');
        $id   = $this->input->post('id');
        $companyid = $this->session->userdata('companyid');
        $data = $this->Reimbursement->delete_call_cat($id,$companyid);
        echo $data;
    }
    
}
