<?php
	//session_start(); //we need to start session in order to access it through CI
	
	Class Login extends CI_Controller {
	
		public function __construct() {
			
			parent::__construct();	
			$this->load->library('encrypt');			
		 	$this->load->helper(array('url','cookie'));
			$this->load->helper('form');						
			$this->load->library('form_validation');						
			$this->load->library('session');		
			$this->load->model('Login_model');			
			$this->load->model('Admin_model');	
			$this->load->model('Cronfunctions_model');		
		}
		
		// Show login page
		public function index() {
			$this->load->model('Cronfunctions_model');
			$this->Cronfunctions_model->automatic_block();
			if($this->session->userdata('user_logged_in'))
			  {			  	
			  	 //$result=$this->input->cookie('user',true); 
			  	 redirect('Login/user_login_page');	      
			  }
		  	else
			  {
				  redirect('login/login');
			  }
			$this->load->view('login');		 	
		}
		public function login()
		{
			if($this->session->userdata('user_logged_in'))
			{			
				redirect('login/user_login_page');	      
			}
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username','username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if($this->form_validation->run())
			{
				$data=$this->Login_model->check_valid_user_or_not();
				if($data)
				{
					if($data=="Your company subscription is closed, kindly contact Super admin"){
						echo '<script type="text/javascript">var answer = confirm("Your company subscription is closed, kindly contact Super admin");if(answer){window.location.href="index.php?/Login/login";}else{window.location.href="index.php?/Login/login";}</script>';
					}else{
						redirect('login/user_login_page');
					}   
				}
				else
				{
					echo '<script type="text/javascript">var answer = confirm("User name or Password is Incorrect");if(answer){window.location.href="index.php?/Login/login";}else{window.location.href="index.php?/Login/login";}</script>';
				}
			}
			$this->load->view('Login');
		}
		public function logout()
		{
			// Clear Session
			$this->session->sess_destroy();		
			redirect('login', 'refresh');
		}  
                            public function callpassword_changes()
    		{
    			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  

			$data['userrole']=$data_role['user_type'];
			$this->load->view('callchangePasswords',$data_role);	
    		}
		public function servpassword_changes()
    		{
    			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  

			$data['userrole']=$data_role['user_type'];
			$this->load->view('servchangePasswords',$data_role);	
    		}	
		public function admpassword_changes()
    		{
    			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  

			$data['userrole']=$data_role['user_type'];
			$this->load->view('changePasswords',$data_role);	
    		}
		public function manpassword_changes()
    		{
    			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  

			$data['userrole']=$data_role['user_type'];
			$this->load->view('manchangePasswords',$data_role);	
			}
			
			// Displaying dashboard for logged in user
		public function user_login_page()
		 {
		 	if(!$this->session->userdata('user_logged_in'))
			{
			   redirect('login');
			 
			}
			else
			{
				$datass=$this->session->userdata('session_username');			
			    $datas= $this->session->userdata('companyid');			 
			    $username= $this->session->userdata('username');

			    $this->load->model('Super_admin');
			 	$this->load->model('Admin_model');

			 if($datass=="superadmin@kaspontech.com"){
				$data['record']=$this->Super_admin->company_count();
	        	$data['renew']=$this->Super_admin->renew_count();
				$data['service']=$this->Super_admin->count_s();
                //$data['service']=$this->Super_admin->counts();
				//$data['tech']=$this->Super_admin->count1();
                $data['tech']=$this->Super_admin->count_t();
				$this->load->view('superad_dashboard',$data);		
			 }
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  
			$user=$data_role;
			//echo "hello".$data_role['user_type'];
		//	die();
			if($data_role['user_type']=='Admin'){
				$user=$data_role;
					//foreach($user as $dt)
				//	{
						$region="";
						$area="";
						$location="";
				//	}			
				$data['pass']=$this->Admin_model->get_employee($datass,$datas);
				$data['get_details'] = $this->Admin_model->get_details($datas);
				$data['last_spare'] = $this->Admin_model->last_spare($datas);
				$data['reg_tech'] = $this->Admin_model->reg_tech($datas);
				$data['tech'] = $this->Admin_model->count_tech($datas);		
				$data['service'] = $this->Admin_model->count_service($datas);		
				$data['call'] = $this->Admin_model->count_call($datas);
				$data['manager'] = $this->Admin_model->count_manager($datas);
				$data['user']=$data_role;
				$this->load->view('admin',$data);	
			}
			else if($data_role['user_type']=='Service_Manager'){	
				/* $datas= $this->session->userdata('companyname');			 
			$datas= $this->session->userdata('companyid'); */			
			//$result=$this->input->cookie('user',true);
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('reimbursement');
			$datass=$this->session->userdata('session_username');
			$datas= $this->session->userdata('companyid');
			 $user=$data_role;
					//foreach($user as $dt)
				//	{
						$region=$user['region'];
						$area=$user['area'];
						$location=$user['location'];
				//	}			
			$data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
			$data['record']     = $this->reimbursement->get_count($datas,$region,$area);
			$data['tickets']    = $this->reimbursement->total_tickets($datas,$region,$area);
			$data['escallated'] = $this->reimbursement->escallated($datas,$region,$area);
			$data['completed']  = $this->reimbursement->completed($datas,$region,$area);
				$data['user']=$data_role;
			$this->load->view('manager', $data);
			}
			else if($data_role['user_type']=='Call-coordinator'){	
			$this->load->helper('url');
			$this->load->database();
			$datass=$this->session->userdata('session_username');
			$datas= $this->session->userdata('companyid');
			$user=$data_role;
					//foreach($user as $dt)
				//	{
						$region=$user['region'];
						$area=$user['area'];
						$location=$user['location'];
				//	}			
			$data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
				$data['user']=$data_role;
			$this->load->view('raise_ticket',$data); 
			}
			else if($data_role['user_type']=='Service_Desk'){	
			$this->load->helper('url');
			$this->load->database();
			$datass=$this->session->userdata('session_username');
			$datas= $this->session->userdata('companyid');
						$user=$data_role;
					//foreach($user as $dt)
				//	{
						$region=$user['region'];
						$area=$user['area'];
						$location=$user['location'];
				//	}			
			$data['pass']=$this->Admin_model->get_employee($datass,$datas,$region,$area);
				$data['user']=$data_role;
			$this->load->view('service_desk',$data);
			}
			}
		 }
		
		// Validate and store registration data in database
		public function new_user_registration() {
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('Admin_model');
			$data = array(
					'user_type' => $this->input->post('role'),
					'username' => $this->input->post('email'),
					'password' => $this->encrypt->encode($this->input->post('password')),
					'companyid' => $this->input->post('c_id'),
					'companyname'=>$this->input->post('c_name')				
				);
			$valueing=$this->Admin_model->login_user($data);
			echo $valueing;
		}

            public function change_password(){
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('Login_model');
			$where = array(
					'user_type' => $this->input->post('role'),
					'username' => $this->input->post('email'),
					'companyid' => $this->input->post('c_id'),			
				);
			$pwd = array(			
					'password' => $this->encrypt->encode($this->input->post('password'))
			);

			$valueing=$this->Login_model->change_password($where,$pwd);
			echo $valueing;
		}
  

		public function Forgot_Password()
        {   $this->load->helper('url');
            $this->load->database(); 
            $this->load->model('Login_model');
            //$json1 = array();
			  $email = $this->input->post('femail');
			      
              $findemail = $this->Login_model->ForgotPassword($email);  
              if($findemail){
			    $this->load->library('encrypt');
			  $email = $findemail['username'];
			  $query1=$this->db->query("SELECT * from login where username = '".$email."' ");
			  $row=$query1->result_array();
			  if ($query1->num_rows()>0)
			
	  {
			  $passwordplain = "";
			  $passwordplain  = rand(999999999,9999999999);
			  $newpass['password'] = $this->encrypt->encode($passwordplain);
			  $this->db->where('username', $email);
			  $this->db->update('login', $newpass); 
			  $mail_message='Dear '.$row[0]['user_type'].','. "\r\n";
			  $mail_message.='Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$passwordplain.'</b>'."\r\n";
			  $mail_message.='<br>Please Update your password.';
			  $mail_message.='<br>Thanks & Regards';
			  $mail_message.='<br>FieldPro Team';        
			  $config   = Array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'ssl://smtp.googlemail.com',
				  'smtp_port' => 465,
				  'smtp_user' => 'kaspondevelopers@gmail.com',
				  'smtp_pass' => 'Kaspon@123',
				  'mailtype' => 'html',
				  'charset' => 'iso-8859-1'
			  );
			  $this->load->library('email', $config);
			  $this->email->set_newline("\r\n");
			  $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
			  $this->email->to($email);
			  $this->email->subject('FieldPro Forgot Password');
			  $this->email->message($mail_message);
			  $this->email->set_newline("\r\n");
			  //$this->email->send();
	  if ($this->email->send()) {
		  
		echo "success";
		  
	  } else {
	 
		 
		  echo "Failed to send password, please try again";
	  }
	      
	  }
	  else
	  {   
		echo  "Email not found"; 
	  }      
                }else{
                   echo "No email found";
           }
          
        }

		public function admin_myprofile(){
			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  			
			//print_r($data_role);
			//return false;
			$data['userrole']=$data_role['user_type'];
			$this->load->view('admin_myprofile',$data_role);
		}
		public function service_myprofile(){
			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  			
			//print_r($data_role);
			//return false;
			$data['userrole']=$data_role['user_type'];
			$this->load->view('service_myprofile',$data_role);
		}
		public function manager_myprofile(){
			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  			
			//print_r($data_role);
			//return false;
			$data['userrole']=$data_role['user_type'];
			$this->load->view('manager_myprofile',$data_role);
		}
		public function call_myprofile(){
			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');			 
			 $username= $this->session->userdata('username');
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  			
			//print_r($data_role);
			//return false;
			$data['userrole']=$data_role['user_type'];
			$data['employee_id']=$data_role['employee_id'];
			$data['first_name']=$data_role['first_name'];
			$data['last_name']=$data_role['last_name'];
			$data['contact_number']=$data_role['contact_number'];
			$data['alternate_number']=$data_role['alternate_number'];

			$this->load->view('call_myprofile',$data);
		}

		public function submit_profile(){
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('Login_model');
				  $comp=$this->input->post('c_id');
                  $emp_id =$this->input->post('profile_id');
                  $firstname= $this->input->post('fname');
                  $lastname= $this->input->post('lname');
                  $contact=  $this->input->post('c_number');
                  $alt_contact=  $this->input->post('alt_num');
                  $email_id= $this->input->post('profile_email');
                  $oldemail_id= $this->input->post('old_usremail');
                  $user_role= $this->input->post('user_role');
                 // echo $comp;
          		//echo "ramya ".$oldemail_id;          		

            if(!empty($comp) && !empty($email_id) && !empty($emp_id) && !empty($firstname) && !empty($lastname) && !empty($contact))
          	{

          		$result=$this->Login_model->submit_profile($emp_id,$comp,$firstname,$lastname,$contact,$alt_contact,$email_id,$oldemail_id,$user_role);
          		echo $result;
          	}
          	else{
          		echo "Fill Mandatory fields!";
          	}
		}

		public function myprofile()
		{
			$datass=$this->session->userdata('session_username');
			 $datas= $this->session->userdata('companyid');				 
			
			$data_role = $this->Admin_model->get_details_user($datass,$datas);  
			//print_r($data_role);
			//return false;
			$data['userrole']=$data_role['user_type'];

			if($data['userrole']=="Admin"){
				$this->load->view('admin_myprofile',$data);
			}
			if($data['userrole']=="Service_Desk"){
				$this->load->view('service_myprofile',$data);
			}
			if($data['userrole']=="Service_Manager"){
				$this->load->view('manager_myprofile',$data);
			}
			if($data['userrole']=="Call-coordinator"){
				$this->load->view('call_myprofile',$data);
			}
			else{
				$this->load->view('myprofile',$data);
			}		

		}
	}

?>