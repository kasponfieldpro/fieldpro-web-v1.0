<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Controller_cronfunctions extends CI_Controller
{
    public function __construct()
	{
        parent:: __construct();
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->helper(array('url'));
        $this->load->database();
        $this->load->model('Super_admin');	
        $this->load->model('Admin_model');
        $this->load->model('Cronfunctions_model');
    }
     public function automatic_block()
     {
        $data = $this->Cronfunctions_model->automatic_block();
        return $data;
     }

     public function remainder_mail(){
        $data = $this->Cronfunctions_model->remainder_mail();
        return $data;
        exit();
        if($data!="fine"){
            return $data;
        }
     }
}
