<?php
ini_set('max_execution_time',0);
defined('BASEPATH') OR exit('No direct script access allowed');
class controller_call extends CI_Controller {
   public  function __construct()
   {
    parent:: __construct();       
    $this->load->library('encrypt');
    $this->load->library('email');
    $this->load->helper(array('url','cookie'));     
    $this->load->database();
    $this->load->library('session');
    $this->load->model('Admin_model'); 
    $this->load->model('New_amc'); 
   }
   public function index()
   {
      if($this->session->userdata('user_logged_in'))
      {  
         $this->load->helper('url');
         $this->load->database();
         $datass=$this->session->userdata('session_username');
         $datas= $this->session->userdata('companyid');
         $data_role = $this->Admin_model->get_details_user($datass,$datas); 
         $data['user']=$data_role;
         $data['pass']=$this->Admin_model->get_employee($datass,$datas);
         $this->load->view('raise_ticket',$data);
      }
      else
      {
         redirect('login/login');
      }
   }
   
   public function amc_ticket()
   {
      if($this->session->userdata('user_logged_in'))
      {  
         /* $datas= $this->session->userdata('companyname');          
         $datas= $this->session->userdata('companyid'); */        
         //$result=$this->input->cookie('user',true);
         $this->load->helper('url');
         $this->load->database();
         $datass=$this->session->userdata('session_username');
         $datas= $this->session->userdata('companyid');
         $data_role = $this->Admin_model->get_details_user($datass,$datas); 
         $data['user']=$data_role;
         $data['pass']=$this->Admin_model->get_employee($datass,$datas);
         $this->load->view('amc_ticket',$data);
      }
      else
      {
         redirect('login/login');
      }
   }
   public function user_profile()
   {
      $this->load->view('sample');
   }
   public function cust_search()
   {
      $this->load->helper('url');
      $this->load->database();            
      $this->load->model('New_amc');
      $number=$this->input->post('contact_num');
      $result=$this->New_amc->cust_search($number);
      echo $result;
   }
   public function fet_details()
   {
      $this->load->helper('url');
                $this->load->database();
      $number=$this->input->post('company_id');
      $this->load->model('New_amc');
                $result= $this->New_amc->get_birds($number);
           return $result;
   }
    public function expiry_date()
     {
        $this->load->helper('url');
        $this->load->database();
          $dop =$this->input->post('dop');
          $cont =$this->input->post('cont');
          if($cont=="Part_only")
              {
                $time = strtotime($dop);
                $CurrentDate = date('Y-m-d',$time);
           $date = new DateTime($CurrentDate);
                $date->modify('6 month');
                echo $date->format('Y-m-d');
             }
         else if($cont=="Labour" || $cont=="Warranty")
              {
                $time = strtotime($dop);
                $CurrentDate = date('Y-m-d',$time);
           $date = new DateTime($CurrentDate);
                $date->modify('+1 year');
                echo $date->format('Y-m-d');
             }
        else
           {
              $time = strtotime($dop);
                $CurrentDate = date('Y-m-d',$time);
           $date = new DateTime($CurrentDate);
                $date->modify('10 month');
                echo $date->format('Y-m-d');
           }
     }
   
    public function raise_newticket()
   {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('New_amc');
        $company_id=  $this->input->post('companyid');
        $tick_id=  $this->input->post('tick_id');
        $cust_id=  $this->input->post('cust_id');
        $cust_name= $this->input->post('name');
        $mail= $this->input->post('e_mail');
        $email = $this->input->post('mail');
        $contact =$this->input->post('contact'); 
        $alt_contact =$this->input->post('alt_contact'); 
        $door= $this->input->post('doornum');
        $addr= $this->input->post('address');
        $c_town= $this->input->post('cus_town');
        $lmrk= $this->input->post('land_mrk');
        $loc= $this->input->post('addr');
        $state= $this->input->post('cstate');
        $country= $this->input->post('ccountry');
        $pincode=$this->input->post('postcode');
        $prod=$this->input->post('cust_prod');
        $cat=$this->input->post('cust_cat');
        $cont=$this->input->post('contract_type');
        $model=$this->input->post('model_no');
        $serial=$this->input->post('s_no');
        $pref_date=$this->input->post('preferred_date');
        $call_tag=$this->input->post('call_tag');
        $prob_desc=$this->input->post('prob_desc');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
		$worktype=$this->input->post('work_type');
        $upload_contents = $_FILES["ticket_image"];
        $upload_content = $_FILES["ticket_image"]["name"]; 
        $upload_content= str_replace(" ","_",$upload_content);
     
         
      $raised_time= new DateTime();
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->format('Y-m-d H:i:s');
      
      $creation = date('Y-m-d H:i:s',strtotime($pref_date));
      $date = new DateTime(str_replace("-","",$pref_date));
      $pr_date = $date->format('Y-m-d H:i:s');

      $new_addr= $addr.','.$lmrk.','.$c_town.','.$loc; 
      $latitude=0;
      $longitude=0;
      $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
      $geo = json_decode($geo, true); 
//print_r($geo);exit;
         if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         }
         else{
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
            $geo = json_decode($geo, true); 
            if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         } 
         else if($geo['status'] == 'ZERO_RESULTS'){
            $msg = "Non-existent address, please try again!";
            exit();   

         }
         else if($geo['status'] == 'OVER_DAILY_LIMIT'){
            $msg = "Google API error,  you are over your quota!";
            exit();   

         }
         else if($geo['status'] == 'REQUEST_DENIED'){
            $msg = "Google API error,  your request was denied!";
            exit();   

         }
         else if($geo['status'] == 'INVALID_REQUEST'){
            $msg = "Google API error,  address, components or latlng missing!";
            exit();   

         }
         else if($geo['status'] == 'UNKNOWN_ERROR'){
            $msg = "Google API error,  please try again!";
            exit();   

         }

           
         }
        
           
          $pref_date=$pref_date.':00';
          $config = array (
            'upload_path'  => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content,
         ); 
         $output_dir = "upload/"; 
        
         $url =  $output_dir . $upload_content;       
               $cdata = array('ticket_id'=>$tick_id,
                        'cust_id'  => $cust_id,
                        'contact_no' => $contact,
                        'alternate_no' => $alt_contact,
                        'door_no' => $door,
                        'address'  => $addr,
                        'town'  => $c_town,
                        'location' => $addr,
                        'city' => $loc,
                        'state'=>$state,
                        'latitude' => $latitude,
                        'longitude' =>$longitude,
                        'landmark' =>$lmrk,
                        'country'=>$country,
                        'pincode'=>$pincode,
                        'product_id' => $prod,
                        'cat_id' => $cat,
                        'model'=>$model,
                        'serial_no'=>$serial,
                        'call_type' => $cont,
                        'call_tag' => $call_tag,
                        'company_id' =>$company_id,
                        'raised_time' =>$raised_time,
                        'cust_preference_date' =>$pref_date,
                        'prob_desc' => $prob_desc,
                        'region' => $region,
                        'town' => $area,
						'work_type'=> $worktype, 
                        'image' =>$url);
               $this->load->library('upload', $config);
               if (!$this->upload->do_upload("ticket_image"))
                  {                       
                     $msg = "Some error in uploading image!";                    
                  }
               else
                  {
                     $insert=$this->New_amc->raise_newticket($cdata);
                     if($insert)
                     {
                        $msg = "Ticket has been Raised.";
                          //Email for ticket creation
                          $logo = base_url() .'assets/layouts/layout/img/logos.png';
                          $cust_email=$email;
                          $emailTemplate='<html>
                         <head>
                             <style>
                             table {
                             border-collapse: collapse;
                             width: 70%;
                         }
                         th{
                            
                             text-align: center;
                             border-bottom: 0px solid #ddd;
                             border-top: 0px solid #ddd;
                             border-left: 0px solid #ddd;
                             border-right: 0px solid #ddd;
                         }
                          td {
                             padding: 8px;
                             text-align: left;
                             border-bottom: 0px solid #ddd;
                             border-top: 0px solid #ddd;
                             border-left: 0px solid #ddd;
                             border-right: 0px solid #ddd;
                         }
                             </style>
                             
                         </head>
                         <body>
                             <table  border="0" width="70%" >
                          <tr>
                         <td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$logo.'"></td>
                         </tr>
                         
                         <tr>
                         <td colspan="2">Hi '.$this->input->post('cust_name').'</td>
                         </tr>
                         <tr>
                         <td colspan="2">New ticket is raised, 
                           Kindly find the details below. </td>
                         </tr>
                          
                          <tr>
                            <td width="50%" align="left" >Ticket Id:</td>
                            <td width="50%" align="left" >'.$tick_id.'</td>
                          </tr>
                          <tr>
                            <td width="50%" align="left" >Problem description</td>
                            <td width="50%" align="left" >'.$prob_desc.'</td>
                            
                          </tr>
                          <tr>
                            <td width="50%" align="left" >Contract Type:</td>
                            <td width="50%" align="left" >'.$cont.'</td>
                          </tr>
                          <tr>
                          <td width="50%" align="left" >Image:</td>
                          <td width="50%" align="left" ><img src="'.$url.'" width="250" height="150"></td>
                        </tr>
                         
                         <tr>
                         <td colspan="2">Thanks and Regards,</td>
                         </tr>
                         
                         <tr>
                         <td colspan="2">Field Pro Team</td>
                         </tr>
                         </table>
                         </body>
                         </html>';
                         
                                                                 
                         $this->load->library('email');
                         $config['protocol']='smtp';
                         $config['smtp_host']='ssl://smtp.gmail.com';
                         $config['smtp_port']='465';
                         $config['smtp_timeout']='300';
                         $config['smtp_user']='kaspondevelopers@gmail.com';
                         $config['smtp_pass']='Kaspon@123';
                         $config['charset']='utf-8';
                         $config['newline']="\r\n";
                         $config['wordwrap'] = TRUE;
                         $config['mailtype'] = 'html';
                         $this->email->initialize($config);
                         $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
                         $this->email->to($cust_email);
                         $this->email->subject('New Ticket Raised -' .$tick_id );
                         $this->email->message($emailTemplate);
                         $this->email->send();
                     }
                     else
                     {
                        $msg = "Something went wrong while raising ticket.";
                     }
                  }
         
            
         echo $msg;
   }
   
    public function raise_ondemandticket()
   {
      $this->load->helper('url');
        $this->load->database();
       $this->load->model('New_amc');
        $company_id=  $this->input->post('companyid');
        $tick_id=  $this->input->post('tick_id');
        $cust_id=  $this->input->post('cust_id');
        $cust_name= $this->input->post('name');
      $mail= $this->input->post('e_mail');
        $contact =$this->input->post('contact'); 
        $alt_contact =$this->input->post('alt_contact'); 
        $door= $this->input->post('doornum');
        $addr= $this->input->post('address');
          $c_town= $this->input->post('cus_town');
           $lmrk= $this->input->post('land_mrk');
        $loc= $this->input->post('addr');
        $state= $this->input->post('cstate');
      $country= $this->input->post('ccountry');
        $pincode=$this->input->post('postcode');
        $prod=$this->input->post('cust_prod');
        $cat=$this->input->post('cust_cat');
        $model=$this->input->post('model_no');
        $serial=$this->input->post('s_no');
        $pref_date=$this->input->post('preferred_date');
        $call_tag=$this->input->post('call_tag');
        $prob_desc=$this->input->post('prob_desc');
        $region=$this->input->post('region');
        $area=$this->input->post('area');
      $upload_contents = $_FILES["ticket_image"];
      $upload_content = $_FILES["ticket_image"]["name"]; 
      $upload_content= str_replace(" ","_",$upload_content);
      if($call_tag=="Minor")
      {
         $call_tag="Minor Issue";
      }
      else if($call_tag=="Emergency")
      {
         $call_tag="Emergency Rescue";
      }
      else if($call_tag=="Visual")
      {
         $call_tag="Visual Problem";
      }
        $raised_time= new DateTime();
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
       $raised_time=$raised_time->format('Y-m-d H:i:s');
      
      $creation = date('Y-m-d H:i:s',strtotime($pref_date));
      $date = new DateTime(str_replace("-","",$pref_date));
      $pr_date = $date->format('Y-m-d H:i:s');

      $new_addr= $addr.','.$lmrk.','.$c_town.','.$loc; 
        $latitude=0; 
      $longitude=0;
      
      $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false');
      $geo = json_decode($geo, true);  // Convert the JSON to an array
      $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false');
      $geo = json_decode($geo, true); 
         if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         }
         else{
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false');
            $geo = json_decode($geo, true); 
            if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         } 
         else if($geo['status'] == 'ZERO_RESULTS'){
            $msg = "Non-existent address, please try again!";
            exit();   

         }
         else if($geo['status'] == 'OVER_DAILY_LIMIT'){
            $msg = "Google API error,  you are over your quota!";
            exit();   

         }
         else if($geo['status'] == 'REQUEST_DENIED'){
            $msg = "Google API error,  your request was denied!";
            exit();   

         }
         else if($geo['status'] == 'INVALID_REQUEST'){
            $msg = "Google API error,  address, components or latlng missing!";
            exit();   

         }
         else if($geo['status'] == 'UNKNOWN_ERROR'){
            $msg = "Google API error,  please try again!";
            exit();   

         }

           
         }
          $pref_date=$pref_date.':00';
          $config = array (
            'upload_path'  => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content,
         ); 
         $cont="On-Demand";
         $output_dir = "upload/"; 
         $url= $output_dir.$upload_content;
         
               $cdata = array('ticket_id'=>$tick_id,
                        'cust_id'  => $cust_id,
                        'contact_no' => $contact,
                        'alternate_no' => $alt_contact,
                        'door_no' => $door,
                        'address'  => $addr,
                                'town'  => $c_town,
                        'location' => $addr,
                        'city' => $loc,
                        'state'=>$state,
                        'latitude' => $latitude,
                        'longitude' =>$longitude,
                        'landmark' =>$lmrk,
                        'country'=>$country,
                        'pincode'=>$pincode,
                        'product_id' => $prod,
                        'cat_id' => $cat,
                        'model'=>$model,
                        'serial_no'=>$serial,
                        'call_type' => $cont,
                        'call_tag' => $call_tag,
                        'company_id' =>$company_id,
                        'raised_time' =>$raised_time,
                        'cust_preference_date' =>$pref_date,
                                'prob_desc' => $prob_desc,
                                'region' => $region,
                                'town' => $area,
                        'image' =>$url);
      
      $this->load->library('upload', $config);
               if (!$this->upload->do_upload("ticket_image"))
                  {                       
                     $msg = "Some error in uploading image!";                    
                  }
               else
                  {
                     $insert=$this->New_amc->raise_newticket($cdata);
                     if($insert)
                     {
                        $msg = "Ticket has been Raised.";
                     }
                     else
                     {
                        $msg = "Something went wrong while raising ticket.";
                     }
                  }
         echo $msg;
   }
   
   public function ondemandticket_withoutimg() {
      $this->load->helper('url');
        $this->load->database();
       $this->load->model('New_amc');
        $tick_id=  $this->input->post('modal_ticket');
        $cust_id=  $this->input->post('modal_cid');
        $cust_name= $this->input->post('modal_cname');
      $mail= $this->input->post('modal_cmail');
        $contact =$this->input->post('modal_number'); 
        $alt_contact =$this->input->post('modal_anum'); 
        $door= $this->input->post('modal_door');
        $addr= $this->input->post('modal_address');
          $cus_town= $this->input->post('modal_town');
          $land_mrk= $this->input->post('modal_lmrk');
        $loc= $this->input->post('modal_addr');
        $state= $this->input->post('modal_cstate');
      $country= $this->input->post('modal_ccountry');
        $pincode=$this->input->post('modal_post');
        $prod=$this->input->post('modal_prod');
        $cat=$this->input->post('modal_cat');
        $model=$this->input->post('modal_modelno');
        $serial=$this->input->post('modal_sno');
        $pref_date=$this->input->post('date');
        $call_tag=$this->input->post('ondemand_calltag');
        $prob_desc=$this->input->post('problem_desc');
        $company_id=  $this->input->post('company_id');
        $region=  $this->input->post('region');
        $area=  $this->input->post('area');
       if($call_tag=='Minor'){
          $call_tag='Minor Issue';
       }
       else if($call_tag=='Emergency'){
          $call_tag='Emergency Rescue';
       }
       else if($call_tag=='Visual'){
          $call_tag='Visual Problem';
       }
      
      $raised_time= new DateTime();
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
       $raised_time=$raised_time->format('Y-m-d H:i:s');
      
                $new_add=$addr.','.$cus_town.','.$land_mrk.','.$loc;
       $latitude=0; 
      $longitude=0;
      
      $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false');
      $geo = json_decode($geo, true); 
         if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         }
         else{
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false');
            $geo = json_decode($geo, true); 
            if ($geo['status'] == 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];   
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
         } 
         else if($geo['status'] == 'ZERO_RESULTS'){
            $msg = "Non-existent address, please try again!";
            exit();   

         }
         else if($geo['status'] == 'OVER_DAILY_LIMIT'){
            $msg = "Google API error,  you are over your quota!";
            exit();   

         }
         else if($geo['status'] == 'REQUEST_DENIED'){
            $msg = "Google API error,  your request was denied!";
            exit();   

         }
         else if($geo['status'] == 'INVALID_REQUEST'){
            $msg = "Google API error,  address, components or latlng missing!";
            exit();   

         }
         else if($geo['status'] == 'UNKNOWN_ERROR'){
            $msg = "Google API error,  please try again!";
            exit();   

         }

           
         }
      
        $cont="On-Demand";
       $cdata = array('ticket_id'=>$tick_id,
                        'cust_id'  => $cust_id,
                        'contact_no' => $contact,
                        'alternate_no' => $alt_contact,
                        'door_no' => $door,
                        'address'  => $addr,
                        'location' => $addr,
                        'town' => $cus_town,
                        'city' =>$loc,
                        'state' =>$state,
                        'latitude' => $latitude,
                        'longitude' =>$longitude,
                        'landmark' =>$land_mrk,
                        'country'=>$country,
                        'pincode'=>$pincode,
                        'product_id' => $prod,
                        'cat_id' => $cat,
                        'model'=>$model,
                        'serial_no'=>$serial,
                        'call_type' => $cont,
                        'call_tag' => $call_tag,
                        'company_id' =>$company_id,
                        'raised_time' =>$raised_time,
                         'prob_desc'=> $prob_desc,
                         'region'=> $region,
                         'town'=> $area,
                        'cust_preference_date' =>$pref_date);
      $insert=$this->New_amc->raise_newticket($cdata);
                     if($insert)
                     {
                        $msg = "Ticket has been Raised.";
                     }
                     else
                     {
                        $msg = "Something went wrong while raising ticket.";
                     }
      echo $msg;
    
   }
   
   public function ticket_withoutimg() {
     $this->load->helper('url');
      $this->load->database();
      $this->load->model('New_amc');
      $tick_id=  $this->input->post('tick_id');
      $cust_id=  $this->input->post('cu_id');
      $cust_name= $this->input->post('name');
      $mail= $this->input->post('email');
      $contact =$this->input->post('number'); 
      $alt_contact =$this->input->post('anum'); 
      $door= $this->input->post('doornum');
      $addr= $this->input->post('address');
      $cus_town= $this->input->post('cus_town');
      $land_mrk= $this->input->post('land_mrk');
      $loc= $this->input->post('addr');
      $state= $this->input->post('cstate');
      $country= $this->input->post('ccountry');
      $pincode=$this->input->post('postcode');
      $prod=$this->input->post('cust_prod');
      $cat=$this->input->post('cust_cat');
      $cont=$this->input->post('contract_type');
      $model=$this->input->post('model_no');
      $serial=$this->input->post('s_no');
      $pref_date=$this->input->post('preferred_date');
      $call_tag=$this->input->post('call_tag');
      $prob_desc=$this->input->post('prob_desc');
      $company_id=  $this->input->post('company_id');
      $region=  $this->input->post('region');
      $area=  $this->input->post('area');
      $work_type=$this->input->post('work_type');
    
     $raised_time= new DateTime();
     $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
     $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
     $raised_time=$raised_time->format('Y-m-d H:i:s');
    
     $new_add=$addr.','.$cus_town.','.$land_mrk.','.$loc;
    $latitude=0; 
    $longitude=0;
    
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_add).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
    $geo = json_decode($geo, true);  // Convert the JSON to an array
//print_r($geo);exit;
       if ($geo['status'] == 'OK') {
              $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
              $longitude = $geo['results'][0]['geometry']['location']['lng'];
       }
       else{
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_addr).'&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI');
        $geo = json_decode($geo, true); 
        if ($geo['status'] == 'OK') {
            $latitude = $geo['results'][0]['geometry']['location']['lat'];   
            $longitude = $geo['results'][0]['geometry']['location']['lng'];
       } 
        else if($geo['status'] == 'ZERO_RESULTS'){
        $msg = "Non-existent address, please try again!";
        exit();   

        }
        else if($geo['status'] == 'OVER_DAILY_LIMIT'){
        $msg = "Google API error,  you are over your quota!";
        exit();   

       }
        else if($geo['status'] == 'REQUEST_DENIED'){
        $msg = "Google API error,  your request was denied!";
        exit();   

       }
        else if($geo['status'] == 'INVALID_REQUEST'){
        $msg = "Google API error,  address, components or latlng missing!";
        exit();   

       }
        else if($geo['status'] == 'UNKNOWN_ERROR'){
        $msg = "Google API error,  please try again!";
        exit();   

     }

       
     }
      
     $cdata = array('ticket_id'=>$tick_id,
                      'cust_id'  => $cust_id,
                      'contact_no' => $contact,
                      'alternate_no' => $alt_contact,
                      'door_no' => $door,
                      'address'  => $addr,
                      'location' => $addr,
                      'town' => $cus_town,
                      'city' =>$loc,
                      'state' =>$state,
                      'latitude' => $latitude,
                      'longitude' =>$longitude,
                      'landmark' =>$land_mrk,
                      'country'=>$country,
                      'pincode'=>$pincode,
                      'product_id' => $prod,
                      'cat_id' => $cat,
                      'model'=>$model,
                      'serial_no'=>$serial,
                      'call_type' => $cont,
                      'call_tag' => $call_tag,
                      'company_id' =>$company_id,
                      'raised_time' =>$raised_time,
                      'prob_desc'=> $prob_desc,
                      'region'=> $region,
                      'town'=> $area,
                      'work_type'=> $work_type,
                      'cust_preference_date' =>$pref_date);
                   $insert=$this->New_amc->raise_newticket($cdata);
                   if($insert)
                   {
                      $msg = "Ticket has been Raised.";
                       //Email for ticket creation without image
                       $logo = base_url() .'assets/layouts/layout/img/logos.png';
                       $cust_email=$mail;
                       $emailTemplate='<html>
                      <head>
                          <style>
                          table {
                          border-collapse: collapse;
                          width: 70%;
                      }
                      th{
                         
                          text-align: center;
                          border-bottom: 0px solid #ddd;
                          border-top: 0px solid #ddd;
                          border-left: 0px solid #ddd;
                          border-right: 0px solid #ddd;
                      }
                       td {
                          padding: 8px;
                          text-align: left;
                          border-bottom: 0px solid #ddd;
                          border-top: 0px solid #ddd;
                          border-left: 0px solid #ddd;
                          border-right: 0px solid #ddd;
                      }
                          </style>
                          
                      </head>
                      <body>
                          <table  border="0" width="70%" >
                       <tr>
                      <td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$logo.'"></td>
                      </tr>
                      
                      <tr>
                      <td colspan="2">Hi '.$cust_name.'</td>
                      </tr>
                      <tr>
                      <td colspan="2">New ticket is raised, 
                        Kindly find the details below. </td>
                      </tr>
                       
                       <tr>
                         <td width="50%" align="left" >Ticket Id:</td>
                         <td width="50%" align="left" >'.$tick_id.'</td>
                       </tr>
                       <tr>
                         <td width="50%" align="left" >Problem description</td>
                         <td width="50%" align="left" >'.$prob_desc.'</td>
                         
                       </tr>
                       <tr>
                         <td width="50%" align="left" >Contract Type:</td>
                         <td width="50%" align="left" >'.$cont.'</td>
                       </tr>
                      
                      <tr>
                      <td colspan="2">Thanks and Regards,</td>
                      </tr>
                      
                      <tr>
                      <td colspan="2">Field Pro Team</td>
                      </tr>
                      </table>
                      </body>
                      </html>';
                      
                                                              
                      $this->load->library('email');
                      $config['protocol']='smtp';
                      $config['smtp_host']='ssl://smtp.gmail.com';
                      $config['smtp_port']='465';
                      $config['smtp_timeout']='300';
                      $config['smtp_user']='kaspondevelopers@gmail.com';
                      $config['smtp_pass']='Kaspon@123';
                      $config['charset']='utf-8';
                      $config['newline']="\r\n";
                      $config['wordwrap'] = TRUE;
                      $config['mailtype'] = 'html';
                      $this->email->initialize($config);
                      $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
                      $this->email->to($cust_email);
                      $this->email->subject('New Ticket Raised -' .$tick_id );
                      $this->email->message($emailTemplate);
                      $this->email->send();
                   }
                   else
                   {
                      $msg = "Something went wrong while raising ticket.";
                   }
    echo $msg;
  
 }

   public function renew_existingcontract()
    {
        $this->load->helper('url');

        $this->load->database();
        $this->load->model('New_amc');
        $tick_id=  $this->input->post('tick_id');
        $amc_id=  $this->input->post('amc_id');
            $myStr=$this->input->post('contract_type');
            $result = substr($myStr, 0, 4);
            $gen_amc_id=$result.'_'. $amc_id;
        
        $cust_id=  $this->input->post('c_id');
        $cust_name= $this->input->post('name');
        $contact =$this->input->post('number'); 
        $alt_contact =$this->input->post('anum'); 
        $door= $this->input->post('doornum');
        $addr= $this->input->post('address');
        $town= $this->input->post('cont_town');
        $landmark= $this->input->post('cont_ldmrk');
        $loc= $this->input->post('addr');
        $state= $this->input->post('cstate');
        $country= $this->input->post('ccountry');
        $pincode=$this->input->post('postcode');
        $prod=$this->input->post('cust_prod');
        $cat=$this->input->post('cust_cat');
        $cont=$this->input->post('contract_type');      
        
            if($cont=="Labour"){
                $cont="Labour Support";
            }
            if($cont=="Part_only"){
                $cont="Part_only";
            }
            if($cont=="Warranty"){
                $cont="Warranty Support";
            }
            if($cont=="On-Demand"){
                $cont="On-Demand";
            }
            if($cont=="Comprehensive"){
                $cont="Comprehensive Support";
            }
        $model=$this->input->post('model_no');
        $serial=$this->input->post('s_no');
        $qty=$this->input->post('call_tag');
        $pref_date=$this->input->post('preferred_date');
        $company_id=  $this->input->post('company_id');
        $region=  $this->input->post('region');
        $area=  $this->input->post('area');
        
        $raised_time= new DateTime();
        $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time=$raised_time->format('Y-m-d H:i:s');
        
        $latitude=0; 
        $longitude=0;
        
        if($landmark!=''){
             $new_add=$addr.','.$town.','.$landmark.','.$loc;
        }
        else{
             $new_add=$addr.','.$town.','.$loc;
        }
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($new_add).'&sensor=false');
           //print_r($geo);
            $geo = json_decode($geo, true);  // Convert the JSON to an array
            if ($geo['status'] == 'OK') {
                 $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                 $longitude = $geo['results'][0]['geometry']['location']['lng'];
            }
        
            /*if(($latitude <0 ||  $longitude<0))
            {
                    $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($loc).'&sensor=false');
                    $geo = json_decode($geo, true);  // Convert the JSON to an array
                    if ($geo['status'] == 'OK') {
                       $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                       $longitude = $geo['results'][0]['geometry']['location']['lng'];
                    }
            }*/

            $where_array = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.amc_id!=" => "",
                "all_tickets.product_id" => $prod,
                "all_tickets.cat_id" => $cat,
                "all_tickets.model" => $model,
                "all_tickets.serial_no" => $serial,
                "all_tickets.call_type" => $cont,
            );  
            $check_ticket = $this->New_amc->check_ticket($where_array);
            if($check_ticket=="0"){   
        //$pref_date=$pref_date.':00';
                $cdata = array('ticket_id'=>$tick_id,
                                        'amc_id'  => $gen_amc_id,
                                        'cust_id'  => $cust_id,
                                        'contact_no' => $contact,
                                        'alternate_no' => $alt_contact,
                                        'door_no' => $door,
                                        'address'  => $addr,
                                        'location' => $addr,
                                        'town' => $town,
                                        'city' =>$loc,
                                        'state' =>$state,
                                        'latitude' => $latitude,
                                        'longitude' =>$longitude,
                                        'country'=>$country,
                                        'pincode'=>$pincode,
                                        'product_id' => $prod,
                                        'cat_id' => $cat,
                                        'model'=>$model,
                                        'serial_no'=>$serial,
                                        'call_type' => $cont,
                                        'quantity' => $qty,
                                        'company_id' =>$company_id,
                                        'raised_time' =>$raised_time,
                                        'region' =>$region,
                                        'town' =>$area,
                                        'cust_preference_date' =>$pref_date);               
                
                    $insert=$this->New_amc->raise_newticket($cdata);
                                    if($insert)
                                    {
                                        $msg = "Ticket has been Raised.";

$baseurl = base_url() .'assets/layouts/layout/img/logos.png';
 $customeremail=$this->input->post('cust_mail');
   $emailTemplate='<html>
<head>
    <style>
    table {
    border-collapse: collapse;
    width: 70%;
}
th{
   
    text-align: center;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
 td {
    padding: 8px;
    text-align: left;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
    </style>
    
</head>
<body>
    <table  border="0" width="70%" >
 <tr>
<td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$baseurl.'"></td>
</tr>

<tr>
<td colspan="2">Hi '.$cust_name.'</td>
</tr>
<tr>
<td colspan="2">Your request for renewal of the contract is under process.
  Kindly find the details below. </td>
</tr>
 
                  <tr>
                    <td width="50%" align="left" >Ticket Id:</td>
                    <td width="50%" align="left" >'.$tick_id.'</td>
                  </tr>
                  <tr>
                    <td width="50%" align="left" >AMC contract Id</td>
                    <td width="50%" align="left" >'.$gen_amc_id.'</td>
                    
                  </tr>
                  <tr>
                    <td width="50%" align="left" >Contract Type:</td>
                    <td width="50%" align="left" >'.$cont.'</td>
                  </tr>

<tr>
<td colspan="2">Thanks and Regards,</td>
</tr>

<tr>
<td colspan="2">Field Pro Team</td>
</tr>
</table>
</body>
</html>';

                                        
$this->load->library('email');
$config['protocol']='smtp';
$config['smtp_host']='ssl://smtp.gmail.com';
$config['smtp_port']='465';
$config['smtp_timeout']='300';
$config['smtp_user']='kaspondevelopers@gmail.com';
$config['smtp_pass']='Kaspon@123';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
$this->email->initialize($config);
$this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
$this->email->to($customeremail);
$this->email->subject('Notification Mail');
$this->email->message($emailTemplate);
$this->email->send();
                                    }
                                    else
                                    {
                                        $msg = "Something went wrong while raising ticket.";
                                    }
            }
            else{
                $msg = "Ticket is In-Progress for the same values.";
            }
        echo $msg;
    }
   public function raise_amcticket()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('New_amc');
        $tick_id=  $this->input->post('modal_ticket');
        $amc_id=  $this->input->post('modal_amc');
            $myStr=$this->input->post('modal_contract');
            $result = substr($myStr, 0, 4);
            $gen_amc_id=$result.'_'. $amc_id;
        
        $cust_id=  $this->input->post('modal_cid');
        $cust_name= $this->input->post('modal_cname');
        $mail= $this->input->post('modal_cmail');
        $contact =$this->input->post('modal_number'); 
        $alt_contact =$this->input->post('modal_anum'); 
        $door= $this->input->post('modal_door');
        $addr= $this->input->post('modal_address');
        $loc= $this->input->post('modal_addr');
        $state= $this->input->post('modal_cstate');
        $country= $this->input->post('modal_ccountry');
        $pincode=$this->input->post('modal_post');
        $prod=$this->input->post('modal_prod');
        $cat=$this->input->post('modal_cat');
        $cont=$this->input->post('modal_contract');

            if($cont=="Labour"){
                $cont="Labour Support";
            }
            if($cont=="Part_only"){
                $cont="Part_only";
            }
            if($cont=="Warranty"){
                $cont="Warranty Support";
            }
            if($cont=="On-Demand"){
                $cont="On-Demand";
            }
            if($cont=="Comprehensive"){
                $cont="Comprehensive Support";
            }
        $model=$this->input->post('modal_modelno');
        $serial=$this->input->post('modal_sno');
        $qty=$this->input->post('modal_quantity');
        $pref_date=$this->input->post('date');
        $company_id=  $this->input->post('company_id');
        $region=  $this->input->post('region');
        $area=  $this->input->post('area');
        
        $raised_time= new DateTime();
        $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
        $raised_time=$raised_time->format('Y-m-d H:i:s');
        
        $latitude=0; 
        $longitude=0;
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addr).'&sensor=false');
        $geo = json_decode($geo, true);  // Convert the JSON to an array
            if ($geo['status'] == 'OK') {
                     $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                     $longitude = $geo['results'][0]['geometry']['location']['lng'];
            }
            if(($latitude <0 ||  $longitude<0))
            {
                    $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($loc).'&sensor=false');
                    $geo = json_decode($geo, true);  // Convert the JSON to an array
                    if ($geo['status'] == 'OK') {
                       $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                       $longitude = $geo['results'][0]['geometry']['location']['lng'];
                    }
            }
        
            $where_array = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.amc_id!=" => "",
                "all_tickets.product_id" => $prod,
                "all_tickets.cat_id" => $cat,
                "all_tickets.model" => $model,
                "all_tickets.serial_no" => $serial,
                "all_tickets.call_type" => $cont,
            );          
            $check_ticket = $this->New_amc->check_ticket($where_array);
            if($check_ticket=="0"){
                $cdata = array('ticket_id'=>$tick_id,
                                'amc_id' =>$gen_amc_id,
                                'cust_id'  => $cust_id,
                                'contact_no' => $contact,
                                'alternate_no' => $alt_contact,
                                'door_no' => $door,
                                'address'  => $addr,
                                'location' => $addr,
                                'town' => $state,
                                'latitude' => $latitude,
                                'longitude' =>$longitude,
                                'country'=>$country,
                                'pincode'=>$pincode,
                                'product_id' => $prod,
                                'cat_id' => $cat,
                                'model'=>$model,
                                'serial_no'=>$serial,
                                'call_type' => $cont,
                                'quantity' => $qty,
                                'company_id' =>$company_id,
                                'raised_time' =>$raised_time,
                                'region' =>$region,
                                'town' =>$area,
                                'cust_preference_date' =>$pref_date);
                $insert=$this->New_amc->raise_newticket($cdata);
                    if($insert)
                    {
                        $msg = "Ticket has been Raised.";



                         $baseurl = base_url() .'assets/layouts/layout/img/logos.png';
 $customeremail=$this->input->post('modal_cmail');
   $emailTemplate='<html>
<head>
    <style>
    table {
    border-collapse: collapse;
    width: 70%;
}
th{
   
    text-align: center;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
 td {
    padding: 8px;
    text-align: left;
    border-bottom: 0px solid #ddd;
    border-top: 0px solid #ddd;
    border-left: 0px solid #ddd;
    border-right: 0px solid #ddd;
}
    </style>
    
</head>
<body>
    <table  border="0" width="70%" >
 <tr>
<td colspan="2" width="100%" style="background-color: #d8fdee;text-align: center;"><img src="'.$baseurl.'"></td>
</tr>

<tr>
<td colspan="2">Hi '.$cust_name.'</td>
</tr>
<tr>
<td colspan="2">Your request for a new contract is under process.
  Kindly find the details below. </td>
</tr>
 
                  <tr>
                    <td width="50%" align="left" >Ticket Id:</td>
                    <td width="50%" align="left" >'.$tick_id.'</td>
                  </tr>
                  <tr>
                    <td width="50%" align="left" >AMC contract Id</td>
                    <td width="50%" align="left" >'.$gen_amc_id.'</td>
                    
                  </tr>
                  <tr>
                    <td width="50%" align="left" >Contract Type:</td>
                    <td width="50%" align="left" >'.$cont.'</td>
                  </tr>

<tr>
<td colspan="2">Thanks and Regards,</td>
</tr>

<tr>
<td colspan="2">Field Pro Team</td>
</tr>
</table>
</body>
</html>';

                                        
$this->load->library('email');
$config['protocol']='smtp';
$config['smtp_host']='ssl://smtp.gmail.com';
$config['smtp_port']='465';
$config['smtp_timeout']='300';
$config['smtp_user']='kaspondevelopers@gmail.com';
$config['smtp_pass']='Kaspon@123';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
$this->email->initialize($config);
$this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
$this->email->to($customeremail);
$this->email->subject('Notification Mail');
$this->email->message($emailTemplate);
$this->email->send();
                    }
                    else
                    {
                        $msg = "Something went wrong while raising ticket.";
                    }
            }
            else{
                $msg = "Ticket is In-Progress for the same values.";
            }
        //$pref_date=$pref_date.':00';
        
        echo $msg;
    
    }
      public function add_customerdetails() {
        $msg;
        $date = date('Y-m-d');
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('New_amc');
        $custid = $this->input->post('custid');
        $cusname = $this->input->post('cusname');
        $email = $this->input->post('email');
        $con_number = $this->input->post('con_number');
        $alter_num = $this->input->post('alter_num');
        $plot_no = $this->input->post('plot_no');
        $street_name = $this->input->post('street_name');
        $town = $this->input->post('c_town');
        $landmark = $this->input->post('c_landmark');
        $city_name = $this->input->post('city_name');
        $state_name = $this->input->post('state_name');
        $country_name = $this->input->post('country_name');
        $pin_num = $this->input->post('pin_num');
        $company_id = $this->input->post('company_id');
        $field = $this->input->post('fields');
        for($i=0;$i<count($field);$i++)
        {   
            $cvalue =$field[$i][3]; 
            if($cvalue=="Labour")
            {
                $cvalue="Labour Support";
            }
            else if($cvalue=="Warranty")
            {
                $cvalue="Warranty Support";
            }
            else if($cvalue=="Comprehensive")
            {
                $cvalue="Comprehensive Support";
            }
            
            if(($field[$i][0]=='dummydata' || $field[$i][1]=='nocat' || $field[$i][3]=='contract') || ($field[$i][0]=='dummydata' && $field[$i][1]=='nocat' && $field[$i][3]=='contract'))
            {
                $msg[]="Product, Sub-cateogry, Contract information are Mandatory.";
            }
            else if($field[$i][2]==$date)
            {
                $msg[]= "Select date of purchase for product!";
            }
            else if($field[$i][5]=='' || $field[$i][6]==''){
                $msg[]="Model number or seriel number empty";
            }
            else{
              $contractdetailsarray=array();
              $cvalue=trim($cvalue);
            $contractdetailsarray=$this->New_amc->getcontractdetails($company_id,$cvalue);
            $contractvalue='';
            $contract_id='';
           // print_r()
         foreach($contractdetailsarray as $value)
          {
            $contractvalue=$value['contract_amount'];
          }
         // echo "value".$contractvalue;

            $idata= array(
                'customer_id'=>$custid ,
                'customer_name'=>$cusname,
                'email_id'=>$email,
                'contact_number'=>$con_number,
                'alternate_number'=>$alter_num,
                'door_num'=>$plot_no,
                'address'=>$street_name,
                'cust_town'=>$town,
                'landmark'=>$landmark,
                'city'=>$city_name,
                'state'=>$state_name,
                'cust_country'=>$country_name,
                'pincode'=>$pin_num,
                'company_id'=>$company_id,
                'product_serial_no'=>$field[$i][0],
                'component_serial_no'=>$field[$i][1],
                'start_date'=>date('Y-m-d', strtotime($field[$i][2])),
                'type_of_contract'=>$cvalue,
                'warrenty_expairy_date'=>$field[$i][4],
                'model_no'=>$field[$i][5],
                'serial_no'=>$field[$i][6],
                'contract_value'=>$contractvalue,
                'end_date'=>$field[$i][4]
            );
                //print_r(json_encode($data));
                //$msg[]=$idata;
                //exit;
                $user = $this->New_amc->check_customer($con_number);
                    if($user=='1'){
                        $user_exists = $this->New_amc->check_custmail($email);
                        if($user_exists=='1'){
                            $data=$this->New_amc->add_customerdetails($idata);
                            if($data==1){
                              $updatestatus=$this->New_amc->updatecontractdetails($custid,$company_id);
                                $msg[]="Customer Details for Product ". $field[$i][0] ." are uploaded";
                            }else{
                                $msg[]="Customer Details for Product ".$field[$i][0]." are Not uploaded";
                            }
                        }else{
                            $msg[]="Email Id already exists for different user!";
                        }
                    }else{
                        $msg[]="Contact Number already exists for different user!!";
                    }   
            }
        }
        print_r(json_encode($msg));
    }
    public function add_productdetails() {
      $msg;
      $this->load->helper('url');
        $this->load->database();
       $this->load->model('New_amc');
        $custid = $this->input->post('custid');
        $cusname = $this->input->post('cusname');
        $email = $this->input->post('remail');
        $con_number = $this->input->post('con_number');
        $alter_num = $this->input->post('alter_num');
        $plot_no = $this->input->post('plot_no');
        $street_name = $this->input->post('street_name');
        $town = $this->input->post('c_town');
        $landmark = $this->input->post('c_landmark');
        $city_name = $this->input->post('city_name');
        $state_name = $this->input->post('state_name');
        $country_name = $this->input->post('country_name');
        $pin_num = $this->input->post('pin_num');
        $company_id = $this->input->post('company_id');
        $field = $this->input->post('fields');
        for($i=0;$i<count($field);$i++)
      {  
         $cvalue =$field[$i][3]; 
         if($cvalue=="Labour")
         {
            $cvalue="Labour Support";
         }
         else if($cvalue=="Warranty")
         {
            $cvalue="Warranty Support";
         }
         else if($cvalue=="Comprehensive")
         {
            $cvalue="Comprehensive Support";
         }
         else if($cvalue=="Part_only")
         {
            $cvalue="Part_only";
         }
         else if($cvalue=="On-Demand")
         {
            $cvalue="On-Demand";
         }
         else if($cvalue=="empty")
         {
            $cvalue="";
         }
               
         $data= array(
            'customer_id'=>$custid ,
            'customer_name'=>$cusname,
            'email_id'=>$email,
            'contact_number'=>$con_number,
            'alternate_number'=>$alter_num,
            'door_num'=>$plot_no,
            'address'=>$street_name,
            'cust_town'=>$town,
            'landmark'=>$landmark,
            'city'=>$city_name,
            'state'=>$state_name,
            'cust_country'=>$country_name,
            'pincode'=>$pin_num,
            'company_id'=>$company_id,
            'product_serial_no'=>$field[$i][0],
            'component_serial_no'=>$field[$i][1],
            'start_date'=>$field[$i][4],
            //'start_date'=>date('Y-m-d', strtotime($field[$i][4])),
            'type_of_contract'=>$cvalue,
            'warrenty_expairy_date'=>$field[$i][5],
            'model_no'=>$field[$i][6],
            'serial_no'=>$field[$i][7]
         );
         //print_r($data);
         //exit;
         $data=$this->New_amc->add_customerdetails($data);
         if($data==1){
            $msg[]="Customer Details for Product ". $field[$i][0] ." are Registered";
         }else{
            $msg[]="Customer Details for Product ".$field[$i][0]." are Not Registered";
         }
      }
      print_r(json_encode($msg));
   }
   
   /*public function fetch()
   { 
     $this->load->helper('url');
     $this->load->helper('array');      
     $this->load->database();
     $cid= $this->session->userdata('companyid');
     $this->load->library('form_validation');
     
     $contact_number=$this->input->post('contact_number');
     //$this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]|min_length[10]|max_length[15]');
    $this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
     if ($this->form_validation->run() == FALSE)
     {
        $result1='';
        $result1="Provide Proper Mobile Number";

     }
               else
           {
          $this->load->model('New_amc');
     $result1=$this->New_amc->retrieve($contact_number,$cid);
     //print_r(json_encode($result1));
     }
      
  echo json_encode($result1);
}*/
public function fetch()
{
    $this->load->helper('url');
    $this->load->database();
    $cid= $this->session->userdata('companyid');
        $this->load->library('form_validation');
   $contact_number=$this->input->post('contact_number');
    //$this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]|min_length[10]|max_length[15]');
  // $contact_number="9042643200";
    $this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
    if ($this->form_validation->run() == FALSE)
    {
       // $result1='';
        $result1="Provide Proper Mobile Number";
    }
     else
        {
    $this->load->model('New_amc');
    $result1=$this->New_amc->retrieve($contact_number,$cid);
    
    }
   echo json_encode($result1);

}
    public function customer_fetch()
    {
      $this->load->helper('url');
$this->load->helper('array');
        $this->load->database();
        $this->load->library('form_validation');
       $contact_number=$this->input->post('contact_number');
       $company_id=$this->input->post('company_id');
      //$this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]|min_length[10]|max_length[10]');
      $this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){7,10}$^]');
      if ($this->form_validation->run() == FALSE)
      {
         $result1='';
         $result1="Provide Proper Mobile Number";
      }
        else
        {
           $this->load->model('New_amc');
         $result1=$this->New_amc->customer_retrieve($contact_number,$company_id);
      }
       echo json_encode($result1);
   
   }
   
 public function fetch_r()
   {
      $this->load->helper('url');
        $this->load->database();
      $this->load->library('form_validation');
       $contact_number=$this->input->post('contact_number');
      //$this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]|min_length[10]|max_length[10]');
      $this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
      if ($this->form_validation->run() == FALSE)
      {
         $result1="Provide Proper Mobile Number";
      }
      else{
         $this->load->model('New_amc');
         $result1=$this->New_amc->retrieve_r($contact_number);
         if(!empty($result1))
         {
         
         }
         else{
         $result1="No details!";
         } 
      }
      echo json_encode($result1);
   }
  public function load_amctype()
   {
      $this->load->helper('url');
        $this->load->database();
      $company_id=$this->input->post('company_id');
      $this->load->model('New_amc');
      $dropdown=$this->New_amc->select_amc($company_id);
      echo json_encode($dropdown);
   }
   public function renew_extcontract()
   {
      $this->load->helper('url');
      $this->load->database();
        $this->load->model('New_amc'); 
       $comp= $this->input->post('company_id');
         $prod= $this->input->post('cmodal_product');
         $cat=$this->input->post('cmodal_cate');
         $prod_id= $this->New_amc->product_id($prod,$comp);
         $cat_id= $this->New_amc->cat_id($cat,$prod_id,$comp);
      $amc_id=$this->New_amc->select_amc1();
      $ticket_id=$this->New_amc->tick_id();
        $cust_id=  $this->input->post('cmodal_id');
        $cust_name= $this->input->post('cmodal_name');
        $contact =$this->input->post('cmodal_contact'); 
        $mail= $this->input->post('cmodal_mail');
        $door= $this->input->post('cmodal_door');
        $addr= $this->input->post('cmodal_add');
        $loc= $this->input->post('cmodal_loca');
      $country= $this->input->post('cmodal_country');
        $cont=$this->input->post('cmodal_ctype');
        $qty=$this->input->post('cmodal_qty');
        $period=$this->input->post('cmodal_period');
        $pref_date=$this->input->post('cmodal_date');
        $amc_time= new DateTime();
      $amc_time=$amc_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $amc_time=$amc_time->setTimezone(new DateTimezone('Asia/Kolkata'));
       $amc_time=$amc_time->format('Y-m-d H:i:s');
      
      if( !empty($ticket_id) && !empty($amc_id) && !empty($prod_id) && !empty($cat_id) && !empty($cust_id) && !empty($cust_name) && !empty($contact) && !empty($mail) && !empty($door) && !empty($addr) && !empty($loc) && !empty($country) && !empty($cont) && !empty($period) && !empty($qty) && !empty($pref_date))
         {
            $this->load->library('form_validation');     
               $this->form_validation->set_rules('cmodal_mail', 'Email', 'required|valid_email');          
               $this->form_validation->set_rules('cmodal_contact', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
               if ($this->form_validation->run() == FALSE)
               {
                  echo "mobile-no or mail-id is not proper";
               }
               else {
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addr).'&sensor=false');
                 $geo = json_decode($geo, true);  // Convert the JSON to an array
               if ($geo['status'] == 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                   }
                  if(($latitude <0 ||  $longitude<0))
                  {
                 $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($loc).'&sensor=false');
                $geo = json_decode($geo, true);  // Convert the JSON to an array
               if ($geo['status'] == 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                  }
                  }
                        $my_date1=implode('-', array_reverse(explode('/',$pref_date)));
                  $date_time1=date("Y-m-d H:i:s",strtotime($pref_date));
                  $cdata = array( 'ticket_id'=>$ticket_id,
                              'amc_id'=>$amc_id,
                              'cust_id'  => $cust_id,
                              'product_id' => $prod_id,
                              'cat_id' => $cat_id,
                              'contact_no' => $contact,
                              'door_no' => $door,
                              'address'  => $addr,
                              'town' => $loc,
                                            'latitude' => $latitude,
                                   'longitude' =>$longitude,
                              'country'=>$country,
                              'call_type' => $cont,
                              'contract_period' => $period,
                              'quantity'=>$qty,
                              'company_id' =>$comp,
                                     'raised_time' =>$amc_time,
                              'cust_preference_date' =>$date_time1);
                            $insert1 = $this->New_amc->save_contract($cdata);
                            if($insert1==1){
                              echo "inserted.";
                           }
                    }
         }
         else {
            echo "Fill All Fields.";
         }
   }
   
     public function submit_data()
    {
      $this->load->helper('url');
      $this->load->database();
        $this->load->model('New_amc'); 
      $yr= $this->input->post('contract_years');
      $months=$yr*12;
      $mon= $this->input->post('contract_months');
      $period=$months+$mon;
      $p_date=$this->input->post('serv_date');  
      $amc_service =$this->input->post('visit_date');
         $my_date1=date("Y-m-d H:i",strtotime((string)$amc_service));
      $cust=$this->input->post('cust_id');
      $amc_id=$this->New_amc->select_amc1();
      $ticket_id=$this->New_amc->tick_id();
                $prod=  $this->input->post('cust_prod');
                $cat_id= $this->input->post('prod_cat');
                $model =$this->input->post('model'); 
                $qty= $this->input->post('quantity');
                $contact= $this->input->post('contact_number');
                $door= $this->input->post('door');
                $addr= $this->input->post('cust_addr');
                $loc= $this->input->post('base_location');
                $pin= $this->input->post('pincode');
                $amc=$this->input->post('amc_type');
if( !empty($ticket_id) && !empty($cust) && !empty($prod) && !empty($cat_id) && !empty($model) && !empty($qty) && !empty($period) && !empty($contact) && !empty($door) && !empty($addr) && !empty($loc) && !empty($pin) && !empty($amc) && !empty($p_date) && !empty($my_date1))
      {
                          $data = array(
               'ticket_id' =>$ticket_id,
               'amc_id' => $id, 
               'cust_id' => $cust,
               'product_id' => $prod, 
               'cat_id'  => $cat_id,
               'model' => $model, 
               'quantity' => $qty, 
               'contract_period'=> $period,
               'contact_no'=>$contact,
               'door_no'=>$door,
               'address'=>$addr,
               'location'=>$loc,
               'pincode'=>$pin,
               'call_type' =>$amc,
               'purchase_date' =>$p_date,
               'cust_preference_date'=>$my_date1,
               'company_id'=>$this->input->post('company'));
                
           $insert= $this->New_amc->savedata($data);
      if($insert==1)
      {
            echo "Successfully done!!";
      }
      else
      {
         echo 'empty';
      }
          }
      else {
               echo "All Fields are Mandatory!";
           }
   }
  public function contract_new()
   {
      $this->load->helper('url');
      $this->load->database();
        $this->load->model('New_amc'); 
      $cust=$this->input->post('c_id');
      $amc_id=$this->New_amc->select_amc1();
      $ticket= $this->input->post('tick_id');
        $contact= $this->input->post('contact_num');
       $a_num =$this->input->post('a_num'); 
        $door= $this->input->post('dr');
                $addr= $this->input->post('c_addr');
                $loc= $this->input->post('c_loc');
                $country= $this->input->post('c_country');
                $amc=$this->input->post('contract');
                $prod=  $this->input->post('c_prod');
                $cat_id= $this->input->post('pr_cat');
      $qty= $this->input->post('qty');
                $period= $this->input->post('period');
           $date= $this->input->post('date');
      $company= $this->input->post('company_id');
                $raised_time = new DateTime();
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
       $raised_time=$raised_time->format('Y-m-d H:i:s');

         if( !empty($amc_id) && !empty($cust) && !empty($ticket) && !empty($contact) && !empty($a_num) && !empty($door) && !empty($addr) && !empty($loc) && !empty($country) && !empty($amc) && !empty($prod) && !empty($cat_id) && !empty($qty) && !empty($period) && !empty($date))
      {
         $this->load->library('form_validation');     
               $this->form_validation->set_rules('c_mail', 'Email', 'required|valid_email');          
               $this->form_validation->set_rules('contact_num', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
               if ($this->form_validation->run() == FALSE)
               {
                  echo "Mobile-no and/or Mail-id is not proper";
               }
               else
               { 
               $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addr).'&sensor=false');
                $geo = json_decode($geo, true);  // Convert the JSON to an array
               if ($geo['status'] == 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                   }
                  if(($latitude <0 ||  $longitude<0))
                  {
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($loc).'&sensor=false');
                $geo = json_decode($geo, true);  // Convert the JSON to an array
               if ($geo['status'] == 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];    // Get Lat & Long
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                  }
                  }
                                        $my_date1=implode('-', array_reverse(explode('/',$date)));
               $date_time=date("Y-m-d H:i:s",strtotime($date));
                    $cont_data = array(
                  'ticket_id' =>$ticket,
                  'amc_id' => $amc_id, 
                  'cust_id' => $cust,
                  'contact_no'=>$contact,
                  'alternate_no'=>$a_num,
                  'product_id' => $prod, 
                  'cat_id'  => $cat_id,
                  'quantity' => $qty, 
                  'contract_period'=> $period,
                  'door_no'=>$door,
                  'address'=>$addr,
                  'location'=>$loc,
                                                'latitude' => $latitude,
                  'longitude' =>$longitude,
                  'country'=>$country,
                  'call_type' =>$amc,
                                                'raised_time' =>$raised_time,
                  'cust_preference_date'=>$date_time,
                  'company_id'=>$company);
                  
                   $insert= $this->New_amc->new_contract($cont_data);
                  if($insert==1)
                  {
                        echo "Successfully done!!";
                  }
               }
      }
      
      else 
      {
        echo "Fill all fields.";
      }
               
   }
   
  public function choose_cat()
   {
      $this->load->helper('url');
        $this->load->database();
      $product=$this->input->post('prod');
      $company=$this->input->post('company_id');
      $this->load->model('New_amc');
      $res=$this->New_amc->select_cat($product,$company);
      print_r(json_encode($res));
   }
   public function customer_cat()
   {
      $this->load->helper('url');
        $this->load->database();
      $product=$this->input->post('prod');
      $company=$this->input->post('company_id');
      $this->load->model('New_amc');
      $res=$this->New_amc->select_cat($product,$company);
      print_r(json_encode($res));
   
   }
        
    public function load_amc1()
   {
      $this->load->helper('url');
        $this->load->database();
      $amc=$this->input->post('amc');
      $company_id= $this->input->post('company_id');
      $this->load->model('New_amc');
      $res=$this->New_amc->load_page1($amc,$company_id);
      return $res;
   }
   public function select_loc()
   {
      $this->load->helper('url');
        $this->load->database();
      $company_id= $this->input->post('company_id');
      $this->load->model('New_amc');
      $result=$this->New_amc->load_location($company_id);
      return $result;
   }
   public function filter()
   {
      $this->load->helper('url');
        $this->load->database();
      $loc=$this->input->post('location');
      $company_id= $this->input->post('company_id');
      $this->load->model('New_amc');
      $loc_result=$this->New_amc->filter_content($loc,$company_id);
      return $loc_result;
   }
       public function filter_location()
   {
      $this->load->helper('url');
        $this->load->database();
      $loc=$this->input->post('loc');
      $company_id= $this->input->post('company_id');
      $this->load->model('New_amc');
      $loc_result=$this->New_amc->filter_location($loc,$company_id);
      return $loc_result;
   }
   public function load_page()
   {
      $this->load->helper('url');
        $this->load->database();
      $company_id= $this->input->post('company_id');
      $this->load->model('New_amc');
      $result=$this->New_amc->load_contract($company_id);
      return $result;
   }
   
   public function fileUpload()
           {
      $attachment_file=$_FILES["fileUpload"];
      //print_r($attachment_file);
      $output_dir = "assets/upload/";
      $fileName = $_FILES["fileUpload"]["name"];
      if(empty($fileName))
      {
       //echo $output_dir = "";
      //move_uploaded_file($_FILES["fileUpload"]["tmp_name"],$output_dir.$fileName);
      echo "$fileName";
      }
      else{
         echo $output_dir = "assets/upload/";
      move_uploaded_file($_FILES["fileUpload"]["tmp_name"],$output_dir.$fileName);
      echo $_FILES["fileUpload"]["tmp_name"];
       }
        }
   public function ticket()
   {
      $this->load->helper('url');
        $this->load->database();
      $this->load->model('New_amc');
      $com=$this->input->post('company_id');
      $result=$this->New_amc->tick_id($com);
      echo $result;
   }
   public function select_amc1()
   {
      $this->load->helper('url');
        $this->load->database();
      $com=$this->input->post('company_id');
      $this->load->model('New_amc');
      $result=$this->New_amc->select_amc1($com);
      echo $result;
   }
   public function customer()
   {
      $this->load->helper('url');
        $this->load->database();
      $this->load->model('New_amc');
      $company_id=$this->input->post('company_id');
      $result_id=$this->New_amc->cust_id($company_id);
      echo $result_id;
   }
   public function calculate_period()
   {
      $this->load->helper('url');
        $this->load->database();
      $this->load->model('New_amc');
      $company_id=$this->input->post('company_id');      
      $cont_type=$this->input->post('cont_type');
      if($cont_type="Labour")
      {
         $cont_type="Labour Support";
      }
      else if($cont_type="Warranty")
      {
         $cont_type="Warranty Support";   
      }
      else if($cont_type="Comprehensive")
      {
         $cont_type="Comprehensive Support";
      }
      $contract=$this->New_amc->calculate_period($company_id,$cont_type);
      //print_r(json_encode($contract));
      return $contract;
   }
   public function modal_product()
   {
      $this->load->helper('url');
        $this->load->database();
      $company_id =$this->input->post('company_id');
      $this->load->model('New_amc');
      $result_id=$this->New_amc->modal_product($company_id);
      echo $result_id;
   }
   
   public function tag_input()
   {
      $this->load->helper('url');
      
      $this->load->database();
      $this->load->model('New_amc');
      $result=$this->New_amc->tags_input();
      return $result;
   }
 public function raise_tick()
   {
   $this->load->helper('url');
    $this->load->database();
    $image =$this->input->post('data');
    $cust_id=$this->input->post('cust_id');
    $contact_no= $this->input->post('contact_number');
    $ticket_id=$this->input->post('ticket_id');
    $door_no=  $this->input->post('door');
    $address= $this->input->post('cust_addr');
    $location= $this->input->post('cust_loc'); 
    $country= $this->input->post('cust_country');
    $product_id = $this->input->post('cust_prod');
        $cat_id=  $this->input->post('prod_cat');
        $cal_tag= $this->input->post('cal_tag');
    $call_type= $this->input->post('call_type');
    $model= $this->input->post('model_no');
    $serial_no= $this->input->post('serial_no');
    $prob_desc= $this->input->post('prob_desc');
    $service =$this->input->post('serv_date');
    $time =$this->input->post('serv_time');
          $raised_time = new DateTime();
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
      $raised_time=$raised_time->setTimezone(new DateTimezone('Asia/Kolkata'));
       $raised_time=$raised_time->format('Y-m-d H:i:s');
      $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
               // Convert the JSON to an array
               $geo = json_decode($geo, true);
               if ($geo['status'] == 'OK') {
                 // Get Lat & Long
                   $latitude = $geo['results'][0]['geometry']['location']['lat'];
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                   }
                  if(($latitude <0 ||  $longitude<0))
                  {
            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($location).'&sensor=false');
               // Convert the JSON to an array
               $geo = json_decode($geo, true);
               if ($geo['status'] == 'OK') {
                 // Get Lat & Long
                  $latitude = $geo['results'][0]['geometry']['location']['lat'];
                   $longitude = $geo['results'][0]['geometry']['location']['lng'];
                  }
                  }
   if(!empty($cust_id))
   {
      if(!empty($ticket_id))
         {
            if(!empty($door_no))
               {
                  if(!empty($address))
                  {
                     if(!empty($location))
                     {
                        if(!empty($country))
                        {
                           if(!empty($product_id))
                           {
                              if(!empty($cat_id))
                              {
                                 if(!empty($cal_tag))
                                 {
                                                                                          if(!empty($call_type))
                                   {
                                    if(!empty($prob_desc))
                                    {
                                       if(!empty($service))
                                       {
                                          if(!empty($time))
                                          {
                        $this->load->library('form_validation');     
                        $this->form_validation->set_rules('cust_mail', 'Email', 'required|valid_email');          
                        $this->form_validation->set_rules('contact_number', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
                        $this->form_validation->set_rules('alternate_num', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
                        if ($this->form_validation->run() == FALSE)
                        {
                           echo "Provide Proper Mobile Number or/and Email Id";
                        }
                        else 
                           {
                           $my_date1=implode('-', array_reverse(explode('/',$service)));
                           $tim=implode('',explode(' ',$time ));
                           $time1=date("H:i:s",strtotime($tim));
                              $date_time2= $service.' '.$time1;
                              $date_time1=date("Y-m-d H:i:s",strtotime((string)$date_time2));
                              $rdata = array('cust_id'  => $cust_id,
                                       'ticket_id'=>$ticket_id,
                                       'product_id' => $product_id,
                                       'cat_id' => $cat_id,
                                       'contact_no' => $contact_no,
                                       'call_type' => $call_type, 
                                       'model' => $model,
                                       'serial_no' => $serial_no,
                                       'door_no' => $door_no,
                                       'address'  => $address, 
                                       'latitude' => $latitude,
                                       'longitude' =>$longitude,
                                       'location' => $location, 
                                       'town' => $location,
                                       'prob_desc' => $prob_desc,
                                       'image' => $image,
                                                                                                        'raised_time' => $raised_time,
                                       'company_id' =>$this->input->post('company'),
                                       'cust_preference_date' =>$date_time1);
                            $this->load->model('New_amc');
                            $insert1 = $this->New_amc->raise_data($rdata);
                            if($insert1==1){
                            $company_id=$this->input->post('company');
$data=array();
               $this->load->model('AutoAssign');
               $address = $address;
                $location = $location;
               $product1 = $product_id;
               $category1 = $cat_id;
               $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('cust_category', 'all');
                        $this->db->group_end();
                        $query2  = $this->db->get();
                        $result2 = $query2->result_array();
                        if (!empty($result2)) {
                            $cust_category = 'all';
                        } else {
                            $cust_category = $row['cust_category'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('call_category', 'all');
                        $this->db->group_end();
                        $query3  = $this->db->get();
                        $result3 = $query3->result_array();
                        if (!empty($result3)) {
                            $call_category = 'all';
                        } else {
                            $call_category = $row['call_tag'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('service_category', 'all');
                        $this->db->group_end();
                        $query4  = $this->db->get();
                        $result4 = $query4->result_array();
                        if (!empty($result4)) {
                            $service_category = 'all';
                        } else {
                            $service_category = $row['call_type'];
                        }
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category,
                            'cust_category' => $cust_category,
                            'service_category' => $service_category,
                            'call_category' => $call_category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time,priority_level');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                               $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                $response = date('H:i:s', strtotime($result6[0]['response_time']));
                                $priority = date('H:i:s', strtotime($result6[0]['priority_level']));
                     $this->load->model('AutoAssign');
                  $tech=$this->AutoAssign->update_priority($priority,$ticket_id);
                  
                  
               // Get JSON results from this request
               $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
               // Convert the JSON to an array
               $geo = json_decode($geo, true);
               if ($geo['status'] == 'OK') {
                 // Get Lat & Long
                  $latitude = $geo['results'][0]['geometry']['location']['lat'];
                  $longitude = $geo['results'][0]['geometry']['location']['lng'];
                  if($latitude<=0 || $longitude<=0)
                  {
                     $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($location).'&sensor=false');
                     // Convert the JSON to an array
                     $geo = json_decode($geo, true);
                     if ($geo['status'] == 'OK') {
                       // Get Lat & Long
                        $latitude = $geo['results'][0]['geometry']['location']['lat'];
                        $longitude = $geo['results'][0]['geometry']['location']['lng'];
                     $this->load->model('AutoAssign');
                  $tech=$this->AutoAssign->get_nearbylocation($latitude,$longitude,$product1,$category1);
                  if(!empty($tech))
                  {
                     foreach($tech as $row1)
                     {
                        $this->load->model('AutoAssign');
                        $result=$this->AutoAssign->assign($row['ticket_id'],$row1['technician_id'],$resolution,$response,$priority);
                        if($result==1){
                                             $res1=$this->AutoAssign->update_tech_task($row1['technician_id'],$row1['today_task_count']); 
                        }
                        
                     }
                  }

                  }
                  }
                  else{
                  $this->load->model('AutoAssign');
                  $tech=$this->AutoAssign->get_nearbylocation($latitude,$longitude,$product1,$category1);
                  if(!empty($tech))
                  {
                     foreach($tech as $row1)
                     {
                        $this->load->model('AutoAssign');
                        $result=$this->AutoAssign->assign($row['ticket_id'],$row1['technician_id'],$resolution,$response,$priority);
                        if($result==1){
                                             $res1=$this->AutoAssign->update_tech_task($row1['technician_id'],$row1['today_task_count']); 
                        }
                        
                     }
                  }
                  }
               }
      
}
                  }
                              echo "inserted.";
                           }
                        }                          
                        }
                                          else{
                                             echo "Select Preferred Time of visit";
                                          }
                                       }
                                       else{
                                          echo "Select Preferred Date of visit";
                                       }
                                    }
                                    else{
                                       echo "Fill Problem Description";
                                    }
                                 }
                                 
                                 else{
                                    echo "Support type is mandatory";
                                 }
                              }
                                                                                  else{
                                    echo "Call Category is mandatory";
                                 }
                              }
                              else{
                                 echo "Select Category to Raise Ticket";
                              }
                           }
                           else{
                              echo "Select Product to Raise Ticket";
                           }
                        }
                        else{
                           echo "Country field is Empty";
                        }
                     }
                     else{
                        echo "FIll All Address Fields";
                     }
                  }
                  else{
                     echo "FIll All Address Fields";
                  }
               }
            else{
               echo "FIll All Address Fields";
            }
         }
         else{
            echo "Ticket id mandatory";
         }
   }
   else{
      echo "Customer id mandatory";
   }
   }
   
   public function cust_info()
   {
    $this->load->helper('url');
    $this->load->database();
    $cust_id=$this->input->post('cmodal_id');
    $name=$this->input->post('cmodal_name');
    $mail_id=$this->input->post('cmodal_mail');
    $pur_date=  $this->input->post('purchase_date');
    $number= $this->input->post('cmodal_num');
    $alt_number=$this->input->post('cmodal_altnum');
    $product_id = $this->input->post('cmodal_prod');
     $cat_id=  $this->input->post('cmodal_cat');
    $contract_type= $this->input->post('cont_modal');
    $model= $this->input->post('cont_modal1');
    $door_no= $this->input->post('cmodal_door');
    $address= $this->input->post('cmodal_addr');
    $loc= $this->input->post('cmodal_loc');
    $country= $this->input->post('cmodal_country');
      if(!empty($cust_id))
      {
         if(!empty($name))
         {
            if(!empty($number))
            {
               if(!empty($product_id))
               {
                  if(!empty($cat_id))
                  {
                     if(!empty($contract_type))
                     {
                        if(!empty($door_no))
                        {
                           if(!empty($address))
                           {
                              if(!empty($loc))
                              {
                                 if(!empty($country))
                                 {
                        $this->load->library('form_validation');     
                        $this->form_validation->set_rules('cmodal_mail', 'Email', 'required|valid_email');          
                        $this->form_validation->set_rules('cmodal_num', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
                        $this->form_validation->set_rules('cmodal_altnum', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]');
                        if ($this->form_validation->run() == FALSE)
                        {
                           echo "Provide Proper Mobile Number or/and Email Id";
                        }
                        else 
                        {
                           $idata = array('customer_id' =>$cust_id,
                                      'customer_name' =>$name,
                                     'email_id' =>$mail_id,
                                     'purchase_date' =>$pur_date,
                                     'contact_number' =>$number,
                                     'alternate_number' =>$alt_number,
                                     'type_of_contract' =>$contract_type,
                                     'model_no' =>$model,
                                     'product_serial_no' =>$product_id,
                                     'component_serial_no' =>$cat_id,
                                     'door_num' => $door_no,
                                     'address' =>$address,
                                     'cust_town' =>$loc,
                                     'cust_country' =>$country) ;
                            $this->load->model('New_amc');
                           $insert1 = $this->New_amc->cust_insert($idata);
                            if($insert1==1){
                              echo "Customer data inserted.";
                           }
                        }  
                              }
                                 else {
                                    echo "Fill all address fields.";
                                    }
                              }
                              else {
                                 echo "Fill all address fields.";
                                 }
                           }
                           else {
                              echo "Fill all address fields.";
                              }
                        }
                        else {
                           echo "Fill all address fields.";
                           }
                     }
                     else {
                        echo "Select Contract Type.";
                        }
                  }
                  else {
                     echo "Select Sub-Category.";
                     }
               }
               else {
                  echo "Select Product Category.";
                  }
            }
            else {
               echo "Contact Number is mandatory.";
               }
         }
         else {
            echo "Customer Name is mandatory.";
            }
      }
      else {
         echo "Customer ID is mandatory.";
         }
   }
 public function retrieve_data()
{
    $this->load->helper('url');
    $this->load->database();
    $amc_id=$this->input->post('ref_id');
    $this->load->model('New_amc');
    $result=$this->New_amc->fetch_info($amc_id);
      return $result;
      
} 
public function contracts()
  {
   $this->load->helper('url');
    $this->load->database();
    $company_id=$this->input->post('company_id');
    $this->load->model('New_amc');
    $result=$this->New_amc->amc_contracts($company_id);
      return $result;
  }
}
