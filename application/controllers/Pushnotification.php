<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class Pushnotification extends CI_Controller {
	
	public function index()
	{
		$this->load->helper('url');
        $this->load->database();
	}
	public function android_notification()
	{
        $this->load->helper('url');
        $this->load->database();
		$msg_payload = array (
			'mtitle' => 'Test push notification title',
			'mdesc' => 'Test push notification body',
		);
		$tech_id='Tech_0001';
        $this->load->model('Pushnotify');
		$this->Pushnotify->send_notification($tech_id,$msg_payload);
	}
	public function ios_notification()
	{
        $this->load->helper('url');
        $this->load->database();
		$deviceToken = '4A8DF7CE083F1C01EE9051BEEAD40F783B2D34C4B3AFBAD4CB173F33D830ADBE';
		$msg_payload = array (
			'mtitle' => 'Test push notification title',
			'mdesc' => 'Test push notification body',
		);
        $this->load->model('Pushnotify');
		$result=$this->Pushnotify->iOS($msg_payload, $deviceToken);
	}
	
}