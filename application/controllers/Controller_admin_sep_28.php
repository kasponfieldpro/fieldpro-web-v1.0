<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Controller_admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->library('session');
        //$this->load->library('ffmpeglib');
        $this->load->helper(array(
            'url',
            'cookie'
        ));
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->model('Super_admin');
    }
	public function load_notify(){
        $this->load->helper('url');
        $this->load->database();
		$this->load->model('Admin_model');
		$company_id=$this->input->post('company_id');
		//$company_id='company1';
		$data=$this->Admin_model->load_notify($company_id);
		echo json_encode($data);
	}
    public function get_details_company()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $company_id = $this->input->post('company_id');
        $datas      = $this->Admin_model->get_details_company($company_id);
        echo json_encode($datas);
    }
    public function get_product_company()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $company_id = $this->input->post('company_id');
        $datas      = $this->Super_admin->get_product_company($company_id);
        echo json_encode($datas);
    }
    public function get_subproduct_company()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $prod_id = $this->input->post('prod_id');
        $datas   = $this->Super_admin->get_subproduct_company($prod_id);
        echo json_encode($datas);
    }
    public function index()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass = $this->session->userdata('session_username');
            /* $datas= $this->session->userdata('companyname');
            $datas= $this->session->userdata('companyid'); */
            //$result=$this->input->cookie('user',true);
           $datas= $this->session->userdata('companyid'); 
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $datas               = $this->session->userdata('companyid');
            $data_role           = $this->Admin_model->get_details_user($datass, $datas);
			$region="";
			$area="";
			$location="";
            $data['pass']        = $this->Admin_model->get_employee($datass, $datas);
            $data['get_details'] = $this->Admin_model->get_details($datas);
            $data['last_spare']  = $this->Admin_model->last_spare($datas);
            $data['reg_tech']    = $this->Admin_model->reg_tech($datas);
            $data['tech']        = $this->Admin_model->count_tech($datas);
            $data['service']     = $this->Admin_model->count_service($datas);
            $data['call']        = $this->Admin_model->count_call($datas);
            $data['manager']     = $this->Admin_model->count_manager($datas);
            $this->load->view('admin', $data);
            redirect('login/user_login_page');
        } else {
            redirect('login/login');
        }
    }
    public function get_licence()
    {
        $company_id = $this->input->post('company_id');
        $data       = $this->Admin_model->get_licence($company_id);
        print_r(json_encode($data));
    }
    public function insert_adminlicence()
    {
        $id    = $this->input->post('admin_id');
        $data  = array(
            'service_desk' => $this->input->post('total_service'),
            'technicians' => $this->input->post('total_techni')
        );
        $datas = $this->Admin_model->insert_adminlicence($data, $id);
        print_r(json_encode($datas));
    }
    public function user()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            //$result=$this->input->cookie('user',true);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['record']   = $this->Admin_model->getproduct($datas);
            $data['recor']    = $this->Admin_model->getsubproduct($datas);
            $data['records']  = $this->Admin_model->getuser($datas);
            $data['accepted'] = $this->Admin_model->view_tech($datas);
            $data['servicegroup'] = $this->Admin_model->get_servicegroup($datas);
            $this->load->view('user_add', $data);
        } else {
            redirect('login/login');
        }
    }
    public function getproduct()
    {
        $datas        = $this->session->userdata('companyid');
        $data_company = $this->Admin_model->getcompany_s($datas);
        $nodes        = array();
        foreach ($data_company as $val) {
            array_push($nodes, array(
                'id' => $val['a'],
                'parent' => "#",
                'text' => $val['b'],
                'icon' => $val['company_logo']
            ));
            $company = $val['company_id'];
            $data    = $this->Admin_model->getproduct_s($company);
            foreach ($data as $val_product) {
                array_push($nodes, array(
                    'id' => $val_product['product_id'],
                    'parent' => $val_product['company_id'],
                    'text' => $val_product['product_name'],
                    'icon' => $val_product['product_image']
                ));
                $prod_id_1     = $val_product['product_id'];
                $data_category = $this->Admin_model->getcategory_s($prod_id_1);
                foreach ($data_category as $val_category) {
                    array_push($nodes, array(
                        'id' => $val_category['cat_id'],
                        'parent' => $val_category['prod_id'],
                        'text' => $val_category['cat_name'],
                        'icon' => $val_category['cat_image']
                    ));
                }
            }
        }
        echo json_encode($nodes);
    }
    /* public function gettrainin_g(){
    $data=$this->Admin_model->gettrainin_g();
    $nodes = array();
    foreach($data as $val)
    {
    array_push($nodes,array('id'=>$val['training_id'],'parent'=>"#",'text'=>$val['title']));
    $prod_id_1=$val['training_id'];
    $data_category=$this->Admin_model->gettrainin_content($prod_id_1);
    foreach($data_category as $val_category){
    array_push($nodes,array('id'=>$val_category['id'],'parent'=>$val_category['training_id'],'text'=>$val_category['title']));
    }
    }
    echo json_encode($nodes);
    } */
    public function get_users()
    {
        $data = $this->Admin_model->getuser();
        print_r(json_encode($data));
    }
    public function user2()
    {
        $this->load->helper('url');
        $this->load->database();
        $filter = $this->input->post('filter');
        $this->load->model('Admin_model');
        $data = $this->Admin_model->disply_admin2($filter);
        return $data;
    }
    public function checkservicedesk()
    {
        $role   = $this->input->post('role');
        $c_id   = $this->input->post('c_id');
        $c_name = $this->input->post('c_name');
        $data   = array(
            'company_id' => $this->input->post('c_id'),
            'company_name' => $this->input->post('c_name')
        );
        $datas  = $this->Admin_model->checkservicedesk($data);
        print_r(json_encode($datas));
    }
    public function checktech()
    {
        $role   = $this->input->post('role');
        $c_id   = $this->input->post('c_id');
        $c_name = $this->input->post('c_name');
        $data   = array(
            'company_id' => $this->input->post('c_id'),
            'company_name' => $this->input->post('c_name')
        );
        $datas  = $this->Admin_model->checktech($data);
        print_r(json_encode($datas));
    }
    public function report()
    {
        if ($this->session->userdata('user_logged_in')) {
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('reimbursement');
            $datass             = $this->session->userdata('session_username');
            $datas              = $this->session->userdata('companyid');
            $data['pass']       = $this->Admin_model->get_employee($datass, $datas);
            $data['rec']        = $this->reimbursement->disply_attendance();
            $data['billing']    = $this->reimbursement->disply_billing();
            $data['escallated'] = $this->reimbursement->disply_escallated1();
            $data['completed']  = $this->reimbursement->disply_completed();
            $data['inven']      = $this->reimbursement->disply_inventory();
            $this->load->view('admin_report', $data);
        } else {
            redirect('login/login');
        }
    }
    public function insertuser()
    {
	$msg="";
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $emp_id          = $this->input->post('emp_id');
        $c_id            = $this->input->post('c_id');
        $c_name          = $this->input->post('c_name');
        $first_name      = $this->input->post('first_name');
        $last_name       = $this->input->post('last_name');
        $email_id        = $this->input->post('email_id');
        $contact_no      = $this->input->post('contact_no');
        $acontact_no     = $this->input->post('acontact_no');
        $door_flatno     = $this->input->post('door_plotno');
        $street_locality = $this->input->post('street_locality');
        $town_name       = $this->input->post('town_name');
        $land_mark       = $this->input->post('land_mark');
        $city_name       = $this->input->post('city_name');
        $country_name    = $this->input->post('country_name');
        $state_name      = $this->input->post('state_name');
        $pin_code        = $this->input->post('pin_code');
        $role            = $this->input->post('role');
        $worktype            = $this->input->post('work_type');
       
        $region          = $this->input->post('region');
        $location        = $this->input->post('location');
        $area            = $this->input->post('area');
        $role            = $this->input->post('role');
        
        $field           = $this->input->post('fields');
        $image           = 'technician_img/user_avatar.png';
        $datass          = $this->Admin_model->userid_check();
        $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass          = $datass + 1;
        $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $company_id      = $this->session->userdata('companyid');
        $datass          = $company_id . "_" . "User_" . $datass;
        $data_1          = $this->Admin_model->techid_check();
        $datass_1        = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
        $datass_1        = $datass_1 + 1;
        $datass_1        = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
        $datass_1        = $company_id . "_" . "Tech_" . $datass_1;
        if (!empty($emp_id) && !empty($c_id) && !empty($first_name) && !empty($last_name) && !empty($email_id) && !empty($contact_no) && !empty($state_name) && !empty($role)) {
            $this->form_validation->set_rules('email_id', 'Email', 'required');
            $this->form_validation->set_rules('email_id', 'Email', 'valid_email');
            if ($this->form_validation->run() == False) {
                $msg= "Email should be in correct format";
            } else {
                $empid_check  = $this->Admin_model->empid_check($c_id, $emp_id);
                $email_check  = $this->Admin_model->useremail_check($role, $email_id, $c_name, $c_id);
                $mobile_check = $this->Admin_model->usernumber_check($role, $contact_no, $c_name, $c_id);
                if ($empid_check == 1) {
                    $msg= "Employee id is already placed, Kindly check it";
                } else {
                    if ($email_check == 1) {
                        $msg= "Email is already placed, Kindly check it";
                    } else {
                        if ($mobile_check == 1) {
                            $msg= "Contact number is already placed, Kindly check it";
                        } else {
                            if (empty($acontact_no)) {
                                $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
                                if ($contact_no == $acontact_no) {
                                    $msg= "Contact Number and Alternative contact number should not be match";
                                } else {
                                    if ($role == "Service_Desk") {
                                        $data['records'] = "";

                                        $datas = array(
                                            'employee_id' => $emp_id,
                                            'user_id' => $datass,
                                            'company_id' => $c_id,
                                            'companyname' => $c_name,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'contact_number' => $contact_no,
                                            'alternate_number' => $acontact_no,
                                            'flat_no' => $door_flatno,
                                            'street' => $street_locality,
                                            'city' => $city_name,
                                            'state' => $state_name,
                                            'region' => $region,
                                            'location' => $location,
                                            'area' => $area,
                                            'role' => $role,
                                            'image' => $image,
                                            'town' => $town_name,
                                            'landmark' => $land_mark,
                                            'country' => $country_name,
                                            'pincode' => $pin_code
                                        );
                                        if (empty($region) || empty($area) || empty($location)) {
                                            $msg= "All fields are Mandatory" . "\r\n";
                                        } else {
                                            $data['records'] = $this->Admin_model->insertuser($datas);
                                            $data['records'] = $data['records'];

                                        }
                                        if ($data['records'] == 1) {
                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                            $data                = $service_added_check + 1;
                                            $datass              = array(
                                                'service_added' => $data
                                            );
                                            $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                            $this->sendmail($email_id, $c_id, $c_name, $role);
                                            $msg= "User added Successfully";
                                        } else {
                                            $msg= "User not added, kindly try again";
                                        }
                                       
                                    } 
									elseif ($role == "Technician") {
                                        $data['records'] = "";
                                        for ($i = 0; $i < count($field); $i++) {
                                            $datas = array(
                                                'employee_id' => $emp_id,
                                                'technician_id' => $datass_1,
                                                'company_id' => $c_id,
                                                'companyname' => $c_name,
                                                'first_name' => $first_name,
                                                'last_name' => $last_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'flat_no' => $door_flatno,
                                                'street' => $street_locality,
                                                'city' => $city_name,
                                                'state' => $state_name,
                                                'region' => $region,
                                                'location' => $location,
                                                'area' => $area,
                                                'role' => $role,
                                                'image' => $image,
                                                'town' => $town_name,
                                                'landmark' => $land_mark,
                                                'country' => $country_name,
                                                'pincode' => $pin_code,
                                                'product' => $field[$i][0],
                                                'category' => $field[$i][1],
                                                'skill_level' => $field[$i][2],
                                                'work_type'=> $field[$i][3]
                                            );
                                            if (empty($region) || empty($location) || empty($area) || empty($field[$i][0]) || empty($field[$i][1]) || empty($field[$i][2])) {
                                                $msg= "All fields are Mandatory" . "\r\n";
                                            } else {
                                                $data['records'] = $this->Admin_model->inserttechnician($datas);
                                                $data['records'] = $data['records'];
                                            }
                                        }
                                        if ($data['records'] == 1) {
                                            $service_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                            $service_added_check = (int) implode('', $service_added_check);
                                            $data                = $service_added_check + 1;
                                            $datass              = array(
                                                'tech_added' => $data
                                            );
                                            $service_added_check = $this->Admin_model->updatetechnician_added_check($c_id, $datass);
                                            $this->sendmail($email_id, $c_id, $c_name, $role);
                                            $msg= "User added Successfully";
                                        } else {
                                            $msg= "User not added, kindly try again";
                                        }
                                      
                                    } else {
                                        $data['records'] = "";

                                        $datas = array(
                                            'employee_id' => $emp_id,
                                            'user_id' => $datass,
                                            'company_id' => $c_id,
                                            'companyname' => $c_name,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'contact_number' => $contact_no,
                                            'alternate_number' => $acontact_no,
                                            'flat_no' => $door_flatno,
                                            'street' => $street_locality,
                                            'city' => $city_name,
                                            'state' => $state_name,
                                            'region' => $region,
                                            'location' => $location,
                                            'area' => $area,
                                            'role' => $role,
                                            'image' => $image,
                                            'town' => $town_name,
                                            'landmark' => $land_mark,
                                            'country' => $country_name,
                                            'pincode' => $pin_code
                                        );
                                        if (empty($region) || empty($location) || empty($area)) {
                                            $msg= "All fields are Mandatory" . "\r\n";
                                        } else {
                                            $data['records'] = $this->Admin_model->insertuser($datas);
                                            $data['records'] = $data['records'];
                                        }
                                        $this->sendmail($email_id, $c_id, $c_name, $role);
                                        $msg= "User added Successfully";
                                    }
                                    

                                }
                            } else {
                                $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');
                                $this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]');

                                if ($contact_no == $acontact_no) {
                                    $msg= "Contact Number and Alternative contact number should not be match";
                                } else {
                                    if ($role == "Service_Desk") {
                                        $data['records'] = "";

                                        $datas = array(
                                            'employee_id' => $emp_id,
                                            'user_id' => $datass,
                                            'company_id' => $c_id,
                                            'companyname' => $c_name,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'contact_number' => $contact_no,
                                            'alternate_number' => $acontact_no,
                                            'flat_no' => $door_flatno,
                                            'street' => $street_locality,
                                            'city' => $city_name,
                                            'state' => $state_name,
                                            'region' => $region,
                                            'location' => $location,
                                            'area' => $area,
                                            'role' => $role,
                                            'image' => $image,
                                            'town' => $town_name,
                                            'landmark' => $land_mark,
                                            'country' => $country_name,
                                            'pincode' => $pin_code
                                        );
                                        if (empty($region) || empty($location) || empty($area)) {
                                            $msg= "All fields are Mandatory" . "\r\n";
                                        } else {
                                            $data['records'] = $this->Admin_model->insertuser($datas);
                                            $data['records'] = $data['records'];

                                        }
                                        if ($data['records'] == 1) {
                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                            $data                = $service_added_check + 1;
                                            $datass              = array(
                                                'service_added' => $data
                                            );
                                            $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                            $this->sendmail($email_id, $c_id, $c_name, $role);
                                            $msg= "User added Successfully";
                                        } else {
                                            $msg= "User not added, kindly try again";
                                        }
                                    } 
                                    elseif ($role == "Technician") {
                                        $data['records'] = "";
                                        for ($i = 0; $i < count($field); $i++) {
                                            $datas = array(
                                                'employee_id' => $emp_id,
                                                'technician_id' => $datass_1,
                                                'company_id' => $c_id,
                                                'companyname' => $c_name,
                                                'first_name' => $first_name,
                                                'last_name' => $last_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'flat_no' => $door_flatno,
                                                'street' => $street_locality,
                                                'city' => $city_name,
                                                'state' => $state_name,
                                                'region' => $region,
                                                'location' => $location,
                                                'area' => $area,
                                                'role' => $role,
                                                'image' => $image,
                                                'town' => $town_name,
                                                'landmark' => $land_mark,
                                                'country' => $country_name,
                                                'pincode' => $pin_code,
                                                'product' => $field[$i][0],
                                                'category' => $field[$i][1],
                                                'skill_level' => $field[$i][2]
                                            );
                                            if (empty($field[$i][0]) || empty($field[$i][1]) || empty($field[$i][2]) || empty($region) || empty($location) || empty($area)) {
                                                $msg= "All fields are Mandatory" . "\r\n";
                                            } else {
                                                $data['records'] = $this->Admin_model->inserttechnician($datas);
                                                $data['records'] = $data['records'];
                                            }
                                        }
                                        if ($data['records'] == 1) {
                                            $service_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                            $service_added_check = (int) implode('', $service_added_check);
                                            $data                = $service_added_check + 1;
                                            $datass              = array(
                                                'tech_added' => $data
                                            );
                                            $service_added_check = $this->Admin_model->updatetechnician_added_check($c_id, $datass);
                                           $this->sendmail($email_id, $c_id, $c_name, $role);
                                            $msg= "User added Successfully";
                                        } else {
                                            $msg= "User not added, kindly try again";
                                        }
                                    } 
                                    else {
                                        $data['records'] = "";

                                        $datas = array(
                                            'employee_id' => $emp_id,
                                            'user_id' => $datass,
                                            'company_id' => $c_id,
                                            'companyname' => $c_name,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                            'email_id' => $email_id,
                                            'contact_number' => $contact_no,
                                            'alternate_number' => $acontact_no,
                                            'flat_no' => $door_flatno,
                                            'street' => $street_locality,
                                            'city' => $city_name,
                                            'state' => $state_name,
                                            'region' => $region,
                                            'location' => $location,
                                            'area' => $area,
                                            'role' => $role,
                                            'image' => $image,
                                            'town' => $town_name,
                                            'landmark' => $land_mark,
                                            'country' => $country_name,
                                            'pincode' => $pin_code
                                        );
                                        if (empty($region) || empty($area) || empty($location)) {
                                            $msg= "All fields are Mandatory" . "\r\n";
                                        } else {
                                            $data['records'] = $this->Admin_model->insertuser($datas);
                                            $data['records'] = $data['records'];

                                        }
                                        $this->sendmail($email_id, $c_id, $role);
                                        $msg= "User added Successfully";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $msg="All fields are Mandatory";
        }
		echo json_encode($msg);
    }
    public function sendmail($email_id, $c_id, $c_name, $role)
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper('string');
             $password= mt_rand(100000, 999999);
             $enc_password1= $this->encrypt->encode($password);
             $login_url=base_url()."index.php?/Login/login";

        if($role == "Technician"){
            $message=" <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                <style>
                p,
                ul,
                ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
                p li,
                ul li,
                ol li {
                list-style-position: inside;
                margin-left: 5px; }
                a {
                color: #3498db;
                text-decoration: underline; }
                </style>
                <body>
                <p>Hi, New user added as a <b>$role</b>,</p>
                <p>Welcome to FieldPro</b></p>
                <p>Your Login Credentials are </p> 
                <p>Get the FieldPro App via <strong>playstore</strong></p>
                <p>Username <strong>$email_id</strong></p>
                <p>Password <strong>$password</strong></p>
                <p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.

                </body>
                </html>
            ";
        }
        else{
           $message=
            " <!doctype html>
                <html>
                <head>
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                <style>
                p,
                ul,
                ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
                p li,
                ul li,
                ol li {
                list-style-position: inside;
                margin-left: 5px; }
                a {
                color: #3498db;
                text-decoration: underline; }
                </style>
                <body>
                <p>Hi, New user added as a <b>$role</b>,</p>
                <p>Welcome to FieldPro</b></p>
                <p>Your Login Credentials are </p> 
                <p>URL <strong><a href='$login_url'>Click here to login</a></strong></p>
                <p>Username <strong>$email_id</strong></p>
                <p>Password <strong>$password</strong></p>
                <p>Best regards,</p> <p>FieldPro Team.</p>Kaspon Techworks.

                </body>
                </html>
            ";
        }
        //$username = base_url() . "/index.php?/controller_admin/password_changes/?user=$email_id&company_id=$c_id&role=$role";
        //$username = base_url() . "/index.php?/controller_admin/password_changes/?user=$email_id&company_id=$c_id&role=$role";
        //$message = "Hi, New user added as a <b>$role</b>, Click the following link to login <b>$username</b>";

            $data = array(
                    'user_type' => $role,
                    'username' => $email_id,
                    'password' => $enc_password1,
                    'companyid' => $c_id,
                    'companyname'=>$c_name             
            );
        $valueing=$this->Admin_model->login_user($data);

        $this->load->library('email');
        $config['protocol']='smtp';
        $config['smtp_host']='ssl://smtp.gmail.com';
        $config['smtp_port']='465';
        $config['smtp_timeout']='300';
        $config['smtp_user']='kaspondevelopers@gmail.com';
        $config['smtp_pass']='Kaspon@123';
        $config['charset']='utf-8';
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
        $this->email->to($email_id);
        $this->email->subject('New user - login credentials');
        $this->email->message($message);
        $this->email->send();
      
    }
    
    public function password_changes()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->view('Passwords');
    }
    public function bulk_user()
    {
        if ($this->session->userdata('user_logged_in')) {
            $msg          = "";
            $datass       = $this->session->userdata('session_username');
            $c_name       = $this->session->userdata('companyname');
            $c_id         = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($c_name, $c_id);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $this->load->library('PHPExcel');
            $handle   = $_FILES["add_excel"]['tmp_name'];
            $filename = $_FILES["add_excel"]["name"];
            $pieces   = explode(".", $filename);
            if ($pieces[1] == "xlsx") {
                $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
                $objPHPExcel   = $objReader->load($handle);
                $sheet         = $objPHPExcel->getSheet(0);
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestDataColumn();
                function toNumber($dest)
                {
                    if ($dest)
                        return ord(strtolower($dest)) - 96;
                    else
                        return 0;
                }
                function myFunction($s, $x, $y)
                {
                    $x = toNumber($x);
                    return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
                }
                for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                    $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                    $content[] = $rowData;
                }
                $e      = 2;
                $result = "";
                for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                    $sheetData   = $objPHPExcel->getActiveSheet();
                    $c_id        = $this->session->userdata('companyid');
                    $c_name      = $this->session->userdata('companyname');
                    $emp_id      = myFunction($sheetData, 0, $e);
                    $first_name  = myFunction($sheetData, 'A', $e);
                    $last_name   = myFunction($sheetData, 'B', $e);
                    $email_id    = myFunction($sheetData, 'C', $e);
                    $contact_no  = myFunction($sheetData, 'D', $e);
                    $acontact_no = myFunction($sheetData, 'E', $e);
                    $flat_no     = myFunction($sheetData, 'F', $e);
                    $street      = myFunction($sheetData, 'G', $e);
                    $town        = myFunction($sheetData, 'H', $e);
                    $landmark    = myFunction($sheetData, 'I', $e);
                    $city        = myFunction($sheetData, 'J', $e);
                    $country     = myFunction($sheetData, 'K', $e);
                    $state       = myFunction($sheetData, 'L', $e);
                    $pincode     = myFunction($sheetData, 'M', $e);
                    $region      = myFunction($sheetData, 'N', $e);
                    $area        = myFunction($sheetData, 'O', $e);
                    $location    = myFunction($sheetData, 'P', $e);
                    $role        = myFunction($sheetData, 'Q', $e);
                    $product     = myFunction($sheetData, 'R', $e);
                    $category    = myFunction($sheetData, 'S', $e);
                    $skill       = myFunction($sheetData, 'T', $e);
                     $work_type       = myFunction($sheetData, 'U', $e);
                     
                    if (empty($emp_id) && empty($first_name) && empty($last_name) && empty($email_id) && empty($contact_no) && empty($flat_no) && empty($street) && empty($town) && empty($city) && empty($country) && empty($state) && empty($pincode) && empty($region) && empty($area) && empty($location) && empty($role)) {
                        $msg[] = "2";
                    } else {
                        /*For user id auto gen*/
                        $datass = 0;
                        $datass = $this->Admin_model->userid_check();
                        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                        $datass = $datass + 1;
                        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                        $datass = $c_id . "_" . "User_" . $datass;
                        $email  = filter_var($email_id, FILTER_SANITIZE_EMAIL);

                        // Validate e-mail
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                            $empid_check = $this->Admin_model->empid_check($c_id, $emp_id); //emp id should not be repeatable for same company

                            $email_check  = $this->Admin_model->useremail_check($role, $email_id, $c_name, $c_id); //Email should not be repeatable for same company
                            $mobile_check = $this->Admin_model->usernumber_check($role, $contact_no, $c_name, $c_id); //Contact Number should not be repeatable for same company

                            $prodct_check      = $this->Admin_model->product_check($product, $c_id); //Contact Number should not be repeatable for same company
                            $subcategory_check = $this->Admin_model->subcategory_check($prodct_check, $category, $c_id); //Contact Number should not be repeatable for same company

                            /*For tech id auto gen*/
                            $data_1   = $this->Admin_model->techid_check();
                            $datass_1 = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                            $datass_1 = $datass_1 + 1;
                            $datass_1 = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                            $datass_1 = $c_id . "_" . "Tech_" . $datass_1;

                            if ($empid_check == 1) {
                                $msg[] .= "Employee id " . $emp_id . "  is already placed, Kindly check it" .  "\r\n";
                            } else {
                                if ($email_check == 1) {
                                    $msg[] .= "Email " . $email_id . " is already placed, Kindly check it" .  "\r\n";
                                } else {
                                    if ($mobile_check == 1) {
                                        $msg[] .= "Mobile number " . $contact_no . " is already placed, Kindly check it" .  "\r\n";
                                    } else {
                                        if (empty($acontact_no)) {
                                            if ($role == "Technician") {
                                               
                                                $data  = array(
                                                    'company_id' => $c_id
                                                );
                                                $datas = $this->Admin_model->checktech($data);
                                                if ($datas['technicians'] <= $datas['tech_added']) {
                                                    $msg[] .= "Technician limit is crossed, contact Super admin" .  "\r\n";
                                                } else {
                                                    if (empty($product) && empty($category) && empty($skill)) {
                                                        $msg[] .= "For Technician " . $emp_id . " Product category, Sub Category and Skill mandatory" .  "\r\n";
                                                    } else {
                                                            $check_prod=$this->Admin_model->check_prod($product);
                                                            if($check_prod != 0){
                                                                $check_produ=$this->Admin_model->check_produ($category,$check_prod);
                                                                if($check_produ !=0 ){
                                                                    $worktypearray=array("Installation","Maintenance","Break Down Calls");
                                                                    $work_type =ucwords(strtolower($work_type));
                                                                    if(in_array($work_type, $worktypearray))
                                                                    {
                                                                       $work_type=array_search($work_type,$worktypearray);
                                                                       $work_type=(int)$work_type+1;
                                                                       $datas           = array(
                                                                    'employee_id' => $emp_id,
                                                                    'technician_id' => $datass_1,
                                                                    'first_name' => $first_name,
                                                                    'last_name' => $last_name,
                                                                    'email_id' => $email_id,
                                                                    'contact_number' => $contact_no,
                                                                    'alternate_number' => $acontact_no,
                                                                    'flat_no' => $flat_no,
                                                                    'street' => $street,
                                                                    'city' => $city,
                                                                    'state' => $state,
                                                                    'region' => $region,
                                                                    'location' => $location,
                                                                    'area' => $area,
                                                                    'role' => $role,
                                                                    'product' => $check_prod['product_id'],
                                                                    'category' => $check_produ['cat_id'],
                                                                    'skill_level' => $skill,
                                                                    'company_id' => $c_id,
                                                                    'companyname' => $c_name,
                                                                    'town' => $town,
                                                                    'landmark' => $landmark,
                                                                    'country' => $country,
                                                                    'pincode' => $pincode,
                                                                    'work_type'=>$work_type
                                                                );
                                                                $data['records'] = $this->Admin_model->inserttechnician($datas);
                                                                if ($data['records'] == 1) {
                                                                    $service_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                                    $service_added_check = (int) implode('', $service_added_check);
                                                                    $data                = $service_added_check + 1;
                                                                    $datass              = array(
                                                                        'tech_added' => $data
                                                                    );
                                                                    $service_added_check = $this->Admin_model->updatetechnician_added_check($c_id, $datass);
                                                                    $this->sendmail($email_id, $c_id, $role);
                                                                    $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                                } else {
                                                                    $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                                }
                                                            }
                                                            else{
                                                                $msg[] .= "Invalid Work Type for Technician " . $emp_id ."\r\n"; 

                                                            }
                                                                }else{
                                                                    $msg[] .= "For Technician " . $emp_id . " Sub Category is not in database, kindly check it and try again" .  "\r\n"; 
                                                                }
                                                            }else{
                                                               $msg[] .= " Incorrect Product category for Technician " . $emp_id . ", kindly check it and try again" .  "\r\n"; 
                                                            }
                                                    }
                                                }
                                            } elseif ($role == "Service_Desk") {
                                              
                                                $data  = array(
                                                    'company_id' => $c_id
                                                );
                                                $datas = $this->Admin_model->checkservicedesk($data);
                                                if ($datas['service_desk'] <= $datas['service_added']) {
                                                    $msg[] .= "Service desk limit is crossed, contact Super admin" .  "\r\n";
                                                } else {
                                                    $data            = array(
                                                        'employee_id' => $emp_id,
                                                        'user_id' => $datass,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flat_no,
                                                        'street' => $street,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'company_id' => $c_id,
                                                        'companyname' => $c_name,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    $data['records'] = $this->Admin_model->insertuser($data);
                                                    if ($data['records'] == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check + 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        $this->sendmail($email_id, $c_id, $role);
                                                        $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                    } else {
                                                        $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                    }
                                                }
                                            } else {
                                                $data            = array(
                                                    'employee_id' => $emp_id,
                                                    'user_id' => $datass,
                                                    'first_name' => $first_name,
                                                    'last_name' => $last_name,
                                                    'email_id' => $email_id,
                                                    'contact_number' => $contact_no,
                                                    'alternate_number' => $acontact_no,
                                                    'flat_no' => $flat_no,
                                                    'street' => $street,
                                                    'city' => $city,
                                                    'state' => $state,
                                                    'region' => $region,
                                                    'location' => $location,
                                                    'area' => $area,
                                                    'role' => $role,
                                                    'company_id' => $c_id,
                                                    'companyname' => $c_name,
                                                    'town' => $town,
                                                    'landmark' => $landmark,
                                                    'country' => $country,
                                                    'pincode' => $pincode
                                                );
                                                $data['records'] = $this->Admin_model->insertuser($data);
                                                if ($data['records'] == 1) {
                                                    $this->sendmail($email_id, $c_id, $role);
                                                    $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                } else {
                                                    $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                }
                                            }
                                        } else {
                                            if ($contact_no == $acontact_no) {
                                                $msg[] .= "Contact Number and Alternate Contact Number Should not be same for " . $emp_id .  "\r\n";
                                            } else {
                                                if ($role == "Technician") {
                                                    $data  = array(
                                                        'company_id' => $c_id
                                                    );
                                                    $datas = $this->Admin_model->checktech($data);
                                                    if ($datas['technicians'] <= $datas['tech_added']) {
                                                        $msg[] .= "Technician limit is crossed, contact Super admin" .  "\r\n";
                                                    } else {
                                                        if (empty($product) && empty($category) && empty($skill)) {
                                                            $msg[] .= "For Technician Product category, Sub Category and Skill mandatory" . $emp_id .  "\r\n";
                                                        } else {
                                                            $check_prod=$this->Admin_model->check_prod($product);
                                                            if($check_prod != 0){
                                                                $check_produ=$this->Admin_model->check_produ($category,$check_prod);
                                                                if($check_produ !=0 ){
                                                                    $worktypearray=array("Installation","Maintenance","Break Down Calls");
                                                                    $work_type =ucwords(strtolower($work_type));
                                                                    if(in_array($work_type, $worktypearray))
                                                                    {
                                                                       $work_type=array_search($work_type,$worktypearray);
                                                                       $work_type=(int)$work_type+1;
                                                                    $datas           = array(
                                                                    'employee_id' => $emp_id,
                                                                    'technician_id' => $datass_1,
                                                                    'first_name' => $first_name,
                                                                    'last_name' => $last_name,
                                                                    'email_id' => $email_id,
                                                                    'contact_number' => $contact_no,
                                                                    'alternate_number' => $acontact_no,
                                                                    'flat_no' => $flat_no,
                                                                    'street' => $street,
                                                                    'city' => $city,
                                                                    'state' => $state,
                                                                    'region' => $region,
                                                                    'location' => $location,
                                                                    'area' => $area,
                                                                    'role' => $role,
                                                                    'product' => $check_prod['product_id'],
                                                                    'category' => $check_produ['cat_id'],
                                                                    'skill_level' => $skill,
                                                                    'company_id' => $c_id,
                                                                    'companyname' => $c_name,
                                                                    'town' => $town,
                                                                    'landmark' => $landmark,
                                                                    'country' => $country,
                                                                    'pincode' => $pincode,
                                                                    'work_type'=>$work_type
                                                                );
                                                                $data['records'] = $this->Admin_model->inserttechnician($datas);
                                                                if ($data['records'] == 1) {
                                                                    $service_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                                    $service_added_check = (int) implode('', $service_added_check);
                                                                    $data                = $service_added_check + 1;
                                                                    $datass              = array(
                                                                        'tech_added' => $data
                                                                    );
                                                                    $service_added_check = $this->Admin_model->updatetechnician_added_check($c_id, $datass);
                                                                    $this->sendmail($email_id, $c_id, $role);
                                                                    $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                                } else {
                                                                    $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                                }
                                                            }
                                                            else{
                                                                $msg[] .= "Invalid Work Type for Technician " . $emp_id ."\r\n"; 

                                                            }
                                                                }else{
                                                                    $msg[] .= "For Technician " . $emp_id . " Sub Category is not in database, kindly check it and try again" .  "\r\n"; 
                                                                }
                                                            }else{
                                                               $msg[] .= " Incorrect Product category for Technician " . $emp_id . ", kindly check it and try again" .  "\r\n"; 
                                                            }
                                                        }
                                                    }
                                                } elseif ($role == "Service_Desk") {
                                                    $data  = array(
                                                        'company_id' => $c_id
                                                    );
                                                    $datas = $this->Admin_model->checkservicedesk($data);
                                                    if ($datas['service_desk'] <= $datas['service_added']) {
                                                        $msg[] .= "Service desk limit is crossed, contact Super admin" .  "\r\n";
                                                    } else {
                                                        $data            = array(
                                                            'employee_id' => $emp_id,
                                                            'user_id' => $datass,
                                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
                                                            'email_id' => $email_id,
                                                            'contact_number' => $contact_no,
                                                            'alternate_number' => $acontact_no,
                                                            'flat_no' => $flat_no,
                                                            'street' => $street,
                                                            'city' => $city,
                                                            'state' => $state,
                                                            'region' => $region,
                                                            'location' => $location,
                                                            'area' => $area,
                                                            'role' => $role,
                                                            'company_id' => $c_id,
                                                            'companyname' => $c_name,
                                                            'town' => $town,
                                                            'landmark' => $landmark,
                                                            'country' => $country,
                                                            'pincode' => $pincode
                                                        );
                                                        $data['records'] = $this->Admin_model->insertuser($data);
                                                        if ($data['records'] == 1) {
                                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                            $data                = $service_added_check + 1;
                                                            $datass              = array(
                                                                'service_added' => $data
                                                            );
                                                            $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                            $this->sendmail($email_id, $c_id, $role);
                                                            $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                        } else {
                                                            $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                        }
                                                    }
                                                } else {
                                                    $data            = array(
                                                        'employee_id' => $emp_id,
                                                        'user_id' => $datass,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flat_no,
                                                        'street' => $street,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'company_id' => $c_id,
                                                        'companyname' => $c_name,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    $data['records'] = $this->Admin_model->insertuser($data);
                                                    if ($data['records'] == 1) {
                                                        $this->sendmail($email_id, $c_id, $role);
                                                        $msg[] .= $first_name . $last_name . " added Successfully" .  "\r\n";
                                                    } else {
                                                        $msg[] .= $first_name . $last_name . " not added, kindly try again" .  "\r\n";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $msg[] .= "Email " . $email_id . " is not a valid email" .  "\r\n";
                        }
                    }
                    $e++;
                }
            }
            print_r(json_encode($msg));
        } else {
            redirect('login/login');
        }
    }
    public function getdetails_user()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data  = $this->input->post('id');
        $datas = $this->Admin_model->getdetails_user($data);
        print_r(json_encode($datas));
    }
    public function edituser()
    {
        $msg = "";
        $welcome=[];
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id                     = $this->input->post('id1');
        $emp_id                 = $this->input->post('emp_id1');
        $c_id                   = $this->input->post('c_id1');
        $c_name                 = $this->input->post('c_name1');
        $first_name             = $this->input->post('first_name1');
        $last_name              = $this->input->post('last_name1');
        $email_id               = $this->input->post('email_id1');
        $contact_no             = $this->input->post('contact_no1');
        $acontact_no            = $this->input->post('acontact_no1');
        $flatno                 = $this->input->post('door_plotno1');
        $colony                 = $this->input->post('street_locality1');
        $town                   = $this->input->post('town_name1');
        $landmark               = $this->input->post('land_mark1');
        $city                   = $this->input->post('city_name1');
        $country                = $this->input->post('country_name1');
        $state                  = $this->input->post('state_name1');
        $pincode                = $this->input->post('pin_code1');
        $region                 = $this->input->post('region1');
        $location               = $this->input->post('location1');
        $area                   = $this->input->post('area1');
        $role                   = $this->input->post('role1');
        $roles1                 = $this->input->post('roles1');
        $field                  = $this->input->post('fields');
        $editemail_check        = $this->Admin_model->editemail_check($email_id, $emp_id);
        $editphone_check        = $this->Admin_model->editphone_check($contact_no, $emp_id);
        $check_roles            = $this->Admin_model->check_roles($role, $emp_id);
        $data_roles             = $this->Admin_model->data_roles($role, $emp_id);
        $technician_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);

        if (!empty($emp_id) && !empty($first_name) && !empty($last_name) && !empty($email_id) && !empty($contact_no) && !empty($region) && !empty($location) && !empty($area) && !empty($role)) {
            $check_roles = $this->Admin_model->check_roles($role, $emp_id);
            if (!empty($check_roles['role'])) {
                $this->form_validation->set_rules('email_id1', 'Email', 'required');
                $this->form_validation->set_rules('email_id1', 'Email', 'valid_email');
                if ($this->form_validation->run() == False) {
                    echo "Email should be in correct format";
                } else {
                    if ($editemail_check == 1) {
                        echo "Email Id should be unique";
                    } else {
                        if ($editphone_check == 1) {
                            echo "Contact Number should be unique";
                        } else {
                            if (empty($acontact_no)) {
                                $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); 
                                if ($this->form_validation->run() == False) {
                                    echo "contact number should be in correct format";
                                } else {
                                    if ($contact_no == $acontact_no) {
                                         echo "Contact Number and Alternative contact number should not be match";
                                    } else {
                                        if ($role == "Technician") {
                                            $data['records'] = "";
                                            $data_count      = $this->Admin_model->check_count($emp_id);
                                            if ($data_count == count($field)) {
                                                for ($i = 0; $i < count($field); $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                   
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0'||$field[$i][3] == '') {
                                                        $welcome = "All fields are Mandatory";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                            //$msg = "User Updated Successfully";
                                                            $welcome = "User Updated Successfully";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                           $welcome ="User not Updated Successfully";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }   
                                                    //echo json_encode($msg);
                                                    /*if($data['records'] == 1){
                                                      echo "User updated Successfully";
                                                    }else{
                                                        echo "User not updated Successfully";
                                                    }*/
                                                }
                                            } else {
                                              
                                                for ($i = 0; $i <count($field); $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                //    print_r($field);
                                                
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0'||$field[$i][3] == '') {
                                                         $message = "All fields are Mandatory";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                             $message = "User Updated Successfully\r\n". "\XA";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                             $message = "User not Updated Successfully\r\n". "\XA";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }
                                                }
                                                for ($i = $data_count + 1; $i <= count($field); $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'companyname' => $c_name,
                                                        'technician_id' => $this->input->post('tech_id1'),
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0') {
                                                         $message = "All fields are Mandatory";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->insert_user($data);
                                                        if ($data['records'] == 1) {
                                                             $message = "User Updated Successfully\r\n". "\XA";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                             $message = "User not Updated Successfully\r\n". "\XA";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }
                                                }
                                            }
                                            echo $welcome;
                                        } else {
                                            $data= array(
                                                'employee_id' => $emp_id,
                                                'company_id' => $c_id,
                                                'first_name' => $first_name,
                                                'last_name' => $last_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'flat_no' => $flatno,
                                                'street' => $colony,
                                                'city' => $city,
                                                'state' => $state,
                                                'region' => $region,
                                                'location' => $location,
                                                'area' => $area,
                                                'role' => $role,
                                                'town' => $town,
                                                'landmark' => $landmark,
                                                'country' => $country,
                                                'pincode' => $pincode
                                            );
                                            $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                            //print_r(json_encode('User added Successfully'));
                                            echo "User Updated Successfully\r\n". "\XA";
                                        }
                                    }
                                }
                            }
                             else {
                                $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                                $this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                                if ($this->form_validation->run() == False) {
                                    echo json_encode("Phone number and contact number should be in correct format");
                                } else {
                                    if ($contact_no == $acontact_no) {
                                        echo json_encode("Contact Number and Alternative contact number should not be same");
                                    }
                                    else {
                                        if ($role == "Technician") {
                                            $data['records'] = "";
                                            $data_count      = $this->Admin_model->check_count($emp_id);
                                            if ($data_count == count($field)) {
                                                for ($i = 0; $i < count($field); $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0') {
                                                        $welcome= "All fields are Mandatory1";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                            $welcome= "User Updated Successfully\r\n". "\XA";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                             $welcome= "User not Updated Successfully\r\n". "\XA";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }
                                                    /*if($data['records'] == 1){
                                                    echo "User updated Successfully";
                                                    }else{
                                                    echo "User not updated Successfully";
                                                    }*/
                                                }
                                            } else {
                                                for ($i = 0; $i < $data_count; $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0') {
                                                         $welcome= "All fields are Mandatory";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                             $welcome= "User Updated Successfully\r\n". "\XA";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                             $welcome= "User not Updated Successfully\r\n". "\XA";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }
                                                }
                                                for ($i = $data_count + 1; $i <= count($field); $i++) {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'companyname' => $c_name,
                                                        'technician_id' => $this->input->post('tech_id1'),
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'product' => $field[$i][0],
                                                        'category' => $field[$i][1],
                                                        'skill_level' => $field[$i][2],
                                                        'work_type'=>$field[$i][3],
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0') {
                                                         $welcome= "All fields are Mandatory";
                                                    } else {
                                                        $data['records'] = $this->Admin_model->insert_user($data);
                                                        if ($data['records'] == 1) {
                                                             $welcome= "User Updated Successfully\r\n". "\XA";
                                                            //$data['records']=$data['records'];
                                                        } else {
                                                            $welcome= "User not Updated Successfully\r\n". "\XA";
                                                            //$data['records'];
                                                        }
                                                        //$data['records']=$data['records'];
                                                    }
                                                }
                                            }
                                            echo $welcome;
                                        } else {
                                            $data= array(
                                                'employee_id' => $emp_id,
                                                'company_id' => $c_id,
                                                'first_name' => $first_name,
                                                'last_name' => $last_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'flat_no' => $flatno,
                                                'street' => $colony,
                                                'city' => $city,
                                                'state' => $state,
                                                'region' => $region,
                                                'location' => $location,
                                                'area' => $area,
                                                'role' => $role,
                                                'town' => $town,
                                                'landmark' => $landmark,
                                                'country' => $country,
                                                'pincode' => $pincode
                                            );
                                            $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                             echo "User Updated Successfully\r\n". "\XA";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $this->form_validation->set_rules('email_id1', 'Email', 'required');
                $this->form_validation->set_rules('email_id1', 'Email', 'valid_email');
                if ($this->form_validation->run() == False) {
                    $msg = "Email should be in correct format";
                } else {
                    if ($editemail_check == 1) {
                        echo "Email Id should be unique";
                    } else {
                        if ($editphone_check == 1) {
                            echo "Contact Number should be unique";
                        } else {
                            if (empty($acontact_no)) {
                                $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                                //$this->form_validation->set_rules('acontact_no_1', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]'); //{10} for 10 digits number
                                if ($this->form_validation->run() == False) {
                                    echo "contact number should be in correct format";
                                } else {
                                    if ($role == $roles1) {
                                        //echo "role and roles are same";
                                    } else {
                                        if ($role == "Service_Desk") {
                                            if ($roles1 == "Technician") {
                                                $datass          = $this->Admin_model->userid_check();
                                                $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                                                $datass          = $datass + 1;
                                                $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $company_id      = $this->session->userdata('companyid');
                                                $datass          = $company_id . "_" . "User_" . $datass;
                                                $data            = array(
                                                    'employee_id' => $emp_id,
                                                    'company_id' => $c_id,
                                                    'companyname' => $c_name,
                                                    'user_id ' => $datass,
                                                    'first_name' => $first_name,
                                                    'last_name' => $last_name,
                                                    'email_id' => $email_id,
                                                    'contact_number' => $contact_no,
                                                    'alternate_number' => $acontact_no,
                                                    'flat_no' => $flatno,
                                                    'street' => $colony,
                                                    'city' => $city,
                                                    'state' => $state,
                                                    'region' => $region,
                                                    'location' => $location,
                                                    'area' => $area,
                                                    'role' => $role,
                                                    'town' => $town,
                                                    'landmark' => $landmark,
                                                    'country' => $country,
                                                    'pincode' => $pincode
                                                );
                                                $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                if ($data['records'] == 1) {
                                                    $technician_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                    $technician_added_check = (int) implode('', $technician_added_check);
                                                    $data                   = $technician_added_check - 1;
                                                    $datas                  = array(
                                                        'tech_added' => $data
                                                    );
                                                    $tech_added_check       = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                    if ($tech_added_check == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check + 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        echo "User Updated Successfully\r\n". "\XA";
                                                    }
                                                } else {
                                                   echo "User not updated kindly try again\r\n"."\XA";
                                                }
                                            } else {
                                                $data = array(
                                                    'employee_id' => $emp_id,
                                                    'company_id' => $c_id,
                                                    'first_name' => $first_name,
                                                    'last_name' => $last_name,
                                                    'email_id' => $email_id,
                                                    'contact_number' => $contact_no,
                                                    'alternate_number' => $acontact_no,
                                                    'flat_no' => $flatno,
                                                    'street' => $colony,
                                                    'city' => $city,
                                                    'state' => $state,
                                                    'region' => $region,
                                                    'location' => $location,
                                                    'area' => $area,
                                                    'role' => $role,
                                                    'town' => $town,
                                                    'landmark' => $landmark,
                                                    'country' => $country,
                                                    'pincode' => $pincode
                                                );
                                                $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                if ($data['records'] == 1) {
                                                    $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                    $data                = $service_added_check + 1;
                                                    $datass              = array(
                                                        'service_added' => $data
                                                    );
                                                    $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                    /* $technician_added_check = $this->Admin_model->technician_added_check($c_name,$c_id);
                                                    $technician_added_check = (int) implode('', $technician_added_check);
                                                    $data=$technician_added_check - 1;
                                                    $datas=array(
                                                    'tech_added'=>$data,
                                                    );
                                                    $tech_added_check = $this->Admin_model->updatetechnician_added_check($c_id,$datas); */
                                                    echo "User Updated Successfully\r\n". "\XA";
                                                } else {
                                                    echo "User not updated successfully, kindly check it\r\n". "\XA";
                                                }
                                            }
                                        } elseif ($role == "Technician") {
                                            if ($roles1 == "Service Desk") {
                                                $data['records'] = "";
                                                $data_1          = $this->Admin_model->techid_check();
                                                $datass_1        = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                                                $datass_1        = $datass_1 + 1;
                                                $datass_1        = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                                                $company_id      = $this->session->userdata('companyid');
                                                $datass_1        = $company_id . "_" . "Tech_" . $datass_1;
                                                for ($i = 0; $i < count($field); $i++) {
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0'||  $field[$i][3] == '') {
                                                        echo "All fields are Mandatory";
                                                    } else {
                                                        $data            = array(
                                                            'technician_id' => $datass_1,
                                                            'employee_id' => $emp_id,
                                                            'company_id' => $c_id,
                                                            'companyname' => $c_name,
                                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
                                                            'email_id' => $email_id,
                                                            'contact_number' => $contact_no,
                                                            'alternate_number' => $acontact_no,
                                                            'flat_no' => $flatno,
                                                            'street' => $colony,
                                                            'city' => $city,
                                                            'state' => $state,
                                                            'region' => $region,
                                                            'location' => $location,
                                                            'area' => $area,
                                                            'role' => $role,
                                                            'product' => $field[$i][0],
                                                            'category' => $field[$i][1],
                                                            'skill_level' => $field[$i][2],
                                                            'work_type'=>$field[$i][3],
                                                            'town' => $town,
                                                            'landmark' => $landmark,
                                                            'country' => $country,
                                                            'pincode' => $pincode
                                                        );
                                                        $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                        $data['records'] = $data['records'];
                                                         echo "User Updated Successfully\r\n". "\XA";
                                                    }
                                                }
                                                if ($data['records'] == 1) {
                                                    $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                    $technician_added_check    = (int) implode('', $technician_added_check);
                                                    $data                      = $technician_added_check + 1;
                                                    $datas                     = array(
                                                        'tech_added' => $data
                                                    );
                                                    $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                    if ($tech_added_check['recor'] == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check - 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                    }
                                                }
                                            } else {
                                                $data_1     = $this->Admin_model->techid_check();
                                                $datass_1   = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                                                $datass_1   = $datass_1 + 1;
                                                $datass_1   = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                                                $company_id = $this->session->userdata('companyid');
                                                $datass_1   = $company_id . "_" . "Tech_" . $datass_1;
                                                for ($i = 0; $i < count($field); $i++) {
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0' ||  $field[$i][3] == '') {
                                                         echo "All fields are Mandatory";
                                                    } else {
                                                        $data            = array(
                                                            'technician_id' => $datass_1,
                                                            'employee_id' => $emp_id,
                                                            'company_id' => $c_id,
                                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
                                                            'email_id' => $email_id,
                                                            'contact_number' => $contact_no,
                                                            'alternate_number' => $acontact_no,
                                                            'flat_no' => $flatno,
                                                            'street' => $colony,
                                                            'city' => $city,
                                                            'state' => $state,
                                                            'region' => $region,
                                                            'location' => $location,
                                                            'area' => $area,
                                                            'role' => $role,
                                                            'product' => $field[$i][0],
                                                            'category' => $field[$i][1],
                                                            'skill_level' => $field[$i][2],
                                                            'work_type'=>$field[$i][3],
                                                            'town' => $town,
                                                            'landmark' => $landmark,
                                                            'country' => $country,
                                                            'pincode' => $pincode
                                                        );
                                                        $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                        $data['records'] = $data['records'];
                                                        $msg             = "User Updated Successfully\r\n". "\XA";
                                                    }
                                                }
                                                if ($data['records'] == 1) {
                                                    $technician_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                    $technician_added_check = (int) implode('', $technician_added_check);
                                                    $data                   = $technician_added_check + 1;
                                                    $datas                  = array(
                                                        'tech_added' => $data
                                                    );
                                                    $tech_added_check       = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                }
                                            }
                                        } else {
                                            if ($role == "Technician") {
                                                $data['records'] = "";
                                                for ($i = 1; $i <= count($field); $i++) {
                                                    if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0'||  $field[$i][3] == '') {
                                                        echo "All fields are Mandatory";
                                                    } else {
                                                        $data            = array(
                                                            'employee_id' => $emp_id,
                                                            'company_id' => $c_id,
                                                            'first_name' => $first_name,
                                                            'last_name' => $last_name,
                                                            'email_id' => $email_id,
                                                            'contact_number' => $contact_no,
                                                            'alternate_number' => $acontact_no,
                                                            'flat_no' => $flatno,
                                                            'street' => $colony,
                                                            'city' => $city,
                                                            'state' => $state,
                                                            'region' => $region,
                                                            'location' => $location,
                                                            'area' => $area,
                                                            'role' => $role,
                                                            'product' => $field[$i][0],
                                                            'category' => $field[$i][1],
                                                            'skill_level' => $field[$i][2],
                                                            'work_type'=>$field[$i][3],
                                                            'town' => $town,
                                                            'landmark' => $landmark,
                                                            'country' => $country,
                                                            'pincode' => $pincode
                                                        );
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        $data['records'] = $data['records'];
                                                        $msg             = "User Updated Successfully\r\n". "\XA";
                                                    }
                                                }
                                                if ($data['records'] == 1) {
                                                    $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                    $technician_added_check    = (int) implode('', $technician_added_check);
                                                    $data                      = $technician_added_check + 1;
                                                    $datas                     = array(
                                                        'tech_added' => $data
                                                    );
                                                    $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);

                                                }
                                            } elseif ($role == "Service_Desk") {
                                                $data            = array(
                                                    'employee_id' => $emp_id,
                                                    'company_id' => $c_id,
                                                    'first_name' => $first_name,
                                                    'last_name' => $last_name,
                                                    'email_id' => $email_id,
                                                    'contact_number' => $contact_no,
                                                    'alternate_number' => $acontact_no,
                                                    'flat_no' => $flatno,
                                                    'street' => $colony,
                                                    'city' => $city,
                                                    'state' => $state,
                                                    'region' => $region,
                                                    'location' => $location,
                                                    'area' => $area,
                                                    'role' => $role,
                                                    'town' => $town,
                                                    'landmark' => $landmark,
                                                    'country' => $country,
                                                    'pincode' => $pincode
                                                );
                                                $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                if ($data['records'] == 1) {
                                                    $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                    $data                = $service_added_check - 1;
                                                    $datass              = array(
                                                        'service_added' => $data
                                                    );
                                                    $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                    echo "User Updated Successfully\r\n". "\XA";
                                                } else {
                                                    echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                }
                                            } else {
                                                $data = array(
                                                    'employee_id' => $emp_id,
                                                    'company_id' => $c_id,
                                                    'first_name' => $first_name,
                                                    'last_name' => $last_name,
                                                    'email_id' => $email_id,
                                                    'contact_number' => $contact_no,
                                                    'alternate_number' => $acontact_no,
                                                    'flat_no' => $flatno,
                                                    'street' => $colony,
                                                    'city' => $city,
                                                    'state' => $state,
                                                    'region' => $region,
                                                    'location' => $location,
                                                    'area' => $area,
                                                    'role' => $role,
                                                    'town' => $town,
                                                    'landmark' => $landmark,
                                                    'country' => $country,
                                                    'pincode' => $pincode
                                                );
                                                if ($roles1 == "Service_Desk") {
                                                    $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                    if ($data['records'] == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check - 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        $msg                 = "User Updated Successfully\r\n". "\XA";
                                                    } else {
                                                         echo "User not updated successfully, kindly check it";
                                                    }
                                                } elseif ($roles1 == "Technician") {
                                                    $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                    if ($data['records'] == 1) {
                                                        $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                        $technician_added_check    = (int) implode('', $technician_added_check);
                                                        $data                      = $technician_added_check - 1;
                                                        $datas                     = array(
                                                            'tech_added' => $data
                                                        );
                                                        $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                         echo "User Updated Successfully\r\n". "\XA";
                                                    } else {
                                                         echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                    }
                                                } else {
                                                    $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                    if ($data['records'] == 1) {
                                                         echo "User Updated Successfully\r\n". "\XA";
                                                    } else {
                                                         echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                                $this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                                if ($this->form_validation->run() == False) {
                                     echo "Phone number and contact number should be in correct format";
                                } else {
                                    if ($contact_no == $acontact_no) {
                                         echo "Contact Number and Alternative contact number should not be match";
                                    } else {
                                        if ($role == $roles1) {
                                            //echo "role and roles are same";
                                        } else {
                                            if ($role == "Service_Desk") {
                                                if ($roles1 == "Technician") {
                                                    $datass          = $this->Admin_model->userid_check();
                                                    $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                                                    $datass          = $datass + 1;
                                                    $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                    $company_id      = $this->session->userdata('companyid');
                                                    $datass          = $company_id . "_" . "User_" . $datass;
                                                    $data            = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'user_id ' => $datass,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                    if ($data['records'] == 1) {
                                                        $technician_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                        $technician_added_check = (int) implode('', $technician_added_check);
                                                        $data                   = $technician_added_check - 1;
                                                        $datas                  = array(
                                                            'tech_added' => $data
                                                        );
                                                        $tech_added_check       = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                        if ($tech_added_check == 1) {
                                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                            $data                = $service_added_check + 1;
                                                            $datass              = array(
                                                                'service_added' => $data
                                                            );
                                                            $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                             echo "User Updated Successfully\r\n". "\XA";
                                                        }
                                                    } else {
                                                         echo "User not updated kindly try again\r\n"."\XA";
                                                    }
                                                } else {
                                                    $data            = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                    if ($data['records'] == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check + 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        /* $technician_added_check = $this->Admin_model->technician_added_check($c_name,$c_id);
                                                        $technician_added_check = (int) implode('', $technician_added_check);
                                                        $data=$technician_added_check - 1;
                                                        $datas=array(
                                                        'tech_added'=>$data,
                                                        );
                                                        $tech_added_check = $this->Admin_model->updatetechnician_added_check($c_id,$datas); */
                                                         echo "User Updated Successfully\r\n". "\XA";
                                                    } else {
                                                         echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                    }
                                                }
                                            } elseif ($role == "Technician") {
                                                if ($roles1 == "Service_Desk") {
                                                    $data_1          = $this->Admin_model->techid_check();
                                                    $datass_1        = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                                                    $datass_1        = $datass_1 + 1;
                                                    $datass_1        = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                                                    $company_id      = $this->session->userdata('companyid');
                                                    $datass_1        = $company_id . "_" . "Tech_" . $datass_1;
                                                    $data['records'] = "";
                                                    for ($i = 0; $i < count($field); $i++) {
                                                        if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0' || $field[$i][3] == '') {
                                                             echo "All fields are Mandatory";
                                                        } else {
                                                            $data            = array(
                                                                'technician_id' => $datass_1,
                                                                'employee_id' => $emp_id,
                                                                'company_id' => $c_id,
                                                                'first_name' => $first_name,
                                                                'last_name' => $last_name,
                                                                'email_id' => $email_id,
                                                                'contact_number' => $contact_no,
                                                                'alternate_number' => $acontact_no,
                                                                'flat_no' => $flatno,
                                                                'street' => $colony,
                                                                'city' => $city,
                                                                'state' => $state,
                                                                'region' => $region,
                                                                'location' => $location,
                                                                'area' => $area,
                                                                'role' => $role,
                                                                'product' => $field[$i][0],
                                                                'category' => $field[$i][1],
                                                                'skill_level' => $field[$i][2],
								'work_type'=>$field[$i][3],
                                                                'town' => $town,
                                                                'landmark' => $landmark,
                                                                'country' => $country,
                                                                'pincode' => $pincode
                                                            );
                                                            $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                            $data['records'] = $data['records'];
                                                             echo "User Updated Successfully\r\n". "\XA";
                                                        }
                                                    }
                                                    //$data['records']=$this->Admin_model-> edit_insert_user($data,$emp_id,$role,$c_id);
                                                    if ($data['records'] == 1) {
                                                        $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                        $technician_added_check    = (int) implode('', $technician_added_check);
                                                        $data                      = $technician_added_check + 1;
                                                        $datas                     = array(
                                                            'tech_added' => $data
                                                        );
                                                        $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                        if ($tech_added_check['recor'] == 1) {
                                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                            $data                = $service_added_check - 1;
                                                            $datass              = array(
                                                                'service_added' => $data
                                                            );
                                                            $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        }
                                                    }
                                                    /*else{
                                                    echo "User not updated successfully, kindly check it";
                                                    } */
                                                } else {
                                                    $data_1          = $this->Admin_model->techid_check();
                                                    $datass_1        = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                                                    $datass_1        = $datass_1 + 1;
                                                    $datass_1        = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                                                    $company_id      = $this->session->userdata('companyid');
                                                    $datass_1        = $company_id . "_" . "Tech_" . $datass_1;
                                                    $data['records'] = "";
                                                    for ($i = 0; $i < count($field); $i++) {
                                                        if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0' || $field[$i][3] == '') {
                                                             echo "All fields are Mandatory". "\r\n";
                                                        } else {
                                                            $data            = array(
                                                                'technician_id' => $datass_1,
                                                                'employee_id' => $emp_id,
                                                                'company_id' => $c_id,
                                                                'first_name' => $first_name,
                                                                'last_name' => $last_name,
                                                                'email_id' => $email_id,
                                                                'contact_number' => $contact_no,
                                                                'alternate_number' => $acontact_no,
                                                                'flat_no' => $flatno,
                                                                'street' => $colony,
                                                                'city' => $city,
                                                                'state' => $state,
                                                                'region' => $region,
                                                                'location' => $location,
                                                                'area' => $area,
                                                                'role' => $role,
                                                                'product' => $field[$i][0],
                                                                'category' => $field[$i][1],
                                                                'skill_level' => $field[$i][2],
                                                                'work_type'=>$field[$i][3],
                                                                'town' => $town,
                                                                'landmark' => $landmark,
                                                                'country' => $country,
                                                                'pincode' => $pincode
                                                            );
                                                            $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                            $data['records'] = $data['records'];
                                                            $msg             = "User Updated Successfully\r\n". "\XA";
                                                        }
                                                    }
                                                    //$data['records']=$this->Admin_model-> edit_insert_user($data,$emp_id,$role,$c_id);
                                                    if ($data['records'] == 1) {
                                                        $technician_added_check = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                        $technician_added_check = (int) implode('', $technician_added_check);
                                                        $data                   = $technician_added_check + 1;
                                                        $datas                  = array(
                                                            'tech_added' => $data
                                                        );
                                                        $tech_added_check       = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                    }
                                                }
                                            } else {
                                                if ($role == "Technician") {
                                                    $data_1          = $this->Admin_model->techid_check();
                                                    $datass_1        = intval(preg_replace('/[^0-9]+/', '', $data_1), 10);
                                                    $datass_1        = $datass_1 + 1;
                                                    $datass_1        = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
                                                    $company_id      = $this->session->userdata('companyid');
                                                    $datass_1        = $company_id . "_" . "Tech_" . $datass_1;
                                                    $data['records'] = "";
                                                    for ($i = 0; $i < count($field); $i++) {
                                                        if (empty($field[$i][0]) || $field[$i][1] == 'sel_sub' || $field[$i][2] == 'L0'|| $field[$i][3] == '') {
                                                            echo "All fields are Mandatory";
                                                        } else {
                                                            $data            = array(
                                                                'technician_id' => $datass_1,
                                                                'employee_id' => $emp_id,
                                                                'company_id' => $c_id,
                                                                'first_name' => $first_name,
                                                                'last_name' => $last_name,
                                                                'email_id' => $email_id,
                                                                'contact_number' => $contact_no,
                                                                'alternate_number' => $acontact_no,
                                                                'flat_no' => $flatno,
                                                                'street' => $colony,
                                                                'city' => $city,
                                                                'state' => $state,
                                                                'region' => $region,
                                                                'location' => $location,
                                                                'area' => $area,
                                                                'role' => $role,
                                                                'product' => $field[$i][0],
                                                                'category' => $field[$i][1],
                                                                'skill_level' => $field[$i][2],
                                                                'work_type'=>$field[$i][3],
                                                                'town' => $town,
                                                                'landmark' => $landmark,
                                                                'country' => $country,
                                                                'pincode' => $pincode
                                                            );
                                                            $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                            $data['records'] = $data['records'];
                                                            echo "User Updated Successfully\r\n". "\XA";
                                                        }
                                                    }
                                                    //$data['records']=$this->Admin_model-> edit_insert_user($data,$emp_id,$role,$c_id);
                                                    if ($data['records'] == 1) {
                                                        $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                        $technician_added_check    = (int) implode('', $technician_added_check);
                                                        $data                      = $technician_added_check + 1;
                                                        $datas                     = array(
                                                            'tech_added' => $data
                                                        );
                                                        $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                    }
                                                    /*else{
                                                    echo "User not updated successfully, kindly check it";
                                                    }*/
                                                } elseif ($role == "Service Desk") {
                                                    $data            = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                    if ($data['records'] == 1) {
                                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                        $data                = $service_added_check - 1;
                                                        $datass              = array(
                                                            'service_added' => $data
                                                        );
                                                        $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                        echo "User Updated Successfully\r\n". "\XA";
                                                    } else {
                                                        echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                    }
                                                } else {
                                                    $data = array(
                                                        'employee_id' => $emp_id,
                                                        'company_id' => $c_id,
                                                        'first_name' => $first_name,
                                                        'last_name' => $last_name,
                                                        'email_id' => $email_id,
                                                        'contact_number' => $contact_no,
                                                        'alternate_number' => $acontact_no,
                                                        'flat_no' => $flatno,
                                                        'street' => $colony,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'region' => $region,
                                                        'location' => $location,
                                                        'area' => $area,
                                                        'role' => $role,
                                                        'town' => $town,
                                                        'landmark' => $landmark,
                                                        'country' => $country,
                                                        'pincode' => $pincode
                                                    );
                                                    if ($roles1 == "Service_Desk") {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                            $service_added_check = $this->Admin_model->service_added_check($c_id);
                                                            $data                = $service_added_check - 1;
                                                            $datass              = array(
                                                                'service_added' => $data
                                                            );
                                                            $serv_added_check    = $this->Admin_model->updateservice_added_check($c_id, $datass);
                                                            echo "User Updated Successfully\r\n". "\XA";
                                                        } else {
                                                             echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                        }
                                                    } elseif ($roles1 == "Technician") {
                                                        $data['records'] = $this->Admin_model->edit_insert_user($data, $emp_id, $role, $c_id);
                                                        if ($data['records'] == 1) {
                                                            $technician_added_check    = $this->Admin_model->technician_added_check($c_name, $c_id);
                                                            $technician_added_check    = (int) implode('', $technician_added_check);
                                                            $data                      = $technician_added_check - 1;
                                                            $datas                     = array(
                                                                'tech_added' => $data
                                                            );
                                                            $tech_added_check['recor'] = $this->Admin_model->updatetechnician_added_check($c_id, $datas);
                                                             echo "User Updated Successfully\r\n". "\XA";
                                                        } else {
                                                             echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                        }
                                                    } else {
                                                        $data['records'] = $this->Admin_model->edituser($data, $emp_id, $role);
                                                        if ($data['records'] == 1) {
                                                             echo "User Updated Successfully\r\n". "\XA";
                                                        } else {
                                                             echo "User not updated successfully, kindly check it\r\n"."\XA";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
             echo "All fields are Mandatory\r\n". "\XA";
        }

       // echo $msg;
    }
    public function deleteuser()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deleteuser($id);
        echo $data;
    }
    public function technician()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            //$result=$this->input->cookie('user',true);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['record']  = $this->Admin_model->getproduct();
            $data['recor']   = $this->Admin_model->getsubproduct();
            $data['records'] = $this->Admin_model->gettechnician();
            $this->load->view('User_Manageeement_Technician', $data);
        } else {
            redirect('login/login');
        }
    }
    public function techid_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data_1   = $this->Admin_model->techid_check();
        $datass_1 = intval(preg_replace('/[^0-9]+/', '', $datass_1), 10);
        $datass_1 = $datass_1 + 1;
        $datass_1 = str_pad($datass_1, 4, "0", STR_PAD_LEFT);
        $datass_1 = "Tech_" . $datass_1;
        echo $datass_1;
    }
    public function inserttechnician()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $tech_id     = $this->input->post('tech_id');
        $first_name  = $this->input->post('first_name');
        $last_name   = $this->input->post('last_name');
        $email_id    = $this->input->post('email_id');
        $skill       = $this->input->post('skill');
        $contact_no  = $this->input->post('contact_no');
        $acontact_no = $this->input->post('acontact_no');
        $address     = $this->input->post('address');
        $product     = $this->input->post('product');
        $category    = $this->input->post('category');
        $c_id        = $this->input->post('c_id');
        $c_name      = $this->input->post('c_name');
        $role        = 'Technician';
        $image       = base_url() . 'assets/images/users/simple.jpg';
        if (!empty($tech_id) && !empty($first_name) && !empty($last_name) && !empty($email_id) && !empty($skill) && !empty($contact_no) && !empty($address) && !empty($product) && !empty($category)) {
            $this->form_validation->set_rules('email_id', 'Email', 'required');
            $this->form_validation->set_rules('email_id', 'Email', 'valid_email');
            if ($this->form_validation->run() == False) {
                echo "Email should be in correct format";
            } else {
                $mobile_check_tech = $this->Admin_model->mobile_check_tech($contact_no);
                $email_check_tech  = $this->Admin_model->email_check_tech($email_id);
                if ($email_check_tech == 1) {
                    echo "Email is already placed, Kindly check it";
                } else {
                    if ($mobile_check_tech == 1) {
                        echo "Contact number is already placed, Kindly check it";
                    } else {
                        if (empty($acontact_no)) {
                            $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            //$this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number should not be match";
                                } else {
                                    $data            = array(
                                        'technician_id' => $this->input->post('tech_id'),
                                        'first_name' => $this->input->post('first_name'),
                                        'last_name' => $this->input->post('last_name'),
                                        'image' => $image,
                                        'email_id' => $this->input->post('email_id'),
                                        'skill_level' => $this->input->post('skill'),
                                        'contact_number' => $this->input->post('contact_no'),
                                        'alternate_number' => $this->input->post('acontact_no'),
                                        'address' => $this->input->post('address'),
                                        'product' => $this->input->post('product'),
                                        'category' => $this->input->post('category'),
                                        'company_id' => $this->input->post('c_id'),
                                        'companyname' => ""
                                    );
                                    $data['records'] = $this->Admin_model->inserttechnician($data);
                                    if ($data['records'] == 1) {
                                        $c_id                = $c_id;
                                        $c_name              = $c_name;
                                        $role                = $role;
                                        $email_id            = $email_id;
                                        //$result = "User added successfully";
                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                        $data                = $service_added_check + 1;
                                        $datas               = array(
                                            'service_added' => $data
                                        );
                                        $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datas);
                                        $this->sendmail($email_id, $c_id, $role);
                                        //$result = "User added Successfully";
                                        echo "Technician added Successfully";
                                    } else {
                                        echo "Technician not added, kindly try again";
                                    }
                                }
                            }
                        } else {
                            $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            $this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    $data            = array(
                                        'technician_id' => $this->input->post('tech_id'),
                                        'first_name' => $this->input->post('first_name'),
                                        'last_name' => $this->input->post('last_name'),
                                        'image' => $image,
                                        'email_id' => $this->input->post('email_id'),
                                        'skill_level' => $this->input->post('skill'),
                                        'contact_number' => $this->input->post('contact_no'),
                                        'alternate_number' => $this->input->post('acontact_no'),
                                        'address' => $this->input->post('address'),
                                        'product' => $this->input->post('product'),
                                        'category' => $this->input->post('category'),
                                        'company_id' => $this->input->post('c_id'),
                                        'companyname' => ""
                                    );
                                    $data['records'] = $this->Admin_model->inserttechnician($data);
                                    if ($data['records'] == 1) {
                                        $c_id                = $c_id;
                                        $c_name              = $c_name;
                                        $role                = $role;
                                        $email_id            = $email_id;
                                        //$result = "User added successfully";
                                        $service_added_check = $this->Admin_model->service_added_check($c_id);
                                        $data                = $service_added_check + 1;
                                        $datas               = array(
                                            'service_added' => $data
                                        );
                                        $service_added_check = $this->Admin_model->updateservice_added_check($c_id, $datas);
                                        $this->sendmail($email_id, $c_id, $role);
                                        //$result = "User added Successfully";
                                        echo "Technician added Successfully";
                                    } else {
                                        echo "Technician not added, kindly try again";
                                    }
                                }
                            }
                        }
                    }
                }
                /*$this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]'); //{10} for 10 digits number
                $this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]'); //{10} for 10 digits number
                if ($this->form_validation->run() == False)
                {
                echo "Phone number and contact number should be in correct format";
                }else{
                if($contact_no==$acontact_no){
                echo "Contact Number and Alternative contact number should not be match";
                }else{
                $data=array(
                'technician_id'=>$this->input->post('tech_id'),
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'image'=>$image,
                'email_id'=>$this->input->post('email_id'),
                'skill_level'=>$this->input->post('skill'),
                'contact_number'=>$this->input->post('contact_no'),
                'alternate_number'=>$this->input->post('acontact_no'),
                'address'=>$this->input->post('address'),
                'product'=>$this->input->post('product'),
                'category'=>$this->input->post('category'),
                'company_id'=>$this->input->post('c_id'),
                'companyname'=>$this->input->post('c_name'),
                );
                $data['records']=$this->Admin_model->inserttechnician($data);
                if($data['records']==1){
                if($valueing==1){
                $c_id=$c_id;
                $c_name=$c_name;
                $role=$role;
                $email_id=$email_id;
                //$result = "User added successfully";
                $service_added_check = $this->Admin_model->service_added_check($c_name,$c_id);
                $data=$service_added_check + 1;
                $datas=array(
                'service_added'=>$data,
                );
                $service_added_check = $this->Admin_model->updateservice_added_check($c_name,$c_id,$datas);
                $this->sendmail($email_id,$c_id,$role);
                $result = "User added Successfully";
                }
                echo "Technician added Successfully";
                }else{
                echo "Technician not added, kindly try again";
                }
                }
                }*/
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function bulk_technician()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('Excel');
        $handle   = $_FILES["add_technician"]['tmp_name'];
        $filename = $_FILES["add_technician"]["name"];
        $pieces   = explode(".", $filename);
        if ($pieces[1] == "xlsx") {
            $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel   = $objReader->load($handle);
            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest)
                    return ord(strtolower($dest)) - 96;
                else
                    return 0;
            }
            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }
            for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }
            $e      = 2;
            $result = "";
            for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                $sheetData       = $objPHPExcel->getActiveSheet();
                $cellData        = myFunction($sheetData, 'B', $e);
                $cellData1       = myFunction($sheetData, 'D', $e);
                $cellData2       = myFunction($sheetData, 'E', $e);
                $datass          = $this->Admin_model->techid_check();
                $datass          = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                $datass          = $datass + 1;
                $datass          = str_pad($datass, 4, "0", STR_PAD_LEFT);
                $datass          = "Tech_" . $datass;
                $datass          = trim($datass, " ");
                $image           = base_url() . 'assets/images/users/simple.jpg';
                $result          = $image;
                $getdata['data'] = $this->Admin_model->gettechdata($cellData, $cellData1);
                if ($getdata['data'] == "") {
                    if (!filter_var($cellData, FILTER_VALIDATE_EMAIL)) {
                        $result = "email not valid";
                    } else {
                        // valid mobile number
                        if ($cellData1 == $cellData2) {
                            $result = "Contact Number and Alternative Contact Number should not be Equal";
                        } else {
                            $data     = implode('","', $content[$makeInsert][0]);
                            $datas    = $content[$makeInsert][0];
                            $data     = array(
                                'technician_id' => $datass,
                                'first_name' => $datas[0],
                                'last_name' => $datas[1],
                                'image' => $image,
                                'email_id' => $cellData,
                                'skill_level' => $datas[3],
                                'contact_number' => $cellData1,
                                'alternate_number' => $cellData2,
                                'address' => $datas[5],
                                'product' => $datas[6],
                                'category' => $datas[7]
                            );
                            //print_r($salary);
                            $valueing = $this->Admin_model->inserttechnician($data);
                            $result   = "User added successfully";
                        }
                    }

                    //$result="value not present";
                } else {
                    $result = "The Email ID: $cellData and Mobile Number: $cellData1 is already present";
                }
                $e++;
            }
            echo $result;
            /* if($valueing==1){
            $result.="Values are uploaded";
            }else{
            $result.="Values are not uploaded, Kindly checkit and try again";
            } */
            //echo $valueing;
        }
    }
    public function getdetails_technician()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_technician($data);
        print_r(json_encode($data));
    }
    public function edit_technician()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id                   = $this->input->post('id1');
        $techid               = $this->input->post('tech_id1');
        $first_name           = $this->input->post('first_name1');
        $last_name            = $this->input->post('last_name1');
        $email_id             = $this->input->post('email_id1');
        $skill                = $this->input->post('skill1');
        $contact_no           = $this->input->post('contact_no1');
        $acontact_no          = $this->input->post('acontact_no1');
        $address              = $this->input->post('address1');
        $product              = $this->input->post('product1');
        $category             = $this->input->post('category1');
        $image                = base_url() . 'assets/images/users/simple.jpg';
        $editemail_check_tech = $this->Admin_model->editemail_check_tech($email_id, $id);
        $editphone_check_tech = $this->Admin_model->editphone_check_tech($contact_no, $id);
        if (!empty($techid) && !empty($first_name) && !empty($last_name) && !empty($skill) && !empty($email_id) && !empty($contact_no) && !empty($address) && !empty($product) && !empty($category)) {
            $this->form_validation->set_rules('email_id1', 'Email', 'required');
            $this->form_validation->set_rules('email_id1', 'Email', 'valid_email');
            if ($this->form_validation->run() == False) {
                echo "Email should be in correct format";
            } else {
                if ($editemail_check_tech == 1) {
                    echo "Email Id should be unique";
                } else {
                    if ($editphone_check_tech == 1) {
                        echo "Contact Number should be unique";
                    } else {
                        if (empty($acontact_no)) {
                            $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            //$this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[/^[789][0-9]{1,9}$/]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    $data            = array(
                                        'technician_id' => $this->input->post('tech_id1'),
                                        'first_name' => $this->input->post('first_name1'),
                                        'last_name' => $this->input->post('last_name1'),
                                        'image' => $image,
                                        'email_id' => $this->input->post('email_id1'),
                                        'skill_level' => $this->input->post('skill1'),
                                        'contact_number' => $this->input->post('contact_no1'),
                                        'alternate_number' => $this->input->post('acontact_no1'),
                                        'address' => $this->input->post('address1'),
                                        'product' => $this->input->post('product1'),
                                        'category' => $this->input->post('category1')
                                    );
                                    //print_r($data);
                                    $data['records'] = $this->Admin_model->edit_technician($data, $id);
                                    //print_r(json_encode('User added Successfully'));
                                    echo "Technician updated Successfully";
                                }
                            }
                        } else {
                            $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            $this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    $data            = array(
                                        'technician_id' => $this->input->post('tech_id1'),
                                        'first_name' => $this->input->post('first_name1'),
                                        'last_name' => $this->input->post('last_name1'),
                                        'image' => $image,
                                        'email_id' => $this->input->post('email_id1'),
                                        'skill_level' => $this->input->post('skill1'),
                                        'contact_number' => $this->input->post('contact_no1'),
                                        'alternate_number' => $this->input->post('acontact_no1'),
                                        'address' => $this->input->post('address1'),
                                        'product' => $this->input->post('product1'),
                                        'category' => $this->input->post('category1')
                                    );
                                    //print_r($data);
                                    $data['records'] = $this->Admin_model->edit_technician($data, $id);
                                    //print_r(json_encode('User added Successfully'));
                                    echo "Technician updated Successfully";
                                }
                            }
                        }
                    }
                }
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function deletetechnician()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletetechnician($id);
        echo $data;
    }
    public function customer()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records'] = $this->Admin_model->getcustomer1($datas);
            $this->load->view('Customer', $data);
        } else {
            redirect('login/login');
        }
    }
    public function custid_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $datass = $this->Admin_model->custid_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "Cust_" . $datass;
        echo $datass;
    }
    public function insertcustomer()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $cust_id               = $this->input->post('cust_id');
        $cust_name             = $this->input->post('cust_name');
        $email_id              = $this->input->post('email_id');
        $contact_no            = $this->input->post('contact_no');
        $acontact_no           = $this->input->post('acontact_no');
        $address               = $this->input->post('address');
        $product               = $this->input->post('product');
        $category              = $this->input->post('category');
        $mobile_check_customer = $this->Admin_model->mobile_check_customer($contact_no);
        $email_check_customer  = $this->Admin_model->email_check_customer($email_id);
        if (!empty($cust_id) && !empty($cust_name) && !empty($email_id) && !empty($contact_no) && !empty($address) && !empty($product) && !empty($category)) {
            $this->form_validation->set_rules('email_id', 'Email', 'required');
            $this->form_validation->set_rules('email_id', 'Email', 'valid_email');
            if ($this->form_validation->run() == False) {
                echo "Email should be in correct format";
            } else {
                if ($email_check_customer == 1) {
                    echo "Email is already placed, Kindly check it";
                } else {
                    if ($mobile_check_customer == 1) {
                        echo "Contact number is already placed, Kindly check it";
                    } else {
                        if (empty($acontact_no)) {
                            $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            //$this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    $data            = array(
                                        'customer_id' => $this->input->post('cust_id'),
                                        'customer_name' => $this->input->post('cust_name'),
                                        'email_id' => $this->input->post('email_id'),
                                        'contact_number' => $this->input->post('contact_no'),
                                        'alternate_number' => $this->input->post('acontact_no'),
                                        'address' => $this->input->post('address'),
                                        'product_serial_no' => $this->input->post('product'),
                                        'component_serial_no' => $this->input->post('category')
                                    );
                                    //print_r($data);
                                    $data['records'] = $this->Admin_model->insertcustomer($data);
                                    if ($data['records'] == 1) {
                                        echo "Customer added Successfully";
                                    } else {
                                        echo "Customer not added, kindly try again";
                                    }
                                }
                            }
                        } else {
                            $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            $this->form_validation->set_rules('acontact_no', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    $data            = array(
                                        'customer_id' => $this->input->post('cust_id'),
                                        'customer_name' => $this->input->post('cust_name'),
                                        'email_id' => $this->input->post('email_id'),
                                        'contact_number' => $this->input->post('contact_no'),
                                        'alternate_number' => $this->input->post('acontact_no'),
                                        'address' => $this->input->post('address'),
                                        'product_serial_no' => $this->input->post('product'),
                                        'component_serial_no' => $this->input->post('category')
                                    );
                                    //print_r($data);
                                    $data['records'] = $this->Admin_model->insertcustomer($data);
                                    if ($data['records'] == 1) {
                                        echo "Customer added Successfully";
                                    } else {
                                        echo "Customer not added, kindly try again";
                                    }
                                }
                            }
                        }
                    }
                }

            }
        } else {
            echo "All fields are Mandatory";
        }
    }

    public function bulk_spare()
    {
        $result = array();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('PHPExcel');
        $handle   = $_FILES["add_excel"]['tmp_name'];
        $filename = $_FILES["add_excel"]["name"];
        $pieces   = explode(".", $filename);
        if ($pieces[1] == "xlsx") {
            $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel   = $objReader->load($handle);
            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest)
                    return ord(strtolower($dest)) - 96;
                else
                    return 0;
            }
            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }
            for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }
            $e = 2;

            for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                $sheetData = $objPHPExcel->getActiveSheet();
                $cellData1 = myFunction($sheetData, 'A', $e);
                $cellData2 = myFunction($sheetData, 'B', $e);
                $datass    = $this->Admin_model->spareid_check();
                $datass    = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                $datass    = $datass + 1;
                $datass    = str_pad($datass, 4, "0", STR_PAD_LEFT);
                $datass    = "Spare" . $datass;
                $datass    = trim($datass, " ");

                $company_id = $this->session->userdata('companyid');
                // $getdata['data']=$this->Admin_model->getcustdata($cellData1,$cellData2);
                $s_code     = myFunction($sheetData, '0', $e);
                $s_desc     = myFunction($sheetData, 'A', $e);
                $name       = myFunction($sheetData, 'B', $e);
                $modal      = myFunction($sheetData, 'C', $e);
                $source     = myFunction($sheetData, 'D', $e);
                $source=str_replace(" ","_", $source);
                $price      = myFunction($sheetData, 'E', $e);
                $qty        = myFunction($sheetData, 'F', $e);
                $product    = myFunction($sheetData, 'G', $e);
                $category   = myFunction($sheetData, 'H', $e);
                $start_date = myFunction($sheetData, 'I', $e);
                $end_date   = myFunction($sheetData, 'J', $e);
                //$produ_checking = $this->Admin_model->produ_checking($product_check);
                // $subcategory_serials = $this->Admin_model->subcategory_serials($subcategory_serial);

                $data       = implode('","', $content[$makeInsert][0]);
                $datas      = $content[$makeInsert][0];
				 $spare_source= str_replace(" ","_",$datas[4]);
                $data       = array(
                    'spare_id' => $datass,
                    'spare_code' => $datas[0],
                    'spare_desc' => $datas[1],
                    'spare_name' => $datas[2],
                    'spare_modal' => $datas[3],
                    'spare_source' => $spare_source,
                    'price' => $datas[5],
                    'quantity_purchased' => $datas[6],
                    'product' => $datas[7],
                    'category' => $datas[8],
                    'p_date' => $datas[9],
                    'ep_date' => $datas[10],
                    'company_id' => $company_id
                );
                $spare_code = $s_code; //print_r($data);
                $valueing   = $this->Admin_model->bulk_spare($data, $product, $category, $datass, $company_id, $s_code, $s_desc, $name, $modal, $source, $price, $qty, $start_date, $end_date);
                if ($valueing == 1) {
                    $result[] .= "Spare " . $spare_code . " added successfully";

                } else if ($valueing == "category does not exist") {
                    $result[] .= "Category " . $category . " does not exist, Check to it.";
                } else if ($valueing == "product does not exist") {
                    $result[] .= "Product " . $product . " does not exist, Check to it.";
                } else if ($valueing == "already exist") {
                    $result[] .= "Spare already exists.";
                } else {
                    $result[] .= "Error in Upload";
                }

                $e++;
            }
            echo json_encode($result);
        }

    }

    public

    function bulk_customer()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->model('New_amc');
        $this->load->library('PHPExcel');
        $handle = $_FILES["add_customer"]['tmp_name'];
        $filename = $_FILES["add_customer"]["name"];
        $pieces = explode(".", $filename);
        $c_id = $this->session->userdata('companyid');
        if ($pieces[1] == "xlsx")
        {
            $objReader = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel = $objReader->load($handle);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest) return ord(strtolower($dest)) - 96;
                else return 0;
            }

            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }

            for ($getContent = 2; $getContent <= $highestRow; $getContent++)
            {
                $rowData = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }

            $e = 2;
            $result = array();
            for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++)
            {
                $sheetData = $objPHPExcel->getActiveSheet();
                $cellData1 = myFunction($sheetData, 'A', $e);
                $cellData2 = myFunction($sheetData, 'B', $e);
                $data = implode('","', $content[$makeInsert][0]);
                $datas = $content[$makeInsert][0];

                // $getdata['data']=$this->Admin_model->getcustdata($cellData1,$cellData2);

                
                $name = myFunction($sheetData, '0', $e);
                $email_id = myFunction($sheetData, 'A', $e);
                $contact = myFunction($sheetData, 'B', $e);
                $alt_num = myFunction($sheetData, 'C', $e);
                $door_no = myFunction($sheetData, 'D', $e);
                $street = myFunction($sheetData, 'E', $e);
                $town = myFunction($sheetData, 'F', $e);
                $city = myFunction($sheetData, 'G', $e);
                $state = myFunction($sheetData, 'H', $e);
                $country = myFunction($sheetData, 'I', $e);
                $product_check = myFunction($sheetData, 'J', $e);
                $modal_no = myFunction($sheetData, 'K', $e);
                $serial_no = myFunction($sheetData, 'L', $e);
                $subcategory = myFunction($sheetData, 'M', $e);
                $pincode = myFunction($sheetData, 'N', $e);
                $priority = myFunction($sheetData, 'O', $e);
                $support_type = myFunction($sheetData, 'P', $e);
                $start_date = myFunction($sheetData, 'Q', $e);
                $contract_duration = myFunction($sheetData, 'R', $e);
                
                $mobile_customer = $datas[2];
                if (!preg_match('/^[1-9][0-9]{0,15}$/', $mobile_customer))
                {
                    array_push($result, ("Mobile " . $datas[2] . " is not a valid Mobile number"));
                }

                $email = filter_var($datas[1], FILTER_SANITIZE_EMAIL);

                // Validate e-mail

                if (filter_var($email, FILTER_VALIDATE_EMAIL))
                {

                    // start

                    $email_check = $this->Admin_model->email_check_customer($email_id, $c_id);
                    if ($email_check == 0)
                    {

                        // start2

                        $mobile_check = $this->Admin_model->mobile_check_customer($mobile_customer, $c_id);
                        if ($mobile_check == 0)
                        {
                            $product_array = $this->Admin_model->produ_checking1($datas[10], $c_id);
                            $product = $product_array['product_id'];
                            if (empty($product_array))
                            {
                                array_push($result, ("Invalid Product-Category " . $datas[10]));
                            }
                            else
                            {
                                $category_array = $this->Admin_model->subcategory_serials1($datas[13], $product, $c_id);
                                $category = $category_array['cat_id'];
                                if (empty($category_array))
                                {
                                    array_push($result, ("Invalid Sub-Category " . $datas[13]));
                                }
                                else
                                {
                                    if ($modal_no == "" || $serial_no == "")
                                    {
                                        array_push($result, ("Model/Serial No. is mandatory for " . $datas[1] . "'s product"));
                                    }
                                    else
                                    {
                                       /* if ($start_date >= $warrenty_date)
                                        {
                                            array_push($result, ("Contract date should be Greater than start date "));
                                        }
                                        else
                                        {*/
                                            $get_cust_id = $this->Admin_model->getcustid($datas[1], $datas[2]);
                                            if ($get_cust_id == '0')
                                            {
                                                $datass = $this->Admin_model->custid_check();
                                                $datass = intval(preg_replace('/[^0-9]+/', '', $datass) , 10);
                                                $datass = $datass + 1;
                                                $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                                                $datass = "Cust_" . $datass;
                                                $customer_id = trim($datass, " ");
                                            }
                                            else
                                            {
                                                $customer_id = $get_cust_id;
                                            }
                                             $contractdetailsarray=$this->New_amc->getcontractdetails($c_id,$datas[16]);
            $contractvalue='';
            $contract_period='';
            $warrantyexpirydate='';
            $end_date='';
           // print_r()
         foreach($contractdetailsarray as $value)
          {
            $contractvalue=$value['contract_amount'];
            $contract_period=(int)$value['contract_period'];
            //echo $contract_period;
            //$warrantyexpirydate=date($datas[17], strtotime("+".$contract_period." months"));
            $warrantyexpirydate=date('Y-m-d', strtotime("+".$contract_period." months", strtotime($datas[17])));
           
          }
         
          $end_date = date('Y-m-d', strtotime("+".$contract_duration." months", strtotime($start_date)));
       if(count($contractdetailsarray)==0)
        {
        array_push($result, ("Enter proper Contract type for customer ".$datas[0]));
        }
        else
        {
                                            $data = array(
                                                'customer_id' => $customer_id,
                                                'customer_name' => $datas[0],
                                                'email_id' => $datas[1],
                                                'contact_number' => $datas[2],
                                                'alternate_number' => $datas[3],
                                                'door_num' => $datas[4],
                                                'address' => $datas[5],
                                                'cust_town' => $datas[6],
                                                'city' => $datas[7],
                                                'state' => $datas[8],
                                                'cust_country' => $datas[9],
                                                'product_serial_no' => $product,
                                                'model_no' => $datas[11],
                                                'serial_no' => $datas[12],
                                                'component_serial_no' => $category,
                                                'pincode' => $datas[14],
                                                'priority' => $datas[15],
                                                'type_of_contract' => $datas[16],
                                                'contract_value' =>$contractvalue,
                                                'warrenty_expairy_date' =>  $warrantyexpirydate,
                                                'start_date' => $datas[17],
                                                //'end_date' => $warrantyexpirydate,
                                                'end_date'=> $end_date,
                                                'company_id' => $c_id
                                            );

                                            // print_r($data);
 
                                            $valueing = $this->Admin_model->insertcustomer($data);
                    $updatestatus=$this->New_amc->updatecontractdetails($customer_id,$c_id);
                                            array_push($result, ("Customer " . $datas[0] . " added successfully"));
                                        }
                                        //}
                                    }
                                }
                            }
                        }
                        else
                        {
                            array_push($result, ("The Mobile No " . $datas[2] . " already exist!!!"));
                        }

                        // end2

                    }
                    else
                    {
                        array_push($result, ("Email " . $datas[1] . " already exist!!!"));
                    }

                    // end

                }
                else
                {
                    array_push($result, ("Email " . $datas[1] . " is not a valid email"));
                }

                $e++;
            }
        }

        echo json_encode($result);
    }

    public function submit_customer()
    {
        $msg;
        $this->load->helper('url');
        $this->load->database();
        $id      = $this->input->post('table_id');
        $cust_id = $this->input->post('edit_id');
        $name    = $this->input->post('edit_name');
        $mail    = $this->input->post('edit_mail');
        $contact = $this->input->post('edit_contact');
        $anum    = $this->input->post('edit_anumber');
        $door    = $this->input->post('edit_door');
        $str     = $this->input->post('edit_str');
        $town    = $this->input->post('edit_town');
        $country = $this->input->post('edit_country');
        $pin     = $this->input->post('edit_pin');
        $data    = array(
            'customer_id' => $cust_id,
            'customer_name' => $name,
            'email_id' => $mail,
            'contact_number' => $contact,
            'alternate_number' => $anum,
            'door_num' => $door,
            'address' => $str,
            'cust_town' => $town,
            'cust_country' => $country,
            'pincode' => $pin
        );
        if (empty($cust_id) && empty($name) && empty($mail) && empty($contact) && empty($anum) && empty($door) && empty($str) && empty($town) && empty($country) && empty($pin)) {
            echo "All Fields are mandatory";
        } else {
            $this->load->model('Admin_model');
            $result = $this->Admin_model->submit_customer($data, $id);
            if ($result) {
                echo "Customer Details Updated";
            } else {
                echo "Something went wrong,Try again.";
            }
        }
    }

    public function getdetails_customer()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_customer($data);
        print_r(json_encode($data));
    }
    public function getdetails_contract()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data   = $this->input->post('id');
        $result = $this->Admin_model->getdetails_contract($data);
        echo json_encode($result);
    }
    public function edit_customer()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id                       = $this->input->post('id1');
        $custid                   = $this->input->post('cust_id1');
        $cust_name                = $this->input->post('cust_name1');
        $email_id                 = $this->input->post('email_id1');
        $contact_no               = $this->input->post('contact_no1');
        $acontact_no              = $this->input->post('acontact_no1');
        $door_num                 = $this->input->post('door_no1');
        $address                  = $this->input->post('street1');
        $cust_town                = $this->input->post('town1');
        $cust_country             = $this->input->post('country1');
        $pin_code                 = $this->input->post('pin_code1');
        $product                  = $this->input->post('product1');
        $category                 = $this->input->post('category1');
        $m_number                 = $this->input->post('m_number');
        $s_number                 = $this->input->post('s_number');
        $expairy_date             = $this->input->post('expairy_date');
        $contract_id              = $this->input->post('contract_id');
        $support_type             = $this->input->post('support_type');
        $contract_value           = $this->input->post('contract_value');
        $start_dates              = $this->input->post('start_dates');
        $end_dates                = $this->input->post('end_dates');
        $editemail_check_customer = $this->Admin_model->editemail_check_customer($email_id, $id);
        $editphone_check_customer = $this->Admin_model->editphone_check_customer($contact_no, $id);
        if (!empty($custid) && !empty($cust_name) && !empty($email_id) && !empty($contact_no) && !empty($door_num) && !empty($product) && !empty($category) && !empty($m_number) && !empty($s_number) && !empty($expairy_date) && !empty($contract_id) && !empty($support_type) && !empty($contract_value) && !empty($start_dates) && !empty($end_dates)) {
            $this->form_validation->set_rules('email_id1', 'Email', 'required');
            $this->form_validation->set_rules('email_id1', 'Email', 'valid_email');
            if ($this->form_validation->run() == False) {
                echo "Email should be in correct format";
            } else {
                if ($editemail_check_customer == 1) {
                    echo "Email Id should be unique";
                } else {
                    if ($editphone_check_customer == 1) {
                        echo "Contact Number should be unique";
                    } else {
                        if (empty($acontact_no)) {
                            $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            //$this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $start_dates) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $end_dates) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $expairy_date)) {
                                        if ($start_dates <= $end_dates) {
                                            $data            = array(
                                                'customer_id' => $custid,
                                                'customer_name' => $cust_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'door_num' => $door_num,
                                                'address' => $address,
                                                'cust_town' => $cust_town,
                                                'cust_country' => $cust_country,
                                                'product_serial_no' => $product,
                                                'component_serial_no' => $category,
                                                'model_no' => $m_number,
                                                'serial_no' => $s_number,
                                                'warrenty_expairy_date' => $expairy_date,
                                                'contract_id' => $contract_id,
                                                'type_of_contract' => $support_type,
                                                'contract_value' => $contract_value,
                                                'start_date' => $start_dates,
                                                'end_date' => $end_dates
                                            );
                                            /* $data=array(
                                            'customer_id'=>$this->input->post('cust_id1'),
                                            'customer_name'=>$this->input->post('cust_name1'),
                                            'email_id'=>$this->input->post('email_id1'),
                                            'contact_number'=>$this->input->post('contact_no1'),
                                            'alternate_number'=>$this->input->post('acontact_no1'),
                                            'door_num'=>$this->input->post('door_no1'),
                                            'address'=>$this->input->post('street1'),
                                            'cust_town'=>$this->input->post('town1'),
                                            'cust_country'=>$this->input->post('country1'),
                                            'product_serial_no'=>$this->input->post('product1'),
                                            'component_serial_no'=>$this->input->post('category1'),
                                            ); */
                                            //print_r($data);
                                            $data['records'] = $this->Admin_model->edit_customer($data, $id);
                                            //print_r(json_encode('User added Successfully'));
                                            echo "customer updated Successfully";
                                        } else {
                                            echo "End Date should not be less than Start Date";
                                        }
                                    } else {
                                        echo "Kindly check Start Date, End Date and warranty Expiry Date";
                                    }
                                }
                            }
                        } else {
                            $this->form_validation->set_rules('contact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            $this->form_validation->set_rules('acontact_no1', 'Mobile Number', 'required|regex_match[^(?:[0-9]|\+)(?!.*-.*-)(?:\d(?:-)?){9,10}$^]'); //{10} for 10 digits number
                            if ($this->form_validation->run() == False) {
                                echo "Phone number and contact number should be in correct format";
                            } else {
                                if ($contact_no == $acontact_no) {
                                    echo "Contact Number and Alternative contact number should not be match";
                                } else {
                                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $start_dates) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $end_dates) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $expairy_date)) {
                                        if ($start_dates <= $end_dates) {
                                            $data            = array(
                                                'customer_id' => $custid,
                                                'customer_name' => $cust_name,
                                                'email_id' => $email_id,
                                                'contact_number' => $contact_no,
                                                'alternate_number' => $acontact_no,
                                                'door_num' => $door_num,
                                                'address' => $address,
                                                'cust_town' => $cust_town,
                                                'cust_country' => $cust_country,
                                                'product_serial_no' => $product,
                                                'component_serial_no' => $category,
                                                'model_no' => $m_number,
                                                'serial_no' => $s_number,
                                                'warrenty_expairy_date' => $expairy_date,
                                                'contract_id' => $contract_id,
                                                'type_of_contract' => $support_type,
                                                'contract_value' => $contract_value,
                                                'start_date' => $start_dates,
                                                'end_date' => $end_dates
                                            );
                                            /* $data=array(
                                            'customer_id'=>$this->input->post('cust_id1'),
                                            'customer_name'=>$this->input->post('cust_name1'),
                                            'email_id'=>$this->input->post('email_id1'),
                                            'contact_number'=>$this->input->post('contact_no1'),
                                            'alternate_number'=>$this->input->post('acontact_no1'),
                                            'door_num'=>$this->input->post('door_no1'),
                                            'address'=>$this->input->post('street1'),
                                            'cust_town'=>$this->input->post('town1'),
                                            'cust_country'=>$this->input->post('country1'),
                                            'product_serial_no'=>$this->input->post('product1'),
                                            'component_serial_no'=>$this->input->post('category1'),
                                            ); */
                                            //print_r($data);
                                            $data['records'] = $this->Admin_model->edit_customer($data, $id);
                                            //print_r(json_encode('User added Successfully'));
                                            echo "customer updated Successfully";
                                        } else {
                                            echo "End Date should not be less than Start Date";
                                        }
                                    } else {
                                        echo "Kindly check Start Date, End Date and warranty Expiry Date";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function deletecustomer()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletecustomer($id);
        echo $data;
    }
    public function contracts()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['record']  = $this->Admin_model->getproduct();
            $data['recor']   = $this->Admin_model->getsubproduct();
            $data['records'] = $this->Admin_model->getcustomer();
            $this->load->view('Contracts', $data);
        } else {
            redirect('login/login');
        }
    }
    public function attendance()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records'] = $this->Admin_model->getattendance();
            $this->load->view('attendance_admin', $data);
        } else {
            redirect('login/login');
        }
    }
    public function product()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records']       = $this->Admin_model->getproduct($datas);
            //$data['records2']=$this->Admin_model->getproduct();
            $data['records2']      = $this->Admin_model->getsubproduct($datas);
            $data['records_spare'] = $this->Admin_model->getspare($datas);
            $data['record']        = $this->Admin_model->getproduct($datas);
            $this->load->view('product', $data);
        } else {
            redirect('login/login');
        }
    }
    public function productid_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $datass = $this->Admin_model->productid_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "product_" . $datass;
        echo $datass;
    }
    public function insert_imageforspare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $image_file = $this->input->post('add_image');
        $scode      = $this->input->post('spar_code');
        $spare      = $_FILES["add_image"];
        $output_dir = "./assets/images/";
        $fileName   = $_FILES["add_image"]["name"];
        $baseurl    = base_url() . 'assets/images/';
        $data       = array(
            'image' => $baseurl . $fileName
        );
        $record     = $this->Admin_model->insert_imageforspare($data, $scode);
        if ($record == 1) {
            move_uploaded_file($_FILES["add_image"]["tmp_name"], $output_dir . $fileName);
            echo "Image added Successfully";
        } else {
            echo "Image not added, kindly try again";
        }
    }
    public function insertproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $product_id    = $this->input->post('product_id');
        $product_name  = $this->input->post('product_name');
        $company       = $this->input->post('company');
        $product_modal = $this->input->post('product_modal');
        $product_desc  = $this->input->post('product_desc');
        if (!empty($product_id) && !empty($product_name) && !empty($company) && !empty($product_desc)) {    
            $datas=$this->Admin_model->check_productss($product_name,$company);
            if($datas){
                echo "Already this product name is present, Kindly check it!";
            }
            else{

                $product_image   = $_FILES["product_image"];
                $output_dir      = "./assets/images/";
                $fileName        = $_FILES["product_image"]["name"];
                $baseurl         = base_url() . 'assets/images/';
                $data            = array(
                    'product_id' => $this->input->post('product_id'),
                    'product_name' => trim($this->input->post('product_name')),
                    'product_image' => $baseurl . $product_id,
                    'company_id' => $this->input->post('company'),
                    'product_modal' => trim($this->input->post('product_modal')),
                    'product_desc' => trim($this->input->post('product_desc'))
                );
                $data['records'] = $this->Admin_model->insertproduct($data);
                if ($data['records'] == 1) {
                    move_uploaded_file($_FILES["product_image"]["tmp_name"], $output_dir . $product_id);
                    echo "product added Successfully";
                } else {
                    echo "product not added, kindly try again";
                }
            }          
        } else {
            echo "All fields are Mandatory";
        }
        /*$data=array(
        'product_id'=>$this->input->post('product_id'),
        'product_name'=>$this->input->post('product_name'),
        'product_image'=>$baseurl.$fileName,
        'company_id'=>$this->input->post('company'),
        );*/
        /*$this->load->library('form_validation');
        $product_id=$this->input->post('product_id');
        $product_name=$this->input->post('product_name');
        $company=$this->input->post('company');
        if(!empty($product_id) && !empty($product_name) && !empty($company) ){
        $product_image=$_FILES["product_image"];
        $output_dir = "./assets/images/";
        $fileName = $_FILES["product_image"]["name"];
        $baseurl = base_url().'assets/images/';
        $data=array(
        'product_id'=>$this->input->post('product_id'),
        'product_name'=>$this->input->post('product_name'),
        'product_image'=>$baseurl.$fileName,
        'company_id'=>$this->input->post('company'),
        );
        $data['records']=$this->Admin_model->insertproduct($data);
        if($data['records']==1){
        move_uploaded_file($_FILES["e_idproof"]["tmp_name"],$output_dir.$fileName);
        echo "product added Successfully";
        }else{
        echo "product not added, kindly try again";
        }
        }else{
        echo "All fields are Mandatory";
        }*/
    }
    public function bulk_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('Excel');
        $handle   = $_FILES["add_product"]['tmp_name'];
        $filename = $_FILES["add_product"]["name"];
        $pieces   = explode(".", $filename);
        if ($pieces[1] == "xlsx") {
            $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
            $objPHPExcel   = $objReader->load($handle);
            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestDataColumn();
            function toNumber($dest)
            {
                if ($dest)
                    return ord(strtolower($dest)) - 96;
                else
                    return 0;
            }
            function myFunction($s, $x, $y)
            {
                $x = toNumber($x);
                return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
            }
            for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                $content[] = $rowData;
            }
            $e      = 2;
            $result = "";
            for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {

                $sheetData = $objPHPExcel->getActiveSheet();
                $cellData  = myFunction($sheetData, 'A', $e);
                $cellData1 = myFunction($sheetData, 'B', $e);
                $cellData2 = myFunction($sheetData, 'C', $e);
                $datass    = $this->Admin_model->productid_check();
                $datass    = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                $datass    = $datass + 1;
                $datass    = str_pad($datass, 4, "0", STR_PAD_LEFT);
                $datass    = "product_" . $datass;
                $datass    = trim($datass, " ");
                $data      = implode('","', $content[$makeInsert][0]);
                $datas     = $content[$makeInsert][0];
                /*foreach ($objWorksheet->getDrawingCollection() as $drawing) {
                //for XLSX format
                $string = $drawing->getCoordinates();
                $coordinate = PHPExcel_Cell::coordinateFromString($string);
                if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
                $image = $drawing->getImageResource();
                // save image to disk
                $renderingFunction = $drawing->getRenderingFunction();
                switch ($renderingFunction) {
                case PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG:
                imagejpeg($image, 'uploads/' . $drawing->getIndexedFilename());
                break;
                case PHPExcel_Worksheet_MemoryDrawing::RENDERING_GIF:
                imagegif($image, 'uploads/' . $drawing->getIndexedFilename());
                break;
                case PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG:
                case PHPExcel_Worksheet_MemoryDrawing::RENDERING_DEFAULT:
                imagepng($image, 'uploads/' . $drawing->getIndexedFilename());
                break;
                }
                }
                }*/
                $data      = array(
                    'product_id' => $datass,
                    'product_name' => $datas[0],
                    'company_id' => $datas[1]
                );
                //print_r($data);
                $valueing  = $this->Admin_model->insertproduct($data);
                $result    = "User added successfully";
                $e++;
            }
            echo $result;
            /* if($valueing==1){
            $result.="Values are uploaded";
            }else{
            $result.="Values are not uploaded, Kindly checkit and try again";
            } */
            //echo $valueing;
        }
    }
    public function edit_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id             = $this->input->post('id1');
        $product_id1    = $this->input->post('product_id1');
        $product_name  = $this->input->post('product_name1');
        $product_modal1 = $this->input->post('product_modal1');
        $product_desc1  = $this->input->post('product_desc1');
        $company1       = $this->input->post('companyid1');
        if (!empty($product_id1) && !empty($product_name) && !empty($company1)) {
            $datas=$this->Admin_model->check_productss($product_name,$company1);
            // if($datas == 1){
            //     echo "Product Name already exists!";
            // }
            // else{
                $product_image1  = $_FILES["product_image1"];
                $output_dir      = "./assets/images/";
                $fileName        = $_FILES["product_image1"]["name"];
                $baseurl         = base_url() . 'assets/images/';
                $data            = array(
                    'product_id' => $this->input->post('product_id1'),
                    'product_name' => trim($product_name),
                    'product_modal' => trim($this->input->post('product_modal1')),
                    'product_desc' => trim($this->input->post('product_desc1')),
                    'product_image' => $baseurl . $product_id1,
                    'company_id' => $this->input->post('companyid1')
                );
                $data['records'] = $this->Admin_model->edit_product($data, $id);
                if ($data['records'] == 1) {
                    move_uploaded_file($_FILES["product_image1"]["tmp_name"], $output_dir . $product_id1);
                    echo "Product updated Successfully";
                } else {
                    echo "Product not updated, kindly try again";
                }
           // }            
           
        } else {
            echo "All fields are Mandatory";
        }
    }

    public function edit_product1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id             = $this->input->post('id1');
        $product_id1    = $this->input->post('product_id1');
        $product_name  = $this->input->post('product_name1');
        $product_modal1 = $this->input->post('product_modal1');
        $product_desc1  = $this->input->post('product_desc1');
        $company1       = $this->input->post('companyid1');
        if (!empty($product_id1) && !empty($product_name) && !empty($company1)) {
            $datas=$this->Admin_model->check_productss($product_name,$company1);
            // if($datas == 1){
            //     echo "Product Name already exists!";
            // }
            // else{                
                $data            = array(
                    'product_id' => $this->input->post('product_id1'),
                    'product_name' => trim($product_name),
                    'product_modal' => trim($this->input->post('product_modal1')),
                    'product_desc' => trim($this->input->post('product_desc1')),
                    'company_id' => $this->input->post('companyid1')
                );
                //print_r($data);
                $data['records'] = $this->Admin_model->edit_product($data, $id);
                if ($data['records'] == 1) {
                    ;
                    echo "Product updated Successfully";
                } else {
                    echo "Product not updated, kindly try again";
                }       
            //}
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function getdetails_product()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_product($data);
        print_r(json_encode($data));
    }
    public function deleteproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $datas=$this->Admin_model->del_prod($id);
        if($datas){
            $data = $this->Admin_model->deleteproduct($id);
            echo $data;
        }
    }
    public function get_subproduct_details()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->get_subproduct_details($id);
        print_r(json_encode($data));
    }
    public function subproduct()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records'] = $this->Admin_model->getsubproduct();
            $data['record']  = $this->Admin_model->getproduct();
            $this->load->view('subproduct', $data);
        } else {
            redirect('login/login');
        }
    }
    public function subproductid_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $datass = $this->Admin_model->subproductid_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "subcategory_" . $datass;
        echo $datass;
    }
    public function insertsubproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $subproduct_id    = $this->input->post('subproduct_id');
        $subproduct_name  = $this->input->post('subproduct_name');
        $subproduct_modal = $this->input->post('subproduct_modal');
        $subproduct_desc  = $this->input->post('subproduct_desc');
        $product_name     = $this->input->post('product_name');
        $companyid        = $this->input->post('companyid');
        if (!empty($subproduct_id) && !empty($product_name) && !empty($subproduct_name) && !empty($subproduct_desc)) {
            $product_image   = $_FILES["subproduct_image"];
            $output_dir      = "./assets/images/";
            $fileName        = $_FILES["subproduct_image"]["name"];
            $baseurl         = base_url() . 'assets/images/';
            $data            = array(
                'cat_id' => $this->input->post('subproduct_id'),
                'prod_id' => $this->input->post('product_name'),
                'cat_name' => trim($this->input->post('subproduct_name')),
                'cat_modal' => trim($this->input->post('subproduct_modal')),
                'cat_desc' => trim($this->input->post('subproduct_desc')),
                'cat_image' => $baseurl . $subproduct_id,
                'company_id' => $companyid
            );
            //print_r($data);
            $data['records'] = $this->Admin_model->insertsubproduct($data);
            if ($data['records'] == 1) {
                move_uploaded_file($_FILES["subproduct_image"]["tmp_name"], $output_dir . $subproduct_id);
                echo true;
            } else {
                echo "Sub category not added, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }

    public function insertsubproduct_new()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $subproduct_id    = $this->input->post('subproduct_id');
        $subproduct_name  = $this->input->post('subproduct_name');
        $subproduct_modal = $this->input->post('subproduct_modal');
        $subproduct_desc  = $this->input->post('subproduct_desc');
        $product_name     = $this->input->post('product_name');
        $companyid        = $this->input->post('companyid');
        if (!empty($subproduct_id) && !empty($product_name) && !empty($subproduct_name) && !empty($subproduct_desc)) {
            $product_image   = $_FILES["subproduct_image_new"];
            $output_dir      = "./assets/images/";
            $fileName        = $_FILES["subproduct_image_new"]["name"];
            $baseurl         = base_url() . 'assets/images/';
            $data            = array(
                'cat_id' => $this->input->post('subproduct_id'),
                'prod_id' => $this->input->post('product_name'),
                'cat_name' => trim($this->input->post('subproduct_name')),
                'cat_modal' => trim($this->input->post('subproduct_modal')),
                'cat_desc' => trim($this->input->post('subproduct_desc')),
                'cat_image' => $baseurl . $subproduct_id,
                'company_id' => $companyid
            );
            //print_r($data);
            $data['records'] = $this->Admin_model->insertsubproduct($data);
            if ($data['records'] == 1) {
                move_uploaded_file($_FILES["subproduct_image_new"]["tmp_name"], $output_dir . $subproduct_id);
                echo true;
            } else {
                echo "Sub category not added, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    
    public function getdetails_subproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_subproduct($data);
        print_r(json_encode($data));
    }
    public function edit_subproducts()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id                = $this->input->post('id1');
        $subproduct_id1    = $this->input->post('subproduct_id1');
        $product_name1     = $this->input->post('product_name1');
        $subproduct_name1  = $this->input->post('subproduct_name1');
        $subproduct_modal1 = $this->input->post('subproduct_modal1');
        $subproduct_desc1  = $this->input->post('subproduct_desc1');
        if (!empty($subproduct_id1) && !empty($product_name1) && !empty($subproduct_name1)) {
            $subproduct_image1 = $_FILES["subproduct_image1"];
            $output_dir        = "./assets/images/";
            $fileName          = $_FILES["subproduct_image1"]["name"];
            $baseurl           = base_url() . 'assets/images/';
            $data              = array(
                'cat_id' => $this->input->post('subproduct_id1'),
                'prod_id' => $this->input->post('product_name1'),
                'cat_name' => trim($this->input->post('subproduct_name1')),
                'cat_image' => trim($this->input->post('subproduct_modal1')),
                'cat_desc' => trim($this->input->post('subproduct_desc1')),
                'cat_image' => $baseurl . $subproduct_id1
            );
            $data['records']   = $this->Admin_model->edit_subproducts($data, $id);
            if ($data['records'] == 1) {
                move_uploaded_file($_FILES["subproduct_image1"]["tmp_name"], $output_dir . $subproduct_id1);
                echo "Sub category updated Successfully";
            } else {
                echo "Sub category not updated, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function edit_subproducts1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id                = $this->input->post('id1');
        $subproduct_id1    = $this->input->post('subproduct_id1');
        $product_name1     = $this->input->post('product_name1');
        $subproduct_name1  = $this->input->post('subproduct_name1');
        $subproduct_modal1 = $this->input->post('subproduct_modal1');
        $subproduct_desc1  = $this->input->post('subproduct_desc1');
        if (!empty($subproduct_id1) && !empty($product_name1) && !empty($subproduct_name1)) {
            $data            = array(
                'cat_id' => $this->input->post('subproduct_id1'),
                'prod_id' => $this->input->post('product_name1'),
                'cat_name' => trim($this->input->post('subproduct_name1')),
                'cat_modal' => trim($this->input->post('subproduct_modal1')),
                'cat_desc' => trim($this->input->post('subproduct_desc1'))
            );
            $data['records'] = $this->Admin_model->edit_subproducts($data, $id);
            if ($data['records'] == 1) {
                echo "Sub category updated Successfully";
            } else {
                echo "Sub category not updated, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function deletesubproduct()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletesubproduct($id);
        echo $data;
    }
    public function spare()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records'] = $this->Admin_model->getspare($datas);
            $data['record']  = $this->Admin_model->getproduct($datas);
            $data['recor']   = $this->Admin_model->getsubproduct($datas);
            $data['spare_loc']   = $this->Admin_model->get_spareloc($datas);
            $this->load->view('Spare_Management', $data);
        } else {
            redirect('login/login');
        }
    }
    public function spareid_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $datass = $this->Admin_model->spareid_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "Spare_" . $datass;
        echo $datass;
    }
    public function getcategory()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data   = $this->input->post('product_id');
        $datass = $this->Admin_model->getcategory($data);
        echo json_encode($datass);
    }
    public function insertspare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $spareid      = $this->input->post('spareid');
        $spare_code   = $this->input->post('spare_code');
        $spare_name   = $this->input->post('spare_name');
        $spare_modal  = $this->input->post('spare_modal');
        $spare_desc   = $this->input->post('spare_desc');
        $spare_source = $this->input->post('spare_source');
        $price        = $this->input->post('price');
        $p_date       = $this->input->post('p_date');
        $ep_date      = $this->input->post('ep_date');
        $q_purchase   = $this->input->post('q_purchase');
        //$q_used=$this->input->post('q_used');
        $product      = $this->input->post('product');
        $category     = $this->input->post('category');
        //$quantity=$this->input->post('quantity');
        $company_id   = $this->input->post('company_id');
        if (!empty($spareid) && !empty($spare_code) && !empty($spare_name) && !empty($spare_modal) && !empty($spare_desc) && !empty($product) && !empty($category) && !empty($company_id)) {
            $this->load->library('form_validation');
            /*$this->form_validation->set_rules('price', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            $this->form_validation->set_rules('q_purchase', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            if ($this->form_validation->run() == False)
            {
            echo "price, Quantity purchased and Quantity should be in correct format";
            }else{
            }   */
            $spare_image     = $_FILES["spare_image"];
            $output_dir      = "./assets/images/";
            $fileName        = $_FILES["spare_image"]["name"];
            $baseurl         = base_url() . 'assets/images/';

            $spare_code= str_replace(" ","_",$this->input->post('spare_code'));
            $data            = array(
                'spare_id' => $this->input->post('spareid'),
                'spare_code' => $spare_code,
                'spare_name' => $this->input->post('spare_name'),
                'spare_modal' => $this->input->post('spare_modal'),
                'spare_desc' => $this->input->post('spare_desc'),
                'image' => $baseurl . $spareid,
                'spare_source' => $this->input->post('spare_source'),
                'price' => $this->input->post('price'),
                'p_date' => $this->input->post('p_date'),
                'ep_date' => $this->input->post('ep_date'),
                'quantity_purchased' => $this->input->post('q_purchase'),
                //'quantity_used'=>$this->input->post('q_used'),
                'product' => $this->input->post('product'),
                'category' => $this->input->post('category'),
                'quantity' => $this->input->post('quantity'),
                'company_id' => $this->input->post('company_id')
            );
           
            $data['records'] = $this->Admin_model->insertspare($data);
            if ($data['records'] == 1) {
                move_uploaded_file($_FILES["spare_image"]["tmp_name"], $output_dir . $spareid);
                echo "Spare added Successfully";
            } else {
                echo "Spare not added, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }

    public function insertspare1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $spareid      = $this->input->post('spareid');
        $spare_code   = $this->input->post('spare_code');
        $spare_name   = $this->input->post('spare_name');
        $spare_modal  = $this->input->post('spare_modal');
        $spare_desc   = $this->input->post('spare_desc');
        $spare_source = $this->input->post('spare_source');
        $price        = $this->input->post('price');
        $p_date       = $this->input->post('p_date');
        $ep_date      = $this->input->post('ep_date');
        $q_purchase   = $this->input->post('q_purchase');
        //$q_used=$this->input->post('q_used');
        $product      = $this->input->post('product');
        $category     = $this->input->post('category');
        //$quantity=$this->input->post('quantity');
        $company_id   = $this->input->post('company_id');
        if (!empty($spareid) && !empty($spare_code) && !empty($spare_name) && !empty($spare_modal) && !empty($spare_desc) && !empty($product) && !empty($category) && !empty($company_id)) {
            $this->load->library('form_validation');

            $baseurl = base_url() . 'assets/images/nospareimage.png';
            $spare_code= str_replace(" ","_",$this->input->post('spare_code'));
            $data            = array(
                'spare_id' => $this->input->post('spareid'),
                'spare_code' => $spare_code,
                'spare_name' => $this->input->post('spare_name'),
				'image' => $baseurl,
                'spare_modal' => $this->input->post('spare_modal'),
                'spare_desc' => $this->input->post('spare_desc'),
                'spare_source' => $this->input->post('spare_source'),
                'price' => $this->input->post('price'),
                'p_date' => $this->input->post('p_date'),
                'ep_date' => $this->input->post('ep_date'),
                'quantity_purchased' => $this->input->post('q_purchase'),
                //'quantity_used'=>$this->input->post('q_used'),
                'product' => $this->input->post('product'),
                'category' => $this->input->post('category'),
                'quantity' => $this->input->post('quantity'),
                'company_id' => $this->input->post('company_id')
            );
            //print_r($data);
            $data['records'] = $this->Admin_model->insertspare($data);
            if ($data['records'] == 1) {
                echo "Spare added Successfully";
            } else {
                echo "Spare not added, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }

    public function getdetails_sparecode()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data   = $this->input->post('id');
        $com    = $this->input->post('company_id');
        $result = $this->Admin_model->getdetails_sparecode($data, $com);
        echo json_encode($result);
    }

    public function getdetails_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_spare($data);
        print_r(json_encode($data));
    }
    public function edit_spare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id1           = $this->input->post('id1');
        $spareid1      = $this->input->post('spareid1');
        $spare_code1   = $this->input->post('spare_code1');
        $spare_modal1  = $this->input->post('spare_modal1');
        $spare_desc1   = $this->input->post('spare_desc1');
        $spare_name1   = $this->input->post('spare_name1');
        $spare_source1 = $this->input->post('spare_source1');
        $price1        = $this->input->post('price1');
        $q_purchase1   = $this->input->post('q_purchase1');
        $product1      = $this->input->post('product1');
        $category1     = $this->input->post('category1');
        //$quantity1=$this->input->post('quantity1');
        $company_id1   = $this->input->post('company_id1');
        if (!empty($id1) && !empty($spareid1) && !empty($spare_code1) && !empty($spare_modal1) && !empty($spare_desc1) && !empty($spare_name1) && !empty($spare_source1) && !empty($price1) && !empty($q_purchase1) && !empty($product1) && !empty($category1) && !empty($company_id1)) {
            $this->form_validation->set_rules('price1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            $this->form_validation->set_rules('q_purchase1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            //$this->form_validation->set_rules('quantity1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            if ($this->form_validation->run() == False) {
                echo "price, Quantity purchased and Quantity should be in correct format";
            } else {
                $spare_image     = $_FILES["spare_image1"];
                $output_dir      = "./assets/images/";
                $fileName        = $_FILES["spare_image1"]["name"];
                $baseurl         = base_url() . 'assets/images/';
                $data            = array(
                    'spare_id' => $this->input->post('spareid1'),
                    'spare_code' => $this->input->post('spare_code1'),
                    'spare_modal' => $this->input->post('spare_modal1'),
                    'spare_desc' => $this->input->post('spare_desc1'),
                    'spare_name' => $this->input->post('spare_name1'),
                    'image' => $baseurl . $spareid1,
                    'spare_source' => $this->input->post('spare_source1'),
                    'price' => $this->input->post('price1'),
                    'quantity_purchased' => $this->input->post('q_purchase1'),
                    'product' => $this->input->post('product1'),
                    'category' => $this->input->post('category1'),
                    //'quantity'=>$this->input->post('quantity1'),
                    'company_id' => $this->input->post('company_id1')
                );
                //print_r($data);
                $data['records'] = $this->Admin_model->edit_spare($data, $id1);
                if ($data['records'] == 1) {
                    move_uploaded_file($_FILES["spare_image1"]["tmp_name"], $output_dir . $spareid1);
                    echo "Spare updated Successfully";
                } else {
                    echo "product not added, kindly try again";
                }
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function edit_spare1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id1           = $this->input->post('id1');
        $spareid1      = $this->input->post('spareid1');
        $spare_code1   = $this->input->post('spare_code1');
        $spare_name1   = $this->input->post('spare_name1');
        $spare_modal1  = $this->input->post('spare_modal1');
        $spare_desc1   = $this->input->post('spare_desc1');
        $spare_source1 = $this->input->post('spare_source1');
        $price1        = $this->input->post('price1');
        $q_purchase1   = $this->input->post('q_purchase1');
        $q_used1       = $this->input->post('q_used1');
        $product1      = $this->input->post('product1');
        $category1     = $this->input->post('category1');
        //$quantity1=$this->input->post('quantity1');
        $company_id1   = $this->input->post('company_id1');
        if (!empty($id1) && !empty($spareid1) && !empty($spare_code1) && !empty($spare_modal1) && !empty($spare_desc1) && !empty($spare_name1) && !empty($spare_source1) && !empty($price1) && !empty($q_purchase1) && !empty($product1) && !empty($category1) && !empty($company_id1)) {
            $this->form_validation->set_rules('price1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            $this->form_validation->set_rules('q_purchase1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            //$this->form_validation->set_rules('quantity1', 'Input', 'required|regex_match[/^[0-9,]+$/]'); //{10} for 10 digits number
            if ($this->form_validation->run() == False) {
                echo "price, Quantity purchased and Quantity should be in correct format";
            } else {
                $data            = array(
                    'spare_id' => $this->input->post('spareid1'),
                    'spare_code' => $this->input->post('spare_code1'),
                    'spare_modal' => $this->input->post('spare_modal1'),
                    'spare_desc' => $this->input->post('spare_desc1'),
                    'spare_name' => $this->input->post('spare_name1'),
                    'spare_source' => $this->input->post('spare_source1'),
                    'price' => $this->input->post('price1'),
                    'quantity_purchased' => $this->input->post('q_purchase1'),
                    'product' => $this->input->post('product1'),
                    'category' => $this->input->post('category1'),
                    //'quantity'=>$this->input->post('quantity1'),
                    'company_id' => $this->input->post('company_id1')
                );
                $data['records'] = $this->Admin_model->edit_spare($data, $id1);
                if ($data['records'] == 1) {
                    echo "Spare updated Successfully";
                } else {
                    echo "Spare not updated, kindly try again";
                }
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function fetch_spareinfo()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $rid  = $this->input->post('row_id');
        $sid  = $this->input->post('sid');
        $data = $this->Admin_model->fetch_spareinfo($rid, $sid);
        echo json_encode($data);
    }
    public function deletespare()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletespare($id);
        echo $data;
    }
    public function getattandance()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $attendance['records'] = $this->Admin_model->getattendances();
            print_r(json_encode($attendance['records']));
        } else {
            redirect('login/login');
        }
    }

    public function getdates()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $year       = $this->input->post('years');
        $month      = $this->input->post('months');
        $attendance = $this->Admin_model->getdate($year, $month);
        print_r(json_encode($attendance));
    }
    public function certificate()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            //$datas= $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records']=$this->Admin_model->getproduct($datas);
			$data['recor']=$this->Admin_model->getsubproduct($datas);
			$data['record']=$this->Admin_model->getcertificate($datas);
			$data['cer']=$this->Admin_model->getcertificate_1($datas);
			$data['r']=$this->Admin_model->gettraining_1($datas);
			$data['reco']=$this->Admin_model->gettraining($datas);
			$data['rec']=$this->Admin_model->gettrainings($datas);
			$data['recordsss']=$this->Admin_model->getquiz($datas);
            $data['trai_con']=$this->Admin_model->gettraining_content($datas);            
            //echo "<pre>";print_r($data['r']);
            $this->load->view('trainingandcertification', $data);
        } else {
            redirect('login/login');
        }
    }
    public function edits_certificates()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass            = $this->session->userdata('session_username');
            //$datas= $this->session->userdata('companyname');
            $datas             = $this->session->userdata('companyid');
            $data['records']   = $this->Admin_model->getproduct($datas);
            $data['recor']     = $this->Admin_model->getsubproduct($datas);
            $data['record']    = $this->Admin_model->getcertificate($datas);
            $data['rec']       = $this->Admin_model->gettrainings_quiz($datas);
            $data['recordsss'] = $this->Admin_model->getquiz($datas);
            $this->load->view('edits_certificates', $data);
            //print_r($data['count']);
        } else {
            redirect('login/login');
        }
    }
    public function certificate_s()
    {
        $id     = $this->input->post('sParameterNames');
        $datass = $this->Admin_model->edits_certificates($id);
		//print_r($datass);
        print_r(json_encode($datass));
    }
    public function quiz_sded()
    {
        $id     = $this->input->post('id');
        $datass = $this->Admin_model->get_quiz_s($id);
        print_r(json_encode($datass));
    }
    public function certificate_check()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $datass = $this->Admin_model->certificate_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "Assestment_" . $datass;
        echo $datass;
    }
    public function insertcertificate()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $certificate_id   = $this->input->post('assessment_id');
        $certificate_name = $this->input->post('certproduct_name');
        $certificate_type = $this->input->post('assessment_type');
        $product_id       = $this->input->post('product_name');
        $cat_id           = $this->input->post('category');
        $com_id           = $this->input->post('com_id');
        if (!empty($certificate_id) && !empty($certificate_name) && !empty($certificate_type)) {
            if ($certificate_type == "product") {
                if (!empty($product_id) && !empty($cat_id)) {
                    $datas=true;//$this->Admin_model->check_tr($product_id,$cat_id,$certificate_type,$com_id);
                    if($datas){                        
                        $data = array(
                            'certificate_id' => $certificate_id,
                            'certificate_name' => $this->input->post('certproduct_name'),
                            'certificate_type' => $this->input->post('assessment_type'),
                            'product_id' => $product_id,
                            'cat_id' => $cat_id,
                            'title' => '',
                            'company_id' => $com_id,
                            'status' => '0'
                        );
                        $data = $this->Admin_model->insertcertificate($data);
                        echo $data;    
                    }else{
                        echo "There is no Training content for this assesstment, kindly add training and try again!";
                    }
                } else {
                    echo "Product category and Sub category is mandatotry";
                }
            } else {
                $datas=true;//$this->Admin_model->check_t($certificate_type,$com_id);
                if($datas){                    
                    $data = array(
                        'certificate_id' => $certificate_id,
                        'certificate_name' => $this->input->post('certproduct_name'),
                        'certificate_type' => $this->input->post('assessment_type'),
                        'product_id' => '',
                        'cat_id' => '',
                        'title' => '',
                        'company_id' => $com_id,
                        'status' => '0'
                    );
                    $data = $this->Admin_model->insertcertificate($data);
                    echo $data;
                }else{
                    echo "There is no Training content for this assesstment, kindly add training and try again!";
                }
            }
        } else {
            echo 'All fields are mandatory';
        }
    }
    public function getdetails_certificate()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->getdetails_certificate_s($id);
        print_r(json_encode($data));
    }
    public function submit_certificate()
    {
        $id               = $this->input->post('id1');
        $certificate_id   = $this->input->post('certificate_id1');
        $certificate_name = $this->input->post('certificate_name1');
        $product_id       = $this->input->post('product_name1');
        $cat_id           = $this->input->post('category1');
        $title            = $this->input->post('title1');
        if (!empty($certificate_id) && !empty($certificate_name) && !empty($product_id) && !empty($cat_id) && !empty($title)) {
            $data = array(
                'certificate_id' => $this->input->post('certificate_id1'),
                'certificate_name' => $this->input->post('certificate_name1'),
                'product_id' => $this->input->post('product_name1'),
                'cat_id' => $this->input->post('category1'),
                'title' => $this->input->post('title1'),
                'status' => '0'
            );
            //print_r($data);
            $data = $this->Admin_model->updatecertificate($data, $id);
            echo $data;
        } else {
            echo 'All fields are mandatory';
        }
    }
    public function deletecertificate()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletecertificate($id);
        echo $data;
    }
    public function training()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records']  = $this->Admin_model->getproduct();
            $data['recor']    = $this->Admin_model->getsubproduct();
            $data['record']   = $this->Admin_model->getcertificate();
            $data['recordss'] = $this->Admin_model->gettraining();
            $this->load->view('training', $data);
        } else {
            redirect('login/login');
        }
    }
    public function training_check()
    {
        $datass = $this->Admin_model->training_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "Training_" . $datass;
        echo $datass;
    }
    public function getcertificates()
    {
        $product  = $this->input->post('product_id');
        $category = $this->input->post('category');
        $data     = $this->Admin_model->getcertificates($product, $category);
        print_r(json_encode($data));
    }
    /* public function insert_trai(){
    $training_id=$this->input->post('training_id');
    $title=$this->input->post('title_training');
    $product_type=$this->input->post('product_type');
    $product_training=$this->input->post('product_training');
    $category_training=$this->input->post('category_training');
    $model_name=$this->input->post('model_name');
    $company_id=$this->input->post('company_id');
    if($product_type=='generic'){
    if(!empty($title) && !empty($product_type)){
    $data=array(
    'training_id'=>$training_id,
    'title'=>$title,
    'product_type'=>$product_type,
    'product'=>"",
    'category'=>"",
    'model_name'=>"",
    'company_id'=>$company_id,
    );
    $datas=$this->Admin_model->insert_traini($data);
    if($datas==1){
    echo "Training updated Successfully";
    }else{
    echo "Training not updated Successfully, kindly try again";
    }
    }else{
    echo "All fields are Mandatory";
    }
    }elseif($product_type=='technology'){
    if(!empty($title) && !empty($product_type) && !empty($product_training) && !empty($category_training) && !empty($model_name)){
    $data=array(
    'training_id'=>$training_id,
    'title'=>$title,
    'product_type'=>$product_type,
    'product'=>$product_training,
    'category'=>$category_training,
    'model_name'=>$model_name,
    'company_id'=>$company_id,
    );
    $datas=$this->Admin_model->insert_traini($data);
    if($datas==1){
    echo "Training updated Successfully";
    }else{
    echo "Training not updated Successfully, kindly try again";
    }
    }else{
    echo "All fields are Mandatory";
    }
    }else{
    echo "All fields are Mandatory";
    }
    } */
    public function getdetails_training()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->getdetails_training($id);
        print_r(json_encode($data));
    }
    public function get_training_details()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->get_training_details($id);
        print_r(json_encode($data));
    }
    public function getdetails_train_content()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->getdetails_train_content($id);
        print_r(json_encode($data));
    }
    public function edittraining()
    {
        $this->load->helper(array(
            'url'
        ));
        $msg             = "";
        $id              = $this->input->post('id');
        $training_select = $this->input->post('training_select');
        $training_titles = $this->input->post('training_titles');
        $content_type    = $this->input->post('content_type');
        $file_type       = $this->input->post('file_type');
        $upload_contents = $_FILES["upload_content"];
        $upload_content  = $_FILES["upload_content"]["name"];
        $output_dir      = "upload/";
        $url             = base_url() . $output_dir . $upload_content;
        $config          = array(
            'upload_path' => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content
        );
        $this->load->library('upload', $config);
        if (!empty($training_select) && !empty($content_type) && !empty($file_type)) {
            if ($file_type == "pdf") {
                if (strtolower($content_type) != strtolower($file_type)) {
                    $msg = "Uploaded file must matched with selected content Type";
                } else {
                    if (!$this->upload->do_upload("upload_content")) {
                        $msg = "Some error while upload a training content";
                    } else {
                        $baseurl = base_url() . 'upload/pdf.png';
                        $datas   = array(
                            'title' => $training_titles,
                            'training_id' => $training_select,
                            'type' => $content_type,
                            'link_image' => $baseurl,
                            'link' => $url
                        );
                        $config  = array(
                            'upload_path' => './upload/',
                            'allowed_types' => '*',
                            'overwrite' => TRUE,
                            'file_name' => $upload_content
                        );
                        $insert  = $this->Admin_model->edittraining_content($datas, $id);
                        if ($insert) {
                            $msg = "Training content updated successfully";
                        } else {
                            $msg = "Something went wrong when saving the file, please try again.";
                        }
                    }
                }
            } else {
                if (strtolower($file_type) == "mp4" || strtolower($file_type) == "divx" || strtolower($file_type) == "3gp" || strtolower($file_type) == "wmv") {
                    if (!$this->upload->do_upload("upload_content")) {
                        $msg = "Some error while upload a training content";
                    } else {
                        $baseurl = base_url() . 'upload/video-player.png';
                        $datas   = array(
                            'title' => $training_titles,
                            'training_id' => $training_select,
                            'type' => $content_type,
                            'link_image' => $baseurl,
                            'link' => $url
                        );
                        $config  = array(
                            'upload_path' => './upload/',
                            'allowed_types' => '*',
                            'overwrite' => TRUE,
                            'file_name' => $upload_content
                        );
                        $insert  = $this->Admin_model->edittraining_content($datas, $id);
                        if ($insert) {
                            $msg = "Training content updated successfully";
                        } else {
                            $msg = "Something went wrong when saving the file, please try again.";
                        }
                    }
                } else {
                    $msg = "Uploaded file must matched with selected content Type";
                }
            }
        } else {
            $msg = "All Fields are Mandatory";
        }
        print_r($msg);
    }
    public function edittraining1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $id              = $this->input->post('id1');
        $training_select = $this->input->post('training_select1');
        $training_titles = $this->input->post('training_titles1');
        $content_type    = $this->input->post('content_type1');
        $url             = $this->input->post('edit_content2');
        $baseurl         = base_url() . 'upload/002-document.png';
        if (!empty($training_select) && !empty($training_titles) && !empty($content_type)) {
            $datas           = array(
                'title' => $training_titles,
                'training_id' => $training_select,
                'type' => $content_type,
                'link_image' => $baseurl,
                'link' => $url
            );
            $data['records'] = $this->Admin_model->edittraining_content($datas, $id);
            if ($data['records'] == 1) {
                echo "Training content updated successfully";
            } else {
                echo "Training content not updated, kindly try again";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    /*public function deletetraining(){
    $id=$this->input->post('id');
    $data=$this->Admin_model->deletetraining($id);
    echo $data;
    }*/
    public function quiz()
    {

        if ($this->session->userdata('user_logged_in')) {
            $datass       = $this->session->userdata('session_username');
            $datas        = $this->session->userdata('companyname');
            $datas        = $this->session->userdata('companyid');
            $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $data['records']  = $this->Admin_model->getproduct();
            $data['recor']    = $this->Admin_model->getsubproduct();
            $data['record']   = $this->Admin_model->gettrainings();
            $data['recordss'] = $this->Admin_model->getquiz();
            //$this->load->view('quiz',$data);
        } else {
            redirect('login/login');
        }
    }
    public function quiz_check()
    {
        $datass = $this->Admin_model->quiz_check();
        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
        $datass = $datass + 1;
        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
        $datass = "Quiz_" . $datass;
        echo $datass;
    }
    /*public function getdetails_training()
    {
    $id=$this->input->post('id');
    $data=$this->Admin_model->getdetails_training($id);
    print_r(json_encode($data));
    }*/
    public function addquiz()
    {
        $msg     = "";
        $quiz_id = $this->input->post('fields');
        for ($i = 0; $i < count($quiz_id); $i++) {
            $data = array(
                'quiz_id' => $quiz_id[$i][0],
                'question' => $quiz_id[$i][1],
                'choice1' => $quiz_id[$i][2],
                'choice2' => $quiz_id[$i][3],
                'choice3' => $quiz_id[$i][4],
                'choice4' => $quiz_id[$i][5],
                'correct_ans' => $quiz_id[$i][6],
                'certificate_id' => $quiz_id[$i][7]
            );
            if (empty($quiz_id[$i][0]) || empty($quiz_id[$i][1]) || empty($quiz_id[$i][2]) || empty($quiz_id[$i][3]) || empty($quiz_id[$i][4]) || empty($quiz_id[$i][5]) || empty($quiz_id[$i][6]) || empty($quiz_id[$i][7])) {
                $msg[] = "All Fields Are Mandatory" . "\XA";
            } else {
                //$msg[]="All Fields Are present" ."\XA";
                $addquiz = $this->Admin_model->addquiz($data);
                if ($addquiz) {
                    $msg = "Quiz added Successfully" . "\XA";
                } else {
                    $msg = "Quiz not added, kindly try again" . "\XA";
                }
                ///print_r($data);
            }
        }
        echo json_encode($msg);
        /*  $quiz_id=$this->input->post('quiz_id');
        $trainings=$this->input->post('trainings');
        $question=$this->input->post('question');
        $option_a=$this->input->post('option_a');
        $option_b=$this->input->post('option_b');
        $option_c=$this->input->post('option_c');
        $option_d=$this->input->post('option_d');
        $c_answer=$this->input->post('c_answer');
        if(empty($quiz_id) && empty($trainings) &&  empty($question) &&  empty($question) &&  empty($option_a) &&  empty($option_b) &&  empty($option_c) &&  empty($option_d) &&  empty($$c_answer)){
        echo "All fields are Mandatory";
        }else{
        $data=array(
        'quiz_id'=>$quiz_id,
        'training_id'=>$trainings,
        'question'=>$question,
        'choice1'=>$option_a,
        'choice2'=>$option_b,
        'choice3'=>$option_c,
        'choice4'=>$option_d,
        'correct_ans'=>$c_answer
        );
        $addquiz=$this->Admin_model->addquiz($data);
        echo $addquiz;
        } */
        /*$quiz_id=$this->input->post('quiz_id');
        $training_id=$this->input->post('training_id');
        $trainings=$this->input->post('trainings');
        $question=$this->input->post('question');
        $option_a=$this->input->post('option_a');
        $option_b=$this->input->post('option_b');
        $option_c=$this->input->post('option_c');
        $option_d=$this->input->post('option_d');
        $c_answer=$this->input->post('c_answer');
        echo "welcome";
        if(empty($c_answer) ||  empty($trainings) ||  empty($training_id) ||  empty($question) ||  empty($option_a) ||  empty($option_b) ||  empty($option_c) ||  empty($option_d) ){
        echo "All fields are Mandatory";
        }else{
        $data=array(
        'quiz_id'=>$quiz_id,
        'training_id'=>$trainings,
        'question'=>$question,
        'choice1'=>$option_a,
        'choice2'=>$option_b,
        'choice3'=>$option_c,
        'choice4'=>$option_d,
        'correct_ans'=>$c_answer
        );
        $addquiz=$this->Admin_model->addquiz($data);
        echo $addquiz;
        }*/
    }
    public function getdetails_quiz()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->getdetails_quiz($id);
        print_r(json_encode($data));
    }
    public function check_assesment()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->check_assesment($id);
        print_r(json_encode($data));
    }
    public function submit_quiz()
    {
        $id1       = $this->input->post('quiz_id_s');
        $quiz_id   = $this->input->post('quiz_id1');
        $trainings = $this->input->post('trainings1');
        $question  = $this->input->post('question1');
        $option_a  = $this->input->post('option_a1');
        $option_b  = $this->input->post('option_b1');
        $option_c  = $this->input->post('option_c1');
        $option_d  = $this->input->post('option_d1');
        $c_answer  = $this->input->post('c_answer1');
        $data      = array(
            'quiz_id' => $quiz_id,
            'training_id' => $trainings,
            'question' => $question,
            'choice1' => $option_a,
            'choice2' => $option_b,
            'choice3' => $option_c,
            'choice4' => $option_d,
            'correct_ans' => $c_answer
        );
        if (empty($quiz_id) || empty($trainings) || empty($question) || empty($option_a) || empty($option_c) || empty($option_c) || empty($option_d) || empty($c_answer)) {
            echo "Fields are Mandatory";
        } else {
            $data    = array(
                'quiz_id' => $quiz_id,
                'training_id' => $trainings,
                'question' => $question,
                'choice1' => $option_a,
                'choice2' => $option_b,
                'choice3' => $option_c,
                'choice4' => $option_d,
                'correct_ans' => $c_answer
            );
            $addquiz = $this->Admin_model->updatequiz($data, $id1);
            if ($addquiz == 1) {
                echo "Quiz updated successfully";
            } else {
                echo "Quiz not updated successfully";
            }
        }
    }
    public function deletequiz()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletequiz($id);
        echo $data;
    }
    public function deletetraining()
    {
        $id=$this->input->post('id');
		$data=$this->Admin_model->deletetraining($id);		
        if($data==1){
        $datas=$this->Admin_model->delete_training($id);
            echo $datas;
        }else{
            echo "Training not deleted";
        }
    }
    public function view_technician_image()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->view_technician_image($id);
        if (!empty($data['image'])) {
            print_r(json_encode($data));
        } else {
            print_r(json_encode(array(
                "image" => "http://85.25.134.22/fieldpro/assets/pages/img/fieldpro/no_image.png"
            )));
        }
    }
    public function view_training_images()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->view_training_images($id);
        print_r(json_encode($data));
    }
    public function getdetails_technicians()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_technicians($data);
        print_r(json_encode($data));
    }
    public function view_product_images()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->view_product_images($id);
        print_r(json_encode($data));
    }
    public function view_category_images()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->view_category_images($id);
        print_r(json_encode($data));
    }
    public function view_spare_images()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->view_spare_images($id);
        if (!empty($data['image'])) {
            print_r(json_encode($data));
        } else {
            print_r(json_encode(array(
                "image" => "http://85.25.134.22/fieldpro/assets/pages/img/fieldpro/no_image.png"
            )));
        }
    }
    public function data_category()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->data_category($id);
        //echo json_encode($data);
        print_r(json_encode($data));
    }
    public function download_sampletemplate($filename = NULL)
    {
        $this->load->helper('url');
        // load download helder
        $this->load->helper('download');
        // read file contents
        $data = file_get_contents(base_url('/assets/templates/' . $filename));
        force_download($filename, $data);
    }
    public function score()
    {
        $datass       = $this->session->userdata('session_username');
        $datas        = $this->session->userdata('companyname');
        $datas        = $this->session->userdata('companyid');
        $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
        $this->load->helper('url');
        $this->load->view('score');
    }
    public function sla()
    {
        if ($this->session->userdata('user_logged_in')) {
            $datass                 = $this->session->userdata('session_username');
            $datas                  = $this->session->userdata('companyname');
            $datas                  = $this->session->userdata('companyid');
            $data['pass']           = $this->Admin_model->get_employee($datass, $datas);
            $data['details']        = $this->Admin_model->disply($datas);
            $data['priority_level'] = $this->Super_admin->priority_level();
            $data['get_sla']        = $this->Admin_model->sla_get($datas);
            $this->load->view('sla_admin', $data);
        } else {
            redirect('login/login');
        }

    }
    public function getdetails_sla()
    {
        $id    = $this->input->post('id');
        $datas = $this->Admin_model->getdetails_sla($id);
        print_r(json_encode($datas));
    }
    /*public function add_sla()
    {
    $this->load->helper('url');
    $id =  $this->input->post('id1');
    $company_id=$this->input->post('company_id');
    $ref_id=$this->input->post('ref_id');
    $priority = $this->input->post('priority');
    $rhours = $this->input->post('rhours');
    $rminute = $this->input->post('rminute');
    $rseconds = $this->input->post('rseconds');
    $resp_time=$rhours.':'.$rminute.':'.$rseconds;
    //$resp_time=date('',strtotime($restime));
    $hours = $this->input->post('hours');
    $minute = $this->input->post('minute');
    $seconds = $this->input->post('seconds');
    $resolution=$hours.':'.$minute.':'.$seconds;
    //$resp_time=date('H:i:s',strtotime($restime));
    //$resolution=date('H:i:s',strtotime($resoltime));
    $mttr_target = $this->input->post('mttr_target');
    $sla_target = $this->input->post('service_reven');
    if( empty($priority) && empty($resp_time) && empty($resolution) && empty($mttr_target) && empty($sla_target)){
    echo "All fields are mandatory";
    }else{
    $product = $this->input->post('for_prod');
    $category = $this->input->post('for_cat');
    $customer = $this->input->post('for_cust');
    $contract = $this->input->post('for_cont_type');
    $call_cat = $this->input->post('txtPassportNumber0');
    // echo $call_cat;
    $keywords = explode(',', $call_cat);
    if( empty($call_cat))
    {
    if($product=='')
    {
    $product = 'all';
    }
    if($category=='')
    {
    $category = 'all';
    }
    if($customer=='')
    {
    $customer = 'all';
    }
    if($contract=='')
    {
    $contract = 'all';
    }
    if($call_cat=='')
    {
    $call_cat = 'all';
    }
    $ref_id = $this->Reimbursement->get_reference_id($company_name);
    $adata1=array(
    'company_id'=>$company_name,
    'priority_level'=>$priority,
    'response_time'=>$resp_time,
    'resolution_time'=>$resolution,
    'mttr_target'=> $mttr_target,
    'SLA_Compliance_Target'=>$sla_target,
    'ref_id'=>$ref_id
    );
    $adata2=array(
    'company_id'=>$company_name,
    'product'=>$product,
    'category'=>$category,
    'cust_category'=>$customer,
    'call_category'=> $call_cat,
    'service_category'=>$contract,
    'ref_id'=>$ref_id
    );

    $insert1 = $this->Admin_modal->add_sla1($adata2,$id,$company_id,$ref_id);
    if($insert1==1){
    $ins = $this->Admin_modal->add_sla($adata1,$id,$company_id,$ref_id);
    if($ins==1)
    {
    echo "Inserted.";
    }
    }
    else
    {
    echo "This combination already Already Exist";
    }
    }
    else
    {
    if(empty($product)){
    $product = 'all';
    }
    if(empty($category)){
    $category = 'all';
    }
    if(empty($customer)){
    $customer = 'all';
    }
    if(empty($contract)){
    $contract = 'all';
    }
    if(empty($call_cat)){
    $call_cat = 'all';
    }
    foreach($keywords as $key){
    $ref_id = $this->Admin_modal->get_reference_id($company_name);
    $adata1=array(
    'priority_level'=>$priority,
    'response_time'=>$resp_time,
    'resolution_time'=>$resolution,
    'mttr_target'=> $mttr_target,
    'SLA_Compliance_Target'=>$sla_target,
    );
    $adata2=array(
    'product'=>$product,
    'category'=>$category,
    'cust_category'=>$customer,
    'call_category'=> $key,
    'service_category'=>$contract,
    );

    $insert1 = $this->Admin_modal->add_sla1($adata2,$id,$company_id,$ref_id);
    if($insert1==1){
    $ins = $this->Reimbursement->add_sla($adata1,$id,$company_id,$ref_id);
    if($ins==1)
    {
    echo "Inserted.";
    }
    }
    else
    {
    echo "This combination already Already Exist";
    }
    }

    }
    }
    }*/
    public function update_sla()
    {
        $id               = $this->input->post('id1');
        $priority         = $this->input->post('priority');
        $rhours           = $this->input->post('rhours');
        $rminute          = $this->input->post('rminute');
        $rseconds         = $this->input->post('rseconds');
        $resp_time        = $rhours . ':' . $rminute . ':' . $rseconds;
        //$resp_time=date('',strtotime($restime));
        $hours            = $this->input->post('hours');
        $minute           = $this->input->post('minute');
        $seconds          = $this->input->post('seconds');
        $resolution       = $hours . ':' . $minute . ':' . $seconds;
        $mttr_target      = $this->input->post('mttr_target');
        $service_reven    = $this->input->post('service_reven');
        $company_id       = $this->input->post('company_id');
        $ref_id           = $this->input->post('ref_id');
        $product          = $this->input->post('for_prod');
        $category         = $this->input->post('for_cat');
        $cust_category    = $this->input->post('for_cust');
        $service_category = $this->input->post('for_cont_type');
        $call_category    = $this->input->post('txtPassportNumber');
        $data             = array(
            'priority_level' => $priority,
            'response_time' => $resp_time,
            'resolution_time' => $resolution,
            'SLA_Compliance_Target' => $service_reven,
            'mttr_target' => $mttr_target
        );
        $data1            = array(
            'product' => $product,
            'category' => $category,
            'cust_category' => $cust_category,
            'service_category' => $service_category,
            'call_category' => $call_category
        );
        $checking         = $this->Admin_model->checking($data1, $ref_id, $company_id);
        if ($checking == "") {
            $result = $this->Admin_model->update_sla($data, $ref_id, $company_id);
            if ($result == 1) {
                $result_1 = $this->Admin_model->update_sla1($data1, $ref_id, $company_id);
                if ($result_1 == 1) {
                    echo "Sla updated successfully";
                } else {
                    echo "Sla combination is not updated, kindly check it and try again";
                }
            } else {
                echo "Sla is not updated successfully kindly check it";
            }
            //print_r(json_encode($result));
        } else {
            echo "Present";
        }
    }
    public function contract()
    {
        $datass       = $this->session->userdata('session_username');
        $datas        = $this->session->userdata('companyname');
        $datas        = $this->session->userdata('companyid');
        $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data['record'] = $this->Admin_model->disply_inventory();
        $data['amc']    = $this->Admin_model->disply_inven($datas);
        $this->load->view('contract_amc', $data);
    }
    public function add_lead()
    {
        $this->load->helper('url');
        $this->load->database();

        $data = array(
            'average_calls' => $this->input->post('avgcall'),
            'first_time' => $this->input->post('first_time'),
            'contract_revenue' => $this->input->post('service_revev'),
            'customer_rating' => $this->input->post('custo_rat'),
            'contribution_to_kb' => $this->input->post('cont_kb'),
            'training_&_certification' => $this->input->post('training'),
            'sla_adher' => $this->input->post('sla_adher'),
            'company_id' => $this->input->post('company_id')
        );

        $this->load->model('Admin_model');
        $count = $this->Admin_model->check_company($this->input->post('company_id'));
        if ($count > 0) {
            $result = $this->Admin_model->update_score($data, $this->input->post('company_id'));
        } else {
            $result = $this->Admin_model->add_score($data);
        }
        if ($result == 1) {
            echo "Added successfully";
        } else {
            echo "Error!!! Please recheck and add";
        }
    }
    public function sla_adh()
    {
        $this->load->helper('url');
        $this->load->database();

        $data = array(
            'priority_level' => $this->input->post('avgcall'),
            'response_time' => $this->input->post('first_time'),
            'resolution_time' => $this->input->post('cus_feed'),
            'SLA_Compliance_Target' => $this->input->post('service_revev')
        );

        $this->load->model('Admin_model');
        $result = $this->Admin_model->sla_adh($data);
        if ($result == 1) {
            echo "Added successfully";
        } else {
            echo "Error!!! Please recheck and add";
        }
    }
    public function view_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $data = $this->input->post('id');
        $data = $this->Admin_model->getdetails_sla($data);
        echo json_encode($data);
    }
    public function add_contract()
    {
        $datas           = $this->session->userdata('companyid');
        $amc_type        = $this->input->post('amc_type');
        $contract_period = $this->input->post('contract_period');
        if (!empty($amc_type) && !empty($contract_period)) {
            $amc_check = $this->Admin_model->amc_check($amc_type, $datas);
            if ($amc_check == "") {
                $data   = array(
                    'amc_type' => $amc_type,
                    'contract_period' => $contract_period,
                    'company_id' => $datas
                );
                $result = $this->Admin_model->add_contracts($data);
                if ($result == 1) {
                    echo "Contract added successfully";
                } else {
                    echo "Contract not added, kindy check it and try again";
                }
            } else {
                echo $amc_type . " is already placed, kindly check it";
            }
        } else {
            echo "All Fields are Mandatory";
        }
    }
    public function edit_contract()
    {
        $id              = $this->input->post('id1');
        $amc_type        = $this->input->post('amc_type1');
        $contract_period = $this->input->post('contract_period1');
        if (!empty($amc_type) && !empty($contract_period)) {
            $data   = array(
                'amc_type' => $amc_type,
                'contract_period' => $contract_period
            );
            $result = $this->Admin_model->edit_contracts($data, $id);
            if ($result == 1) {
                echo "Contract updated successfully";
            } else {
                echo "Contract not updated, kindy check it and try again";
            }
        } else {
            echo "All Fields are Mandatory";
        }
    }
    public function edit_amctype()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->edit_amctype($id);
        print_r(json_encode($data));
    }
    public function add_price()
    {
        $this->load->helper('url');
        $this->load->database();

        $data = array(
            'contract_type' => $this->input->post('amc'),
            'start_date' => $this->input->post('srtdate'),
            'end_date' => $this->input->post('endate'),
            'price' => $this->input->post('price'),
            'company_id' => $this->input->post('company')
        );

        $this->load->model('Admin_model');
        $result = $this->Admin_model->add_price($data);
        if ($result == 1) {
            echo "Added successfully";
        } else {
            echo "Error!!! Please recheck and add";
        }
    }

    public function deletecontract()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletecontract($id);
        echo $data;
    }
    public function deletesla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $id   = $this->input->post('id');
        $data = $this->Admin_model->deletesla($id);
        echo $data;
    }
    public function edit_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
        $priority_level = $this->input->post('avgcall1');
        $data           = array(
            'response_time' => $this->input->post('first_time1'),
            'resolution_time' => $this->input->post('cus_feed1'),
            'SLA_Compliance_Target' => $this->input->post('service_revev1')
        );
        $data = $this->Admin_model->edit_sla($data, $priority_level);
        if ($data == 1) {
            echo "User updated Successfully";
        }
    }
    public function change_password()
    {
        $this->load->helper('url');
        $this->load->database();
        $id        = $this->input->post('change_id');
        $usernames = $this->input->post('usernames');
        $old_pass  = $this->input->post('old_pass');
        $this->load->model('Admin_model');
        $data = $this->Admin_model->password_check($id, $usernames, $old_pass);
        echo $data;
    }
    public function change_password_1()
    {
        $this->load->helper('url');
        $this->load->database();
        $id           = $this->input->post('change_id');
        //$this->encrypt->encode($this->input->post('confirm_pass'))
        $confirm_pass = $this->encrypt->encode($this->input->post('confirm_pass'));
        $values       = array(
            'password' => $confirm_pass
        );
        $this->load->model('Admin_model');
        $data = $this->Admin_model->password_change($id, $values);
        echo $data;
    }
    public function renew_info()
    {
        $id         = $this->input->post('company_id');
        $name       = $this->input->post('cmodal_name');
        $service    = $this->input->post('cmodal_serv');
        $technician = $this->input->post('cmodal_tech');
        $subs_plan  = $this->input->post('time_yr');
        $renew      = $this->input->post('cust_id_renew');
        $update     = array(
            'service_desk' => $service,
            'technicians' => $technician,
            'subs_plan' => $subs_plan,
            'renewal_date' => $renew
        );
        if (!empty($subs_plan)) {
            $company_info = $this->Admin_model->renew_info($update, $id);
            if ($company_info == 1) {
                echo "inserted";
            } else {
                echo "Error occured in Renewal.";
            }
        } else {
            echo "All Fields are Mandatory.";
        }
    }

    public function gettrainin_g()
    {
        $datas        = $this->session->userdata('companyid');
        $data_company = $this->Admin_model->getcompany_s($datas);
        $nodes        = array();
        foreach ($data_company as $val) {
            array_push($nodes, array(
                'id' => $val['a'],
                'parent' => "#",
                'text' => 'Add Training'
            ));
            $company = $val['a'];
            $data    = $this->Admin_model->gettrainin_g($company);
            foreach ($data as $val_training) {
                array_push($nodes, array(
                    'id' => $val_training['training_id'],
                    'parent' => $val_training['company_id'],
                    'text' => $val_training['title'],
                    'icon' => $val_training['training_image']
                ));
                $training_id        = $val_training['training_id'];
                $data_train_content = $this->Admin_model->gettrainin_content($training_id);
                foreach ($data_train_content as $val_train_content) {
                    array_push($nodes, array(
                        'id' => $val_train_content['id'],
                        'parent' => $val_train_content['training_id'],
                        'text' => $val_train_content['title'],
                        'icon' => $val_train_content['link_image']
                    ));
                }
            }
        }
        echo json_encode($nodes);


        /* $datas = $this->input->post('company_id');
        $data=$this->Admin_model->gettrainin_g($datas);
        $nodes = array();
        foreach($data as $val)
        {
        array_push($nodes,array('id'=>$val['training_id'],'parent'=>"#",'text'=>$val['title']));
        $prod_id_1=$val['training_id'];
        $data_category=$this->Admin_model->gettrainin_content($prod_id_1);
        foreach($data_category as $val_category){
        array_push($nodes,array('id'=>$val_category['id'],'parent'=>$val_category['training_id'],'text'=>$val_category['title']));
        }
        }
        echo json_encode($data); */
    }
    public function insert_trai()
    {
        $this->load->helper(array(
            'url'
        ));
        $msg               = "";
        $training_id       = $this->input->post('training_id');
        $company_id        = $this->input->post('company_id');
        $title             = $this->input->post('title_training');
        $product_type      = $this->input->post('product_type');
        $product_training  = $this->input->post('product_training');
        $category_training = $this->input->post('category_training');
        $model_name        = $this->input->post('model_name');
        $content_type      = $this->input->post('content_type');
        $file_type         = $this->input->post('file_type');
        $upload_contents   = $_FILES["upload_content"];
        $upload_content    = $_FILES["upload_content"]["name"];
        $config          = array(
            'upload_path' => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content
        );
        $output_dir      = "upload/";
        $upload_content= str_replace(" ","_",$upload_content);
        $url             = base_url() . $output_dir . $upload_content;
        $this->load->library('upload', $config);

        if ($product_type == 'generic' || $product_type == 'technology') {
            if (!empty($title) && !empty($product_type) && !empty($content_type)) {
                if ($file_type == "pdf") {
                    if (strtolower($content_type) != strtolower($file_type)) {
                        echo "Uploaded file must matched with selected content Type";
                    } else {
                        if (!$this->upload->do_upload("upload_content")) {
                            $msg = "Some error while upload a training content";
                        } else {
                            $baseurl           = base_url() . 'upload/001-business.png';
                            $data  = array(
                                'training_id' => $training_id,
                                'title' => $title,
                                'product_type' => $product_type,
                                'product' => "",
                                'category' => "",
                                'model_name' => "",
                                'training_image' => $baseurl,
                                'thumbnail'=>"http://52.35.246.28/fieldpro_v1/assets/images/training/".$product_type.".png",
                                'company_id' => $company_id
                            );
                            $datas = $this->Admin_model->insert_traini($data);
                            if ($datas == 1) {
                                $baseurl = base_url() . 'upload/pdf.png';
                                $datas   = array(
                                    'title' => $title,
                                    'training_id' => $training_id,
                                    'type' => $content_type,
                                    'link_image' => $baseurl,
                                    'link' => $url
                                );
                                $insert  = $this->Admin_model->inserttraining_content($datas);
                                if ($insert) {
                                    echo "Training added Successfully";
                                }
                            } else {
                                echo "Training not added Successfully, kindly try again";
                            }
                        }
                    }
                } else {
                    if (strtolower($file_type) == "mp4" || strtolower($file_type) == "divx" || strtolower($file_type) == "3gp" || strtolower($file_type) == "wmv") {
                        if (!$this->upload->do_upload("upload_content")) {
                            echo "Some error while upload a training content";
                            echo $this->upload->display_errors();
                        } else {
                            $baseurl           = base_url() . 'upload/001-business.png';
                            $data  = array(
                                'training_id' => $training_id,
                                'title' => $title,
                                'product_type' => $product_type,
                                'product' => "",
                                'category' => "",
                                'model_name' => "",
                                'thumbnail'=>"http://52.35.246.28/fieldpro_v1/assets/images/training/".$product_type.".png",
                                'training_image' => $baseurl,
                                'company_id' => $company_id
                            );
                            $datas = $this->Admin_model->insert_traini($data);
                            if ($datas == 1) {
                                $baseurl = base_url() . 'upload/video-player.png';
                                $datas   = array(
                                    'title' => $title,
                                    'training_id' => $training_id,
                                    'type' => $content_type,
                                    'link_image' => $baseurl,
                                    'link' => $url
                                );
                                $insert  = $this->Admin_model->inserttraining_content($datas);
                                if ($insert) {
                                    echo "Training added Successfully";
                                }
                            } else {
                                echo "Training not added Successfully, kindly try again";
                            }
                        }
                    } else {
                        $msg = "Uploaded file must matched with selected content Type";
                    }
                }
                
            } else {
                echo "All fields are Mandatory";
            }
        } elseif ($product_type == 'product') {
            if (!empty($title) && !empty($product_type) && !empty($product_training) && !empty($category_training) && !empty($model_name) && !empty($content_type)) {
                if ($file_type == "pdf") {
                    if (strtolower($content_type) != strtolower($file_type)) {
                        echo "Uploaded file must matched with selected content Type";
                    } else {
                        if (!$this->upload->do_upload("upload_content")) {
                            echo "Some error while upload a training content";
                        } else {
                            $baseurl           = base_url() . 'upload/001-business.png';
                            $data  = array(
                                'training_id' => $training_id,
                                'title' => $title,
                                'product_type' => $product_type,
                                'product' => $product_training,
                                'category' => $category_training,
                                'model_name' => $model_name,
                                'thumbnail'=>"http://52.35.246.28/fieldpro_v1/assets/images/training/".$product_type.".png",
                                'training_image' => $baseurl,
                                'company_id' => $company_id
                            );
                            $datas = $this->Admin_model->insert_traini($data);
                            if ($datas == 1) {
                                $baseurl = base_url() . 'upload/pdf.png';
                                $datas   = array(
                                    'title' => $title,
                                    'training_id' => $training_id,
                                    'type' => $content_type,
                                    'link_image' => $baseurl,
                                    'link' => $url
                                );
                                $insert  = $this->Admin_model->inserttraining_content($datas);
                                if ($insert) {
                                    echo "Training added Successfully";
                                }
                            } else {
                                echo "Training not added Successfully, kindly try again";
                            }
                        }
                    }
                } else {
                    if (strtolower($file_type) == "mp4" || strtolower($file_type) == "divx" || strtolower($file_type) == "3gp" || strtolower($file_type) == "wmv") {
                        if (!$this->upload->do_upload("upload_content")) {
                            echo "Some error while upload a training content";
                        } else {
                            $baseurl           = base_url() . 'upload/001-business.png';
                            $data  = array(
                                'training_id' => $training_id,
                                'title' => $title,
                                'product_type' => $product_type,
                                'product' => $product_training,
                                'category' => $category_training,
                                'model_name' => $model_name,
                                'training_image' => $baseurl,
                                'thumbnail'=>"http://52.35.246.28/fieldpro_v1/assets/images/training/".$product_type.".png",
                                'company_id' => $company_id
                            );
                            $datas = $this->Admin_model->insert_traini($data);
                            if ($datas == 1) {
                                $baseurl = base_url() . 'upload/video-player.png';
                                $datas   = array(
                                    'title' => $title,
                                    'training_id' => $training_id,
                                    'type' => $content_type,
                                    'link_image' => $baseurl,
                                    'link' => $url
                                );
                                $insert  = $this->Admin_model->inserttraining_content($datas);
                                if ($insert) {
                                    echo "Training added Successfully";
                                }
                            } else {
                                echo "Training not added Successfully, kindly try again";
                            }
                        }
                    } else {
                        echo "Uploaded file must matched with selected content Type";
                    }
                }



                /*$data  = array(
                    'training_id' => $training_id,
                    'title' => $title,
                    'product_type' => $product_type,
                    'product' => $product_training,
                    'category' => $category_training,
                    'model_name' => $model_name,
                    'training_image' => $baseurl,
                    'company_id' => $company_id
                );
                $datas = $this->Admin_model->insert_traini($data);
                if ($datas == 1) {
                    echo "Training added Successfully";
                } else {
                    echo "Training not added Successfully, kindly try again";
                }*/
            } else {
                echo "All fields are Mandatory";
            }
        } else {
            echo "All fields are Mandatory";
        }
    }
    public function edit_trai_1()
    {
        $id                = $this->input->post('ids');
        $training_id       = $this->input->post('training_id1');
        $title             = $this->input->post('title_training1');
        $product_type      = $this->input->post('product_type1');
        $product_training  = $this->input->post('product_training1');
        $category_training = $this->input->post('category_training1');
        $model_name        = $this->input->post('model_name1');
        $company_id        = $this->input->post('company_id_1'); 
        $content_type        = $this->input->post('content_type_1'); 
        $url        = $this->input->post('upload_con_1'); 
        //$baseurl           = base_url() . 'upload/001-business.png';
        if ($product_type == 'generic' || $product_type == 'technology') {
            if (!empty($title) && !empty($product_type)) {
                $baseurl           = base_url() . 'upload/001-business.png';
                $data  = array(
                    'training_id' => $training_id,
                    'title' => $title,
                    'product_type' => $product_type,
                    'product' => "",
                    'category' => "",
                    'model_name' => "",
                    'training_image' => $baseurl,
                    'company_id' => $company_id
                );
                $datas = $this->Admin_model->edit_traini($data, $training_id);
                if ($datas == 1) {
                    $baseurl = base_url() . 'upload/pdf.png';
                    $datas   = array(
                        'title' => $title,
                        'training_id' => $training_id,
                        'type' => $content_type,
                        'link_image' => $baseurl,
                        'link' => $url
                    );
                    $data['records'] = $this->Admin_model->edittraining_content($datas, $training_id);
                    if ($data['records']) {
                        echo "Training updated Successfully";
                    }else{
                        echo "Training not updated Successfully, kindly try again";
                    }
                } else {
                    echo "Training not updated Successfully, kindly try again";
                }
            } else {
                echo "All fields are Mandatory";
            }
        } elseif ($product_type == 'product') {
            if (!empty($title) && !empty($product_type) && !empty($product_training) && !empty($category_training) && !empty($model_name)){
                $baseurl           = base_url() . 'upload/001-business.png';
                $data  = array(
                    'training_id' => $training_id,
                    'title' => $title,
                    'product_type' => $product_type,
                    'product' => $product_training,
                    'category' => $category_training,
                    'model_name' => $model_name,
                    'training_image' => $baseurl,
                    'company_id' => $company_id
                );
                $datas = $this->Admin_model->edit_traini($data, $training_id);
                if ($datas == 1) {
                    $baseurl = base_url() . 'upload/pdf.png';
                    $datas   = array(
                        'title' => $title,
                        'training_id' => $training_id,
                        'type' => $content_type,
                        'link_image' => $baseurl,
                        'link' => $url
                    );
                    $data['records'] = $this->Admin_model->edittraining_content($datas, $training_id);
                    if ($data['records']) {
                        echo "Training updated Successfully";
                    }else{
                        echo "Training not updated Successfully, kindly try again";
                    }
                } else {
                    echo "Training not updated Successfully, kindly try again";
                }
            } else {
                echo "All fields are Mandatory";
            }
        }
    }

    public function edit_trai_2()
    {
         $this->load->helper(array(
            'url'
        ));
        $id                = $this->input->post('ids');
        $training_id       = $this->input->post('training_id1');
        $company_id        = $this->input->post('company_id_1');
        $title             = $this->input->post('title_training1');
        $product_type      = $this->input->post('product_type1');
        $product_training  = $this->input->post('product_training1');
        $category_training = $this->input->post('category_training1');
        $model_name        = $this->input->post('model_name1');        
        $content_type      = $this->input->post('content_type_1');
        $file_type       = $this->input->post('file_type');
        $upload_contents = $_FILES["upload_con_1"];
        $upload_content  = $_FILES["upload_con_1"]["name"];
        $upload_content= str_replace(" ","_",$upload_content);
        $config          = array(
            'upload_path' => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content
        );        
        $output_dir      = "upload/";
        
        $url             = base_url() . $output_dir . $upload_content;
        $this->load->library('upload', $config);
        if ($product_type == 'generic' || $product_type == 'technology') {
            if (!empty($title) && !empty($product_type)) {
                if (!$this->upload->do_upload("upload_con_1")) {
                    echo "Some error while upload a training content";
                } else {
                    $baseurl = base_url() . 'upload/001-business.png';
                    $data  = array(
                        'training_id' => $training_id,
                        'title' => $title,
                        'product_type' => $product_type,
                        'product' => "",
                        'category' => "",
                        'model_name' => "",
                        'training_image' => $baseurl,
                        'company_id' => $company_id
                    );
                    $datas = $this->Admin_model->edit_traini($data, $training_id);
                    if ($datas == 1) {
                        $baseurl = base_url() . 'upload/pdf.png';
                        $datas   = array(
                            'title' => $title,
                            'training_id' => $training_id,
                            'type' => $content_type,
                            'link_image' => $baseurl,
                            'link' => $url
                        );
                        $data['records'] = $this->Admin_model->edittraining_content($datas, $training_id);
                        if ($data['records']) {
                            echo "Training updated Successfully";
                        }else{
                            echo "Training not updated Successfully, kindly try again";
                        }
                    } else {
                        echo "Training not updated Successfully, kindly try again";
                    }
                    
                }
            }else{
                echo "All fields are mandatory";
            }
        } elseif ($product_type == 'product') {
            if (!empty($title) && !empty($product_type) && !empty($product_training) && !empty($category_training) && !empty($model_name)){
                    if (!$this->upload->do_upload("upload_con_1")) {
                        echo "Some error while upload a training content";
                    } else {
                        $baseurl           = base_url() . 'upload/001-business.png';
                        $data  = array(
                            'training_id' => $training_id,
                            'title' => $title,
                            'product_type' => $product_type,
                            'product' => $product_training,
                            'category' => $category_training,
                            'model_name' => $model_name,
                            'training_image' => $baseurl,
                            'company_id' => $company_id
                        );
                        $datas = $this->Admin_model->edit_traini($data, $training_id);
                        if ($datas == 1) {
                            $baseurl = base_url() . 'upload/pdf.png';
                            $datas   = array(
                                'title' => $title,
                                'training_id' => $training_id,
                                'type' => $content_type,
                                'link_image' => $baseurl,
                                'link' => $url
                            );
                            $data['records'] = $this->Admin_model->edittraining_content($datas, $training_id);
                            if ($data['records']) {
                                echo "Training updated Successfully";
                            }else{
                                echo "Training not updated Successfully, kindly try again";
                            }
                        } else {
                            echo "Training not updated Successfully, kindly try again";
                        }
                        
                    }
            } else {
                echo "All fields are Mandatory";
            }
        }

    }



    /*public function inserttraining()
    {
        $this->load->helper(array(
            'url'
        ));
        $msg             = "";
        $training_select = $this->input->post('training_select');
        $training_titles = $this->input->post('training_titles');
        $content_type    = $this->input->post('content_type');
        $file_type       = $this->input->post('file_type');
        $upload_contents = $_FILES["upload_content"];
        $upload_content  = $_FILES["upload_content"]["name"];
        $config          = array(
            'upload_path' => './upload/',
            'allowed_types' => '*',
            'overwrite' => TRUE,
            'file_name' => $upload_content
        );
        $output_dir      = "upload/";
        $url             = base_url() . $output_dir . $upload_content;
        $this->load->library('upload', $config);
        if (!empty($training_select) && !empty($content_type) && !empty($file_type)) {
            if ($file_type == "pdf") {
                if (strtolower($content_type) != strtolower($file_type)) {
                    $msg = "Uploaded file must matched with selected content Type";
                } else {
                    if (!$this->upload->do_upload("upload_content")) {
                        $msg = "Some error while upload a training content";
                    } else {

                        $baseurl = base_url() . 'upload/pdf.png';
                        $datas   = array(
                            'title' => $training_titles,
                            'training_id' => $training_select,
                            'type' => $content_type,
                            'link_image' => $baseurl,
                            'link' => $url
                        );
                        $insert  = $this->Admin_model->inserttraining_content($datas);
                        if ($insert) {
                            $msg = "Training content successfully uploaded";
                        } else {
                            $msg = "Something went wrong when saving the file, please try again.";
                        }
                    }
                }
            } else {
                if (strtolower($file_type) == "mp4" || strtolower($file_type) == "divx" || strtolower($file_type) == "3gp" || strtolower($file_type) == "wmv") {
                    if (!$this->upload->do_upload("upload_content")) {
                        $msg = "Some error while upload a training content";
                    } else {
                        $baseurl = base_url() . 'upload/video-player.png';
                        $datas   = array(
                            'title' => $training_titles,
                            'training_id' => $training_select,
                            'type' => $content_type,
                            'link_image' => $baseurl,
                            'link' => $url
                        );
                        $insert  = $this->Admin_model->inserttraining_content($datas);
                        if ($insert) {
                            $msg = "Training content successfully uploaded";
                        } else {
                            $msg = "Something went wrong when saving the file, please try again.";
                        }
                    }
                } else {
                    $msg = "Uploaded file must matched with selected content Type";
                }
            }
        } else {
            $msg = "All Fields are Mandatory";
        }
        print_r($msg);
    }*/
    public function delete_trai_content()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->delete_training($id);
        echo $data;
    }


public

    function updated_quizes()
    {
        $msg = "";
        $id = $this->input->post('id1');
        $assess_id = $this->input->post('assess_id1');
        $certificate_name1 = $this->input->post('certificate_name1');
        $assessment_type1 = $this->input->post('assessment_type1');
        $product_name1 = $this->input->post('product_name1');
        $category1 = $this->input->post('category1');
        $title1 = $this->input->post('title1');
        $selecteds = $this->input->post('selecteds');
        $quiz_a = $this->input->post('quiz_a');
        $thrus_ha = $this->input->post('thrus_ha');
        if (!empty($assess_id) && !empty($certificate_name1) && !empty($assessment_type1))
        {
            if ($assessment_type1 == "product")
            {
                if (!empty($product_name1) && !empty($category1))
                {
                    $data_assestment = array(
                            'certificate_id' => $this->input->post('assess_id1') ,
                            'certificate_name' => $this->input->post('certificate_name1') ,
                            'certificate_type' => $this->input->post('assessment_type1') ,
                            'product_id' => $this->input->post('product_name1') ,
                            'cat_id' => $this->input->post('category1') ,
                            'title' => $this->input->post('title1') ,
                            'score' => $quiz_a,
                            'random_quiz' => $thrus_ha,
                        );
                        $data = $this->Admin_model->updated_quizes($data_assestment, $id);
                    if (!empty($selecteds))
                    {

                        // echo(count($selecteds));

                        $select = implode(',', $selecteds);
                        
                        if ($data == 1)
                        {
                            $datas = $this->Admin_model->delete_certificate_id($assess_id);
                            for ($i = 0; $i < count($selecteds); $i++)
                            {
                                $datass = $this->Admin_model->insert_certificate_id($assess_id, $selecteds[$i]);
                            }

                            $msg.= 1;
                        }
                        else
                        {
                            $msg.= 'Not Updated Successfully, Kindly Try again!';
                        }
                    }
                    else
                    {
                        if ($data == 1)
                        {
                            $msg= 1;
                        }
                        else
                        {
                            $msg= 'Not Updated Successfully, Kindly Try again!';
                        }
                    }
                    /*else
                    {
                        $msg.= "Training Reference is mandatory";
                    }*/
                }
                else
                {
                    $msg.= 'Product Category and Sub Category is mandatory';
                }
            }
            else
            {
                 $data_assestment = array(
                        'certificate_id' => $this->input->post('assess_id1') ,
                        'certificate_name' => $this->input->post('certificate_name1') ,
                        'certificate_type' => $this->input->post('assessment_type1') ,
                        'product_id' => '',
                        'cat_id' => '',
                        'title' => $this->input->post('title1') ,
                        'score' => $quiz_a,
                        'random_quiz' => $thrus_ha,
                    );
                    $data = $this->Admin_model->updated_quizes($data_assestment, $id);
                if (!empty($selecteds))
                {

                    // echo(count($selecteds));

                    $select = implode(',', $selecteds);
                   
                    if ($data == 1)
                    {
                        $datas = $this->Admin_model->delete_certificate_id($assess_id);
                        for ($i = 0; $i < count($selecteds); $i++)
                        {
                            $datass = $this->Admin_model->insert_certificate_id($assess_id, $selecteds[$i]);
                        }

                        $msg.= 1;
                    }
                    else
                    {
                        $msg.= 'Not Updated Successfully, Kindly Try again!';
                    }
                }
                 else
                    {
                        if ($data == 1)
                        {
                            $msg= 1;
                        }
                        else
                        {
                            $msg= 'Not Updated Successfully, Kindly Try again!';
                        }
                    }
                /*else
                {
                    $msg.= "Training Reference is mandatory";
                }*/
            }
        }
        else
        {
            $msg.= "All fields are Mandatory";
        }

        echo $msg;
    }

    public function updated_quizes_old()
    {
        $msg               = "";
        $id                = $this->input->post('id1');
        $assess_id         = $this->input->post('assess_id1');
        $certificate_name1 = $this->input->post('certificate_name1');
        $assessment_type1  = $this->input->post('assessment_type1');
        $product_name1     = $this->input->post('product_name1');
        $category1         = $this->input->post('category1');
        $title1            = $this->input->post('title1');
        $selecteds         = $this->input->post('selecteds');
        $quiz_a            = $this->input->post('quiz_a');
        $thrus_ha          = $this->input->post('thrus_ha');
        if (!empty($assess_id) && !empty($certificate_name1) && !empty($assessment_type1)) {
            if ($assessment_type1 == "product") {
                if (!empty($product_name1) && !empty($category1)) {
                    if (!empty($selecteds)) {
                        //echo(count($selecteds));
                        $select          = implode(',', $selecteds);
                        $data_assestment = array(
                            'certificate_id' => $this->input->post('assess_id1'),
                            'certificate_name' => $this->input->post('certificate_name1'),
                            'certificate_type' => $this->input->post('assessment_type1'),
                            'product_id' => $this->input->post('product_name1'),
                            'cat_id' => $this->input->post('category1'),
                            'title' => $this->input->post('title1'),
                            'score' => $quiz_a,
                            'random_quiz' => $thrus_ha,
                        );
                        $data            = $this->Admin_model->updated_quizes($data_assestment, $id);
                        if ($data == 1) {
                            $datas = $this->Admin_model->delete_certificate_id($assess_id);
                            for ($i = 0; $i < count($selecteds); $i++) {
                                $datass = $this->Admin_model->insert_certificate_id($assess_id, $selecteds[$i]);
                            }
                            $msg .= 1;
                        } else {
                            $msg .= 'Not Updated Successfully, Kindly Try again!';
                        }
                    } else {
                        $msg .= "Training Reference is mandatory";
                    }
                } else {
                    $msg .= 'Product Category and Sub Category is mandatory';
                }
            } else {
                if (!empty($selecteds)) {
                    //echo(count($selecteds));
                    $select          = implode(',', $selecteds);
                    $data_assestment = array(
                        'certificate_id' => $this->input->post('assess_id1'),
                        'certificate_name' => $this->input->post('certificate_name1'),
                        'certificate_type' => $this->input->post('assessment_type1'),
                        'product_id' => '',
                        'cat_id' => '',
                        'title' => $this->input->post('title1'),
                        'score' => $quiz_a,
                        'random_quiz' => $thrus_ha,
                    );
                    $data            = $this->Admin_model->updated_quizes($data_assestment, $id);
                    if ($data == 1) {
                        $datas = $this->Admin_model->delete_certificate_id($assess_id);
                        for ($i = 0; $i < count($selecteds); $i++) {
                            $datass = $this->Admin_model->insert_certificate_id($assess_id, $selecteds[$i]);
                        }
                        $msg .= 1;
                    } else {
                        $msg .= 'Not Updated Successfully, Kindly Try again!';
                    }
                } else {
                    $msg .= "Training Reference is mandatory";
                }
            }
        } else {
            $msg .= "All fields are Mandatory";
        }
        echo $msg;
    }
    public function sla_check()
    {
        if ($this->session->userdata('user_logged_in')) {
            $msg;
            $parent = $this->session->userdata('companyid');
            $datas  = $this->Admin_model->get_refnumber($parent);
            $msg    = $datas;
            /* $prod=$this->input->post('child');
            $cate=$this->input->post('g_child');
            if(is_array($prod)){
            for($i=0;$i<count($prod);$i++){
            $arr=array();
            $datas=$this->Super_admin->get_subcate($prod[$i]);
            $oneDimensionalArray = array_map('current', $datas);
            $arr= array_intersect($oneDimensionalArray,$cate);
            for($j=0;$j<count($arr);$j++){
            $datas=$this->Admin_model->get_refnumber($parent,$prod[$i],$arr[$j]);
            $datass=$this->Admin_model->get_ref_number($datas);
            //$msg[]="SLA Combination of the Product category ".$prod[$i]." and Sub Category ".$arr[$j]." with Editable Rights ".$datass['editable_rights'];
            $msg[]=$datass['editable_rights'];
            //print_r(json_encode($datass));
            }
            }
            }else{
            //for single product selection
            $prod=explode(",",$prod);
            for($i=0;$i<count($prod);$i++){
            $arr=array();
            $datas=$this->Super_admin->get_subcate($prod[$i]);
            $oneDimensionalArray = array_map('current', $datas);
            $arr= array_intersect($oneDimensionalArray,$cate);
            for($j=0;$j<count($arr);$j++){
            $datas=$this->Admin_model->get_refnumber($parent,$prod[$i],$arr[$j]);
            $datass=$this->Admin_model->get_ref_number($datas);
            //$msg[]="SLA Combination of the Product category ".$prod[$i]." and Sub Category ".$arr[$j]." with Editable Rights ".$datass['editable_rights'];
            //print_r(json_encode($datass));
            $msg[]=$datass['editable_rights'];
            }
            }
            } */
            print_r(json_encode($msg));
        } else {
            redirect('login/login');
        }
    }

    public function get_details_sla()
    {
        $result;
        $product_id         = $this->input->post('prod_id');
        $category_id        = $this->input->post('cate_id');
        $company_id         = $this->input->post('company_id');
        $priority           = $this->input->post('priority');
        $reso_hour          = $this->input->post('reso_hour');
        $reso_hour          = str_pad($reso_hour, 2, "0", STR_PAD_LEFT);
        $reso_minute        = $this->input->post('reso_minute');
        $reso_minute        = str_pad($reso_minute, 2, "0", STR_PAD_LEFT);
        $resolution_time    = $reso_hour . ':' . $reso_minute . ':' . '00';
        $res_hour           = $this->input->post('res_hour');
        $res_hour           = str_pad($res_hour, 2, "0", STR_PAD_LEFT);
        $res_minute         = $this->input->post('res_minute');
        $res_minute         = str_pad($res_minute, 2, "0", STR_PAD_LEFT);
        $response_time      = $res_hour . ':' . $res_minute . ':' . '00';
        $acc_hour           = $this->input->post('acc_hour');
        $acc_hour           = str_pad($acc_hour, 2, "0", STR_PAD_LEFT);
        $acc_minute         = $this->input->post('acc_minute');
        $acc_minute         = str_pad($acc_minute, 2, "0", STR_PAD_LEFT);
        $acceptance_time    = $acc_hour . ':' . $acc_minute . ':' . '00';
        $sla_target         = $this->input->post('sla_target');
        $mttr_target_hour   = $this->input->post('mttr_target_hour');
        $mttr_target_hour   = str_pad($mttr_target_hour, 2, "0", STR_PAD_LEFT);
        $mttr_target_minute = $this->input->post('mttr_target_minute');
        $mttr_target_minute = str_pad($mttr_target_minute, 2, "0", STR_PAD_LEFT);
        $mttr_target        = $mttr_target_hour . ':' . $mttr_target_minute . ':' . '00';
        //$product_id= explode(",", $product_id);
        //print_r(json_encode(count($product_id[])));
        //echo "<pre>"; print_r($product_id);
        //$prod=explode(',', $product_id);
        if (!empty($priority) && !empty($reso_hour) && !empty($reso_minute) && !empty($res_hour) && !empty($res_minute) && !empty($acc_hour) && !empty($acc_minute) && !empty($sla_target) && !empty($mttr_target_hour) && !empty($mttr_target_minute)) {
            $prod = explode(",", $product_id);
            $cate = explode(",", $category_id);
            for ($i = 0; $i < count($prod); $i++) {
                $arr                 = array();
                $datas               = $this->Super_admin->get_subcate($prod[$i]);
                $oneDimensionalArray = array_map('current', $datas);
                $arr                 = array_intersect($oneDimensionalArray, $cate);
                for ($j = 0; $j < count($arr); $j++) {
                    $ref_id           = $this->Super_admin->get_reference_id($company_id);
                    $data_combination = array(
                        'product' => $prod[$i],
                        'category' => $arr[$j],
                        'ref_id' => $ref_id,
                        'company_id' => $company_id,
                        'priority_level' => $priority
                    );
                    $data_mapping     = array(
                        'ref_id' => $ref_id,
                        'company_id' => $company_id,
                        'priority_level' => $priority,
                        'response_time' => $response_time,
                        'resolution_time' => $resolution_time,
                        'acceptance_time' => $acceptance_time,
                        'SLA_Compliance_Target' => $sla_target,
                        'mttr_target' => $mttr_target
                    );
                    $insert_slac      = $this->Admin_model->update_slac($data_combination, $data_mapping, $company_id);
                    if ($insert_slac == 1) {
                        $result[] = "SLA Combination of the Product category " . $prod[$i] . " and Sub Category " . $arr[$j] . " is successfully mapped" . "\r\n";
                    } else {
                        $result[] = "SLA Combination of the Product category " . $prod[$i] . " and Sub Category " . $arr[$j] . " is not mapped" . "\r\n";
                    }
                }
            }
        } else {
            $result = 2;
        }
        print_r(json_encode($result));
    }
    public function training_sded()
    {
        $id     = $this->input->post('id');
        $datass = $this->Admin_model->get_training_s($id);
        print_r(json_encode($datass));
    }
    public function sla_admin_check()
    {
        $product_id  = $this->input->post('prod_id');
        $category_id = $this->input->post('cate_id');
        $company_id  = $this->input->post('company_id');
        $priority    = $this->input->post('priority');
        $prod        = explode(",", $product_id);
        $cate        = explode(",", $category_id);
        for ($i = 0; $i < count($prod); $i++) {
            $arr   = array();
            $prod  = 'product_0021';
            $datas = $this->Super_admin->get_subcate($prod);
            print_r($datas);
            //$oneDimensionalArray = array_map('current', $datas);
            //print_r($datas);
        }
    }
    public function renewal_date()
    {
        $years    = $this->input->post('yr');
        $ext_date = $this->input->post('exp_date');
        /* if($years=="category1")
        {
        $date1 = strtotime(date("Y-m-d", strtotime($ext_date)) . " +".$yr."years");
        $date1 = strtotime(date("Y-m-d", strtotime($date1)) . " +".$mon."month");
        $date2 = date('Y-m-d',$date1);
        echo $date2;
        } */
        if ($years == "Quarterly Subscription") {

            $yr  = 0;
            $mon = 3;
            //$date = date("Y-m-d");
            //$date = date('Y-m-d', strtotime('+'.$yr.' years'));

            $time        = strtotime($ext_date);
            $CurrentDate = date('Y-m-d', $time);
            $date        = new DateTime($CurrentDate);
            $date->modify('3 month');

            echo $date->format('Y-m-d');


        } else if ($years == "Half Yearly Subscription") {
            $yr          = 0;
            $mon         = 6;
            $time        = strtotime($ext_date);
            $CurrentDate = date('Y-m-d', $time);
            $date        = new DateTime($CurrentDate);
            $date->modify('6 month');

            echo $date->format('Y-m-d');
        } else {
            $yr          = 1;
            $mon         = 0;
            $time        = strtotime($ext_date);
            $CurrentDate = date('Y-m-d', $time);
            $date        = new DateTime($CurrentDate);
            $date->modify('+1 year');

            echo $date->format('Y-m-d');
        }

    }

    public function set_unblock()
    {
        $admin_id          = $this->input->post('cust_id_cancel');
        $optional          = $this->input->post('optional');
        $service_regs      = $this->input->post('service_regs');
        $service_needs     = $this->input->post('service_needs');
        $service_registere = $this->input->post('service_registere');
        $tech_regs         = $this->input->post('tech_regs');
        $tech_needs        = $this->input->post('tech_needs');
        $tech_registere    = $this->input->post('tech_registere');
        /*$data=array(
        'technicians'=>$tech_needs,
        'service_desk'=>$service_needs,
        'tech_added'=>$tech_registere,
        'service_added'=>$service_registere,
        );
        print_r($data);
        exit;*/
        if ($optional == 'partially') {
            $data   = array(
                'technicians' => $tech_needs,
                'service_desk' => $service_needs
            );
            $datass = $this->Admin_model->set_unblock($data, $admin_id);
            if ($datass == 1) {
                echo "updated";
            } else {
                echo "Entire Subscription is not updated, kindly try again";
            }
        } else {
            $data   = array(
                'blocked_status' => 1
            );
            $datass = $this->Admin_model->set_unblock($data, $admin_id);
            if ($datass == 1) {
                echo "updated";
            } else {
                echo "Entire Subscription is not updated, kindly try again";
            }
        }
        /*$value=$this->Admin_model->set_unblock($admin_id);
        if($value==1){
        echo"updated";
        }
        else{
        echo "error";
        }*/
    }


    public function getdetails_productcategory()
    {
        $id   = $this->input->post('id');
        $data = $this->Admin_model->getdetails_productcategory($id);
        print_r(json_encode($data));
    }
    public function selected_training()
    {
        $id   = $this->input->post('id');
        $prod_id   = $this->input->post('prod_id');
        $cat_id   = $this->input->post('cat_id');
        $company_id   = $this->input->post('company_id');
        $data = $this->Admin_model->selected_training($id,$prod_id,$cat_id,$company_id);
        print_r(json_encode($data));
    }
    public function bulk_quiz()
    {
        if ($this->session->userdata('user_logged_in')) {
            $msg     = array();
            $c_id    = $this->session->userdata('companyid');
            $a_value = $this->input->post('a_value');
            $this->load->helper('url');
            $this->load->database();
            $this->load->model('Admin_model');
            $this->load->library('PHPExcel');
            $handle   = $_FILES["add_excel"]['tmp_name'];
            $filename = $_FILES["add_excel"]["name"];
            $pieces   = explode(".", $filename);
            if ($pieces[1] == "xlsx") {
                $objReader     = PHPExcel_IOFactory::createReader('Excel2007'); //2007PHP Excel_IOFactory
                $objPHPExcel   = $objReader->load($handle);
                $sheet         = $objPHPExcel->getSheet(0);
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestDataColumn();
                function toNumber($dest)
                {
                    if ($dest)
                        return ord(strtolower($dest)) - 96;
                    else
                        return 0;
                }
                function myFunction($s, $x, $y)
                {
                    $x = toNumber($x);
                    return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
                }
                for ($getContent = 2; $getContent <= $highestRow; $getContent++) {
                    $rowData   = $sheet->rangeToArray('A' . $getContent . ':' . $highestColumn . $getContent, NULL, TRUE, TRUE);
                    $content[] = $rowData;
                }
                $e      = 2;
                $result = "";
                for ($makeInsert = 0; $makeInsert < count($content); $makeInsert++) {
                    $sheetData = $objPHPExcel->getActiveSheet();
                    $c_id      = $this->session->userdata('companyid');
                    $question  = myFunction($sheetData, 0, $e);
                    $option_a  = myFunction($sheetData, 'A', $e);
                    $option_b  = myFunction($sheetData, 'B', $e);
                    $option_c  = myFunction($sheetData, 'C', $e);
                    $option_d  = myFunction($sheetData, 'D', $e);
                    $cor_ans   = myFunction($sheetData, 'E', $e);
                    if (empty($question) && empty($option_a) && empty($option_b) && empty($option_c) && empty($option_d) && empty($cor_ans)) {
                       $line=$makeInsert+1;    
$valu ="Quiz ".$line." Needed to fill all mandatory fields";                 
					  array_push($msg,array("response"=>$valu));
                    } else {
                        /*For tech id auto gen*/
                        $choicearray=array();
                        array_push($choicearray,trim($option_a));
                        array_push($choicearray,trim($option_b));
                        array_push($choicearray,trim($option_c));
                        array_push($choicearray,trim($option_d));

                      
                        if(!in_array(trim($cor_ans),$choicearray))
                        {
                            $line=$makeInsert+1;
                            $valu ="In Quiz ".$line." The answer should be any one of the options"; 
                            array_push($msg,array("response"=>$valu));
                        }
                        else{
                        $datass = $this->Admin_model->quiz_check();
                        $datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
                        $datass = $datass + 1;
                        $datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
                        $datass = "Quiz_" . $datass;

                        $ques_check = $this->Admin_model->question_check($question,$a_value,$c_id);
                        if($ques_check == 0){
                            $line1=$makeInsert+1;
                            $val2 = "Question " .$line1. " already exist";
                            array_push($msg, array("response"=>$val2));
                        }
                        else{
                        $datas  = array(
                            'quiz_id' => $datass,
                            'certificate_id' => $a_value,
                            'question' => $question,
                            'choice1' => $option_a,
                            'choice2' => $option_b,
                            'choice3' => $option_c,
                            'choice4' => $option_d,
                            'correct_ans' => $cor_ans,
                            'status' => 0
                        );
                        $data   = $this->Admin_model->bulk_quiz($datas);
                        if ($data == 1) {
                            array_push($msg,array("response"=>"Quiz Added Successfully"));
                        }
                    }
                    }
                }
                    $e++;
                }
            }
            echo json_encode($msg);
        } else {
            redirect('login/login');
        }
    }
    
    public function data_category_1()
    {
        $id                  = $this->input->post('id');
        $count_n             = $this->input->post('count_n');
        $data['user_detail'] = $this->Admin_model->data_category($id);
        $data['count_v']     = $count_n;
        print_r(json_encode($data));
    }
    public function get_product_company1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Super_admin');
        $company_id = $this->input->post('company_id');
        $datas      = $this->Admin_model->get_product_company1($company_id);
        echo json_encode($datas);
    }
    public function get_subproduct_company1()
     {
         $this->load->helper('url');
         $this->load->database();
         $this->load->model('Super_admin');
         $prod_id = $this->input->post('prod_id');
         $datas   = $this->Admin_model->get_subproduct_company1($prod_id);
         echo json_encode($datas);
     }
     public function checksubcategory()
     {
        $name=$this->input->post('name');
        $product_name=$this->input->post('product_name');
        $companyid=$this->input->post('companyid');
        $data=$this->Admin_model->checksubcategory($name,$product_name,$companyid);
        echo $data;
     }
     public function getdetails_content(){
        $id=$this->input->post('id');
        $data=$this->Admin_model->getdetails_content($id);
        echo json_encode($data);
     }
     public function get_view_trai(){
        $id=$this->input->post('id');
        $data=$this->Admin_model->get_view_trai($id);
        echo json_encode($data);
     }
     public function check_trai(){
        $name=$this->input->post('title_training');
        $product_type=$this->input->post('product_type');
        $company_id=$this->input->post('company_id');        
        $data=$this->Admin_model->check_trai($name,$product_type,$company_id);
        echo json_encode($data);
     }
    public function check_rights(){
        $id=$this->input->post('company_id');
        $data=$this->Admin_model->check_rights($id);
        echo json_encode($data);
    }
    public function check_rights_1(){
        $company_id=$this->input->post('company_id');
        $prod_id=$this->input->post('prod_id');        
        $data=$this->Admin_model->check_rights_1($company_id);
        if($data==1){
            echo json_encode("No Rights");
        }else{
            $data=$this->Admin_model->check_rights1($prod_id);
            echo json_encode($data);       
        }
    }
    public function check_rights_2(){
        $company_id=$this->input->post('company_id');
        $prod_id=$this->input->post('prod_id');        
        $data=$this->Admin_model->check_rights_2($company_id);
        echo json_encode($data);
        /*if($data==1){
            echo json_encode("No Rights");
        }else{
            $data=$this->Admin_model->check_rights2($prod_id);
            echo json_encode($data);       
        }*/
    }

    public function submit_edit_sla()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $company_id     = $this->input->post('company_id');
        $ref_id         = $this->input->post('ref_id');
        $response       = $this->input->post('response');
        $resolution     = $this->input->post('resolution');
        $acceptance     = $this->input->post('acceptance');
        $sla_compliance = $this->input->post('sla_compliance');
        $mttr           = $this->input->post('mttr');                
        $result = $this->Admin_model->submit_edit_sla($company_id, $ref_id, $response, $resolution, $acceptance, $sla_compliance, $mttr);
        if($result==1)
        {
            echo "SLA Updated Successfully";
        }
        else
        {
            echo "Error in Update!";
        }
    }
    public function submit_edit_sla1()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $company_id     = $this->input->post('product_id');
        $ref_id         = $this->input->post('ref_id');
        $response       = $this->input->post('response');
        $resolution     = $this->input->post('resolution');
        $acceptance     = $this->input->post('acceptance');
        $sla_compliance = $this->input->post('sla_compliance');
        $mttr           = $this->input->post('mttr');
        $result = $this->Admin_model->submit_edit_sla($company_id, $ref_id, $response, $resolution, $acceptance, $sla_compliance, $mttr);
        if($result==1)
        {
            echo "SLA Updated Successfully";
        }
        else
        {
            echo "Error in Update!";
        }
    }
    public function submit_edit_sla2()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin_model');
        $company_id     = $this->input->post('cat_id');
        $ref_id         = $this->input->post('ref_id');
        $response       = $this->input->post('response');
        $resolution     = $this->input->post('resolution');
        $acceptance     = $this->input->post('acceptance');
        $sla_compliance = $this->input->post('sla_compliance');
        $mttr           = $this->input->post('mttr');                
        $result = $this->Admin_model->submit_edit_sla($company_id, $ref_id, $response, $resolution, $acceptance, $sla_compliance, $mttr);
        if($result==1)
        {
            echo "SLA Updated Successfully";
        }
        else
        {
            echo "Error in Update!";
        }
    }
    public function check_slas(){
        $prod_id=$this->input->post('product_id');
        $id=$this->input->post('company_id');        
        $datas=$this->Admin_model->check_slas($id,$prod_id);
        echo json_encode($datas);
    }        
    public function check_slas1(){          
        $prod_id=$this->input->post('prod_id');
        $id=$this->input->post('company_id');
        $datas=$this->Admin_model->check_slas1($id,$prod_id);
        echo json_encode($datas);
    }        
    public function check_slas2(){          
        $id=$this->input->post('company_id');
        $datas=$this->Admin_model->check_slas2($id);
        echo json_encode($datas);
    }
    public function che_ck(){
        $id=$this->input->post('company_id');
        $delete_rec=$this->Admin_model->delete_rec($id);
        if($delete_rec){
            foreach($delete_rec as $d){
                $dele=$this->Admin_model->dele($d);
                if($dele == 1){
                    $dele=$this->Admin_model->deles($d);
                    echo $dele;                     
                }
            }
        }           
    }
    public function chec_k(){
        $id=$this->input->post('company_id');
        $delete_rec=$this->Admin_model->delete_recds($id);
        if($delete_rec){
            foreach($delete_rec as $d){
                $dele=$this->Admin_model->dele_s($d);
                if($dele == 1){
                    $dele=$this->Admin_model->deles_s($d);
                    echo $dele;                     
                }
            }
        }           
    }
    public function get_spare_loc(){
        $id=$this->input->post('id');
		 $companyid = $this->session->userdata('companyid');
        $data=$this->Admin_model->get_spare_loca($id,$companyid);
        echo json_encode($data);
    }

    public

    function update_spare_locations()
    {
        $select_option = $this->input->post('select_option');
        $location_id = $this->input->post('location_id');
        $day_del = $this->input->post('day_del');
        $companyids = $this->input->post('companyids');
        $data = $this->Admin_model->update_spare_locations($select_option, $location_id, $day_del, $companyids);
        echo $data;
    }
    public function get_count(){
        $assess_id=$this->input->post('assess_id1');
        $get_count=$this->Admin_model->get_count($assess_id);
        echo $get_count;
    }
	public function company_ifslaexists(){
		$id=$this->input->post('company_id');
		$data=$this->Admin_model->company_ifslaexists($id);
		echo json_encode($data);
	}
	public function product_ifslaexists(){
		$id=$this->input->post('company_id');
		$prod_id=$this->input->post('prod_id');
		if($id=="" && $prod_id!="")
		{
			$id=$this->Super_admin->select_com($prod_id);
		}
		$data=$this->Admin_model->product_ifslaexists($id,$prod_id);
		echo json_encode($data);
	}
	public function category_ifslaexists(){
		$id=$this->input->post('company_id');
		$cat_id=$this->input->post('prod_id');		
		$data=$this->Admin_model->category_ifslaexists($id,$cat_id);
		echo json_encode($data);
	}
public

    function service_group()
    {
        if ($this->session->userdata('user_logged_in'))
        {

            // $datass       = $this->session->userdata('session_username');
            // $datas        = $this->session->userdata('companyname');
            // $datas        = $this->session->userdata('companyid');
            // $data['pass'] = $this->Admin_model->get_employee($datass, $datas);
            // $data['spare_loc']   =

            $this->load->view('servicegroup');
        }
        else
        {
            redirect('login/login');
        }
    }

    public

    function getservice_groupall()
    {
        $datas = $this->session->userdata('companyid');
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Servicegroup_model');
        $query1 = $this->Servicegroup_model->get_servicegroup($datas);
        $data = array();

        // print_r($query1);
        //  echo 'count'.count($query1).'  ';
        //  echo "hello";

        if (count($query1) > 0)
        {
            $i = 0;
            for ($k = 0; $k < count($query1); $k++)
            {

                // echo $query1[$k]['service_group_id'];
                // echo $query1[$k]['service_group'];

                $i = $i + 1;
                $button = '';
                $id = $query1[$k]['service_group_id'];
                $name = $query1[$k]['service_group'];

                //    echo 'hi';

                $button = "<button class='btn btn-circle blue btn-outline btn-sm'  onclick='edit_details(" . $id . ")'><i class='fa fa-pencil' aria-hidden='true'></i></button>";

                //  echo 'hello';

                $button = $button . "<button class='btn btn-circle red btn-outline btn-sm'  onclick='delete_details(" . $id . ")'><i class='fa fa-trash' aria-hidden='true'></i>
</button>";

                // echo 'hohello';

                $data[] = array(
                    $i,
                    ucfirst($query1[$k]['service_group']) ,
                    $button
                );
            }

            /* foreach($query1->result() as $row) {
            }*/
        }

        // print_r($data);
        // echo "hello";

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($query1) ,
            "recordsFiltered" => count($query1) ,
            "data" => $data
        );

        // echo "hello";

        print_r(json_encode($output));
    }

    public

    function insertservicegroup()
    {

        // echo 1;

        $servicegroupid = $this->input->post('servicegroupid');
        $servicegroupname = $this->input->post('servicegroupname');
        $datas = $this->session->userdata('companyid');
        $this->load->model('Servicegroup_model');
        $query1 = $this->Servicegroup_model->insertservicegroup($datas, $servicegroupid, $servicegroupname);

        // print_r($query1);

        print_r(json_encode($query1));
    }

    public

    function deleteservicegroup()
    {
        $servicegroupid = $this->input->post('servicegroupid');
        $datas = $this->session->userdata('companyid');
        $this->load->model('Servicegroup_model');
        $query1 = $this->Servicegroup_model->deleteservicegroup($datas, $servicegroupid);
        print_r(json_encode($query1));
    }

    public

    function editservicegroup()
    {
        $servicegroupid = $this->input->post('servicegroupid');
        $datas = $this->session->userdata('companyid');
        $this->load->model('Servicegroup_model');
        $query1 = $this->Servicegroup_model->editservicegroup($datas, $servicegroupid);
        print_r(json_encode($query1));
    }
	public function remaining_days()
{
$this->load->helper('url');
$this->load->database();
$this->load->model('Admin_model');
$company_id = $this->input->post('company_id');
$result = $this->Admin_model->remaining_days($company_id);
echo $result;
}
}
