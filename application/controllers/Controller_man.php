<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class controller_man extends CI_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->helper(array(
            'url',
            'cookie'
        ));
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->load->helper('url');
        $this->load->database();
        $username           = $this->input->post('username');
        $password           = $this->input->post('password');
        $device_token       = $this->input->post('device_token');
        $reg_id             = $this->input->post('reg_id');
        $encrypted_password = $password;
        $this->load->model('model_man');
        if ($username != '') {
            $result = $this->model_man->check_login($username);
            if ($password != '') {
                $result_1 = $this->encrypt->decode($result);
                if ($result_1 == $encrypted_password) {
                    
                    if ($device_token != '' || $reg_id != '') {
                        $this->load->model('model_man');
                        $result2 = $this->model_man->store_id($device_token, $reg_id, $username);
                        if ($result2 == 1) {
                            $userid   = $this->model_man->get_userid($username);
                            $empid    = $this->model_man->get_empid($username);
                            $name     = $this->model_man->get_username($username);
                            $image    = $this->model_man->get_image($username);
                            $mobile   = $this->model_man->get_mobile($username);
                            $location = $this->model_man->get_location($username);
                            $json     = array(
                                "status" => 1,
                                "msg" => "Logged in Successfully!!!",
                                "man_id" => $userid,
                                "employee_id" => $empid,
                                "man_name" => $name,
                                "image" => $image,
                                "mobile" => $mobile,
                                "location" => $location
                            );
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Error storing device token/Register ID!!!"
                            );
                        }
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide Device Token/Register ID!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide correct details!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide Password!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide Username!!!"
            );
        }
        echo json_encode($json);
    }
    public function leaderboard()
    {
        $this->load->helper('url');
        $this->load->database();
        $level  = $this->input->post('level');
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        $json = array();
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                
                $company_id  = $this->model_man->get_company($this->input->post('userid'));
                $leaderboard = $this->model_man->leaderboard($company_id, $level);
                $json        = array(
                    "status" => 1,
                    "result" => $leaderboard
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function ongoing_tickets()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        $json = array();
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id       = $this->model_man->get_company($this->input->post('userid'));
                $work_in_progress = $this->model_man->work_in_progress($company_id);
                $escallated       = $this->model_man->escallated($company_id);
                $spare_request    = $this->model_man->spare_request($company_id);
                $json             = array(
                    "status" => 1,
                    "work_in_progress" => $work_in_progress,
                    "escallated" => $escallated,
                    "spare_request" => $spare_request
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function new_tickets()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        $json = array();
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $assigned   = $result = $this->model_man->load_assigned($company_id);
                $unassigned = $this->model_man->load_assigned($company_id);
                $accepted   = $this->model_man->load_accepted($company_id);
                $deferred   = $this->model_man->load_deferred($company_id);
                $json       = array(
                    "status" => 1,
                    "assigned" => $assigned,
                    "unassigned" => $unassigned,
                    "accepted" => $accepted,
                    "deferred" => $deferred
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id               = $this->model_man->get_company($this->input->post('userid'));
                $data['new_request']      = $this->model_man->disply_Spare($company_id);
                $data['accepted_request'] = $this->model_man->disply_accept_Spare($company_id);
                $data['rejected_request'] = $this->model_man->disply_reject_Spare($company_id);
                $json                     = array(
                    "status" => 1,
                    "result" => $data
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function imprest_spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id               = $this->model_man->get_company($this->input->post('userid'));
                $data['new_request']      = $this->model_man->impressed_Spare($company_id);
                $data['accepted_request'] = $this->model_man->impressed_accept_Spare($company_id);
                $json                     = array(
                    "status" => 1,
                    "result" => $data
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    
    public function action_spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $status    = $this->input->post('status');
        $userid    = $this->input->post('userid');
        $ticket_id = $this->input->post('ticket_id');
        $this->load->model('model_man');
        $this->load->model('reimbursement');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                if ($ticket_id == "") {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide Ticket ID!!!"
                    );
                } else {
                    if ($status == "") {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide Status!!!"
                        );
                    } else {
                        
                        $company_id = $this->model_man->get_company($this->input->post('userid'));
                        if ($status == "Accepted") {
                            $changes = 11;
                        } else if ($status == "Rejected") {
                            $changes = 17;
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide correct status!!!"
                            );
                            echo json_encode($json);
                            return;
                        }
                        $data  = array(
                            'current_status' => $changes
                        );
                        $where = array(
                            'all_tickets.ticket_id' => $ticket_id,
                            'all_tickets.company_id' => $company_id
                        );
                        $datas = $this->reimbursement->statuschange($data, $where);
                        
                        if ($datas == 1) {
                            if ($status == "Accepted") {
                                $res  = 'You have accepted Spare(s) for ticket ' . $ticket_id;
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            } else if ($status == "Rejected") {
                                $res  = 'You have rejected Spare(s) for ticket ' . $ticket_id;
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            }
                        }
                    }
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function action_imprest_spare_request()
    {
        $this->load->helper('url');
        $this->load->database();
        $status     = $this->input->post('status');
        $userid     = $this->input->post('userid');
        $tech_id    = $this->input->post('tech_id');
        $imprest_id = $this->input->post('p_id');
        $this->load->model('model_man');
        $this->load->model('reimbursement');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                if ($imprest_id == "") {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide Imprest ID!!!"
                    );
                } else {
                    if ($tech_id == "") {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide Technician ID!!!"
                        );
                    } else {
                        if ($status == "") {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide Status!!!"
                            );
                        } else {
                            
                            $company_id = $this->model_man->get_company($this->input->post('userid'));
                            if ($status == "Accepted") {
                                $changes = 1;
                            } else if ($status == "Rejected") {
                                $changes = 2;
                            } else {
                                $json = array(
                                    "status" => 0,
                                    "msg" => "Please provide correct status!!!"
                                );
                                echo json_encode($json);
                                return;
                            }
                            $data  = array(
                                'status' => $changes
                            );
                            $where = array(
                                'id' => $imprest_id,
                                'company_id' => $company_id,
                                'tech_id' => $tech_id
                            );
                        //    if ($changes == 1) {
                                $datas = $this->reimbursement->impressed_accept($where, $tech_id, $company_id);
                                if ($datas == 1) {
                                     $datas1 = $this->reimbursement->impressed_accept_status($data, $where);
                                } else {
                                  echo  $datas1 = $this->reimbursement->impressed_accept_status($data, $where);
                                }
                                
                                if ($datas1 == 1) {
                                    if ($status == "Accepted") {
                                        $res  = 'You have accepted Imprest Spare(s) request';
                                        $json = array(
                                            "status" => 1,
                                            "result" => $res
                                        );
                                    } else if ($status == "Rejected") {
                                        $res  = 'You have rejected  Imprest Spare(s) request';
                                        $json = array(
                                            "status" => 1,
                                            "result" => $res
                                        );
                                    }
                                }
								else
								{
									$json = array(
										"status" => 0,
										"msg" => "Error during uploading!!!"
									);
								}
                          //  }
                        }
                    }
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function contracts()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id                = $this->model_man->get_company($this->input->post('userid'));
                $data['new_contract']      = $this->reimbursement->disply_contract($company_id);
                $data['accepted_contract'] = $this->reimbursement->disply_accept_contract($company_id);
                $json                      = array(
                    "status" => 1,
                    "result" => $data
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    
    public function action_contract()
    {
        $this->load->helper('url');
        $this->load->database();
        $status = $this->input->post('status');
        $userid = $this->input->post('userid');
        $action = $this->input->post('ticket_id');
        $this->load->model('model_man');
        $this->load->model('reimbursement');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                if ($action == "") {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide Ticket ID!!!"
                    );
                } else {
                    if ($status == "") {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide Status!!!"
                        );
                    } else {
                        
                        $company_id = $this->model_man->get_company($this->input->post('userid'));
                        if ($status == "Accepted") {
                            $changes = 20;
                        } else if ($status == "Rejected") {
                            $changes = 19;
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide correct status!!!"
                            );
                            echo json_encode($json);
                            return;
                        }
                        $data  = array(
                            'current_status' => $changes
                        );
                        $datas = $this->reimbursement->contract_change($data, $action, $company_id);
                        
                        if ($datas == 1) {
                            if ($status == "Accepted") {
                                $res  = 'You have accepted Contract for ticket ' . $action;
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            } else if ($status == "Rejected") {
                                $res  = 'You have rejected Contract for ticket ' . $action;
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            }
                        }
                    }
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function reimbursement()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id              = $this->model_man->get_company($this->input->post('userid'));
                $data['new_claims']      = $this->reimbursement->disply_employee($company_id);
                $data['accepted_claims'] = $this->reimbursement->accepted($company_id);
                $data['rejected_claims'] = $this->reimbursement->rejected($company_id);
                $json                    = array(
                    "status" => 1,
                    "result" => $data
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function view_reimb_form()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid  = $this->input->post('userid');
        $tech_id = $this->input->post('tech_id');
        $from    = $this->input->post('from');
        $to      = $this->input->post('to');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                if ($from != '') {
                    if ($to != '') {
                        $data  = $this->reimbursement->view_form($tech_id, $company_id, $from, $to);
                        $json1 = "";
                        if (!empty($data)) {
                            $json1 = '<html><body><form><p>Employee ID:' . $data[0]['employee_id'] . '</p><p> Technician name:' . $data[0]['first_name'] . '</p><p> Technician name:' . $from . '</p><p> Technician name:' . $to . '</p><br/><br/></form><table style="width:100%;border: 0px solid black; border-collapse: collapse;"><tr style="border: 1px solid black; background-color: #eee;"><th style="border: 1px solid black">Ticket ID</th><th style="border: 1px solid black">Source Address</th><th style="border: 1px solid black">Desctination Address</th><th style="border: 1px solid black">Mode of Travel</th><th style="border: 1px solid black">No. of Kms</th><th style="border: 1px solid black">Claim amount</th><th style="border: 1px solid black">Attachments</th></tr>';
                            
                            foreach ($data as $row) {
                                
                                $json1 = $json1 . '<tr style="border: 1px solid black; background-color: #eee;"><td style="border: 1px solid black">' . $row['ticket_id'] . '</td><td style="border: 1px solid black">' . $row['source_Address'] . '</td><td style="border: 1px solid black">' . $row['destin_Address'] . '</td><td style="border: 1px solid black">' . $row['mode_of_travel'] . '</td><td style="border: 1px solid black">' . $row['no_of_km_travelled'] . '</td><td style="border: 1px solid black">' . $row['travelling_charges'] . '</td><td style="border: 1px solid black">';
                                //if($row['view']!="")
                                //	{
                                $json1 = $json1 . '<a href="' . base_url() . $row['view'] . '"><img src=""  alt="view" height="300px" width="95%"/></a></td></tr>';
                                //	}
                                //	else
                                //	{
                                //		$json1=$json1.'No Attachments</td></tr>';
                                //	}
                            }
                            $json1 = $json1 . '</table></body></html>';
                        }
                        $json = array(
                            "status" => 1,
                            "result" => $json1
                        );
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide to date!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide from date!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    
    public function view_reimb_image()
    {
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        //$datass=$this->session->userdata('session_username');
        //$data['pass']=$this->Admin_model->get_employee($datass,$datas);
        $userid = $this->input->post('userid');
        $status = $this->input->post('reim_id');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                if ($status != '') {
                    
                    $data = $this->reimbursement->view($status, $company_id);
                    if (!empty($data)) {
                        $data['view'] = base_url() . "index.php?/" . $data['view'];
                    }
                    $json = array(
                        "status" => 1,
                        "result" => $data
                    );
                    
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide reimbursement id!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function action_claim()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid  = $this->input->post('userid');
        $tech_id = $this->input->post('tech_id');
        $from    = $this->input->post('from');
        $to      = $this->input->post('to');
        $status  = $this->input->post('status');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                if ($from != '') {
                    if ($to != '') {
                        if ($status == "Accepted") {
                            $changes = 1;
                            $data    = array(
                                'action' => $changes,
                                'status' => 13
                            );
                        } else if ($status == "Rejected") {
                            $changes = 2;
                            $data    = array(
                                'action' => $changes,
                                'status' => 13
                            );
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide correct status!!!"
                            );
                            echo json_encode($json);
                            return;
                        }
                        $where = array(
                            'technician_id' => $tech_id,
                            'start_date' => $from,
                            'end_date' => $to,
                            'company_id' => $company_id
                        );
                        $datas = $this->reimbursement->claimchange($data, $where);
                        
                        if ($datas == 1) {
                            if ($status == "Accepted") {
                                $res  = 'Accepted Successfully';
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            } else if ($status == "Rejected") {
                                $res  = 'Rejected Successfully';
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            }
                        }
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide to date!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide from date!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    
    public function view_attendance()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $region     = "south";
                $area       = "chennai";
                $data       = $this->reimbursement->disply_attendance($company_id, $region, $area);
                $data       = $data->result_array();
                $json1      = "";
                if (!empty($data)) {
                    $json1 = '<html><body><table style="width:100%;border: 0px solid black; border-collapse: collapse;"><tr style="border: 1px solid black; background-color: #eee;"><th style="border: 1px solid black">Employee ID</th><th style="border: 1px solid black">Technician Name</th><th style="border: 1px solid black">Location</th><th style="border: 1px solid black">Availability</th><th style="border: 1px solid black">In Time</th><th style="border: 1px solid black">Out Time</th></tr>';
                    
                    foreach ($data as $row) {
                        if ($row['availability'] == 1) {
                            $avail = "Available";
                        } else {
                            $avail = "Unavailable";
                        }
                        
                        $json1 = $json1 . '<tr style="border: 1px solid black; background-color: #eee;"><td style="border: 1px solid black">' . $row['employee_id'] . '</td><td style="border: 1px solid black">' . $row['first_name'] . '</td><td style="border: 1px solid black">' . $row['location'] . '</td><td style="border: 1px solid black">' . $avail . '</td><td style="border: 1px solid black">' . $row['in_time'] . '</td><td style="border: 1px solid black">' . $row['out_time'] . '</td>';
                        //if($row['view']!="")
                        //	{
                        $json1 = $json1 . '</tr>';
                        //	}
                        //	else
                        //	{
                        //		$json1=$json1.'No Attachments</td></tr>';
                        //	}
                    }
                    $json1 = $json1 . '</table></body></html>';
                }
                $json = array(
                    "status" => 1,
                    "result" => $json1
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function productivity()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $region     = "south";
                $area       = "chennai";
                $filter     = "";
                $product    = "";
                $da         = $this->reimbursement->csat_insights($company_id, $filter, $product, $region, $area);
                if ($da[2]['value'] != 0) {
                    $data['first_call'] = $da[2]['value'];
                } else {
                    $data['first_call'] = 0;
                }
                if ($da[3]['value'] != 0) {
                    $data['sla_complaince'] = $da[3]['value'];
                } else {
                    $data['sla_complaince'] = 0;
                }
                $da1                    = $this->reimbursement->sla_complaince($company_id, $region, $area);
                $data['sla_complaince'] = $da1[0]['percent'];
                $data['call_closed']    = $this->reimbursement->callclosed($company_id, $region, $area);
                $data['escallated']     = $this->reimbursement->escallatedcalls($company_id, $region, $area);
                $json                   = array(
                    "status" => 1,
                    "result" => $data
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function load_tech()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $product_id = $this->input->post('product_id');
                $cat_id     = $this->input->post('cat_id');
                $tech_id    = $this->input->post('tech_id');
                $address    = $this->input->post('location');
                if ($product_id != "") {
                    if ($cat_id != "") {
                        if ($address != "") {
                            
                            // Get JSON results from this request
                            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                            
                            // Convert the JSON to an array
                            $geo = json_decode($geo, true);
                            
                            if ($geo['status'] == 'OK') {
                                // Get Lat & Long
                                $latitude  = $geo['results'][0]['geometry']['location']['lat'];
                                $longitude = $geo['results'][0]['geometry']['location']['lng'];
                                $tech      = $this->model_man->get_tech($company_id, $latitude, $longitude, $product_id, $cat_id, $tech_id);
                                $json      = array(
                                    "status" => 1,
                                    "result" => $tech
                                );
                            } else {
                                $json = array(
                                    "status" => 0,
                                    "msg" => "Error Finding location!!!"
                                );
                            }
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide location!!!"
                            );
                        }
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide cat_id!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide product_id!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function change_tech()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id    = $this->model_man->get_company($this->input->post('userid'));
                $technician_id = $this->input->post('technician_id');
                $employee_id   = $this->input->post('employee_id');
                $ticket_id     = $this->input->post('ticket_id');
                $count         = $this->input->post('count');
                $prod          = $this->input->post('product_id');
                $cat           = $this->input->post('cat_id');
                if ($prod != "") {
                    if ($cat != "") {
                        if ($count != "") {
                            if ($ticket_id != "") {
                                if ($employee_id != "") {
                                    $this->load->model('model_service');
                                    $result = $this->model_service->change_tech($company_id, $technician_id, $ticket_id, $count);
                                    if ($result == 1) {
                                        $re = $this->model_service->tkt_ass($company_id, $technician_id, $ticket_id, $count, $prod, $cat);
                                        if ($re == 1) {
                                            $res    = $ticket_id . " is assigned to " . $employee_id;
                                            $json   = array(
                                                "status" => 1,
                                                "result" => $res
                                            );
                                            $notify = $ticket_id . " is assigned to " . $employee_id;
                                            $this->load->model('Pushnotify');
                                            $tech_id      = $technician_id;
                                            $token        = $this->Pushnotify->get_tech_token($tech_id);
                                            $device_token = $token['device_token'];
                                            $reg_id       = $token['reg_id'];
                                            if ($device_token == "") {
                                                $this->Pushnotify->send_notification($reg_id, $notify, "New Ticket Assigned");
                                            } else {
                                                $this->Pushnotify->iOS($notify, "New Ticket Assigned", $device_token);
                                            }
                                        } else {
                                            $res  = "Error while assigning!!!";
                                            $json = array(
                                                "status" => 0,
                                                "msg" => $res
                                            );
                                        }
                                    } else {
                                        $res  = "Error while assigning!!!";
                                        $json = array(
                                            "status" => 0,
                                            "msg" => $res
                                        );
                                    }
                                } else {
                                    $json = array(
                                        "status" => 0,
                                        "msg" => "Please provide employee_id!!!"
                                    );
                                }
                            } else {
                                $json = array(
                                    "status" => 0,
                                    "msg" => "Please provide ticket_id!!!"
                                );
                            }
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide count!!!"
                            );
                        }
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide cat_id!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide product_id!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
    public function tickets_count()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $result     = $this->model_man->tickets_count($company_id);
                $json       = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        
        echo json_encode($json);
    }
    
    public function tickets_status()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $result     = $this->model_man->tickets_status($company_id);
                $json       = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        
        echo json_encode($json);
    }
	
    public function spare_transfer()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
		$result['yet_to_approve']=array();
		$result['reject']=array();
		$result['approve']=array();
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $result1['yet_to_approve']= $this->reimbursement->spare_transfer($company_id);
				$result1['approve']= $this->reimbursement->spare_transfer_accept($company_id);
				$result1['reject']= $this->reimbursement->Spare_transfer_reject($company_id);
				foreach ($result1['yet_to_approve'] as $row) {
                $spare_array = json_decode($row['spare_array'], true);
				if(!empty($spare_array))
				{
                $row['spare_array']=$spare_array;
				}
				else
				{
                $row['spare_array']=array();
				}
				array_push($result['yet_to_approve'],$row);
		}
				foreach ($result1['reject'] as $row1) {
                $spare_array = json_decode($row1['spare_array'], true);
                if(!empty($spare_array))
				{
                $row1['spare_array']=$spare_array;
				}
				else
				{
                $row1['spare_array']=array();
				}
				array_push($result['reject'],$row1);
		}
				foreach ($result1['approve'] as $row2) {
                $spare_array = json_decode($row2['spare_array'], true);
                if(!empty($spare_array))
				{
                $row2['spare_array']=$spare_array;
				}
				else
				{
                $row2['spare_array']=array();
				}
				array_push($result['approve'],$row2);
		}
                $json       = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        
        echo json_encode($json);
    }
	public function action_transfer()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid  = $this->input->post('userid');
        $tech_id = $this->input->post('tech_id');
        $id    = $this->input->post('id');
        $status = $this->input->post('status');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
        if ($userid != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                if ($id != '') {
                    if ($tech_id != '') {
                        if ($status == "Accepted") {
                            $changes = 1;
                            $data    = array(
                                'status' => $changes
                            );
                        } else if ($status == "Rejected") {
                            $changes = 2;
                            $data    = array(
                                'status' => $changes
                            );
                        } else {
                            $json = array(
                                "status" => 0,
                                "msg" => "Please provide correct status!!!"
                            );
                            echo json_encode($json);
                            return;
                        }
                       $where= array(
						'id'=>$id,
						'company_id'=>$company_id,'from_tech_id'=>$tech_id
						 );
                        $datas   = $this->reimbursement->transfer_accept($where,$data);
                        
                        if ($datas == 1) {
                            if ($status == "Accepted") {
                                $res  = 'Accepted Successfully';
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            } else if ($status == "Rejected") {
                                $res  = 'Rejected Successfully';
                                $json = array(
                                    "status" => 1,
                                    "result" => $res
                                );
                            }
                        }
                    } else {
                        $json = array(
                            "status" => 0,
                            "msg" => "Please provide Tech id!!!"
                        );
                    }
                } else {
                    $json = array(
                        "status" => 0,
                        "msg" => "Please provide id!!!"
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide userid!!!"
            );
        }
        echo json_encode($json);
    }
	public function to_tech_id()
    {
        $this->load->helper('url');
        $this->load->database();
        $userid = $this->input->post('userid');
        $to_tech_id = $this->input->post('to_tech_id');
        $this->load->model('reimbursement');
        $this->load->model('model_man');
       if ($userid != '' || $to_tech_id != '') {
            $check_user = $this->model_man->check_user($this->input->post('userid'));
            if ($check_user == 1) {
                $company_id = $this->model_man->get_company($this->input->post('userid'));
                $result  = $this->reimbursement->to_tech_id($to_tech_id);
                $json       = array(
                    "status" => 1,
                    "result" => $result
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Please provide correct userid!!!"
                );
            }
        } else {
            $json = array(
                "status" => 0,
                "msg" => "Please provide all details!!!"
            );
        }
        
        echo json_encode($json);
    }
}