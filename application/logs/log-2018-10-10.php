<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2018-10-10 12:39:47 --> Query error: Expression #84 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'fieldpro_v1.customer.id' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by - Invalid query: SELECT *, `technician`.`first_name` as `tech_name`, `customer`.`contact_number` as `cont_num`, `all_tickets`.`town` as `town_tkt`, `technician`.`email_id` as `tech_email_id`, `technician`.`contact_number` as `tech_contact`, `all_tickets`.`last_update` as `last`, `all_tickets`.`priority` as `prio`, `all_tickets`.`assigned_time` as `assigned`, `all_tickets`.`latitude` as `latitud`, `all_tickets`.`longitude` as `longitud`, `all_tickets`.`address` as `add`, `all_tickets`.`image` as `ticket_img`, `all_tickets`.`company_id` as `companyid`
FROM `all_tickets`
JOIN `customer` ON `customer`.`customer_id`=`all_tickets`.`cust_id`
JOIN `call_tag` ON `call_tag`.`id`=`all_tickets`.`call_tag`
JOIN `product_management` ON `product_management`.`product_id`=`all_tickets`.`product_id`
JOIN `category_details` ON `category_details`.`cat_id`=`all_tickets`.`cat_id`
JOIN `technician` ON `technician`.`technician_id`=`all_tickets`.`tech_id`
WHERE `all_tickets`.`tech_id` = 'company1_Tech_0015'
AND `all_tickets`.`current_status` = 1
AND `all_tickets`.`amc_id` = ''
AND  `all_tickets`.`cust_preference_date` LIKE '%2018-10-10%' ESCAPE '!'
GROUP BY `all_tickets`.`ticket_id`, `customer`.`customer_id`
ORDER BY `all_tickets`.`last_update` DESC
