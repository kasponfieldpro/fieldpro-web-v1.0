<?php 
   class Login_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
	  public function login_user($data){
		  $this->db->insert('login',$data);
		  return true;
	  }	
      public function change_password($where,$pwd)
	  {
	  	$this->db->where($where);
       $this->db->update('login',$pwd);
	  	return true;
	  }   

	  public function ForgotPassword($email)
	  {
			 $this->db->select('username');
			 $this->db->from('login'); 
			 $this->db->where('username', $email); 
			 $query=$this->db->get();
			 return $query->row_array();
	  }

	


	  public function check_valid_user_or_not()
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');		  
			$c_flag = $this->input->post('rememberme');
		  if($username=="superadmin@kaspontech.com"){
				$this->db->select('*');
				$this->db->from('login');
				$this->db->where('username', $username);	
				$query=$this->db->get();
				if($query->num_rows()>0)
				{
					$res = $query->row_array();
					$db_password = $this->encrypt->decode($res['password']);
					if($password==$db_password)
					{												// Create Session
							$session_array = array(
									'user_logged_in'	=>	TRUE,
									'session_username' 	=> 	$username,
									'session_userid'	=>	$res['id'],	
									'role'				=>	$res['user_type'],
							);
							$this->session->set_userdata($session_array);
							return $session_array;
							// Create Cookie
							if( $c_flag == 1)	
							{
									//Set the cookie and the cookies value,expires in one hour
									$this->input->set_cookie('username', $username, 3600);
									$this->input->set_cookie('password', $password, 3600);
							}
							else
							{
									// Clear Cookie
									delete_cookie("username");
									delete_cookie("password");
							}				
								return 1;
							
					}
					else{
							return false;
					}					
				}
				else
				{
						return false;
				}
		  }
		  else
		  {
			  $this->db->select('*');
			$this->db->from('login');
			$this->db->where('username', $username);	
			$query=$this->db->get();
			if($query->num_rows()>0)
			{
				$res = $query->row_array();
				$db_password = $this->encrypt->decode($res['password']);
				if($password==$db_password)
				{
					$this->db->select('*');
					$this->db->from('admin_user');
					$this->db->where('company_id', $res['companyid']);	
					$this->db->order_by('admin_id','desc');
					$this->db->limit(1);
					$query1=$this->db->get();
					$res1 = $query1->row_array();
					if($query1->num_rows()>0){
						$res1 = $query1->row_array(); 
						if($res1['blocked_status']==0){
							// Create Session
							$session_array = array(
									'user_logged_in'	=>	TRUE,
									'session_username' 	=> 	$username,
									'session_userid'	=>	$res['id'],						
									'companyid'			=>	$res['companyid'],						
									'companyname'		=>	$res['companyname'],
									'role'				=>	$res['user_type'],
							);
							$this->session->set_userdata($session_array);
							return $session_array;

							// Create Cookie
							if( $c_flag == 1)	
							{
									//Set the cookie and the cookies value,expires in one hour
									$this->input->set_cookie('username', $username, 3600);
									$this->input->set_cookie('password', $password, 3600);
							}
							else
							{
									// Clear Cookie
									delete_cookie("username");
									delete_cookie("password");
							}				
							return 1;
						}else{
							return 'Your company subscription is closed, kindly contact Super admin';
						}
					}else{
						return false;
					}					
				}
				else
				{
						return false;
				}
			}
			else
			{
				return false;
			}
		  }
			
		}
	   
	    public function submit_profile($emp_id,$comp,$firstname,$lastname,$contact,$alt_contact,$email_id,$oldemail_id,$user_role){
	  			$login_where = array(
          			'username'=>$oldemail_id,
          			'companyid'=>$comp,
          		);
          		$user_where = array(
          			'email_id'=>$oldemail_id,
          			'company_id	'=>$comp);

          		$admin_userwhere = array(
          			'company_id'=>$comp,
          			'ad_mailid'=>$oldemail_id,
          		);
          		$admin_usertable = array(
          			'company_id'=>$comp,
          			'ad_fname'=>$firstname,
          			'ad_lname'=>$lastname,
          			'ad_mailid'=>$email_id,
          			'ad_contact'=>$contact,
          		);          		
          		$usertable_data = array(
          			'employee_id'=>$emp_id,
          			'first_name'=>$firstname,
          			'last_name'=>$lastname,
          			'email_id'=>$email_id,
          			'contact_number'=>$contact,
          			'alternate_number'=>$alt_contact,
          			'role'=>$user_role,
          			'company_id'=>$comp,
          		);
          		$login_table = array(
          			'companyid'=>$comp,
          			'user_type'=>$user_role,
          			'username'=>$email_id,
          		);
          		//print_r($admin_userwhere);
          		//echo $user_role;
      		if($user_role=="Admin"){
      			$this->db->select('*');
				$this->db->from('admin_user');
				$this->db->where($admin_usertable);
				$result = $this->db->get();
				//print_r($result);
				$answer = $result->num_rows();				
      		}

      		$this->db->select('*');
      		$this->db->from('login');
      		$this->db->where($login_table);
			$final = $this->db->get();
			$count = $final->num_rows();	

			$this->db->select('*');
      		$this->db->from('user');
      		$this->db->where($usertable_data);
			$final_ans = $this->db->get();
			$res_count = $final_ans->num_rows();	

      		if($user_role=="Admin"){
      			if($answer=="1" && $count=="1" && $res_count=="1"){
      				echo "No Changes made!";
      			}
      			else{
      				$this->db->where($admin_userwhere);
      				$this->db->update('admin_user',$admin_usertable);

      				$this->db->where($user_where);
      				$this->db->update('user',$usertable_data);

      				$this->db->where($login_where);
      				$this->db->update('login',$login_table);

      				echo "Details Updated!!";
      			}
      		}

  			else{
  				if($count=="1" && $res_count=="1"){
      				echo "No Changes made!";
      			}
      			else{

      				$this->db->where($user_where);
      				$this->db->update('user',$usertable_data);

      				$this->db->where($login_where);
      				$this->db->update('login',$login_table);

      				echo "Details Updated!!";
      			}
      		}
	  }
   }
?>