<?php
date_default_timezone_set('Asia/Kolkata');
class Punch_in extends CI_Model

    {
    function __construct()
        {

        // Call the Model constructor

        parent::__construct();
       
        }
    public function tech_select($tech_id, $company_id, $product_id, $cat_id)
        {
        $where = array(
            'technician_id!=' => $tech_id,
            'company_id' => $company_id,
            'product' => $product_id,
            'category' => $cat_id
        );
        $this->db->select('technician_id,employee_id,first_name');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        }

    public function updte_amc($data)
    {
        $dat=array("current_status"=>12);
        $this->db->where('ticket_id', $data);
        $this->db->update('all_tickets', $dat);
        return true;
    }
public

    function get_company_tkt($ticket_id)
        {
        $this->db->select('company_id');
        $this->db->from('all_tickets');
        $this->db->where('ticket_id', $ticket_id);
        $query = $this->db->get();
        $resul = $query->row_array();
        $company_id = $resul['company_id'];
        return $company_id;
        }

        public function get_ticket_details($ticket_id)
        {
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where('ticket_id', $ticket_id);
        //$this->db->join('billing', 'billing.ticket_id = all_tickets.ticket_id');
        $query = $this->db->get();
        $resul = $query->row_array();
        return $resul;
        }


    public

    function get_emp_id($technician_id)
        {
        $this->db->select('employee_id');
        $this->db->from('technician');
        $this->db->where('technician_id', $technician_id);
        $query = $this->db->get();
        $resul = $query->row_array();
        $employee_id = $resul['employee_id'];
        return $employee_id;
        }

        public function profile_image_link($company_id, $technician_id)
        {
        $this->db->select('image');
        $this->db->from('technician');
        $this->db->where('technician_id', $technician_id);
        $this->db->where('company_id', $company_id);
        $query = $this->db->get();
        $resul = $query->row_array();
        $image = $resul['image'];
        return $image;
        }

    public

    function update_notify($notify, $company_id, $role, $tech_id)
        {
        $data = array(
            'msg' => $notify,
            'role' => $role,
            'company_id' => $company_id,
            'tech_id' => $tech_id
        );
        $this->db->insert('notification', $data);
        return true;
        }

        function ticket_history($ticket_id, $tech_id,$company_id,$cust_id,$mode_of_payment, $total_amount,$status)
        {
        $data = array(
            'ticket_id' => $ticket_id,
            'technician_id' => $tech_id,
            'company_id' => $company_id,
            'cust_id' => $cust_id,
            'mode_of_payment' => $mode_of_payment,
            'total_amount' => $total_amount,
            'status' => $status,
        );
        $this->db->insert('ticket_history', $data);
        return true;
        }

public function get_det_cust($ticket_id)
        {
        $where = array(
                'ticket_id' => $ticket_id
            );
            $this->db->select('customer.customer_id,customer.email_id,customer.contact_number');
            $this->db->from('all_tickets');
            $this->db->join('customer', 'all_tickets.cust_id=customer.customer_id');
            $this->db->where($where);
        //  $this->db->group_by('customer_id');
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        }
    public

    function get_prod_cat($product, $category)
        {
        $where = array(

            // 'product_management.product_id' => $product,

            'cat_name' => $category
        );
        $this->db->select('prod_id,cat_id');
        $this->db->from('category_details');

        //     $this->db->join('category_details', 'category_details.prod_id=product_management.product_id');

        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        }

    public

    function display_reimbursement($tech_id)
        {
        $json = array();
        $date = date('Y-04-01');
        $this->db->select('*');
        $this->db->from('reimbursement');
        $this->db->where('technician_id', $tech_id);
        $this->db->where('start_time>=', $date);
        $this->db->where('action', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $row)
            {
          /*  $lat = $row['source_lat'];
            $long = $row['source_long'];
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($long) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($response);
            $status = $output['status'];

            // Get address from json data

            $source_Address = ($status == "OK") ? $output->results[1]->formatted_address : '';
            $lat1 = $row['dest_lat'];
            $long1 = $row['dest_long'];
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat1) . ',' . trim($long1) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($response);
            $status = $output['status'];

            // Get address from json data

            $destin_Address = ($status == "OK") ? $output->results[1]->formatted_address : '';*/
            array_push($json, array(
                "ticket_id" => $row['ticket_id'],
                "technician_id" => $row['technician_id'],
                "mode_of_travel" => $row['mode_of_travel'],
                "source_Address" => $row['source'],
                "destin_Address" => $row['destination'],
                "no_of_km_travelled" => $row['no_of_km_travelled'],
                "travelling_charges" => $row['travelling_charges'],
                "start_date" => $row['start_time'],
                "end_date" => $row['end_time'],
                "request_id" => $row['id'],
                "comments" => $row['comment'],
                "status" => $row['action']
            ));
            }

        return $json;
        }

    public

    function check_tech($tech_id)
        {
        $date = date("Y-m-d");
        $time = date("h:i:sa");
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        return $rowcount = $query->num_rows();
        }

    public

    function get_category($tech_id)
        {
        $json = array();
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('product,technician.company_id,product_name');
        $this->db->from('technician');
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $product_id = $result[0]['product'];
            $company_id = $result[0]['company_id'];
            $product_name = $result[0]['product_name'];
            /* $where1     = array(
            'prod_id' => $product_id,'company_id' => $company_id
            );*/
            $where1 = array(
                'company_id' => $company_id
            );
            $this->db->select('cat_name,cat_id,cat_image,prod_id');
            $this->db->from('category_details');
            $this->db->where($where1);
            $query1 = $this->db->get();
            $res = $query1->result_array();
            foreach($res as $row)
                {
                array_push($json, array(
                    "Category Id" => $row['cat_id'],
                    "Category Name" => $row['cat_name'],
                    "Thumbnail" => $row['cat_image'],
                    "Product Id" => $row['prod_id']
                ));
                }

            return $json;
            }
        }

    public

    function get_spare($tech_id)
        {
        $where = array(
            'technician_id' => $tech_id
        );
$count=0;
        $this->db->select('technician.product,technician.category,technician.company_id,product_management.product_name');
        $this->db->from('technician');
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->where($where);
$this->db->group_by("technician.product");
        $query = $this->db->get();
        $result = $query->result_array();
        $json = array();
        if (!empty($result))
            {
            foreach($result as $r)
            {
            $product_id = $r['product'];
            $product_name = $r['product_name'];
            $cat_id = $r['category'];

            $company_id = $r['company_id'];
                            $where2 = array(
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('spare');
                $this->db->where($where2);
                $this->db->order_by('expiry_date','asc');
                $query2 = $this->db->get();
                $res1 = $query2->result_array();
                foreach($res1 as $row)
                    {
                    $remain = $row['quantity_purchased'] - $row['quantity_used'];
                    
                    array_push($json, array(
                        "Spare Id" => $row['spare_code'],
                        "Spare code" => $row['spare_code'],
                        "Spare Name" => $row['spare_name'],
                        "Thumbnail" => $row['image'],
                        "Product Id" => $row['product'],
                        "Category Id" => $row['category'],
                        "Location" => $row['spare_source'],
                        "Quantity" => $remain,
                        "Model" => $row['spare_modal'],
                        "Cost" => $row['price']
                    ));
                    }

                }

//$json=array_map("unserialize", array_unique(array_map("serialize", $json)));
    return $json;
            }
        }

    public

    function get_tech_product($tech_id)
        {
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*,technician.company_id as c_id');
        $this->db->from('technician');
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $product_name = $result[0]['product_name'];
            $product_id = $result[0]['product_id'];
            $company_id = $result[0]['c_id'];
            $region = $result[0]['region'];
            $area = $result[0]['area'];
            $json = array();
            array_push($json, array(
                "product_name" => $product_name,
                "product_id" => $product_id,
                "company_id" => $company_id,
                "region" => $region,
                "area" => $area
            ));
           
            return $json;
            }
            else{
                echo "No records";
            }
        }

    public

    function get_tkts($company_id, $product_id, $cat_id, $latitude, $longitude)
        {
        $today = date("Y-m-d");
        $data = array();
        $where = array(
            'company_id' => $company_id,
            'product_id' => $product_id,
            'cat_id' => $cat_id
        );
        $this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,product_id,cat_id,location,( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) *   cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) *     sin( radians( latitude ) ) ) ) AS distance');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->like('cust_preference_date', $today);
        $this->db->having('distance < 2');
        $this->db->order_by('cust_preference_date');
        $this->db->limit(1);
        $this->db->where($where);
        $this->db->group_by('customer.customer_id');
        $this->db->group_start();
        $this->db->where("(current_status=0 OR current_status=3)");
        $this->db->group_end();
        $query1 = $this->db->get();
        $query = $query1->result_array();
        if (!empty($query))
            {
            foreach($query as $row)
                {
                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('product', 'all');
                $this->db->group_end();
                $query = $this->db->get();
                $result = $query->result_array();
                if (!empty($result))
                    {
                    $product = 'all';
                    }
                  else
                    {
                    $product = $row['product_id'];
                    }

                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('category', 'all');
                $this->db->group_end();
                $query1 = $this->db->get();
                $result1 = $query1->result_array();
                if (!empty($result1))
                    {
                    $category = 'all';
                    }
                  else
                    {
                    $category = $row['cat_id'];
                    }

                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('cust_category', 'all');
                $this->db->group_end();
                $query2 = $this->db->get();
                $result2 = $query2->result_array();
                if (!empty($result2))
                    {
                    $cust_category = 'all';
                    }
                  else
                    {
                    $cust_category = $row['cust_category'];
                    }

                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('call_category', 'all');
                $this->db->group_end();
                $query3 = $this->db->get();
                $result3 = $query3->result_array();
                if (!empty($result3))
                    {
                    $call_category = 'all';
                    }
                  else
                    {
                    $call_category = $row['call_tag'];
                    }

                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('service_category', 'all');
                $this->db->group_end();
                $query4 = $this->db->get();
                $result4 = $query4->result_array();
                if (!empty($result4))
                    {
                    $service_category = 'all';
                    }
                  else
                    {
                    $service_category = $row['call_type'];
                    }

                $where8 = array(
                    'company_id' => $company_id,
                    'product' => $product,
                    'category' => $category,
                    'cust_category' => $cust_category,
                    'service_category' => $service_category,
                    'call_category' => $call_category
                );
                $this->db->select('ref_id');
                $this->db->from('sla_combination');
                $this->db->where($where8);
                $query5 = $this->db->get();
                $result5 = $query5->result_array();
                if (!empty($result5))
                    {
                    $ref_id = $result5[0]['ref_id'];
                    $this->db->select('resolution_time,response_time,priority_level');
                    $this->db->from('sla_mapping');
                    $this->db->where('ref_id', $ref_id);
                    $query6 = $this->db->get();
                    $result6 = $query6->result_array();
                    if (!empty($result6))
                        {
                        $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                        $response = date('H:i:s', strtotime($result6[0]['response_time']));
                        $priority = date('H:i:s', strtotime($result6[0]['priority_level']));
                        $address = $row['add'];
                        $datetime1 = $row['last'];
                        $date1 = date('Y-m-d', strtotime($datetime1));
                        $time1 = date('H:i:s', strtotime($datetime1));
                        $dt = array(
                            'priority' => $priority
                        );
                        $this->db->where('ticket_id', $row['ticket_id']);
                        $this->db->update('all_tickets', $dt);
                        array_push($data, array(
                            'ticket_id' => $row['ticket_id'],
                            'cust_id' => $row['cust_id'],
                            'customer_name' => $row['customer_name'],
                            'contact_number' => $row['contact_number'],
                            'alternate_number' => $row['alternate_number'],
                            'door_no' => $row['door_no'],
                            'address' => $row['address'],
                            'city' => $row['town'],
                            'location' => $row['town'],
                            'country' => $row['country'],
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                            'priority' => $priority,
                            'call_category' => $row['call_type'],
                            'call_tag' => $row['call_tag'],
                            'prob_description' => $row['prob_desc'],
                            'image' => $row['image'],
                            'audio' => $row['audio'],
                            'tech_id' => $row['tech_id'],
                            'status' => $row['current_status'],
                            'created_date' => $date1,
                            'created_time' => $time1,
                            'response_time' => $response . ' Hours',
                            'resolution_time' => $resolution . ' Hours'
                        ));
                        }
                    }
                }
            }
          else
            {
            $today = date("Y-m-d");
            $data = array();
            $where = array(
                'company_id' => $company_id,
                'product_id' => $product_id,
                'cat_id' => $cat_id
            );
            $this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,product_id,cat_id,location,( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) *   cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) *     sin( radians( latitude ) ) ) ) AS distance');
            $this->db->from('all_tickets');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
            $this->db->like('cust_preference_date', $today);
            $this->db->having('distance < 4');
            $this->db->order_by('cust_preference_date');
            $this->db->limit(1);
            $this->db->where($where);
            $this->db->group_by('customer.customer_id');
            $this->db->group_start();
            $this->db->where("(current_status=0 OR current_status=3)");
            $this->db->group_end();
            $query2 = $this->db->get();
            $query3 = $query2->result_array();
            if (!empty($query3))
                {
                foreach($query3 as $row1)
                    {
                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('product', 'all');
                    $this->db->group_end();
                    $query = $this->db->get();
                    $result = $query->result_array();
                    if (!empty($result))
                        {
                        $product = 'all';
                        }
                      else
                        {
                        $product = $row1['product_id'];
                        }

                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('category', 'all');
                    $this->db->group_end();
                    $query1 = $this->db->get();
                    $result1 = $query1->result_array();
                    if (!empty($result1))
                        {
                        $category = 'all';
                        }
                      else
                        {
                        $category = $row1['cat_id'];
                        }

                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('cust_category', 'all');
                    $this->db->group_end();
                    $query2 = $this->db->get();
                    $result2 = $query2->result_array();
                    if (!empty($result2))
                        {
                        $cust_category = 'all';
                        }
                      else
                        {
                        $cust_category = $row1['cust_category'];
                        }

                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('call_category', 'all');
                    $this->db->group_end();
                    $query3 = $this->db->get();
                    $result3 = $query3->result_array();
                    if (!empty($result3))
                        {
                        $call_category = 'all';
                        }
                      else
                        {
                        $call_category = $row1['call_tag'];
                        }

                    $this->db->select('*');
                    $this->db->from('sla_combination');
                    $this->db->where('company_id', $company_id);
                    $this->db->group_start();
                    $this->db->like('service_category', 'all');
                    $this->db->group_end();
                    $query4 = $this->db->get();
                    $result4 = $query4->result_array();
                    if (!empty($result4))
                        {
                        $service_category = 'all';
                        }
                      else
                        {
                        $service_category = $row1['call_type'];
                        }

                    $where8 = array(
                        'company_id' => $company_id,
                        'product' => $product,
                        'category' => $category,
                        'cust_category' => $cust_category,
                        'service_category' => $service_category,
                        'call_category' => $call_category
                    );
                    $this->db->select('ref_id');
                    $this->db->from('sla_combination');
                    $this->db->where($where8);
                    $query5 = $this->db->get();
                    $result5 = $query5->result_array();
                    if (!empty($result5))
                        {
                        $ref_id = $result5[0]['ref_id'];
                        $this->db->select('resolution_time,response_time,priority_level');
                        $this->db->from('sla_mapping');
                        $this->db->where('ref_id', $ref_id);
                        $query6 = $this->db->get();
                        $result6 = $query6->result_array();
                        if (!empty($result6))
                            {
                            $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                            $response = date('H:i:s', strtotime($result6[0]['response_time']));
                            $priority = date('H:i:s', strtotime($result6[0]['priority_level']));
                            $address = $row1['add'];
                            $datetime1 = $row1['last'];
                            $date1 = date('Y-m-d', strtotime($datetime1));
                            $time1 = date('H:i:s', strtotime($datetime1));
                            $dt = array(
                                'priority' => $priority
                            );
                            $this->db->where('ticket_id', $row['ticket_id']);
                            $this->db->update('all_tickets', $dt);
                            array_push($data, array(
                                'ticket_id' => $row1['ticket_id'],
                                'cust_id' => $row1['cust_id'],
                                'customer_name' => $row1['customer_name'],
                                'contact_number' => $row1['contact_number'],
                                'alternate_number' => $row1['alternate_number'],
                                'door_no' => $row1['door_no'],
                                'address' => $row1['address'],
                                'city' => $row1['town'],
                                'location' => $row1['town'],
                                'country' => $row1['country'],
                                'latitude' => $latitude,
                                'longitude' => $longitude,
                                'priority' => $priority,
                                'call_category' => $row1['call_type'],
                                'call_tag' => $row1['call_tag'],
                                'prob_description' => $row1['prob_desc'],
                                'image' => $row1['image'],
                                'audio' => $row1['audio'],
                                'tech_id' => $row1['tech_id'],
                                'status' => $row1['current_status'],
                                'created_date' => $date1,
                                'created_time' => $time1,
                                'response_time' => $response . ' Hours',
                                'resolution_time' => $resolution . ' Hours'
                            ));
                            }
                        }
                    }
                }
              else
                {
                $today = date("Y-m-d");
                $data = array();
                $where = array(
                    'company_id' => $company_id,
                    'product_id' => $product_id,
                    'cat_id' => $cat_id
                );
                $this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,product_id,cat_id,location,( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) *   cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) *     sin( radians( latitude ) ) ) ) AS distance');
                $this->db->from('all_tickets');
                $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                $this->db->like('cust_preference_date', $today);
                $this->db->having('distance < 8');
                $this->db->order_by('cust_preference_date');
                $this->db->limit(1);
                $this->db->where($where);
                $this->db->group_by('customer.customer_id');
                $this->db->group_start();
                $this->db->where("(current_status=0 OR current_status=3)");
                $this->db->group_end();
                $query4 = $this->db->get();
                $query5 = $query4->result_array();
                if (!empty($query5))
                    {
                    foreach($query5 as $row2)
                        {
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result))
                            {
                            $product = 'all';
                            }
                          else
                            {
                            $product = $row2['product_id'];
                            }

                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1 = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1))
                            {
                            $category = 'all';
                            }
                          else
                            {
                            $category = $row2['cat_id'];
                            }

                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('cust_category', 'all');
                        $this->db->group_end();
                        $query2 = $this->db->get();
                        $result2 = $query2->result_array();
                        if (!empty($result2))
                            {
                            $cust_category = 'all';
                            }
                          else
                            {
                            $cust_category = $row2['cust_category'];
                            }

                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('call_category', 'all');
                        $this->db->group_end();
                        $query3 = $this->db->get();
                        $result3 = $query3->result_array();
                        if (!empty($result3))
                            {
                            $call_category = 'all';
                            }
                          else
                            {
                            $call_category = $row2['call_tag'];
                            }

                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('service_category', 'all');
                        $this->db->group_end();
                        $query4 = $this->db->get();
                        $result4 = $query4->result_array();
                        if (!empty($result4))
                            {
                            $service_category = 'all';
                            }
                          else
                            {
                            $service_category = $row2['call_type'];
                            }

                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category,
                            'cust_category' => $cust_category,
                            'service_category' => $service_category,
                            'call_category' => $call_category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5 = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5))
                            {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time,priority_level');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6 = $this->db->get();
                            $result6 = $query6->result_array();
                            if (!empty($result6))
                                {
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                $response = date('H:i:s', strtotime($result6[0]['response_time']));
                                $priority = date('H:i:s', strtotime($result6[0]['priority_level']));
                                $address = $row2['add'];
                                $datetime1 = $row2['last'];
                                $date1 = date('Y-m-d', strtotime($datetime1));
                                $time1 = date('H:i:s', strtotime($datetime1));
                                $dt = array(
                                    'priority' => $priority
                                );
                                $this->db->where('ticket_id', $row['ticket_id']);
                                $this->db->update('all_tickets', $dt);
                                array_push($data, array(
                                    'ticket_id' => $row2['ticket_id'],
                                    'cust_id' => $row2['cust_id'],
                                    'customer_name' => $row2['customer_name'],
                                    'contact_number' => $row2['contact_number'],
                                    'alternate_number' => $row2['alternate_number'],
                                    'door_no' => $row2['door_no'],
                                    'address' => $row2['address'],
                                    'city' => $row2['town'],
                                    'location' => $row2['town'],
                                    'country' => $row2['country'],
                                    'latitude' => $latitude,
                                    'longitude' => $longitude,
                                    'priority' => $priority,
                                    'call_category' => $row2['call_type'],
                                    'call_tag' => $row2['call_tag'],
                                    'prob_description' => $row2['prob_desc'],
                                    'image' => $row2['image'],
                                    'audio' => $row2['audio'],
                                    'tech_id' => $row2['tech_id'],
                                    'status' => $row2['current_status'],
                                    'created_date' => $date1,
                                    'created_time' => $time1,
                                    'response_time' => $response . ' Hours',
                                    'resolution_time' => $resolution . ' Hours'
                                ));
                                }
                            }
                        }
                    }
                  else
                    {
                    $today = date("Y-m-d");
                    $data = array();
                    $where = array(
                        'company_id' => $company_id,
                        'product_id' => $product_id,
                        'cat_id' => $cat_id
                    );
                    $this->db->select('*,all_tickets.last_update as last,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,DATE_FORMAT(cust_preference_date,"%H:%i:%s") AS time,ticket_id,product_id,cat_id,location,( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) *   cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) *     sin( radians( latitude ) ) ) ) AS distance');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->like('cust_preference_date', $today);
                    $this->db->having('distance > 8');
                    $this->db->order_by('cust_preference_date');
                    $this->db->limit(1);
                    $this->db->where($where);
                    $this->db->group_by('customer.customer_id');
                    $this->db->group_start();
                    $this->db->where("(current_status=0 OR current_status=3)");
                    $this->db->group_end();
                    $query6 = $this->db->get();
                    $query7 = $query6->result_array();
                    if (!empty($query7))
                        {
                        foreach($query7 as $row3)
                            {
                            $this->db->select('*');
                            $this->db->from('sla_combination');
                            $this->db->where('company_id', $company_id);
                            $this->db->group_start();
                            $this->db->like('product', 'all');
                            $this->db->group_end();
                            $query = $this->db->get();
                            $result = $query->result_array();
                            if (!empty($result))
                                {
                                $product = 'all';
                                }
                              else
                                {
                                $product = $row3['product_id'];
                                }

                            $this->db->select('*');
                            $this->db->from('sla_combination');
                            $this->db->where('company_id', $company_id);
                            $this->db->group_start();
                            $this->db->like('category', 'all');
                            $this->db->group_end();
                            $query1 = $this->db->get();
                            $result1 = $query1->result_array();
                            if (!empty($result1))
                                {
                                $category = 'all';
                                }
                              else
                                {
                                $category = $row3['cat_id'];
                                }

                            $this->db->select('*');
                            $this->db->from('sla_combination');
                            $this->db->where('company_id', $company_id);
                            $this->db->group_start();
                            $this->db->like('cust_category', 'all');
                            $this->db->group_end();
                            $query2 = $this->db->get();
                            $result2 = $query2->result_array();
                            if (!empty($result2))
                                {
                                $cust_category = 'all';
                                }
                              else
                                {
                                $cust_category = $row3['cust_category'];
                                }

                            $this->db->select('*');
                            $this->db->from('sla_combination');
                            $this->db->where('company_id', $company_id);
                            $this->db->group_start();
                            $this->db->like('call_category', 'all');
                            $this->db->group_end();
                            $query3 = $this->db->get();
                            $result3 = $query3->result_array();
                            if (!empty($result3))
                                {
                                $call_category = 'all';
                                }
                              else
                                {
                                $call_category = $row3['call_tag'];
                                }

                            $this->db->select('*');
                            $this->db->from('sla_combination');
                            $this->db->where('company_id', $company_id);
                            $this->db->group_start();
                            $this->db->like('service_category', 'all');
                            $this->db->group_end();
                            $query4 = $this->db->get();
                            $result4 = $query4->result_array();
                            if (!empty($result4))
                                {
                                $service_category = 'all';
                                }
                              else
                                {
                                $service_category = $row3['call_type'];
                                }

                            $where8 = array(
                                'company_id' => $company_id,
                                'product' => $product,
                                'category' => $category,
                                'cust_category' => $cust_category,
                                'service_category' => $service_category,
                                'call_category' => $call_category
                            );
                            $this->db->select('ref_id');
                            $this->db->from('sla_combination');
                            $this->db->where($where8);
                            $query5 = $this->db->get();
                            $result5 = $query5->result_array();
                            if (!empty($result5))
                                {
                                $ref_id = $result5[0]['ref_id'];
                                $this->db->select('resolution_time,response_time,priority_level');
                                $this->db->from('sla_mapping');
                                $this->db->where('ref_id', $ref_id);
                                $query6 = $this->db->get();
                                $result6 = $query6->result_array();
                                if (!empty($result6))
                                    {
                                    $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                    $response = date('H:i:s', strtotime($result6[0]['response_time']));
                                    $priority = date('H:i:s', strtotime($result6[0]['priority_level']));
                                    $address = $row3['add'];
                                    $datetime1 = $row3['last'];
                                    $date1 = date('Y-m-d', strtotime($datetime1));
                                    $time1 = date('H:i:s', strtotime($datetime1));
                                    $dt = array(
                                        'priority' => $priority
                                    );
                                    $this->db->where('ticket_id', $row['ticket_id']);
                                    $this->db->update('all_tickets', $dt);
                                    array_push($data, array(
                                        'ticket_id' => $row3['ticket_id'],
                                        'cust_id' => $row3['cust_id'],
                                        'customer_name' => $row3['customer_name'],
                                        'contact_number' => $row3['contact_number'],
                                        'alternate_number' => $row3['alternate_number'],
                                        'door_no' => $row3['door_no'],
                                        'address' => $row3['address'],
                                        'city' => $row3['town'],
                                        'location' => $row3['town'],
                                        'country' => $row3['country'],
                                        'latitude' => $latitude,
                                        'longitude' => $longitude,
                                        'priority' => $priority,
                                        'call_category' => $row3['call_type'],
                                        'call_tag' => $row3['call_tag'],
                                        'prob_description' => $row3['prob_desc'],
                                        'image' => $row3['image'],
                                        'audio' => $row3['audio'],
                                        'tech_id' => $row3['tech_id'],
                                        'status' => $row3['current_status'],
                                        'created_date' => $date1,
                                        'created_time' => $time1,
                                        'response_time' => $response . ' Hours',
                                        'resolution_time' => $resolution . ' Hours'
                                    ));
                                    }
                                }
                            }
                        }
                    }
                }
            }

        return $data;
        }

    public

    function get_tech_cat($tech_id)
        {
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*,technician.company_id as c_id,street');
        $this->db->from('technician');
        $this->db->join('category_details', 'category_details.cat_id=technician.category');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $cat_name = $result[0]['cat_name'];
            $cat_id = $result[0]['cat_id'];
            $company_id = $result[0]['c_id'];
            $json = array();
            array_push($json, array(
                "cat_name" => $cat_name,
                "cat_id" => $cat_id,
                "company_id" => $company_id,
                "address" => $result[0]['current_location']
            ));
            return $json;
            }
        }

    public

    function search($key, $sub)
        {
        $this->db->select('*');
        $this->db->from('knowledge_base');
        $this->db->join('category_details', 'knowledge_base.category=category_details.cat_id');
        $this->db->where('knowledge_base.status', 1);
        $this->db->where('category_details.cat_name', $sub);
        $this->db->group_start();
        $this->db->or_like('knowledge_base.image', $key);
        $this->db->or_like('knowledge_base.video', $key);
        $this->db->or_like('knowledge_base.problem', $key);
        $this->db->or_like('knowledge_base.solution', $key);
        $this->db->group_end();
        $query1 = $this->db->get();
        $query = $query1->result_array();
        return $query;
        }

        

    public

    function check_punch_in($tech_id)
        {
        $date = date("Y-m-d h:i:s");
        $time = date("Y-m-d 00:00:00");
        $where = array(
            'technician_id' => $tech_id,
            'date >=' => $date,
            'date <=' => $time
        );
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->where($where);
        $query = $this->db->get();
        return $rowcount = $query->num_rows();
        }

    public

    function punch_in($tech_id, $company_id)
        {
        $date = date("Y-m-d");
        $time = date("h:i:sa");
        $data = array(
            'technician_id' => $tech_id,
            'company_id' => $company_id,
            'date' => $date,
            'in_time' => $time
        );
        $this->db->insert('attendance', $data);
        return true;
        }

    public

    function available($tech_id, $current_location)
        {
        $where = array(
            'technician_id' => $tech_id
        );
        $data = array(
            'availability' => 1,
            'current_location' => $current_location
        );
        $this->db->where($where);
        $this->db->update('technician', $data);
        return true;
        }

    public

    function nt_available($tech_id,$current_location)
        {
        $where = array(
            'technician_id' => $tech_id
        );
        $data = array(
            'availability' => 0,
            'current_location' => $current_location
        );
        $this->db->where($where);
        $this->db->update('technician', $data);
        return true;
        }

    public

    function personal_spare_request($tech_id, $spare_array, $company_id)
        {
        $data = array(
            'tech_id' => $tech_id,
            'spare_array' => $spare_array,
            'company_id' => $company_id
        );
        $this->db->insert('personal_spare', $data);
        return true;
        }

    public

    function personal_spare_transfer($from_tech_id,$to_tech_id, $spare_array, $company_id)
        {
        $data = array(
            'from_tech_id' => $from_tech_id,
            'to_tech_id' => $to_tech_id,
            'spare_array' => $spare_array,
            'company_id' => $company_id
        );
        $this->db->insert('spare_transfer', $data);
        return true;
        }

    public

    function check_punch_out($tech_id)
        {
        $date = date("Y-m-d");
        $time = date("h:i:sa");
        $where = array(
            'technician_id' => $tech_id,
            'date' => $date,
            'out_time!=' => '00:00:00'
        );
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->where('id', $this->db->insert_id());
        $this->db->where($where);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if ($rowcount == 0)
            {
            $where1 = array(
                'technician_id' => $tech_id,
                'date' => $date,
                'out_time' => '00:00:00'
            );
            $this->db->select('*');
            $this->db->from('attendance');
            $this->db->where($where1);
            $this->db->order_by('id', 'desc');
            $this->db->limit(1);
            $query = $this->db->get();
            $result = $query->row_array();
            return $result['id'];
            }
          else
            {
            return 0;
            }
        }

    public

    function punch_out($tech_id, $result)
        {
        $date = date("Y-m-d");
        $time = date("h:i:sa");
        $where = array(
            'technician_id' => $tech_id,
            'date' => $date
        );
        $data = array(
            'out_time' => $time
        );
        $this->db->where('id', $result);
        $this->db->where($where);
        $this->db->update('attendance', $data);
        return true;
        }

    public

    function get_new_tkts($tech_id)
        {
        $data = array();
        $data1 = array();
        $data2 = array();
        $date = date("Y-m-d");
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        foreach($query as $row)
            {
            
            if($row['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row['assigned']);
            $last = new DateTime($row['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row['response_time'])
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data, array(
                    'ticket_id' => $row['ticket_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['contact_number'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prior'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url() .$row['image'],
                    'audio' => base_url() .$row['audio'],
                    'video' => base_url() .$row['video'],
                    'tech_id' => $row['tech_id'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' => $row['schedule_next'],
                    'status' => $row['current_status'],
                    'prev_status' => $prev,
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row['ticket_id']
                ));
                }
            }
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id!=' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query11 = $this->db->get();
        $query2 = $query11->result_array();
        foreach($query2 as $row2)
            {
            
            if($row2['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row2['assigned']);
            $last = new DateTime($row2['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row2['response_time'])
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data, array(
                    'ticket_id' => $row2['ticket_id'],
                'current_tech_id' => "",
                'current_tech_name' => "",
                'tech_contact_no' => "",
                'tech_email_id' => "",
                'amc_id' => $row2['amc_id'],
                'cust_id' => $row2['cust_id'],
                'customer_name' => $row2['customer_name'],
                'contact_number' => $row2['contact_no'],
                'alternate_number' => $row2['alternate_no'],
                'door_no' => $row2['door_no'],
                'address' => $row2['add'],
                'city' => $row2['town_tkt'],
                /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                'location' => $row['cust_town'],
                'country' => $row2['country'],
                'latitude' => $latitude,
                'longitude' => $longitude,
                'type_cont' => $row2['call_type'],
                'call_tag' => $row2['call_tag'],
                'contract_period' => $row2['contract_period'],
                'product_name' => $row2['product_name'],
                'cat_name' => $row2['cat_name'],
                'quantity' => $row2['quantity'],
                'lead_time' => $row2['lead_time'],
                'schedule_next' => $row2['schedule_next'],
                'priority' => $row2['prior'],
                'cust_preference_date' => $row2['cust_preference_date'],
                'prev_status' => $prev,
                'created_date' => $date1,
                'created_time' => $time1
                ));
                }
              else
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row2['ticket_id']
                ));
                }
            }

    $where41 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.amc_id' => ''
        );
        $cur_status=["1","0","12"];
        $prev_status=["15"];
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where41);
        $this->db->group_start();
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->where_not_in('previous_status',$prev_status);
        $this->db->group_end();
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query1233 = $this->db->get();
        $query21 = $query1233->result_array();
        foreach($query21 as $row1)
            {
            $assigned = new DateTime($row1['assigned']);
            $last = new DateTime($row1['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row1['response_time'])
                {
                
            if($row1['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
                $address = $row1['add'];
                $datetime1 = $row1['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row1['latitud'];
                $longitude = $row1['longitud'];
                array_push($data, array(
                    'ticket_id' => $row1['ticket_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row1['cust_id'],
                    'customer_name' => $row1['customer_name'],
                    'contact_number' => $row1['contact_number'],
                    'alternate_number' => $row1['alternate_number'],
                    'door_no' => $row1['door_no'],
                    'address' => $row1['add'],
                    'city' => $row1['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row1['cust_town'],
                    'country' => $row1['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row1['prior'],
                    'call_category' => $row1['call_type'],
                    'call_tag' => $row1['call'],
                    'prob_description' => $row1['prob_desc'],
                    'image' => base_url().$row1['image'],
                    'audio' => base_url().$row1['audio'],
                    'video' => base_url().$row1['video'],
                    'tech_id' => $row1['tech_id'],
                    'lead_time' => $row1['lead_time'],
                    'schedule_next' => $row1['schedule_next'],
                    'status' => $row1['current_status'],
                    'prev_status' => $prev,
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row1['response_time'] . ' Hours',
                    'resolution_time' => $row1['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row1['add'];
                $datetime1 = $row1['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row1['latitud'];
                $longitude = $row1['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row1['ticket_id']
                ));
                }
            }
    $where41 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.amc_id!=' => ''
        );
        $cur_status=["1","0","12"];
        $prev_status=["15"];
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where41);
        $this->db->group_start();
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->where_not_in('previous_status',$prev_status);
        $this->db->group_end();
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query12 = $this->db->get();
        $query21 = $query12->result_array();
        foreach($query21 as $row21)
            {
            $assigned = new DateTime($row21['assigned']);
            $last = new DateTime($row21['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row21['response_time'])
                {
                
            if($row21['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                array_push($data, array(
                    'ticket_id' => $row21['ticket_id'],
                'current_tech_id' => "",
                'current_tech_name' => "",
                'tech_contact_no' => "",
                'tech_email_id' => "",
                'amc_id' => $row21['amc_id'],
                'cust_id' => $row21['cust_id'],
                'customer_name' => $row21['customer_name'],
                'contact_number' => $row21['contact_no'],
                'alternate_number' => $row21['alternate_no'],
                'door_no' => $row21['door_no'],
                'address' => $row21['add'],
                'city' => $row21['town_tkt'],
                /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                'location' => $row21['cust_town'],
                'country' => $row21['country'],
                'latitude' => $latitude,
                'longitude' => $longitude,
                'type_cont' => $row21['call_type'],
                'call_tag' => $row21['call_tag'],
                'contract_period' => $row21['contract_period'],
                'product_name' => $row21['product_name'],
                'cat_name' => $row21['cat_name'],
                'quantity' => $row21['quantity'],
                'lead_time' => $row21['lead_time'],
                'schedule_next' => $row21['schedule_next'],
                'priority' => $row21['prior'],
                'cust_preference_date' => $row21['cust_preference_date'],
                'prev_status' => $prev,
                'created_date' => $date1,
                'created_time' => $time1
                ));
                }
              else
                {
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row21['ticket_id']
                ));
                }
            }
            $where42 = array(
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id' => ''
        );
        $cur_status=["1","0","12"];
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where42);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query122 = $this->db->get();
        $query212 = $query122->result_array();
        foreach($query212 as $row12)
            {
            if($row12['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row12['assigned']);
            $last = new DateTime($row12['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row12['response_time'])
                {
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
                array_push($data, array(
                    'ticket_id' => $row12['ticket_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row12['cust_id'],
                    'customer_name' => $row12['customer_name'],
                    'contact_number' => $row12['contact_number'],
                    'alternate_number' => $row12['alternate_number'],
                    'door_no' => $row12['door_no'],
                    'address' => $row12['add'],
                    'city' => $row12['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                   'location' => $row12['cust_town'],
                    'country' => $row12['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row12['prior'],
                    'call_category' => $row12['call_type'],
                    'call_tag' => $row12['call'],
                    'prob_description' => $row12['prob_desc'],
                    'image' => base_url().$row12['image'],
                    'audio' => base_url().$row12['audio'],
                    'video' => base_url().$row12['video'],
                    'tech_id' => $row12['tech_id'],
                    'lead_time' => $row12['lead_time'],
                    'schedule_next' => $row12['schedule_next'],
                    'status' => $row12['current_status'],
                    'prev_status' => $prev,
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row12['response_time'] . ' Hours',
                    'resolution_time' => $row12['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row12['ticket_id']
                ));
                }
            }
    $where413 = array(
            
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id!=' => ''
        );
        $cur_status=["1","0","12"];
        $this->db->select('*,technician.first_name as tech_name,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add,all_tickets.town as town_tkt');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where413);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $query123 = $this->db->get();
        $query213 = $query123->result_array();
        foreach($query213 as $row213)
            {
            if($row213['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row213['assigned']);
            $last = new DateTime($row213['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row213['response_time'])
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*   $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                array_push($data, array(
                    'ticket_id' => $row213['ticket_id'],
                'current_tech_id' => "",
                'current_tech_name' => "",
                'tech_contact_no' => "",
                'tech_email_id' => "",
                'amc_id' => $row213['amc_id'],
                'cust_id' => $row213['cust_id'],
                'customer_name' => $row213['customer_name'],
                'contact_number' => $row213['contact_no'],
                'alternate_number' => $row213['alternate_no'],
                'door_no' => $row213['door_no'],
                'address' => $row213['add'],
                'city' => $row213['town_tkt'],
                /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                'location' => $row213['cust_town'],
                'country' => $row213['country'],
                'latitude' => $latitude,
                'longitude' => $longitude,
                'type_cont' => $row213['call_type'],
                'call_tag' => $row213['call_tag'],
                'contract_period' => $row213['contract_period'],
                'product_name' => $row213['product_name'],
                'cat_name' => $row213['cat_name'],
                'quantity' => $row213['quantity'],
                'lead_time' => $row213['lead_time'],
                'schedule_next' => $row213['schedule_next'],
                'priority' => $row213['prior'],
                'cust_preference_date' => $row213['cust_preference_date'],
                'prev_status' => $prev,
                'created_date' => $date1,
                'created_time' => $time1
                ));
                }
              else
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                // Get JSON results from this request

                /*  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . trim($address) . '&sensor=false&key=AIzaSyCpGuMQiiTlZFrdJlhu4mvy4Ao_s9z4VkQ';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);
                $response_a = json_decode($response);
                $latitude= $response_a->results[0]->geometry->location->lat;
                $longitude = $response_a->results[0]->geometry->location->lng;*/
                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row213['ticket_id']
                ));
                }
            }
        echo json_encode(array(
            "status" => 1,
            "tickets" => $data,
            "deleted" => $data1,
            "amc_payment" => $data2
        ));
        }

    public

    function home($tech_id)
        {
        $tech_reward_point = array();
        $next_reward_point = array();
        $overall_rank = array();
        $date = date("m", strtotime("-1 month"));
    
        $date1 = date("Y-m-d 00:00:00");
        $date2 = date("Y-m-d 59:59:59");
        $this->db->select('skill_level,company_id');
        $this->db->from('technician');
        $this->db->where('technician_id', $tech_id);
        $query2 = $this->db->get();
        $query3 = $query2->result_array();

        // echo $month = date("m",$date);

        if (!empty($query3))
            {
            $where = array(
                'reward.tech_id' => $tech_id,
                'sla.level' => $query3[0]['skill_level']
            );
            $this->db->select('*');
            $this->db->from('reward,sla');
            $this->db->where($where);
            $this->db->order_by('reward.last_update', 'desc');
            $query1 = $this->db->get();
            $query = $query1->result_array();
            
            $where1 = array(
                'all_tickets.tech_id' => $tech_id,
                'current_status' => 8
            );
            $wh = array(
                'all_tickets.tech_id' => $tech_id,
                'current_status' => 12
            );

            //progressing ticket count for the day
            $status =["7","8","10","11","12","14","15","18"];
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where('all_tickets.tech_id',$tech_id);
            $this->db->where_in('all_tickets.current_status',$status);
            $this->db->where('all_tickets.cust_preference_date >=', $date1);
            $this->db->where('all_tickets.cust_preference_date <=', $date2);
            $query6 = $this->db->get();
            $count = $query6->num_rows();
            
            //overaall tickets for the day
            $status_not_in =["1","3"];
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where('all_tickets.tech_id',$tech_id);
            $this->db->where_not_in('current_status',$status_not_in);
            $this->db->where('all_tickets.cust_preference_date >=', $date1);
            $this->db->where('all_tickets.cust_preference_date <=', $date2);
            $query61 = $this->db->get();
            $tcount = $query61->num_rows();
          
            $monthly_target = array();
            $overall_rank = 0;
            if (!empty($query))
                {
                foreach($query as $row)
                    {
                    if (count($tech_reward_point) == 0)
                        {
                        $tech_reward_point = $row['overall_score'];
                        $next_reward_point = $row['reward_point'];
                        }

                    if ($row['month'] == $date)
                        {
                        $overall_rank = $row['rank'];
                        }

                    array_push($monthly_target, array(
                        $row['month'] => $row['overall_score']
                    ));
                    }
                }
              else
                {
                $this->db->select('company_id,skill_level');
                $this->db->from('technician');
                $this->db->where('technician_id', $tech_id);
                $query2 = $this->db->get();
                $query3 = $query2->result_array();
                if (!empty($query3))
                    {
                    $company_id = $query3[0]['company_id'];
                    $this->db->select('*');
                    $this->db->from('sla');
                    $this->db->where('company_id', $company_id);
                    $this->db->where('level', $query3[0]['skill_level']);
                    $query1 = $this->db->get();
                    $query = $query1->result_array();
                    $tech_reward_point = "0";
                    if (count($query) != 0)
                        {
                        $next_reward_point = $query[0]['reward_point'];
                        }
                      else
                        {
                        $next_reward_point = 0;
                        }

                    $tech_reward_point = "0";
                    $overall_rank = "0";
                    $this->db->select('rank');
                    $month = date('Y-m', strtotime('-1 month'));
                    $this->db->from('reward');
                    $this->db->where('company_id', $company_id);
                    $this->db->like('last_update', $month);
                    $this->db->order_by('rank', 'desc');
                    $this->db->limit(1);
                    $query4 = $this->db->get();
                    $query5 = $query4->result_array();
                    if (!empty($query5))
                        {
                        $overall_rank = strval($query5[0]['rank'] + 1);
                        }
                      else
                        {
                        $overall_rank = '1';
                        }
                    }
                }

            if (!empty($tech_reward_point) || !empty($overall_rank) || !empty($next_reward_point) || !empty($count))
                {
                $monthly_target = array_reverse($monthly_target);
                echo json_encode(array(
                    "status" => 1,
                    "tech_reward_point" => $tech_reward_point,
                    "next_reward_point" => $next_reward_point,
                    "overall_rank" => $overall_rank,
                    "monthly_target" => $monthly_target,
                    "task_count" => $count,
                    "total_task_count" => $tcount
                ));
                }
              else
                {
                $this->db->select('company_id,skill_level');
                $this->db->from('technician');
                $this->db->where('technician_id', $tech_id);
                $query2 = $this->db->get();
                $query3 = $query2->result_array();
                if (!empty($query3))
                    {
                    $company_id = $query3[0]['company_id'];
                    $this->db->select('*');
                    $this->db->from('reward');
                    $this->db->where('company_id', $company_id);
                    $this->db->where('level', $query3[0]['skill_level']);
                    $query1 = $this->db->get();
                    $query = $query1->result_array();
                    $next_reward_point = $query[0]['reward_point'];
                    echo json_encode(array(
                        "status" => 1,
                        "tech_reward_point" => 0,
                        "next_reward_point" => $next_reward_point,
                        "overall_rank" => $overall_rank,
                        "monthly_target" => $monthly_target,
                        "task_count" => $count,
                        "total_task_count" => $tcount
                    ));
                    }
                  else
                    {
                    echo json_encode(array(
                        "status" => 0,
                        "msg" => "Error Tech id"
                    ));
                    }
                }
            }
          else
            {
            echo json_encode(array(
                "status" => 0,
                "msg" => "Error Tech id"
            ));
            }
        }

    public

    function accept_ticket($tech_id, $ticket_id, $reason)
        {
        $this->db->where('ticket_id', $ticket_id);
        $result = array();
        if ($reason == 'Accept')
            {
            $dbdata = array(
                "tech_id" => $tech_id,
                "current_status" => 2
            );
            $result = array(
                "status" => 1,
                "msg" => 'Accepted!!!'
            );
            }
          else
            {
            $dbdata = array(
                "tech_id" => $tech_id,
                "current_status" => 3,
                "reason" => $reason
            );
            $result = array(
                "status" => 1,
                "msg" => 'Rejected!!!'
            );
            }

        $this->db->update('all_tickets', $dbdata);
        echo json_encode($result);
        }

    public

    function upload($data)
        {
        $this->db->insert('knowledge_base', $data);
        return true;
        }

    public

    function get_company($technician_id)
        {
        $this->db->select('company_id');
        $this->db->from('technician');
        $this->db->where('technician_id', $technician_id);
        $query = $this->db->get();
        $resul = $query->row_array();
        $company_id = $resul['company_id'];
        return $company_id;
        }
       public function get_travel_charge($company_id)
        {
        $this->db->select('*');
        $this->db->from('service_disc');
        $this->db->where('company_id', $company_id);
        $query = $this->db->get();
        $resul = $query->row_array();
        return $resul;
        }

    public function start_travel($data)
    
        {   $status =["10","11"];
            $this->db->select('*');
            $this->db->from('reimbursement');
            $this->db->where('reimbursement.ticket_id',$data['ticket_id']);
            $this->db->where('reimbursement.technician_id', $data['technician_id']);
            $this->db->where('reimbursement.company_id', $data['company_id']);
            $this->db->where('reimbursement.mode_of_travel',$data['mode_of_travel']);
            $this->db->join("all_tickets", "reimbursement.ticket_id =all_tickets.ticket_id");
            $this->db->where_not_in('all_tickets.current_status',$status);

            $query = $this->db->get();
            $resul = $query->row_array();
        if(count($resul)>0)
        {
           
            $this->db->where('ticket_id',$data['ticket_id']);
            $this->db->where('technician_id', $technician_id);
            $this->db->where('company_id', $data['company_id']);
            $this->db->update('reimbursement',$data);
            return true;

        }
        else{

            $this->db->insert('reimbursement', $data);
            return true;
        }
    }

    public

    function end_travel($data, $ticket_id, $technician_id)
        {
        $this->db->where('ticket_id', $ticket_id);
        $this->db->where('technician_id', $technician_id);
        $this->db->update('reimbursement', $data);
        return true;
        }

    public

    function change_travel_status($data, $ticket_id)
        {
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('all_tickets', $data);
        return true;
        }

    public

    function bill($technician_id, $ticket_id, $file, $travel_charges, $company_id)
    {
    // $this->db->where('ticket_id', $ticket_id);
    // $this->db->where('technician_id', $technician_id);
    // $data = array(
    //     'view' => $file,
    //     'company_id' => $company_id,
    //     'travelling_charges' => $travel_charges,
    //     'status' => 6
    // );
    // $this->db->update('reimbursement', $data);
    // return true;
    $data = array(
            'view' => $file,
             'company_id' => $company_id,
             'travelling_charges' => $travel_charges,
            'status' => 6
         );
    $where = array(
        'ticket_id' => $ticket_id,
        'technician_id' => $technician_id
    );
   $this->db->set($data);
   $this->db->where($where);
   $this->db->update('reimbursement');
   return true; 
    }

    public

    function ticket_start($tech_id, $ticket_id, $start_time)
        {
        $this->db->where('ticket_id', $ticket_id);
        $this->db->where('tech_id', $tech_id);
        $data = array(
            'ticket_start_time' => $start_time,
            'current_status' => 7
        );
        $this->db->update('all_tickets', $data);
        return true;
        }

    public

    function complete_ticket($tech_id, $ticket_id, $company_id)
        {
        $where = array(
            'ticket_id' => $ticket_id,
            'tech_id' => $tech_id
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->where($where);
        $this->db->group_by('customer.customer_id');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        if (!empty($query))
            {
            $res = 0;
            $spare = $query[0]['requested_spare'];
            $spare_array = json_decode($spare, true);
            if (!empty($spare_array))
                {
                foreach($spare_array as $sp)
                    {
                    $spare_code = $sp['Spare_code'];
                    $quantity = $sp['quantity'];
                    $where2 = array(
                        'spare_code' => $spare_code
                    );
                    $this->db->select('*');
                    $this->db->from('spare');
                    $this->db->where($where2);
                    $query2 = $this->db->get();
                    $query21 = $query2->result_array();
                    if (!empty($query21))
                        {
                        $price = $query21[0]['price'];
                        $res = $res + ($quantity * $price);
                        }
                    }
                }

            $where1 = array(
                'company_id' => $company_id
            );
            $this->db->select('*');
            $this->db->from('service_disc');
            $this->db->where($where1);
            $query11 = $this->db->get();
            $query111 = $query11->row_array();
            $product_gst = $query111['pro_gst'];
            $service_gst = $query111['ser_gst'];
            $email_id = $query[0]['email_id'];
            $contact_number = $query[0]['contact_number'];
            if($service_gst=="" || $service_gst==null || $service_gst=="null")
            {
            $service_gst=0;
            }
            if($product_gst=="" || $product_gst==null || $product_gst=="null")
            {
            $product_gst=0;
            }
            $json = array(
                'status' => 1,
                'spare charge' => $res,
                'service_gst' => $service_gst,
                'product_gst' => $product_gst,
                'customer email_id' => $email_id,
                'contact_number' => $contact_number
            );
            }
          else
            {
            $res = 0;
            $where1 = array(
                'ticket_id' => $ticket_id,
                'tech_id' => $tech_id
            );
            $this->db->select('*,all_tickets.contact_no as c_no');
            $this->db->from('all_tickets');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
            $this->db->where($where1);
            $this->db->group_by('customer.customer_id');
            $query2 = $this->db->get();
            $query3 = $query2->result_array();
            $contact_number = $query3[0]['c_no'];
            $mail = $query3[0]['email_id'];
            $this->db->select('*');
            $this->db->from('service_disc');
            $this->db->where($where1);
            $query11 = $this->db->get();
            $query111 = $query11->row_array();
            $product_gst = $query111['pro_gst'];
            $service_gst = $query111['ser_gst'];
            if($service_gst=="" || $service_gst==null || $service_gst=="null")
            {
            $service_gst=0;
            }
            if($product_gst=="" || $product_gst==null || $product_gst=="null")
            {
            $product_gst=0;
            }
            $json = array(
                'status' => 1,
                'spare charge' => $res,
                'service_gst' => $service_gst,
                'product_gst' => $product_gst,
                'customer email_id' => $mail,
                'contact_number' => $contact_number
            );
            }

        echo json_encode($json);
        }

    public

    function show_skill()
        {
        $this->db->select('*');
        $this->db->from('technician_hierarchy');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        if (!empty($query))
            {
            $json = array(
                'status' => 1,
                'skill level' => $query
            );
            }
          else
            {
            $json = array(
                'status' => 0,
                'msg' => 'Error!!!'
            );
            }

        echo json_encode($json);
        }

    public

    function reimbursement_request($technician_id, $start_date, $end_date, $comment, $company_id)
        {
        $start_date = str_replace('/', '-', $start_date);
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = str_replace('/', '-', $end_date);
        $end_date = date('Y-m-d', strtotime($end_date));
        $data = array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'technician_id' => $technician_id,
            'status' => 17,
            'comment' => $comment,
            'company_id' => $company_id
        );
        $this->db->insert('reimbursement_request', $data);
        return true;
        }

    public

    function submit_escalate($ticket_id, $tech_id, $data)
        {
        $where = array(
            'tech_id' => $tech_id,
            'ticket_id' => $ticket_id
        );
        $this->db->where($where);
        $this->db->update('all_tickets', $data);
        return true;
        }

    // public function submit_complete($ticket_id, $tech_id, $data)
    //     {
    //     $this->db->insert('billing', $data);
    //     return true;
    //     }

    public function submit_complete($ticket_id, $tech_id, $data)
        {

            $this->db->select('*');
            $this->db->from('billing');
            $this->db->where('ticket_id',$ticket_id);
            $this->db->where('tech_id', $tech_id);
            $this->db->where('company_id', $data['company_id']);
            $query = $this->db->get();
            $resul = $query->row_array();
            if(count($resul)>0)
            {
               
                $this->db->where('ticket_id',$ticket_id);
                $this->db->where('tech_id', $tech_id);
                $this->db->where('company_id', $data['company_id']);
                $this->db->update('billing',$data);
                return true;
    
            }
            else{
                  $this->db->insert('billing', $data);
                  return true;
            }

      
        }

    public function submit_complete1($ticket_id, $tech_id, $datas,$cust_mobile,$code)
        {

        $apiKey = urlencode('2EodOA4pMpk-bmz7D8FaLwCWJ2n6XBK4fdXN2hU8hR');
        $numbers = array($cust_mobile);
        $sender = urlencode('TXTLCL');
        //$message = rawurlencode($array['string']);
        $message = rawurlencode("Dear Customer if you satisfy with our service please share this OTP with technician - ".$code);
        $numbers = implode(',', $numbers);
       
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
      
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        //return $response; 

    $where = array(
        'tech_id' => $tech_id,
        'ticket_id' => $ticket_id
    );
    $this->db->where($where);
    $this->db->update('all_tickets', $datas);
    return true;
        }


        public function verify_otp($ticket_id,$tech_id,$otp_mobile)
        {
        $where = array(
            'tech_id' => $tech_id,
            'ticket_id' => $ticket_id,
            'otp_code'=> $otp_mobile
        );
        $data = array(
            'current_status'=> 50
        );
        $this->db->select('otp_code');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query = $this->db->get();
        $resul = $query->row_array();
        $otp_db = $resul['otp_code'];
      
        if(count($resul)==1)
        {  
            $this->db->where($where);
            $this->db->update('all_tickets', $data);
            return true;
        }
        else{
            echo "Error in update";
           
        }
        
        }

        public function frm_complete_ticket($ticket_id, $tech_id, $company_id,$billing_data, $ticket_data){
            $where = array(
                'tech_id' => $tech_id,
                'ticket_id' => $ticket_id,
                'company_id'=>$company_id
            );
      
        $this->db->where($where);
        $this->db->update('all_tickets', $ticket_data);
        if ($this->db->affected_rows() > 0)
        {
         $this->db->where($where);
         $this->db->update('billing', $billing_data);
            
        return 1;
        }
        else
        {
        return 0;
        }

        }

        public function return_pending_frm($ticket_id, $tech_id, $company_id){
            $where = array(
                'tech_id'=>$tech_id,
                'ticket_id'=>$ticket_id,
                'company_id'=> $company_id,
                //'drop_spare' => $drop_spare
            );

            $data = array(
                'frm_status'=> "RETURNED"
            );
            $this->db->where($where);
            $this->db->update('billing',$data);
            if ($this->db->affected_rows() > 0)
            {
                return 1;
            }
            else{
                return 0;
            }

        }

    public function work_in_progress($data, $ticket_id)
        {
        $where = array(
            'ticket_id' => $ticket_id
        );
        $this->db->where($where);
        $this->db->update('all_tickets', $data);
        return true;
        }

    // public

    // function pdf_report($tech_id, $company_id, $from, $to)
    //     {
    //     $json = array();
    //     $address = 0;
    //     $from = date('Y-m-d 00:00:00', strtotime($from));
    //     $to = date('Y-m-d 23:59:59', strtotime($to));
    //     $where = array(
    //         "billing.company_id" => $company_id,
    //         "billing.tech_id" => $tech_id,
    //         "billing.ticket_end_time>=" => $from,
    //         "billing.ticket_end_time<=" => $to
    //         // "all_tickets.current_status" => 12
    //     );
    //     $status =["8","10","11","12","14","15","18"];
       
    //     $this->db->select('*,billing.id as reim_id, (select technician.employee_id from technician WHERE technician.technician_id=billing.tech_id limit 1) as emp_id, (select technician.first_name from technician WHERE technician.technician_id=billing.tech_id limit 1) as empname');
    //     $this->db->from('billing');
    //     $this->db->join("all_tickets", "billing.ticket_id =all_tickets.ticket_id");
    //     $this->db->join("customer", "customer.customer_id = all_tickets.cust_id");
    //     $this->db->where_in('all_tickets.current_status',$status);
    //     $this->db->where($where);
    //     $query = $this->db->get();
    //     $result = $query->result_array();
      
    //     foreach($result as $row)
    //         {
         
    //         array_push($json, array(
    //             "employee_id" => $row['emp_id'],
    //             "reim_id" => $row['reim_id'],
    //             "ticket_id" => $row['ticket_id'],
    //             "technician_id" => $row['tech_id'],
    //             "first_name" => $row['empname'],
    //             "customer_name" => $row['customer_name'],
    //             "mode_of_travel" => $row['mode_of_payment'],
    //             "total_amount" => $row['total'],
    //             "status" => $row['current_status'],
    //             "date"=> date('Y-m-d',strtotime($row['ticket_end_time']))
    //         ));
    //         }
           

    //     return $json;
    //     }


    public

    function pdf_report($tech_id, $company_id, $from, $to)
        {
        $json = array();
        $address = 0;
        $from = date('Y-m-d 00:00:00', strtotime($from));
        $to = date('Y-m-d 23:59:59', strtotime($to));
        $where = array(
            "ticket_history.company_id" => $company_id,
            "ticket_history.technician_id" => $tech_id,
            "ticket_history.date>=" => $from,
            "ticket_history.date<=" => $to
           
        );
       
        $this->db->select('*, (select technician.employee_id from technician WHERE technician.technician_id=ticket_history.technician_id limit 1) as emp_id, (select technician.first_name from technician WHERE technician.technician_id=ticket_history.technician_id limit 1) as empname');
        $this->db->from('ticket_history');
        $this->db->where($where);
        $this->db->join("customer", "customer.customer_id = ticket_history.cust_id");
        $query = $this->db->get();
        $result = $query->result_array();
      
        foreach($result as $row)
            {
         
            array_push($json, array(
                "employee_id" => $row['emp_id'],
                "reim_id" => $row['ticket_history_id'],
                "ticket_id" => $row['ticket_id'],
                "technician_id" => $row['technician_id'],
                "first_name" => $row['empname'],
                "customer_name" => $row['customer_name'],
                "mode_of_payment" => $row['mode_of_payment'],
                "total_amount" => $row['total_amount'],
                "status" => $row['status'],
                "date"=> date('Y-m-d',strtotime($row['date']))
            ));
            }
           

        return $json;
        }

    public

    function reimbursement_report($tech_id, $company_id, $from, $to)
        {
        $json = array();
        $address = 0;
        $where = array(
            "reimbursement.company_id" => $company_id,
            "reimbursement.technician_id" => $tech_id,
            "reimbursement.start_time>=" => $from,
            "reimbursement.end_time<=" => $to
        );
        $this->db->select('*,reimbursement.id as reim_id,reimbursement.last_update as lupdate, (select technician.employee_id from technician WHERE technician.technician_id=reimbursement.technician_id limit 1) as emp_id, (select technician.first_name from technician WHERE technician.technician_id=reimbursement.technician_id limit 1) as empname');
        $this->db->from('reimbursement');
        //$this->db->join("technician", "reimbursement.technician_id=technician.technician_id");
        $this->db->join("all_tickets", "all_tickets.ticket_id=reimbursement.ticket_id");
        $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
    
        foreach($result as $row)
            {
          /*  $lat = $row['source_lat'];
            $long = $row['source_long'];
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($long) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($response);
            $status = $output->status;

            // Get address from json data

            $source_Address = ($status == "OK") ? $output->results[0]->formatted_address : '';
            $lat1 = $row['dest_lat'];
            $long1 = $row['dest_long'];

          

            

            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat1) . ',' . trim($long1) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output1 = json_decode($response);
            $status1 = $output1->status;
           

            // Get address from json data

            $address = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';*/

            // Return address of the given latitude and longitude

            array_push($json, array(
                "employee_id" => $row['emp_id'],
                "reim_id" => $row['reim_id'],
                "ticket_id" => $row['ticket_id'],
                "technician_id" => $row['technician_id'],
                "first_name" => $row['empname'],
                "customer_name" => $row['customer_name'],
                "mode_of_travel" => $row['mode_of_travel'],
                "source_Address" => $row['source'],
                "destin_Address" => $row['destination'],
                "no_of_km_travelled" => $row['no_of_km_travelled'],
                "travelling_charges" => $row['travelling_charges'],
                "date"=> date('Y-m-d',strtotime($row['lupdate']))
            ));
            }

        return $json;
        }

    public

    function product($tech_id)
        {
        $where1 = array(
            'technician_id' => $tech_id
        );
        $this->db->select('company_id');
        $this->db->from('technician');
        $this->db->where($where1);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $company_id = $result[0]['company_id'];
            $where = array(
                'company_id' => $company_id
            );
            $this->db->select('product_name,product_id,product_image');
            $this->db->from('product_management');
            $this->db->where($where);
            $query1 = $this->db->get();
            $query = $query1->result_array();
            return $query;
            }
        }

       

    public

    function get_amc($tech_id)
        {
        $where1 = array(
            'technician_id' => $tech_id
        );
        $this->db->select('company_id');
        $this->db->from('technician');
        $this->db->where($where1);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $company_id = $result[0]['company_id'];
            $where = array(
                'company_id' => $company_id
            );
            $this->db->select('*');
            $this->db->from('amc_type');

            // $this->db->where($where);

            $query1 = $this->db->get();
            $query = $query1->result_array();
            return $query;
            }
        }

    public

    function category($tech_id, $product_id)
        {
        $where1 = array(
            'technician_id' => $tech_id
        );
        $this->db->select('company_id');
        $this->db->from('technician');
        $this->db->where($where1);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            $company_id = $result[0]['company_id'];
            $where = array(
                'prod_id' => $product_id,
                'company_id' => $company_id
            );
            $this->db->select('cat_id,cat_name');
            $this->db->from('category_details');
            $this->db->where($where);
            $query1 = $this->db->get();
            $query = $query1->result_array();
            return $query;
            }
        }

    public

    function get_ids($prod, $cat)
        {
        $where = array(
            'product_name' => $prod
        );
        $this->db->select('product_id');
        $this->db->from('product_management');
        $this->db->where($where);
        $query1 = $this->db->get();
        $query = $query1->row_array();
        $product = $query['product_id'];
        $where1 = array(
            'cat_name' => $cat,
            'prod_id' => $product
        );
        $json = array();
        $this->db->select('cat_id');
        $this->db->from('category_details');
        $this->db->where($where1);
        $query1 = $this->db->get();
        $query = $query1->row_array();
        $cat = $query['cat_id'];
        array_push($json, array(
            "product_id" => $product,
            "cat_id" => $cat
        ));
        return $json;
        }

    public

    function gen_customer($customer)
        {
        $this->db->select('id');
        $this->db->from('customer');
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query2 = $this->db->get();
        $query3 = $query2->result_array();
        $id = $query3[0]['id'] + 1;
        $cust_id = 'Cust_' . str_pad($id, 4, "0", STR_PAD_LEFT);
        $customer1 = array();
        $customer1 = array_merge($customer, array(
            'customer_id' => $cust_id
        ));
        $this->db->insert('customer', $customer1);
        return $cust_id;
        }

    public

    function update_tech_task($tech)
        {
            //commented on jul 3 due to negative value
     /*   $this->db->select('today_task_count,task_count');
        $this->db->from('technician');
        $this->db->where('technician_id', $tech);
        $query1 = $this->db->get();
        $query = $query1->result_array();
        $today_task_count = $query[0]['today_task_count'];
        $count = $today_task_count - 1;
        $task_count = $query[0]['task_count'];
        $task_count = $task_count + 1;
        $this->db->where('technician_id', $tech);
        $dbdata = array(
            "today_task_count" => $count,
            "task_count" => $task_count
        );
        $this->db->update('technician', $dbdata);*/
        return true;
        }

    public

    function amc($data,$ticket_id)
        {
        $this->db->select('id');
        $this->db->from('all_tickets');
        $this->db->where('ticket_id',$ticket_id);
        $query2 = $this->db->get();
        $query3 = $query2->num_rows();
        if($query3<=0)
        {
        $this->db->insert('all_tickets', $data);
        return true;
        }
        else
        {
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('all_tickets', $data);
        return true;
        }
        }

    public

    function update_amc($data, $amc_id)
        {
        $this->db->where('amc_id', $amc_id);
        $this->db->update('all_tickets', $data);
        return true;
        }

    public

    function select_amc()
        {
        $this->db->select('id');
        $this->db->from('all_tickets');
        $this->db->order_by('all_tickets.id', 'desc');
        $this->db->limit(1);
        $query1 = $this->db->get();
        $query = $query1->result_array();
        if (!empty($query))
            {
            $id = $query[0]['id'] + 1;
            $new_amc_id = 'AMC' . str_pad($id, 4, "0", STR_PAD_LEFT);
            }
          else
            {
            $new_amc_id = 'AMC0001';
            }

        return $new_amc_id;
        }

    public

    function search_amc($amc_id,$company_id)
        {
        $result = array();
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->join('amc_type', 'customer.type_of_contract=amc_type.amc_type');
        $this->db->join('product_management', 'customer.product_serial_no=product_management.product_id');
        //$this->db->join('category_details', 'customer.component_serial_no=category_details.cat_id');
        $this->db->where('customer.company_id',$company_id);
        $this->db->group_start();
        $this->db->where('customer.contract_id', $amc_id, FALSE);
        $this->db->or_where('customer.contact_number', $amc_id, NULL, FALSE);
        $this->db->group_end();
        $this->db->group_by('customer.customer_id');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        foreach($query as $row)
            {
            array_push($result, array(
                'customer_id' => $row['customer_id'],
                'customer_name' => $row['customer_name'],
                'customer_email' => $row['email_id'],
                'door_no' => $row['door_num'],
                'street' => $row['address'],
                'town' => $row['cust_town'],
                'country' => $row['cust_country'],
                'contact_num' => $row['contact_number'],
                'contract_id' => $amc_id,
                'type_cont' => $row['type_of_contract'],
                'contract_period' => $row['contract_period'],
                'product_name' => $row['product_name'],
                'cat_name' => $row['cat_name'],
                'product_id' => $row['product_serial_no'],
                'cat_id' => $row['component_serial_no'],
                'serial_no' => $row['serial_no'],
                'model_no' => $row['model_no'],
                'quantity' => 1
            ));
            }

        return $result;
        }

        function find_amc($amc_id,$company_id)
        {
        $result = array();
        $this->db->select('*,product_management.product_name,category_details.cat_name,amc_type.contract_period');
        $this->db->from('customer');
        $this->db->join('amc_type', 'customer.type_of_contract=amc_type.amc_type');
        $this->db->join('product_management', 'customer.product_serial_no=product_management.product_id');
        $this->db->join('category_details', 'customer.component_serial_no=category_details.cat_id');
        $this->db->where('customer.company_id',$company_id);
        $this->db->group_start();
        $this->db->where('customer.contract_id', $amc_id);
        $this->db->or_where('customer.contact_number', $amc_id);
        $this->db->group_end();
        $this->db->group_by('customer.customer_id');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        $count = $query1->num_rows();
        if($count > 0)
        {
            return 1;
        }
        // foreach($query as $row)
        //     {
        //     array_push($result, array(
        //         'customer_id' => $row['customer_id'],
        //         'customer_name' => $row['customer_name'],
        //         'door_no' => $row['door_num'],
        //         'street' => $row['address'],
        //         'town' => $row['cust_town'],
        //         'country' => $row['cust_country'],
        //         'contact_num' => $row['contact_number'],
        //         'contract_id' => $amc_id,
        //         'type_cont' => $row['type_of_contract'],
        //         'contract_period' => $row['contract_period'],
        //         'product_name' => $row['product_name'],
        //         'cat_name' => $row['cat_name'],
        //         'serial_no' => $row['serial_no'],
        //         'model_no' => $row['model_no'],
        //         'quantity' => 1
        //     ));
        //     }

        // return $result;
        }


    public

    function amc_price($cat_name, $tech_id, $amc_type, $quantity)
        {
        $result = 0;
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('company_id');
        $this->db->from('technician');
        $this->db->where($where);
        $que = $this->db->get();
        $que1 = $que->result_array();
        $company_id = $que1[0]['company_id'];
        $where2 = array(
            'cat_name' => $cat_name
        );
        $this->db->select('*');
        $this->db->from('category_details');
        $this->db->where($where2);
        $que2 = $this->db->get();
        $que12 = $que2->result_array();
        $cat_id = $que12[0]['cat_id'];
        $where1 = array(
            'company_id' => $company_id,
            'cat_id' => $cat_id,
            'contract_type' => $amc_type
        );
        $this->db->select('*');
        $this->db->from('amc_type');
        $this->db->where($where1);
        $query1 = $this->db->get();
        $query = $query1->result_array();
        $result = $query[0]['contract_amount'] * $quantity;
        return $result;
        }

    public

    function training($tech_id)
        {
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        $product_id = $result[0]['product'];
        $cat_id = $result[0]['category'];
        $company_id = $result[0]['company_id'];
        $json = array();
        $where2 = array(
            'product' => $product_id,
            'category' => $cat_id,
            'company_id' => $company_id
        );
        $this->db->select('*');
        $this->db->from('training');
        $this->db->where($where2);
        $query2 = $this->db->get();
        $result2 = $query2->result_array();
        foreach($result2 as $row)
            {
            $pdf_link = array();
            $video_link = array();
            $where3 = array(
                'training_id' => $row['training_id'],
                'type' => 'PDF'
            );
            $this->db->select('*');
            $this->db->from('training_link');
            $this->db->where($where3);
            $query3 = $this->db->get();
            $result3 = $query3->result_array();
            foreach($result3 as $row1)
                {
                array_push($pdf_link, array(
                    "id" => $row1['id'],
                    "title" => $row1['title'],
                    "link" => $row1['link']
                ));
                }

            $where6 = array(
                'training_id' => $row['training_id'],
                'type' => 'Video'
            );
            $this->db->select('*');
            $this->db->from('training_link');
            $this->db->where($where6);
            $query6 = $this->db->get();
            $result6 = $query6->result_array();
            foreach($result6 as $row6)
                {
                array_push($video_link, array(
                    "id" => $row6['id'],
                    "title" => $row6['title'],
                    "link" => $row6['link'],
                    "video_time" => $row6['video_time']
                ));
                }

            array_push($json, array(
                "training id" => $row['training_id'],
                "certificate_id" => $row['certificate_id'],
                "thumbnail" => $row['thumbnail'],
                "title" => $row['title'],
                "pdf_link" => $pdf_link,
                "video" => $video_link
            ));
            }
        $where4 = array(
            'product' => "",
            'category' => "",
            'company_id' => $company_id
        );
        $this->db->select('*');
        $this->db->from('training');
        $this->db->where($where4);
        $query4 = $this->db->get();
        $result4 = $query4->result_array();
        foreach($result4 as $row2)
            {
            $pdf_link = array();
            $video_link = array();
            $where5 = array(
                'training_id' => $row2['training_id'],
                'type' => 'PDF'
            );
            $this->db->select('*');
            $this->db->from('training_link');
            $this->db->where($where5);
            $query5 = $this->db->get();
            $result5 = $query5->result_array();
            foreach($result5 as $row5)
                {
                array_push($pdf_link, array(
                    "id" => $row5['id'],
                    "title" => $row5['title'],
                    "link" => $row5['link']
                ));
                }

            $where7 = array(
                'training_id' => $row2['training_id'],
                'type' => 'Video'
            );
            $this->db->select('*');
            $this->db->from('training_link');
            $this->db->where($where7);
            $query7 = $this->db->get();
            $result7 = $query7->result_array();
            foreach($result7 as $row7)
                {
                array_push($video_link, array(
                    "id" => $row7['id'],
                    "title" => $row7['title'],
                    "link" => $row7['link'],
                    "video_time" => $row7['video_time']
                ));
                }

            array_push($json, array(
                "training id" => $row2['training_id'],
                "certificate_id" => $row2['certificate_id'],
                "thumbnail" => $row2['thumbnail'],
                "title" => $row2['title'],
                "pdf_link" => $pdf_link,
                "video" => $video_link
            ));
            }

        return $json;
        }
 public function certificate($tech_id)
 {
    $finalresult=array();
     $json = array();
        $where = array(
            'technician_id' => $tech_id
        );

        $companyid='';
        $productarray=array();
        $subcategoryarray=array();
        $this->db->select('product,category,company_id');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
         $company_id = $result[0]['company_id'];
        foreach($result as $value)
        {
            $productarray[]=$value['product'];
            $subcategoryarray[]=$value['category'];
        }
      
       $this->db->select('id,certificate_id,certificate_name,product_id,cat_id,title,status,score as totalquestions, random_quiz as questionstoanswer, company_id,ref_training,certificate_type,(select score from training_score WHERE training_score.tech_id="'.$tech_id.'" AND training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id order BY id desc LIMIT 1) as previous_score, (select status from training_score WHERE training_score.tech_id="'.$tech_id.'" AND training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id order BY id desc LIMIT 1) as status');
       $this->db->from('certificate');
       $this->db->where_in('product_id',$productarray);
      $this->db->where_in('cat_id',$subcategoryarray);
      $this->db->where('company_id',$company_id);
        $query1 = $this->db->get();
        $result1 = $query1->result_array();

        $this->db->select('id,certificate_id,certificate_name,product_id,cat_id,title,status,score as totalquestions, random_quiz as questionstoanswer, company_id,ref_training,certificate_type,(select score from training_score WHERE training_score.tech_id="'.$tech_id.'" AND training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id order BY id desc LIMIT 1) as previous_score, (select status from training_score WHERE training_score.tech_id="'.$tech_id.'" AND training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id order BY id desc LIMIT 1) as status');
        $this->db->from('certificate');
        $this->db->where('product_id','');
       $this->db->where('cat_id','');
       $this->db->where('company_id',$company_id);
         $query2 = $this->db->get();
         $result2 = $query2->result_array();

         $dataarray=array();

        $arraydata=array_merge($result1,$result2);

        foreach( $arraydata as $value1)
        {
            if($value['previous_score']==null || $value['previous_score']=='' || $value['previous_score']==NULL)
            {
                $value['previous_score']=0;
            }
            $quizaaray=array();
            $this->db->select('*');
            $this->db->from('quiz');
            $this->db->where('certificate_id',$value1['certificate_id']);
            $this->db->order_by('rand()');
            $this->db->limit($value1['score']);
            $query2 = $this->db->get();
            $result2=$query2->result_array();
     
           $trainingarray=array();
           $this->db->select('training_id');
           $this->db->from('training');
           $this->db->where('certificate_id',$value1['certificate_id']);
           $this->db->where('company_id',$company_id);

      if($value1['certificate_type']=="product")
      {
      $this->db->where('product',$value1['product_id']);
      $this->db->where('category',$value1['cat_id']);
      }
       $query3 = $this->db->get();
       $result3=$query3->result_array();
       $score=$value1['previous_score'];
       $status=$value1['status'];
       if($value1['status'] == null)
       {
       
        $status=0;
       }
       if($value1['previous_score'] == null)
       {
        $score=0;
      
       }
       $finalresult[]=array("title"=>$value1['certificate_name'],
                            "totalquestions"=>$value1['totalquestions'],
                            "questionstoanswer"=>$value1['questionstoanswer'],
                            "score"=> $score,
                            "status"=> $status,
                            "certifcate_id"=> $value1['certificate_id'],
                            "Training Details"=>$result3,
                            "quiz"=>$result2);
        }

         return $finalresult;
        
 }
    public function certificate_old($tech_id)
        {
        $json = array();
        $where = array(
            'technician_id' => $tech_id
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        $product_id = $result[0]['product'];
        $cat_id = $result[0]['category'];
        $company_id = $result[0]['company_id'];
        $where2 = array(
            'product_id' => $product_id,
            'cat_id' => $cat_id,
            'certificate.company_id' => $company_id
        );
        $this->db->select('*,certificate.score as threshold,certificate.certificate_name as cert_title, (select training_score.score from training_score WHERE training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id ORDER BY training_score.id desc limit 1) as t_score');
        $this->db->from('certificate');
        //$this->db->join('training_score', 'training_score.certificate_id=certificate.certificate_id');
        $this->db->where($where2);
        $query2 = $this->db->get();
        $result2 = $query2->result_array();
       
        if (($result2[0]['t_score']!=NULL))
            {
            foreach($result2 as $row)
                {
                $certificate_id = $row['certificate_id'];
                $where3 = array(
                    'certificate_id' => $certificate_id,
                    'training.company_id' => $company_id
                );
                $this->db->select('training_id');
                $this->db->from('training');
                $this->db->where($where3);
                $query3 = $this->db->get();
                $result3 = $query3->result_array();
                $count = $query3->num_rows();
                $where5 = array(
                    'certificate_id' => $certificate_id
                );
                $this->db->select('*');
                $this->db->from('quiz');
                $this->db->where($where5);
                $query5 = $this->db->get();
                $quiz = $query5->result_array();
                //$random_keys = array_rand($quiz, $row['random_quiz']);
                $quiz_a = array();
                /*foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }*/
/*Changed on 20/06/2018*/
    if(count($quiz)>(int)$row['random_quiz']){
                $random_keys = array_rand($quiz, (int)$row['random_quiz']);
                
                if(count($random_keys)>1){
                foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }
}
else if(count($random_keys)==1)
{
array_push($quiz_a, $quiz[$random_keys]);
}
}
else
{
    foreach($quiz as $row1)
                {
                    array_push($quiz_a, $row1);
                }   
}
/*Change end*/

                array_push($json, array(
                    "title" => $row['cert_title'],
                    "pass_threshold" => $row['threshold'],
                    "score" => $row['t_score'],
                    "status" => $row['status'],
                    "certifcate_id" => $certificate_id,
                    "Training Details" => $result3,
                    "quiz" => $quiz_a
                ));
                }
            }
          else
            {
            $this->db->select('*');
            $this->db->from('certificate');
            $this->db->where($where2);
            $query2 = $this->db->get();
            $result2 = $query2->result_array();
           
            foreach($result2 as $row)
                {
                $certificate_id = $row['certificate_id'];
                $where3 = array(
                    'certificate_id' => $certificate_id,
                    'training.company_id' => $company_id
                );
                $this->db->select('training_id');
                $this->db->from('training');
                $this->db->where($where3);
                $query3 = $this->db->get();
                $result3 = $query3->result_array();
                $count = $query3->num_rows();
                $where5 = array(
                    'certificate_id' => $certificate_id,
                    'company_id' => $company_id
                );
                $this->db->select('*');
                $this->db->from('quiz');
                $this->db->where($where5);
                $query5 = $this->db->get();
                $quiz = $query5->result_array();
                $random_keys = array_rand($quiz, (int)$row['random_quiz']);
                $quiz_a = array();
                /*foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }*/
/*Changed on 20/06/2018*/
if(count($quiz)>(int)$row['random_quiz']){
if(count($random_keys)>1){
                foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }
}
else if(count($random_keys)==1)
{
    
array_push($quiz_a, $quiz[$random_keys]);
}
}
else
{
    foreach($quiz as $row1)
                {
                    array_push($quiz_a, $row1);
                }
}
/*Change end*/
                array_push($json, array(
                    "title" => $row['title'],
                    "pass_threshold" => $row['score'],
                    "score" => 0,
                    "status" => "0",
                    "certifcate_id" => $certificate_id,
                    "Training Details" => $result3,
                    "quiz" => $quiz_a
                ));
                }
            }
        $where5 = array(
            'product_id' => "",
            'cat_id' => "",
            'certificate.company_id' => $company_id
        );
        $this->db->select('*,certificate.score as threshold,certificate.certificate_name as cert_title, (select training_score.score from training_score WHERE training_score.certificate_id=certificate.certificate_id AND training_score.company_id=certificate.company_id ORDER BY training_score.id desc limit 1) as t_score');
        $this->db->from('certificate');
        //$this->db->join('training_score', 'training_score.certificate_id=certificate.certificate_id');
        $this->db->where($where5);
        $query5 = $this->db->get();
        $result5 = $query5->result_array();
     
        if (($result5[0]['t_score']!=NULL))
            {
            foreach($result5 as $row5)
                {
                $certificate_id = $row5['certificate_id'];
                $where6 = array(
                    'certificate_id' => $certificate_id,
            'company_id' => $company_id
                );
                
                $this->db->select('training_id');
                $this->db->from('training');
                $this->db->where($where6);
                $this->db->order_by('id', 'desc');
                $this->db->group_by('certificate_id');
                $query6 = $this->db->get();
                $result6 = $query6->result_array();
               
                $count = $query6->num_rows();
                $where5 = array(
                    'certificate_id' => $certificate_id
                );
                $this->db->select('*');
                $this->db->from('quiz');
                $this->db->where($where5);
                $this->db->order_by('id', 'desc');
              
                $query5 = $this->db->get();
                $quiz = $query5->result_array();
                $quiz_a = array();
                /*  foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }*/
/*Changed on 20/06/2018*/
                if(count($quiz)>(int)$row['random_quiz']){
                $random_keys = array_rand($quiz, (int)$row5['random_quiz']);
                
            
if(count($random_keys)>1){
                foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }
}
else if(count($random_keys)==1)
{
    
array_push($quiz_a, $quiz[$random_keys]);
}
}
else
{
    foreach($quiz as $row1)
                {
                    array_push($quiz_a, $row1);
                }   
}
/*Change end*/
                array_push($json, array(
                    "title" => $row5['cert_title'],
                    "pass_threshold" => $row5['threshold'],
                    "score" =>  $row5['t_score'],
                    "status" => $row5['status'],
                    "certifcate_id" => $certificate_id,
                    "Training Details" => $result6,
                    "quiz" => $quiz_a
                ));
                }
            }
          else
            {
                $where5 = array(
                    'product_id' => "",
                    'cat_id' => "",
                    'certificate.company_id' => $company_id
                );
            $this->db->select('*');
            $this->db->from('certificate');
            $this->db->where($where5);
            $query2 = $this->db->get();
            $result2 = $query2->result_array();
           
            foreach($result2 as $row)
                {
                $certificate_id = $row['certificate_id'];
                $where3 = array(
                    'certificate_id' => $certificate_id,
                    'company_id' => $company_id
                );
                $this->db->select('training_id');
                $this->db->from('training');
                $this->db->where($where3);
                $query3 = $this->db->get();
                $result3 = $query3->result_array();
                $count = $query3->num_rows();
                $where5 = array(
                    'certificate_id' => $certificate_id
                );
                $this->db->select('*');
                $this->db->from('quiz');
                $this->db->where($where5);
                $query5 = $this->db->get();
                $quiz = $query5->result_array();
                $quiz_a = array();
                
                
                /*foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }*/
/*Changed on 20/06/2018*/
if(count($quiz)>(int)$row['random_quiz']){
    $random_keys = array_rand($quiz, (int)$row['random_quiz']);
    if(count($random_keys)>1){
                foreach($random_keys as $r)
                    {
                    array_push($quiz_a, $quiz[$r]);
                    }
}
else if(count($random_keys)==1)
{
    
array_push($quiz_a, $quiz[$random_keys]);
}
}
else
{
    foreach($quiz as $row1)
                {
                    array_push($quiz_a, $row1);
                }   
}
/*Change end*/
                array_push($json, array(
                    "title" => $row['title'],
                    "pass_threshold" => $row['score'],
                    "score" => 0,
                    "status" => "0",
                    "certifcate_id" => $certificate_id,
                    "Training Details" => $result3,
                    "quiz" => $quiz_a
                ));
                }
            }

        return $json;
        }

    public

    function get_tech_tkt($tech_id, $start, $end)
        {
        $start_date = date('Y-m-d 00:00:00', strtotime($start));
        $end_date = date('Y-m-d 23:59:59', strtotime($end));
        $where = array(
            'tech_id' => $tech_id
        );
        $where1 = array(
            'prev_tech_id' => $tech_id
        );
        $where2 = array(
            'cust_preference_date>=' => $start_date,
            'cust_preference_date<=' => $end_date
        );
        $this->db->select('ticket_id');
        $this->db->from('all_tickets');
        $this->db->group_start();
        $this->db->where('current_status', 12);
        $this->db->where($where2);
        $this->db->group_end();
        $this->db->group_start();
        $this->db->or_where($where);
        $this->db->or_where($where1);
        $this->db->group_end();
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        }

    public

    function spare_view($spare_code)
        {
        $json = array();
        $where = array(
            'spare_code' => $spare_code
        );
        $this->db->select('*');
        $this->db->from('spare');
        $this->db->join('product_management', 'product_management.product_id=spare.product');
        $this->db->join('category_details', 'category_details.cat_id=spare.category');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            {
            foreach($result as $row)
                {
                $json = '<html><body><table style="width:100%;height:100%;border:1px solid black;"><tr><td>Spare Code</td><td>' . $row['spare_code'] . '</td></tr><tr><td>Spare Name</td><td>' . $row['spare_name'] . '</td></tr><tr><td>Spare source</td><td>' . $row['spare_source'] . '</td></tr><tr><td>Price</td><td>' . $row['price'] . '</td></tr><tr><td>Quantity Purchased</td><td>' . $row['quantity_purchased'] . '</td></tr><tr><td>Quantity Used</td><td>' . $row['quantity_used'] . '</td></tr><tr><td>Product</td><td>' . $row['product_name'] . '</td></tr><tr><td>Category</td><td>' . $row['cat_name'] . '</td></tr></table></body></html>';
                }

            $res = array(
                'spare_details' => $json
            );
            return $res;
            }
        }

    public

    function spare_search($spare_code)
        {
        $spare_code = json_decode($spare_code, 'true');
        $json = array();
        $price = 0;
        $availability = 0;
        foreach($spare_code as $sp)
            {
            $where = array(
                'spare_location' => $sp['spare_source'],
                'spare.spare_code' => $sp['Spare_code']
            );
            $this->db->select('*');
            $this->db->from('spare_location,spare');
            $this->db->where($where);
            $query = $this->db->get();
            $result = $query->result_array();
            if (!empty($result))
                {
                foreach($result as $row)
                    {
                    if($availability<=$row['days_to_deliver'])
                    {
                    $availability=$row['days_to_deliver'];
                    }
                    $sp_price = $row['price'];
                    'price' . $price = $price + ($sp['quantity'] * $sp_price);
                    }
                }
        }
$availability=$availability.' days';
        array_push($json, array(
            "price" => $price,
            "avalability" => $availability
        ));
        return $json;
        }

    public

    function request_spare($data, $ticket_id)
        {
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('all_tickets', $data);
        return true;
        }

        public function rerequest_spare($ticket_id,$tech_id,$company_id)
            {

             
			$data = array(
			    "current_status" => 14
            );  
             
            $this->db->where('ticket_id', $ticket_id);
            $this->db->where('tech_id', $tech_id);
            $this->db->where('company_id', $company_id);
            $this->db->update('all_tickets', $data);
            return true;
            }


    public function submit_quiz($data)
        {
	    //print_r($data);exit;
	    $certificate_id = $data['certificate_id'];
	    $tech_id = $data['tech_id'];
	    $status = $data['status'];
	    $score = $data['score'];
            $company_id = $data['company_id'];		
	    // Check whether the technician already atteneted the quiz
            $where = array(
                'certificate_id' => $certificate_id,
                'tech_id' => $tech_id
            );
            $this->db->select('id');
            $this->db->from('training_score');
            $this->db->where($where);
            $query = $this->db->get();
	    //print_r($data);exit;
            $result = $query->result_array();
    	    // insert score if not exist
            if (empty($result))
                {
        		$this->db->insert('training_score', $data);
		        return true;
		}
    	    // Update score if exist
		else
		{
			$data = array(
			    "status" => $status,
			    "score" => $score
			);
			$this->db->where("tech_id", $tech_id);
			$this->db->where("certificate_id", $certificate_id);
			$this->db->where("company_id", $company_id);
			$this->db->update("training_score", $data);
			return true;
		}
        }

    public function get_notify($tech_id)
        {
        $json = array();
        $where = array(
            'tech_id' => $tech_id,
            'role' => 'Technician'
        );
        $this->db->select('*,last_update as last');
        $this->db->from('notification');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $row)
            {
            $timestamp = strtotime($row['last']);
            array_push($json, array(
                'msg' => $row['msg'],
                'key' => $row['key'],
                'time' => $timestamp
            ));
            }

        return $json;
        }

    public

    function android_get_new_tkts($tech_id)
        {
        $data = array();
        $data1 = array();
        $data2 = array();
        $date = date("Y-m-d");
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_img, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        foreach($query as $row)
            {
            
            if($row['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row['assigned']);
            $last = new DateTime($row['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row['response_time'])
                {
                $address = $row['add'];
                $datetime1 = $row['last'];
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $schedulenext = $row['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));

                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url().$row['ticket_img'],
                    'audio' => base_url().$row['audio'],
                    'video' => base_url().$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' =>$schedule_next,
                    'status' => $row['current_status'],
                    'prev_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours',
                    'companyid' => $row['companyid']
                ));
                }
              else
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                $schedulenext = $row['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data1, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url().$row['ticket_img'],
                    'audio' => base_url().$row['audio'],
                    'video' => base_url().$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' => $row['schedule_next'],
                    'status' => $row['current_status'],
                    'perv_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours',
                    'companyid' => $row['companyid']
                ));
                }
            }
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id!=' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query11 = $this->db->get();
        $query2 = $query11->result_array();
        foreach($query2 as $row2)
            {
            
            if($row2['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row2['assigned']);
            $last = new DateTime($row2['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row2['response_time'])
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $schedulenext = $row2['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));

                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data, array(
                'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url().$row2['ticket_image'],
                    'audio' => base_url().$row2['audio'],
                    'video' => base_url().$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' => $schedule_next,
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours',
                    'companyid' => $row2['companyid']
                ));
                }
              else
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                $schedulenext = $row2['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data, array(
                    'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url().$row2['ticket_image'],
                    'audio' => base_url().$row2['audio'],
                    'video' => base_url().$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' => $schedulenext,
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours',
                    'companyid' => $row2['companyid']

                ));
                }
            }

    // $where41 = array(
    //         'all_tickets.tech_id' => $tech_id,
    //         'all_tickets.amc_id' => ''
    //     );
    //     //$cur_status=["1","0","12","17","19","20"];
    //     $cur_status=["0","12","17","19","20","8","15","11","14","7","6","50","10","2","3","5"];
    //     $prev_status=["15"];
    //     $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
    //     $this->db->from('all_tickets');
    //     $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
    //     $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
    //     $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
    //     $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
    //     $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
    //     $this->db->where($where41);
    //     $this->db->like('all_tickets.cust_preference_date', $date);
    //     $this->db->group_start();
    //     $this->db->where_not_in('current_status',$cur_status);
    //     $this->db->where_not_in('previous_status',$prev_status);
    //     $this->db->group_end();
    //     $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
    //     $this->db->order_by('all_tickets.last_update','desc');
    //     $query1233 = $this->db->get();
    //     $query21 = $query1233->result_array();
    //     foreach($query21 as $row1)
    //         {
    //         $assigned = new DateTime($row1['assigned']);
    //         $last = new DateTime($row1['last']);
    //         $interval = $assigned->diff($last);
    //         $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
    //         if ($elapsed <= $row1['response_time'])
    //             {
                
    //         if($row1['previous_status']=="15")
    //         {
    //         $prev=15;
    //         }
    //         else
    //         {
    //         $prev="";
    //         }
    //             $address = $row1['add'];
    //             $datetime1 = $row1['last'];;
    //             $date1 = date('Y-m-d', strtotime($datetime1));
    //             $time1 = date('H:i:s', strtotime($datetime1));
    //             $latitude = $row1['latitud'];
    //             $longitude = $row1['longitud'];
    //             $schedulenext = $row1['schedule_next'];
    //             $schedule_next = date('Y-m-d',strtotime($schedulenext));
    //             array_push($data, array(
    //                 'ticket_id' => $row1['ticket_id'],
    //                 'ticket_type' => 'regular',
    //                 'amc_id' => "",
    //                 'current_tech_id' => "",
    //                 'current_tech_name' => "",
    //                 'tech_contact_no' => "",
    //                 'tech_email_id' => "",
    //                 'cust_id' => $row1['cust_id'],
    //                 'customer_name' => $row1['customer_name'],
    //                 'contact_number' => $row1['cont_num'],
    //                 'alternate_number' => $row1['alternate_number'],
    //                 'door_no' => $row1['door_no'],
    //                 'address' => $row1['add'],
    //                 'city' => $row1['town_tkt'],
    //                 /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
    //                 'location' => $row1['cust_town'],
    //                 'country' => $row1['country'],
    //                 'latitude' => $latitude,
    //                 'longitude' => $longitude,
    //                 'priority' => $row1['prio'],
    //                 'call_category' => $row1['call_type'],
    //                 'call_tag' => $row1['call'],
    //                 'prob_description' => $row1['prob_desc'],
    //                 'image' => base_url().$row1['ticket_image'],
    //                 'audio' => base_url().$row1['audio'],
    //                 'video' => base_url().$row1['video'],
    //                 'tech_id' => $row1['tech_id'],
    //                 'type_cont' => $row1['call_type'],
    //                 'contract_period' => $row1['contract_period'],
    //                 'product_name' => $row1['product_name'],
    //                 'cat_name' => $row1['cat_name'],
    //                 'quantity' => $row1['quantity'],
    //                 'lead_time' => $row1['lead_time'],
    //                 'schedule_next' => $schedule_next,
    //                 'status' => $row1['current_status'],
    //                 'prev_status' => "",
    //                 'created_date' => $date1,
    //                 'created_time' => $time1,
    //                 'response_time' => $row1['response_time'] . ' Hours',
    //                 'resolution_time' => $row1['resolution_time'] . ' Hours',
    //                 'companyid' => "company11".$row1['companyid']
    //             ));
    //             }
    //           else
    //             {
    //             $address = $row1['add'];
    //             $datetime1 = $row1['last'];;
    //             $date1 = date('Y-m-d', strtotime($datetime1));
    //             $time1 = date('H:i:s', strtotime($datetime1));
    //             $latitude = $row1['latitud'];
    //             $longitude = $row1['longitud'];
    //             $schedulenext = $row1['schedule_next'];
    //             $schedule_next = date('Y-m-d',strtotime($schedulenext));
    //             array_push($data1, array(
    //                 'ticket_id' => $row1['ticket_id'],
    //                 'ticket_type' => 'regular',
    //                 'amc_id' => "",
    //                 'current_tech_id' => "",
    //                 'current_tech_name' => "",
    //                 'tech_contact_no' => "",
    //                 'tech_email_id' => "",
    //                 'cust_id' => $row1['cust_id'],
    //                 'customer_name' => $row1['customer_name'],
    //                 'contact_number' => $row1['cont_num'],
    //                 'alternate_number' => $row1['alternate_number'],
    //                 'door_no' => $row1['door_no'],
    //                 'address' => $row1['add'],
    //                 'city' => $row1['town_tkt'],
    //                 /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
    //                 'location' => $row1['cust_town'],
    //                 'country' => $row1['country'],
    //                 'latitude' => $latitude,
    //                 'longitude' => $longitude,
    //                 'priority' => $row1['prio'],
    //                 'call_category' => $row1['call_type'],
    //                 'call_tag' => $row1['call'],
    //                 'prob_description' => $row1['prob_desc'],
    //                 'image' => base_url().$row1['ticket_image'],
    //                 'audio' => base_url().$row1['audio'],
    //                 'video' => base_url().$row1['video'],
    //                 'tech_id' => $row1['tech_id'],
    //                 'type_cont' => $row1['call_type'],
    //                 'contract_period' => $row1['contract_period'],
    //                 'product_name' => $row1['product_name'],
    //                 'cat_name' => $row1['cat_name'],
    //                 'quantity' => $row1['quantity'],
    //                 'lead_time' => $row1['lead_time'],
    //                 'schedule_next' => $row1['schedule_next'],
    //                 'status' => $row1['current_status'],
    //                 'perv_status' => "",
    //                 'created_date' => $date1,
    //                 'created_time' => $time1,
    //                 'response_time' => $row1['response_time'] . ' Hours',
    //                 'resolution_time' => $row1['resolution_time'] . ' Hours',
    //                 'companyid' => $row1['companyid']
    //             ));
    //             }
    //         }


    $where41 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.amc_id!=' => ''
        );
       // $cur_status=["1","0","12","17","19","20"];
        $cur_status=["0","12","17","19","20","8","15","11","14","7","6","50","10","2","3","5"];
        $prev_status=["15"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where41);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_start();
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->where_not_in('previous_status',$prev_status);
        $this->db->group_end();
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query12 = $this->db->get();
        $query21 = $query12->result_array();
        foreach($query21 as $row21)
            {
            $assigned = new DateTime($row21['assigned']);
            $last = new DateTime($row21['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row21['response_time'])
                {
                
            if($row21['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $schedulenext = $row21['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));

                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                array_push($data, array(
                'ticket_id' => $row21['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row21['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row21['cust_id'],
                    'customer_name' => $row21['customer_name'],
                    'contact_number' => $row21['cont_num'],
                    'alternate_number' => $row21['alternate_no'],
                    'door_no' => $row21['door_no'],
                    'address' => $row21['add'],
                    'city' => $row21['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $ro21['cust_town'],
                    'country' => $row21['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row21['call_type'],
                    'priority' => $row21['prior'],
                    'prob_description' => $row21['prob_desc'],
                    'image' => base_url().$row21['ticket_image'],
                    'audio' => base_url().$row21['audio'],
                    'video' => base_url().$row21['video'],
                    'tech_id' => $row21['tech_id'],
                    'type_cont' => $row21['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row21['contract_period'],
                    'product_name' => $row21['product_name'],
                    'cat_name' => $row21['cat_name'],
                    'quantity' => $row21['quantity'],
                    'lead_time' => $row21['lead_time'],
                    'schedule_next' => $row21['schedule_next'],
                    'status' => $row21['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row21['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row21['response_time'] . ' Hours',
                    'resolution_time' => $row21['resolution_time'] . ' Hours',
                    'companyid' => $row21['companyid']
                ));
                }
              else
                {
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                $schedulenext = $row21['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data1, array(
                    'ticket_id' => $row21['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row21['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row21['cust_id'],
                    'customer_name' => $row21['customer_name'],
                    'contact_number' => $row21['cont_num'],
                    'alternate_number' => $row21['alternate_no'],
                    'door_no' => $row21['door_no'],
                    'address' => $row21['add'],
                    'city' => $row21['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row21['cust_town'],
                    'country' => $row21['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row21['call_type'],
                    'priority' => $row21['prior'],
                    'prob_description' => $row21['prob_desc'],
                    'image' => base_url().$row21['ticket_image'],
                    'audio' => base_url().$row21['audio'],
                    'video' => base_url().$row21['video'],
                    'tech_id' => $row21['tech_id'],
                    'type_cont' => $row21['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row21['contract_period'],
                    'product_name' => $row21['product_name'],
                    'cat_name' => $row21['cat_name'],
                    'quantity' => $row21['quantity'],
                    'lead_time' => $row21['lead_time'],
                    'schedule_next' => $row21['schedule_next'],
                    'status' => $row21['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row21['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row21['response_time'] . ' Hours',
                    'resolution_time' => $row21['resolution_time'] . ' Hours',
                    'companyid' => $row21['companyid']
                ));
                }
            }
            $where42 = array(
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id' => ''
        );
        //$cur_status=["1","0","12","17","19","20","9"];
        $cur_status=["0","12","17","19","20","9","8","15","11","14","7","6","50","10","2","3","5"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where42);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query122 = $this->db->get();
        $query212 = $query122->result_array();
        foreach($query212 as $row12)
            {
            if($row12['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row12['assigned']);
            $last = new DateTime($row12['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row12['response_time'])
                {
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
                $schedulenext = $row12['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data, array(
                    'ticket_id' => $row12['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' =>  $row12['tech_id'],
                    'current_tech_name' =>  $row12['tech_name'],
                    'tech_contact_no' => $row12['tech_contact'],
                    'tech_email_id' => $row12['tech_email_id'],
                    'cust_id' => $row12['cust_id'],
                    'customer_name' => $row12['customer_name'],
                    'contact_number' => $row12['cont_num'],
                    'alternate_number' => $row12['alternate_number'],
                    'door_no' => $row12['door_no'],
                    'address' => $row12['add'],
                    'city' => $row12['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row12['cust_town'],
                    'country' => $row12['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row12['prio'],
                    'call_category' => $row12['call_type'],
                    'call_tag' => $row12['call'],
                    'prob_description' => $row12['prob_desc'],
                    'image' => base_url().$row12['ticket_image'],
                    'audio' => base_url().$row12['audio'],
                    'video' => base_url().$row12['video'],
                    'tech_id' => $row12['tech_id'],
                    'type_cont' => $row12['call_type'],
                    'contract_period' => $row12['contract_period'],
                    'product_name' => $row12['product_name'],
                    'cat_name' => $row12['cat_name'],
                    'quantity' => $row12['quantity'],
                    'lead_time' => $row12['lead_time'],
                    'schedule_next' => $schedulenext,
                    'status' => $row12['current_status'],
                    'prev_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row12['response_time'] . ' Hours',
                    'resolution_time' => $row12['resolution_time'] . ' Hours',
                    'companyid' => $row21['companyid']
                ));
                }
              else
                {
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
                $schedulenext = $row12['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data1, array(
                    'ticket_id' => $row12['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => $row12['tech_id'],
                    'current_tech_name' => $row12['tech_name'],
                    'tech_contact_no' => $row12['tech_contact'],
                    'tech_email_id' => $row12['tech_email_id'],
                    'cust_id' => $row12['cust_id'],
                    'customer_name' => $row12['customer_name'],
                    'contact_number' => $row12['cont_num'],
                    'alternate_number' => $row12['alternate_number'],
                    'door_no' => $row12['door_no'],
                    'address' => $row12['add'],
                    'city' => $row12['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row12['cust_town'],
                    'country' => $row12['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row12['prio'],
                    'call_category' => $row12['call_type'],
                    'call_tag' => $row12['call'],
                    'prob_description' => $row12['prob_desc'],
                    'image' => base_url().$row12['ticket_image'],
                    'audio' => base_url().$row12['audio'],
                    'video' => base_url().$row12['video'],
                    'tech_id' => $row12['tech_id'],
                    'type_cont' => $row12['call_type'],
                    'contract_period' => $row12['contract_period'],
                    'product_name' => $row12['product_name'],
                    'cat_name' => $row12['cat_name'],
                    'quantity' => $row12['quantity'],
                    'lead_time' => $row12['lead_time'],
                    'schedule_next' => $schedulenext,
                    'status' => $row12['current_status'],
                    'perv_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row12['response_time'] . ' Hours',
                    'resolution_time' => $row12['resolution_time'] . ' Hours',
                    'companyid' => $row12['companyid']
                ));
                }
            }
    $where413 = array(
            
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id!=' => ''
        );
       // $cur_status=["1","0","12","17","19","20","9"];
        $cur_status=["0","12","17","19","20","9","8","15","11","14","7","6","50","10","2","3","5"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where413);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query123 = $this->db->get();
        $query213 = $query123->result_array();
        foreach($query213 as $row213)
            {
            if($row213['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row213['assigned']);
            $last = new DateTime($row213['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row213['response_time'])
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                
                array_push($data, array(
                'ticket_id' => $row213['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row213['amc_id'],
                    'current_tech_id' => $row213['tech_id'],
                    'current_tech_name' => $row213['tech_name'],
                    'tech_contact_no' => $row213['tech_contact'],
                    'tech_email_id' => $row213['tech_email_id'],
                    'cust_id' => $row213['cust_id'],
                    'customer_name' => $row213['customer_name'],
                    'contact_number' => $row213['cont_num'],
                    'alternate_number' => $row213['alternate_no'],
                    'door_no' => $row213['door_no'],
                    'address' => $row213['add'],
                    'city' => $row213['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row213['cust_town'],
                    'country' => $row213['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row213['call_type'],
                    'priority' => $row213['prior'],
                    'prob_description' => $row213['prob_desc'],
                    'image' => base_url().$row213['ticket_image'],
                    'audio' => base_url().$row213['audio'],
                    'video' => base_url().$row213['video'],
                    'tech_id' => $row213['tech_id'],
                    'type_cont' => $row213['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row213['contract_period'],
                    'product_name' => $row213['product_name'],
                    'cat_name' => $row213['cat_name'],
                    'quantity' => $row213['quantity'],
                    'lead_time' => $row213['lead_time'],
                    'schedule_next' => $row213['schedule_next'],
                    'status' => $row213['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row213['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row213['response_time'] . ' Hours',
                    'resolution_time' => $row213['resolution_time'] . ' Hours',
                    'companyid' => $row213['companyid']
                ));
                }
              else
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                $schedulenext = $row213['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data1, array(
                    'ticket_id' => $row213['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row213['amc_id'],
                    'current_tech_id' => $row213['tech_id'],
                    'current_tech_name' => $row213['tech_name'],
                    'tech_contact_no' => $row213['tech_contact'],
                    'tech_email_id' => $row213['tech_email_id'],
                    'cust_id' => $row213['cust_id'],
                    'customer_name' => $row213['customer_name'],
                    'contact_number' => $row213['cont_num'],
                    'alternate_number' => $row213['alternate_no'],
                    'door_no' => $row213['door_no'],
                    'address' => $row213['add'],
                    'city' => $row213['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row213['cust_town'],
                    'country' => $row213['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row213['call_type'],
                    'priority' => $row213['prior'],
                    'prob_description' => $row213['prob_desc'],
                    'image' => base_url().$row213['ticket_image'],
                    'audio' => base_url().$row213['audio'],
                    'video' => base_url().$row213['video'],
                    'tech_id' => $row213['tech_id'],
                    'type_cont' => $row213['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row213['contract_period'],
                    'product_name' => $row213['product_name'],
                    'cat_name' => $row213['cat_name'],
                    'quantity' => $row213['quantity'],
                    'lead_time' => $row213['lead_time'],
                    'schedule_next' => $schedule_next,
                    'status' => $row213['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row213['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row213['response_time'] . ' Hours',
                    'resolution_time' => $row213['resolution_time'] . ' Hours',
                    'companyid' => $row213['companyid']
                ));
                }
            }
        echo json_encode(array(
            "status" => 1,
            "tickets" => $data,
            "deleted" => $data1,
            "amc_payment" => $data2
        ));
        }


        function tickets_forthe_day($tech_id)
        {
        $data = array();
        $data1 = array();
        $data2 = array();
        $date = date("Y-m-d");
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
           // 'all_tickets.current_status' => 1,
            'all_tickets.amc_id' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_img, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        foreach($query as $row)
            {
            
            if($row['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row['assigned']);
            $last = new DateTime($row['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row['response_time'])
                {
                $address = $row['add'];
                $datetime1 = $row['last'];
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $schedulenext = $row['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));

                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url() .$row['ticket_img'],
                    'audio' => base_url() .$row['audio'],
                    'video' => base_url() .$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' =>$row['schedule_next'],
                    'status' => $row['current_status'],
                    'prev_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours',
                    'companyid' => $row['companyid']
                ));
                }
              else
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                $schedulenext = $row['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data1, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url() .$row['ticket_img'],
                    'audio' => base_url() .$row['audio'],
                    'video' => base_url() .$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' => $row['schedule_next'],
                    'status' => $row['current_status'],
                    'perv_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours',
                    'companyid' => $row['companyid']
                ));
                }
            }
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
           // 'all_tickets.current_status' => 1,
            'all_tickets.amc_id!=' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image, all_tickets.company_id as companyid');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.cust_preference_date', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query11 = $this->db->get();
        $query2 = $query11->result_array();
        foreach($query2 as $row2)
            {
            
            if($row2['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row2['assigned']);
            $last = new DateTime($row2['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row2['response_time'])
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $schedulenext = $row2['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));

                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data, array(
                'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url() .$row2['ticket_image'],
                    'audio' => base_url() .$row2['audio'],
                    'video' => base_url() .$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' =>$row2['schedule_next'],
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours',
                    'companyid' => $row2['companyid']
                ));
                }
              else
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                $schedulenext = $row2['schedule_next'];
                $schedule_next = date('Y-m-d',strtotime($schedulenext));
                array_push($data, array(
                    'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' => "",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' => "",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url() .$row2['ticket_image'],
                    'audio' => base_url() .$row2['audio'],
                    'video' => base_url() .$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' => $row2['schedule_next'],
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours',
                    'companyid' => $row2['companyid']

                ));
                }
            }

        echo json_encode(array(
            "status" => 1,
            "tickets" => $data,
            "deleted" => $data1,
            "amc_payment" => $data2
        ));
        }

        public function ongoing_tickets($tech_id)
        {
        $data = array();
        $data1 = array();
        $data2 = array();
        $date = date("Y-m-d");
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_img');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query1 = $this->db->get();
        $query = $query1->result_array();
        foreach($query as $row)
            {
            
            if($row['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row['assigned']);
            $last = new DateTime($row['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row['response_time'])
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url() .$row['ticket_img'],
                    'audio' => base_url() .$row['audio'],
                    'video' => base_url() .$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' => $row['schedule_next'],
                    'status' => $row['current_status'],
                    'prev_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row['add'];
                $datetime1 = $row['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row['latitud'];
                $longitude = $row['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row['cust_id'],
                    'customer_name' => $row['customer_name'],
                    'contact_number' => $row['cont_num'],
                    'alternate_number' => $row['alternate_number'],
                    'door_no' => $row['door_no'],
                    'address' => $row['add'],
                    'city' => $row['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row['cust_town'],
                    'country' => $row['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row['prio'],
                    'call_category' => $row['call_type'],
                    'call_tag' => $row['call'],
                    'prob_description' => $row['prob_desc'],
                    'image' => base_url() .$row['ticket_img'],
                    'audio' => base_url() .$row['audio'],
                    'video' => base_url() .$row['video'],
                    'tech_id' => $row['tech_id'],
                    'type_cont' => $row['call_type'],
                    'contract_period' => $row['contract_period'],
                    'product_name' => $row['product_name'],
                    'cat_name' => $row['cat_name'],
                    'quantity' => $row['quantity'],
                    'lead_time' => $row['lead_time'],
                    'schedule_next' => $row['schedule_next'],
                    'status' => $row['current_status'],
                    'perv_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row['response_time'] . ' Hours',
                    'resolution_time' => $row['resolution_time'] . ' Hours'
                ));
                }
            }
        $where4 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.current_status' => 1,
            'all_tickets.amc_id!=' => ''
        );
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where4);
        $this->db->like('all_tickets.assigned_time', $date);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query11 = $this->db->get();
        $query2 = $query11->result_array();
        foreach($query2 as $row2)
            {
            
            if($row2['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row2['assigned']);
            $last = new DateTime($row2['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row2['response_time'])
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data, array(
                'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url() .$row2['ticket_image'],
                    'audio' => base_url() .$row2['audio'],
                    'video' => base_url() .$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' => $row2['schedule_next'],
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row2['add'];
                $datetime1 = $row2['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row2['latitud'];
                $longitude = $row2['longitud'];
                array_push($data, array(
                    'ticket_id' => $row2['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row2['amc_id'],
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row2['cust_id'],
                    'customer_name' => $row2['customer_name'],
                    'contact_number' => $row2['cont_num'],
                    'alternate_number' => $row2['alternate_no'],
                    'door_no' => $row2['door_no'],
                    'address' => $row2['add'],
                    'city' => $row2['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row2['cust_town'],
                    'country' => $row2['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row2['call_type'],
                    'priority' => $row2['prior'],
                    'prob_description' => $row2['prob_desc'],
                    'image' => base_url() .$row2['ticket_image'],
                    'audio' => base_url() .$row2['audio'],
                    'video' => base_url() .$row2['video'],
                    'tech_id' => $row2['tech_id'],
                    'type_cont' => $row2['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row2['contract_period'],
                    'product_name' => $row2['product_name'],
                    'cat_name' => $row2['cat_name'],
                    'quantity' => $row2['quantity'],
                    'lead_time' => $row2['lead_time'],
                    'schedule_next' => $row2['schedule_next'],
                    'status' => $row2['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row2['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row2['response_time'] . ' Hours',
                    'resolution_time' => $row2['resolution_time'] . ' Hours'
                ));
                }
            }

    $where41 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.amc_id' => ''
        );
        $cur_status=["1","0","12","19","20"];
        $prev_status=["15"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where41);
        $this->db->group_start();
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->where_not_in('previous_status',$prev_status);
        $this->db->group_end();
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query1233 = $this->db->get();
        $query21 = $query1233->result_array();
        foreach($query21 as $row1)
            {
            $assigned = new DateTime($row1['assigned']);
            $last = new DateTime($row1['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row1['response_time'])
                {
                
            if($row1['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
                $address = $row1['add'];
                $datetime1 = $row1['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row1['latitud'];
                $longitude = $row1['longitud'];
                array_push($data, array(
                    'ticket_id' => $row1['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row1['cust_id'],
                    'customer_name' => $row1['customer_name'],
                    'contact_number' => $row1['cont_num'],
                    'alternate_number' => $row1['alternate_number'],
                    'door_no' => $row1['door_no'],
                    'address' => $row1['add'],
                    'city' => $row1['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row1['cust_town'],
                    'country' => $row1['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row1['prio'],
                    'call_category' => $row1['call_type'],
                    'call_tag' => $row1['call'],
                    'prob_description' => $row1['prob_desc'],
                    'image' => base_url() .$row1['ticket_image'],
                    'audio' => base_url() .$row1['audio'],
                    'video' => base_url() .$row1['video'],
                    'tech_id' => $row1['tech_id'],
                    'type_cont' => $row1['call_type'],
                    'contract_period' => $row1['contract_period'],
                    'product_name' => $row1['product_name'],
                    'cat_name' => $row1['cat_name'],
                    'quantity' => $row1['quantity'],
                    'lead_time' => $row1['lead_time'],
                    'schedule_next' => $row1['schedule_next'],
                    'status' => $row1['current_status'],
                    'prev_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row1['response_time'] . ' Hours',
                    'resolution_time' => $row1['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row1['add'];
                $datetime1 = $row1['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row1['latitud'];
                $longitude = $row1['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row1['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row1['cust_id'],
                    'customer_name' => $row1['customer_name'],
                    'contact_number' => $row1['cont_num'],
                    'alternate_number' => $row1['alternate_number'],
                    'door_no' => $row1['door_no'],
                    'address' => $row1['add'],
                    'city' => $row1['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row1['cust_town'],
                    'country' => $row1['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row1['prio'],
                    'call_category' => $row1['call_type'],
                    'call_tag' => $row1['call'],
                    'prob_description' => $row1['prob_desc'],
                    'image' => base_url() .$row1['ticket_image'],
                    'audio' => base_url() .$row1['audio'],
                    'video' => base_url() .$row1['video'],
                    'tech_id' => $row1['tech_id'],
                    'type_cont' => $row1['call_type'],
                    'contract_period' => $row1['contract_period'],
                    'product_name' => $row1['product_name'],
                    'cat_name' => $row1['cat_name'],
                    'quantity' => $row1['quantity'],
                    'lead_time' => $row1['lead_time'],
                    'schedule_next' => $row1['schedule_next'],
                    'status' => $row1['current_status'],
                    'perv_status' => "",
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row1['response_time'] . ' Hours',
                    'resolution_time' => $row1['resolution_time'] . ' Hours'
                ));
                }
            }
    $where41 = array(
            'all_tickets.tech_id' => $tech_id,
            'all_tickets.amc_id!=' => ''
        );
        $cur_status=["1","0","12","19","20"];
        $prev_status=["15"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where41);
        $this->db->group_start();
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->where_not_in('previous_status',$prev_status);
        $this->db->group_end();
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query12 = $this->db->get();
        $query21 = $query12->result_array();

        foreach($query21 as $row21)
            {
            $assigned = new DateTime($row21['assigned']);
            $last = new DateTime($row21['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row21['response_time'])
                {
                
            if($row21['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                array_push($data, array(
                    'ticket_id' => $row21['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row21['amc_id'],
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row21['cust_id'],
                    'customer_name' => $row21['customer_name'],
                    'contact_number' => $row21['cont_num'],
                    'alternate_number' => $row21['alternate_no'],
                    'door_no' => $row21['door_no'],
                    'address' => $row21['add'],
                    'city' => $row21['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $ro21['cust_town'],
                    'country' => $row21['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row21['call_type'],
                    'priority' => $row21['prior'],
                    'prob_description' => $row21['prob_desc'],
                    'image' => base_url() .$row21['ticket_image'],
                    'audio' => base_url() .$row21['audio'],
                    'video' => base_url() .$row21['video'],
                    'tech_id' => $row21['tech_id'],
                    'type_cont' => $row21['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row21['contract_period'],
                    'product_name' => $row21['product_name'],
                    'cat_name' => $row21['cat_name'],
                    'quantity' => $row21['quantity'],
                    'lead_time' => $row21['lead_time'],
                    'schedule_next' => $row21['schedule_next'],
                    'status' => $row21['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row21['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row21['response_time'] . ' Hours',
                    'resolution_time' => $row21['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row21['add'];
                $datetime1 = $row21['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row21['latitud'];
                $longitude = $row21['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row21['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row21['amc_id'],
                    'current_tech_id' =>"",
                    'current_tech_name' => "",
                    'tech_contact_no' => "",
                    'tech_email_id' =>"",
                    'cust_id' => $row21['cust_id'],
                    'customer_name' => $row21['customer_name'],
                    'contact_number' => $row21['cont_num'],
                    'alternate_number' => $row21['alternate_no'],
                    'door_no' => $row21['door_no'],
                    'address' => $row21['add'],
                    'city' => $row21['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row21['cust_town'],
                    'country' => $row21['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row21['call_type'],
                    'priority' => $row21['prior'],
                    'prob_description' => $row21['prob_desc'],
                    'image' => base_url() .$row21['ticket_image'],
                    'audio' => base_url() .$row21['audio'],
                    'video' => base_url() .$row21['video'],
                    'tech_id' => $row21['tech_id'],
                    'type_cont' => $row21['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row21['contract_period'],
                    'product_name' => $row21['product_name'],
                    'cat_name' => $row21['cat_name'],
                    'quantity' => $row21['quantity'],
                    'lead_time' => $row21['lead_time'],
                    'schedule_next' => $row21['schedule_next'],
                    'status' => $row21['current_status'],
                    'prev_status' => "",
                    'cust_preference_date' => $row21['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row21['response_time'] . ' Hours',
                    'resolution_time' => $row21['resolution_time'] . ' Hours'
                ));
                }
            }
            $where42 = array(
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id' => ''
        );
        $cur_status=["1","0","12","19","20"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prio,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where42);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query122 = $this->db->get();
        $query212 = $query122->result_array();
        foreach($query212 as $row12)
            {
            if($row12['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row12['assigned']);
            $last = new DateTime($row12['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row12['response_time'])
                {
                //    echo 'if'.$row12['ticket_id'].'   ';
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));
                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
                array_push($data, array(
                    'ticket_id' => $row12['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => $row12['tech_id'],
                    'current_tech_name' => $row12['tech_name'],
                    'tech_contact_no' => $row12['tech_contact'],
                    'tech_email_id' => $row12['tech_email_id'],
                    'cust_id' => $row12['cust_id'],
                    'customer_name' => $row12['customer_name'],
                    'contact_number' => $row12['cont_num'],
                    'alternate_number' => $row12['alternate_number'],
                    'door_no' => $row12['door_no'],
                    'address' => $row12['add'],
                    'city' => $row12['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row12['cust_town'],
                    'country' => $row12['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row12['prio'],
                    'call_category' => $row12['call_type'],
                    'call_tag' => $row12['call'],
                    'prob_description' => $row12['prob_desc'],
                    'image' => base_url() .$row12['ticket_image'],
                    'audio' => base_url() .$row12['audio'],
                    'video' => base_url() .$row12['video'],
                    'tech_id' => $row12['tech_id'],
                    'type_cont' => $row12['call_type'],
                    'contract_period' => $row12['contract_period'],
                    'product_name' => $row12['product_name'],
                    'cat_name' => $row12['cat_name'],
                    'quantity' => $row12['quantity'],
                    'lead_time' => $row12['lead_time'],
                    'schedule_next' => $row12['schedule_next'],
                    'status' => $row12['current_status'],
                    'prev_status' => $row12['previous_status'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row12['response_time'] . ' Hours',
                    'resolution_time' => $row12['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row12['add'];
                $datetime1 = $row12['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row12['latitud'];
                $longitude = $row12['longitud'];
           //     echo 'else'.$row12['ticket_id'].'   ';
                array_push($data1, array(
                    'ticket_id' => $row12['ticket_id'],
                    'ticket_type' => 'regular',
                    'amc_id' => "",
                    'current_tech_id' => $row12['tech_id'],
                    'current_tech_name' => $row12['tech_name'],
                    'tech_contact_no' => $row12['tech_contact'],
                    'tech_email_id' => $row12['tech_email_id'],
                    'cust_id' => $row12['cust_id'],
                    'customer_name' => $row12['customer_name'],
                    'contact_number' => $row12['cont_num'],
                    'alternate_number' => $row12['alternate_number'],
                    'door_no' => $row12['door_no'],
                    'address' => $row12['add'],
                    'city' => $row12['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row12['cust_town'],
                    'country' => $row12['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'priority' => $row12['prio'],
                    'call_category' => $row12['call_type'],
                    'call_tag' => $row12['call'],
                    'prob_description' => $row12['prob_desc'],
                    'image' => base_url() .$row12['ticket_image'],
                    'audio' => base_url() .$row12['audio'],
                    'video' => base_url() .$row12['video'],
                    'tech_id' => $row12['tech_id'],
                    'type_cont' => $row12['call_type'],
                    'contract_period' => $row12['contract_period'],
                    'product_name' => $row12['product_name'],
                    'cat_name' => $row12['cat_name'],
                    'quantity' => $row12['quantity'],
                    'lead_time' => $row12['lead_time'],
                    'schedule_next' => $row12['schedule_next'],
                    'status' => $row12['current_status'],
                    'prev_status' => $row12['previous_status'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row12['response_time'] . ' Hours',
                    'resolution_time' => $row12['resolution_time'] . ' Hours'
                ));
                }
            }
    $where413 = array(
            
            'all_tickets.prev_tech_id' => $tech_id,
            'all_tickets.previous_status' => 15,
            'all_tickets.amc_id!=' => ''
        );
        $cur_status=["1","0","12","19","20"];
        $this->db->select('*,technician.first_name as tech_name,customer.contact_number as cont_num,all_tickets.town as town_tkt,technician.email_id as tech_email_id,technician.contact_number as tech_contact,all_tickets.last_update as last,all_tickets.priority as prior,all_tickets.assigned_time as assigned,all_tickets.latitude as latitud,all_tickets.longitude as longitud,all_tickets.address as add, all_tickets.image as ticket_image');
        $this->db->from('all_tickets');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
        $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        $this->db->where($where413);
        $this->db->where_not_in('current_status',$cur_status);
        $this->db->group_by('all_tickets.ticket_id,customer.customer_id');
        $this->db->order_by('all_tickets.last_update','desc');
        $query123 = $this->db->get();
        $query213 = $query123->result_array();
        foreach($query213 as $row213)
            {
            if($row213['previous_status']=="15")
            {
            $prev=15;
            }
            else
            {
            $prev="";
            }
            $assigned = new DateTime($row213['assigned']);
            $last = new DateTime($row213['last']);
            $interval = $assigned->diff($last);
            $elapsed = $interval->format('%y-%m-%a-%h-%i-%S');
            if ($elapsed <= $row213['response_time'])
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                array_push($data, array(
                'ticket_id' => $row213['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row213['amc_id'],
                    'current_tech_id' => $row213['tech_id'],
                    'current_tech_name' => $row213['tech_name'],
                    'tech_contact_no' => $row213['tech_contact'],
                    'tech_email_id' => $row213['tech_email_id'],
                    'cust_id' => $row213['cust_id'],
                    'customer_name' => $row213['customer_name'],
                    'contact_number' => $row213['cont_num'],
                    'alternate_number' => $row213['alternate_no'],
                    'door_no' => $row213['door_no'],
                    'address' => $row213['add'],
                    'city' => $row213['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row213['cust_town'],
                    'country' => $row213['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row213['call_type'],
                    'priority' => $row213['prior'],
                    'prob_description' => $row213['prob_desc'],
                    'image' => base_url() .$row213['ticket_image'],
                    'audio' => base_url() .$row213['audio'],
                    'video' => base_url() .$row213['video'],
                    'tech_id' => $row213['tech_id'],
                    'type_cont' => $row213['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row213['contract_period'],
                    'product_name' => $row213['product_name'],
                    'cat_name' => $row213['cat_name'],
                    'quantity' => $row213['quantity'],
                    'lead_time' => $row213['lead_time'],
                    'schedule_next' => $row213['schedule_next'],
                    'status' => $row213['current_status'],
                    'prev_status' => $row213['previous_status'],
                    'cust_preference_date' => $row213['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row213['response_time'] . ' Hours',
                    'resolution_time' => $row213['resolution_time'] . ' Hours'
                ));
                }
              else
                {
                $address = $row213['add'];
                $datetime1 = $row213['last'];;
                $date1 = date('Y-m-d', strtotime($datetime1));
                $time1 = date('H:i:s', strtotime($datetime1));

                $latitude = $row213['latitud'];
                $longitude = $row213['longitud'];
                array_push($data1, array(
                    'ticket_id' => $row213['ticket_id'],
                    'ticket_type' => 'amc',
                    'amc_id' => $row213['amc_id'],
                    'current_tech_id' => $row213['tech_id'],
                    'current_tech_name' => $row213['tech_name'],
                    'tech_contact_no' => $row213['tech_contact'],
                    'tech_email_id' => $row213['tech_email'],
                    'cust_id' => $row213['cust_id'],
                    'customer_name' => $row213['customer_name'],
                    'contact_number' => $row213['cont_num'],
                    'alternate_number' => $row213['alternate_no'],
                    'door_no' => $row213['door_no'],
                    'address' => $row213['add'],
                    'city' => $row213['town_tkt'],
                    /*'location' => $row['town_tkt'], changed by muthu because of location issue*/
                    'location' => $row213['cust_town'],
                    'country' => $row213['country'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'call_category' => $row213['call_type'],
                    'priority' => $row213['prior'],
                    'prob_description' => $row213['prob_desc'],
                    'image' => base_url() .$row213['ticket_image'],
                    'audio' => base_url() .$row213['audio'],
                    'video' => base_url() .$row213['video'],
                    'tech_id' => $row213['tech_id'],
                    'type_cont' => $row213['call_type'],
                    'call_tag' => "",
                    'contract_period' => $row213['contract_period'],
                    'product_name' => $row213['product_name'],
                    'cat_name' => $row213['cat_name'],
                    'quantity' => $row213['quantity'],
                    'lead_time' => $row213['lead_time'],

                    'schedule_next' => $row213['schedule_next'],
                    'status' => $row213['current_status'],
                    'prev_status' => $row213['previous_status'],
                    'cust_preference_date' => $row213['cust_preference_date'],
                    'created_date' => $date1,
                    'created_time' => $time1,
                    'response_time' => $row213['response_time'] . ' Hours',
                    'resolution_time' => $row213['resolution_time'] . ' Hours'
                ));
                }
            }
        echo json_encode(array(
            "status" => 1,
            "tickets" => $data,
            "deleted" => $data1,
            "amc_payment" => $data2
        ));
        }

    public

    function personal_spare_update($tech_id, $id, $company_id)
        {
        $data = array(
            "status" => 2
        );
        $this->db->where("tech_id", $tech_id);
        $this->db->where("id", $id);
        $this->db->where("company_id", $company_id);
        $this->db->update("personal_spare", $data);
        return true;
        }

    public

    function onhand($tech_id, $company_id)
        {
        $data = array();
        $this->db->select('*');
        $this->db->from('personal_spare');
        $this->db->where('tech_id', $tech_id);
        $this->db->where('company_id', $company_id);
        $this->db->where('status', 2);
        $query = $this->db->get();
        $result = $query->result_array();
            $a=0;
        foreach($result as $res)
            {
            $spare_array = json_decode($res['spare_array'], true);
            foreach($spare_array as $sp)
                {
                $a=$a+1;
                $this->db->select('spare_name,image');
                $this->db->from('spare');
                $this->db->where('spare_code', $sp['Spare_code']);
                $this->db->where('company_id', $company_id);
                $this->db->group_by('spare_code');
                $query1 = $this->db->get();
                $resu = $query1->row_array();
                $spare_name = $resu['spare_name'];
                if ($res['status'] == 2)
                    {
                    array_push($data, array(
                        "id" => $a,
                        "spare_code" => $sp['Spare_code'],
                        "spare_name" => $spare_name,
                        "spare_image" => $resu['image'],
                        "quantity" => $sp['quantity'],
                        "status" => "Received"
                    ));
                    }
                  else
                    {
                    array_push($data, array(
                        "id" =>$a,
                        "spare_code" => $sp['Spare_code'],
                        "spare_name" => $spare_name,
                        "spare_image" => $resu['image'],
                        "quantity" => $sp['quantity'],
                        "status" => "Arrived"
                    ));
                    }
                    $a++;
                }
            }

        return $data;
        }

    public

    function requested_personal($tech_id, $company_id)
        {
        $data = array();
        $this->db->select('*');
        $this->db->from('personal_spare');
        $this->db->where('tech_id', $tech_id);
        $this->db->where('company_id', $company_id);
    //  $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $res)
            {
            $spare_array = json_decode($res['spare_array'], true);
            foreach($spare_array as $sp)
                {
                $this->db->select('spare_name');
                $this->db->from('spare');
                $this->db->where('spare_code', $sp['Spare_code']);
                $this->db->where('company_id', $company_id);
                $this->db->group_by('spare_code');
                $query1 = $this->db->get();
                $resu = $query1->row_array();
                $spare_name = $resu['spare_name'];
                if ($res['status'] == 1)
                    {
                    array_push($data, array(
                        "spare_code" => $sp['Spare_code'],
                        "spare_name" => $spare_name,
                        "quantity" => $sp['quantity'],
                        "status" => "Arrived"
                    ));
                    }
                  else
                    {
                    array_push($data, array(
                        "spare_code" => $sp['Spare_code'],
                        "spare_name" => $spare_name,
                        "quantity" => $sp['quantity'],
                        "status" => "Requested"
                    ));
                    }
                }
            }

        return $data;
        }

    public

    function spare_array_tkt($tech_id, $company_id)
        {
        $data = array();
        $spare_array = array();
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where('tech_id', $tech_id);
        $this->db->where('company_id', $company_id);
        $this->db->where('requested_spare!=', '');
        $query = $this->db->get();
        $result = $query->result_array();
        if (count($result) != 0)
            {
            foreach($result as $res)
                {
                $spare_array = json_decode($res['spare_array'], true);
                if (count($spare_array) != 0)
                    {
                    foreach($spare_array as $sp)
                        {
                        $this->db->select('spare_name');
                        $this->db->from('spare');
                        $this->db->where('spare_code', $sp['Spare_code']);
                        $this->db->where('company_id', $company_id);
                        $this->db->group_by('spare_code');
                        $query1 = $this->db->get();
                        $resu = $query1->row_array();
                        $spare_name = $resu['spare_name'];
                        if ($sp['quantity'] == 0)
                            {
                            array_push($data, array(
                                "ticket_id" => $res['ticket_id'],
                                "spare_code" => $sp['Spare_code'],
                                "spare_name" => $spare_name,
                                "status" => "Arrived"
                            ));
                            }
                          else
                            {
                            array_push($data, array(
                                "ticket_id" => $res['ticket_id'],
                                "spare_code" => $sp['Spare_code'],
                                "spare_name" => $spare_name,
                                "status" => "Requested"
                            ));
                            }
                        }
                    }
                }
            }

        return $data;
        }

    public

    function submitted_claims($tech_id, $company_id)
        {
        $data = array();
        $status =["17","13"];
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->where('technician_id', $tech_id);
        $this->db->where('company_id', $company_id);
        //$this->db->where('status', 17);
        $this->db->where_in('status',$status);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $res)
            {
            $where = array(
                "reimbursement.company_id" => $res['company_id'],
                "reimbursement.technician_id" => $res['technician_id'],
                "reimbursement.start_time>=" => $res['start_date'],
                "reimbursement.end_time<=" => $res['end_date']
            );
            $this->db->select('travelling_charges');
            $this->db->from('reimbursement');
            $this->db->where($where);
            $sql = $this->db->get();
            $res1 = $sql->result_array();
            $count = $sql->num_rows();
            $charge = 0;
            for ($i = 0; $i < $count; $i++)
                {
                $charge+= $res1[$i]['travelling_charges'];
                }

            if ($res['status'] == "17")
                {
                array_push($data, array(
                    "start_date" => $res['start_date'],
                    "end_date" => $res['end_date'],
                    "comment" => $res['comment'],
                    "total_amount" => $charge,
                    "status" => "Requested"
                ));
                }
              else
                {
                if ($res['action'] == 1)
                    {
                    array_push($data, array(
                        "start_date" => $res['start_date'],
                        "end_date" => $res['end_date'],
                        "comment" => $res['comment'],
                        "total_amount" => $charge,
                        "status" => "Approved"
                    ));
                    }
                  else
                    {
                    array_push($data, array(
                        "start_date" => $res['start_date'],
                        "end_date" => $res['end_date'],
                        "comment" => $res['comment'],
                        "total_amount" => $charge,
                        "status" => "Rejected"
                    ));
                    }
                }
            }

        return $data;
        }
public function storeratingkey($ticketid,$companyid,$ratingkey)
        {
            $where1 = array(
                "all_tickets.ticket_id" => $ticketid,
                "all_tickets.company_id" => $companyid
                );
$where=array("ticket_id" => $ticketid,
                "company_id" => $companyid
                );
            $dat=array("rating_key"=>$ratingkey);
        $this->db->where($where);
        $this->db->update('all_tickets', $dat);


        $this->db->select('customer.email_id,customer.contact_number');
        $this->db->from('customer');
        $this->db->join('all_tickets', 'all_tickets.cust_id=customer.customer_id');
        $this->db->where($where1);
        $this->db->where('customer.company_id',$companyid);
        $query = $this->db->get();
        $resul = $query->row_array();
        //$emailid = $resul['customer.email_id'];
        return $resul;

        
        }
        public function getfeedbackcustomerdata($token)
        {

         $this->db->select('customer.email_id,all_tickets.ticket_id,all_tickets.id,all_tickets.current_status');
        $this->db->from('customer');
        $this->db->join('all_tickets', 'all_tickets.cust_id=customer.customer_id');
        $this->db->where('all_tickets.rating_key',$token);
        $query = $this->db->get();
        $resul = $query->row_array();
        return $resul;
        }
         public function gettechdetails($techid)
        {
        
        $this->db->select('area,region');
        $this->db->from('technician');
        $this->db->where('technician_id',$techid);
        $query = $this->db->get();
        $resul = $query->row_array();
      
        return $resul; 
        }



		public function ForgotPassword_cus($email)
		{
			   $this->db->select('username');
			   $this->db->from('login'); 
			   $this->db->where('username', $email); 
			   $query=$this->db->get();
			   return $query->row_array();
		}

		public function sendpassword($data)
		{       $this->load->library('encrypt');
				$email = $data['username'];
				$query1=$this->db->query("SELECT * from login where username = '".$email."' ");
				$row=$query1->result_array();
				if ($query1->num_rows()>0)
			  
		{
				$passwordplain = "";
				$passwordplain  = rand(999999999,9999999999);
				$newpass['password'] = $this->encrypt->encode($passwordplain);
				$this->db->where('username', $email);
				$this->db->update('login', $newpass); 
				$mail_message='Dear '.$row[0]['user_type'].','. "\r\n";
				$mail_message.='Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$passwordplain.'</b>'."\r\n";
				$mail_message.='<br>Please Update your password.';
				$mail_message.='<br>Thanks & Regards';
				$mail_message.='<br>FieldPro Team';        
				$config   = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'kaspondevelopers@gmail.com',
					'smtp_pass' => 'Kaspon@123',
					'mailtype' => 'html',
					'charset' => 'iso-8859-1'
				);
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('kaspondevelopers@gmail.com', 'Fieldpro');
				$this->email->to($email);
				$this->email->subject('FieldPro Forgot Password');
				$this->email->message($mail_message);
				$this->email->set_newline("\r\n");
				//$this->email->send();
		if ($this->email->send()) {
		     // $this->session->set_flashdata('msg','Password sent to your email!');
             $json1 = '';
             $json1 = array(
               "status" => 1,
               "msg" => "Password sent to your email!"
           );
			
		} else {
         
        $json1 = '';
			$json1 = array(
				"status" => 0,
				"msg" => "Failed to send password, please try again!"
			);
		}
		echo json_encode($json1);       
		}
		else
		{   $json2 = '';
			$json2 = array(
				"status" => 0,
				"msg" => "Email not found, try again!"
			);
			echo json_encode($json2);  
		}
        }
        
        public function checkOldPass($email,$oldpass){
			$this->load->library('encrypt');
			$where = array(
				'username' => $email
			);
			$this->db->select('*');
			$this->db->from('login');
			$this->db->where($where);
			$query    = $this->db->get();
		    $rowcount = $query->row_array();
			$pass1=$rowcount['password'];
			$pass2= $this->encrypt->decode($pass1);
            if($oldpass == $pass2){
				return 1;
			}
			else{
				return 0;
			}
		}


		public function save_new_pass($email,$newpass)
		{  
			$this->load->library('encrypt');
			$data = array(
				'password' => $this->encrypt->encode($newpass)
				);
			$this->db->where('username', $email);
            $this->db->update('login', $data);
            
			return 1;
        }
        
        public function reschedule_tickets($tech_id,$company_id,$ticket_id,$new_date)

        {   
            //$cust_pre = date("Y-m-d H:i:s",strtotime($new_date))
            $data=array("lead_time"=>$new_date,"cust_preference_date"=>$new_date,"schedule_next"=>$new_date);
            $where = array(
                'tech_id=' => $tech_id,
                'company_id' => $company_id,
                'ticket_id'=> $ticket_id
            );
            $this->db->where($where);
            $this->db->update('all_tickets', $data);
           
            return ($this->db->affected_rows() > 0) ? 1 : 0; 
        }

        public function imprest_spare_track($tech_id, $ticket_id, $spare_array, $company_id,$spare_cost)
            {
            $data = array(
                'tech_id' => $tech_id,
                'ticket_id' => $ticket_id,
                'spare_array' => $spare_array,
                'company_id' => $company_id,
                'spare_cost' => $spare_cost
            );
            $this->db->insert('impress_consumed_spare', $data);
            return true;
            }

            public function spare_transfer_list ($tech_id, $company_id)
            {
            $where = array(
                'from_tech_id' => $tech_id,
                'company_id' => $company_id
            );
            $this->db->select('*,(select technician.first_name from technician WHERE technician.technician_id=spare_transfer.from_tech_id limit 1) as from_tech_name, (select technician.first_name from technician WHERE technician.technician_id=spare_transfer.to_tech_id limit 1) as to_tech_name');
            $this->db->from('spare_transfer');
            $this->db->where($where);
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
            }

            public function spare_received_list ($tech_id, $company_id)
            {
            $where = array(
                'to_tech_id' => $tech_id,
                'company_id' => $company_id
            );
            $this->db->select('*, (select technician.first_name from technician WHERE technician.technician_id=spare_transfer.to_tech_id limit 1) as to_tech_name, (select technician.first_name from technician WHERE technician.technician_id=spare_transfer.from_tech_id limit 1) as from_tech_name');
            $this->db->from('spare_transfer');
            $this->db->where($where);
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
            }

           public function accept_transfered_spare($spare_id,$tech_id, $to_tech_id,$company_id)
            {
                $where = array(
                    'id' => $spare_id,
                    'from_tech_id' => $tech_id,
                    'to_tech_id' => $to_tech_id,
                    'company_id' => $company_id
                   
                );
                $data = array(
                    'status'=> 3
                );
            $this->db->where($where);
            $this->db->update('spare_transfer', $data);
            if ($this->db->affected_rows() > 0)
            {
                return true;
            }
            else{
                return 0;
            }
            }

            public function accept_received_spare($spare_id,$tech_id, $from_tech_id,$company_id)
            {
                $where = array(
                    'id' => $spare_id,
                    'from_tech_id' => $from_tech_id,
                    'to_tech_id' => $tech_id,
                    'company_id' => $company_id
                   
                );
                $data = array(
                    'status'=> 4
                );
            $this->db->where($where);
            $this->db->update('spare_transfer', $data);
            if ($this->db->affected_rows() > 0)
            {
                return true;
            }
            else{
                return 0;
            }
            
            }

            public function profile_image_upload($company_id,$technician_id, $img_dir1)
            {

                $where = array(
                    'technician_id' => $technician_id,
                    'company_id' => $company_id                 
                );
                $data = array('image'=> $img_dir1);

            $this->db->where($where);
            $this->db->update('technician', $data);
            return true;          

            }

            public function pending_frm ($tech_id, $company_id)
            {
            $where = array(
                'billing.tech_id' => $tech_id,
                'billing.company_id' => $company_id,
                'billing.frm_status'=> "PENDING"
            );
            $this->db->select('ticket_id, drop_spare, frm_status,');
            $this->db->from('billing');
            $this->db->where($where);
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->group_by('billing.ticket_id');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
            }


            public function load_location($company_id, $region, $area)
            {  
                if ($region == '' && $area == '') {
                    $where = array(
                        'company_id' => $company_id
                    );
                } else if ($region == '' && $area != '') {
                    $where = array(
                        'company_id' => $company_id,
                        //'town' => $area
                    );
                } else if ($region != '' && $area == '') {
                    $where = array(
                        'company_id' => $company_id,
                        'region' => $region
                    );
                } else {
                    $where = array(
                        'company_id' => $company_id,
                        'region' => $region,
                        //'town' => $area
                    );
                }
                
                $this->db->distinct();
                $this->db->select('location');
                $this->db->from('all_tickets');
                $this->db->where($where);
                $query  = $this->db->get();
                $result = $query->result_array();
              
                return $result;

            }

            public function travel_update_list($company_id,$tech_id)
            {  
                $where   = array(
                    "company_id" => $company_id,
                    "technician_id" => $tech_id,
                    "status" => 5         
                );

                $json    = array();
                $this->db->select('ticket_id,no_of_km_travelled,estimated_time,start_time,end_time,travelling_charges,view');
                $this->db->from('reimbursement');
                // $this->db->join("all_tickets", "all_tickets.ticket_id=reimbursement.ticket_id");
                $this->db->where($where);
                $query  = $this->db->get();
                $result = $query->result_array();
                foreach ($result as $row) {
                    array_push($json, array(           
                        "ticket_id" => $row['ticket_id'],
                        "technician_id" => $row['technician_id'],
                        "mode_of_travel" => $row['mode_of_travel'],
                        "no_of_km_travelled" => $row['no_of_km_travelled'],
                        "travelling_charges" => $row['travelling_charges'],
                        "view" => $row['view'],
                        "estimated_time"=> $row['estimated_time'],
                        "start_time"=> $row['start_time'],
                        "end_time"=> $row['end_time']
                    ));
                }

                return $result;
            }


            

    }
