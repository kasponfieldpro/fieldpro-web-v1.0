<?php
header('Content-type: text/html; charset=utf-8');
date_default_timezone_set('Asia/Kolkata');
class reimbursement extends CI_Model
{
    
    function __construct()
    {
        // Call the Model constructor  
        parent::__construct();
    }
    //we will use the select function 
	public function load_notify($company_id)
    {
        $where = array(
            'company_id' => $company_id,
            'role' => 'Manager'
        );
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return $data;
    }
	public function onload_prev_tech($tech_id)
    {
        $where = array(
            'technician_id' => $tech_id,
        );
        $this->db->select('employee_id');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->row_array();
        return $data['employee_id'];
    }
	  public function Spare_transfer($company_id)
    {
        $json = array();
        $company_id = $company_id;
        $where      = array(
            'spare_transfer.company_id' => $company_id,
            'spare_transfer.status' => 0
        );
        $this->db->select('*,spare_transfer.id as p_id, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.from_tech_id limit 1) as from_tech, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.to_tech_id limit 1) as to_tech');
        $this->db->from('spare_transfer');
        $this->db->join("technician", "spare_transfer.from_tech_id=technician.technician_id");
		
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $this->db->group_by('spare_transfer.id');
        $query = $this->db->get();
	
        $data  = $query->result_array();
		
        return ($data);
    }
	  public function to_tech_id($to_tech_id)
    {
        $json = array();
        $to_tech_id = $to_tech_id;
        $where      = array(
            'spare_transfer.to_tech_id' => $to_tech_id,
           
        );
        $this->db->select('*');
        $this->db->from('technician');
       $this->db->join("spare_transfer", "spare_transfer.to_tech_id=technician.technician_id");
		          $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $this->db->group_by('technician.technician_id');
        $query = $this->db->get();
	
        $data  = $query->result_array();
		
        return ($data);
    }
	  public function Spare_transfer_accept($company_id)
    {
        $json = array();
        $company_id = $company_id;
        $where      = array(
            'spare_transfer.company_id' => $company_id,
            'spare_transfer.status' => 1
        );
        $this->db->select('*,spare_transfer.id as p_id, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.from_tech_id limit 1) as from_tech, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.to_tech_id limit 1) as to_tech');
        $this->db->from('spare_transfer');
        $this->db->join("technician", "spare_transfer.from_tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }
    
	  public function Spare_transfer_reject($company_id)
    {
        $json = array();
        $company_id = $company_id;
        $where      = array(
            'spare_transfer.company_id' => $company_id,
            'spare_transfer.status' => 2
        );
        $this->db->select('*,spare_transfer.id as p_id, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.from_tech_id limit 1) as from_tech, (select technician.employee_id from technician WHERE technician.technician_id=spare_transfer.to_tech_id limit 1) as to_tech');
        $this->db->from('spare_transfer');
        $this->db->join("technician", "spare_transfer.from_tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }


	public function renewal_date($company_id)
    {
		$disweek=date("Y-m-d",strtotime("+5 days"));
        $where = array(
            'company_id' => $company_id,'renewal_date<='=>$disweek
        );
        $this->db->select('renewal_date');
        $this->db->from('admin_user');
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->result_array();
		$json=array();
        foreach($query as $q)
		{
			$ex='Renewal date is getting nearer ('.$q['renewal_date'].')';
			array_push($json,array('data'=>$ex));
		}
		return $json;
    }
    public function spare_expired($company_id)
    {
		$disweek=date("Y-m-d",strtotime("+5 days"));
        $where = array(
            'company_id' => $company_id,'ep_date<='=>$disweek
        );
        $this->db->select('spare_code,spare_name,spare_source,ep_date');
        $this->db->from('spare');
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->result_array();
		$json=array();
        foreach($query as $q)
		{
			$ex='Expiry date of '.$q['spare_code'] .' is '.$q['ep_date'].' which is placed in '.$q['spare_source'];
			array_push($json,array('data'=>$ex));
		}
		return $json;
    }
	public function spare_quan($company_id)
    {
        $where = array(
            'company_id' => $company_id,'quantity_purchased<='=>5
        );
        $this->db->select('spare_code,spare_name,spare_source,ep_date');
        $this->db->from('spare');
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->result_array();
		$json=array();
        foreach($query as $q)
		{
			$ex='Only lesser value of spare  '.$q['spare_code'] .' is available ';
			array_push($json,array('data'=>$ex));
		}
		return $json;
    }
    public function custrat($company_id)
    {
        $res       = array();
        $sub_where = array(
            'all_tickets.company_id' => $company_id,
            'current_status' => 12
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($sub_where);
        $query2 = $this->db->get();
        $total  = $query2->num_rows();
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where('company_id', $company_id);
        $query22         = $this->db->get();
        $target_cust     = $query22->result_array();
        $target_cust_rat = 0;
        $target_first    = 0;
        foreach ($target_cust as $t) {
            $target_cust_rat = $t['customer_rating'];
        }
        
        $target_cust_rat = $target_cust_rat;
        $sub_where2      = array(
            "cust_rating!=" => 0,
            'all_tickets.company_id' => $company_id,
            'current_status' => 12
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($sub_where2);
        $query3      = $this->db->get();
        $cust_rating = $query3->result_array();
        $cust_rat    = 0;
        foreach ($cust_rating as $c) {
            $cust_rat = $cust_rat + $c['cust_rating'];
        }
        $cust_rating = $cust_rat / $total;
        $cust_rating = $cust_rating / 5;
        if ($target_cust_rat <= $cust_rating) {
            $this->db->from('score_reward');
            $this->db->where('company_id', $company_id);
            $query22_r       = $this->db->get();
            $target_cust_r   = $query22_r->result_array();
            $target_cust_rat = $target_cust_rat;
            $target_first    = 0;
            foreach ($target_cust_r as $t_r) {
                $cust_rating = $t_r['customer_rating'];
            }
        } else {
            $cust_rating = 0;
        }
        array_push($res, array(
            'label' => 'Customer Rating',
            'value' => number_format((float) $cust_rating, 0, '.', ''),
            'value1' => $target_cust_rat
        ));
        return $res;
    }
    public function custfeed($company_id)
    {
        $res       = array();
        $sub_where = array(
            'all_tickets.company_id' => $company_id,
            'current_status' => 12
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($sub_where);
        $query2     = $this->db->get();
        $total      = $query2->num_rows();
        $sub_where1 = array(
            "cust_feedback!=" => '',
            'all_tickets.company_id' => $company_id,
            'current_status' => 12
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($sub_where1);
        $query2        = $this->db->get();
        $cust_feedback = $query2->num_rows();
        //   $cust_feedback = $cust_feedback/$total;
        
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where('company_id', $company_id);
        $query22_r       = $this->db->get();
        $target_cust_r   = $query22_r->result_array();
        $target_cust_rat = 0;
        $target_first    = 0;
        foreach ($target_cust_r as $t_r) {
            $this->db->select('*');
            $this->db->from('score_reward');
            $this->db->where('company_id', $company_id);
            $query22         = $this->db->get();
            $target_cust     = $query22->result_array();
            $target_cust_rat = 0;
            $target_first    = 0;
            
            foreach ($target_cust as $t) {
                $target_cust_rat = $t['cust_feed'];
            }
            if ($cust_feedback >= $t_r['cust_feed']) {
                $cust_feedback = ($cust_feedback * $t['cust_feed']) / $total;
                
            }
        }
        
        //$target_cust_rat=$target_cust_rat/$query22->num_rows();
        array_push($res, array(
            'label' => 'Customer Feedback',
            'value' => number_format((float) $cust_feedback, 0, '.', ''),
            'value1' => $target_cust_rat
        ));
        return $res;
    }
    public function sla_complaince($company_id)
    {
        $res       = array();
        $ar        = array();
        $ar1       = array();
        $ar2       = array();
        $sub_where = array(
            "all_tickets.current_status" => 12,
            'all_tickets.company_id' => $company_id
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($sub_where);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $overall_tickets = $query->num_rows();
            $ar1             = array();
            $ar2             = array();
            foreach ($result as $row) {
                $tst      = $row['ticket_start_time'];
                $tet      = $row['ticket_end_time'];
                $dteEnd   = new DateTime($tet);
                $dteStart = new DateTime($tst);
                $dteDiff  = $dteStart->diff($dteEnd);
                $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                $dteStart = strtotime($tst);
                $dteDiff  = strtotime($tet);
                $interval = abs($dteDiff - $dteStart);
                $minutes  = round($interval / 60);
                $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                array_push($ar, array(
                    "diff" => $value
                ));
                $product = $row['product_id'];
                $cat_id  = $row['cat_id'];
                
                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('product', 'all');
                $this->db->group_end();
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $product = 'all';
                } else {
                    $product = $row['product_id'];
                }
                $this->db->select('*');
                $this->db->from('sla_combination');
                $this->db->where('company_id', $company_id);
                $this->db->group_start();
                $this->db->like('category', 'all');
                $this->db->group_end();
                $query1  = $this->db->get();
                $result1 = $query1->result_array();
                if (!empty($result1)) {
                    $category = 'all';
                } else {
                    $category = $row['cat_id'];
                }
                
                $where8 = array(
                    'company_id' => $company_id,
                    'product' => $product,
                    'category' => $category
                );
                $this->db->select('ref_id');
                $this->db->from('sla_combination');
                $this->db->where($where8);
                $query5  = $this->db->get();
                $result5 = $query5->result_array();
                if (!empty($result5)) {
                    $ref_id = $result5[0]['ref_id'];
                    $this->db->select('resolution_time');
                    $this->db->from('sla_mapping');
                    $this->db->where('ref_id', $ref_id);
                    $query6  = $this->db->get();
                    $result6 = $query6->result_array();
                    
                    if (!empty($result6)) {
                        $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                        if ($value <= $resolution) {
                            array_push($ar1, array(
                                $resolution
                            ));
                        } else {
                            array_push($ar2, array(
                                $resolution
                            ));
                        }
                    }
                    
                    $sla_complaince = count($ar1);
                } else {
                    $sla_complaince = 0;
                }
            }
            
            $where1 = array(
                "all_tickets.current_status" => 12,
                'all_tickets.company_id' => $company_id
            );
            $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
            $this->db->from('all_tickets');
            $this->db->where($where1);
            $query = $this->db->get();
            $res6  = $query->num_rows();
            if ($res6 != 0) {
                $answer  = 0;
                $percent = 0;
                $answer  = $sla_complaince / $res6;
                $percent = $answer * 100;
            }
        } else {
            $percent = 0;
        }
        $this->db->select('SLA_Compliance_Target');
        $this->db->from('sla_mapping');
        $this->db->where('company_id', $company_id);
        $query133   = $this->db->get();
        $res61      = $query133->result_array();
        $target_sla = 0;
        foreach ($res61 as $r) {
            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
        }
		if($query133->num_rows()!=0)
		{
        $target_sla = $target_sla / $query133->num_rows();
		}
		else
		{
			$target_sla =0;
		}
        array_push($res, array(
            'percent' =>  number_format((float) $percent, 0, '.', ''),
            'target_sla' => $target_sla
        ));
        return $res;
    }
     public function disply_employee($company_id)
    {
        
        //$this->db->group_by('ticket_id');
		$final=array();
		
		$i=0;
        $where = array(
            "reimbursement_request.action" => 0,
            "reimbursement_request.status" => 17,
            "reimbursement_request.company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
        $this->db->join("product_management", "technician.product=product_management.product_id");
        $this->db->join("category_details", "technician.category=category_details.cat_id");
        $this->db->where($where);
        $this->db->order_by('reimbursement_request.id','desc');
        $this->db->group_by(array(
            "reimbursement_request.start_date",
            "reimbursement_request.end_date",
            "reimbursement_request.technician_id"
        ));
        $query = $this->db->get();
	    $data  = $query->result_array();
		$total=$query->num_rows();
		
		foreach($data as $row){
			$where   = array(
				"reimbursement.company_id" =>$row['company_id'],
				"reimbursement.technician_id" =>$row['technician_id'],
				"reimbursement.start_time>=" =>$row['start_date'],
				"reimbursement.end_time<=" =>$row['end_date']
			);
		$this->db->select('travelling_charges');
        $this->db->from('reimbursement');
       	$this->db->where($where);
		$sql=$this->db->get();
		$res = $sql->result_array();
		$count= $sql->num_rows();
			$charge=0;
			for($i=0;$i<$count;$i++)
			{
				$charge+=$res[$i]['travelling_charges'];
			}
			   array_push($final,array('technician_id'=>$row['technician_id'],'employee_id'=>$row['employee_id'],'first_name'=>$row['first_name'],'skill_level'=>$row['skill_level'],'contact_number'=>$row['contact_number'],'location'=>$row['location'],'product_name'=>$row['product_name'],'cat_name'=>$row['cat_name'],'start_date'=>$row['start_date'],'end_date'=>$row['end_date'],'total_charge'=>$charge));
			 /*$data= array_merge($data,array(
				'total_charge'=>$charge
				)
	   		 );*/
		}
      
		//print_r($final);
		//exit;
        return $final;
    }
	
    public function disply_frminventory($datas)
    {
        $company_id = $datas;
        $today      = date("Y-m-d H:i:s");
        $today1     = date("Y-m-d 00:00:00");
        $where      = array(
            'billing.company_id' => $company_id,
            'billing.frm' => 1,
            'billing.drop_spare!=' => '',
           // 'billing.last_update>=' => $today1,
           // 'billing.last_update<=' => $today
        );
        //$this->db->distinct();
        $this->db->select('*');
        $this->db->from('billing');
        $this->db->join('technician','technician.technician_id=billing.tech_id');
        $this->db->where($where);
        $this->db->group_by('billing.ticket_id');
        $this->db->order_by('billing.last_update', 'desc');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function change_frm($company_id, $filter)
    {
        $result = array();
        if ($filter == '' || $filter == 'today') {
            $today  = date("Y-m-d H:i:s");
            $today1 = date("Y-m-d 00:00:00");
            $where  = array(
                'billing.company_id' => $company_id,
                'billing.frm' => 1,
                'billing.drop_spare!=' => '',
                'billing.last_update>=' => $today1,
                'billing.last_update<=' => $today
            );
            //data is retrive from this query 
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('billing');
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'week') {
            $today = date("Y-m-d H:i:s");
            $week  = date("Y-m-d 00:00:00", strtotime("-1 week"));
            $where = array(
                'billing.company_id' => $company_id,
                'billing.frm' => 1,
                'billing.drop_spare!=' => '',
                'billing.last_update>=' => $week,
                'billing.last_update<=' => $today
            );
            //data is retrive from this query 
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('billing');
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            
        } else if ($filter == 'month') {
            $today = date("Y-m-d H:i:s");
            $mon   = date("Y-m-d 00:00:00", strtotime("-1 month"));
            $where = array(
                'billing.company_id' => $company_id,
                'billing.frm' => 1,
                'drop_spare!=' => '',
                'billing.last_update>=' => $mon,
                'billing.last_update<=' => $today
            );
            //data is retrive from this query 
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('billing');
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            
        } else if ($filter == 'year') {
            $today = date("Y-m-d H:i:s");
            $yr    = date("Y-01-01 00:00:00");
            $where = array(
                'billing.company_id' => $company_id,
                'billing.frm' => 1,
                'billing.drop_spare!=' => '',
                'billing.last_update>=' => $yr,
                'billing.last_update<=' => $today
            );
            //data is retrive from this query 
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('billing');
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            
        } else {
            $where = array(
                'billing.company_id' => $company_id,
                'billing.frm' => 1,
                'billing.drop_spare!=' => ''
            );
            //data is retrive from this query 
            $this->db->select('*');
            $this->db->from('billing');
            $this->db->join('technician','technician.technician_id=billing.tech_id');
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            
        }
        echo json_encode($result);
    }
    public function leaderboard_newdata($json, $company_id)
    {
        $seth = 0;
        foreach ($json as $row) {
            
            $today = date('Y-m-d', strtotime('last day of previous month'));
            $month = date("Y-m-01 ", strtotime("-1 month"));
            $where = array(
                'reward.month' => $row['month'],
                'reward.company_id' => $company_id,
                'reward.tech_id' => $row['tech_id']
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->where($where);
            $this->db->order_by('last_update', 'desc');
            $this->db->limit(1);
            $company = $this->db->get();
            $result  = $company->result_array();
            
            if (empty($result)) {
                $this->db->insert('reward', $row);
            } else {
                $seth = $result[0]['tech_reward_point'];
                $add  = array(
                    "tech_reward_point" => intval($seth) + intval($row['tech_reward_point'])
                );
                $this->db->where('last_update', $month);
                $this->db->where('tech_id', $row['tech_id']);
                $this->db->where('company_id', $company_id);
                $this->db->update('reward', $add);
            }
        }
		
    }
    public function disply_contract($datas)
    {
        $where = array(
            "all_tickets.company_id" => $datas,
            "all_tickets.amc_id!=" => ''
        );
        $group = array(
            "customer.customer_id",
            "customer.serial_no",
            "customer.product_serial_no"
        );
        $this->db->select('*,customer.customer_name,product_management.product_name,category_details.cat_name');
        $this->db->from('all_tickets');
        $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
        $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
        $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
        $this->db->order_by('all_tickets.last_update', 'desc');
        $this->db->group_by('all_tickets.ticket_id');
        $this->db->group_start();
        $this->db->where('all_tickets.current_status', 8);
        $this->db->or_where('all_tickets.current_status', 12);
        $this->db->group_end();
        $this->db->where($where);
        
        //$this->db->where("all_tickets.product_id=customer.product_serial_no");
        //$this->db->where("all_tickets.cat_id=customer.component_serial_no");
        //        $this->db->group_by('customer.customer_id');
        $query = $this->db->get();
        //print_r($query);
        //exit;
        $data  = $query->result_array();
        return $data;
        
    }
    public function view_billimages($id, $c)
    {
        $where = array(
            'ticket_id' => $id,
            'company_id' => $c
        );
        $this->db->select('bill_image');
        $this->db->from('billing');
        $this->db->join('technician','technician.technician_id=billing.tech_id');
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->result_array();
        return $query;
    }
    
    public function view_form($tech_id, $company_id, $from, $to)
    {
        $json    = array();
        $address = 0;
        $where   = array(
            "reimbursement.company_id" => $company_id,
            "reimbursement.technician_id" => $tech_id,
            "reimbursement.start_time>=" => $from,
            "reimbursement.end_time<=" => $to           
        );

        $this->db->select('*, reimbursement.id as reim_id,reimbursement.view as img');
        $this->db->from('reimbursement');
        //$this->db->join("technician", "reimbursement.technician_id=technician.technician_id");
        $this->db->join("all_tickets", "all_tickets.ticket_id=reimbursement.ticket_id");
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        
        foreach ($result as $row) {
            /*
            $lat  = $row['source_lat'];
            $long = $row['source_long'];
            
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($long) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output         = json_decode($response);
            $status         = $output->status;
            //Get address from json data
            $source_Address = ($status == "OK") ? $output->results[0]->formatted_address : '';
            
            $lat1  = $row['dest_lat'];
            $long1 = $row['dest_long'];
           
            $url   = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat1) . ',' . trim($long1) . '&sensor=false&key=AIzaSyDa3Ydy7cDtnIt0F2cZ9RBw4S3sVkuqCjI';
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output1 = json_decode($response);
            $status1 = $output1->status;
            
            //Get address from json data
            $address = ($status1 == "OK") ? $output1->results[1]->formatted_address : '';*/
            //Return address of the given latitude and longitude
            
            
            array_push($json, array(
                "reim_id" => $row['reim_id'],
                "ticket_id" => $row['ticket_id'],
                "employee_id" => $row['employee_id'],
                "first_name" => $row['first_name'],
                "technician_id" => $row['technician_id'],
                "mode_of_travel" => $row['mode_of_travel'],
                "source_Address" => $row['source'],
                "destin_Address" => $row['destination'],
                "no_of_km_travelled" => $row['no_of_km_travelled'],
                "travelling_charges" => $row['travelling_charges'],
                "view" => $row['img']
            ));
        }
        return $json;
        
    }
    
    
    
    
    public function csat_insight($company_id, $filter, $product_id)
    {
        $res = array();
        $ar  = array();
        if ($filter == '' || $filter == 'today') {
            if ($product_id == '') {
                $now       = date('Y-m-d H:i:s');
                $start     = date('Y-m-d 00:00:00');
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                        
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            } else {
                $now       = date('Y-m-d H:i:s');
                $start     = date('Y-m-d 00:00:00');
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            }
        } else if ($filter == 'week') {
            if ($product_id == '') {
                $now = date('Y-m-d H:i:s');
                
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $week
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            } else {
                $now = date('Y-m-d H:i:s');
                
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $week
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    
                    $percent = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            }
        } else if ($filter == 'month') {
            if ($product_id == '') {
                $i          = 0;
                $start_date = date("Y-m-01 00:00:00");
                $d          = new DateTime($start_date);
                $end_date   = $d->format('Y-m-t 23:00:00');
                $sub_where  = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $end_date,
                    'raised_time>=' => $start_date
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            } else {
                $i          = 0;
                $start_date = date("Y-m-01 00:00:00");
                $d          = new DateTime($start_date);
                $end_date   = $d->format('Y-m-t 23:00:00');
                $sub_where  = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $end_date,
                    'raised_time>=' => $start_date
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product_id = $product_id;
                        $cat_id     = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5 = $this->db->get();
                        
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4 = $this->db->get();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $close_first     = $query4->num_rows();
                    
                    $percent = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            }
        } else if ($filter == 'year') {
            if ($product_id == '') {
                $now           = date('Y-m-d H:i:s');
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("-1 year")); //April 1st calculation
                } else {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("today")); //April 1st calculation
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start_of_yr
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    
                    $total = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            } else {
                $now           = date('Y-m-d H:i:s');
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("-1 year")); //April 1st calculation
                } else {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("today")); //April 1st calculation
                }
                
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start_of_yr
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst       = $row['ticket_start_time'];
                        $tet       = $row['ticket_end_time'];
                        $dteEnd    = new DateTime($tet);
                        $dteStart  = new DateTime($tst);
                        $dteDiff   = $dteStart->diff($dteEnd);
                        $d         = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart  = strtotime($tst);
                        $dteDiff   = strtotime($tet);
                        $interval  = abs($dteDiff - $dteStart);
                        $minutes   = round($interval / 60);
                        $value     = $this->convertToHoursMins($minutes, '%02d:%02d:00');
                        $tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1  = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response   = date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 1;
                                } else if ($value > $resolution && $value1 <= $response) {
                                    $ar1 = $ar1 + 0.5;
                                } else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
                    $sub_where2      = array(
                        "cust_rating!=" => 0,
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    $sub_whe         = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $percent     = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                } else {
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_first    = 0;
                    $this->db->select('SLA_Compliance_Target');
                    $this->db->from('sla_mapping');
                    $this->db->where('company_id', $company_id);
                    $query133   = $this->db->get();
                    $res61      = $query133->result_array();
                    $target_sla = 0;
                    foreach ($res61 as $r) {
                        $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                    }
                    if (count($res61) != 0) {
                        $target_sla = $target_sla / count($res61);
                    }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_first    = $t['first_time'];
                    }
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => 0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => 0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => 0,
                        'value1' => $target_sla
                    ));
                }
            }
        }
        echo json_encode($res);
    }
    
     public function csat_insights($company_id, $filter, $product_id)
    {
        $res = array();
        $ar  = array();
        if ($filter == '' || $filter == 'today') {
            if ($product_id == '') 
			{
                $now       = date('Y-m-d H:i:s');
                $start     = date('Y-m-d 00:00:00');
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');

$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
 $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                        
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                   
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            } 
			else {
                $now       = date('Y-m-d H:i:s');
                $start     = date('Y-m-d 00:00:00');
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                               $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
        } else if ($filter == 'week') {
            if ($product_id == '') 
			{
                $now = date('Y-m-d H:i:s');
                
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $week
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                              $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $sub_whe    = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                   
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
			else {
                $now = date('Y-m-d H:i:s');
                
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $week
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product =  $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                               $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $week,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $week
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                   
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
        } else if ($filter == 'month') {
            if ($product_id == '') 
			{
                $i          = 0;
                $start_date = date("Y-m-01 00:00:00");
                $d          = new DateTime($start_date);
                $end_date   = $d->format('Y-m-t 23:00:00');
                $sub_where  = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $end_date,
                    'raised_time>=' => $start_date
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                   
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
			else {
                $i          = 0;
                $start_date = date("Y-m-01 00:00:00");
                $d          = new DateTime($start_date);
                $end_date   = $d->format('Y-m-t 23:00:00');
                $sub_where  = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $end_date,
                    'raised_time>=' => $start_date
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product_id = $product_id;
                        $cat_id     = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5 = $this->db->get();
                        
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $end_date,
                        'raised_time>=' => $start_date
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4 = $this->db->get();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $close_first     = $query4->num_rows();
                    
                    $percent = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
        } else if ($filter == 'year') {
            if ($product_id == '')
				{
                $now           = date('Y-m-d H:i:s');
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("-1 year")); //April 1st calculation
                } else {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("today")); //April 1st calculation
                }
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start_of_yr
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $row['product_id'];
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                               $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    $sub_where2    = array(
                        "cust_rating!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    $sub_whe       = array(
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    
                    $total = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $percent         = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100, 0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            } 
			else {
                $now           = date('Y-m-d H:i:s');
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("-1 year")); //April 1st calculation
                } else {
                    $start_of_yr = date('Y-04-01 00:00:00', strtotime("today")); //April 1st calculation
                }
                
                $sub_where = array(
                    "all_tickets.current_status" => 12,
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'raised_time<=' => $now,
                    'raised_time>=' => $start_of_yr
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $query  = $this->db->get();
                $result = $query->result_array();
                if (!empty($result)) {
                    $overall_tickets = $query->num_rows();
                    $ar1             = 0;
                    $ar2             = array();
                    foreach ($result as $row) {
                        $tst      = $row['ticket_start_time'];
                        $tet      = $row['ticket_end_time'];
                        $dteEnd   = new DateTime($tet);
                        $dteStart = new DateTime($tst);
                        $dteDiff  = $dteStart->diff($dteEnd);
                        $d        = $dteDiff->format("%Y-%m-%d %H:%I:%S");
                        $dteStart = strtotime($tst);
                        $dteDiff  = strtotime($tet);
                        $interval = abs($dteDiff - $dteStart);
                        $minutes  = round($interval / 60);
                        $value    = $this->convertToHoursMins($minutes, '%02d:%02d:00');
$tst1      = $row['assigned_time'];
                        $tet1      = $row['acceptance_time'];
                        $dteEnd1   = new DateTime($tet1);
                        $dteStart1 = new DateTime($tst1);
                        $dteDiff1 = $dteStart1->diff($dteEnd1);
                        $d1        = $dteDiff1->format("%Y-%m-%d %H:%I:%S");
                        $dteStart1 = strtotime($tst1);
                        $dteDiff1  = strtotime($tet1);
                        $interval1 = abs($dteDiff1 - $dteStart1);
                        $minutes1  = round($interval1 / 60);
                        $value1    = $this->convertToHoursMins($minutes1, '%02d:%02d:00');
                        array_push($ar, array(
                            "diff" => $value
                        ));
                        $product = $product_id;
                        $cat_id  = $row['cat_id'];
                        
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('product', 'all');
                        $this->db->group_end();
                        $query  = $this->db->get();
                        $result = $query->result_array();
                        if (!empty($result)) {
                            $product = 'all';
                        } else {
                            $product = $row['product_id'];
                        }
                        $this->db->select('*');
                        $this->db->from('sla_combination');
                        $this->db->where('company_id', $company_id);
                        $this->db->group_start();
                        $this->db->like('category', 'all');
                        $this->db->group_end();
                        $query1  = $this->db->get();
                        $result1 = $query1->result_array();
                        if (!empty($result1)) {
                            $category = 'all';
                        } else {
                            $category = $row['cat_id'];
                        }
                        
                        $where8 = array(
                            'company_id' => $company_id,
                            'product' => $product,
                            'category' => $category
                        );
                        $this->db->select('ref_id');
                        $this->db->from('sla_combination');
                        $this->db->where($where8);
                        $query5  = $this->db->get();
                        $result5 = $query5->result_array();
                        if (!empty($result5)) {
                            $ref_id = $result5[0]['ref_id'];
                            $this->db->select('resolution_time,response_time');
                            $this->db->from('sla_mapping');
                            $this->db->where('ref_id', $ref_id);
                            $query6  = $this->db->get();
                            $result6 = $query6->result_array();
                            
                            if (!empty($result6)) {
                                $response= date('H:i:s', strtotime($result6[0]['response_time']));
                                $resolution = date('H:i:s', strtotime($result6[0]['resolution_time']));
                                if ($value <= $resolution && $value1<=$response) {
                                   $ar1=$ar1+1;
                                } 
else if($value > $resolution && $value1<=$response)
{
$ar1=$ar1+0.5;
}
else {
                                    array_push($ar2, array(
                                        $resolution
                                    ));
                                }
                            }
                            
                            $sla_complaince = $ar1;
                        } else {
                            $sla_complaince = 0;
                        }
                    }
                    $where1 = array(
                        "all_tickets.current_status" => 12,
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    $this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
                    $this->db->from('all_tickets');
                    $this->db->where($where1);
                    $query = $this->db->get();
                    $res6  = $query->num_rows();
                    if ($res6 != 0) {
                        $answer  = 0;
                        $percent = 0;
                        $answer  = $sla_complaince / $res6;
                        $percent = $answer * 100;
                        $this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    } else {
                        $percent = 0;
                    }
                    $sub_where1 = array(
                        "cust_feedback!=" => '',
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.company_id' => $company_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where1);
                    $query2        = $this->db->get();
                    $cust_feedback = $query2->num_rows();
                    
                    $this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
                    $sub_where2      = array(
                        "cust_rating!=" => 0,
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                    );
                    $sub_whe         = array(
                        'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr,
                        'current_status' => 12
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_whe);
                    $query2 = $this->db->get();
                    $total  = $query2->num_rows();
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where2);
                    $query3      = $this->db->get();
                    $cust_rating = $query3->result_array();
                    $cust_rat    = 0;
                    foreach ($cust_rating as $c) {
                        $cust_rat = $cust_rat + $c['cust_rating'];
                    }
                    $cust_rating = $cust_rat / $total;
                    $cust_rating = $cust_rating / 5;
                    $sub_where3  = array(
                        "all_tickets.ticket_id" => $row['ticket_id'],
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.product_id' => $product_id,
                        'prev_tech_id' => '',
                        'raised_time<=' => $now,
                        'raised_time>=' => $start_of_yr
                    );
                    
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->where($sub_where3);
                    $this->db->group_start();
                    $this->db->where("(all_tickets.current_status = 12 OR all_tickets.previous_status!=9)");
                    $this->db->group_end();
                    $query4      = $this->db->get();
                    $close_first = $query4->num_rows();
                    $percent     = number_format((float) $percent, 0, '.', '');
                    array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' => number_format((float) $cust_feedback * 100, 0, '.', ''),
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' => number_format((float) $cust_rating * 100,0, '.', ''),
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' => number_format((float) $close_first * 100, 0, '.', ''),
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' => $percent,
                        'value1' => $target_sla
                    ));
                }
				else
				{
				$this->db->select('*');
                    $this->db->from('score_update');
                    $this->db->where('company_id', $company_id);
                    $query22         = $this->db->get();
                    $target_cust     = $query22->result_array();
                    $target_cust_rat = 0;
                    $target_cust_feed = 0;
                    $target_first    = 0;
					$this->db->select('SLA_Compliance_Target');
                        $this->db->from('sla_mapping');
                        $this->db->where('company_id', $company_id);
                        $query133   = $this->db->get();
                        $res61      = $query133->result_array();
                        $target_sla = 0;
                        foreach ($res61 as $r) {
                            $target_sla = $target_sla + $r['SLA_Compliance_Target'];
                        }
                        if (count($res61) != 0) {
                            $target_sla = $target_sla / count($res61);
                        }
                    foreach ($target_cust as $t) {
                        $target_cust_rat = $target_cust_rat + $t['customer_rating'];
                        $target_cust_feed = $target_cust_feed + $t['cust_feed'];
                        $target_first    = $t['first_time'];
                    }
                    
					if($query22->num_rows() >0)
					{
                    $target_cust_rat = $target_cust_rat / $query22->num_rows();
					$target_cust_feed = $target_cust_feed / $query22->num_rows();
					}
					else
					{
						$target_cust_rat =0;	
						$target_cust_feed =0;
					}
				array_push($res, array(
                        'label' => 'Customer Feedback',
                        'value' =>0,
                        'value1' => $target_cust_feed
                    ));
                    array_push($res, array(
                        'label' => 'Customer Rating',
                        'value' =>0,
                        'value1' => $target_cust_rat
                    ));
                    array_push($res, array(
                        'label' => 'First Call Closure',
                        'value' =>0,
                        'value1' => $target_first
                    ));
                    array_push($res, array(
                        'label' => 'SLA Complaince',
                        'value' =>0,
                        'value1' => $target_sla
                    ));
				}
            }
        }
        return $res;
    }
   
    public function load_level_based($company_id, $area,$region,$location, $level)
    {
        if($level == '')
        {
          
             //
        $month=date('m',strtotime('-1 month'));
        $this->db->select('*');
        $this->db->from('reward');
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
        //$this->db->where('technician.skill_level', $level);
       // $this->db->where('technician.area', $area);
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->order_by('reward.tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id', 'desc');
        $query = $this->db->get();
		if( $query->num_rows()>0)
		{
        return $query->result_array();
        
		}
		else{
			$month1=date('m');
			
			$this->db->select('*');
			$this->db->from('reward');
			$this->db->where('reward.company_id', $company_id);
			$this->db->where('reward.month', $month1);
		    //$this->db->where('technician.skill_level', $level);
		   // $this->db->where('technician.area', $area);
			$this->db->join("technician", "reward.tech_id=technician.technician_id");
			$this->db->order_by('reward.tech_reward_point', 'desc');
			$this->db->group_by('reward.tech_id');
            $query1 = $this->db->get();
            $result = $query1->result_array();
			return $result;	
		}
        //
        }
        else{
          //
        $month=date('m',strtotime('-1 month'));
        $this->db->select('*');
        $this->db->from('reward');
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
        $this->db->where('technician.skill_level', $level);
       // $this->db->where('technician.area', $area);
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->order_by('reward.tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id', 'desc');
        $query = $this->db->get();
		if( $query->num_rows()>0)
		{
        return $query->result_array();
        
		}
		else{
			$month1=date('m');
			
			$this->db->select('*');
			$this->db->from('reward');
			$this->db->where('reward.company_id', $company_id);
			$this->db->where('reward.month', $month1);
		    $this->db->where('technician.skill_level', $level);
		   // $this->db->where('technician.area', $area);
			$this->db->join("technician", "reward.tech_id=technician.technician_id");
			$this->db->order_by('reward.tech_reward_point', 'desc');
			$this->db->group_by('reward.tech_id');
            $query1 = $this->db->get();
            $result = $query1->result_array();
			return $result;	
		}
        //
        }
       
    }


        public function load_closed($company_id, $area, $region, $location, $level)
    {
		$mon=date('m',strtotime('-1 month'));
        if ($region == '') {
            if ($level == '') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                      'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        //'technician.area' => $area
                        'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location
                        'technician.company_id' => $company_id
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.area' => $area
                        'technician.company_id' => $company_id
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L1') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                    );
                } else {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L2') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                       // 'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L3') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L4') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
            
        } else if ($region == 'north') {
            if ($level == '') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        //'technician.area' => $area,
                        //'technician.region' => 'north'
                        'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        //'technician.region' => 'north'
                        'technician.company_id' => $company_id
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.region' => 'north'
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        //'technician.region' => 'north'
                        'technician.company_id' => $company_id
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L1') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1',
                        //'technician.region' => 'north'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'north'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1',
                        //'technician.region' => 'north'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L2') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'north'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'north'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'north'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L3') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                       // 'technician.region' => 'north'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                    //    'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'north'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'north'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'north'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L4') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'north'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'north'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'north'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'north'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
            
        } else if ($region == 'south') {
            if ($level == '') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        //'technician.area' => $area,
                       // 'technician.region' => 'south'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == '' || $level == 'L1') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        //'technician.area' => $area,
                        'technician.skill_level' => 'L1',
                        'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'south'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'south'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1',
                        //'technician.region' => 'south'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L2') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'south'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'south'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'south'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L3') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //technician.region' => 'south'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'south'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'south'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'south'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L4') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'south'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                       // 'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'south'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'south'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'south'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
            
        } else if ($region == 'east') {
            if ($level == '') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L1') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        //'technician.area' => $area,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'east'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.skill_level' => 'L1',
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'east'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L2') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'east'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'east'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                        //'technician.region' => 'east'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L3') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'east'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'east'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'east'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L4') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'east'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'east'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'east'
                    );
                } else {
                    $where = array(
                        //'technician.location' => $location,
                        //'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                        //'technician.region' => 'east'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
            
        } else if ($region == 'west') {
            if ($level == '') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                     //   'technician.region' => 'west'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        'technician.company_id' => $company_id
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                } else {
                    $where = array(
                        'technician.company_id' => $company_id
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L1') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'west'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                        
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                     //   'technician.region' => 'west'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                     //   'technician.region' => 'west'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                        //'technician.region' => 'west'
                    );
                } else {
                    $where = array(
                     //   'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L1'
                 //       'technician.region' => 'west'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L2') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                 //       'technician.region' => 'west'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                      //  'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                    //    'technician.region' => 'west'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                   //     'technician.area' => $area,
                     //   'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                       // 'technician.region' => 'west'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                     //   'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                       // 'technician.region' => 'west'
                    );
                } else {
                    $where = array(
                   //     'technician.location' => $location,
                     //   'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L2'
                       // 'technician.region' => 'west'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L3') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                   //     'technician.region' => 'west'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                      //  'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'west'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                        //'technician.area' => $area,
                        //'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                        //'technician.region' => 'west'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                      //  'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                    //    'technician.region' => 'west'
                    );
                } else {
                    $where = array(
                  //      'technician.location' => $location,
                    //    'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L3'
                      //  'technician.region' => 'west'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($level == 'L4') {
                if ($area == '' && $location == '' || $area == 'all' && $location == 'all') {
                    $where = array(
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
               //         'technician.region' => 'west'
                    );
                } else if ($area != '' && $location == '' || $area != 'all' && $location == 'all') {
                    $where = array(
                 //       'technician.area' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                   //     'technician.region' => 'west'
                    );
                } else if ($area != '' && $location != '' || $area != 'all' && $location != 'all') {
                    $where = array(
                  //      'technician.area' => $area,
                   //     'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                  //      'technician.region' => 'west'
                    );
                } else if ($location != '' && $area == '' || $location != 'all' && $area == 'all') {
                    $where = array(
                    //    'technician.location' => $location,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                     //   'technician.region' => 'west'
                    );
                } else {
                    $where = array(
                    //    'technician.location' => $location,
                      //  'technician.town' => $area,
                        'technician.company_id' => $company_id,
                        'technician.skill_level' => 'L4'
                     //   'technician.region' => 'west'
                    );
                }
                $this->db->select('*,technician.image');
                $this->db->from('reward');
                $this->db->join("technician", "reward.tech_id=technician.technician_id");
                $this->db->where($where);
                $this->db->where('reward.month',$mon);
                $this->db->order_by('reward.tech_reward_point', 'desc');
                $this->db->group_by('reward.tech_id');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
            
        }
        echo json_encode($result);
    }
    public function callclosed($datas)
    {
        $company_id = $datas;
        $where      = array(
            "all_tickets.current_status" => 12,
            "all_tickets.company_id" => $datas
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function disply_accept_contract($datas)
    {
        $where = array(
            "all_tickets.current_status" => 12,
            "all_tickets.company_id" => $datas,
            "all_tickets.amc_id!=" => ''
        );
        $this->db->select('*,customer.customer_name,product_management.product_name,category_details.cat_name');
        $this->db->from('all_tickets');
        $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
        $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
        $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
        $this->db->order_by('all_tickets.last_update', 'desc');
        $this->db->group_by('all_tickets.cust_id');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return $data;
        
    }
    public function convertToHoursMins($time, $format = '%02d:%02d')
    {
        if ($time < 1) {
            return;
        }
        $hours   = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }
    public function disply_attendance($datas)
    {
        $company_id = $datas;
        $today      = date("Y-m-d");
        $where      = array(
            'attendance.company_id' => $company_id,
            'attendance.date' => $today
        );
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join("technician", "attendance.technician_id=technician.technician_id");
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->join('category_details', 'category_details.cat_id=technician.category');
        //$this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
        $this->db->where($where);
        $query = $this->db->get(); // print_r($query);
        return $query;
    }
     public function leader($company_id, $region, $area)
    {  
		$month=date('m',strtotime('-1 month'));
        $this->db->select('*');
        $this->db->from('reward');
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
       // $this->db->where('technician.region', $region);
       // $this->db->where('technician.area', $area);
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->order_by('reward.tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id', 'desc');
        $query = $this->db->get();
		if( $query->num_rows()>0)
		{
        return $query;
		}
		else{
			$month1=date('m');
			
			$this->db->select('*');
			$this->db->from('reward');
			$this->db->where('reward.company_id', $company_id);
			$this->db->where('reward.month', $month1);
		   // $this->db->where('technician.region', $region);
		   // $this->db->where('technician.area', $area);
			$this->db->join("technician", "reward.tech_id=technician.technician_id");
			$this->db->order_by('reward.tech_reward_point', 'desc');
			$this->db->group_by('reward.tech_id', 'desc');
			$query1 = $this->db->get();
			return $query1;	
		}
    }

    public function disply_Spare($company_id)
    {
        $json = array();
        $this->db->group_by('ticket_id');
        
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.requested_spare!=' => NULL,
            'all_tickets.current_status' => 14
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.cat_id=all_tickets.cat_id");
        $this->db->where($where);
        $query = $this->db->get();
	//print_r($query);
        $data  = $query->result_array();
        return ($data);
    }
    public function impressed_Spare($company_id)
    {
        $json = array();
        $company_id = $company_id;
        $where      = array(
            'personal_spare.company_id' => $company_id,
            'personal_spare.status' => 0
        );
        $this->db->select('personal_spare.spare_array,personal_spare.spare_level,personal_spare.tech_id,personal_spare.status,personal_spare.status_comment,personal_spare.company_id,technician.employee_id,technician.technician_id,technician.first_name,technician.last_name,technician.image,technician.email_id,technician.skill_level,technician.contact_number,technician.alternate_number,technician.flat_no,technician.street,technician.city,technician.state,technician.region,technician.location,technician.area,technician.role,technician.product,technician.category,technician.current_location,technician.current_lat,technician.current_long,technician.availability,technician.today_task_count,technician.task_count,technician.companyname,technician.last_update,technician.town,technician.landmark,technician.country,technician.pincode,product_management.product_id,product_management.product_name,product_management.product_image,product_management.product_modal,product_management.product_desc,category_details.cat_id,category_details.cat_name,category_details.cat_image,category_details.prod_id,category_details.cat_modal,category_details.cat_desc,personal_spare.id as p_id');
        $this->db->from('personal_spare');
        $this->db->join("technician", "personal_spare.tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $this->db->group_by("personal_spare.id");
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }
    public function leader_region($datas, $filter)
    {
        $company_id = $datas;
        if ($filter == 'north') {
            $where = array(
                'technician.region' => 'north'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'south') {
            $where = array(
                'technician.region' => 'south'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'east') {
            $where = array(
                'technician.region' => 'east'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'west') {
            $where = array(
                'technician.region' => 'west'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    public function impressed_accept_Spare($company_id)
    {
        $json = array();
        
        $company_id = $company_id;
        $where      = array(
            'personal_spare.company_id' => $company_id,
            'personal_spare.status' => 1,
            'personal_spare.spare_array!=' => ''
        );
        $this->db->select('personal_spare.id,personal_spare.spare_array,personal_spare.spare_level,personal_spare.tech_id,personal_spare.status,personal_spare.status_comment,personal_spare.company_id,technician.employee_id,technician.technician_id,technician.first_name,technician.last_name,technician.image,technician.email_id,technician.skill_level,technician.contact_number,technician.alternate_number,technician.flat_no,technician.street,technician.city,technician.state,technician.region,technician.location,technician.area,technician.role,technician.product,technician.category,technician.current_location,technician.current_lat,technician.current_long,technician.availability,technician.today_task_count,technician.task_count,technician.companyname,technician.last_update,technician.town,technician.landmark,technician.country,technician.pincode,product_management.product_id,product_management.product_name,product_management.product_image,product_management.product_modal,product_management.product_desc,category_details.cat_id,category_details.cat_name,category_details.cat_image,category_details.prod_id,category_details.cat_modal,category_details.cat_desc');
        $this->db->from('personal_spare');
        $this->db->join("technician", "personal_spare.tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
		$this->db->group_by("personal_spare.id");
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }
    public function disply_accept_Spare($company_id)
    {
        $json = array();
        
        $company_id = $company_id;
        $today      = date("Y-m-d");
        $today1     = date("Y-m-d");
        $this->db->group_by('all_tickets.ticket_id');
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.current_status' => 11
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }
    public function disply_reject_Spare($company_id)
    {
        $json = array();
        $this->db->group_by('ticket_id');
        $company_id = $company_id;
        $today      = date("Y-m-d");
        $today1     = date("Y-m-d");
        $this->db->group_by('ticket_id');
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.current_status' => 17
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        return ($data);
    }
    
    public function leaderboard_static($company_id, $filter)
    {
        $company_id = $company_id;
        if ($filter == 'L1') {
            $where = array(
                'reward.technician_level' => 'L1'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'L2') {
            $where = array(
                'reward.technician_level' => 'L2'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'L3') {
            $where = array(
                'reward.technician_level' => 'L3'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'L4') {
            $where = array(
                'reward.technician_level' => 'L4'
            );
            $this->db->select('*');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    public function rewardrank($datas)
    {
        $company_id = $datas;
        $month =date('m',strtotime("-1 month"));
        
        $this->db->select('tech_reward_point,tech_name,technician.image');
        $this->db->from('reward');
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
       // $this->db->where('technician.region', $region);
       // $this->db->where('technician.area', $area);
        $this->db->order_by('tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id');
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query;
    }
    public function rewardrank2($datas)
    {
        $company_id = $datas;
        $month =date('m',strtotime("-1 month"));
        $this->db->select('tech_reward_point,tech_name,technician.image');
        $this->db->from('reward');
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
       // $this->db->where('technician.region', $region);
      //  $this->db->where('technician.area', $area);
        $this->db->order_by('tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id');
        $this->db->limit(1, 1);
        $query = $this->db->get();
        return $query;
    }
    public function rewardrank3($datas)
    {
        $company_id = $datas;
        $month =date('m',strtotime("-1 month"));
        $this->db->select('tech_reward_point,tech_name,technician.image');
        $this->db->from('reward');
        $this->db->join("technician", "reward.tech_id=technician.technician_id");
        $this->db->where('reward.company_id', $company_id);
        $this->db->where('reward.month', $month);
      //  $this->db->where('technician.region', $region);
      //  $this->db->where('technician.area', $area);
        $this->db->order_by('tech_reward_point', 'desc');
        $this->db->group_by('reward.tech_id');
        $this->db->limit(1, 2);
        $query = $this->db->get();
        return $query;
    }
    
    public function contract_change($data, $action, $company_id)
    {
        $where1 = array(
            'all_tickets.ticket_id' => $action,
            'all_tickets.company_id' => $company_id
        );
		$date=date('Y-m-d');
       // $amc_id=$this->select_amc();
		
		$this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where1);
        $cust = $this->db->get();
		$cus = $cust->result_array();
		
		//$myStr=$cus[0]['call_type'];
		//$result = substr($myStr, 0, 4);
		//$gen_amc_id=$result.'_'. $amc_id;
		
		$period=$this->calculate_contract($action);
		$wdate=$this->warrenty_calc($date,$period);
		//echo $wdate;
		
		$this->db->select('*');
		$this->db->from('customer');
        $this->db->where('customer.customer_id',$cus[0]['cust_id']);
       // $this->db->where('customer.type_of_contract',$cus[0]['call_type']);
        $info = $this->db->get();
		$res= $info->result_array();
		$existing_contract = array(
'customer.customer_id'=>$cus[0]['cust_id'],
'customer.contact_number'=>$res[0]['contact_number'],
'customer.type_of_contract'=>$cus[0]['call_type'],
'customer.product_serial_no'=>$cus[0]['product_id'],
'customer.component_serial_no'=>$cus[0]['cat_id'],
'customer.model_no'=>$cus[0]['model'],
'customer.serial_no'=>$cus[0]['serial_no'],
'customer.company_id'=>$company_id
);

$this->db->select('*');
$this->db->from('customer');
$this->db->where($existing_contract);
$existing = $this->db->get();
$existing_cust = $existing->result_array();
$count_contract= $existing->num_rows();
if($count_contract<1){
		$new_cont=array(
		'customer_id'=>$cus[0]['cust_id'],
		'customer_name'=>$res[0]['customer_name'],
		'email_id'=>$res[0]['email_id'],
		'contact_number'=>$res[0]['contact_number'],
		'alternate_number'=>$res[0]['alternate_number'],
		'door_num'=>$res[0]['door_num'],
		'address'=>$res[0]['address'],
		'cust_town'=>$res[0]['cust_town'],
		'landmark'=>$res[0]['landmark'],
		'city'=>$res[0]['city'],
		'state'=>$res[0]['state'],
		'cust_country'=>$res[0]['cust_country'],
		'pincode'=>$res[0]['pincode'],
		'type_of_contract'=>$cus[0]['call_type'],
		'product_serial_no'=>$cus[0]['product_id'],
		'component_serial_no'=>$cus[0]['cat_id'],
		'model_no'=>$cus[0]['model'],
		'serial_no'=>$cus[0]['serial_no'],
		'priority'=>$cus[0]['priority'],
		'contract_id'=>$cus[0]['amc_id'],
		'contract_value'=>$cus[0]['total_amount'],
		'start_date'=>$date,
		'warrenty_expairy_date'=>$wdate,
		'company_id'=>$company_id
		);
$true=$this->db->insert('customer', $new_cont);
}
else{
	/*
$this->db->where($existing_contract);
$true= $this->db->update('warrenty_expairy_date', $wdate);*/
$this->db->set('warrenty_expairy_date', $wdate);
$this->db->where($existing_contract);
$true=$this->db->update('customer');
}
if($true==1){
$this->db->where($where1);
$this->db->update('all_tickets', $data);
return true;
}
		$this->db->select('*');
		$this->db->from('customer');
        $this->db->where('customer.customer_id',$cus[0]['cust_id']);
        $this->db->where('customer.type_of_contract','');
        $info1 = $this->db->get();
		$res1= $info1->num_rows();
		$res2= $info1->row_array();
		//if($res1>0)
		//{
		//	$this->db->where('id',$res2['id']);
		///	$this->db->update('customer', $data);
        //	return true;
		//}
		//else{
			 $new_cont=array(
        'customer_id'=>$cus[0]['cust_id'],
        'customer_name'=>$res[0]['customer_name'],
        'email_id'=>$res[0]['email_id'],
        'contact_number'=>$res[0]['contact_number'],
        'alternate_number'=>$res[0]['alternate_number'],
        'door_num'=>$res[0]['door_num'],
        'address'=>$res[0]['address'],
        'cust_town'=>$res[0]['cust_town'],
        'landmark'=>$res[0]['landmark'],
        'city'=>$res[0]['city'],
        'state'=>$res[0]['state'],
        'cust_country'=>$res[0]['cust_country'],
        'pincode'=>$res[0]['pincode'],
        'type_of_contract'=>$cus[0]['call_type'],
        'product_serial_no'=>$cus[0]['product_id'],
        'component_serial_no'=>$cus[0]['cat_id'],
        'model_no'=>$cus[0]['model'],
        'serial_no'=>$cus[0]['serial_no'],
        'priority'=>$cus[0]['priority'],
        'contract_id'=>$cus[0]['amc_id'],
        'contract_value'=>$cus[0]['total_amount'],
        'start_date'=>$date,
        'warrenty_expairy_date'=>$wdate,
        'company_id'=>$company_id
        );
		 $true=$this->db->insert('customer', $new_cont);
		 
		if($true==1){
			$this->db->where($where1);
			$this->db->update('all_tickets', $data);
        	return true;
		}
		//}
        // $this->db->where($where2);
        //  $this->db->update('customer', $data1);
    }
    public function contract_change1($data1, $action, $company_id)
    {
        $where1 = array(
            'all_tickets.ticket_id' => $action,
            'all_tickets.company_id' => $company_id
        );
        $this->db->where($where1);
        $this->db->update('all_tickets', $data1);
        return true;
    }
    public function calculate_contract($action)
    {
        $this->db->select('call_type','contract_period');
        $this->db->from('all_tickets');
        $this->db->where('all_tickets.ticket_id', $action);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            //print_r($result);
            $this->db->select('contract_period');
            $this->db->from('amc_type');
            $this->db->where('amc_type.amc_type', $result[0]['call_type']);
            $query1 = $this->db->get();
            $res    = $query1->result_array();
            //print_r($res);
           // return $res[0]['contract_period'];
            return $res[0]['contract_period']; 
        }
    }
	public function warrenty_calc($date,$calc)
     {
        $this->load->helper('url');
        $this->load->database();
         // $cont =date('Y-m-d');
		  $time = strtotime($date);
                $CurrentDate = date('Y-m-d',$time);
	        $date = new DateTime($CurrentDate);
                $date->modify($calc);
                return $date->format('Y-m-d');
	 }
    //public function expiry_date($action,$contract_type)
    public function expiry_date($action, $period)
    {
        $this->db->select('last_update');
        $this->db->from('all_tickets');
        $this->db->where('all_tickets.ticket_id', $action);
        $query    = $this->db->get();
        $result   = $query->result_array();
        $new_date = date("Y-m-d", strtotime($result[0]['last_update']));
        //$date1= date('Y-m-d', strtotime("+$period months", strtotime($new_date)));
        //$date1 = date("Y-m-d", strtotime($new_date)) . " +".$yr."years";
        //$date1 = strtotime(date("Y-m-d", strtotime($date1)) . " +".$period."month");
        //$date = date('Y-m-d',strtotime($date1));
        return $new_date;
    }
    public function warrenty_date($old_date, $match)
    {
        //$date1 = date("Y-m-d", strtotime($old_date)) . " +".$match." month";
        $rdate = date($old_date, mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
        //$date1 = strtotime(date("Y-m-d", strtotime($date1)) . " +".$period."month");
        //$date = date('Y-m-d',strtotime($date1));
        //$time=strtotime($old_date);
        $time  = '2017-05-04';
        $time  = strtotime($time);
        $month = date("F", $time);
        $year  = date("Y", $time);
        $d     = date_parse_from_format("Y-m-d", $old_date);
        //echo $d["month"];
        //return $d["day"];
        $num_of_days;
        $a           = 0;
        $b           = 0;
        $total_month = $d['month'];
        /* if($year == date('Y'))
        $total_month = date('m');
        else
        $total_month = 12; */
        for ($m = 1; $m <= $total_month; $m++) {
            $num_of_days = cal_days_in_month(CAL_GREGORIAN, $m, $year);
            $a           = $a + $num_of_days;
        }
        for ($m = 1; $m <= $match; $m++) {
            $num_of_days = cal_days_in_month(CAL_GREGORIAN, $m, $year);
            $b           = $b + $num_of_days;
        }
        $result           = array(
            $a + $d["day"] + $b
        );
        //return $values = $values % 365;
        $sub_struct_month = ($result[0] / 30);
        $sub_struct_month = floor($sub_struct_month);
        $sub_struct_days  = ($result[0] % 30); // the rest of days			
        $sub_struct_year  = floor($result[0] % 365);
        $sub_struct       = $year . "-" . $sub_struct_month . "-" . $sub_struct_days;
        return $sub_struct;
        
        //return date("t",mktime(0,0,0,$d['month'],1,$d['year']));
    }
    
    public function disply_attendance2($company_id, $product_id, $region, $area, $location)
    {
        $json = array();
        if ($region == 'north') {
            if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area != '' && $location != '' || $product_id != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.current_status' => 11,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area == '' && $location != '' || $product_id != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('*');
            $this->db->from('attendance');
            $this->db->join("technician", "attendance.technician_id=technician.technician_id");
            $this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
            $this->db->join("all_tickets", "attendance.technician_id=all_tickets.tech_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($region == 'south') {
            if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.product_id' => $product_id,
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area != '' && $location != '' || $product_id != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area == '' && $location != '' || $product_id != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.current_status' => 11,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.current_status' => 11,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('*');
            $this->db->from('attendance');
            $this->db->join("technician", "attendance.technician_id=technician.technician_id");
            //$this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
            $this->db->join("all_tickets", "attendance.technician_id=all_tickets.tech_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($region == 'east') {
            if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.current_status' => 11,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area != '' && $location != '' || $product_id != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area == '' && $location != '' || $product_id != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('*');
            $this->db->from('attendance');
            $this->db->join("technician", "attendance.technician_id=technician.technician_id");
            //$this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
            $this->db->join("all_tickets", "attendance.technician_id=all_tickets.tech_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if($region == 'west') {
            if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area == '' && $location == '' || $product_id != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $area != '' && $location == '' || $product_id != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area == '' && $location == '' || $product_id == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area != '' && $location == '' || $product_id == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $area != '' && $location != '' || $product_id == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area != '' && $location != '' || $product_id != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $area == '' && $location != '' || $product_id == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $area == '' && $location != '' || $product_id != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $where = array(
                    'attendance.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('*');
            $this->db->from('attendance');
            $this->db->join("technician", "attendance.technician_id=technician.technician_id");
            //$this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($json);
    }
    
    public function disply_billing($datas)
    {
        $company_id = $datas;
        $today      = date("Y-m-d H:i:s");
        $where      = array(
         'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => ""
        );
        //data is retrive from this query 
       $this->db->select('*');
            
            
            $this->db->from('billing');
            $this->db->join("all_tickets", "all_tickets.ticket_id=billing.ticket_id");
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("amc_type", "amc_type.company_id=all_tickets.company_id");
            $this->db->where($where);
            $this->db->group_by('billing.ticket_id');
            $query = $this->db->get();
        
        // $query = $this->db->get('Users');  
        return $query;
    }
    public function disply_amc_bling($company_id, $filter, $revenue, $product_id, $region, $area, $location)
    {
        $result = array();
        if ($revenue == 'amc_ren') {
            $select = '*';
            if ($filter == 'today') {
                $today = date("Y-m-d");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' =>$area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d");
                    $today1 = date("Y-m-d");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.purchase_date' => $today,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                $today = date("Y-m-d");
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.purchase_date <='=>$today,'all_tickets.current_status!='=>16,'amc_id!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'week') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 week"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today = date("Y-m-d");
                    $week  = date("Y-m-d", strtotime("-1 week"));
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.purchase_date <='=>$today,'all_tickets.purchase_date >='=>$week,'all_tickets.current_status!='=>16,'amc_id!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'month') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 month"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' =>$today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today = date("Y-m-d");
                    $week  = date("Y-m-d", strtotime("-1 month"));
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.purchase_date <='=>$today,'all_tickets.purchase_date >='=>$week,'all_tickets.current_status!='=>16,'all_tickets.amc_id!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'year') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 year"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today = date("Y-m-d");
                    $week  = date("Y-m-d", strtotime("-1 year"));
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.purchase_date <=' => $today,
                        'all_tickets.purchase_date >=' => $week,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.purchase_date <='=>$today,'all_tickets.purchase_date >='=>$week,'all_tickets.current_status!='=>16,'all_tickets.amc_id!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else {
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today = date("Y-m-d");
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 20,
                        'all_tickets.amc_id!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.current_status!='=>16,'all_tickets.amc_id!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            }
        } else if ($revenue == 'service_bill') {
            $select = 'tech_id,total_amount,call_type,ticket_id,requested_spare';
            if ($filter == 'today') {
                $today = date("Y-m-d");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand'
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update'=>$today,'all_tickets.call_type'=>'On-Demand');
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'week') {
                $today                 = date("Y-m-d");
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.call_type' => 'On-Demand'
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.call_type'=>'On-Demand');
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'month') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 month"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand'
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d");
                    $today1 = date("Y-m-d");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.call_type'=>'On-Demand');
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'year') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 year"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.current_status' => 12,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.call_type' => 'On-Demand'
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.call_type'=>'On-Demand');
                $this->display_datails_amc($where, $select, $revenue);
            } else {
                $today = date("Y-m-d");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand'
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'on_demand',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.current_status' => 12,
                        'all_tickets.call_type' => 'On-Demand',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.call_type'=>'on_demand');
                $this->display_datails_amc($where, $select, $revenue);
            }
        } else if ($revenue == 'spre_bill') {
            $select = '*';
            if ($filter == 'today') {
                $today = date("Y-m-d");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        "all_tickets.last_update>=" => $today,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update'=>$today,'all_tickets.current_status!='=>14,'all_tickets.requested_spare!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'week') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.current_status!='=>14,'all_tickets.requested_spare!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'month') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 month"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d");
                    $today1 = date("Y-m-d");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.current_status!='=>14,'all_tickets.requested_spare!='=>"");
                $this->display_datails_amc($where, $select, $revenue);
            } else if ($filter == 'year') {
                $today = date("Y-m-d");
                $week  = date("Y-m-d", strtotime("-1 year"));
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d");
                    $today1 = date("Y-m-d");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.last_update <=' => $today,
                        'all_tickets.last_update >=' => $week,
                        'all_tickets.current_status!=' => 14,
                        'all_tickets.requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.last_update <='=>$today,'all_tickets.last_update >='=>$week,'all_tickets.current_status!='=>14,'all_tickets.requested_spare!='=>"");				
                $this->display_datails_amc($where, $select, $revenue);
            } else {
                $today = date("Y-m-d");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => ""
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d");
                    $today1 = date("Y-m-d");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'current_status!=' => 14,
                        'requested_spare!=' => "",
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                //$where=array('all_tickets.company_id'=>$company_id,'all_tickets.current_status!='=>14,'all_tickets.requested_spare!='=>"");				
                $this->display_datails_amc($where, $select, $revenue);
            }
        }
        //echo json_encode($result);
    }
    
    
    public function display_datails_amc($where, $select, $revenue)
    {
        $result   = array();
        $revenues = array(
            'revenue' => $revenue
        );
        if ($revenue == 'amc_ren') {
            $this->db->select('*');
            
            
            $this->db->from('billing');
            $this->db->join("all_tickets", "all_tickets.ticket_id=billing.ticket_id");
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("amc_type", "amc_type.company_id=all_tickets.company_id");
            $this->db->where($where);
            $this->db->group_by('billing.ticket_id');
            $query = $this->db->get();
            $query = $query->result_array();
        } else if ($revenue == 'service_bill') {
            $this->db->from('billing');
            $this->db->join("all_tickets", "all_tickets.ticket_id=billing.ticket_id");
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where($where);
            $this->db->group_by('billing.ticket_id');
            $query = $this->db->get();
            $query = $query->result_array();
        } else {
            $this->db->from('billing');
            $this->db->join("all_tickets", "all_tickets.ticket_id=billing.ticket_id");
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where('all_tickets.requested_spare!=', '');
            $this->db->where('billing.spare_amount!=', 0);
            $this->db->where($where);
            $this->db->group_by('billing.ticket_id');
            $query = $this->db->get();
            $query = $query->result_array();
        }
        //	array_push($result,$query);
        //array_push($result,$revenues);
        echo json_encode($query);
        
        //print_r(json_encode($query));
        //return $query;
    }
    public function disply_servicedesk($company_id)
    {
        $where = array(
            "technician_level" => 'L1',
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_servicedesk1($company_id)
    {
        $where = array(
            "technician_level" => 'L2',
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_servicedesk2($company_id)
    {
        $where = array(
            "technician_level" => 'L3',
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_servicedesk3($company_id)
    {
        $where = array(
            "technician_level" => 'L4',
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_update');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_rewarddesk($company_id)
    {
        $where = array(
            "technician_level" => "L1",
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_reward');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_rewarddesk2($company_id)
    {
        $where = array(
            "technician_level" => "L2",
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_reward');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_rewarddesk3($company_id)
    {
        $where = array(
            "technician_level" => "L3",
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_reward');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function disply_rewarddesk4($company_id)
    {
        $where = array(
            "technician_level" => "L4",
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('score_reward');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
    public function detail_spre_bill($id, $company)
    {
        $where = array(
            'ticket_id' => $id,
            'company_id' => $company
        );
        $this->db->select('requested_spare');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query = $this->db->get();
        $query = $query->result_array();
        print_r(json_encode($query));
    }
    public function disply_inventory($datas)
    {
        $company_id = $datas;
        $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => ''
                    );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
        return $result;
    }
    /* Display attendance details */
    public function load_attendance($company_id, $product_id, $region, $area, $location)
    {
        $today = date("Y-m-d");
        if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today
                
            );
        } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id
            );
        } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.region' => $region
            );
        } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.region' => $region,
                'technician.area' => $area
            );
        } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.area' => $area
            );
        } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.region' => $region
            );
        } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.region' => $region,
                'technician.area' => $area
            );
        } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' =>$today,
                'technician.region' => $region,
                'technician.area' => $area,
                'technician.location' => $location
            );
        } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.region' => $region,
                'technician.location' => $location
            );
        } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.area' => $area
            );
        } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.area' => $area,
                'technician.location' => $location
            );
        } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.area' => $area,
                'technician.location' => $location
            );
        } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.location' => $location
            );
        } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.location' => $location
            );
        } else {
            $where = array(
                'attendance.company_id' => $company_id,
                'attendance.date' => $today,
                'technician.product' => $product_id,
                'technician.location' => $location,
                'technician.region' => $region,
                'technician.area' => $area
            );
        }
        
        $this->db->select('*,technician.first_name,technician.location');
        $this->db->from('attendance');
        $this->db->join("technician", "attendance.technician_id=technician.technician_id");
        //	$this->db->join("avaliblity", "technician.availability=avaliblity.ava_id");
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->join('category_details', 'category_details.cat_id=technician.category');
        $this->db->where($where);
        //$this->db->where('attendance.date =',$today);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function disply_inventory2($company_id, $filter, $spare_bill, $product_id, $region, $area, $location)
    {
        $company_id = $company_id;
        $result     = array();
        if ($filter == 'c_today') 
		
		{
            if ($spare_bill == 'billed') {
                $today1 = date("Y-m-d H:i:s");
                $today  = date("Y-m-d 00:00:00");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get(); //print_r($query);
                $result = $query->result_array();
            } else if ($spare_bill == 'warrenty') {
                $today1 = date("Y-m-d H:i:s");
                $today  = date("Y-m-d 00:00:00");
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $today
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
        }
		else if ($filter == 'c_weekly')
			{
            if ($spare_bill == 'billed')
				{
                $today1                = date("Y-m-d H:i:s");
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            } 
			else if ($spare_bill == 'warrenty') {
                $today1                = date("Y-m-d H:i:s");
                $first_day_of_the_week = 'Monday';
                $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
                if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                    $week = date('Y-m-d', strtotime('today'));
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
        }
		else if ($filter == 'c_monthly') 
		{
            if ($spare_bill == 'billed')
				{
                $today1        = date("Y-m-d H:i:s");
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                } else {
                    $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($spare_bill == 'warrenty') {
                $today1        = date("Y-m-d H:i:s");
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                } else {
                    $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today1        = date("Y-m-d H:i:s");
                    $current_month = date('M'); //$current_month='Apr';
                    if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                        $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                    } else {
                        $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                    }
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
        }
		else if ($filter == 'c_yearly')
			{
            if ($spare_bill == 'billed')
				{
                $today1        = date("Y-m-d H:i:s");
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                } else {
                    $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today1        = date("Y-m-d H:i:s");
                    $current_month = date('M'); //$current_month='Apr';
                    if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                        $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                    } else {
                        $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                    }
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($spare_bill == 'warrenty') {
                $today1        = date("Y-m-d H:i:s");
                $current_month = date('M'); //$current_month='Apr';
                if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                    $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                } else {
                    $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                }
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                } else {
                    $today1        = date("Y-m-d H:i:s");
                    $current_month = date('M'); //$current_month='Apr';
                    if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                        $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
                    } else {
                        $week = date('Y-04-01', strtotime("today")); //April 1st calculation
                    }
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.last_update<=' => $today1,
                        'all_tickets.last_update>=' => $week
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
        }
		else if ($filter == '' || $filter == 'c_all')
			{
            if ($spare_bill == 'billed')
				{
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost!=' => 0
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost!=' => 0,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            } else if ($spare_bill == 'warrenty') {
                if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.requested_spare!=' => ''
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id
                    );
                } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.region' => $region,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area
                    );
                } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.town' => $area,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.location' => $location
                    );
                } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                    $where = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location
                    );
                } else {
                    $today  = date("Y-m-d 00:00:00");
                    $today1 = date("Y-m-d H:i:s");
                    $where  = array(
                        'all_tickets.company_id' => $company_id,
                        'all_tickets.requested_spare!=' => '',
                        'all_tickets.spare_cost' => 0,
                        'all_tickets.product_id' => $product_id,
                        'all_tickets.location' => $location,
                        'all_tickets.region' => $region,
                        'all_tickets.town' => $area
                    );
                }
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->join("technician", "technician.technician_id=all_tickets.tech_id");
                $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
                $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
                $this->db->where($where);
                $this->db->order_by('all_tickets.ticket_id', 'desc');
                $query  = $this->db->get();
                $result = $query->result_array();
            }
        }
        echo json_encode($result);
    }
    public function disply_escallated1($datas)
    {
        $company_id = $datas;
        //data is retrive from this query 
        $today      = date("Y-m-d H:i:s");
        $today1     = date("Y-m-d 00:00:00");
        $where      = array(
            'all_tickets.company_id' => $company_id,
           // 'all_tickets.last_update<=' => $today,
           // 'all_tickets.last_update>=' => $today1
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
        $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
        $this->db->where($where);
        $this->db->group_start();
        $this->db->where("(all_tickets.current_status = 9 OR all_tickets.previous_status=9)");
        $this->db->group_end();
        $query = $this->db->get();
        //$query = $this->db->get('Users');  
        return $query;
    }
    public function escallatedcalls($datas)
    {
        $company_id = $datas;
        $where      = array(
            "all_tickets.current_status" => 9,
                'all_tickets.company_id' => $company_id
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function getproduct()
    {
        $this->db->select('*');
        $this->db->from('product_management');
        $query  = $this->db->get();
        $result = $query->result_array();
        foreach ($result as $row) {
            $data[] = $row;
        }
        return $data;
        
    }
    public function disply_escallated2($company_id, $filter)
    {
        $company_id = $company_id;
        if ($filter == '' || $filter == 'c_today') {
            $today  = date("Y-m-d H:i:s");
            $today1 = date("Y-m-d 00:00:00");
            $where  = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.last_update<=' => $today,
                'all_tickets.last_update>=' => $today1
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where($where);
            $this->db->group_start();
            $this->db->where("(all_tickets.current_status = 9 OR all_tickets.previous_status=9)");
            $this->db->group_end();
            $query = $this->db->get();
            return $query;
        } else if ($filter == 'c_weekly') {
            $today = date("Y-m-d H:i:s");
            $week  = date("Y-m-d 00:00:00", strtotime("-1 week"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.last_update>=' => $week
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where($where);
            $this->db->where("(all_tickets.current_status = 9 OR all_tickets.previous_status=9)");
            $query = $this->db->get();
            return $query;
        } else if ($filter == '' || $filter == 'c_monthly') {
            $today = date("Y-m-d H:i:s");
            $month = date("Y-m-d 00:00:00", strtotime("-1 month"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.last_update<=' => $today,
                'all_tickets.last_update>=' => $month
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where($where);
            $this->db->where("(all_tickets.current_status = 9 OR all_tickets.previous_status=9)");
            $query = $this->db->get();
            return $query;
        } else if ($filter == '' || $filter == 'c_yearly') {
            $today = date("Y-m-d H:i:s");
            $year  = date("Y-m-d 00:00:00", strtotime("-1 year"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.last_update<=' => $today,
                'all_tickets.last_update>=' => $year
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->where($where);
            $this->db->where("(all_tickets.current_status = 9 OR all_tickets.previous_status=9)");
            $query = $this->db->get();
            return $query;
        }
    }
    
    public function getproduct_s()
    {
        //$stack = array("orange", "banana");
        $this->db->select('*');
        $this->db->from('product_management');
        //$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
        $this->db->order_by("id", "desc");
        $query  = $this->db->get();
        $result = $query->result_array();
        foreach ($result as $row) {
            $data[] = $row;
        }
        return $data;
    }
    
    public function getcategory_s($prod_id_1)
    {
        //$stack = array("orange", "banana");
        $this->db->select('*');
        $this->db->from('category_details');
        //$this->db->join('category_details', 'category_details.prod_id = product_management.product_id');
        $this->db->where("prod_id", $prod_id_1);
        
        $query  = $this->db->get();
        $result = $query->result_array();
        /* foreach ($result as $row) { 
        $data[] = $row; 
        } */
        return $result;
    }
    
    public function disply_completed($datas)
    {
        
        // data is retrive from this query
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        
        $where = array(
            'all_tickets.company_id' => $datas,
            'all_tickets.current_status' => 12,
          //  'all_tickets.last_update<=' => $today,
          //  'all_tickets.last_update>=' => $today1
        );
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
        $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
        $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
        $this->db->group_by("customer.customer_id");
        $this->db->where($where);
        $query = $this->db->get();
        
        // $query = $this->db->get('Users');
        
        return $query;
    }
    
    public function display_csat($company_id, $filter, $region, $area, $location, $product_id)
    {
        $today  = date("Y-m-d 00:00:00");
        $today1 = date("Y-m-d H:i:s");
        // data is retrive from this query
        if ($filter == '' || $filter == 'today') {
            $today1 = date("Y-m-d H:i:s");
            $today  = date("Y-m-d 00:00:00");
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('technician.employee_id,customer.customer_id,customer.customer_name,all_tickets.priority,all_tickets.ticket_id,all_tickets.cust_feedback,all_tickets.cust_rating,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.task_count as today_task_count');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->group_by("customer.customer_id");
            $this->db->where($where);
            $this->db->where('all_tickets.last_update>=', $today);
            $this->db->where('all_tickets.last_update<=', $today1);
            ;
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'week') {
            $today1                = date("Y-m-d H:i:s");
            $first_day_of_the_week = 'Monday';
            $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
            if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                $week = date('Y-m-d', strtotime('today'));
            }
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12
                    
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id
                    
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region
                    
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region
                    
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                    
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            }
            $this->db->select('technician.employee_id,customer.customer_id,customer.customer_name,all_tickets.priority,all_tickets.ticket_id,all_tickets.cust_feedback,all_tickets.cust_rating,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.task_count as today_task_count');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->group_by("customer.customer_id");
            $this->db->where($where);
            $this->db->where('all_tickets.last_update>=', $week);
            $this->db->where('all_tickets.last_update<=', $today1);
            ;
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'month') {
            $today1 = date("Y-m-d H:i:s");
            $week   = date("Y-m-01 00:00:00");
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12
                    
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id
                    
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('technician.employee_id,customer.customer_id,customer.customer_name,all_tickets.priority,all_tickets.ticket_id,all_tickets.cust_feedback,all_tickets.cust_rating,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.task_count as today_task_count');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->group_by("customer.customer_id");
            $this->db->where($where);
            $this->db->where('all_tickets.last_update>=', $week);
            $this->db->where('all_tickets.last_update<=', $today1);
            ;
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today1        = date("Y-m-d H:i:s");
            $week          = date("Y-01-01 00:00:00");
            $current_month = date('M'); //$current_month='Apr';
            /*if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
            $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
            } else {
            $week = date('Y-01-01', strtotime("today")); //April 1st calculation
            }*/
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12
                    
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id
                    
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region
                    
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region
                    
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area
                    
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.location' => $location
                    
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                    
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                    
                );
            }
            
            $this->db->select('technician.employee_id,customer.customer_id,customer.customer_name,all_tickets.priority,all_tickets.ticket_id,all_tickets.cust_feedback,all_tickets.cust_rating,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.task_count as today_task_count');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->group_by("customer.customer_id");
            $this->db->where($where);
            $this->db->where('all_tickets.last_update>=', $week);
            $this->db->where('all_tickets.last_update<=', $today1);
            ;
            $query  = $this->db->get(); //print_r($query);
            $result = $query->result_array();
        } else if ($filter == 'all') {
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.location' => $location
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 12,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area
                );
            }
            $this->db->select('technician.employee_id,customer.customer_id,customer.customer_name,all_tickets.priority,all_tickets.ticket_id,all_tickets.cust_feedback,all_tickets.cust_rating,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.task_count as today_task_count');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->group_by("customer.customer_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    public function disply_completed2($company_id, $filter, $product_id, $region, $area, $location)
    {
        $result     = array();
        $company_id = $company_id;
        if ($filter == 'today') {
            $today  = date("Y-m-d");
            $today1 = date("Y-m-d H:i:s");
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $today
                );
            }
            $today = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'week') {
            $today1 = date("Y-m-d H:i:s");
            
            $first_day_of_the_week = 'Monday';
            $week                  = date('Y-m-d', strtotime("Last $first_day_of_the_week"));
            if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
                $week = date('Y-m-d', strtotime('today'));
            }
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            }
            $today = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'month') {
            $today1 = date("Y-m-d H:i:s");
            $week   = date("Y-m-01 00:00:00");
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            }
            $today = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today1        = date("Y-m-d H:i:s");
            $current_month = date('M'); //$current_month='Apr';
            if ($current_month == 'Jan' || $current_month == 'Feb' || $current_month == 'Mar') {
                $week = date('Y-04-01', strtotime("-1 year")); //April 1st calculation
            } else {
                $week = date('Y-04-01', strtotime("today")); //April 1st calculation
            }
            if ($product_id == '' && $region == '' && $area == '' && $location == '' || $product_id == 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location == '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area == '' && $location == '' || $product_id != 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region != '' && $area != '' && $location == '' || $product_id != 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location == '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location == '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location == '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area != '' && $location != '' || $product_id == 'all' && $region != 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region != '' && $area == '' && $location != '' || $product_id == 'all' && $region != 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.region' => $region,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location == '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location == 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area != '' && $location != '' || $product_id == 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area != '' && $location != '' || $product_id != 'all' && $region == 'all' && $area != 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.town' => $area,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id == '' && $region == '' && $area == '' && $location != '' || $product_id == 'all' && $region == 'all' && $area == 'al' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else if ($product_id != '' && $region == '' && $area == '' && $location != '' || $product_id != 'all' && $region == 'all' && $area == 'all' && $location != 'all') {
                $where = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            } else {
                $today  = date("Y-m-d 00:00:00");
                $today1 = date("Y-m-d H:i:s");
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.product_id' => $product_id,
                    'all_tickets.location' => $location,
                    'all_tickets.region' => $region,
                    'all_tickets.town' => $area,
                    'all_tickets.last_update<=' => $today1,
                    'all_tickets.last_update>=' => $week
                );
            }
            $today = date("Y-m-d");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
            $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
            $this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    public function get_count($datas1)
    {
        $where = array(
            "date" => date("Y/m/d"),
            'attendance.company_id' => $datas1,
          //  'technician.region' => $region,
          //  'technician.area' => $area
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('attendance');
        $this->db->join('technician','technician.technician_id=attendance.technician_id');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function total_tickets($datas)
    {
        $company_id = $datas;
		$where = array(
                    'all_tickets.company_id' => $company_id,
            //        'all_tickets.region' => $region,
            //        'all_tickets.town' => $area
                );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where);	
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function escallated($datas)
    {
        $company_id = $datas;
        $where      = array(
            "all_tickets.current_status" => 6,
            'all_tickets.company_id' => $company_id,
           //         'all_tickets.region' => $region,
           //         'all_tickets.town' => $area
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function completed($datas)
    {
        $company_id = $datas;
        $where      = array(
            "all_tickets.current_status" => 7,
            'all_tickets.company_id' => $company_id,
             //       'all_tickets.region' => $region,
             //       'all_tickets.town' => $area
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function overall_claims($datas)
    {
        $company_id = $datas;
        $where      = array(
            "reimbursement_request.action" => 0,
            "reimbursement_request.status" => 13,
            "reimbursement_request.company_id" => $company_id
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function accept_spare_request($company_id, $filter)
    {
        $result     = array();
        $company_id = $company_id;
        $json       = array();
        $this->db->distinct();
        if ($filter == 'today') {
            $today      = date("Y-m-d");
            $today1     = date("Y-m-d");
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 11,
                "all_tickets.last_update<=" => $today,
                "all_tickets.last_update>=" => $today1
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'week') {
            $today      = date("Y-m-d");
            $week       = date("Y-m-d", strtotime("-1 week"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 11,
                "all_tickets.last_update>=" => $week
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == '' || $filter == 'month') {
            $today      = date("Y-m-d");
            $month      = date("Y-m-d", strtotime("-1 month"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 11,
                "all_tickets.last_update>=" => $month
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today      = date("Y-m-d");
            $year       = date("Y-m-d", strtotime("-1 year"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 11,
                "all_tickets.last_update>=" => $year
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'all') {
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 11
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    
    public function reject_spare_request($company_id, $filter)
    {
        $result     = array();
        $company_id = $company_id;
        $json       = array();
        $this->db->distinct();
        if ($filter == 'today') {
            $today      = date("Y-m-d");
            $today1     = date("Y-m-d");
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 17,
                "all_tickets.last_update<=" => $today,
                "all_tickets.last_update>=" => $today1
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'week') {
            $today      = date("Y-m-d");
            $week       = date("Y-m-d", strtotime("-1 week"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 17,
                "all_tickets.last_update>=" => $week
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == '' || $filter == 'month') {
            $today      = date("Y-m-d");
            $month      = date("Y-m-d", strtotime("-1 month"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 17,
                "all_tickets.last_update>=" => $month
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today      = date("Y-m-d");
            $year       = date("Y-m-d", strtotime("-1 year"));
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 17,
                "all_tickets.last_update>=" => $year
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'all') {
            $company_id = $company_id;
            $where      = array(
                "all_tickets.company_id" => $company_id,
                "all_tickets.current_status" => 17
            );
            
            $this->db->from('all_tickets');
            $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
            $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
            $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
            $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
            $this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
            $this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    
    public function Count_accept($company_id)
    {
        $company_id = $company_id;
        $where      = array(
            "reimbursement_request.action" => 1,
            "reimbursement_request.status" => 13,
            "reimbursement_request.company_id" => $company_id
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function Count_reject($datas)
    {
        $company_id = $datas;
        $where      = array(
            "reimbursement_request.action" => 2,
            "reimbursement_request.status" => 13,
            "reimbursement_request.company_id" => $company_id
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function service_call($data, $call)
    {
        $where = array(
            "call_tag.call=" => $call
        );
        //data is retrive from this query 
        $this->db->select('*');
        $this->db->from('call_tag');
        $this->db->where("call_tag.call=", $call);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $this->db->insert('call_tag', $data);
        } else {
            $this->db->where('call', $call);
            $this->db->update('call_tag', $data);
        }
        return true;
    }
    public function rejected($company_id)
    {
        
        $this->db->group_by('reimbursement_request.technician_id');
        $where = array(
            "reimbursement_request.action" => 2,
            "reimbursement_request.status" => 13,
            "reimbursement_request.company_id" => $company_id
        );
        
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
        //$this->db->join("all_tickets", "all_tickets.ticket_id=reimbursement_request.ticket_id");
        $this->db->join("product_management", "technician.product=product_management.product_id");
        $this->db->join("category_details", "technician.category=category_details.cat_id");
        $this->db->where($where);
        $this->db->order_by('reimbursement_request.id','desc');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function accepted($company_id)
    {
        
       $this->db->group_by('reimbursement_request.id');
        $where = array(
            "reimbursement_request.action" => 1,
            "reimbursement_request.status" => 13,
            "reimbursement_request.company_id" => $company_id
        );
        
        $this->db->select('*');
        $this->db->from('reimbursement_request');
        $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
        $this->db->join("product_management", "technician.product=product_management.product_id");
        $this->db->join("category_details", "technician.category=category_details.cat_id");
        $this->db->where($where);
        $this->db->order_by('reimbursement_request.id','desc');
        $query  = $this->db->get();
        $result = $query->result_array();

        return $result;
        
    }
    public function reimb_accept($company_id, $filter)
    {
        $result     = array();
        $company_id = $company_id;
        if ($filter == 'today') {
            $today  = date("Y-m-d H:i:s");
            $today1 = date("Y-m-d 00:00:00");
            
            $where = array(
                "reimbursement_request.action" => 1,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update<=" => $today,
                "reimbursement_request.last_update>=" => $today1
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'week') {
            $today = date("Y-m-d H:i:s");
            $week  = date("Y-m-d 00:00:00", strtotime("-1 week"));
            
            $where = array(
                "reimbursement_request.action" => 1,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $week
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == '' || $filter == 'month') {
            $today = date("Y-m-d H:i:s");
            $month = date("Y-m-d 00:00:00", strtotime("-1 month"));
            
            $where = array(
                "reimbursement_request.action" => 1,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $month
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today = date("Y-m-d H:i:s");
            $year  = date("Y-m-d 00:00:00", strtotime("-1 year"));
            
            $where = array(
                "reimbursement_request.action" => 1,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $year
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'all') {
            $where = array(
                "reimbursement_request.action" => 1,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    
    public function reimb_reject($company_id, $filter)
    {
        $result     = array();
        $company_id = $company_id;
        if ($filter == 'today') {
            $today      = date("Y-m-d H:i:s");
            $today1     = date("Y-m-d 00:00:00");
            $company_id = $company_id;
            $where      = array(
                "reimbursement_request.action" => 2,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update<=" => $today,
                "reimbursement_request.last_update>=" => $today1
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'week') {
            $today      = date("Y-m-d H:i:s");
            $week       = date("Y-m-d 00:00:00", strtotime("-1 week"));
            $company_id = $company_id;
            $where      = array(
                "reimbursement_request.action" => 2,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $week
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == '' || $filter == 'month') {
            $today      = date("Y-m-d H:i:s");
            $month      = date("Y-m-d 00:00:00", strtotime("-1 month"));
            $company_id = $company_id;
            $where      = array(
                "reimbursement_request.action" => 2,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $month
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'year') {
            $today      = date("Y-m-d H:i:s");
            $year       = date("Y-m-d 00:00:00", strtotime("-1 year"));
            $company_id = $company_id;
            $where      = array(
                "reimbursement_request.action" => 2,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id,
                "reimbursement_request.last_update>=" => $year
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'all') {
            $company_id = $company_id;
            $where      = array(
                "reimbursement_request.action" => 2,
                "reimbursement_request.status" => 13,
                "reimbursement_request.company_id" => $company_id
            );
            
            $this->db->select('*');
            $this->db->from('reimbursement_request');
            $this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
            $this->db->join("product_management", "technician.product=product_management.product_id");
            $this->db->join("category_details", "technician.category=category_details.cat_id");
            $this->db->where($where);
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        echo json_encode($result);
    }
    public function view($status, $datas)
    {
        
        $this->db->select('view');
        $this->db->from('reimbursement');
        //$this->db->where($where);
        $this->db->where('id', $status);
        $this->db->where('company_id', $datas);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return NULL;
        }
    }
    public function viewcliams($status)
    {
        
        $this->db->select('view');
        $this->db->from('reimbursement_request');
        //$this->db->where($where);
        $this->db->where('ticket_id', $status);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return NULL;
        }
    }
    public function statuschange($data, $where)
    {
        
        $this->db->where($where);
        $this->db->update('all_tickets', $data);
        return true;
    }
    public function claimchange($data, $where)
    {
        $this->db->where($where);
        $this->db->update('reimbursement_request', $data);
        return true;
    }
    public function impressed_accept_status($data, $where)
    {
        $this->db->where($where);
        $this->db->update('personal_spare', $data);
        return true;
    }
    public function transfer_accept($where,$data)
    {
        $this->db->where($where);
        $this->db->update('spare_transfer', $data);
        return true;
    }
    public function impressed_accept($where,$tech_id,$cid)
    {
        $this->db->select('spare_array');
        $this->db->from('personal_spare');
        $this->db->where($where);
        $query=$this->db->get();
        $result = $query->result_array();
		foreach($result as $res1)
		{

			$spare_array=json_decode($res1['spare_array'],true);	
		foreach($spare_array as $res)
		{
			$spare_code=$res['Spare_code'];
			$quantity=$res['quantity'];
			$this->db->select('*');
			$this->db->from('spare');
			$this->db->where('spare_code',$spare_code);
			$this->db->where('spare_source','Local_warehouse');
			$this->db->where('tech_id',$tech_id);
			$this->db->where('company_id',$cid);
			$query  = $this->db->get();
		 $count = $query->num_rows();
		 $count1 = $query->row_array();
			if($count>0)
			{
				$quanty=$count1['quantity_purchased']+$quantity;
				$spare_where=array("spare_code"=>$spare_code,"tech_id"=>$tech_id);
				$data=array("quantity_purchased"=>$quanty);
				$this->db->where($spare_where);
				$this->db->where('spare_source','Local_warehouse');
				$this->db->where('company_id',$cid);
				$this->db->update('spare', $data);
			}
			else{
				$this->db->select('*');
				$this->db->from('spare');
				$this->db->where('spare_code',$spare_code);
				$this->db->where('company_id',$cid);
				$this->db->where('spare_source','Local_warehouse');
				$query  = $this->db->get();
				$result_array = $query->result_array();
				foreach($result_array as $resu)
				{
					$this->db->select('*');
					$this->db->from('spare');
					$this->db->order_by("id", "desc");
					$query = $this->db->get();
					$id = $query->result_array();
					if(!empty($id)){
						$datass=$id[0]['id'];
					}else{
						$datass=0;
					}
					$datass = intval(preg_replace('/[^0-9]+/', '', $datass), 10);
					$datass = $datass + 1;
					$datass = str_pad($datass, 4, "0", STR_PAD_LEFT);
					$spare_id = "Spare_" . $datass;
					$data1            = array(
					'spare_id' => $spare_id,
					'spare_code' => $spare_code,
					'spare_name' => $resu['spare_name'],
					'spare_modal' =>  $resu['spare_modal'],
					'spare_desc' => $resu['spare_desc'],
					'spare_source' =>'Local_warehouse',
					'price' => $resu['price'],
					'p_date' => $resu['p_date'],
					'ep_date' => $resu['ep_date'],
					'quantity_purchased' => $quantity,
					'product' => $resu['product'],
					'category' => $resu['category'],
					'tech_id' => $tech_id,
					'quantity' => $quantity,
					'company_id' => $cid
				);
				$this->db->insert('spare', $data1);
				}
			}
			
				$this->db->select('*');
				$this->db->from('spare');
				$this->db->where('spare_code',$spare_code);
				$this->db->where('company_id',$cid);
				$this->db->where('spare_source','Centralised_warehouse');
				$query1  = $this->db->get();
				$result_array1 = $query1->row_array();
				$quan=$result_array1['quantity_purchased']-$quantity;
				$dt=array("quantity_purchased"=>$quan);
				$this->db->where('company_id',$cid);
				$this->db->where('spare_source','Centralised_warehouse');
				$this->db->update('spare', $dt);
				return true;
		}
		}
    }
    public function average_call($company_id)
    {
        $json   = array();
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        //data is retrive from this query 
        $where2 = array(
            "technician.company_id" => $company_id,
     //       "technician.region" => $region,
     //       "technician.area" => $area
        );
        $this->db->select('technician_id,employee_id,skill_level');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        foreach ($result as $row) {
            $tech_id = $row['technician_id'];
            $where   = array(
                "all_tickets.tech_id" => $tech_id,
                "all_tickets.current_status" => 12,
                "all_tickets.company_id" => $company_id,
                'all_tickets.last_update<=' => $today,
                'all_tickets.last_update>=' => $month
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where);
            $this->db->like('all_tickets.last_update', $today);
            $que  = $this->db->get();
            $resu = $que->num_rows();
            $this->db->select('*');
            $this->db->from('score_update');
            $where1 = array(
                "score_update.company_id" => $company_id,
                "score_update.technician_level" => $row['skill_level']
            );
            $this->db->where($where1);
            $qu = $this->db->get();
            $re = $qu->row_array();
            $this->db->select('*');
            $this->db->from('score_reward');
            $where1 = array(
                "score_reward.company_id" => $company_id,
                "score_reward.technician_level" => $row['skill_level']
            );
            $this->db->where($where1);
            $qu1 = $this->db->get();
            $re1 = $qu1->row_array();
            if ($re['average_calls'] <= $resu) {
                
                array_push($json, array(
                    $tech_id => $re1['average_calls']
                ));
            } else {
                array_push($json, array(
                    $tech_id => 0
                ));
            }
        }
        return json_encode($json);
    }
    public function technician($company_id)
    {
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $where2 = array(
            'technician.company_id' => $company_id  ,
			//'technician.region' => $region  ,
		//	'technician.area' => $area
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        $count  = $query->num_rows();
        $json   = array();
        foreach ($result as $row) {
            $wher  = array(
                'all_tickets.tech_id' => $row['technician_id'],
                'all_tickets.company_id' => $company_id,
                'all_tickets.last_update<=' => $today,
                'all_tickets.last_update>=' => $month
            );
            $today = date('Y-m', strtotime('first day of last month'));
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($wher);
            //$this->db->like('all_tickets.last_update', $today);
            $query1 = $this->db->get();
            $count  = $query1->num_rows();
            array_push($json, array(
                'tech_id' => $row['technician_id'],
                'tech_name' => $row['first_name'],
                'skill_level' => $row['skill_level'],
                'ticket_count' => $count
            ));
        }
        return json_encode($json);
    }
    public function first_time_fix($company_id)
    {
        $tech_id = 0;
        $json    = array();
        $today   = date("Y-m-d H:i:s");
        $month   = date("Y-m-d 00:00:00", strtotime("-1 month"));
        //$today = date('Y-m', strtotime('first day of last month'));
        //data is retrive from this query 
        $where2  = array(
            "technician.company_id" => $company_id,
         //   "technician.region" => $region,
        //    "technician.area" => $area
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        $sr     = array();
        $se     = '';
        if (!empty($result)) {
            foreach ($result as $row) {
                $tech_id = $row['technician_id'];
                $where   = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.previous_status !=" => 15,
                    "all_tickets.current_status" => 12,
                    "all_tickets.company_id" => $company_id,
                    'all_tickets.last_update<=' => $today,
                    'all_tickets.last_update>=' => $month
                );
                $where2  = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.current_status" => 12,
                    "all_tickets.company_id" => $company_id,
                    'all_tickets.last_update<=' => $today,
                    'all_tickets.last_update>=' => $month
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($where);
                // $this->db->like('all_tickets.last_update', $today);
                $que = $this->db->get(); //attained
                $this->db->select('first_time');
                $this->db->from('score_update');
                $where1 = array(
                    "score_update.company_id" => $company_id,
                    "score_update.technician_level" => $row['skill_level']
                );
                $this->db->where($where1);
                $qu     = $this->db->get();
                $re     = $qu->row_array();
                $re     = $re['first_time']; //target
                $resu   = $que->num_rows();
                $where1 = array(
                    "all_tickets.tech_id" => $tech_id
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($where2);
                $quer    = $this->db->get();
                $result1 = $quer->num_rows(); //overall count
                //$se      = 0;
                if ($resu != 0 && $result1 != 0) {
                    $where1 = array(
                        "score_reward.company_id" => $company_id,
                        "score_reward.technician_level" => $row['skill_level']
                    );
                    $this->db->select('first_time');
                    $this->db->from('score_reward');
                    $this->db->where($where1);
                    $qu33           = $this->db->get();
                    $re33          = $qu33->row_array();
                    $re1          = $re['first_time']; 
                    $estimated          = $re33['first_time']; //reward
                    //$actual_value = (intval($resu) / intval($result1));
                    $actual=((int)$re/(int)$result1)*100;
                    if ($actual <= $estimated) {
                        
                        //$se = ($actual_value * intval($re1));
                        //$avg     = ($rating / $tot_tic) * $re[0]['customer_rating'];
                        array_push($sr, array(
                            $tech_id => $re1
                        ));
                    } else {
                        array_push($sr, array(
                            $tech_id => 0
                        ));
                    }
                } else {
                    array_push($sr, array(
                        $tech_id => 0
                    ));
                }
            }
        } else {
            array_push($sr, array(
                $tech_id => 0
            ));
        }
        return json_encode($sr);
    }
    
    
     public function Customer_Rating($company_id)
    {
        $json   = array();
        $today  = date('Y-m-d', strtotime('last day of previous month'));
        $today1 = date("Y-m-d 00:00:00");
        $month  = date("Y-m-01 00:00:00", strtotime("-1 month"));
        //data is retrive from this query 
        $where2 = array(
            "technician.company_id" => $company_id,
          //  "technician.region" => $region,
         //   "technician.area" => $area
        );
        $this->db->select('technician_id,employee_id,skill_level');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $tech_id     = $row['technician_id'];
                $skill_level = $row['skill_level'];
                $where       = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.current_status" => 12,
                    "all_tickets.company_id" => $company_id,
                    'all_tickets.last_update<=' => $today,
                    'all_tickets.last_update>=' => $month
                );
                $this->db->select('cust_rating');
                $this->db->from('all_tickets');
                $this->db->where($where);
                // $this->db->like('all_tickets.last_update', $today);
                $que  = $this->db->get();
                $resu = $que->result_array();
                $this->db->select('*');
                $this->db->from('score_update');
                $where1 = array(
                    "score_update.company_id" => $company_id,
                    "score_update.technician_level" => $skill_level
                );
                $this->db->where($where1);
                $qu     = $this->db->get();
                $re     = $qu->result_array();
                $rating = 0;
                if (!empty($resu)) {
                    foreach ($resu as $row1) {
                        $rating = $rating + $row1['cust_rating'];
                    }
                    $tot_tic = $que->num_rows();
                    $rat     = ($rating / $tot_tic);
                    $this->db->select('*');
                    $this->db->from('score_reward');
                    $where1 = array(
                        "score_reward.company_id" => $company_id,
                        "score_reward.technician_level" => $skill_level
                    );
                    $this->db->where($where1);
                    $qu1 = $this->db->get();
                    $re1 = $qu1->result_array();
                    if ($re[0]['customer_rating'] <= $rat) {
                        $avg = $rat * $re1[0]['customer_rating'];
                        array_push($json, array(
                            $tech_id => $re1[0]['customer_rating']
                        ));
                    } else {
                        array_push($json, array(
                            $tech_id => 0
                        ));
                    }
                } else {
                    array_push($json, array(
                        $tech_id => 0
                    ));
                }
            }
        } else {
            array_push($json, array(
                "status" => "No data"
            ));
        }
        return json_encode($json);
    }
 
    
     public function Knowledge($company_id)
    {
        $json   = array();
        $today  = date('Y-m', strtotime('first day of last month'));
        //data is retrive from this query 
        $where2 = array(
            "technician.company_id" => $company_id,
      //      "technician.region" => $region,
      //      "technician.area" => $area
        );
        $this->db->select('*');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        foreach ($result as $row) {
            $tech_id = $row['technician_id'];
            $where   = array(
                "knowledge_base.technician_id" => $tech_id,
                "knowledge_base.Status" => 1,
                "knowledge_base.company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('knowledge_base');
            $this->db->where($where);
            $this->db->like('knowledge_base.last_update', $today);
            $que = $this->db->get();
            $this->db->select('*');
            $this->db->from('score_update');
            $where1 = array(
                "score_update.company_id" => $company_id,
                "score_update.technician_level" => $row['skill_level']
            );
            $this->db->where($where1);
            $qu = $this->db->get();
            $re = $qu->row_array();
            $this->db->select('*');
            $this->db->from('score_update');
            $where1 = array(
                "score_update.company_id" => $company_id,
                "score_update.technician_level" => $row['skill_level']
            );
            $this->db->where($where1);
            $qu1  = $this->db->get();
            $re1  = $qu1->row_array();
            $resu = $que->num_rows();
            if ($resu >= $re['contribution_to_kb']) {
                $avg = $re['contribution_to_kb'];
                array_push($json, array(
                    $tech_id => $re1['contribution_to_kb']
                ));
            } else {
                array_push($json, array(
                    $tech_id => 0
                ));
            }
            
        }
        return json_encode($json);
        
    }
   
    
     public function Training($company_id)
    {
        $json  = array();
        $today = date('Y-m', strtotime('first day of last month'));
        //data is retrive from this query 
        $where = array(
            "technician.company_id" => $company_id,
       //     "technician.region" => $region,
       //     "technician.area" => $area
        );
        $this->db->select('technician_id,skill_level');
        $this->db->from('technician');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $tech_id = $row['technician_id'];
                $where2  = array(
                    "training_score.tech_id" => $tech_id,
                    "training_score.company_id" => $company_id
                );
                $this->db->select('score');
                $this->db->from('training_score');
                $this->db->where($where2);
                $this->db->like('training_score.last_update', $today);
                $que  = $this->db->get();
                $resu = $que->result_array();
                $this->db->select('*');
                $this->db->from('score_update');
                $where1 = array(
                    "score_update.company_id" => $company_id,
                    "score_update.technician_level" => $row['skill_level']
                );
                $this->db->where($where1);
                $qu = $this->db->get();
                $re = $qu->result_array();
                $this->db->from('score_reward');
                $where1 = array(
                    "score_reward.company_id" => $company_id,
                    "score_reward.technician_level" => $row['skill_level']
                );
                $this->db->where($where1);
                $qu1    = $this->db->get();
                $re1    = $qu1->result_array();
                $rating = 0;
                if (!empty($resu)) {
                    foreach ($resu as $row1) {
                        $rating = $rating + $row1['score'];
                    }
                    $tot_tic = $que->num_rows();
                    $avg     = ($rating / $tot_tic);
                    if ($tot_tic >= $re[0]['training_&_certification']) {
                        
                        array_push($json, array(
                            $tech_id => $re1[0]['training_&_certification']
                        ));
                    } else {
                        array_push($json, array(
                            $tech_id => 0
                        ));
                    }
                } else {
                    array_push($json, array(
                        $tech_id => 0
                    ));
                    
                }
            }
        } else {
            array_push($json, array(
                "status" => "No data"
            ));
            
        }
        return json_encode($json);
    }
   
    public function contract($company_id)
    {
        $json   = array();
        $today  = date('Y-m', strtotime('first day of last month'));
        //data is retrive from this query 
        $where2 = array(
            "technician.company_id" => $company_id,
          //  "technician.region" => $region,
         //   "technician.area" => $area
        );
        $this->db->select('technician_id,employee_id,skill_level');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        
        foreach ($result as $row) {
            $tech_id = $row['technician_id'];
            $where   = array(
                "all_tickets.tech_id" => $tech_id,
                "amc_id!=" => ""
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where);
            $this->db->like('all_tickets.last_update', $today);
            $que = $this->db->get();
            $this->db->select('*');
            $this->db->from('score_update');
            $where1 = array(
                "score_update.company_id" => $company_id,
                "score_update.technician_level" => $row['skill_level']
            );
            $this->db->where($where1);
            $qu   = $this->db->get();
            $re   = $qu->row_array();
            $resu = $que->num_rows();
            if ($resu >= $re['contract_revenue']) {
                $this->db->select('*');
                $this->db->from('score_reward');
                $where1 = array(
                    "score_reward.company_id" => $company_id,
                    "score_reward.technician_level" => $row['skill_level']
                );
                $this->db->where($where1);
                $qu1 = $this->db->get();
                $re1 = $qu1->row_array();
                $avg = $re1['contract_revenue'];
                array_push($json, array(
                    $tech_id => $avg
                ));
            } else {
                array_push($json, array(
                    $tech_id => 0
                ));
            }
            
        }
        return json_encode($json);
        
    }
  
   public function mttr($company_id)
    {
        $json       = array();
        $today      = date('Y-m', strtotime('first day of last month'));
        $count_mttr = 0;
        $this->db->select('*');
        $this->db->from('call_tag');
        $where2 = array(
            "call_tag.company_id" => $company_id
        );
        $this->db->where($where2);
        $qu2 = $this->db->get();
        $qu2 = $qu2->result_array();
		if(!empty($qu2))
		{
			
        foreach ($qu2 as $qu2_res) {
            //data is retrive from this query 
            $where2 = array(
                "technician.company_id" => $company_id,
        //        "technician.region" => $region,
         //       "technician.area" => $area
            );
            $this->db->select('technician_id,skill_level');
            $this->db->from('technician');
            $this->db->where($where2);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            foreach ($result as $row) {
                $tech_id  = $row['technician_id'];
                $call_tag = $qu2_res['call'];
                $where    = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.call_tag" => $call_tag
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($where);
                $this->db->like('all_tickets.last_update', $today);
                $que  = $this->db->get();
                $resu = $que->result_array();
				if(!empty($resu))
				{
                foreach ($resu as $resu_row) {
                    $strStart = $resu_row['ticket_start_time'];
                    $strEnd   = $resu_row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $actual_mttr = $dteDiff->format("%H:%I:%S");
                    $target_mttr = new DateTime($qu2_res['mttr']);
                    if ($actual_mttr >= $target_mttr) {
                        $count_mttr = $count_mttr + 1;
                    }
                }
                $this->db->select('*');
                $this->db->from('score_update');
                $where1 = array(
                    "score_update.company_id" => $company_id,
                    "score_update.technician_level" => $row['skill_level']
                );
                $this->db->where($where1);
                $qu = $this->db->get();
                $re = $qu->result_array();
                $percent_count_mttr=($count_mttr/$total)*100;
                if ($count_mttr >= $re[0]['mttr']) {
                    $this->db->select('*');
                    $this->db->from('score_reward');
                    $where1 = array(
                        "score_reward.company_id" => $company_id,
                        "score_reward.technician_level" => $row['skill_level']
                    );
                    $this->db->where($where1);
                    $qu1 = $this->db->get();
                    $re1 = $qu1->result_array();
                    $avg = $re1[0]['mttr'];
                    array_push($json, array(
                        $tech_id => $avg
                    ));
                } else {
                    array_push($json, array(
                        $tech_id => 0
                    ));
                }
                
            }
			else
			{
				array_push($json, array(
                        $tech_id => 0
                    ));
			}
			}
        }
        
		}
		else
		{
			$where2 = array(
                "technician.company_id" => $company_id,
        //        "technician.region" => $region,
         //       "technician.area" => $area
            );
            $this->db->select('technician_id,skill_level');
            $this->db->from('technician');
            $this->db->where($where2);
            $query  = $this->db->get();
            $result = $query->result_array();
            
            foreach ($result as $row) {
                $tech_id  = $row['technician_id'];
				array_push($json, array(
                        $tech_id => 0
                    ));
			}
		}
        return json_encode($json);
        
    }
    
   public function Customer_feedback($company_id)
    {
        $json   = array();
        $today  = date('Y-m-d', strtotime('last day of previous month'));
        $today1 = date("Y-m-d 00:00:00");
        $month  = date("Y-m-01 00:00:00", strtotime("-1 month"));
        //data is retrive from this query 
        $where2 = array(
            "technician.company_id" => $company_id,
         //   "technician.region" => $region,
        //    "technician.area" => $area
        );
        $this->db->select('technician_id,skill_level');
        $this->db->from('technician');
        $this->db->where($where2);
        $query  = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $tech_id     = $row['technician_id'];
                $skill_level = $row['skill_level'];
                $where       = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.current_status" => 12,
                    "all_tickets.company_id" => $company_id,
                    'all_tickets.last_update<=' => $today,
                    'all_tickets.cust_rating!=' => '',
                    'all_tickets.last_update>=' => $month
                );
                $this->db->select('cust_rating');
                $this->db->from('all_tickets');
                $this->db->where($where);
                // $this->db->like('all_tickets.last_update', $today);
                $que    = $this->db->get();
                $resu   = $que->result_array();
                $where1 = array(
                    "all_tickets.tech_id" => $tech_id,
                    "all_tickets.current_status" => 12,
                    "all_tickets.company_id" => $company_id,
                    'all_tickets.last_update<=' => $today,
                    'all_tickets.last_update>=' => $month
                );
                $this->db->select('cust_rating');
                $this->db->from('all_tickets');
                $this->db->where($where1);
                // $this->db->like('all_tickets.last_update', $today);
                $que1  = $this->db->get();
                $resu1 = $que1->result_array();
                $this->db->select('*');
                $this->db->from('score_update');
                $where1 = array(
                    "score_update.company_id" => $company_id,
                    "score_update.technician_level" => $skill_level
                );
                $this->db->where($where1);
                $qu     = $this->db->get();
                $re     = $qu->result_array();
                $rating = 0;
                if (!empty($resu)) {
                    $rating  = $que->num_rows();
                    $tot_tic = $que1->num_rows();
                    $rat     = ($rating / $tot_tic)*100;
                    $this->db->select('*');
                    $this->db->from('score_reward');
                    $where1 = array(
                        "score_reward.company_id" => $company_id,
                        "score_reward.technician_level" => $skill_level
                    );
                    $this->db->where($where1);
                    $qu1 = $this->db->get();
                    $re1 = $qu1->result_array();
                    if ($re[0]['cust_feed'] <= $rat) {
                        //$avg = $rat * $re1[0]['cust_feed'];
                        array_push($json, array(
                            $tech_id => $re1[0]['cust_feed']
                        ));
                    } else {
                        array_push($json, array(
                            $tech_id => 0
                        ));
                    }
                } else {
                    array_push($json, array(
                        $tech_id => 0
                    ));
                }
            }
        } else {
            array_push($json, array(
                "status" => "No data"
            ));
        }
        return json_encode($json);
    }
    
        function daysDifference($company_id)
    {
        $json  = array();
        $today = date('Y-m', strtotime('first day of last month'));
        //data is retrive from this query 
        $where = array(
            "technician.company_id" => $company_id,
             //   "technician.region" =>$region,
             //   "technician.area" => $area
        );
        $this->db->select('technician_id');
        $this->db->from('technician');
        $this->db->where($where);
        $query = $this->db->get();
        $resu  = $query->result_array();
        //$this->db->where($where);
        $ar    = array();
        $st    = array();
        foreach ($resu as $row1) {
            $tech_id = $row1['technician_id'];
            $where1  = array(
                "all_tickets.tech_id" => $tech_id,
                "all_tickets.current_status" => 12,
                "all_tickets.company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where1);
            $this->db->like('all_tickets.last_update', $today);
            $query1 = $this->db->get();
            $result = $query1->result_array();
            if (!empty($result)) {
                $overall_tickets = $query1->num_rows();
                $ar1             = array();
                $ar2             = array();
                
                foreach ($result as $row) {
                    $strStart = $row['ticket_start_time'];
                    $strEnd   = $row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $d = $dteDiff->format("%H:%I:%S");
                    array_push($ar, array(
                        "diff" => $d
                    ));
                    $where1 = array(
                        "all_tickets.tech_id" => $tech_id,
                        "all_tickets.ticket_id" => $row['ticket_id']
                    );
                    $this->db->select('resolution_time');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->where($where1);
                    $this->db->group_by('all_tickets.ticket_id');
                    $query2  = $this->db->get();
                    $result2 = $query2->row_array();
                    
                    $resolution = $result2['resolution_time'];
                    if ($d <= $resolution) {
                        array_push($ar1, array(
                            $resolution
                        ));
                    }
                }
                $re = count($ar1) / $overall_tickets;
                array_push($st, array(
                    $tech_id => $re * 10
                ));
            } else {
                array_push($st, array(
                    $tech_id => 0
                ));
            }
        }
        return json_encode($st);
    }
    public function productivity_trends($company_id, $filter)
    {
        $res                = array();
        $res1               = array();
        $ar                 = array();
        $today              = date("Y-m-d H:i:s");
        $today1             = date("Y-m-d 00:00:00");
        $week               = date("Y-m-d 00:00:00", strtotime("-1 week"));
        $month              = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $year               = date("Y-m-d 00:00:00", strtotime("-1 year"));
        $where1             = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $week
        );
        $where2             = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $month
        );
        $where3             = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $year
        );
        $where              = array(
            'company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $today1
        );
        $resu               = array();
        $resu1              = array();
        $res                = array();
        $below12_array      = array();
        $eleven_count_array = array();
        $above3_count_array = array();
        if ($filter == '' || $filter == 'today') {
            $query1        = 0;
            $query2        = 0;
            $first_fix     = 0;
            $not_first_fix = 0;
            $sub_where     = array(
                "current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where);
            $this->db->where($where);
            $query1 = $this->db->get();
            $closed = $query1->num_rows();
            array_push($res, array(
                'ticket_status' => 'closed',
                'count' => $closed
            ));
            $sub_where1 = array(
                "current_status!=" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where1);
            $this->db->where($where);
            $query2         = $this->db->get();
            $not_yet_closed = $query2->num_rows();
            array_push($res, array(
                'ticket_status' => 'not_yet_closed',
                'count' => $not_yet_closed
            ));
            $sub_where2 = array(
                "all_tickets.previous_status !=" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where2);
            $this->db->where($where);
            $query3    = $this->db->get();
            $first_fix = $query3->num_rows();
            array_push($res, array(
                'label' => 'First Fix Rate',
                'first_fix' => $first_fix
            ));
            $sub_where3 = array(
                "all_tickets.previous_status" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where3);
            $this->db->where($where);
            $query4        = $this->db->get();
            $not_first_fix = $query4->num_rows();
            array_push($res, array(
                'label' => 'Remaining Tickets',
                'not_first_fix' => $not_first_fix
            ));
            $sub_where4 = array(
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where);
            $this->db->where($sub_where4);
            $query5  = $this->db->get();
            $result5 = $query5->result_array();
            if (!empty($result5)) {
                $overall_tickets = $query5->num_rows();
                $ar1             = array();
                $ar2             = array();
                
                foreach ($result5 as $row) {
                    $strStart = $row['ticket_start_time'];
                    $strEnd   = $row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $d = $dteDiff->format("%H:%I:%S");
                    array_push($ar, array(
                        "diff" => $d
                    ));
                    $where5 = array(
                        "all_tickets.ticket_id" => $row['ticket_id']
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->where($where5);
                    $this->db->where($where3);
                    $query6  = $this->db->get();
                    $result6 = $query6->result_array();
                    if (!empty($result6)) {
                        $resolution = $result6[0]['resolution_time'];
                        if ($d <= $resolution) {
                            array_push($ar1, array(
                                $resolution
                            ));
                        } else {
                            array_push($ar2, array(
                                $resolution
                            ));
                        }
                    }
                }
                $sla_complaince     = count($ar1);
                $not_sla_complaince = count($ar2);
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            } else {
                $sla_complaince     = 0;
                $not_sla_complaince = 0;
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            }
            $sub_where10 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('amc_type');
            $this->db->where($sub_where10);
            $query10 = $this->db->get();
            $amc     = $query10->result_array();
            foreach ($amc as $a) {
                $sub_where7 = array(
                    "current_status!=" => '14',
                    "requested_spare!=" => '',
                    "amc_type" => $a['amc_type']
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where7);
                $this->db->where($where);
                $query7   = $this->db->get();
                $warranty = $query7->num_rows();
                array_push($resu1, array(
                    'Spare_amctype' => $a['amc_type'],
                    'count' => $warranty
                ));
            }
            array_push($res, array(
                'result' => $resu1
            ));
            $sub_where11 = array(
                "company_id" => $company_id,
                "requested_spare!=" => ''
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where11);
            $this->db->where($where);
            $query11    = $this->db->get();
            $prod_spare = $query11->result_array();
            foreach ($prod_spare as $p) {
                $sparecode = json_decode($p['requested_spare'], true);
                foreach ($sparecode as $spr) {
                    $sub_where8 = array(
                        "spare_code" => $spr['Spare_code'],
                        "spare.company_id" => $company_id
                    );
                    $this->db->select('product_name');
                    $this->db->from('spare');
                    $this->db->join('product_management', 'product_management.product_id=spare.product');
                    $this->db->where($sub_where8);
                    $query12 = $this->db->get();
                    $spar    = $query12->result_array();
                    $pr_id   = $spar[0]['product_name'];
                    array_push($res1, array(
                        'spare_product' => $pr_id
                    ));
                }
            }
            $sub_where9 = array(
                "company_id" => $company_id
            );
            $this->db->select('product_name');
            $this->db->from('product_management');
            $this->db->where($sub_where9);
            $query13  = $this->db->get();
            $pro_name = $query13->result_array();
            $counter  = 0;
            foreach ($pro_name as $prod) {
                $counter_array = array();
                foreach ($res1 as $re) {
                    if (in_array($prod['product_name'], $re)) {
                        array_push($counter_array, array(
                            'Spare_' . $prod['product_name'] => $prod['product_name']
                        ));
                    }
                }
                $counter = count($counter_array);
                array_push($resu, array(
                    'Spare_product' => $prod['product_name'],
                    'count' => $counter
                ));
            }
            array_push($res, array(
                'result' => $resu
            ));
        } else if ($filter == 'week') {
            $query1        = 0;
            $query2        = 0;
            $first_fix     = 0;
            $not_first_fix = 0;
            $sub_where     = array(
                "current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where);
            $this->db->where($where1);
            $query1 = $this->db->get();
            $closed = $query1->num_rows();
            array_push($res, array(
                'ticket_status' => 'closed',
                'count' => $closed
            ));
            $sub_where1 = array(
                "current_status!=" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where1);
            $this->db->where($where1);
            $query2         = $this->db->get();
            $not_yet_closed = $query2->num_rows();
            array_push($res, array(
                'ticket_status' => 'not_yet_closed',
                'count' => $not_yet_closed
            ));
            $sub_where2 = array(
                "all_tickets.previous_status !=" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where2);
            $this->db->where($where1);
            $query3    = $this->db->get();
            $first_fix = $query3->num_rows();
            array_push($res, array(
                'label' => 'First Fix Rate',
                'first_fix' => $first_fix
            ));
            $sub_where3 = array(
                "all_tickets.previous_status" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where3);
            $this->db->where($where1);
            $query4        = $this->db->get();
            $not_first_fix = $query4->num_rows();
            array_push($res, array(
                'label' => 'Remaining Tickets',
                'not_first_fix' => $not_first_fix
            ));
            $sub_where4 = array(
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where1);
            $this->db->where($sub_where4);
            $query5  = $this->db->get();
            $result5 = $query5->result_array();
            if (!empty($result5)) {
                $overall_tickets = $query5->num_rows();
                $ar1             = array();
                $ar2             = array();
                
                foreach ($result5 as $row) {
                    $strStart = $row['ticket_start_time'];
                    $strEnd   = $row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $d = $dteDiff->format("%H:%I:%S");
                    array_push($ar, array(
                        "diff" => $d
                    ));
                    $where5 = array(
                        "all_tickets.ticket_id" => $row['ticket_id']
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->where($where5);
                    $this->db->where($where3);
                    $query6  = $this->db->get();
                    $result6 = $query6->result_array();
                    
                    if (!empty($result6)) {
                        $resolution = $result6[0]['resolution_time'];
                        if ($d <= $resolution) {
                            array_push($ar1, array(
                                $resolution
                            ));
                        } else {
                            array_push($ar2, array(
                                $resolution
                            ));
                        }
                    }
                }
                $sla_complaince     = count($ar1);
                $not_sla_complaince = count($ar2);
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            } else {
                $sla_complaince     = 0;
                $not_sla_complaince = 0;
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            }
            $sub_where10 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('amc_type');
            $this->db->where($sub_where10);
            $query10 = $this->db->get();
            $amc     = $query10->result_array();
            foreach ($amc as $a) {
                $sub_where7 = array(
                    "current_status!=" => '14',
                    "requested_spare!=" => '',
                    "amc_type" => $a['amc_type']
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where7);
                $this->db->where($where1);
                $query7   = $this->db->get();
                $warranty = $query7->num_rows();
                array_push($resu1, array(
                    'Spare_amctype' => $a['amc_type'],
                    'count' => $warranty
                ));
            }
            array_push($res, array(
                'result' => $resu1
            ));
            $sub_where11 = array(
                "company_id" => $company_id,
                "requested_spare!=" => ''
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where11);
            $this->db->where($where1);
            $query11    = $this->db->get();
            $prod_spare = $query11->result_array();
            foreach ($prod_spare as $p) {
                $sparecode = json_decode($p['requested_spare'], true);
                foreach ($sparecode as $spr) {
                    $sub_where8 = array(
                        "spare_code" => $spr['Spare_code'],
                        "spare.company_id" => $company_id
                    );
                    $this->db->select('product_name');
                    $this->db->from('spare');
                    $this->db->join('product_management', 'product_management.product_id=spare.product');
                    $this->db->where($sub_where8);
                    $query12 = $this->db->get();
                    $spar    = $query12->result_array();
                    $pr_id   = $spar[0]['product_name'];
                    array_push($res1, array(
                        'spare_product' => $pr_id
                    ));
                }
            }
            $sub_where9 = array(
                "company_id" => $company_id
            );
            $this->db->select('product_name');
            $this->db->from('product_management');
            $this->db->where($sub_where9);
            $query13  = $this->db->get();
            $pro_name = $query13->result_array();
            $counter  = 0;
            foreach ($pro_name as $prod) {
                $counter_array = array();
                foreach ($res1 as $re) {
                    if (in_array($prod['product_name'], $re)) {
                        array_push($counter_array, array(
                            'Spare_' . $prod['product_name'] => $prod['product_name']
                        ));
                    }
                }
                $counter = count($counter_array);
                array_push($resu, array(
                    'Spare_product' => $prod['product_name'],
                    'count' => $counter
                ));
            }
            array_push($res, array(
                'result' => $resu
            ));
        } else if ($filter == 'month') {
            $query1        = 0;
            $query2        = 0;
            $first_fix     = 0;
            $not_first_fix = 0;
            $sub_where     = array(
                "current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where);
            $this->db->where($where2);
            $query1 = $this->db->get();
            $closed = $query1->num_rows();
            array_push($res, array(
                'ticket_status' => 'closed',
                'count' => $closed
            ));
            $sub_where1 = array(
                "current_status!=" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where1);
            $this->db->where($where2);
            $query2         = $this->db->get();
            $not_yet_closed = $query2->num_rows();
            array_push($res, array(
                'ticket_status' => 'not_yet_closed',
                'count' => $not_yet_closed
            ));
            $sub_where2 = array(
                "all_tickets.previous_status !=" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where2);
            $this->db->where($where2);
            $query3    = $this->db->get();
            $first_fix = $query3->num_rows();
            array_push($res, array(
                'label' => 'First Fix Rate',
                'first_fix' => $first_fix
            ));
            $sub_where3 = array(
                "all_tickets.previous_status" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where3);
            $this->db->where($where2);
            $query4        = $this->db->get();
            $not_first_fix = $query4->num_rows();
            array_push($res, array(
                'label' => 'Remaining Tickets',
                'not_first_fix' => $not_first_fix
            ));
            $sub_where4 = array(
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where2);
            $this->db->where($sub_where4);
            $query5  = $this->db->get();
            $result5 = $query5->result_array();
            if (!empty($result5)) {
                $overall_tickets = $query5->num_rows();
                $ar1             = array();
                $ar2             = array();
                
                foreach ($result5 as $row) {
                    $strStart = $row['ticket_start_time'];
                    $strEnd   = $row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $d = $dteDiff->format("%H:%I:%S");
                    array_push($ar, array(
                        "diff" => $d
                    ));
                    $where5 = array(
                        "all_tickets.ticket_id" => $row['ticket_id']
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->where($where5);
                    $this->db->where($where3);
                    $query6  = $this->db->get();
                    $result6 = $query6->result_array();
                    if (!empty($result6)) {
                        $resolution = $result6[0]['resolution_time'];
                        if ($d <= $resolution) {
                            array_push($ar1, array(
                                $resolution
                            ));
                        } else {
                            array_push($ar2, array(
                                $resolution
                            ));
                        }
                    }
                }
                $sla_complaince     = count($ar1);
                $not_sla_complaince = count($ar2);
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            } else {
                $sla_complaince     = 0;
                $not_sla_complaince = 0;
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            }
            $sub_where10 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('amc_type');
            $this->db->where($sub_where10);
            $query10 = $this->db->get();
            $amc     = $query10->result_array();
            foreach ($amc as $a) {
                $sub_where7 = array(
                    "current_status!=" => '14',
                    "requested_spare!=" => '',
                    "amc_type" => $a['amc_type']
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where7);
                $this->db->where($where2);
                $query7   = $this->db->get();
                $warranty = $query7->num_rows();
                array_push($resu1, array(
                    'Spare_amctype' => $a['amc_type'],
                    'count' => $warranty
                ));
            }
            array_push($res, array(
                'result' => $resu1
            ));
            $sub_where11 = array(
                "company_id" => $company_id,
                "requested_spare!=" => ''
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where11);
            $this->db->where($where2);
            $query11    = $this->db->get();
            $prod_spare = $query11->result_array();
            foreach ($prod_spare as $p) {
                $sparecode = json_decode($p['requested_spare'], true);
                foreach ($sparecode as $spr) {
                    $sub_where8 = array(
                        "spare_code" => $spr['Spare_code'],
                        "spare.company_id" => $company_id
                    );
                    $this->db->select('product_name');
                    $this->db->from('spare');
                    $this->db->join('product_management', 'product_management.product_id=spare.product');
                    $this->db->where($sub_where8);
                    $query12 = $this->db->get();
                    $spar    = $query12->result_array();
                    $pr_id   = $spar[0]['product_name'];
                    array_push($res1, array(
                        'spare_product' => $pr_id
                    ));
                }
            }
            $sub_where9 = array(
                "company_id" => $company_id
            );
            $this->db->select('product_name');
            $this->db->from('product_management');
            $this->db->where($sub_where9);
            $query13  = $this->db->get();
            $pro_name = $query13->result_array();
            $counter  = 0;
            foreach ($pro_name as $prod) {
                $counter_array = array();
                foreach ($res1 as $re) {
                    if (in_array($prod['product_name'], $re)) {
                        array_push($counter_array, array(
                            'Spare_' . $prod['product_name'] => $prod['product_name']
                        ));
                    }
                }
                $counter = count($counter_array);
                array_push($resu, array(
                    'Spare_product' => $prod['product_name'],
                    'count' => $counter
                ));
            }
            array_push($res, array(
                'result' => $resu
            ));
        } else if ($filter == 'year') {
            $query1        = 0;
            $query2        = 0;
            $first_fix     = 0;
            $not_first_fix = 0;
            $sub_where     = array(
                "current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where);
            $this->db->where($where3);
            $query1 = $this->db->get();
            $closed = $query1->num_rows();
            array_push($res, array(
                'ticket_status' => 'closed',
                'count' => $closed
            ));
            $sub_where1 = array(
                "current_status!=" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where1);
            $this->db->where($where3);
            $query2         = $this->db->get();
            $not_yet_closed = $query2->num_rows();
            array_push($res, array(
                'ticket_status' => 'not_yet_closed',
                'count' => $not_yet_closed
            ));
            $sub_where2 = array(
                "all_tickets.previous_status !=" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where2);
            $this->db->where($where3);
            $query3    = $this->db->get();
            $first_fix = $query3->num_rows();
            array_push($res, array(
                'label' => 'First Fix Rate',
                'first_fix' => $first_fix
            ));
            $sub_where3 = array(
                "all_tickets.previous_status" => 15,
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where3);
            $this->db->where($where3);
            $query4        = $this->db->get();
            $not_first_fix = $query4->num_rows();
            array_push($res, array(
                'label' => 'Remaining Tickets',
                'not_first_fix' => $not_first_fix
            ));
            $sub_where4 = array(
                "all_tickets.current_status" => 12
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($where3);
            $this->db->where($sub_where4);
            $query5  = $this->db->get();
            $result5 = $query5->result_array();
            if (!empty($result5)) {
                $overall_tickets = $query5->num_rows();
                $ar1             = array();
                $ar2             = array();
                
                foreach ($result5 as $row) {
                    $strStart = $row['ticket_start_time'];
                    $strEnd   = $row['ticket_end_time'];
                    $dteEnd   = new DateTime($strEnd);
                    $dteStart = new DateTime($strStart);
                    $dteDiff  = $dteStart->diff($dteEnd);
                    
                    $d = $dteDiff->format("%H:%I:%S");
                    array_push($ar, array(
                        "diff" => $d
                    ));
                    $where5 = array(
                        "all_tickets.ticket_id" => $row['ticket_id']
                        
                    );
                    $this->db->select('*');
                    $this->db->from('all_tickets');
                    $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id');
                    $this->db->where($where5);
                    $this->db->where($where3);
                    $query6  = $this->db->get();
                    $result6 = $query6->result_array();
                    if (!empty($result6)) {
                        $resolution = $result6[0]['resolution_time'];
                        if ($d <= $resolution) {
                            array_push($ar1, array(
                                $resolution
                            ));
                        } else {
                            array_push($ar2, array(
                                $resolution
                            ));
                        }
                    }
                }
                $sla_complaince     = count($ar1);
                $not_sla_complaince = count($ar2);
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            } else {
                $sla_complaince     = 0;
                $not_sla_complaince = 0;
                array_push($res, array(
                    'sla_complaince' => $sla_complaince
                ));
                array_push($res, array(
                    'not_sla_complaince' => $not_sla_complaince
                ));
            }
            $sub_where10 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('amc_type');
            $this->db->where($sub_where10);
            $query10 = $this->db->get();
            $amc     = $query10->result_array();
            foreach ($amc as $a) {
                $sub_where7 = array(
                    "current_status!=" => '14',
                    "requested_spare!=" => '',
                    "amc_type" => $a['amc_type']
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where7);
                $this->db->where($where3);
                $query7   = $this->db->get();
                $warranty = $query7->num_rows();
                array_push($resu1, array(
                    'Spare_amctype' => $a['amc_type'],
                    'count' => $warranty
                ));
            }
            array_push($res, array(
                'result' => $resu1
            ));
            $sub_where11 = array(
                "company_id" => $company_id,
                "requested_spare!=" => ''
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->where($sub_where11);
            $this->db->where($where3);
            $query11    = $this->db->get();
            $prod_spare = $query11->result_array();
            foreach ($prod_spare as $p) {
                $sparecode = json_decode($p['requested_spare'], true);
                foreach ($sparecode as $spr) {
                    $sub_where8 = array(
                        "spare_code" => $spr['Spare_code'],
                        "spare.company_id" => $company_id
                    );
                    $this->db->select('product_name');
                    $this->db->from('spare');
                    $this->db->join('product_management', 'product_management.product_id=spare.product');
                    $this->db->where($sub_where8);
                    $query12 = $this->db->get();
                    $spar    = $query12->result_array();
                    $pr_id   = $spar[0]['product_name'];
                    array_push($res1, array(
                        'spare_product' => $pr_id
                    ));
                }
            }
            $sub_where9 = array(
                "company_id" => $company_id
            );
            $this->db->select('product_name');
            $this->db->from('product_management');
            $this->db->where($sub_where9);
            $query13  = $this->db->get();
            $pro_name = $query13->result_array();
            $counter  = 0;
            foreach ($pro_name as $prod) {
                $counter_array = array();
                foreach ($res1 as $re) {
                    if (in_array($prod['product_name'], $re)) {
                        array_push($counter_array, array(
                            'Spare_' . $prod['product_name'] => $prod['product_name']
                        ));
                    }
                }
                $counter = count($counter_array);
                array_push($resu, array(
                    'Spare_product' => $prod['product_name'],
                    'count' => $counter
                ));
            }
            array_push($res, array(
                'result' => $resu
            ));
        }
        echo json_encode($res);
    }
    public function customer_status($company_id, $filter)
    {
        $res    = array();
        $res1   = array();
        $ar     = array();
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $week   = date("Y-m-d 00:00:00", strtotime("-1 week"));
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $year   = date("Y-m-d 00:00:00", strtotime("-1 year"));
        $where1 = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $week
        );
        $where2 = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $month
        );
        $where3 = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $year
        );
        $where  = array(
            'company_id' => $company_id,
            'all_tickets.last_update<=' => $today,
            'all_tickets.last_update>=' => $today1
        );
        $res    = array();
        $res1   = array();
        $res2   = array();
        if ($filter == '' || $filter == 'today') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product_id" => $prd['product_id'],
                    "cust_feedback!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where);
                $query2        = $this->db->get();
                $cust_feedback = $query2->num_rows();
                $sub_where1    = array(
                    "product_id" => $prd['product_id'],
                    "cust_rating!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where);
                $query3      = $this->db->get();
                $cust_rating = $query3->num_rows();
                array_push($res1, array(
                    'product_name' => $prd['product_name'],
                    'cust_feedback' => $cust_feedback
                ));
                array_push($res2, array(
                    'product_name' => $prd['product_name'],
                    'cust_rating' => $cust_rating
                ));
            }
            array_push($res, array(
                'feedback' => $res1,
                'rating' => $res2
            ));
        } else if ($filter == 'week') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product_id" => $prd['product_id'],
                    "cust_feedback!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where1);
                $query2        = $this->db->get();
                $cust_feedback = $query2->num_rows();
                $sub_where1    = array(
                    "product_id" => $prd['product_id'],
                    "cust_rating!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where1);
                $query3      = $this->db->get();
                $cust_rating = $query3->num_rows();
                array_push($res1, array(
                    'product_name' => $prd['product_name'],
                    'cust_feedback' => $cust_feedback
                ));
                array_push($res2, array(
                    'product_name' => $prd['product_name'],
                    'cust_rating' => $cust_rating
                ));
            }
            array_push($res, array(
                'feedback' => $res1,
                'rating' => $res2
            ));
        } else if ($filter == 'month') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product_id" => $prd['product_id'],
                    "cust_feedback!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where2);
                $query2        = $this->db->get();
                $cust_feedback = $query2->num_rows();
                $sub_where1    = array(
                    "product_id" => $prd['product_id'],
                    "cust_rating!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where2);
                $query3      = $this->db->get();
                $cust_rating = $query3->num_rows();
                array_push($res1, array(
                    'product_name' => $prd['product_name'],
                    'cust_feedback' => $cust_feedback
                ));
                array_push($res2, array(
                    'product_name' => $prd['product_name'],
                    'cust_rating' => $cust_rating
                ));
            }
            array_push($res, array(
                'feedback' => $res1,
                'rating' => $res2
            ));
        } else if ($filter == 'year') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product_id" => $prd['product_id'],
                    "cust_feedback!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where3);
                $query2        = $this->db->get();
                $cust_feedback = $query2->num_rows();
                $sub_where1    = array(
                    "product_id" => $prd['product_id'],
                    "cust_rating!=" => ''
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where3);
                $query3      = $this->db->get();
                $cust_rating = $query3->num_rows();
                array_push($res1, array(
                    'product_name' => $prd['product_name'],
                    'cust_feedback' => $cust_feedback
                ));
                array_push($res2, array(
                    'product_name' => $prd['product_name'],
                    'cust_rating' => $cust_rating
                ));
            }
            array_push($res, array(
                'feedback' => $res1,
                'rating' => $res2
            ));
        }
        echo json_encode($res);
    }
    public function field_force($company_id, $filter)
    {
        $res    = array();
        $res1   = array();
        $ar     = array();
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $week   = date("Y-m-d 00:00:00", strtotime("-1 week"));
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $year   = date("Y-m-d 00:00:00", strtotime("-1 year"));
        $where1 = array(
            'technician.company_id' => $company_id,
            'technician.last_update<=' => $today,
            'technician.last_update>=' => $week
        );
        $where2 = array(
            'technician.company_id' => $company_id,
            'technician.last_update<=' => $today,
            'technician.last_update>=' => $month
        );
        $where3 = array(
            'technician.company_id' => $company_id,
            'technician.last_update<=' => $today,
            'technician.last_update>=' => $year
        );
        $where  = array(
            'company_id' => $company_id,
            'technician.last_update<=' => $today,
            'technician.last_update>=' => $today1
        );
        $res    = array();
        if ($filter == '' || $filter == 'today') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where);
                $query2     = $this->db->get();
                $technician = $query2->num_rows();
                array_push($res, array(
                    'product_name' => $prd['product_name'],
                    'technician' => $technician
                ));
            }
        } else if ($filter == 'week') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where1);
                $query2     = $this->db->get();
                $technician = $query2->num_rows();
                array_push($res, array(
                    'product_name' => $prd['product_name'],
                    'technician' => $technician
                ));
            }
        } else if ($filter == 'month') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where2);
                $query2     = $this->db->get();
                $technician = $query2->num_rows();
                array_push($res, array(
                    'product_name' => $prd['product_name'],
                    'technician' => $technician
                ));
            }
        } else if ($filter == 'year') {
            $sub_where = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where3);
                $query2     = $this->db->get();
                $technician = $query2->num_rows();
                array_push($res, array(
                    'product_name' => $prd['product_name'],
                    'technician' => $technician
                ));
            }
        }
        echo json_encode($res);
    }
    public function revenue_status($company_id, $filter)
    {
        $res    = array();
        $res1   = array();
        $ar     = array();
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $week   = date("Y-m-d 00:00:00", strtotime("-1 week"));
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $year   = date("Y-m-d 00:00:00", strtotime("-1 year"));
        $where1 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $week
        );
        $where2 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $month
        );
        $where3 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $year
        );
        $where  = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $today1
        );
        $res    = array();
        $res1   = array();
        $res2   = array();
        if ($filter == '' || $filter == 'today') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "status" => 0,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => ""
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res1, array(
                    "product" => $prd['product_name'],
                    "NewAmc" => $new_amc
                ));
                $sub_where1 = array(
                    "status" => 1,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => "",
                    "current_status" => 12,
                    "previous_status" => 16
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where);
                $query3    = $this->db->get();
                $renew_amc = $query3->num_rows();
                array_push($res2, array(
                    "product" => $prd['product_name'],
                    "RenewalAmc" => $renew_amc
                ));
            }
        } else if ($filter == 'week') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "status" => 0,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => ""
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where1);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res1, array(
                    "product" => $prd['product_name'],
                    "NewAmc" => $new_amc
                ));
                $sub_where1 = array(
                    "status" => 1,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => "",
                    "current_status" => 12,
                    "previous_status" => 16
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where1);
                $query3    = $this->db->get();
                $renew_amc = $query3->num_rows();
                array_push($res2, array(
                    "product" => $prd['product_name'],
                    "RenewalAmc" => $renew_amc
                ));
            }
        } else if ($filter == 'month') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "status" => 0,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => ""
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where2);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res1, array(
                    "product" => $prd['product_name'],
                    "NewAmc" => $new_amc
                ));
                $sub_where1 = array(
                    "status" => 1,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => "",
                    "current_status" => 12,
                    "previous_status" => 16
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where2);
                $query3    = $this->db->get();
                $renew_amc = $query3->num_rows();
                array_push($res2, array(
                    "product" => $prd['product_name'],
                    "RenewalAmc" => $renew_amc
                ));
            }
        } else if ($filter == 'year') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "status" => 0,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => ""
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where);
                $this->db->where($where3);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res1, array(
                    "product" => $prd['product_name'],
                    "NewAmc" => $new_amc
                ));
                $sub_where1 = array(
                    "status" => 1,
                    "product_id" => $prd['product_id'],
                    "amc_id!=" => "",
                    "current_status" => 12,
                    "previous_status" => 16
                );
                $this->db->select('*');
                $this->db->from('all_tickets');
                $this->db->where($sub_where1);
                $this->db->where($where3);
                $query3    = $this->db->get();
                $renew_amc = $query3->num_rows();
                array_push($res2, array(
                    "product" => $prd['product_name'],
                    "RenewalAmc" => $renew_amc
                ));
            }
        }
        array_push($res, array(
            "New" => $res1,
            "Renew" => $res2
        ));
        echo json_encode($res);
    }
    public function tech_count($company_id, $filter)
    {
        $res    = array();
        $res1   = array();
        $ar     = array();
        $today  = date("Y-m-d H:i:s");
        $today1 = date("Y-m-d 00:00:00");
        $week   = date("Y-m-d 00:00:00", strtotime("-1 week"));
        $month  = date("Y-m-d 00:00:00", strtotime("-1 month"));
        $year   = date("Y-m-d 00:00:00", strtotime("-1 year"));
        $where1 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $week
        );
        $where2 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $month
        );
        $where3 = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $year
        );
        $where  = array(
            'company_id' => $company_id,
            'last_update<=' => $today,
            'last_update>=' => $today1
        );
        $res    = array();
        $res1   = array();
        $res2   = array();
        if ($filter == '' || $filter == 'today') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res, array(
                    "product" => $prd['product_name'],
                    "tech_count" => $new_amc
                ));
            }
        } else if ($filter == 'week') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where1);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res, array(
                    "product" => $prd['product_name'],
                    "tech_count" => $new_amc
                ));
            }
        } else if ($filter == 'month') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where2);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res, array(
                    "product" => $prd['product_name'],
                    "tech_count" => $new_amc
                ));
            }
        } else if ($filter == 'year') {
            $sub_where2 = array(
                "company_id" => $company_id
            );
            $this->db->select('*');
            $this->db->from('product_management');
            $this->db->where($sub_where2);
            $query1 = $this->db->get();
            $produ  = $query1->result_array();
            foreach ($produ as $prd) {
                $sub_where = array(
                    "product" => $prd['product_id']
                );
                $this->db->select('*');
                $this->db->from('technician');
                $this->db->where($sub_where);
                $this->db->where($where3);
                $query2  = $this->db->get();
                $new_amc = $query2->num_rows();
                array_push($res, array(
                    "product" => $prd['product_name'],
                    "tech_count" => $new_amc
                ));
            }
        }
        echo json_encode($res);
    }
    
    //<!--  Sla Contract -->
    public function add_newscore($data)
    {
        $this->db->insert('score_update', $data);
        return true;
    }
    public function add_newreward($data)
    {
        $this->db->insert('score_reward', $data);
        return true;
    }
    public function add_score($data, $id1, $c_id)
    {
        $this->db->where('technician_level', $id1);
        $this->db->where('company_id', $c_id);
        $this->db->update('score_update', $data);
        return true;
    }
    public function add_reward($data, $id2)
    {
        $this->db->where('technician_level', $id2);
        $this->db->update('score_reward', $data);
        return true;
    }
    public function sla_adh($data)
    {
        $this->db->insert('priority', $data);
        return true;
    }
    public function service_disc($data)
    {
        $this->db->insert('service_disc', $data);
        return true;
    }
    public function service_tax($data)
    {
        $this->db->insert('service_disc', $data);
        return true;
    }
    public function update_service_disc($data, $id)
    {
        $this->db->where('priority_level', $id);
        $this->db->update('sla_mapping', $data);
        return true;
    }
    public function update_disc($data, $id)
    {
        $this->db->where('company_id', $id);
        $this->db->update('service_disc', $data);
        return true;
    }
    public function select_service_disc($id)
    {
        $this->db->where('company_id', $id);
        $this->db->select('*');
        $this->db->from('service_disc');
        $query = $this->db->get();
        $res   = $query->result_array();
        
        return $res;
    }
    public function isservice_disc($data)
    {
        $this->db->where('company_id', $data);
        $this->db->select('*');
        $this->db->from('service_disc');
        $query = $this->db->get();
        $res   = $query->result_array();
        
        return $res;
    }
    public function add_contract($data)
    {
        $this->db->insert('amc_type', $data);
        return true;
    }
    public function add_price($data)
    {
        $this->db->insert('amc_price', $data);
        return true;
    }
    public function edit_sla($data, $priority_level)
    {
        $this->db->where('priority_level', $priority_level);
        $this->db->update('priority', $data);
        return true;
    }
    public function disply_invento($datas)
    {
        $company_id = $datas;
        $where      = array(
            "category_details.company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('category_details');
        $this->db->where($where);
        $query = $this->db->get();
        
        return $query;
    }
    public function disply()
    {
        $this->db->select('*');
        $this->db->from('priority');
        //$this->db->where($where);
        $query = $this->db->get();
        
        return $query;
    }
    public function display_score($datas)
    {
        $where = array(
            "call_tag.company_id" => $datas
        );
        $this->db->select('*');
        $this->db->from('call_tag');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    public function display_call_category($cat_id,$cid){
        $where = array(
            "call_tag.company_id" => $cid,
            "call_tag.id" => $cat_id
        );
        $this->db->select('*');
        $this->db->from('call_tag');
        $this->db->where($where);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function disply_inven($datas)
    {
        $company_id = $datas;
        $where      = array(
            "amc_price.company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('amc_price');
        $this->db->where($where);
        $query = $this->db->get();
        
        return $query;
    }

    public function update_call_cat($id,$data,$cid)
    {
       
    $this->db->where('id',$id);
    $this->db->where('company_id',$cid);
    $this->db->update('call_tag', $data);
    return true;
    }
    
    public function delete_call_cat($id,$companyid)
    {
    $this->db->where('id',$id);
    $this->db->where('company_id',$companyid);
    $this->db->delete('call_tag');
    return true;
    }

    public function getdetails_sla($data)
    {
        $this->db->select('*');
        $this->db->from('priority');
        $this->db->where('id', $data);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return NULL;
        }
    }
    public function deletesla($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('priority');
        return true;
    }
    public function load_techregion($company_id)
    {
        $where = array(
            'company_id' => $company_id
        );
        $this->db->distinct();
        $this->db->select('region');
        $this->db->from('technician');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_techlocation($company_id)
    {
        $where = array(
            'company_id' => $company_id
        );
        $this->db->distinct();
        $this->db->select('location');
        $this->db->from('technician');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_techarea($company_id)
    {
        $where = array(
            'company_id' => $company_id
        );
        $this->db->distinct();
        $this->db->select('area');
        $this->db->from('technician');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_techproduct($company_id)
    {
        $where = array(
            'technician.company_id' => $company_id
        );
        $this->db->distinct();
        $this->db->select('product_id,product_name');
        $this->db->from('product_management');
        $this->db->join("technician", "product_management.product_id=technician.product");
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_area_tech($company_id, $region)
    {
        if ($region == '') {
            $where = array(
                'company_id' => $company_id
            );
        } else {
            $where = array(
                'company_id' => $company_id,
                'region' => $region
            );
        }
        
        $this->db->select('area');
        $this->db->from('technician');
        $this->db->where($where);
        $this->db->group_by('area');
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_location_tech($company_id, $region, $area)
    {
        if ($region == '' && $area == '') {
            $where = array(
                'company_id' => $company_id
            );
        } else if ($region == '' && $area != '') {
            $where = array(
                'company_id' => $company_id,
                'area' => $area
            );
        } else if ($region != '' && $area == '') {
            $where = array(
                'company_id' => $company_id,
                'region' => $region
            );
        } else {
            $where = array(
                'company_id' => $company_id,
                'region' => $region,
                'area' => $area
            );
        }
        
        $this->db->distinct();
        $this->db->select('location');
        $this->db->from('technician');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    
    public function load_area($company_id, $region, $area)
    {
        if ($region == '' && $area == '') {
            $where = array(
                'company_id' => $company_id
            );
        } else if ($region == '' && $area != '') {
            $where = array(
                'company_id' => $company_id,
                'town' => $area
            );
        } else if ($region != '' && $area == '') {
            $where = array(
                'company_id' => $company_id,
                'region' => $region
            );
        } else {
            $where = array(
                'company_id' => $company_id,
                'region' => $region,
                'town' => $area
            );
        }
        
        $this->db->distinct();
        $this->db->select('town');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    
    public function load_location($company_id, $region, $area)
    {
        if ($region == '' && $area == '') {
            $where = array(
                'company_id' => $company_id
            );
        } else if ($region == '' && $area != '') {
            $where = array(
                'company_id' => $company_id,
                'town' => $area
            );
        } else if ($region != '' && $area == '') {
            $where = array(
                'company_id' => $company_id,
                'region' => $region
            );
        } else {
            $where = array(
                'company_id' => $company_id,
                'region' => $region,
                'town' => $area
            );
        }
        
        $this->db->distinct();
        $this->db->select('location');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_region($company_id)
    {
        $where = array(
            'company_id' => $company_id
        );
        $this->db->distinct();
        $this->db->select('region');
        $this->db->from('all_tickets');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_product($company_id)
    {
        $where = array(
            'company_id' => $company_id
        );
        $this->db->select('*');
        $this->db->from('product_management');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    public function load_cat($company_id, $product_id)
    {
        $where = array(
            'company_id' => $company_id,
            'prod_id' => $product_id
        );
        $this->db->select('*');
        $this->db->from('category_details');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        echo json_encode($result);
    }
    //sla
    public function choose_company()
    {
        $this->db->select('company_id,company_name');
        $this->db->from('company');
        $company = $this->db->get();
        $result  = $company->result_array();
        echo json_encode($result);
    }
    public function check($company_name)
    {
        $result = array();
        $this->db->select('*');
        $this->db->from('sla_combination');
        $this->db->where('company_id', $company_name);
        $result1 = $this->db->get();
        $res     = $result1->result_array();
        if (!empty($res)) {
            foreach ($res as $row) {
                if ($row['product'] == 'all') {
                    array_push($result, ('product'));
                }
                if ($row['category'] == 'all') {
                    array_push($result, ('category'));
                }
                if ($row['cust_category'] == 'all') {
                    array_push($result, ('cust_category'));
                }
                if ($row['call_category'] == 'all') {
                    array_push($result, ('call_category'));
                }
                if ($row['service_category'] == 'all') {
                    array_push($result, ('service_category'));
                }
            }
        }
        echo json_encode(array(
            'result' => $result
        ), true);
    }
    public function get_reference_id($company_name)
    {
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $result1 = $this->db->get();
        $res     = $result1->result_array();
        if (empty($res)) {
            $ref_id = 1;
        } else {
            $ref_id = $res[0]['ref_id'] + 1;
        }
        return $ref_id;
    }
    public function add_sla1($adata2)
    {
        $this->db->select('*');
        $this->db->from('sla_combination');
        $this->db->where($adata2);
        $company = $this->db->get();
        $result  = $company->result_array();
        if (empty($result)) {
            $this->db->insert('sla_combination', $adata2);
            return true;
        } else {
            return "Already Exist!";
        }
    }
    public function add_sla($adata1)
    {
        
        $this->db->insert('sla_mapping', $adata1);
        return true;
        
    }
    
    public function call_closed($company_id, $filter)
    {
        $result     = array();
        if ($filter == '' || $filter == 'today') {
            $today = date("Y-m-d");
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 12,
                'all_tickets.last_update.date' => $today
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'week') {
            $week  = date("Y-m-d", strtotime("-1 week"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 12,
                'all_tickets.last_update>=' => $week
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
            
        } else if ($filter == 'month') {
            $month = date("Y-m-d", strtotime("-1 month"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 12,
                'all_tickets.last_update>=' => $month
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'year') {
            $year  = date("Y-m-d", strtotime("-1 year"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 12,
                'all_tickets.last_update>=' => $year
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        }
        return $result;
        
    }
    public function No_of_calls_Escalated($company_id, $filter)
    {
        $result     = array();
        if ($filter == '' || $filter == 'today') {
            $today = date("Y-m-d");
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 9,
                'all_tickets.previous_status' => 9,
                'all_tickets.last_update.date' => $today
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'week') {
            // $today=date("Y-m-d");
            $week  = date("Y-m-d", strtotime("-1 week"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 9,
                'all_tickets.previous_status' => 9,
                'all_tickets.last_update>=' => $week
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
            
        } else if ($filter == 'month') {
            //$today=date("Y-m-d");
            $month = date("Y-m-d", strtotime("-1 month"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 9,
                'all_tickets.previous_status' => 9,
                'all_tickets.last_update>=' => $month
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'year') {
            //$today=date("Y-m-d");
            $year  = date("Y-m-d", strtotime("-1 year"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 9,
                'all_tickets.previous_status' => 9,
                'all_tickets.last_update>=' => $year
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
            
        }
        return $result;
    }
    public function No_of_First_Call_Closure($company_id, $filter)
    {
        $result     = array();
        if ($filter == '' || $filter == 'today') {
            $today = date("Y-m-d");
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status!=' => 9,
                'all_tickets.previous_status !=' => 9,
                'all_tickets.last_update.date' => $today
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'week') {
            // $today=date("Y-m-d");
            $week  = date("Y-m-d", strtotime("-1 week"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status!=' => 9,
                'all_tickets.previous_status !=' => 9,
                'all_tickets.last_update>=' => $week
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
            
        } else if ($filter == 'month') {
            //$today=date("Y-m-d");
            $month = date("Y-m-d", strtotime("-1 month"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status!=' => 9,
                'all_tickets.previous_status !=' => 9,
                'all_tickets.last_update>=' => $month
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
        } else if ($filter == 'year') {
            //$today=date("Y-m-d");
            $year  = date("Y-m-d", strtotime("-1 year"));
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status!=' => 9,
                'all_tickets.previous_status !=' => 9,
                'all_tickets.last_update>=' => $year
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $query  = $this->db->get();
            $result = $query->num_rows();
            
        }
        return $result;
    }
    
}
