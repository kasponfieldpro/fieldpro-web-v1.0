<?php
date_default_timezone_set('Asia/Kolkata');
class Customer extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor  
        parent::__construct();
    }
    public function company()
    {
        $this->db->select('company_id,company_name');
        $this->db->from('company');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_tickets($cust_id,$company_id)
    {
        $this->db->select('all_tickets.ticket_id, technician.first_name as tech_name, product_management.product_name, category_details.cat_name, all_tickets.contact_no,all_tickets.door_no, all_tickets.address as street, all_tickets.town, all_tickets.city, all_tickets.state, all_tickets.country,all_tickets.landmark, all_tickets.pincode,all_tickets.prob_desc, all_tickets.call_tag as call_category,all_tickets.call_type as service_category,all_tickets.current_status, all_tickets.cust_preference_date, all_tickets.raised_time,all_tickets.total_amount,all_tickets.bill_no,all_tickets.ticket_start_time,all_tickets.ticket_end_time');
        $this->db->from('all_tickets,customer,product_management,category_details,technician');
        $this->db->where('all_tickets.product_id=product_management.product_id');
        $this->db->where('all_tickets.cat_id=category_details.cat_id');
        $this->db->where('all_tickets.tech_id=technician.technician_id');
        $this->db->where('all_tickets.company_id', $company_id);
        $this->db->where('all_tickets.cust_id', $cust_id);
        $this->db->group_by('all_tickets.ticket_id');
        $query  = $this->db->get();
        $result = $query->result_array();
		$this->db->select('all_tickets.ticket_id, technician.first_name as tech_name, product_management.product_name, category_details.cat_name, all_tickets.contact_no,all_tickets.door_no, all_tickets.address as street, all_tickets.town, all_tickets.city, all_tickets.state, all_tickets.country,all_tickets.landmark, all_tickets.pincode,all_tickets.prob_desc, all_tickets.call_tag as call_category,all_tickets.call_type as service_category,all_tickets.current_status, all_tickets.cust_preference_date, all_tickets.raised_time,all_tickets.total_amount,all_tickets.bill_no,all_tickets.ticket_start_time,all_tickets.ticket_end_time');
        $this->db->from('all_tickets,customer,product_management,category_details,technician');
        $this->db->where('all_tickets.product_id=product_management.product_id');
        $this->db->where('all_tickets.cat_id=category_details.cat_id');
        $this->db->where('all_tickets.company_id', $company_id);
        $this->db->where('all_tickets.cust_id', $cust_id);
        $this->db->where('all_tickets.current_status',0);
        $this->db->group_by('all_tickets.ticket_id');
        $query  = $this->db->get();
        $result1 = $query->result_array();
		$result=array_merge($result,$result1);
        return $result;
    }
    public function load_service_category()
    {
        $this->db->select('amc_type,id');
        $this->db->from('amc_type');
      //  $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_cust_service_category($cust_id,$company_id)
    {
        $this->db->select('amc_type.amc_type,amc_type.id,customer.customer_id');
        $this->db->from('amc_type,customer');
        $this->db->where('customer.customer_id', $cust_id);
        $this->db->where('customer.company_id', $company_id);
        $this->db->where('amc_type.amc_type=customer.type_of_contract');
        $this->db->group_by('customer.type_of_contract');
        $query  = $this->db->get();
        $re= $query->result_array();
        return $re;
    }
    public function load_call_tags($company_id)
    {
        $this->db->select('call,id');
        $this->db->from('call_tag');
       // $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_product($company_id)
    {
        $this->db->select('product_id,product_name');
        $this->db->from('product_management');
        $this->db->where('company_id', $company_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	public function load_customer_details($company_id,$cust_id)
	{
		$this->db->select('*');
        $this->db->from('product_management,customer,category_details,amc_type');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no=product_management.product_id');
        $this->db->where('customer.component_serial_no=category_details.cat_id');
        $this->db->where('amc_type.amc_type=customer.type_of_contract');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
	}
    public function load_cust_product($company_id,$cust_id)
    {
        $this->db->select('product_management.product_id,product_management.product_name');
        $this->db->from('product_management,customer');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no=product_management.product_id');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.product_serial_no');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_cust_category($company_id,$cust_id,$product_id)
    {
        $this->db->select('category_details.cat_id,category_details.cat_name');
        $this->db->from('customer,category_details');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('category_details.prod_id',$product_id);
        $this->db->where('customer.component_serial_no=category_details.cat_id');
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.component_serial_no');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_model($cust_id,$company_id,$product_id,$cat_id)
    {
        $this->db->select('customer.model_no');
        $this->db->from('customer');
        $this->db->where('customer.customer_id',$cust_id);
        $this->db->where('customer.product_serial_no',$product_id);
        $this->db->where('customer.component_serial_no',$cat_id);
        $this->db->where('customer.company_id', $company_id);
        $this->db->group_by('customer.customer_id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function load_category($company_id, $product_id)
    {
        $this->db->select('cat_id,cat_name');
        $this->db->from('category_details');
        $this->db->where('company_id', $company_id);
        $this->db->where('prod_id', $product_id);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function check_cust($emailid, $contact_no, $product_id, $cat_id, $model_no, $serial_no, $company_id)
    {
        $where = array(
            "email_id" => $emailid,
            "contact_number" => $contact_no,
            "product_serial_no" => $product_id,
            "component_serial_no" => $cat_id,
            "model_no" => $model_no,
            "serial_no" => $serial_no,
            "company_id" => $company_id
        );
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    public function get_cust_id($company_id)
    {
        $where = array(
            "company_id" => $company_id
        );
        $this->db->select('id');
        $this->db->from('customer');
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        $query  = $this->db->get();
        $result = $query->row_array();
        $newid  = $result['id'] + 1;
        $new_id = "Cust_000" . $newid;
        return $new_id;
    }
    public function register($data)
    {
        $this->db->insert("customer", $data);
        return $this->login_details($data);
        //return TRUE;
    }
    public function login_details($data)
    {
        $insert = array(
            'user_type' => "Customer",
            'username' => $data['email_id'],
            'companyid' => $data['company_id']
        );
        $this->db->insert('login', $insert);
        return true;
    }
    public function check_login($username)
    {
        $where = array(
            'username' => $username
        );
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->row_array();
        return $rowcount['password'];
    }
    public function get_userid($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('customer_id');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['customer_id'];
    }
    public function get_mobile($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('contact_number');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['contact_number'];
    }
    public function get_location($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('cust_town');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res[0]['cust_town'];
    }
    public function get_username($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->result_array();
        return $res;
    }
    public function store_id($device_token, $reg_id, $username)
    {
        if ($device_token != '') {
            $data  = array(
                'device_token' => $device_token,
                'reg_id' => ''
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        } else {
            $data  = array(
                'device_token' => '',
                'reg_id' => $reg_id
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        }
        
    } 
	public function add_product($cust_id,$company_id,$product_id,$cat_id,$model_no,$serial_no,$contract_type,$purchase_date,$retailer_name)
    {
        $data=array("cust_id"=>$cust_id,
		"company_id"=>$company_id,
		"product_id"=>$product_id,
		"cat_id"=>$cat_id,
		"model_no"=>$model_no,
		"serial_no"=>$serial_no,
		"contract_type"=>$contract_type,
		"purchase_date"=>$purchase_date,
		"retailer_name"=>$retailer_name);
		
            $this->db->insert('customer_contract', $data);
            return "Inserted Successfully";
        }
        
    
	public function get_company($username)
    {
		$this->db->select('company_id');
        $this->db->from('customer');
        $this->db->where('email_id', $username);
        $this->db->group_by('customer_id');
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result[0]['company_id'];
    }
        
    
}