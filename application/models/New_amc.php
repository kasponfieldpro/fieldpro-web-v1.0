<?php
   class New_amc extends CI_MODEL
    {  
		function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
	

	  public function retrieve($contact_number,$cid)
	  {
	   $today=date('Y-m-d 00:00:00');
	  $data=array();
	  $chart=array();
	  $where=array('contact_number'=>$contact_number,
				   'company_id'=>$cid
				  );
	 $this->db->select('customer.customer_id,customer.customer_name,customer.email_id,customer.contact_number,customer.alternate_number,customer.door_num,customer.address,customer.cust_town, customer.landmark,customer.city,customer.state,customer.cust_country,customer.pincode');
	 $this->db->group_by('customer_id');
	   $this->db->from('customer');
	   $this->db->where($where);
	   $query1= $this->db->get();
	   $query=$query1->result_array();
	   array_push($chart,array('customer'=>$query));
	   foreach($query as $row){
	      $this->db->select('product_management.product_name,customer.product_serial_no');
					$this->db->from('customer');
					$this->db->join('product_management','customer.product_serial_no=product_management.product_id');
					$this->db->where('customer_id',$row['customer_id']);
					$this->db->group_by('product_serial_no');
					$this->db->order_by('product_serial_no');
					//$this->db->limit(1);
					$result= $this->db->get();
					$prod=$result->result_array(); 
					array_push($chart,array('product'=>$prod));
                foreach($prod as $rw){
					$this->db->select('customer.component_serial_no,category_details.cat_name');
                    $this->db->from('customer');
					$this->db->join('category_details','customer.component_serial_no=category_details.cat_id');
            		$this->db->where('product_serial_no',$rw['product_serial_no']);
					$this->db->where('customer_id',$row['customer_id']);
					$this->db->group_by('component_serial_no');
					$this->db->order_by('component_serial_no');
                    $res= $this->db->get();
					$cat=$res->result_array(); 
					array_push($chart,array('category'=>$cat));					
                foreach($cat as $r){
                $this->db->select('model_no,serial_no,type_of_contract,contract_value,warrenty_expairy_date,start_date,end_date');
			    $this->db->from('customer');
                $this->db->where('component_serial_no',$r['component_serial_no']);	
				$this->db->where('customer_id',$row['customer_id']);
				$this->db->group_by('type_of_contract');
                $this->db->order_by('last_update','desc');
				$this->db->limit(1);
                $cont= $this->db->get();				
				if($cont->num_rows()>0){
					$result1=$cont->result_array(); 
					$result1=array_merge($result1[0],array("product_id"=>$rw["product_serial_no"],"product_name"=>$rw['product_name'],"cat_id"=>$r["component_serial_no"],"cat_name"=>$r["cat_name"],"custid"=>$row["customer_id"],"customer_name"=>$row["customer_name"],"email_id"=>$row["email_id"],"contact"=>$row['contact_number'],"anumber"=>$row["alternate_number"],"door_num"=>$row["door_num"],"address"=>$row['address'],"cust_town"=>$row["cust_town"],"land_mark"=>$row["landmark"],"city"=>$row["city"],"state"=>$row["state"],"cust_country"=>$row["cust_country"],"pincode"=>$row["pincode"]));
				}else{
					$result1=array("product_id"=>$rw["product_serial_no"],"product_name"=>$rw['product_name'],"cat_id"=>$r["component_serial_no"],"cat_name"=>$r["cat_name"],"custid"=>$row["customer_id"],"customer_name"=>$row["customer_name"],"email_id"=>$row["email_id"],"contact"=>$row['contact_number'],"anumber"=>$row["alternate_number"],"door_num"=>$row["door_num"],"address"=>$row['address'],"cust_town"=>$row["cust_town"],"land_mark"=>$row["landmark"],"city"=>$row["city"],"state"=>$row["state"],"cust_country"=>$row["cust_country"],"pincode"=>$row["pincode"]);
				}				
				array_push($data,$result1);
				}
				}	
				}
				array_push($chart,array('contract'=>$data));
			
	if((empty($query)) && (empty($data))){
		return "No details!";
	}
	else {
		return $chart;
	}
		
		}
		
	public function customer_retrieve($contact_number,$company_id)
	{
	 $data=array();
	 $chart=array();
	 $this->db->select('customer.customer_id,customer.customer_name,customer.email_id,customer.alternate_number,customer.door_num,
	 customer.address,customer.cust_town,customer.landmark,customer.city,customer.state,customer.cust_country,customer.pincode,product_management.product_name,
	 product_management.product_id');
	   $this->db->from('customer');
	   $this->db->distinct('customer.product_serial_no');
	   $this->db->join('product_management','product_management.product_id=customer.product_serial_no');
	   $this->db->where('customer.contact_number',$contact_number);
	   $this->db->where('customer.company_id',$company_id);
	   $query1= $this->db->get();
	   $query=$query1->result_array(); 
	   foreach($query as $row)
	   {
			$result1=array("product_id"=>$row["product_id"],"product_name"=>$row['product_name'],"custid"=>$row["customer_id"],"customer_name"=>$row["customer_name"],"email_id"=>$row["email_id"],"anumber"=>$row["alternate_number"],"door_num"=>$row["door_num"],"address"=>$row['address'],"cust_town"=>$row["cust_town"],"l_mrk"=>$row["landmark"],"city"=>$row["city"],"state"=>$row["state"],"cust_country"=>$row["cust_country"],"pincode"=>$row["pincode"]);
			
		array_push($data,$result1);
	   }
	   array_push($chart,array('contract'=>$data));
	 if((empty($query)) && (empty($data))){
		return "No details!";
	}
	else {
		return $chart;
	}
	}
	public function retrieve_r($contact_number)
	{
	 $data=array();
	 $this->db->select('customer.customer_id,customer.customer_name,customer.email_id,customer.alternate_number,customer.door_num,customer.address,customer.cust_town,
	 customer.cust_country,product_management.product_name,product_management.product_id');
	   $this->db->from('customer');
		$this->db->join('product_management','product_management.product_id=customer.product_serial_no');
	   $this->db->where('contact_number',$contact_number);
	   $query1= $this->db->get();
	   $query=$query1->result_array(); 
	   return $query;
	  
	}
	public function cust_search($number)
	{  //GROUP_CONCAT(type_of_contract) AS contract
		$this->db->select('product_serial_no,GROUP_CONCAT(component_serial_no) AS sub_cat');
		$this->db->from('customer');
		$this->db->where('contact_number',$number);
		$this->db->group_by('product_serial_no');
		//$this->db->group_by(array('product_serial_no', 'component_serial_no'));
		//$this->db->distinct('component_serial_no');
		$query1= $this->db->get();
		$result=$query1->result_array();
		echo json_encode($result);
		
	}
	public function modal_product($company_id)
	{
		$this->db->select('product_id,product_name');
		$this->db->from('product_management');
		$this->db->where('company_id',$company_id);
		$query1= $this->db->get();
		$amc=$query1->result_array();
		echo json_encode($amc);
	}
	public function calculate_period($company_id,$cont_type)
	{
		/* $where=array(
					'amc_type'=>$cont_type,		
					'company_id'=>$company_id,		
				);*/
		$this->db->select('contract_period');
		$this->db->from('amc_type');
		$this->db->where('amc_type',$cont_type);
		$query1= $this->db->get();
		$amc_type=$query1->row_array();
		echo json_encode($amc_type);
	}
	public function select_amc($company_id)
	{
		$this->db->select('id,amc_type');
				$this->db->from('amc_type');
		//$this->db->where('company_id',$company_id);
				$query1= $this->db->get();
		$amc=$query1->result_array();
			   return $amc;	
	}
		public function select_cat($product,$company){
			$data=array();
			$where1=array('prod_id'=>$product,
						  'company_id'=>$company
							 );
			$this->db->select('cat_id,cat_name');
				$this->db->from('category_details');
			$this->db->where($where1);
				$query1= $this->db->get();
			$query=$query1->result_array();
			   return $query;
		}
		
		public function tags_input()
		{
			$this->db->select('tag');
			$this->db->from('call_tagging');
			$tag= $this->db->get();
			$result=$tag->result_array();
				echo json_encode($result);
		}
		
		//$where = "product_serial_no='$prod' AND customer_id='$cust_id'";
				//$this->db->select('category.cat_id,category.cat_name');
             //$this->db->from('category');
				//$this->db->join('customer','customer.component_serial_no=category.cat_id');
			 //$this->db->where($where);
				//$query1= $this->db->get();
			 //$res=$query1->result_array();
				//echo json_encode($res);
	 public function load_location($company_id)
       {           
              $data1=array(
                     'amc_id!='=>'', 'company_id='=>$company_id
              );
		$this->db->select('location');
		$this->db->from('all_tickets');
		$this->db->distinct();
                $this->db->where($data1); 
		$location= $this->db->get();
		$result=$location->result_array();
		echo json_encode($result);
    }	 
	
	public function cust_id($company_id)
	{
		 $this->db->select('id');
        $this->db->from('customer');
		$this->db->order_by('customer.id','desc');
		$this->db->limit(1);
		$this->db->where('company_id',$company_id);
		$query1 = $this->db->get();
				$query  = $query1->result_array();
		if(!empty($query))
		{
		$id=$query[0]['id']+1;
		$new_cust_id='Cust_'.str_pad($id,4,"0",STR_PAD_LEFT);
		}
		else
		{
		$new_cust_id='Cust_0001';
		}
		return $new_cust_id;
    }	
	public function select_amc1($com)
    {
         $this->db->select('id');
        $this->db->from('all_tickets');
        $this->db->where('company_id',$com);
		$this->db->order_by('all_tickets.id','desc');
		$this->db->limit(1);
		$query1 = $this->db->get();
				$query  = $query1->result_array();
		if(!empty($query))
		{
		$id=$query[0]['id']+1;
		$new_amc_id='AMC'.str_pad($id,4,"0",STR_PAD_LEFT);
		}
		else
		{
		$new_amc_id='AMC0001';
		}
		return $new_amc_id;
    }
 public function check_ticket($where_array){
		$this->db->select('*');
        $this->db->from('all_tickets');
        $this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
        $this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
        $this->db->order_by('all_tickets.last_update', 'desc');
        $this->db->group_start();
        $this->db->where('all_tickets.current_status!=', 8);
        $this->db->or_where('all_tickets.current_status!=', 12);
        $this->db->or_where('all_tickets.current_status!=', 20);
        $this->db->group_end();
		$this->db->where($where_array);
		$query = $this->db->get();
		$result = $query->num_rows();
		return $result;
	}
	 public function raise_newticket($cdata){
		$this->db->insert('all_tickets', $cdata); 
		return true;
	 }
	 public function savedata($data){
		$this->db->insert('all_tickets', $data); 
		return true;
    }
	public function raise_data($rdata){
        $this->db->insert('all_tickets', $rdata); 
        return true;
    }
	public function cust_insert($idata){
        $this->db->insert('customer', $idata); 
        return true;
    }
	public function add_customerdetails($idata) {
		 $this->db->insert('customer', $idata); 
        return true;
	}
    public function check_customer($contact){
		$this->db->select('*');		
		$this->db->from('customer');	
		$this->db->where('contact_number',$contact);
		$query=$this->db->get();
		$res=$query->result_array();
		if (count($res) != 0) {
			return '0';
		}
		else{
			return '1';
		}
	}
	public function check_custmail($mail){
		$this->db->select('*');		
		$this->db->from('customer');	
		$this->db->where('email_id',$mail);
		$query=$this->db->get();
		$res=$query->result_array();
		if (count($res) != 0) {
			return '0';
		}
		else{
			return '1';
		}
	}
	/*public function get_birds(){
		$this->db->select('tag');		
		$this->db->from('call_tagging');		
		$query=$this->db->get();
		$res=$query->result();
		return $res;
	}*/
        public function get_birds($number){
		$this->db->select('*');		
		$this->db->from('call_tag');	
		$this->db->where('company_id',$number);
		$query=$this->db->get();
		$res=$query->result_array();
		echo json_encode($res);
         }
	public function update_cust($cust,$data1){
        $this->db->update('customer', $data1); 
		$this->db->where('customer_id',$cust);
        return TRUE;
    }
	public function save_contract($cdata){
		$this->db->insert('all_tickets', $cdata); 
        return true;
	}
	public function new_contract($cont_data)
	{
		$this->db->insert('all_tickets', $cont_data); 
        return true;
	}
		public function load_contract($company_id){
			 $data1=array(
            'all_tickets.amc_id!='=>'', 'all_tickets.company_id'=>$company_id, 'all_tickets.current_status' =>0
            );
			$this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.	call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.location,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				$this->db->join('customer','all_tickets.cust_id=customer.customer_id');
                                $this->db->where($data1);
				$page=$this->db->get();
				$result = $page->result_array();
				echo json_encode($result);
		}
	public function load_page($amc,$company_id){
	       $data=array(
            'all_tickets.amc_id!='=>'', 'all_tickets.company_id='=>$company_id, 'all_tickets.current_status='=>0,'all_tickets.call_type=' =>$amc
            );
				$this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.	call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.location,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				$this->db->join('customer','all_tickets.cust_id=customer.customer_id');
				$page=$this->db->get();
				$location_result = $res->result_array();
				echo json_encode($location_result);
		}
    public function load_page1($amc,$company_id){
	       $data=array(
            'all_tickets.amc_id!='=>'',  
            'all_tickets.company_id='=>$company_id,                     
            'all_tickets.current_status='=>8,
            'all_tickets.call_type=' =>$amc
            );
				$this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.	call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.total_amount,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				 $this->db->join('customer','all_tickets.cust_id=customer.customer_id');
                                $this->db->where($data);
				$page=$this->db->get();
				$location_result1 = $res->result_array();
				echo json_encode($location_result1);
		}
	public function filter_content($loc,$company_id){
			 $data=array(
            'all_tickets.amc_id!='=>'', 'all_tickets.company_id='=>$company_id, 'all_tickets.current_status='=> 0,
			'all_tickets.location=' =>$loc
            );
			$this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.	call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.location,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				$this->db->join('customer','all_tickets.cust_id=customer.customer_id');
                                  $this->db->where($data);
				$page=$this->db->get();
				$location_result = $res->result_array();
				echo json_encode($location_result);
		}
          public function filter_location($loc,$company_id){
			 $data=array(
            'all_tickets.amc_id!='=>'', 'all_tickets.company_id='=>$company_id, 'all_tickets.current_status='=> 8,
			'all_tickets.location=' =>$loc
            );
$this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.total_amount,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				$this->db->join('customer','all_tickets.cust_id=customer.customer_id');
                $this->db->where($data);
				$page=$this->db->get();
				$location_result = $res->result_array();
				echo json_encode($location_result);
		}
	
	public function tick_id($com)
	{
	  $this->db->select('id');
	   $this->db->from('all_tickets');
	//	$this->db->where('company_id',$com);
	   $this->db->order_by('id','DESC');
	   $this->db->limit(1);
	   $query=$this->db->get();
$res = $query->result();
if(!empty($res))
		{
	   
		$row = $res[0]->id+1; 
		$row=str_pad($row, 4, '0', STR_PAD_LEFT);
		return 'Tk_'.$row;
}
else
{
return 'Tk_0001';
}  // return $query1;
	}
public function amc_id($com)
{	
	  $this->db->select('id');
        $this->db->from('all_tickets');
      //  $this->db->where('company_id',$com);
		$this->db->order_by('all_tickets.id','desc');
		$this->db->limit(1);
		$query1 = $this->db->get();
				$query  = $query1->result_array();
		if(!empty($query))
		{
		$id=$query[0]['id']+1;
		$new_amc_id='Tk_'.str_pad($id,4,"0",STR_PAD_LEFT);
		}
		else
		{
		$new_amc_id='AMC0001';
		}
		return $new_amc_id;
	}
	public function product_id($prod,$comp)
	{
		$where=array('product_management.product_name'=>$prod,'product_management.company_id'=>$comp);
			$this->db->select('product_id');
			$this->db->from('product_management');
			$this->db->where($where);
			$query1= $this->db->get();
			$query=$query1->result_array();
			   return $query[0]['product_id'];
	}
	public function cat_id($cat,$prod_id,$comp)
	{
		$where1=array('category_details.cat_name'=>$cat,'category_details.prod_id'=>$prod_id,'category_details.company_id'=>$comp);
		$this->db->select('cat_id');
		$this->db->from('category_details');
		$this->db->where($where1);
		$query1= $this->db->get();
		$query=$query1->result_array();
	    return $query[0]['cat_id'];
	}
	public function select_call($prod,$cat){
		     
			$data=array();
				$where=array('product_serial_no'=>$prod,'component_serial_no'=>$cat);
			$this->db->select('type_of_call');
				$this->db->from('customer');
			$this->db->where($where);
				$query1= $this->db->get();
			$query=$query1->result();
			   echo $query;
	
	}
       public function amc_contracts($company_id){
		  $data1=array(
            'all_tickets.amc_id!='=>'', 'all_tickets.company_id'=>$company_id, 'all_tickets.current_status' =>12
            );
	   $this->db->select('all_tickets.amc_id,all_tickets.cust_id,product_management.product_name,category_details.cat_name,all_tickets.	call_type,all_tickets.quantity,all_tickets.address,all_tickets.contact_no,all_tickets.total_amount,customer.customer_name,customer.email_id');
				$this->db->from('all_tickets');
				$this->db->join('product_management','all_tickets.product_id=product_management.product_id');
				$this->db->join('category_details','all_tickets.cat_id=category_details.cat_id');
				$this->db->join('customer','all_tickets.cust_id=customer.customer_id');
                                $this->db->where($data1);
				$page=$this->db->get();
				$result = $page->result_array();
				echo json_encode($result);
	}
	public function fetch_info($amc_id){
		 
	 $this->db->select('all_tickets.cust_id,customer.customer_name,customer.contact_number,customer.door_num,customer.address,customer.base_location,customer.pincode,product_management.product_name,product_management.product_id');
	   $this->db->from('all_tickets');
	      $this->db->where('amc_id',$amc_id);
	    $this->db->join('customer','customer.customer_id=all_tickets.cust_id');
		$this->db->join('product_management','product_management.product_id=customer.product_serial_no');
	
	   $query1= $this->db->get();
	   $query=$query1->result_array();
		if(!empty($query))
			{ 
				echo json_encode($query,true);
		 	} 
		else
			{
				echo "This ID doesn't exist!";
			}
		 	
	}
	public function getcontractdetails($companyid,$contractname)
	{
	$where=array('amc_type'=>$contractname);
			$this->db->select('amc_type,contract_period,contract_amount');
				$this->db->from('amc_type');
			$this->db->where($where);
		
			 	$query = $this->db->get();
		$result=$query->result_array();
		return $result;
	}
	public function updatecontractdetails($custid,$companyid)
	{
		$this->db->select('id');
			$this->db->from('customer');
			$this->db->where('customer_id',$custid);
			$this->db->where('company_id', $companyid);
			$query1= $this->db->get();
			$query=$query1->result_array();
			if(count($query)>0)
			{
			$id=$query[0]['id'];
			   $contractid="Cont_".$query[0]['id'];

		$this->db->set('contract_id', $contractid);
		$this->db->where('id', $id);
		$this->db->where('customer_id', $custid);
		$this->db->where('company_id', $companyid);
		$this->db->update('customer');
		}
		return true;
	}
	}
