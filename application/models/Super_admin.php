<?php
class Super_admin extends CI_MODEL

	{
	function __construct()
		{

		// Call the Model constructor

		parent::__construct();
		}
	
             public function user_data($user_table){
		$this->db->insert('user', $user_table);
		return true;
	}
           public function comp_data1($cdata)
		{
		$this->db->insert('company', $cdata);
		return true;
		}
public function delet_sla($ref_id, $company_id)
     {
          $this->db->where('company_id', $company_id);
          $this->db->where('ref_id', $ref_id);
          $this->db->delete('sla_combination');
          return true;
     }
public function delet_sla_map($ref_id, $company_id)
     {
          $this->db->where('company_id', $company_id);
          $this->db->where('ref_id', $ref_id);
          $this->db->delete('sla_mapping');
          return true;
     }
     
public function submit_edit_sla($company_id, $ref_id, $response, $resolution, $acceptance, $sla_compliance, $mttr)
     {
          $data_edit = array(
               'response_time' => $response,
               'resolution_time' => $resolution,
               'acceptance_time' => $acceptance,
               'SLA_Compliance_Target' => $sla_compliance,
               'mttr_target' => $mttr
          );
          $this->db->where('company_id', $company_id);
          $this->db->where('ref_id', $ref_id);
          $this->db->update('sla_mapping', $data_edit);
          return true;
     }
	public function admin_data($adata1)
		{
		$this->db->insert('admin_user', $adata1);
		return true;
		}
public function login_data($adata1)
		{
		$this->db->insert('login', $adata1);
		return true;
		}

	public function bulk_slabill($company_id, $product, $category, $priority, $sla_amount, $elapsed_amount, $data)
		{
		$query = $this->db->select('company_id')->from('company')->like('company_name', $company_id)->get();
		if ($query->num_rows() > 0)
			{
			$query = $query->row_array();
			$query1 = $this->db->select('product_id')->from('product_management')->where('company_id', $query['company_id'])->like('product_name', $product)->get();
			if ($query1->num_rows() > 0)
				{
				$query1 = $query1->row_array();
				$query2 = $this->db->select('cat_id')->from('category_details')->where('prod_id', $query1['product_id'])->like('cat_name', $category)->get();
				if ($query2->num_rows() > 0)
					{
					$query2 = $query2->row_array();
					$data1 = array(
						'company_id' => $query['company_id'],
						'product' => $query1['product_id'],
						'category' => $query2['cat_id'],
						'priority' => $priority,
						'sla_amount' => $sla_amount,
						'elapsed_amount' => $elapsed_amount
					);
					$this->db->insert('billingfor_sla', $data1);
					return true;
					}
				  else
					{
					return "Sorry, the Sub-Category" . $category . "does not exist in" . $company_id;
					}
				}
			  else
				{
				return "Sorry, the Product" . $product . "does not exist in" . $company_id;
				}
			}
		  else
			{
			return "Sorry, Company" . $company_id . "does not exist";
			}
		}

	/*  public function getproduct()
	{
	$this->db->select('*');
	$this->db->from('product_management');
	$this->db->order_by("id", "desc");
	$query = $this->db->get();
	return $query;
	} */
	public function load_product($id)
		{
		$this->db->select('product_id,product_name');
		$this->db->from('product_management');
		$this->db->where("company_id", $id);
		$query = $this->db->get();
		$result = $query->result_array();
		echo json_encode($result);
		}

	public function load_subcategory($id)
		{
		$this->db->select('cat_id,cat_name');
		$this->db->from('category_details');
		$this->db->where("prod_id", $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
		}

	public function load_priority()
		{
		$this->db->select('priority_level');
		$this->db->from('priority');
		$company = $this->db->get();
		$priority = $company->result_array();
		echo json_encode($priority);
		}
       public function get_licence($company_id)
  {
		$this->db->select('*');
		$this->db->from('admin_user');
		$this->db->where('company_id',$company_id);
		$this->db->order_by("last_update", "DESC");
		$this->db->limit(1);
		$query = $this->db->get();    
		return $query->row_array();
  }
  public function insert_adminlicence($data,$id,$comp_id){
		$where=array('admin_id'=>$id,
					  'company_id'=>$comp_id
		   );
	     $this->db->where($where);
		$this->db->update('admin_user',$data);
		return true;
	}
	public function renew_ondemand($companyid,$newDate)
	{
		$where=array(
					  'renewal_date'=>$newDate
		   );
	     $this->db->where('company_id',$companyid);
		$this->db->update('admin_user',$where);
		return true;
	}

	public function view_existingbilling($company)
		{
		$result1 = array();
		$data = array();
		$this->db->select('billingfor_sla.product,product_management.product_name');
		$this->db->distinct();
		$this->db->from('billingfor_sla');
		$this->db->join('product_management', 'product_management.product_id = billingfor_sla.product');
		$this->db->where("billingfor_sla.company_id", $company);
		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $row)
			{
			$where1 = array(
				"billingfor_sla.company_id" => $company,
				"billingfor_sla.product" => $row["product"]
			);
			$this->db->select('billingfor_sla.category,category_details.cat_name');
			$this->db->distinct();
			$this->db->from('billingfor_sla');
			$this->db->join('category_details', 'billingfor_sla.category = category_details.cat_id');
			$this->db->where($where1);
			$cat = $this->db->get();
			$category = $cat->result_array();
			foreach($category as $rw)
				{
				$this->db->select('priority_level');
				$this->db->distinct();
				$this->db->from('priority');
				$this->db->order_by('priority.priority_level', 'asc');
				$prio = $this->db->get();
				$priority = $prio->result_array();
				$result2 = array();
				foreach($priority as $r)
					{
					$where3 = array(
						'billingfor_sla.company_id' => $company,
						'billingfor_sla.product' => $row['product'],
						'billingfor_sla.category' => $rw['category'],
						'billingfor_sla.priority' => $r['priority_level']
					);
					$this->db->select('sla_amount,elapsed_amount,id');
					$this->db->distinct();
					$this->db->from('billingfor_sla');
					$this->db->where($where3);
					$pr = $this->db->get();
					if ($pr->num_rows() > 0)
						{
						$answer = $pr->result_array();
						array_push($result2, array(
							$r["priority_level"] => $answer
						));

						//	 array_push($result2,$answer);
						// array_merge($result1,array("pr"=>$answer[0]));

						}
					  else
						{
						$answer[0] = array(
							"sla_amount" => "0",
							"elapsed_amount" => "0"
						);
						array_push($result2, array(
							$r["priority_level"] => $answer
						));

						//	array_merge($result1,array("priority"=>$r["priority"]));

						}
					}

				//  print_r($result2);

				array_push($result1, array(
					"product_name" => $row["product_name"],
					"cat_name" => $rw["cat_name"],
					"priority" => $result2
				));

				// $result1   $data=array_merge($result1,$result2);

				}
			}

		return $result1;
		}

	public function testing_combo($check_company, $check_contact, $check_mail, $check_adcontact)
		{
		$where = array(
			'company_name' => $check_company,
			'ad_mailid' => $check_mail,
			'ad_contact' => $check_adcontact
		);
		$this->db->select('admin_id');
		$this->db->from('admin_user');
		$this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			return TRUE;
			}
		  else
			{
			return NULL;
			}
		}
              public function empid_check($emp_id){
	    $where=array(
			'employee_id'=>$emp_id
	    );	    
	      $this->db->select('employee_id');
	      $this->db->from('user');
	      $this->db->where($where);
	      $query=$this->db->get();
	      if($query->num_rows()>0){
	        return TRUE;
	      }else{
	        return NULL;
	      }
		}
		

		public function get_employeeid_count()
		{
			 
			$this->db->select('*');
			$this->db->from('user');
			$query    = $this->db->get();
			$rowcount = $query->num_rows();
			return $rowcount;
		}

  public function useremail_check($email_id){
    $where=array(
      'email_id'=>$email_id,
    );
    $this->db->select('email_id');
    $this->db->from('technician');
    $this->db->where($where);
    $query=$this->db->get();
	    if($query->num_rows()>0){
	      return true;
	    }else{
	      $this->db->select('email_id');
	      $this->db->from('user');
	      $this->db->where($where);
	      $query=$this->db->get();
		      if($query->num_rows()>0){
		        return true;
		      }else{
		        return NULL;
		      }
	    }
  }

    public function usernumber_check($contact_no){
	    $where=array(
	      'contact_number'=>$contact_no,
	    );	    
	      $this->db->select('contact_number');
	      $this->db->from('user');
	      $this->db->where($where);
	      $query=$this->db->get();
		      if($query->num_rows()>0){
		        return true;
		      }else{
		        return NULL;
		      }
    } 

	public function check_bulkcombo($company_id, $product, $category, $priority)
		{
		$query = $this->db->select('company_id')->from('company')->where('company_name=', $company_id)->get();
		if ($query->num_rows() > 0)
			{
			$query = $query->row_array();
			$query1 = $this->db->select('product_id')->from('product_management')->like('company_id', $query['company_id'])->where('product_name', $product)->get();
			if ($query1->num_rows() > 0)
				{
				$query1 = $query1->row_array();
				$query2 = $this->db->select('cat_id')->from('category_details')->where('prod_id', $query1['product_id'])->like('cat_name', $category)->get();
				if ($query2->num_rows() > 0)
					{
					$query2 = $query2->row_array();
					$where = array(
						'company_id' => $query['company_id'],
						'product' => $query1['product_id'],
						'category' => $query2['cat_id'],
						'priority' => $priority
					);
					$this->db->select('sla_amount,elapsed_amount');
					$this->db->from('billingfor_sla');
					$this->db->where($where);
					$query3 = $this->db->get();
					if ($query3->num_rows() == 0)
						{
						return TRUE;
						}
					  else
						{
						return "dont execute";
						}
					}
				  else
					{
					return "Sorry, the Sub-Category does not exist";
					}
				}
			  else
				{
				return "Sorry, the product-Category does not exist";
				}
			}
		  else
			{
			return "Sorry, Company" . $company_id . "does not exist";
			}
		}

	public function check_combo($comp, $prio, $prod, $cat)
		{
		$where = array(
			'company_id' => $comp,
			'product' => $prod,
			'category' => $cat,
			'priority' => $prio
		);
		$this->db->select('sla_amount,elapsed_amount');
		$this->db->from('billingfor_sla');
		$this->db->where($where);
		$q = $this->db->get();
		$result = $q->result_array();
		echo json_encode($result);
		}

	public function update_sla($comp, $prod, $cat, $sp1, $ep1, $sp2, $ep2, $sp3, $ep3, $sp4, $ep4)
		{
		$this->db->select('product_management.product_id');
		$this->db->from('product_management');
		$this->db->where('product_management.product_name', $prod);
		$query = $this->db->get();
		$ans = $query->row_array();
		if (!empty($ans))
			{
			$this->db->select('category_details.cat_id');
			$this->db->from('category_details');
			$this->db->where('category_details.cat_name', $cat);
			$answer = $this->db->get();
			$res = $answer->row_array();
			$check1 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P1'
			);
			$check2 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P2'
			);
			$check3 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P3'
			);
			$check4 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P4'
			);
			$where1 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P1'
			);
			$where2 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P2'
			);
			$where3 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P3'
			);
			$where4 = array(
				'company_id' => $comp,
				'product' => $ans['product_id'],
				'category' => $res['cat_id'],
				'priority' => 'P4'
			);
			$data1 = array(
				'sla_amount' => $sp1,
				'elapsed_amount' => $ep1
			);
			$data2 = array(
				'sla_amount' => $sp2,
				'elapsed_amount' => $ep2
			);
			$data3 = array(
				'sla_amount' => $sp3,
				'elapsed_amount' => $ep3
			);
			$data4 = array(
				'sla_amount' => $sp4,
				'elapsed_amount' => $ep4
			);
			$this->db->select('sla_amount,elapsed_amount');
			$this->db->from('billingfor_sla');
			$this->db->where($check1);
			$q = $this->db->get();
			$result = $q->result_array();
			if (!empty($result))
				{
				$this->db->where($where1);
				$this->db->update('billingfor_sla', $data1);
				}
			  else
				{
				$this->db->insert('billingfor_sla', $where1);
				$this->db->where($where1);
				$this->db->update('billingfor_sla', $data1);
				}

			$this->db->select('sla_amount,elapsed_amount');
			$this->db->from('billingfor_sla');
			$this->db->where($check2);
			$q1 = $this->db->get();
			$result1 = $q1->result_array();
			if (!empty($result1))
				{
				$this->db->where($where2);
				$this->db->update('billingfor_sla', $data2);
				}
			  else
				{
				$this->db->insert('billingfor_sla', $where2);
				$this->db->where($where2);
				$this->db->update('billingfor_sla', $data2);
				}

			$this->db->select('sla_amount,elapsed_amount');
			$this->db->from('billingfor_sla');
			$this->db->where($check3);
			$q2 = $this->db->get();
			$result2 = $q2->result_array();
			if (!empty($result2))
				{
				$this->db->where($where3);
				$this->db->update('billingfor_sla', $data3);
				}
			  else
				{
				$this->db->insert('billingfor_sla', $where3);
				$this->db->where($where3);
				$this->db->update('billingfor_sla', $data3);
				}

			$this->db->select('sla_amount,elapsed_amount');
			$this->db->from('billingfor_sla');
			$this->db->where($check4);
			$q3 = $this->db->get();
			$result3 = $q3->result_array();
			if (!empty($result3))
				{
				$this->db->where($where4);
				$this->db->update('billingfor_sla', $data4);
				}
			  else
				{
				$this->db->insert('billingfor_sla', $where4);
				$this->db->where($where4);
				$this->db->update('billingfor_sla', $data4);
				}
			}

		return true;
		}

	public function getsubproduct()
		{
		$this->db->select('*,category_details.id as a');
		$this->db->from('category_details');
		$this->db->join('product_management', 'product_management.product_id = category_details.prod_id');
		$this->db->order_by("category_details.id", "desc");
		$query = $this->db->get();
		return $query;
		}

	public function get_subproduct_details($id)
		{
		$this->db->select('*');
		$this->db->from('category_details');
		$this->db->where('prod_id', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			return $query->result_array();
			}
		  else
			{
			return 1;
			}
		}

	public function getspare()
		{
		$this->db->select('*,spare.id as a');
		$this->db->from('spare');
		$this->db->join('product_management', 'product_management.product_id = spare.product');
		$this->db->join('category_details', 'category_details.cat_id = spare.category');
		$this->db->order_by("a", "desc");
		$query = $this->db->get();
		return $query;
		}

	public function enter_pass($enc_password, $company, $to_mail)
		{
		$data_pass = array(
			'user_type' => 'Admin',
			'password' => $enc_password,
			'username' => $to_mail,
			'companyid' => $company
		);
		$this->db->insert('login', $data_pass);
		return true;
		}

	public function set_block($admin_id)
		{
$date = date('Y-m-d');
$this->db->select('admin_user.company_id,admin_user.renewal_date');
$this->db->where('admin_id', $admin_id);
$this->db->from('admin_user');
$query3 = $this->db->get();
$query_result = $query3->result_array();
$renew_date = "'".$query_result[0]['renewal_date']."'";
//echo $renew_date;
$today = strtotime($date);
$renew = strtotime($query_result[0]['renewal_date']);
//print_r($today);
//print_r($renew);
if($today > $renew){
//print_r($date);
return $query_result[0]['company_id'];
}
else if($today < $renew){
		$data = array(
			'blocked_status' => 0
		);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin_user', $data);
		return "updated";
		}
}

	public function set_unblock($admin_id)
		{
		$data = array(
			'blocked_status' => 1
		);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin_user', $data);
		return true;
		}

	public function sample_func($ticket_id)
		{
		$this->db->select('product_id,cat_id,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where('ticket_id', $ticket_id);
		$company = $this->db->get();
		$result = $company->row_array();
		echo json_encode($result);
		}

	public function productid_check($input)
		{
		$this->db->select('*');
		$this->db->from('product_management');
		$this->db->order_by("id", "desc");
		$this->db->where('company_id', $input);
		$query = $this->db->get();
		$id = $query->result_array();
		if (!$id == "")
			{
			return $id[0]['id'];
			}
		  else
			{
			return 0;
			}
		}

	public function search_state($name)
		{
		$this->db->like('state', $name, 'both');
		return $this->db->get('states')->result();
		}

	public function insertproduct($data)
		{
		$this->db->insert('product_management', $data);
		return true;
		}

	public function getproduct()
		{
		$this->db->select('*');
		$this->db->from('product_management');
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		return $query;
		}

	public function choose_company()
		{

		$blockedcompanyarray=array();
        $this->db->select('admin_user.company_id');
		$this->db->from('admin_user');
		$this->db->where('admin_user.blocked_status', 1);
		$this->db->group_by("admin_user.company_id ");
		$query_b1 = $this->db->get();
		$query_b   = $query_b1->result_array();
		foreach($query_b as $com){
			array_push($blockedcompanyarray, $com['company_id']);
		}


		$this->db->select('company_id,company_name');
		$this->db->from('company');
		$this->db->where_not_in('company.company_id', $blockedcompanyarray);
		$this->db->order_by("last_update", "desc");
		$company = $this->db->get();
		$result = $company->result_array();
		echo json_encode($result);
		}

	public function list_prod($company)
		{
		$this->db->select('product_id,product_name');
		$this->db->from('product_management');
		$this->db->where('company_id', $company);
		$company = $this->db->get();
		$result = $company->result_array();
		echo json_encode($result);
		}

	public function add_sla($adata1, $insert1)
		{
		array_merge($adata1, array(
			"ref_id" => $insert1
		));
		$this->db->insert('sla_mapping', $adata1);
		return true;
		}

	public function add_sla1($adata2)
		{
		$this->db->select('company_id,priority_level,product,category,cust_category,call_category,service_category');
		$this->db->from('sla_combination');
		$this->db->where($adata2);
		$company = $this->db->get();
		$result = $company->result_array();
		if (empty($result))
			{
			$ref_id = $this->get_reference_id($company_name);
			array_merge($adata2, array(
				"ref_id" => $ref_id
			));
			$this->db->insert('sla_combination', $adata2);
			return $ref_id;
			}
		  else
			{
			return "Already Exist!";
			}
		}

	public function submit_amount($data, $combo)
		{

		// $where=array('company_id'=>$comp,'product'=>$prod,'category'=>$cat,'priority'=>$prio);

		$this->db->select('sla_amount,elapsed_amount');
		$this->db->from('billingfor_sla');
		$this->db->where($combo);
		$q = $this->db->get();
		$result = $q->result_array();
		if ($result)
			{
			$this->db->where($combo);
			$this->db->update('billingfor_sla', $data);
			return true;
			}
		  else
			{
			$this->db->where($combo);
			$this->db->insert('billingfor_sla', $data);
			return true;
			}
		}

	public function slaa($company_id, $filter, $today, $start_of_the_week, $month, $quart, $start_of_yr)
		{
		$where1 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $start_of_the_week,
			'all_tickets.current_status' => 12
		);
		$where2 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $month,
			'all_tickets.current_status' => 12
		);
		$where3 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $quart,
			'all_tickets.current_status' => 12
		);
		$where4 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $start_of_yr,
			'all_tickets.current_status' => 12
		);
		$where5 = array(
			'sla_mapping.company_id=' => $company_id
		);
		$final = array();
		$element = array();
		$result = array();
		if ($filter == 'Filters' || $filter == 'Weekly')
			{
			$this->db->select('priority_level');
			$this->db->from('priority');
			$ticket = $this->db->get();
			$p = $ticket->result_array();
			$result = array();
			foreach($p as $rw)
				{
				$pr = 0;
				$total_num = 0;
				$ar1 = array();
				$ar2 = array();
				$target = 0;
				$average = 0;
				$sla_complaince = 0;
				$this->db->select('SLA_Compliance_Target');
				$this->db->from('sla_mapping');
				$this->db->where($where5);
				$this->db->where('priority_level', $rw['priority_level']);
				$p_level = $this->db->get();
				$priority = $p_level->result_array();
				for ($i = 0; $i < count($priority); $i++)
					{
					$total_num = $total_num + $priority[$i]['SLA_Compliance_Target'];
					}

				if (count($priority) != 0)
					{
					$average = $total_num / count($priority);
					}
				  else $average = 0;
				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where1);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$ticket = $this->db->get();
				$welcome = $ticket->result_array();
				foreach($welcome as $row)
					{
					$tick = $row['ticket_id'];
					$prod = $row['product_id'];
					$cat = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$act = $row['acceptance_time'];
					$ast = $row['assigned_time'];
					$assn = new DateTime($ast);
					$accp = new DateTime($act);
					$dteDif = $assn->diff($accp);
					$di = $dteDif->format("%Y-%m-%d %H:%I:%S");
					$dteStar = strtotime($ast);
					$dteDif = strtotime($act);
					$intervl = abs($dteDif - $dteStar);
					$minute = round($intervl / 60);
					$value1 = $this->convertToHoursMins($minute, '%02d:%02d:00');
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"ticket" => $tick,
						"prod" => $prod,
						"cat" => $cat,
						"priority_level" => $priority,
						"difference" => $value,
						"resp_dif" => $value1
					);

					// print_r($element);

					$this->db->select('sla_combination.ref_id');
					$this->db->from('sla_combination');
					$this->db->where('product', $prod);
					$this->db->where('category', $cat);
					$this->db->where('sla_combination.priority_level', $priority);
					$target = $this->db->get();
					$total = $target->result_array();
					if (!empty($total))
						{
						$this->db->select('response_time,resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('sla_mapping.ref_id', $total[0]['ref_id']);
						$query1 = $this->db->get();
						$query2 = $query1->result_array();

						// print_r($query2);

						foreach($query2 as $q)
							{
							if (($q['resolution_time'] >= $element['difference']) && ($q['response_time'] >= $element['resp_dif']))
								{
								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}

						$sla_complaince = count($ar1);
						}
					  else
						{
						$sla_complaince = 0;
						}
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where1);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$query = $this->db->get();
				$res = $query->num_rows();
				if ($res != 0)
					{
					$answer = 0;
					$percent = 0;
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$percent = number_format((float)$percent, 2, '.', '');
				$average = number_format((float)$average, 2, '.', '');
				$final = array(
					"priority" => $rw['priority_level'],
					"target" => $average,
					"sla" => $percent
				);
				array_push($result, $final);
				} //foreach priority
			}

		if ($filter == 'Monthly')
			{
			$this->db->select('priority_level');
			$this->db->from('priority');
			$ticket = $this->db->get();
			$p = $ticket->result_array();
			$result = array();
			foreach($p as $rw)
				{
				$pr = 0;
				$total_num = 0;
				$ar1 = array();
				$ar2 = array();
				$target = 0;
				$average = 0;
				$sla_complaince = 0;
				$this->db->select('SLA_Compliance_Target');
				$this->db->from('sla_mapping');
				$this->db->where($where5);
				$this->db->where('priority_level', $rw['priority_level']);
				$p_level = $this->db->get();
				$priority = $p_level->result_array();
				for ($i = 0; $i < count($priority); $i++)
					{
					$total_num = $total_num + $priority[$i]['SLA_Compliance_Target'];
					}

				if (count($priority) != 0)
					{
					$average = $total_num / count($priority);
					}
				  else
					{
					$average = 0;
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where2);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$ticket = $this->db->get();
				$welcome = $ticket->result_array();
				foreach($welcome as $row)
					{
					$tick = $row['ticket_id'];
					$prod = $row['product_id'];
					$cat = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$act = $row['acceptance_time'];
					$ast = $row['assigned_time'];
					$assn = new DateTime($ast);
					$accp = new DateTime($act);
					$dteDif = $assn->diff($accp);
					$di = $dteDif->format("%Y-%m-%d %H:%I:%S");
					$dteStar = strtotime($ast);
					$dteDif = strtotime($act);
					$intervl = abs($dteDif - $dteStar);
					$minute = round($intervl / 60);
					$value1 = $this->convertToHoursMins($minute, '%02d:%02d:00');
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"ticket" => $tick,
						"prod" => $prod,
						"cat" => $cat,
						"priority_level" => $priority,
						"difference" => $value,
						"resp_dif" => $value1
					);

					// print_r($element);

					$this->db->select('sla_combination.ref_id');
					$this->db->from('sla_combination');
					$this->db->where('product', $prod);
					$this->db->where('category', $cat);
					$this->db->where('sla_combination.priority_level', $priority);
					$target = $this->db->get();
					$total = $target->result_array();
					if (!empty($total))
						{
						$this->db->select('response_time,resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('sla_mapping.ref_id', $total[0]['ref_id']);
						$query1 = $this->db->get();
						$query2 = $query1->result_array();
						foreach($query2 as $q)
							{
							if (($q['resolution_time'] >= $element['difference']) && ($q['response_time'] >= $element['resp_dif']))
								{
								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}

						$sla_complaince = count($ar1);
						}
					  else
						{
						$sla_complaince = 0;
						}
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where2);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$query = $this->db->get();
				$res = $query->num_rows();
				if ($res != 0)
					{
					$answer = 0;
					$percent = 0;
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$percent = number_format((float)$percent, 2, '.', '');
				$average = number_format((float)$average, 2, '.', '');
				$final = array(
					"priority" => $rw['priority_level'],
					"target" => $average,
					"sla" => $percent
				);
				array_push($result, $final);
				} //foreach priority
			}

		if ($filter == 'Quarterly')
			{
			$this->db->select('priority_level');
			$this->db->from('priority');
			$ticket = $this->db->get();
			$p = $ticket->result_array();
			$result = array();
			foreach($p as $rw)
				{
				$pr = 0;
				$total_num = 0;
				$ar1 = array();
				$ar2 = array();
				$target = 0;
				$average = 0;
				$sla_complaince = 0;
				$this->db->select('SLA_Compliance_Target');
				$this->db->from('sla_mapping');
				$this->db->where($where5);
				$this->db->where('priority_level', $rw['priority_level']);
				$p_level = $this->db->get();
				$priority = $p_level->result_array();
				for ($i = 0; $i < count($priority); $i++)
					{
					$total_num = $total_num + $priority[$i]['SLA_Compliance_Target'];
					}

				if (count($priority) != 0)
					{
					$average = $total_num / count($priority);
					}
				  else
					{
					$average = 0;
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where3);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$ticket = $this->db->get();
				$welcome = $ticket->result_array();
				foreach($welcome as $row)
					{
					$tick = $row['ticket_id'];
					$prod = $row['product_id'];
					$cat = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$act = $row['acceptance_time'];
					$ast = $row['assigned_time'];
					$assn = new DateTime($ast);
					$accp = new DateTime($act);
					$dteDif = $assn->diff($accp);
					$di = $dteDif->format("%Y-%m-%d %H:%I:%S");
					$dteStar = strtotime($ast);
					$dteDif = strtotime($act);
					$intervl = abs($dteDif - $dteStar);
					$minute = round($intervl / 60);
					$value1 = $this->convertToHoursMins($minute, '%02d:%02d:00');
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"ticket" => $tick,
						"prod" => $prod,
						"cat" => $cat,
						"priority_level" => $priority,
						"difference" => $value,
						"resp_dif" => $value1
					);

					// print_r($element);

					$this->db->select('sla_combination.ref_id');
					$this->db->from('sla_combination');
					$this->db->where('product', $prod);
					$this->db->where('category', $cat);
					$this->db->where('sla_combination.priority_level', $priority);
					$target = $this->db->get();
					$total = $target->result_array();
					if (!empty($total))
						{
						$this->db->select('response_time,resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('sla_mapping.ref_id', $total[0]['ref_id']);
						$query1 = $this->db->get();
						$query2 = $query1->result_array();
						foreach($query2 as $q)
							{
							if (($q['resolution_time'] >= $element['difference']) && ($q['response_time'] >= $element['resp_dif']))
								{
								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}

						$sla_complaince = count($ar1);
						}
					  else
						{
						$sla_complaince = 0;
						}
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where3);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$query = $this->db->get();
				$res = $query->num_rows();
				if ($res != 0)
					{
					$answer = 0;
					$percent = 0;
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$percent = number_format((float)$percent, 2, '.', '');
				$average = number_format((float)$average, 2, '.', '');
				$final = array(
					"priority" => $rw['priority_level'],
					"target" => $average,
					"sla" => $percent
				);
				array_push($result, $final);
				} //foreach priority
			}

		if ($filter == 'Annually')
			{
			$this->db->select('priority_level');
			$this->db->from('priority');
			$ticket = $this->db->get();
			$p = $ticket->result_array();
			$result = array();
			foreach($p as $rw)
				{
				$pr = 0;
				$total_num = 0;
				$ar1 = array();
				$ar2 = array();
				$target = 0;
				$average = 0;
				$sla_complaince = 0;
				$this->db->select('SLA_Compliance_Target');
				$this->db->from('sla_mapping');
				$this->db->where($where5);
				$this->db->where('priority_level', $rw['priority_level']);
				$p_level = $this->db->get();
				$priority = $p_level->result_array();
				for ($i = 0; $i < count($priority); $i++)
					{
					$total_num = $total_num + $priority[$i]['SLA_Compliance_Target'];
					}

				if (count($priority) != 0)
					{
					$average = $total_num / count($priority);
					}
				  else
					{
					$average = 0;
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where4);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$ticket = $this->db->get();
				$welcome = $ticket->result_array();
				foreach($welcome as $row)
					{
					$tick = $row['ticket_id'];
					$prod = $row['product_id'];
					$cat = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$act = $row['acceptance_time'];
					$ast = $row['assigned_time'];
					$assn = new DateTime($ast);
					$accp = new DateTime($act);
					$dteDif = $assn->diff($accp);
					$di = $dteDif->format("%Y-%m-%d %H:%I:%S");
					$dteStar = strtotime($ast);
					$dteDif = strtotime($act);
					$intervl = abs($dteDif - $dteStar);
					$minute = round($intervl / 60);
					$value1 = $this->convertToHoursMins($minute, '%02d:%02d:00');
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"ticket" => $tick,
						"prod" => $prod,
						"cat" => $cat,
						"priority_level" => $priority,
						"difference" => $value,
						"resp_dif" => $value1
					);

					// print_r($element);

					$this->db->select('sla_combination.ref_id');
					$this->db->from('sla_combination');
					$this->db->where('product', $prod);
					$this->db->where('category', $cat);
					$this->db->where('sla_combination.priority_level', $priority);
					$target = $this->db->get();
					$total = $target->result_array();
					if (!empty($total))
						{
						$this->db->select('response_time,resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('sla_mapping.ref_id', $total[0]['ref_id']);
						$query1 = $this->db->get();
						$query2 = $query1->result_array();
						foreach($query2 as $q)
							{
							if (($q['resolution_time'] >= $element['difference']) && ($q['response_time'] >= $element['resp_dif']))
								{
								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}

						$sla_complaince = count($ar1);
						}
					  else
						{
						$sla_complaince = 0;
						}
					}

				$this->db->select('ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time,acceptance_time,assigned_time');
				$this->db->from('all_tickets');
				$this->db->where($where4);
				$this->db->where('all_tickets.priority', $rw['priority_level']);
				$query = $this->db->get();
				$res = $query->num_rows();
				if ($res != 0)
					{
					$answer = 0;
					$percent = 0;
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$percent = number_format((float)$percent, 2, '.', '');
				$average = number_format((float)$average, 2, '.', '');
				$final = array(
					"priority" => $rw['priority_level'],
					"target" => $average,
					"sla" => $percent
				);
				array_push($result, $final);
				} //foreach priority
			}

		echo json_encode($result);
		}

	public function dashboard1($today, $month, $quart, $start_of_yr, $filter)
		{
		$where1 = array(
			'all_tickets.current_status' => 12
		);
		if ($filter == 'Month To Date')
			{
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$date = date('d');
			$final1 = array();
			$final2 = array();
			$final_rev = array();
			if ($mon == 'Apr' || $mon == 'Jun' || $mon == 'Sep' || $mon == 'Nov')
				{
				for ($i = 1; $i <= 30; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m--01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}

					if ($i <= date('d'))
						{
						$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
						$this->db->from('all_tickets');
						$this->db->where('all_tickets.last_update<=', $date);
						$this->db->where('all_tickets.last_update>=', $date2);
						$querys = $this->db->get();
						$rev_count1 = $querys->result_array();
						$test2 = 0;
						$test3 = 0;
						$sum1 = 0;
						foreach($rev_count1 as $row)
							{
							$company_id = $row['company_id'];
							$ticket_id = $row['ticket_id'];
							$product_id = $row['product_id'];
							$cat_id = $row['cat_id'];
							$priority = $row['priority'];
							$tst = $row['ticket_start_time'];
							$tet = $row['ticket_end_time'];
							$dteEnd = new DateTime($tet);
							$dteStart = new DateTime($tst);
							$dteDiff = $dteStart->diff($dteEnd);
							$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
							$dteStart = strtotime($tst);
							$dteDiff = strtotime($tet);
							$interval = abs($dteDiff - $dteStart);
							$minutes = round($interval / 60);
							$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
							$element = array(
								"company_id" => $company_id,
								"prod" => $product_id,
								"cat" => $cat_id,
								"priority_level" => $priority,
								"difference" => $value
							);
							$product = $product_id;
							$category = $cat_id;
							$priority_level = $priority;
							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('product', 'all');
							$this->db->group_end();
							$query = $this->db->get();
							$result = $query->result_array();
							if (!empty($result))
								{
								$product = 'all';
								}
							  else
								{
								$product = $product_id;
								}

							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('category', 'all');
							$this->db->group_end();
							$query1 = $this->db->get();
							$result1 = $query1->result_array();
							if (!empty($result1))
								{
								$category = 'all';
								}
							  else
								{
								$category = $cat_id;
								}

							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('priority_level', 'all');
							$this->db->group_end();
							$query1 = $this->db->get();
							$result1 = $query1->result_array();
							if (!empty($result1))
								{
								$priority_level = 'all';
								}
							  else
								{
								$priority_level = $priority;
								}

							$this->db->select('ref_id');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							if (empty($query))
								{
								$sum1 = $sum1 + 0;
								}
							  else
								{
								$refid = $query[0]['ref_id'];
								$this->db->select('resolution_time');
								$this->db->from('sla_mapping');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority_level', $priority_level);
								$this->db->where('ref_id', $refid);
								$query1 = $this->db->get();
								$query = $query1->result_array();
								foreach($query as $q)
									{
									$test2 = 0;
									$test3 = 0;
									if ($q['resolution_time'] >= $element['difference'])
										{
										$this->db->select('sla_amount');
										$this->db->from('billingfor_sla');
										$this->db->where('company_id', $element['company_id']);
										$this->db->where('priority', $priority_level);
										$this->db->where('product', $product);
										$this->db->where('category', $category);
										$query = $this->db->get();
										$amt = $query->result_array();
										if (!empty($amt))
											{
											$test2 = $test2 + $amt[0]['sla_amount'];
											}
										  else
											{
											$test2 = 0;
											}
										}
									  else
										{
										$this->db->select('elapsed_amount');
										$this->db->from('billingfor_sla');
										$this->db->where('company_id', $element['company_id']);
										$this->db->where('priority', $priority_level);
										$this->db->where('product', $element['prod']);
										$this->db->where('category', $element['cat']);
										$query = $this->db->get();
										$amt = $query->result_array();
										if (!empty($amt))
											{
											$test3 = $test3 + $amt[0]['elapsed_amount'];
											}
										  else
											{
											$test3 = 0;
											}
										}
									}

								$sum1 = $sum1 + $test2 + $test3;
								}
							}

						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
					  else
						{
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						);
						}

					array_push($final1, $rev_graph);
					}
				} //if
			  else if ($mon == 'Jan' || $mon == 'Mar' || $mon == 'May' || $mon == 'Jul' || $mon == 'Aug' || $mon == 'Oct' || $mon == 'Dec')
				{
					 
				for ($i = 1; $i <= 31; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

					
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
					  else
						{
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						);
						}

					array_push($final1, $rev_graph);
					}
				} // else if
				
			  else
				
				{
					 $year= date("Y");
					if( (0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400) )
					{
					for ($i = 1; $i <= 29; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
              if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

					    $rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
						else
						{
							  $rev_graph = array(
							    "profit" => $sum,
							    "Date" => $day
							 );
						}
					array_push($final1, $rev_graph);
					}
				}
				else 
				{
				for ($i = 1; $i <= 28; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
  if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						 );
						}
						else
						{
							$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						   );
						}
					array_push($final1, $rev_graph);
					}
				 }
				} //else
			echo json_encode($final1);
			} //end if
			
		  else if ($filter == 'Quarter To Date')
			{
			$month_num=date("n");
				
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$final1 = array();
			$final2 = array();
			$final_rev = array();
				
			$date = date('Y-01-01 00:00:00');
			$date2 = date('Y-03-t 23:59:00');
			$date3 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$date1 = date('Y-03-t 23:59:00', strtotime("-1 year"));
			$q1 = 'Q1 (JFM)';
				
			$dateq2 = date('Y-01-01 00:00:00');
			$dateq22 = date('Y-06-t 23:59:00');
			$dateq23 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq21 = date('Y-06-t 23:59:00', strtotime("-1 year"));
			$q2 = 'Q2 (AMJ)';
				
			$dateq3 = date('Y-01-01 00:00:00');
			$dateq32 = date('Y-09-t 23:59:00');
			$dateq33 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq31 = date('Y-09-t 23:59:00', strtotime("-1 year"));
			$q3 = 'Q3 (JAS)';
				
			$dateq4 = date('Y-01-01 00:00:00');
			$dateq42 = date('Y-12-t 23:59:00');
			$dateq43 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq41 = date('Y-12-t 23:59:00', strtotime("-1 year"));
			$q4 = 'Q4 (OND)';
			
		if($month_num==1 || $month_num==2 || $month_num==3)
		{
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
				// last year Q2	
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq2 = 0;
			$test1q2 = 0;
			$sumq2 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q2 = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}
			$rev_graphq2 = array(
					"profit" => $sumq2,
					"Date" => $q2
				);
			
			array_push($final1, $rev_graphq2);
			
			//last year q3
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3
			);
			array_push($final1,$rev_graph3);
			
			//last year q4
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}
				$rev_graph4 = array(
				   "profit" => $sumq4,
				   "Date" => $q4
			   );
			array_push($final1, $rev_graph4);
			echo json_encode($final1);

				
		}
		// Quarter 2
         else if($month_num==4 || $month_num==5 || $month_num==6)
		  {
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq2 = 0;
			$test1q2 = 0;
			$sumq2 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test12q = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq2);
			$this->db->where('all_tickets.last_update<=', $dateq22);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2q2 = 0;
			$test3q2 = 0;
			$sum1q2 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q2 = $sum1q2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q2 = 0;
						$test3q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q2 = $test2q2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q2 = $test3q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q2 = 0;
								}
							}
						}

					$sum1q2 = $sum1q2 + $test2q2 + $test3q2;
					}
				}

			$rev_graph1 = array(
				"profit" => $sumq2,
				"Date" => $q2,
				"profit_now" => $sum1q2
			);
 
			array_push($final1, $rev_graph1);
			
			//last year q3
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3
			);
			array_push($final1,$rev_graph3);
			
			//last year q4
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}
				$rev_graph4 = array(
				   "profit" => $sumq4,
				   "Date" => $q4
			   );
			array_push($final1, $rev_graph4);
			echo json_encode($final1);
		  }

			// Quarter 3
		else if($month_num==7 || $month_num==8 || $month_num==9)
		{
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test12q = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq2);
			$this->db->where('all_tickets.last_update<=', $dateq22);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			$sumq2 = 0;
			$sum1q2 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q2 = $sum1q2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q2 = 0;
						$test3q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q2 = $test2q2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q2 = $test3q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q2 = 0;
								}
							}
						}

					$sum1q2 = $sum1q2 + $test2q2 + $test3q2;
					}
				}

			$rev_graph1 = array(
				"profit" => $sumq2,
				"Date" => $q2,
				"profit_now" => $sum1q2
			);
 
			array_push($final1, $rev_graph1);
			

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
				
  
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq3);
			$this->db->where('all_tickets.last_update<=', $dateq32);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2q3 = 0;
			$test3q3 = 0;
			$sum1q3 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q3 = $sum1q3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q3 = 0;
						$test3q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q3 = $test2q3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q3 = $test3q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q3 = 0;
								}
							}
						}

					$sum1q3 = $sum1q3 + $test2q3 + $test3q3;
					}
				}

			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3,
				"profit_now" => $sum1q3
			);
   
			array_push($final1, $rev_graph3);
			
		//last year q4		
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}

				$rev_graph4 = array(
				"profit" => $sumq4,
				"Date" => $q4
			);
   
			array_push($final1, $rev_graph4);
				echo json_encode($final1);
			}
			// Quarter 4
      else
	  {
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq4);
			$this->db->where('all_tickets.last_update<=', $dateq42);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

			$rev_graph3 = array(
				"profit" => $sum,
				"Date" => $q4,
				"profit_now" => $sum1
			);
			array_push($final1, $rev_graph3);
			echo json_encode($final1);
			} //end else if
		}
			
		else if ($filter == 'Year To Date')
			{
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$final1 = array();
			$final2 = array();
			$final_rev = array();

			// if($mon =='Jan' || $mon =='Mar' || $mon =='May' || $mon =='July' || $mon =='Aug' || $mon =='Oct' || $mon =='Dec' ) {

			for ($i = 1; $i <= 12; $i++)
				{
			if ($i < 10)
					{
						 $monthnum=date("n");
						if($monthnum<0)
						{
							$monthnum = '0' . $monthnum;
						}
						else
						{
							$monthnum =  $monthnum;
						}
					$i = '0' . $i;
					$d = date('Y-' . $i . '-d');
					$d = new DateTime($d);
				//	$date = date('Y-' . $i . '-t 23:59:00');
					$abc = $d->format('Y-' . $i . '-t 23:59:00');
				
			       $date = date("Y-m-d 23:59:00", mktime(0, 0, 0, $i+1,0,date("Y")));
	 			   $date2 = date('Y-01-01 00:00:00');
	 			  // $date2 = date('Y-' . $i . '-01 00:00:00');
						
						$dat = new DateTime($date);
					$abc1 = $dat->format('Y-m-d H:i:s');	
						
				    $date1 = date("Y-m-d 23:59:00" , strtotime("-1 year", strtotime($abc1)));
				    $date3 = date('Y-01-01 00:00:00', strtotime("-1 year"));
				  // $date3 = date('Y-' . $i . '-01 00:00:00', strtotime("-1 year"));
						
					// $yr=date("Y-".$i."-t 00:00:00");
					// $date1=date($yr,strtotime("-1 year"));
					// $d =  date( '2017-04-03' );
					// $d =$d->format( 'Y-m-t' );
					// $date1=date($d,strtotime("-1 year"));

					 $day = 'Day ' . $i;
					$monthNum = $i;
					$dateObj = DateTime::createFromFormat('!m', $monthNum);
					$monthName = $dateObj->format('F');
					}
				  else
					{ 
					$d = date('Y-' . $i . '-d');
					$d = new DateTime($d);
					$date = $d->format('Y-' . $i . '-t 23:59:00');
					$date2 = date('Y-' . $i . '-01 00:00:00');
						
					$date3 = date('Y-' . $i . '-01 00:00:00', strtotime("-1 year"));
					$date1 = date($date, strtotime("-1 year"));
					$day = 'Day ' . $i;
					$monthNum = $i;
					$dateObj = DateTime::createFromFormat('!m', $monthNum);
					$monthName = $dateObj->format('F');
					} 

				$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where1);
				$this->db->where('all_tickets.last_update<=', $date1);
				$this->db->where('all_tickets.last_update>=', $date3);
				$querys = $this->db->get();
				$rev_count = $querys->result_array();
				$test = 0;
				$test1 = 0;
				$sum = 0;
				foreach($rev_count as $row)
					{
					$company_id = $row['company_id'];
					$ticket_id = $row['ticket_id'];
					$product_id = $row['product_id'];
					$cat_id = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"company_id" => $company_id,
						"prod" => $product_id,
						"cat" => $cat_id,
						"priority_level" => $priority,
						"difference" => $value
					);
					$product = $product_id;
					$category = $cat_id;
					$priority_level = $priority;
					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('product', 'all');
					$this->db->group_end();
					$query = $this->db->get();
					$result = $query->result_array();
					if (!empty($result))
						{
						$product = 'all';
						}
					  else
						{
						$product = $product_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('category', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$category = 'all';
						}
					  else
						{
						$category = $cat_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('priority_level', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$priority_level = 'all';
						}
					  else
						{
						$priority_level = $priority;
						}

					$this->db->select('ref_id');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('product', $product);
					$this->db->where('category', $category);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					if (empty($query))
						{
						$sum = $sum + 0;
						}
					  else
						{
						$refid = $query[0]['ref_id'];
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('ref_id', $refid);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						foreach($query as $q)
							{
							$test = 0;
							$test1 = 0;
							if ($q['resolution_time'] >= $element['difference'])
								{
								$this->db->select('sla_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $product);
								$this->db->where('category', $category);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test = $test + $amt[0]['sla_amount'];
									}
								  else
									{
									$test = 0;
									}
								}
							  else
								{
								$this->db->select('elapsed_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $element['prod']);
								$this->db->where('category', $element['cat']);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test1 = $test1 + $amt[0]['elapsed_amount'];
									}
								  else
									{
									$test1 = 0;
									}
								}
							}

						$sum = $sum + $test + $test1;
						}
					}
   if($monthnum>=$i)
  {
				$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where('all_tickets.last_update<=', $date);
				$this->db->where('all_tickets.last_update>=', $date2);
				$querys = $this->db->get();
				$rev_count1 = $querys->result_array();
				$test2 = 0;
				$test3 = 0;
				$sum1 = 0;
				foreach($rev_count1 as $row)
					{
					$company_id = $row['company_id'];
					$ticket_id = $row['ticket_id'];
					$product_id = $row['product_id'];
					$cat_id = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"company_id" => $company_id,
						"prod" => $product_id,
						"cat" => $cat_id,
						"priority_level" => $priority,
						"difference" => $value
					);
					$product = $product_id;
					$category = $cat_id;
					$priority_level = $priority;
					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('product', 'all');
					$this->db->group_end();
					$query = $this->db->get();
					$result = $query->result_array();
					if (!empty($result))
						{
						$product = 'all';
						}
					  else
						{
						$product = $product_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('category', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$category = 'all';
						}
					  else
						{
						$category = $cat_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('priority_level', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$priority_level = 'all';
						}
					  else
						{
						$priority_level = $priority;
						}

					$this->db->select('ref_id');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('product', $product);
					$this->db->where('category', $category);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					if (empty($query))
						{
						$sum1 = $sum1 + 0;
						}
					  else
						{
						$refid = $query[0]['ref_id'];
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('ref_id', $refid);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						foreach($query as $q)
							{
							$test2 = 0;
							$test3 = 0;
							if ($q['resolution_time'] >= $element['difference'])
								{
								$this->db->select('sla_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $product);
								$this->db->where('category', $category);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test2 = $test2 + $amt[0]['sla_amount'];
									}
								  else
									{
									$test2 = 0;
									}
								}
							  else
								{
								$this->db->select('elapsed_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $element['prod']);
								$this->db->where('category', $element['cat']);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test3 = $test3 + $amt[0]['elapsed_amount'];
									}
								  else
									{
									$test3 = 0;
									}
								}
							}

						$sum1 = $sum1 + $test2 + $test3;
						}
					}

				$rev_graph_ytd = array(
					"profit" => $sum,
					"profit_now" => $sum1,
					"Date" => $monthName
				);
             }
			else
			{
				$rev_graph_ytd = array(
					"profit" => $sum,
					"Date" => $monthName
				);
			}
				array_push($final1, $rev_graph_ytd);
				}

			// }

			echo json_encode($final1);
			} //end if (YTD)
		}
			
				
	public function fetch()
		{
		$this->db->select('admin_user.company_id');
		$this->db->distinct('company_id,company_name');
		$this->db->from('admin_user');
		$this->db->order_by("last_update", "DESC");

		// $this->db->join('company','company.company_id=admin_user.company_id');

		$query3 = $this->db->get();
		$query4 = $query3->result_array();
		$result = array();
		foreach($query4 as $row1)
			{
			$where = array(
				'admin_user.company_id' => $row1['company_id']
			);
			$this->db->select('admin_id,admin_user.company_id,admin_user.company_name,service_desk,technicians,admin_user.start_date,
admin_user.renewal_date,blocked_status,company.company_logo');
			$this->db->where($where);
			$this->db->from('admin_user');
			$this->db->join('company', 'company.company_id=admin_user.company_id');
			$this->db->order_by('admin_user.last_update', 'desc');

			// $this->db->limit(1);

			$query1 = $this->db->get();
			$query = $query1->result_array();
			foreach($query as $row)
				{
				$admin_id = $row['admin_id'];
				$company_id = $row['company_id'];
				$company_logo = $row['company_logo'];
				$company = $row['company_name'];
				$tech = $row['technicians'];
				$serv = $row['service_desk'];
				$date = $row['start_date'];
				$future = $row['renewal_date'];
				$block = $row['blocked_status'];
				$array = array(
					'company_id' => $company_id,
					'role' => 'ServiceDesk'
				);
				$this->db->select('technician_id');
				$this->db->from('technician');
				$this->db->where('company_id', $company_id);
				$query_t = $this->db->get();
				$res = $query_t->num_rows();

				// echo $res;

				$this->db->select('first_name');
				$this->db->from('user');
				$this->db->where($array);
				$query_s = $this->db->get();
				$result1 = $query_s->num_rows();

				// $total=$tech+$serv;

				array_push($result, array(
					"admin_id" => $admin_id,
					"company_id" => $company_id,
					"company_name" => $company,
					"company_logo" => $company_logo,
					"technicians" => $tech,
					"service_desk" => $serv,
					"tech_used" => $res,
					"serv_used" => $result1,
					"start_date" => $date,
					"renewal_date" => $future,
					"block" => $block
				));
				}
			}

		// print_r($query);

		echo json_encode($result, true);
		}

	public function count1()
		{
		$this->db->select('technicians');
		$this->db->from('admin_user');
		$query = $this->db->get();
		$query1 = $query->result_array();
		$total = 0;
		foreach($query1 as $row)
			{
			$tech = $row['technicians'];
			$total+= $tech;
			}

		// $elements = array("count"=> $total);
		// array_push($result, $elements);

		return $total;
		}

	public function counts()
		{
		$this->db->select('service_desk');
		$this->db->from('admin_user');
		$query = $this->db->get();
		$query1 = $query->result_array();
		$final = 0;
		foreach($query1 as $row)
			{
			$serv = $row['service_desk'];
			$final = $final + $serv;
			}

		// $elements = array("count"=> $total);
		// array_push($result, $elements);

		return $final;
		}

	public function count_t()
		{
		$total_tech = 0;
		$technician = 0;
		$this->db->select('company_id');
		$this->db->distinct('company_id');
		$this->db->from('admin_user');
		$result = $this->db->get();
		$res = $result->result_array();
		foreach($res as $r)
			{
			$this->db->select('technicians');
			$this->db->from('admin_user');
			$this->db->order_by('last_update', 'desc');
			$this->db->where('company_id', $r['company_id']);
			$this->db->where('admin_user.blocked_status=', "0");
			$count = $this->db->get();
			$total = $count->result_array();
			foreach($total as $row)
				{
				$company_id = $r['company_id'];
				$technician = $row['technicians'];
				$total_tech = $total_tech + $technician;
				}
			}

		return $total_tech;
		}

	public function count_s()
		{
		$total_serv = 0;
		$service = 0;
		$this->db->select('company_id');
		$this->db->distinct('company_id');
		$this->db->from('admin_user');
		$result = $this->db->get();
		$res = $result->result_array();
		foreach($res as $r)
			{
			$this->db->select('service_desk');
			$this->db->from('admin_user');
			$this->db->order_by('last_update', 'desc');
			$this->db->where('company_id', $r['company_id']);
			$this->db->where('admin_user.blocked_status=', "0");
			$count = $this->db->get();
			$total = $count->result_array();
			foreach($total as $row)
				{
				$company_id = $r['company_id'];
				$service = $row['service_desk'];
				$total_serv = $total_serv + $service;
				}
			}

		return $total_serv;
		}

	public function company_count()
		{
		$this->db->select('company_id');
		$this->db->from('company');
		$this->db->distinct('company_id');
		$query = $this->db->get();
		$res = $query->num_rows();
		return $res;
		}

	public function renew_count()
		{
		$date = date('Y-m-d 00:00:00');
		$rdate = date('Y-m-d 23:59:00', mktime(0, 0, 0, date('m') , date('d') + 3, date('Y')));
		$rdata = array(
			'renewal_date<=' => $rdate,
			'renewal_date>=' => $date
		);

		// print_r($rdata);
		// exit;

		$this->db->select('company_id');
		$this->db->from('admin_user');
		$this->db->distinct('company_id');
		$this->db->order_by('admin_user.last_update', 'desc');
		$this->db->where($rdata);
		$query = $this->db->get();
		$result = $query->num_rows();
		return $result;
		}

	public function display_company()
		{
		$this->db->select('company_id');
		$this->db->distinct('company_id,company_name');
		$this->db->from('admin_user');
		$query3 = $this->db->get();
		$query4 = $query3->result_array();
		$result = array();
		foreach($query4 as $row1)
			{
			$where = array(
				'company_id' => $row1['company_id']
			);
			$this->db->select('company_id,company_name,renewal_date');
			$this->db->where($where);
			$this->db->from('admin_user');
			$this->db->order_by('last_update', 'desc');
			$this->db->limit(1);
			$query1 = $this->db->get();
			$query = $query1->result_array();
			foreach($query as $row)
				{
				$company_id = $row['company_id'];
				$company = $row['company_name'];
				$future = $row['renewal_date'];
				array_push($result, array(
					"company_id" => $company_id,
					"company_name" => $company,
					"renewal_date" => $future
				));
				}
			}

		echo json_encode($result, true);
		}

	public function convertToHoursMins($time, $format = '%02d:%02d')
		{
		if ($time < 1)
			{
			return;
			}

		$hours = floor($time / 60);
		$minutes = ($time % 60);
		return sprintf($format, $hours, $minutes);
		}

	public function select_company($company_id, $today, $month, $quart, $start_of_yr, $date, $prev_month, $prev_quart, $prevstart_of_yr)
		{
		$where1 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $month,
			'all_tickets.current_status' => 12
		);
		$where2 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $quart,
			'all_tickets.current_status' => 12
		);
		$where3 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $start_of_yr,
			'all_tickets.current_status' => 12
		);
		$where4 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $date,
			'all_tickets.last_update>=' => $prev_month,
			'all_tickets.current_status' => 12
		);
		$where5 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $date,
			'all_tickets.last_update>=' => $prev_quart,
			'all_tickets.current_status' => 12
		);
		$where6 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $date,
			'all_tickets.last_update>=' => $prevstart_of_yr,
			'all_tickets.current_status' => 12
		);
		$final = array();
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where1);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue);
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where2);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue); //echo $test+10;
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where3);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue); //echo $test+10;
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where4);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue);
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where5);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue);
		$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
		$this->db->from('all_tickets');
		$this->db->where($where6);
		$querys = $this->db->get();
		$rev_count = $querys->result_array();
		$test = 0;
		$test1 = 0;
		if (!empty($rev_count))
			{
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$cal_cat = $row['call_tag'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"call_category" => $cal_cat,
					"priority_level" => $priority,
					"difference" => $value
				);
				$this->db->select('resolution_time');
				$this->db->from('sla_mapping');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $element['priority_level']);
				$query = $this->db->get();
				$query = $query->result_array();
				foreach($query as $q)
					{

					// echo json_encode($query);

					if ($q['resolution_time'] >= $element['difference'])
						{

						// echo "if \n";

						$this->db->select('sla_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test = $test + $amt['sla_amount'];

						// echo $test."\n";

						}
					  else
						{

						// echo "else \n";

						$this->db->select('elapsed_amount');
						$this->db->from('billingfor_sla');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('product', $element['prod']);
						$this->db->where('category', $element['cat']);
						$this->db->where('cal_category', $element['call_category']);
						$query = $this->db->get();
						$amt = $query->row_array();
						$test1 = $test1 + $amt['elapsed_amount'];

						// echo $test1."\n";

						}
					}
				}

			$sum = $test + $test1;
			}
		  else
			{
			$sum = 0;
			}

		$revenue = array(
			"company_id" => $company_id,
			"revenue" => $sum
		);
		array_push($final, $revenue);
		print_r(json_encode($final));
		}

	public function priority($company)
		{
		$this->db->select('priority_level');
		$this->db->from('sla_mapping');
		$this->db->where('company_id', $company);
		$querys = $this->db->get();
		$priority = $querys->result_array();
		echo json_encode($priority);
		}

	public function revenue_graph($filter, $company)
		{
		$where1 = array(
			'all_tickets.current_status' => 12,
			'all_tickets.company_id' => $company
		);
			if ($filter == 'Month To Date')
			{
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$date = date('d');
			$final1 = array();
			$final2 = array();
			$final_rev = array();
			if ($mon == 'Apr' || $mon == 'Jun' || $mon == 'Sep' || $mon == 'Nov')
				{
				for ($i = 1; $i <= 30; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m--01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}

					if ($i <= date('d'))
						{
						$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
						$this->db->from('all_tickets');
						$this->db->where('all_tickets.last_update<=', $date);
						$this->db->where('all_tickets.last_update>=', $date2);
						$querys = $this->db->get();
						$rev_count1 = $querys->result_array();
						$test2 = 0;
						$test3 = 0;
						$sum1 = 0;
						foreach($rev_count1 as $row)
							{
							$company_id = $row['company_id'];
							$ticket_id = $row['ticket_id'];
							$product_id = $row['product_id'];
							$cat_id = $row['cat_id'];
							$priority = $row['priority'];
							$tst = $row['ticket_start_time'];
							$tet = $row['ticket_end_time'];
							$dteEnd = new DateTime($tet);
							$dteStart = new DateTime($tst);
							$dteDiff = $dteStart->diff($dteEnd);
							$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
							$dteStart = strtotime($tst);
							$dteDiff = strtotime($tet);
							$interval = abs($dteDiff - $dteStart);
							$minutes = round($interval / 60);
							$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
							$element = array(
								"company_id" => $company_id,
								"prod" => $product_id,
								"cat" => $cat_id,
								"priority_level" => $priority,
								"difference" => $value
							);
							$product = $product_id;
							$category = $cat_id;
							$priority_level = $priority;
							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('product', 'all');
							$this->db->group_end();
							$query = $this->db->get();
							$result = $query->result_array();
							if (!empty($result))
								{
								$product = 'all';
								}
							  else
								{
								$product = $product_id;
								}

							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('category', 'all');
							$this->db->group_end();
							$query1 = $this->db->get();
							$result1 = $query1->result_array();
							if (!empty($result1))
								{
								$category = 'all';
								}
							  else
								{
								$category = $cat_id;
								}

							$this->db->select('*');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $company_id);
							$this->db->group_start();
							$this->db->like('priority_level', 'all');
							$this->db->group_end();
							$query1 = $this->db->get();
							$result1 = $query1->result_array();
							if (!empty($result1))
								{
								$priority_level = 'all';
								}
							  else
								{
								$priority_level = $priority;
								}

							$this->db->select('ref_id');
							$this->db->from('sla_combination');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							if (empty($query))
								{
								$sum1 = $sum1 + 0;
								}
							  else
								{
								$refid = $query[0]['ref_id'];
								$this->db->select('resolution_time');
								$this->db->from('sla_mapping');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority_level', $priority_level);
								$this->db->where('ref_id', $refid);
								$query1 = $this->db->get();
								$query = $query1->result_array();
								foreach($query as $q)
									{
									$test2 = 0;
									$test3 = 0;
									if ($q['resolution_time'] >= $element['difference'])
										{
										$this->db->select('sla_amount');
										$this->db->from('billingfor_sla');
										$this->db->where('company_id', $element['company_id']);
										$this->db->where('priority', $priority_level);
										$this->db->where('product', $product);
										$this->db->where('category', $category);
										$query = $this->db->get();
										$amt = $query->result_array();
										if (!empty($amt))
											{
											$test2 = $test2 + $amt[0]['sla_amount'];
											}
										  else
											{
											$test2 = 0;
											}
										}
									  else
										{
										$this->db->select('elapsed_amount');
										$this->db->from('billingfor_sla');
										$this->db->where('company_id', $element['company_id']);
										$this->db->where('priority', $priority_level);
										$this->db->where('product', $element['prod']);
										$this->db->where('category', $element['cat']);
										$query = $this->db->get();
										$amt = $query->result_array();
										if (!empty($amt))
											{
											$test3 = $test3 + $amt[0]['elapsed_amount'];
											}
										  else
											{
											$test3 = 0;
											}
										}
									}

								$sum1 = $sum1 + $test2 + $test3;
								}
							}

						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
					  else
						{
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						);
						}

					array_push($final1, $rev_graph);
					}
				} //if
			  else if ($mon == 'Jan' || $mon == 'Mar' || $mon == 'May' || $mon == 'Jul' || $mon == 'Aug' || $mon == 'Oct' || $mon == 'Dec')
				{
					 
				for ($i = 1; $i <= 31; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-01 00:00:00');
						$date3 = date('Y-m-01 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

					
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
					  else
						{
						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						);
						}

					array_push($final1, $rev_graph);
					}
				} // else if
				
			  else
				
				{
					 $year= date("Y");
					if( (0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400) )
					{
					for ($i = 1; $i <= 29; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
              if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

					    $rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						);
						}
						else
						{
							  $rev_graph = array(
							    "profit" => $sum,
							    "Date" => $day
							 );
						}
					array_push($final1, $rev_graph);
					}
				}
				else 
				{
				for ($i = 1; $i <= 28; $i++)
					{
					if ($i < 10)
						{
						$i = '0' . $i;
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}
					  else
						{
						$date = date('Y-m-' . $i . ' 23:59:00');
						$date2 = date('Y-m-' . $i . ' 00:00:00');
						$date3 = date('Y-m-' . $i . ' 00:00:00', strtotime("-1 year"));
						$date1 = date('Y-m-' . $i . ' 23:59:00', strtotime("-1 year"));
						$day = $i;
						}

					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where($where1);
					$this->db->where('all_tickets.last_update<=', $date1);
					$this->db->where('all_tickets.last_update>=', $date3);
					$querys = $this->db->get();
					$rev_count = $querys->result_array();
					$test = 0;
					$test1 = 0;
					$sum = 0;
					foreach($rev_count as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum = $sum + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test = 0;
								$test1 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test = $test + $amt[0]['sla_amount'];
										}
									  else
										{
										$test = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test1 = $test1 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test1 = 0;
										}
									}
								}

							$sum = $sum + $test + $test1;
							}
						}
  if ($i <= date('d'))
						{
					$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
					$this->db->from('all_tickets');
					$this->db->where('all_tickets.last_update<=', $date);
					$this->db->where('all_tickets.last_update>=', $date2);
					$querys = $this->db->get();
					$rev_count1 = $querys->result_array();
					$test2 = 0;
					$test3 = 0;
					$sum1 = 0;
					foreach($rev_count1 as $row)
						{
						$company_id = $row['company_id'];
						$ticket_id = $row['ticket_id'];
						$product_id = $row['product_id'];
						$cat_id = $row['cat_id'];
						$priority = $row['priority'];
						$tst = $row['ticket_start_time'];
						$tet = $row['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"priority_level" => $priority,
							"difference" => $value
						);
						$product = $product_id;
						$category = $cat_id;
						$priority_level = $priority;
						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('product', 'all');
						$this->db->group_end();
						$query = $this->db->get();
						$result = $query->result_array();
						if (!empty($result))
							{
							$product = 'all';
							}
						  else
							{
							$product = $product_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('category', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$category = 'all';
							}
						  else
							{
							$category = $cat_id;
							}

						$this->db->select('*');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $company_id);
						$this->db->group_start();
						$this->db->like('priority_level', 'all');
						$this->db->group_end();
						$query1 = $this->db->get();
						$result1 = $query1->result_array();
						if (!empty($result1))
							{
							$priority_level = 'all';
							}
						  else
							{
							$priority_level = $priority;
							}

						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('product', $product);
						$this->db->where('category', $category);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						if (empty($query))
							{
							$sum1 = $sum1 + 0;
							}
						  else
							{
							$refid = $query[0]['ref_id'];
							$this->db->select('resolution_time');
							$this->db->from('sla_mapping');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority_level', $priority_level);
							$this->db->where('ref_id', $refid);
							$query1 = $this->db->get();
							$query = $query1->result_array();
							foreach($query as $q)
								{
								$test2 = 0;
								$test3 = 0;
								if ($q['resolution_time'] >= $element['difference'])
									{
									$this->db->select('sla_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $product);
									$this->db->where('category', $category);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test2 = $test2 + $amt[0]['sla_amount'];
										}
									  else
										{
										$test2 = 0;
										}
									}
								  else
									{
									$this->db->select('elapsed_amount');
									$this->db->from('billingfor_sla');
									$this->db->where('company_id', $element['company_id']);
									$this->db->where('priority', $priority_level);
									$this->db->where('product', $element['prod']);
									$this->db->where('category', $element['cat']);
									$query = $this->db->get();
									$amt = $query->result_array();
									if (!empty($amt))
										{
										$test3 = $test3 + $amt[0]['elapsed_amount'];
										}
									  else
										{
										$test3 = 0;
										}
									}
								}

							$sum1 = $sum1 + $test2 + $test3;
							}
						}

						$rev_graph = array(
							"profit" => $sum,
							"Date" => $day,
							"profit_now" => $sum1
						 );
						}
						else
						{
							$rev_graph = array(
							"profit" => $sum,
							"Date" => $day
						   );
						}
					array_push($final1, $rev_graph);
					}
				 }
				} //else
			echo json_encode($final1);
			} //end if
			
		  else if ($filter == 'Quarter To Date')
			{
			$month_num=date("n");
				
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$final1 = array();
			$final2 = array();
			$final_rev = array();
				
			$date = date('Y-01-01 00:00:00');
			$date2 = date('Y-03-t 23:59:00');
			$date3 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$date1 = date('Y-03-t 23:59:00', strtotime("-1 year"));
			$q1 = 'Q1 (JFM)';
				
			$dateq2 = date('Y-01-01 00:00:00');
			$dateq22 = date('Y-06-t 23:59:00');
			$dateq23 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq21 = date('Y-06-t 23:59:00', strtotime("-1 year"));
			$q2 = 'Q2 (AMJ)';
				
			$dateq3 = date('Y-01-01 00:00:00');
			$dateq32 = date('Y-09-t 23:59:00');
			$dateq33 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq31 = date('Y-09-t 23:59:00', strtotime("-1 year"));
			$q3 = 'Q3 (JAS)';
				
			$dateq4 = date('Y-01-01 00:00:00');
			$dateq42 = date('Y-12-t 23:59:00');
			$dateq43 = date('Y-01-01 00:00:00', strtotime("-1 year"));
			$dateq41 = date('Y-12-t 23:59:00', strtotime("-1 year"));
			$q4 = 'Q4 (OND)';
			
		if($month_num==1 || $month_num==2 || $month_num==3)
		{
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
				// last year Q2	
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq2 = 0;
			$test1q2 = 0;
			$sumq2 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q2 = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}
			$rev_graphq2 = array(
					"profit" => $sumq2,
					"Date" => $q2
				);
			
			array_push($final1, $rev_graphq2);
			
			//last year q3
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3
			);
			array_push($final1,$rev_graph3);
			
			//last year q4
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}
				$rev_graph4 = array(
				   "profit" => $sumq4,
				   "Date" => $q4
			   );
			array_push($final1, $rev_graph4);
			echo json_encode($final1);

				
		}
		// Quarter 2
         else if($month_num==4 || $month_num==5 || $month_num==6)
		  {
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq2 = 0;
			$test1q2 = 0;
			$sumq2 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test12q = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq2);
			$this->db->where('all_tickets.last_update<=', $dateq22);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2q2 = 0;
			$test3q2 = 0;
			$sum1q2 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q2 = $sum1q2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q2 = 0;
						$test3q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q2 = $test2q2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q2 = $test3q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q2 = 0;
								}
							}
						}

					$sum1q2 = $sum1q2 + $test2q2 + $test3q2;
					}
				}

			$rev_graph1 = array(
				"profit" => $sumq2,
				"Date" => $q2,
				"profit_now" => $sum1q2
			);
 
			array_push($final1, $rev_graph1);
			
			//last year q3
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3
			);
			array_push($final1,$rev_graph3);
			
			//last year q4
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}
				$rev_graph4 = array(
				   "profit" => $sumq4,
				   "Date" => $q4
			   );
			array_push($final1, $rev_graph4);
			echo json_encode($final1);
		  }

			// Quarter 3
		else if($month_num==7 || $month_num==8 || $month_num==9)
		{
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $date3);
			$this->db->where('all_tickets.last_update<=', $date1);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}
           
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $date);
			$this->db->where('all_tickets.last_update<=', $date2);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

				$rev_graph = array(
					"profit" => $sum,
					"Date" => $q1,
					"profit_now" => $sum1
				);
			 	array_push($final1, $rev_graph);
			
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq23);
			$this->db->where('all_tickets.last_update<=', $dateq21);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq2 = $sumq2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq2 = 0;
						$test1q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq2 = $testq2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test12q = $test1q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q2 = 0;
								}
							}
						}

					$sumq2 = $sumq2 + $testq2 + $test1q2;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq2);
			$this->db->where('all_tickets.last_update<=', $dateq22);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			$sumq2 = 0;
			$sum1q2 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q2 = $sum1q2 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q2 = 0;
						$test3q2 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q2 = $test2q2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q2 = $test3q2 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q2 = 0;
								}
							}
						}

					$sum1q2 = $sum1q2 + $test2q2 + $test3q2;
					}
				}

			$rev_graph1 = array(
				"profit" => $sumq2,
				"Date" => $q2,
				"profit_now" => $sum1q2
			);
 
			array_push($final1, $rev_graph1);
			

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq33);
			$this->db->where('all_tickets.last_update<=', $dateq31);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq3 = 0;
			$test1q3 = 0;
			$sumq3 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq3 = $sumq3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq3 = 0;
						$test1q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq3 = $testq3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q3 = $test1q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q3 = 0;
								}
							}
						}

					$sumq3 = $sumq3 + $testq3 + $test1q3;
					}
				}
				
  
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq3);
			$this->db->where('all_tickets.last_update<=', $dateq32);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2q3 = 0;
			$test3q3 = 0;
			$sum1q3 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1q3 = $sum1q3 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2q3 = 0;
						$test3q3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2q3 = $test2q3 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2q3 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3q3 = $test3q3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3q3 = 0;
								}
							}
						}

					$sum1q3 = $sum1q3 + $test2q3 + $test3q3;
					}
				}

			$rev_graph3 = array(
				"profit" => $sumq3,
				"Date" => $q3,
				"profit_now" => $sum1q3
			);
   
			array_push($final1, $rev_graph3);
			
		//last year q4		
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$testq4 = 0;
			$test1q4 = 0;
			$sumq4 = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sumq4 = $sumq4 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$testq4 = 0;
						$test1q4 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$testq4 = $testq4 + $amt[0]['sla_amount'];
								}
							  else
								{
								$testq4 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1q4 = $test1q4 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1q4 = 0;
								}
							}
						}

					$sumq4 = $sumq4 + $testq4 + $test1q4;
					}
				}

				$rev_graph4 = array(
				"profit" => $sumq4,
				"Date" => $q4
			);
   
			array_push($final1, $rev_graph4);
				echo json_encode($final1);
			}
			// Quarter 4
      else
	  {
			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where($where1);
			$this->db->where('all_tickets.last_update>=', $dateq43);
			$this->db->where('all_tickets.last_update<=', $dateq41);
			$querys = $this->db->get();
			$rev_count = $querys->result_array();
			$test = 0;
			$test1 = 0;
			$sum = 0;
			foreach($rev_count as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum = $sum + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test = 0;
						$test1 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test = $test + $amt[0]['sla_amount'];
								}
							  else
								{
								$test = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test1 = $test1 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test1 = 0;
								}
							}
						}

					$sum = $sum + $test + $test1;
					}
				}

			$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
			$this->db->from('all_tickets');
			$this->db->where('all_tickets.last_update>=', $dateq4);
			$this->db->where('all_tickets.last_update<=', $dateq42);
			$querys = $this->db->get();
			$rev_count1 = $querys->result_array();
			$test2 = 0;
			$test3 = 0;
			$sum1 = 0;
			foreach($rev_count1 as $row)
				{
				$company_id = $row['company_id'];
				$ticket_id = $row['ticket_id'];
				$product_id = $row['product_id'];
				$cat_id = $row['cat_id'];
				$priority = $row['priority'];
				$tst = $row['ticket_start_time'];
				$tet = $row['ticket_end_time'];
				$dteEnd = new DateTime($tet);
				$dteStart = new DateTime($tst);
				$dteDiff = $dteStart->diff($dteEnd);
				$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
				$dteStart = strtotime($tst);
				$dteDiff = strtotime($tet);
				$interval = abs($dteDiff - $dteStart);
				$minutes = round($interval / 60);
				$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
				$element = array(
					"company_id" => $company_id,
					"prod" => $product_id,
					"cat" => $cat_id,
					"priority_level" => $priority,
					"difference" => $value
				);
				$product = $product_id;
				$category = $cat_id;
				$priority_level = $priority;
				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('product', 'all');
				$this->db->group_end();
				$query = $this->db->get();
				$result = $query->result_array();
				if (!empty($result))
					{
					$product = 'all';
					}
				  else
					{
					$product = $product_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('category', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$category = 'all';
					}
				  else
					{
					$category = $cat_id;
					}

				$this->db->select('*');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $company_id);
				$this->db->group_start();
				$this->db->like('priority_level', 'all');
				$this->db->group_end();
				$query1 = $this->db->get();
				$result1 = $query1->result_array();
				if (!empty($result1))
					{
					$priority_level = 'all';
					}
				  else
					{
					$priority_level = $priority;
					}

				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where('company_id', $element['company_id']);
				$this->db->where('priority_level', $priority_level);
				$this->db->where('product', $product);
				$this->db->where('category', $category);
				$query1 = $this->db->get();
				$query = $query1->result_array();
				if (empty($query))
					{
					$sum1 = $sum1 + 0;
					}
				  else
					{
					$refid = $query[0]['ref_id'];
					$this->db->select('resolution_time');
					$this->db->from('sla_mapping');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('ref_id', $refid);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					foreach($query as $q)
						{
						$test2 = 0;
						$test3 = 0;
						if ($q['resolution_time'] >= $element['difference'])
							{
							$this->db->select('sla_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $product);
							$this->db->where('category', $category);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test2 = $test2 + $amt[0]['sla_amount'];
								}
							  else
								{
								$test2 = 0;
								}
							}
						  else
							{
							$this->db->select('elapsed_amount');
							$this->db->from('billingfor_sla');
							$this->db->where('company_id', $element['company_id']);
							$this->db->where('priority', $priority_level);
							$this->db->where('product', $element['prod']);
							$this->db->where('category', $element['cat']);
							$query = $this->db->get();
							$amt = $query->result_array();
							if (!empty($amt))
								{
								$test3 = $test3 + $amt[0]['elapsed_amount'];
								}
							  else
								{
								$test3 = 0;
								}
							}
						}

					$sum1 = $sum1 + $test2 + $test3;
					}
				}

			$rev_graph3 = array(
				"profit" => $sum,
				"Date" => $q4,
				"profit_now" => $sum1
			);
			array_push($final1, $rev_graph3);
			echo json_encode($final1);
			} //end else if
		}
			
		else if ($filter == 'Year To Date')
			{
			$today = new DateTime();
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->setTimezone(new DateTimezone('Asia/Kolkata'));
			$today = $today->format('Y-m-d H:i:s');
			$mon = date('M');
			$final1 = array();
			$final2 = array();
			$final_rev = array();

			// if($mon =='Jan' || $mon =='Mar' || $mon =='May' || $mon =='July' || $mon =='Aug' || $mon =='Oct' || $mon =='Dec' ) {

			for ($i = 1; $i <= 12; $i++)
				{
			if ($i < 10)
					{
						 $monthnum=date("n");
						if($monthnum<0)
						{
							$monthnum = '0' . $monthnum;
						}
						else
						{
							$monthnum =  $monthnum;
						}
					$i = '0' . $i;
					$d = date('Y-' . $i . '-d');
					$d = new DateTime($d);
				//	$date = date('Y-' . $i . '-t 23:59:00');
					$abc = $d->format('Y-' . $i . '-t 23:59:00');
				
			       $date = date("Y-m-d 23:59:00", mktime(0, 0, 0, $i+1,0,date("Y")));
	 			   $date2 = date('Y-01-01 00:00:00');
	 			  // $date2 = date('Y-' . $i . '-01 00:00:00');
						
						$dat = new DateTime($date);
					$abc1 = $dat->format('Y-m-d H:i:s');	
						
				    $date1 = date("Y-m-d 23:59:00" , strtotime("-1 year", strtotime($abc1)));
				    $date3 = date('Y-01-01 00:00:00', strtotime("-1 year"));
				  // $date3 = date('Y-' . $i . '-01 00:00:00', strtotime("-1 year"));
						
					// $yr=date("Y-".$i."-t 00:00:00");
					// $date1=date($yr,strtotime("-1 year"));
					// $d =  date( '2017-04-03' );
					// $d =$d->format( 'Y-m-t' );
					// $date1=date($d,strtotime("-1 year"));

					 $day = 'Day ' . $i;
					$monthNum = $i;
					$dateObj = DateTime::createFromFormat('!m', $monthNum);
					$monthName = $dateObj->format('F');
					}
				  else
					{ 
					$d = date('Y-' . $i . '-d');
					$d = new DateTime($d);
					$date = $d->format('Y-' . $i . '-t 23:59:00');
					$date2 = date('Y-' . $i . '-01 00:00:00');
						
					$date3 = date('Y-' . $i . '-01 00:00:00', strtotime("-1 year"));
					$date1 = date($date, strtotime("-1 year"));
					$day = 'Day ' . $i;
					$monthNum = $i;
					$dateObj = DateTime::createFromFormat('!m', $monthNum);
					$monthName = $dateObj->format('F');
					} 

				$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where1);
				$this->db->where('all_tickets.last_update<=', $date1);
				$this->db->where('all_tickets.last_update>=', $date3);
				$querys = $this->db->get();
				$rev_count = $querys->result_array();
				$test = 0;
				$test1 = 0;
				$sum = 0;
				foreach($rev_count as $row)
					{
					$company_id = $row['company_id'];
					$ticket_id = $row['ticket_id'];
					$product_id = $row['product_id'];
					$cat_id = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"company_id" => $company_id,
						"prod" => $product_id,
						"cat" => $cat_id,
						"priority_level" => $priority,
						"difference" => $value
					);
					$product = $product_id;
					$category = $cat_id;
					$priority_level = $priority;
					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('product', 'all');
					$this->db->group_end();
					$query = $this->db->get();
					$result = $query->result_array();
					if (!empty($result))
						{
						$product = 'all';
						}
					  else
						{
						$product = $product_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('category', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$category = 'all';
						}
					  else
						{
						$category = $cat_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('priority_level', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$priority_level = 'all';
						}
					  else
						{
						$priority_level = $priority;
						}

					$this->db->select('ref_id');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('product', $product);
					$this->db->where('category', $category);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					if (empty($query))
						{
						$sum = $sum + 0;
						}
					  else
						{
						$refid = $query[0]['ref_id'];
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('ref_id', $refid);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						foreach($query as $q)
							{
							$test = 0;
							$test1 = 0;
							if ($q['resolution_time'] >= $element['difference'])
								{
								$this->db->select('sla_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $product);
								$this->db->where('category', $category);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test = $test + $amt[0]['sla_amount'];
									}
								  else
									{
									$test = 0;
									}
								}
							  else
								{
								$this->db->select('elapsed_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $element['prod']);
								$this->db->where('category', $element['cat']);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test1 = $test1 + $amt[0]['elapsed_amount'];
									}
								  else
									{
									$test1 = 0;
									}
								}
							}

						$sum = $sum + $test + $test1;
						}
					}
   if($monthnum>=$i)
  {
				$this->db->select('company_id,ticket_id,product_id,cat_id,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where('all_tickets.last_update<=', $date);
				$this->db->where('all_tickets.last_update>=', $date2);
				$querys = $this->db->get();
				$rev_count1 = $querys->result_array();
				$test2 = 0;
				$test3 = 0;
				$sum1 = 0;
				foreach($rev_count1 as $row)
					{
					$company_id = $row['company_id'];
					$ticket_id = $row['ticket_id'];
					$product_id = $row['product_id'];
					$cat_id = $row['cat_id'];
					$priority = $row['priority'];
					$tst = $row['ticket_start_time'];
					$tet = $row['ticket_end_time'];
					$dteEnd = new DateTime($tet);
					$dteStart = new DateTime($tst);
					$dteDiff = $dteStart->diff($dteEnd);
					$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
					$dteStart = strtotime($tst);
					$dteDiff = strtotime($tet);
					$interval = abs($dteDiff - $dteStart);
					$minutes = round($interval / 60);
					$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
					$element = array(
						"company_id" => $company_id,
						"prod" => $product_id,
						"cat" => $cat_id,
						"priority_level" => $priority,
						"difference" => $value
					);
					$product = $product_id;
					$category = $cat_id;
					$priority_level = $priority;
					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('product', 'all');
					$this->db->group_end();
					$query = $this->db->get();
					$result = $query->result_array();
					if (!empty($result))
						{
						$product = 'all';
						}
					  else
						{
						$product = $product_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('category', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$category = 'all';
						}
					  else
						{
						$category = $cat_id;
						}

					$this->db->select('*');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $company_id);
					$this->db->group_start();
					$this->db->like('priority_level', 'all');
					$this->db->group_end();
					$query1 = $this->db->get();
					$result1 = $query1->result_array();
					if (!empty($result1))
						{
						$priority_level = 'all';
						}
					  else
						{
						$priority_level = $priority;
						}

					$this->db->select('ref_id');
					$this->db->from('sla_combination');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority_level', $priority_level);
					$this->db->where('product', $product);
					$this->db->where('category', $category);
					$query1 = $this->db->get();
					$query = $query1->result_array();
					if (empty($query))
						{
						$sum1 = $sum1 + 0;
						}
					  else
						{
						$refid = $query[0]['ref_id'];
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $priority_level);
						$this->db->where('ref_id', $refid);
						$query1 = $this->db->get();
						$query = $query1->result_array();
						foreach($query as $q)
							{
							$test2 = 0;
							$test3 = 0;
							if ($q['resolution_time'] >= $element['difference'])
								{
								$this->db->select('sla_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $product);
								$this->db->where('category', $category);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test2 = $test2 + $amt[0]['sla_amount'];
									}
								  else
									{
									$test2 = 0;
									}
								}
							  else
								{
								$this->db->select('elapsed_amount');
								$this->db->from('billingfor_sla');
								$this->db->where('company_id', $element['company_id']);
								$this->db->where('priority', $priority_level);
								$this->db->where('product', $element['prod']);
								$this->db->where('category', $element['cat']);
								$query = $this->db->get();
								$amt = $query->result_array();
								if (!empty($amt))
									{
									$test3 = $test3 + $amt[0]['elapsed_amount'];
									}
								  else
									{
									$test3 = 0;
									}
								}
							}

						$sum1 = $sum1 + $test2 + $test3;
						}
					}

				$rev_graph_ytd = array(
					"profit" => $sum,
					"profit_now" => $sum1,
					"Date" => $monthName
				);
             }
			else
			{
				$rev_graph_ytd = array(
					"profit" => $sum,
					"Date" => $monthName
				);
			}
				array_push($final1, $rev_graph_ytd);
				}

			// }

			echo json_encode($final1);
			} //end if (YTD)
		}

	public function slacomp_graph($company_id, $filter, $today, $start_of_the_week, $month, $quart, $start_of_yr)
		{
		$where1 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $start_of_the_week,
			'all_tickets.current_status' => 12
		);
		$where2 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $month,
			'all_tickets.current_status' => 12
		);
		$where3 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $quart,
			'all_tickets.current_status' => 12
		);
		$where4 = array(
			'all_tickets.company_id=' => $company_id,
			'all_tickets.last_update<=' => $today,
			'all_tickets.last_update>=' => $start_of_yr,
			'all_tickets.current_status' => 12
		);
		if ($filter == 'Filters' || $filter == 'Weekly')
			{
			$this->db->select('priority_level,SLA_Compliance_Target');
			$this->db->from('sla_mapping');
			$this->db->where('company_id', $company_id);
			$querys = $this->db->get();
			$priority = $querys->result_array();
			$final = array();
			foreach($priority as $row)
				{
				$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where1);
				$this->db->where('all_tickets.priority', $row['priority_level']);
				$querys = $this->db->get();
				$sla_amount = $querys->result_array();
				$test = 0;
				$test1 = 0;
				if (!empty($sla_amount))
					{
					$ar1 = array();
					$ar2 = array();
					foreach($sla_amount as $rw)
						{
						$company_id = $rw['company_id'];
						$ticket_id = $rw['ticket_id'];
						$product_id = $rw['product_id'];
						$cat_id = $rw['cat_id'];
						$cal_cat = $rw['call_tag'];
						$priority = $rw['priority'];
						$tst = $rw['ticket_start_time'];
						$tet = $rw['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"call_category" => $cal_cat,
							"priority_level" => $priority,
							"difference" => $value
						);
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $element['priority_level']);
						$query = $this->db->get();
						$query = $query->result_array();
						foreach($query as $q)
							{

							// echo $q['resolution_time'];
							// echo $element['difference'];

							if ($q['resolution_time'] >= $element['difference'])
								{

								// echo "if \n";

								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}
						}

					$this->db->select('ticket_id');
					$this->db->from('all_tickets');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority', $element['priority_level']);
					$query = $this->db->get();
					$res = $query->num_rows();
					$sla_complaince = count($ar1);
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$revenue = array(
					"sla" => $percent,
					"priority" => $row['priority_level'],
					"target" => $row['SLA_Compliance_Target']
				);
				array_push($final, $revenue);
				}
			}
		  else
		if ($filter == 'Monthly')
			{
			$this->db->select('priority_level,SLA_Compliance_Target');
			$this->db->from('sla_mapping');
			$this->db->where('company_id', $company_id);
			$querys = $this->db->get();
			$priority = $querys->result_array();
			$final = array();
			foreach($priority as $row)
				{
				$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where2);
				$this->db->where('all_tickets.priority', $row['priority_level']);
				$querys = $this->db->get();
				$sla_amount = $querys->result_array();
				$test = 0;
				$test1 = 0;
				if (!empty($sla_amount))
					{
					$ar1 = array();
					$ar2 = array();
					foreach($sla_amount as $rw)
						{
						$company_id = $rw['company_id'];
						$ticket_id = $rw['ticket_id'];
						$product_id = $rw['product_id'];
						$cat_id = $rw['cat_id'];
						$cal_cat = $rw['call_tag'];
						$priority = $rw['priority'];
						$tst = $rw['ticket_start_time'];
						$tet = $rw['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"call_category" => $cal_cat,
							"priority_level" => $priority,
							"difference" => $value
						);
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $element['priority_level']);
						$query = $this->db->get();
						$query = $query->result_array();
						foreach($query as $q)
							{

							// echo $q['resolution_time'];
							// echo $element['difference'];

							if ($q['resolution_time'] >= $element['difference'])
								{

								// echo "if \n";

								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}
						}

					$this->db->select('ticket_id');
					$this->db->from('all_tickets');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority', $element['priority_level']);
					$query = $this->db->get();
					$res = $query->num_rows();
					$sla_complaince = count($ar1);
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$revenue = array(
					"sla" => $percent,
					"priority" => $row['priority_level'],
					"target" => $row['SLA_Compliance_Target']
				);
				array_push($final, $revenue);
				}
			}
		  else
		if ($filter == 'Quarterly')
			{
			$this->db->select('priority_level,SLA_Compliance_Target');
			$this->db->from('sla_mapping');
			$this->db->where('company_id', $company_id);
			$querys = $this->db->get();
			$priority = $querys->result_array();
			$final = array();
			foreach($priority as $row)
				{
				$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where2);
				$this->db->where('all_tickets.priority', $row['priority_level']);
				$querys = $this->db->get();
				$sla_amount = $querys->result_array();
				$test = 0;
				$test1 = 0;
				if (!empty($sla_amount))
					{
					$ar1 = array();
					$ar2 = array();
					foreach($sla_amount as $rw)
						{
						$company_id = $rw['company_id'];
						$ticket_id = $rw['ticket_id'];
						$product_id = $rw['product_id'];
						$cat_id = $rw['cat_id'];
						$cal_cat = $rw['call_tag'];
						$priority = $rw['priority'];
						$tst = $rw['ticket_start_time'];
						$tet = $rw['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"call_category" => $cal_cat,
							"priority_level" => $priority,
							"difference" => $value
						);
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $element['priority_level']);
						$query = $this->db->get();
						$query = $query->result_array();
						foreach($query as $q)
							{

							// echo $q['resolution_time'];
							// echo $element['difference'];

							if ($q['resolution_time'] >= $element['difference'])
								{

								// echo "if \n";

								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}
						}

					$this->db->select('ticket_id');
					$this->db->from('all_tickets');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority', $element['priority_level']);
					$query = $this->db->get();
					$res = $query->num_rows();
					$sla_complaince = count($ar1);
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$revenue = array(
					"sla" => $percent,
					"priority" => $row['priority_level'],
					"target" => $row['SLA_Compliance_Target']
				);
				array_push($final, $revenue);
				}
			}
		  else
		if ($filter == 'Annualy')
			{
			$this->db->select('priority_level,SLA_Compliance_Target');
			$this->db->from('sla_mapping');
			$this->db->where('company_id', $company_id);
			$querys = $this->db->get();
			$priority = $querys->result_array();
			$final = array();
			foreach($priority as $row)
				{
				$this->db->select('company_id,ticket_id,product_id,cat_id,call_tag,priority,ticket_start_time,ticket_end_time');
				$this->db->from('all_tickets');
				$this->db->where($where4);
				$this->db->where('all_tickets.priority', $row['priority_level']);
				$querys = $this->db->get();
				$sla_amount = $querys->result_array();
				$test = 0;
				$test1 = 0;
				if (!empty($sla_amount))
					{
					$ar1 = array();
					$ar2 = array();
					foreach($sla_amount as $rw)
						{
						$company_id = $rw['company_id'];
						$ticket_id = $rw['ticket_id'];
						$product_id = $rw['product_id'];
						$cat_id = $rw['cat_id'];
						$cal_cat = $rw['call_tag'];
						$priority = $rw['priority'];
						$tst = $rw['ticket_start_time'];
						$tet = $rw['ticket_end_time'];
						$dteEnd = new DateTime($tet);
						$dteStart = new DateTime($tst);
						$dteDiff = $dteStart->diff($dteEnd);
						$d = $dteDiff->format("%Y-%m-%d %H:%I:%S");
						$dteStart = strtotime($tst);
						$dteDiff = strtotime($tet);
						$interval = abs($dteDiff - $dteStart);
						$minutes = round($interval / 60);
						$value = $this->convertToHoursMins($minutes, '%02d:%02d:00');
						$element = array(
							"company_id" => $company_id,
							"prod" => $product_id,
							"cat" => $cat_id,
							"call_category" => $cal_cat,
							"priority_level" => $priority,
							"difference" => $value
						);
						$this->db->select('resolution_time');
						$this->db->from('sla_mapping');
						$this->db->where('company_id', $element['company_id']);
						$this->db->where('priority_level', $element['priority_level']);
						$query = $this->db->get();
						$query = $query->result_array();
						foreach($query as $q)
							{

							// echo $q['resolution_time'];
							// echo $element['difference'];

							if ($q['resolution_time'] >= $element['difference'])
								{

								// echo "if \n";

								array_push($ar1, array(
									$q['resolution_time']
								));
								}
							  else
								{
								array_push($ar2, array(
									$q['resolution_time']
								));
								}
							}
						}

					$this->db->select('ticket_id');
					$this->db->from('all_tickets');
					$this->db->where('company_id', $element['company_id']);
					$this->db->where('priority', $element['priority_level']);
					$query = $this->db->get();
					$res = $query->num_rows();
					$sla_complaince = count($ar1);
					$answer = $sla_complaince / $res;
					$percent = $answer * 100;
					}
				  else
					{
					$percent = 0;
					}

				$revenue = array(
					"sla" => $percent,
					"priority" => $row['priority_level'],
					"target" => $row['SLA_Compliance_Target']
				);
				array_push($final, $revenue);
				}
			}

		print_r(json_encode($final));
		}

	public function total_subscription()
		{
		$this->db->select('company_id');
		$this->db->distinct('company_id');
		$this->db->from('admin_user');
		$result = $this->db->get();
		$res = $result->result_array();
		$total_serv = 0;
		$total_tech = 0;
		$technician = 0;
		$useds = 0;
		$usedt = 0;
		$available_t = 0;
		$available_s = 0;
		$element = array();
		foreach($res as $r)
			{
			$this->db->select('service_desk,technicians,service_added,tech_added');
			$this->db->from('admin_user');

			// $this->db->order_by('last_update','desc');

			$this->db->where('company_id', $r['company_id']);
			$this->db->where('admin_user.blocked_status=', "0");
			$count = $this->db->get();
			$total = $count->result_array();
			foreach($total as $row)
				{
				$company_id = $r['company_id'];
				$service = $row['service_desk'];
				$technician = $row['technicians'];
				$used_serv = $row['service_added'];
				$used_tech = $row['tech_added'];
				$total_serv = $total_serv + $service;
				$total_tech = $total_tech + $technician;
				$useds = $useds + $used_serv;
				$usedt = $usedt + $used_tech;
				$available_s = $total_serv - $useds;
				$available_t = $total_tech - $usedt;
				}
			}

			$this->db->select('ticket_id');
			$this->db->distinct('ticket_id');
			$this->db->from('all_tickets');
			$tic_result = $this->db->get();
			$tickets_count = $tic_result->num_rows();

		array_push($element, array(
			"service_desk" => $total_serv,
			"technician" => $total_tech,
			"used_serv" => $useds,
			"used_tech" => $usedt,
			"available_s" => $available_s,
			"available_t" => $available_t,
			"tickets_count" => $tickets_count,
		));
		echo json_encode($element);
		}

	public function total_subscription1()
		{
		$this->db->select('company_id,service_desk,technicians,service_added,tech_added');
		$this->db->from('admin_user');
		$this->db->order_by('last_update', 'desc');
		$this->db->distinct('company_id');

		// $this->db->limit(1);

		$result = $this->db->get();
		$res = $result->result_array();
		$total_serv = 0;
		$total_tech = 0;
		$technician = 0;
		$useds = 0;
		$usedt = 0;
		$available_t = 0;
		$available_s = 0;
		$element = array();
		foreach($res as $row)
			{
			$company_id = $row['company_id'];
			$service = $row['service_desk'];
			$technician = $row['technicians'];
			$used_serv = $row['service_added'];
			$used_tech = $row['tech_added'];
			$total_serv = $total_serv + $service;
			$total_tech = $total_tech + $technician;
			$useds = $useds + $used_serv;
			$usedt = $usedt + $used_tech;
			$available_s = $total_serv - $useds;
			$available_t = $total_tech - $usedt;
			}

		// $element= array("service_desk" =>$total_serv, "technician"=>$total_tech);
			$this->db->select('ticket_id');
			$this->db->distinct('ticket_id');
			$this->db->from('all_tickets');
			$tic_result = $this->db->get();
			$tickets_count = $tic_result->num_rows();

		array_push($element, array(
			"service_desk" => $total_serv,
			"technician" => $total_tech,
			"used_serv" => $useds,
			"used_tech" => $usedt,
			"available_s" => $available_s,
			"available_t" => $available_t,
			"tickets_count" => $tickets_count,
		));
		echo json_encode($element);
		}

	public function get_reference_id($company_name)
		{
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$result1 = $this->db->get();
		$res = $result1->result_array();
		if (empty($res))
			{
			$ref_id = 1;
			}
		  else
			{
			$ref_id = $res[0]['ref_id'] + 1;
			}

		return $ref_id;
		}

	public function check($company_name, $priority)
		{
		$array = array(
			'company_id' => $company_name,
			'priority_level' => $priority
		);
		$result = array();
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($array);
		$result1 = $this->db->get();
		$res = $result1->result_array();
		if (!empty($res))
			{
			foreach($res as $row)
				{
				if ($row['product'] == 'all')
					{
					array_push($result, ('product'));
					}

				if ($row['category'] == 'all')
					{
					array_push($result, ('category'));
					}

				if ($row['cust_category'] == 'all')
					{
					array_push($result, ('cust_category'));
					}

				if ($row['call_category'] == 'all')
					{
					array_push($result, ('call_category'));
					}

				if ($row['service_category'] == 'all')
					{
					array_push($result, ('service_category'));
					}
				}
			}

		echo json_encode(array(
			'result' => $result
		) , true);
		}

	public function subscribed_tech($company_id)
		{
		$this->db->select('company_id,service_desk,technicians,service_added,tech_added');
		$this->db->from('admin_user');
		$this->db->order_by('last_update', 'desc');
		$this->db->limit(1);
		$this->db->where('company_id', $company_id);
		$result = $this->db->get();
		$res = $result->result_array();
		$total_serv = 0;
		$total_tech = 0;
		$technician = 0;
		$useds = 0;
		$usedt = 0;
		$available_t = 0;
		$available_s = 0;
		$element = array();
		foreach($res as $row)
			{
			$company_id = $row['company_id'];
			$service = $row['service_desk'];
			$technician = $row['technicians'];
			$used_serv = $row['service_added'];
			$used_tech = $row['tech_added'];
			$total_serv = $total_serv + $service;
			$total_tech = $total_tech + $technician;
			$useds = $useds + $used_serv;
			$usedt = $usedt + $used_tech;
			$available_s = $total_serv - $useds;
			$available_t = $total_tech - $usedt;
			}

		// $element= array("service_desk" =>$total_serv, "technician"=>$total_tech);
			$this->db->select('ticket_id');
			$this->db->distinct('ticket_id');
			$this->db->from('all_tickets');
			$this->db->where('company_id', $company_id);
			$tic_result = $this->db->get();
			$tickets_count = $tic_result->num_rows();

		array_push($element, array(
			"service_desk" => $total_serv,
			"technician" => $total_tech,
			"used_serv" => $useds,
			"used_tech" => $usedt,
			"available_s" => $available_s,
			"available_t" => $available_t,
			"tickets_count" => $tickets_count,
		));
		echo json_encode($element);
		}

	public function revenue_count()
		{
		$this->db->select('company_id,company_name,service_desk,technicians');
		$this->db->from('admin_user');
		$query1 = $this->db->get();
		$query = $query1->result_array();
		$final = array();
		$serv_count = 0;
		$count = 0;
		$total = 0;
		foreach($query as $row)
			{
			$company_id = $row['company_id'];
			$company = $row['company_name'];
			$serv = $row['service_desk'];
			$tech = $row['technicians'];
			$serv_count = $serv * 1000;
			$count = $tech * 1000;
			$total = $serv_count + $count;
			$elements = array(
				"company_name" => $company,
				"profit" => $total
			);
			array_push($final, $elements);
			}

		echo json_encode($final);
		}

	public function weeklyfetch()
		{
		$today1 = date("Y-m-d H:i:s");
		$week = date("Y-m-d 00:00:00", strtotime("-1 week"));
		$where1 = array(
			'admin_user.start_date<=' => $today1,
			'admin_user.start_date>=' => $week
		);
		$this->db->select('company_id,service_desk,technicians,start_date');
		$this->db->from('admin_user');
		$this->db->where($where1);
		$query1 = $this->db->get();
		$result = $query1->result_array();
		$sum = 0;
		$week_report = 0;
		$stotal = 0;
		$ttotal = 0;
		foreach($result as $row)
			{
			$company_id = $row['company_id'];
			$service = $row['service_desk'];
			$technician = $row['technicians'];
			$stotal = $stotal + $service;
			$ttotal = $ttotal + $technician;
			}

		$sum = $stotal + $ttotal;
		$week_report = $sum * 1000;
		return $week_report;
		}

	public function monthfetch()
		{
		$today1 = date("Y-m-d H:i:s");
		$month = date("Y-m-d 00:00:00", strtotime("-1 month"));
		$where2 = array(
			'admin_user.start_date<=' => $today1,
			'admin_user.start_date>=' => $month
		);
		$this->db->select('company_id,service_desk,technicians,start_date');
		$this->db->from('admin_user');
		$this->db->where($where2);
		$query1 = $this->db->get();
		$result = $query1->result_array();
		$sum = 0;
		$month_report = 0;
		$stotal = 0;
		$ttotal = 0;
		foreach($result as $row)
			{
			$company_id = $row['company_id'];
			$service = $row['service_desk'];
			$technician = $row['technicians'];
			$stotal = $stotal + $service;
			$ttotal = $ttotal + $technician;
			}

		$sum = $stotal + $ttotal;
		$month_report = $sum * 1000;
		return $month_report;
		}

	public function yearfetch()
		{
		$today1 = date("Y-m-d H:i:s");
		$year = date("Y-m-d 00:00:00", strtotime("-1 year"));
		$where3 = array(
			'admin_user.start_date<=' => $today1,
			'admin_user.start_date>=' => $year
		);
		$this->db->select('company_id,service_desk,technicians,start_date');
		$this->db->from('admin_user');
		$this->db->where($where3);
		$query1 = $this->db->get();
		$result = $query1->result_array();
		$sum = 0;
		$year_report = 0;
		$stotal = 0;
		$ttotal = 0;
		foreach($result as $row)
			{
			$company_id = $row['company_id'];
			$service = $row['service_desk'];
			$technician = $row['technicians'];
			$stotal = $stotal + $service;
			$ttotal = $ttotal + $technician;
			}

		$sum = $stotal + $ttotal;
		$year_report = $sum * 1000;
		return $year_report;
		}

	public function user_details($input)
		{
		if ($input == "" || $input == "Users in Total")
			{
			$this->db->select('company_id,company_name,service_desk,technicians');
			$this->db->from('admin_user');
			$result = $this->db->get();
			$query = $result->result_array();
			$answer = array();
			$serv_count = 0;
			$count = 0;
			$total = 0;
			foreach($query as $row)
				{
				$company_id = $row['company_id'];
				$company = $row['company_name'];
				$serv = $row['service_desk'];
				$tech = $row['technicians'];
				$serv_count = $serv_count + $serv;
				$count = $count + $tech;
				$total = $serv_count + $count;
				$elements = array(
					"company_name" => $company,
					"total" => $total
				);
				array_push($answer, $elements);
				}
			}
		  else
		if ($input == "Users Registered")
			{
			$this->db->select('company_id,company_name');
			$this->db->from('admin_user');
			$query1 = $this->db->get();
			$query = $query1->result_array();
			$answer = array();
			$total = 0;
			foreach($query as $row)
				{
				$company_id = $row['company_id'];
				$company = $row['company_name'];
				$array = array(
					'companyid' => $company_id,
					'role' => 'ServiceDesk'
				);

				// calculating no. of tech//

				$this->db->select('technician_id');
				$this->db->from('technician');
				$this->db->where('company_id', $company_id);
				$query_t = $this->db->get();
				$res = $query_t->num_rows();

				// calculating no. of serv//

				$this->db->select('first_name');
				$this->db->from('user');
				$this->db->where($array);
				$query_s = $this->db->get();
				$result1 = $query_s->num_rows();
				$total = $res + $result1;
				$elements = array(
					"company_id" => $company_id,
					"company_name" => $company,
					"total" => $total
				);
				array_push($answer, $elements);
				}
			}

		echo json_encode($answer);
		}

	public function load_chart()
		{
		$this->db->select('company_id,company_name,service_desk,technicians');
		$this->db->from('admin_user');
		$result = $this->db->get();
		$query = $result->result_array();
		$first = array();
		$serv_count = 0;
		$count = 0;
		$total = 0;
		foreach($query as $row)
			{
			$company_id = $row['company_id'];
			$company = $row['company_name'];
			$serv = $row['service_desk'];
			$tech = $row['technicians'];
			$serv_count = $serv_count + $serv;
			$count = $count + $tech;
			$total = $serv_count + $count;
			$change = array(
				"company_name" => $company,
				"users" => $total
			);
			array_push($first, $change);
			}

		echo json_encode($first);
		}

	public function fetch_company($id)
		{
		$this->db->select('*');
		$this->db->from('admin_user');
		$this->db->order_by('last_update', 'desc');
		$this->db->where('company_id', $id);
		$this->db->where('blocked_status=', "0");
		$query = $this->db->get();
		return $query->row_array();
		/*if ($query->num_rows() > 0)
		{
		return $query->result_array();
		}
		  else {return NULL;}*/
		}

	public function fetch_renewdate($id)
		{
		$this->db->select("renewal_date");
		$this->db->from('admin_user');
		$this->db->where('company_id', $id);
		$query = $this->db->get();
		$ans = $query->row_array();
		return $ans;
		}

	public function renew_info($name, $tech, $serv, $subs_plan, $renew, $mydate)
		{
		$total_tech = 0;
		$total_serv = 0;
		$sample = array();
		$this->db->select('company_id,ad_fname,ad_lname,ad_mailid,ad_contact');
		$this->db->from('admin_user');
		$this->db->where('company_name', $name);
		$query = $this->db->get();
		$ans = $query->result_array();
		foreach($ans as $row)
			{
			$company_id = $row['company_id'];
			$ad_fname = $row['ad_fname'];
			$ad_lname = $row['ad_lname'];
			$ad_mailid = $row['ad_mailid'];
			$ad_contact = $row['ad_contact'];
			$pusharray = array(
				"company_id" => $company_id,
				"company_name" => $name,
				"ad_fname" => $ad_fname,
				"ad_lname" => $ad_lname,
				"ad_mailid" => $ad_mailid,
				"ad_contact" => $ad_contact,
				"service_desk" => $serv,
				"technicians" => $tech,
				"subs_plan" => $subs_plan,
				"renewal_date" => $renew
			);
			}

		$this->db->where('company_id', $company_id);
		$this->db->update('admin_user', $pusharray);
		return true;
		}

	/* for taking last id from coloumn and incrementing by 1*/
	public function company_id()
		{
		$this->db->select('id');
		$this->db->from('company');
		$this->db->order_by('company.id', 'desc');
		$this->db->limit(1);
		$query1 = $this->db->get();
		$query = $query1->result_array();
		if (!empty($query))
			{
			$id = $query[0]['id'] + 1;
			$company_id = 'company' . $id;
			}
		  else
			{
			$company_id = 'company1';
			}

		return $company_id;
		}

	public function notification($rdate, $date)
		{
		$data = array(
			'admin_user.renewal_date<=' => $rdate,
			'admin_user.renewal_date>=' => $date
		);
		$this->db->select('admin_user.company_id,admin_user.company_name,admin_user.renewal_date,company.company_logo');
		$this->db->from('admin_user');
		$this->db->order_by('admin_user.last_update', 'desc');
		$this->db->join('company', 'admin_user.company_id=company.company_id');
		$this->db->where($data);
		$noti = $this->db->get();
		$notific = $noti->result_array();
		echo json_encode($notific);
		}

	public function noti_count($rdate, $date)
		{
		$data = array(
			'admin_user.renewal_date<=' => $rdate,
			'admin_user.renewal_date>=' => $date
		);
		$this->db->select('admin_user.company_id,admin_user.company_name,admin_user.renewal_date,company.company_logo');
		$this->db->from('admin_user');
		$this->db->order_by('admin_user.last_update', 'desc');
		$this->db->join('company', 'admin_user.company_id=company.company_id');
		$this->db->where($data);
		$noti = $this->db->get();
		$noti_count = $noti->num_rows();
		return $noti_count;
		}

	public function auto_block($today)
		{
		$array = array(
			'blocked_status' => 0,
			'renewal_date<=' => $today
		);
		$this->db->select('admin_id,company_name');
		$this->db->from('admin_user');
		$this->db->where($array);
		$blocking = $this->db->get();
		$block = $blocking->result_array();
		foreach($block as $row)
			{
			$data1 = array(
				'blocked_status' => 1
			);
			$admin_id = $row['admin_id'];
			$this->db->where('admin_user.admin_id', $admin_id);
			$this->db->update('admin_user', $data1);
			}
		}

	public function getcompany_s()
		{
		$this->db->select('*');
		$this->db->from('company');

		// $this->db->join('admin_user', 'admin_user.company_id = company.company_id');

		$query = $this->db->get();
		$result = $query->result_array();
		foreach($result as $row)
			{
			$data[] = $row;
			}

		return $data;
		}

	public function getcompany_ss()
		{
		$blockedcompanyarray=array();
		$this->db->select('admin_user.company_id');
		$this->db->from('admin_user');
		$this->db->where('admin_user.blocked_status', 1);
		$this->db->group_by("admin_user.company_id ");
		$query_b1 = $this->db->get();

		$query_b   = $query_b1->result_array();
		foreach($query_b as $com){
			array_push($blockedcompanyarray, $com['company_id']);
		}

		
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where_not_in('company_id', $blockedcompanyarray);
		$this->db->order_by("last_update", "desc");
		$query = $this->db->get();
		return $query;
		}

	public function getproduct_s($company)
		{

		// $stack = array("orange", "banana");

		$this->db->select('*,product_management.id as a');
		$this->db->from('product_management');

		// $this->db->join('category_details', 'category_details.prod_id = product_management.product_id');

		$this->db->order_by("a", "desc");
		$this->db->where("company_id", $company);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
		}

	public function getcategory_s($prod_id_1)
		{

		// $stack = array("orange", "banana");

		$this->db->select('*');
		$this->db->from('category_details');

		// $this->db->join('category_details', 'category_details.prod_id = product_management.product_id');

		$this->db->where("prod_id", $prod_id_1);
		$this->db->order_by("last_update", "desc");
		$query = $this->db->get();
		$result = $query->result_array();
		/* foreach ($result as $row) {
		$data[] = $row;
		} */
		return $result;
		}

	public function priority_level()
		{
		$this->db->select('*');
		$this->db->from('priority');
		$query = $this->db->get();
		return $query;
		}

	public function getdetails_product($data)
		{
		$this->db->select('*');
		$this->db->from('product_management');
		$this->db->where('product_id', $data);

		// $this->db->order_by("id", "desc");

		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			return $query->row_array();
			}
		  else
			{
			return NULL;
			}
		}

	public function deleteproduct($id)
		{
		$this->db->where('product_id', $id);
		$this->db->delete('product_management');
		return true;
		}
		
    public function del_prod($id){
      $this->db->where('prod_id',$id);
      $this->db->delete('category_details');
      return true; 
    }

	public function getdetails_subproduct($data)
		{
		$this->db->select('*');
		$this->db->from('category_details');
		$this->db->where('cat_id', $data);

		// $this->db->order_by("id", "desc");

		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			return $query->row_array();
			}
		  else
			{
			return NULL;
			}
		}

	public function deletesubproduct($id)
		{
		$this->db->where('cat_id', $id);
		$this->db->delete('category_details');
		return true;
		}

	public function get_subcate($prod)
		{
		$this->db->select('cat_id');
		$this->db->from('category_details');
		$this->db->where('prod_id', $prod);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			return $query->result_array();
			}
		  else
			{
			return NULL;
			}
		}

	public function insert_slac($data_combination, $data_mapping, $data_edit, $company_id)
		{
		$this->db->insert('sla_combination', $data_combination);
		return $this->insert_slam($data_mapping, $data_edit, $company_id);
		}

	public function insert_slam($data_mapping, $data_edit, $company_id)
		{
		$this->db->insert('sla_mapping', $data_mapping);
		return $this->insert_editable($data_edit, $company_id);
		}

	public function insert_editable($data_edit, $company_id)
		{
		$this->db->where('company_id', $company_id);
		$this->db->update('sla_mapping', $data_edit);
		return true;
		}

	public function sla_get()
    {
        $this->db->select('*,sla_combination.id as a,sla_combination.priority_level as b,sla_combination.ref_id as c,sla_mapping.last_update as d');
        $this->db->from('sla_combination');
        $this->db->join('sla_mapping', 'sla_mapping.ref_id = sla_combination.ref_id');
        $this->db->join('product_management', 'product_management.product_id = sla_combination.product or sla_combination.product ="all"');
        $this->db->join('category_details', 'category_details.cat_id = sla_combination.category or sla_combination.category="all"');
        $this->db->join('company', 'company.company_id = sla_combination.company_id');
        $this->db->order_by('a', 'desc');
        $this->db->group_by('sla_combination.ref_id', 'desc');
        $query = $this->db->get();
        return $query;
    }

	public function sla_get1($company)
		{
		$this->db->select('*,sla_combination.id as a,sla_combination.priority_level as b');
		$this->db->from('sla_combination');
		$this->db->where('sla_combination.company_id', $company);
		$this->db->join('sla_mapping', 'sla_mapping.ref_id = sla_combination.ref_id');
		$this->db->join('product_management', 'product_management.product_id = sla_combination.product');
		$this->db->join('category_details', 'category_details.cat_id = sla_combination.category');
		$this->db->join('company', 'company.company_id = sla_combination.company_id');
		$this->db->order_by('a', 'desc');
		$query = $this->db->get();
		return $query->result_array();
		}

	public function view_priority($ref_id)
		{
		$this->db->select('*');
		$this->db->from('sla_mapping');
		$this->db->where('ref_id', $ref_id);
		$query = $this->db->get();
		return $query->row_array();
		}
		
     public function get_details_company()
     {    
		$blockedcompanyarray=array();


		$this->db->select('admin_user.company_id');
		$this->db->from('admin_user');
		$this->db->where('admin_user.blocked_status', 1);
		$this->db->group_by("admin_user.company_id ");
		$query_b1 = $this->db->get();
		$query_b   = $query_b1->result_array();
		foreach($query_b as $com){
			array_push($blockedcompanyarray, $com['company_id']);
		}
		

		  $resu = array();
		 
          $this->db->select('company.company_id,company.company_logo,company_name');
          $this->db->from('company,sla_combination');
          $this->db->where('company.company_id=sla_combination.company_id ');
          $this->db->group_by("company.company_id ");
          $this->db->order_by("company.last_update", "DESC");
          $query = $this->db->get();
          $res   = $query->result_array();
          if (!empty($res)) {
               foreach ($res as $r) {
                    $this->db->select('company.company_id,company.company_logo,company_name');
                    $this->db->from('company,sla_combination');
                    $this->db->where('(sla_combination.product="all" and sla_combination.category="all"  and sla_combination.priority_level="all"  and sla_combination.company_id="' . $r['company_id'] . '" ) ');
					$this->db->group_by("company.company_id ");
					$this->db->order_by("company.last_update", "DESC");
                    $query = $this->db->get();
                    $re    = $query->result_array();
                    if (!empty($re)) {
                         foreach ($re as $r1) {
                              $this->db->select('company.company_id,company.company_logo,company_name');
                              $this->db->from('company');
							  $this->db->where('company.company_id!=', $r1['company_id']);
							  $this->db->where_not_in('company.company_id', $blockedcompanyarray);
							  $this->db->group_by("company.company_id ");
							  $this->db->order_by("company.last_update", "DESC");
                              $query = $this->db->get();
                              array_push($resu, $query->result_array());
                         }
                    } else {
                         $this->db->select('company.company_id,company.company_logo,company_name');
                         $this->db->from('company,sla_combination');
						 $this->db->where('(sla_combination.product="all" and sla_combination.category="all"   and sla_combination.priority_level="all"  and sla_combination.company_id="' . $r['company_id'] . '" ) ');
						 $this->db->where_not_in('company.company_id', $blockedcompanyarray);
						 $this->db->group_by("company.company_id ");
						 $this->db->order_by("company.last_update", "DESC");
                         $query = $this->db->get();
                         $re    = $query->result_array();
                         if (count($re) < 4) {
                              $this->db->select('company.company_id,company.company_logo,company_name');
                              $this->db->from('company,sla_combination');
                              $this->db->where('sla_combination.company_id', $r['company_id']);							 
							  $this->db->where_not_in('company.company_id', $blockedcompanyarray);
							  $this->db->group_by("company.company_id ");
							  $this->db->order_by("company.last_update", "DESC");
                              $query = $this->db->get();
                              array_push($resu, $query->result_array());
                         } else {
                              $this->db->select('company.company_id,company.company_logo,company_name');
                              $this->db->from('company');
							  $this->db->where('company.company_id!=', $r['company_id']);
							  $this->db->where_not_in('company.company_id', $blockedcompanyarray);
							  $this->db->group_by("company.company_id ");
							  $this->db->order_by("company.last_update", "DESC");
                              $query = $this->db->get();
                              array_push($resu, $query->result_array());
                         }
                    }
               }
          } else {
               $this->db->select('company.company_id,company.company_logo,company_name');
               $this->db->from('company');
			   $this->db->group_by("company.company_id ");
			   $this->db->where_not_in('company.company_id', $blockedcompanyarray);
               $this->db->order_by("company.last_update", "DESC");
               $query = $this->db->get();
               array_push($resu, $query->result_array());
          }
          return $resu[0];
     }
     public function get_details_company1(){
			   $blockedcompanyarray=array();
				$this->db->select('admin_user.company_id');
				$this->db->from('admin_user');
				$this->db->where('admin_user.blocked_status', 1);
				$this->db->group_by("admin_user.company_id ");
				$query_b1 = $this->db->get();
				$query_b   = $query_b1->result_array();
				foreach($query_b as $com){
					array_push($blockedcompanyarray, $com['company_id']);
				}


               $resu=array();
               $this->db->select('company.company_id,company.company_logo,company.company_name');
			   $this->db->from('company');
			   $this->db->where_not_in('company.company_id', $blockedcompanyarray);
               $this->db->group_by("company.company_id ");
               $this->db->order_by("last_update", "DESC");
               $query = $this->db->get();
               array_push($resu, $query->result_array());
               return $resu[0];
		}
		

public function get_product_company1($company_id)
     {
          $this->db->select('*');
          $this->db->from('product_management');
          $this->db->where('product_management.company_id', $company_id);
          $this->db->group_by("product_management.product_id");
          $this->db->order_by('last_update', 'desc');
          $query51 = $this->db->get();
          $r551    = $query51->result_array();
          return $r551;
     }
     public function get_subproduct_company1($prod_id)
     {
          $this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
          $this->db->from('category_details');
          $this->db->where('prod_id', $prod_id);
          $this->db->group_by("category_details.cat_id");
          $query = $this->db->get();
          $res   = $query->result_array();
          return $res;
     }

        public function get_product_company($company_id){
        $resu=array();
		$this->db->select('*');
        $this->db->from('product_management');
        $this->db->where('company_id',$company_id);
        $this->db->group_by("product_management.product_id");
        $this->db->order_by("last_update", "DESC");
        $query=$this->db->get();
        $res= $query->result_array();
		foreach($res as $r){
			$this->db->select('*');
			$this->db->from('product_management,sla_combination');
			$this->db->where('sla_combination.product',$r['product_id']);
			$this->db->where('sla_combination.company_id',$company_id);
			$this->db->where('product_management.company_id',$company_id);
			$this->db->group_by("sla_combination.ref_id");
			$query5=$this->db->get();
			$res5= $query5->result_array();
			if(!empty($res5))
			{
				$this->db->select('*');
				$this->db->from('product_management,sla_combination');
				$this->db->where('sla_combination.product="'.$r['product_id'].'" and sla_combination.category="all" and sla_combination.priority_level="all"  ');
				$this->db->where('sla_combination.company_id',$company_id);
				$this->db->where('product_management.company_id',$company_id);
				$this->db->group_by("sla_combination.ref_id");
				$query1=$this->db->get();
				$res1= $query1->result_array();
				if(!empty($res1))
				{
					
				}
				else
				{
					$this->db->select('*');
					$this->db->from('product_management,sla_combination');
					$this->db->where('sla_combination.product="'.$r['product_id'].'" and sla_combination.category="all" and sla_combination.priority_level!="all"  ');
					$this->db->where('sla_combination.company_id',$company_id);
					$this->db->where('product_management.company_id',$company_id);
					$this->db->group_by("sla_combination.ref_id");
					$query2=$this->db->get();
					$res2= $query2->result_array();
					if(!empty($res2))
					{
						if(count($res2)>=4)
						{
							
						}
						else
						{
							$this->db->select('product_id,product_image,product_management.company_id,product_name');
							$this->db->from('product_management');
							$this->db->where('product_id',$r['product_id']);
							$this->db->where('company_id',$company_id);
							$this->db->group_by("product_management.product_id");
							$query3=$this->db->get();
							array_push($resu,$query3->row_array());
						}
					}
					else
					{
						$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
						$this->db->from('category_details,sla_combination');
						$this->db->where('prod_id',$r['product_id']);
						$this->db->group_by("category_details.cat_id");
						$query4=$this->db->get();
						$res4= $query4->num_rows();
						$count=$res4*4;
						
						$this->db->select('*');
						$this->db->from('product_management,sla_combination');
						$this->db->where('sla_combination.product="'.$r['product_id'].'" and sla_combination.category!="all" and sla_combination.priority_level="all"  ');
						$this->db->where('sla_combination.company_id',$company_id);
						$this->db->where('product_management.company_id',$company_id);
						$this->db->group_by("sla_combination.ref_id");
						$query3=$this->db->get();
						$res3= $query3->result_array();
						if(!empty($res3))
						{
						if($res4>count($res3))
						{
							$this->db->select('product_id,product_image,product_management.company_id,product_name');
							$this->db->from('product_management');
							$this->db->where('product_id',$r['product_id']);
							$this->db->where('company_id',$company_id);
							$this->db->group_by("product_management.product_id");
							$query3=$this->db->get();
							array_push($resu,$query3->row_array());
						}
						else
						{
						$this->db->select('*');
						$this->db->from('product_management,sla_combination');
						$this->db->where('sla_combination.product="'.$r['product_id'].'" and sla_combination.category!="all" and sla_combination.priority_level!="all"  ');
						$this->db->where('sla_combination.company_id',$company_id);
						$this->db->where('product_management.company_id',$company_id);
						$this->db->group_by("sla_combination.ref_id");
						$query3=$this->db->get();
						$res3= $query3->result_array();
						if(!empty($res3))
			{
						if($count>count($res3))
						{
							$this->db->select('product_id,product_image,product_management.company_id,product_name');
							$this->db->from('product_management');
							$this->db->where('product_id',$r['product_id']);
							$this->db->where('company_id',$company_id);
							$this->db->group_by("product_management.product_id");
							$query3=$this->db->get();
							array_push($resu,$query3->row_array());
						}
						else
						{
							
						}
						}
						}
						}
						
					}
				}	
			}
			else
			{
				$this->db->select('product_id,product_image,product_management.company_id,product_name');
				$this->db->from('product_management');
				$this->db->where('product_id',$r['product_id']);
				$this->db->where('company_id',$company_id);
				$this->db->group_by("product_management.product_id");
				$query6=$this->db->get();
				array_push($resu,$query6->row_array());
			}
		}
		return $resu;
    }
    public function get_subproduct_company($prod_id){
		$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
        $this->db->from('category_details,sla_combination');
        $this->db->where('prod_id',$prod_id);
        $this->db->group_by("category_details.cat_id");
        $query=$this->db->get();
        $res= $query->result_array();
        $res1= array();
        $res2= array();
        $resu= array();
        $res3= array();
		if(!empty($res))
		{
			foreach($res as $r)
			{
				$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
				$this->db->from('category_details,sla_combination');
				$this->db->where('sla_combination.product="'.$prod_id.'" and sla_combination.category="all" and sla_combination.priority_level!="all" ');
				$this->db->where('sla_combination.product',$prod_id);
				$this->db->group_by("category_details.cat_id");
				$query=$this->db->get();
				array_push($res2,$query->result_array());
				$re1=$query->result_array();
				if(!empty($re1))
				{
					foreach($re1 as $r11)
					{
						$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
						$this->db->from('category_details');
						$this->db->where('prod_id',$prod_id);
						$this->db->group_by("category_details.cat_id");
						$query=$this->db->get();
						array_push($resu,$query->result_array());
					}
				}
				else
				{
					
					$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
					$this->db->from('category_details,sla_combination');
					$this->db->where('sla_combination.product="'.$prod_id.'" and sla_combination.category="'.$r['cat_id'].'" and sla_combination.priority_level="all" ');
					$this->db->where('sla_combination.product',$prod_id);
					$this->db->group_by("category_details.cat_id");
					$query=$this->db->get();
					array_push($res2,$query->result_array());
					$re11=$query->result_array();
					if(!empty($re11))
					{
						foreach($re11 as $r111)
						{
							$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
							$this->db->from('category_details');
							$this->db->where('prod_id',$prod_id);
							$this->db->where('cat_id!=',$r111['cat_id']);
							$this->db->group_by("category_details.cat_id");
							$query=$this->db->get();
							array_push($resu,$query->result_array());
						}
					}
					else
					{
						if(count($re11)<4)
					{
						$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
						$this->db->from('category_details');
						$this->db->where('prod_id',$prod_id);
						//$this->db->where('cat_id',$r['cat_id']);
						$this->db->group_by("category_details.cat_id");
						$query=$this->db->get();
						array_push($resu,$query->result_array());
					}
					else
					{
						$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
							$this->db->from('category_details');
							$this->db->where('prod_id',$prod_id);
							$this->db->where('cat_id!=',$r['cat_id']);
							$this->db->group_by("category_details.cat_id");
							$query=$this->db->get();
							array_push($resu,$query->result_array());
					}
					}
				}
			}
		}
		else
		{
			$this->db->select('cat_id,cat_name,cat_image,category_details.company_id,prod_id');
			$this->db->from('category_details');
			 $this->db->where('prod_id',$prod_id);
			$this->db->group_by("category_details.cat_id");
			$query=$this->db->get();
			array_push($resu,$query->result_array());
		}
        return $resu[0];
    }
	public function select_sla_comb($company_id,$product_id,$cat_id,$priority)
    {
		
        $where = array(
            'company_id' => $company_id,
            'priority_level' =>"all",
            'product' => "all",
			'category'=>"all"
        );
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->num_rows();
		if($data>0)
		{
			return false;
		}
		else
		{
		$where = array(
            'company_id' => $company_id,
            'priority_level' =>"all",
            'product' => $product_id,
			'category'=>"all"
        );
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->num_rows();
		if($data>0)
		{
			return false;
		}
		else
		{
		$where = array(
            'company_id' => $company_id,
            'priority_level' =>$priority,
            'product' => $product_id,
			'category'=>"all"
        );
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->num_rows();
		if($data>0)
		{
			return false;
		}
		else
		{
        $where = array(
            'company_id' => $company_id,
            'priority_level' => $priority,
            'product' => $product_id,
			'category'=>$cat_id
        );
        $this->db->select('ref_id');
        $this->db->from('sla_combination');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->num_rows();
		if($data>0)
		{
			return false;
		}
		else
		{
		
			$this->db->select('ref_id');
			$this->db->from('sla_combination');
			$this->db->order_by('id','desc');
			$this->db->limit(1);
			$query1 = $this->db->get();
			$ref_id=$query1->result_array();
			if(!empty($ref_id))
			{
				$ref_id=$ref_id[0]['ref_id'];
				$ref_id=$ref_id+1;
			}
			else
			{
				$ref_id=1;
			}
			$data=array('company_id' => $company_id,
            'priority_level' => $priority,
            'product' => $product_id,
			'category'=>$cat_id,
			'ref_id'=>$ref_id);
			$this->db->insert('sla_combination', $data);
			return $ref_id;
		}
		}
		}
		}
    }
	public function submit_sla($company_id,$priority,$response,$resolution,$acceptance,$sla_compliance,$mttr,$datas,$rights)
    {
			$data=array('company_id' => $company_id,
            'priority_level' => $priority,
            'response_time' => $response,
			'resolution_time'=>$resolution,
			'acceptance_time'=>$acceptance,
			'SLA_Compliance_Target'=>$sla_compliance,
			'mttr_target'=>$mttr,
			'editable_rights'=>$rights,
			'ref_id'=>$datas);
			$this->db->insert('sla_mapping', $data);
			return true;
	}
	public function select_com($product_id)
    {
		$data=array('product_id' => $product_id);
			$this->db->select('company_id');
			$this->db->from('product_management');
			$this->db->where($data);
			$query1 = $this->db->get();
			$query=$query1->result_array();
			if(!empty($query))
			{
				return $query[0]['company_id'];
			}
	}
	public function select_pro($cat_id)
    {
		$data=array('cat_id' => $cat_id);
			$this->db->select('prod_id');
			$this->db->from('category_details');
			$this->db->where($data);
			$query1 = $this->db->get();
			$query=$query1->result_array();
			if(!empty($query))
			{
				return $query[0]['prod_id'];
			}
	}
	public function select_table_type($selectedcategory)
	{
		$data=array('company_id'=>$selectedcategory);
		$this->db->select('company_id');
			$this->db->from('company');
			$this->db->where($data);
			$query1 = $this->db->get();
			$query=$query1->result_array();
			if(count($query)>0)
			{
				return "company";
			}
			else{
				$data=array('cat_id' => $selectedcategory);
				$this->db->select('company_id');
				$this->db->from('category_details');
				$this->db->where($data);
				$query1 = $this->db->get();
				$query=$query1->result_array();
				if(count($query)>0)
				{
					return "category";
				}
				else{
					$data=array('product_id'=>$selectedcategory);
					$this->db->select('id');
				$this->db->from('product_management');
				$this->db->where($data);
				$query1 = $this->db->get();
				$query=$query1->result_array();
				if(count($query)>0)
				{
					return "product";
				}
				else{
					return "no id";
				}
				}
			}
	}
	public function select_com_with_cat($cat_id)
    {
		$data=array('cat_id' => $cat_id);
			$this->db->select('company_id');
			$this->db->from('category_details');
			$this->db->where($data);
			$query1 = $this->db->get();
			$query=$query1->result_array();
			if(!empty($query))
			{
				return $query[0]['company_id'];
			}
	}
	
	public function check_slas($id,$prod_id){
		$selected = '';
		$where1=array('company_id'=>$id['company_id'],'product'=>'all','category'=>'all');
		$where2=array('company_id'=>$id['company_id'],'product'=>$prod_id,'category'=>'all');
		$where3=array('company_id'=>$id['company_id'],'product'=>$prod_id,'category !='=>'all');
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($where1);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return 1;
		}else{
			$this->db->select('*');
			$this->db->from('sla_combination');
			$this->db->where($where3);
			$query=$this->db->get();
			if($query->num_rows()>0){
				return 2;
			}else{
				$this->db->select('ref_id');
				$this->db->from('sla_combination');
				$this->db->where($where2);
				$query=$this->db->get();
				if($query->num_rows()>0){
					$query=$query->result_array();
					foreach($query as $ques){
						$this->db->select('*');
						$this->db->from('sla_mapping');
						$this->db->where('ref_id',$ques['ref_id']);
						$datass=$this->db->get();
						if($datass->num_rows()>0){
							$datass=$datass->row_array();
							$selected[] = $datass;
						}else{
							$selected[] = 0;
						}
					}
				}else{
					$selected[] = 0;
				}
			}
			return $selected;
		}
	}
	public function check_slas1($id,$prod_id){
		$selected = '';
		$where1=array('company_id'=>$id['company_id'],'product'=>'all','category'=>'all');
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($where1);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return 1;
		}else{
			$this->db->select('prod_id');
			$this->db->from('category_details');
			$this->db->where('cat_id',$prod_id);
			$query=$this->db->get();
			if($query->num_rows()>0){
				$query=$query->row_array();
				foreach($query as $q){					
					$where1=array('company_id'=>$id['company_id'],'product'=>$q,'category'=>'all');
					$this->db->select('ref_id');
					$this->db->from('sla_combination');
					$this->db->where($where1);
					$dat=$this->db->get();				
					if($dat->num_rows()>0){
						return 1;					
					}else{	
						$dat=$dat->result_array();
						$where2=array('company_id'=>$id['company_id'],'product'=>$q,'category'=>$prod_id);
						$this->db->select('ref_id');
						$this->db->from('sla_combination');
						$this->db->where($where2);
						$datass=$this->db->get();
						if($datass->num_rows()>0){
							$datass=$datass->result_array();
							foreach($datass as $d){
								$this->db->select('*');
								$this->db->from('sla_mapping');
								$this->db->where('ref_id',$d['ref_id']);
								$datass=$this->db->get();
								if($datass->num_rows()>0){
									$datass=$datass->row_array();
									$selected[] = $datass;
								}else{
									$selected[] = 0;
								}
							}
						}else{
							return 0;
						}
					}
				}				
			}else{
				return null;		
			}
		}
		return $selected;			
	}
	public function sla_company_id($prod_id){
		$this->db->select('company_id');
		$this->db->from('category_details');
		$this->db->where('cat_id',$prod_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->row_array();
		}else{
			return 0;
		}
	}
	public function sla_company_id1($prod_id){
		$this->db->select('company_id');
		$this->db->from('product_management');
		$this->db->where('product_id',$prod_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->row_array();
		}else{
			return 0;
		}
	}
	/*public function check_slas2($id){
		$selected = '';
		$where=array('company_id'=>$id,'product != '=>'all','category != '=>'all');
		$this->db->select('*');
		$this->db->from('sla_combination');
		$this->db->where($where);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->result_array();
			foreach($query as $q){
				$this->db->select('*');
				$this->db->from('sla_mapping');
				$this->db->where('ref_id',$q['ref_id']);
				$datass=$this->db->get();
				if($datass->num_rows()>0){
					$datass=$datass->row_array();
					$selected[] = $datass;
				}else{
					$selected[] = 0;
				}
			}
			return $selected;		
		}else{
			return 1;			
		}
	}*/
	public function check_slas2($id){
	    $selected = '';
	    $where=array('company_id'=>$id,'product != '=>'all');
	    $where1=array('company_id'=>$id,'category != '=>'all');
	    $where2=array('company_id'=>$id,'product'=>'all','category'=>'all');
	    $this->db->select('*');
	    $this->db->from('sla_combination');
	    $this->db->where('company_id',$id);
	    $q=$this->db->get(); 
	    if($q->num_rows()>0){
	      $this->db->select('*');
	      $this->db->from('sla_combination');
	      $this->db->where($where);
	      $query=$this->db->get();
	      if($query->num_rows() > 0){
	        return 1;
	      }else{
	        $this->db->select('*');
	        $this->db->from('sla_combination');
	        $this->db->where($where1);
	        $query=$this->db->get();
	        if($query->num_rows()>0){
	          return 1;
	        }else{
	          $this->db->select('*');
	          $this->db->from('sla_combination');
	          $this->db->where($where2);
	          $q=$this->db->get();
	          if($q->num_rows() > 0){
	            $q=$q->result_array();
	            foreach($q as $d){
	              $this->db->select('*');
	              $this->db->from('sla_mapping');
	              $this->db->where('ref_id',$d['ref_id']);
	              $datass=$this->db->get();
	              if($datass->num_rows()>0){
	                $datass=$datass->row_array();
	                $selected[] = $datass;
	              }else{
	                $selected[] = 0;
	              }
	            }
	            return $selected;
	          }else{
	             return 0; 
	          }
	        }
	      }
	    }else{
	      return 0;
	    }
	}
	public function delete_rec($id){
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->where('company_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->result_array();
			return $query;
			/*foreach($query as $q){
				$this->db->select('*');
				$this->db->from('sla_mapping');
				$this->db->where('ref_id',$q['ref_id']);
				$dq=$this->db->get();
				if($dq->num_rows()>0){
					$this->db->where('ref_id',$q['ref_id']);
					$this->db->delete('sla_mapping');					
				}
			}*/
		}else{
			return null;	
		}
	}	
	public function delete_recd($id){
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->where('company_id',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->result_array();
			return $query;
		}else{
			return null;	
		}
	}
	public function dele($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_mapping');
		return true;
	}
	public function deles($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_combination');
		return true;	
	}
	public function delete_recds($id){
		$this->db->select('ref_id');
		$this->db->from('sla_combination');
		$this->db->where('product',$id);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->result_array();
			return $query;
		}else{
			return null;	
		}
	}	
	public function dele_s($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_mapping');
		return true;
	}
	public function deles_s($d){
		$this->db->where('ref_id',$d['ref_id']);
		$this->db->delete('sla_combination');
		return true;
	}
public function check_existslas2($company,$priority)
	{
		$where3=array('company_id'=>$company,'priority_level'=>$priority,'category'=>'all','product'=>'all');
		$this->db->select('ref_id');
		$this->db->from('sla_combination');

		$this->db->where($where3);
		$query=$this->db->get();
		if($query->num_rows()>0){
			$query=$query->result_array();
			return true;
		}else{
			return false;	
		}
	}

	
	public function fetch_renewcompany($id)
{
$this->db->select('*');
$this->db->from('admin_user');
$this->db->order_by('last_update', 'desc');
$this->db->where('admin_id', $id);
$query = $this->db->get();
//print_r($query);
return $query->row_array();
/*if ($query->num_rows() > 0)
{
return $query->result_array();
}
else {return NULL;}*/
}
}
