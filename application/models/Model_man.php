<?php
date_default_timezone_set('Asia/Kolkata');
class model_man extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor  
        parent::__construct();
    }
    public function check_login($username)
    {
        $where = array(
            'username' => $username
        );
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->row_array();
        return $rowcount['password'];
    }
    public function check_user($userid)
    {
        $where = array(
            'user_id' => $userid
        );
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($where);
        $query    = $this->db->get();
        $rowcount = $query->row_array();
        if ($rowcount > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function store_id($device_token, $reg_id, $username)
    {
        if ($device_token != '') {
            $data  = array(
                'device_token' => $device_token,
                'reg_id' => ''
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        } else {
            $data  = array(
                'device_token' => '',
                'reg_id' => $reg_id
            );
            $where = array(
                'username' => $username
            );
            $this->db->where($where);
            $this->db->update('login', $data);
            return true;
        }
    }
    
    public function get_userid($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('user_id');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['user_id'];
    }
    public function get_empid($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('employee_id');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['employee_id'];
    }
    public function get_mobile($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('contact_number');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['contact_number'];
    }
    public function get_location($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('location');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['location'];
    }
    public function get_username($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('first_name');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['first_name'];
    }
    public function get_image($username)
    {
        $where = array(
            'email_id' => $username
        );
        $this->db->select('image');
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        $res   = $query->row_array();
        return $res['image'];
    }
    public function leaderboard($company_id, $filter)
    {
        $company_id = $company_id;
        $month      = date('m', strtotime('-1 month'));
        if ($filter == '') {
            $this->db->select('technician.employee_id,reward.tech_reward_point,technician.first_name');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where('reward.month', $month);
            $this->db->where('reward.company_id', $company_id);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'L1') {
            $where = array(
                'reward.technician_level' => 'L1'
            );
            $this->db->select('technician.employee_id,reward.tech_reward_point,technician.first_name');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->where('reward.month', $month);
            $this->db->where('reward.company_id', $company_id);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'L2') {
            $where = array(
                'reward.technician_level' => 'L2'
            );
            $this->db->select('technician.employee_id,reward.tech_reward_point,technician.first_name');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->where('reward.month', $month);
            $this->db->where('reward.company_id', $company_id);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
            
        } else if ($filter == 'L3') {
            $where = array(
                'reward.technician_level' => 'L3'
            );
            $this->db->select('technician.employee_id,reward.tech_reward_point,technician.first_name');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->where('reward.month', $month);
            $this->db->where('reward.company_id', $company_id);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        } else if ($filter == 'L4') {
            $where = array(
                'reward.technician_level' => 'L4'
            );
            $this->db->select('technician.employee_id,reward.tech_reward_point,technician.first_name');
            $this->db->from('reward');
            $this->db->join("technician", "reward.tech_id=technician.technician_id");
            $this->db->where($where);
            $this->db->where('reward.month', $month);
            $this->db->where('reward.company_id', $company_id);
            $this->db->order_by('reward.tech_reward_point', 'desc');
            $query  = $this->db->get();
            $result = $query->result_array();
        }
        return $result;
    }
    public function get_company($userid)
    {
        $this->db->select('company_id');
        $this->db->from('user');
        $this->db->where('user_id', $userid);
        $query      = $this->db->get();
        $resul      = $query->row_array();
        $company_id = $resul['company_id'];
        return $company_id;
    }
	public function work_in_progress($company_id)
    {
		$this->db->select('technician.employee_id,customer.customer_name,customer.contact_number as cust_contact,all_tickets.priority,all_tickets.ticket_id,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.today_task_count');
		$this->db->from('all_tickets');
		$this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
		$this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
		$this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id and customer.model_no=all_tickets.model');
		$this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
		$this->db->where('all_tickets.company_id',$company_id);
		$this->db->where('all_tickets.current_status',10);
		$this->db->group_by('all_tickets.ticket_id');
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function escallated($company_id)
    {
		$this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,all_tickets.prev_tech_id,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.location as tech_loc,technician.skill_level,technician.contact_number,technician.today_task_count');
		$this->db->from('all_tickets');
		$this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
		$this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
		$this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id and customer.model_no=all_tickets.model');
		$this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
		$this->db->where('all_tickets.company_id',$company_id);
		$this->db->where("all_tickets.current_status!=",12); 
		$this->db->group_start();
		$this->db->where("(all_tickets.current_status = 15 OR all_tickets.current_status = 9 OR all_tickets.previous_status=15)");
		$this->db->group_end();
		$this->db->group_by('all_tickets.ticket_id');
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function spare_request($company_id)
    {
		$json=array();
		$where  = array(
                    'all_tickets.company_id' => $company_id,
					);
		 $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.current_status,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,all_tickets.prev_tech_id,all_tickets.location,all_tickets.requested_spare,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.location as tech_loc,technician.today_task_count');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id and customer.model_no=all_tickets.model');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
			$this->db->group_by('all_tickets.ticket_id');
            $this->db->where($where);
            $this->db->group_start();
            $this->db->where("(all_tickets.current_status = 11 OR all_tickets.previous_status=18)");
            $this->db->group_end();
			$this->db->group_by('all_tickets.ticket_id');
            $query  = $this->db->get();
            $result = $query->result_array();
			foreach ($result as $row) {
                $spare_array = json_decode($row['requested_spare'], true);
                if (!empty($spare_array)) {
                    array_push($json, array(
                        'ticket_id' => $row['ticket_id'],
                        'customer_name' => $row['customer_name'],
                        'location' => $row['location'],
                        'product_name' => $row['product_name'],
                        'cat_name' => $row['cat_name'],
                        'prob_desc' => $row['prob_desc'],
                        'cust_contact'=>$row['cust_contact'],
                        'priority' => $row['priority'],
                        'current_status' => $row['current_status'],
                        'schedule_next' => $row['schedule_next'],
                        'lead_time' => $row['lead_time'],
                        'poa' => $row['poa'],
                        'reason' => $row['reason'],
                        'call_tag' => $row['call_tag'],
                        'call_type' => $row['call_type'],
                        'tech_loc' => $row['tech_loc'],
                        'employee_id' => $row['employee_id'],
                        'tech_id' => $row['tech_id'],
                        'first_name' => $row['first_name'],
                        'email_id' => $row['email_id'],
                        'contact_number' => $row['contact_number'],
                        'skill_level' => $row['skill_level'],
                        'today_task_count' => $row['today_task_count'],
                        'spare_array' => $spare_array
                    ));
                }
            }
		return $json;
	}
     public function disply_Spare($company_id)
    {
        $json = array();
        $this->db->group_by('ticket_id');
        
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.requested_spare!=' => NULL,
            'all_tickets.current_status' => 14
        );
        $this->db->select('*,customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.current_status,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,all_tickets.prev_tech_id,all_tickets.location,all_tickets.requested_spare,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.location as tech_loc,technician.today_task_count');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.cat_id=all_tickets.cat_id");
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
		foreach ($data  as $row) {
                $spare_array = json_decode($row['requested_spare'], true);
                if (!empty($spare_array)) {
                    array_push($json, array(
                        'ticket_id' => $row['ticket_id'],
                        'customer_name' => $row['customer_name'],
                        'location' => $row['location'],
                        'product_name' => $row['product_name'],
                        'cat_name' => $row['cat_name'],
                        'prob_desc' => $row['prob_desc'],
                        'cust_contact'=>$row['cust_contact'],
                        'priority' => $row['priority'],
                        'current_status' => $row['current_status'],
                        'schedule_next' => $row['schedule_next'],
                        'lead_time' => $row['lead_time'],
                        'poa' => $row['poa'],
                        'reason' => $row['reason'],
                        'call_tag' => $row['call_tag'],
                        'call_type' => $row['call_type'],
                        'is_it_chargeable' => $row['is_it_chargeable'],
						'spare_cost' => $row['spare_cost'],
						'cust_acceptance' => $row['cust_acceptance'],
                        'tech_loc' => $row['tech_loc'],
                        'employee_id' => $row['employee_id'],
                        'tech_id' => $row['tech_id'],
                        'first_name' => $row['first_name'],
                        'email_id' => $row['email_id'],
                        'contact_number' => $row['contact_number'],
                        'skill_level' => $row['skill_level'],
                        'today_task_count' => $row['today_task_count'],
                        'spare_array' => $spare_array
                    ));
                }
		}
        return ($json);
    }
	
    public function impressed_Spare($company_id)
    {
        $json = array();
        $spare_array=array();
        $company_id = $company_id;
        $where      = array(
            'personal_spare.company_id' => $company_id,
            'personal_spare.status' => 0
        );
        $this->db->select('personal_spare.spare_array,personal_spare.spare_level,personal_spare.tech_id,personal_spare.status,personal_spare.status_comment,personal_spare.company_id,technician.employee_id,technician.technician_id,technician.first_name,technician.last_name,technician.image,technician.email_id,technician.skill_level,technician.contact_number,technician.alternate_number,technician.flat_no,technician.street,technician.city,technician.state,technician.region,technician.location,technician.area,technician.role,technician.product,technician.category,technician.current_location,technician.current_lat,technician.current_long,technician.availability,technician.today_task_count,technician.task_count,technician.companyname,technician.last_update,technician.town,technician.landmark,technician.country,technician.pincode,product_management.product_id,product_management.product_name,product_management.product_image,product_management.product_modal,product_management.product_desc,category_details.cat_id,category_details.cat_name,category_details.cat_image,category_details.prod_id,category_details.cat_modal,category_details.cat_desc,personal_spare.id as p_id');
        $this->db->from('personal_spare');
        $this->db->join("technician", "personal_spare.tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
        foreach ($data as $row) {
                $spare_array = json_decode($row['spare_array'], true);
               if (!empty($spare_array)) {
                $row['spare_array']=$spare_array;
				}
				else
				{
				$row['spare_array']=array();
				}
				array_push($json,$row);
		}
        return ($json);
    }
   public function impressed_accept_Spare($company_id)
    {
        $json = array();
        $spare_array=array();
        $company_id = $company_id;
        $where      = array(
            'personal_spare.company_id' => $company_id,
            'personal_spare.status' => 1,
            'personal_spare.spare_array!=' => ''
        );
        $this->db->select('personal_spare.id,personal_spare.spare_array,personal_spare.spare_level,personal_spare.tech_id,personal_spare.status,personal_spare.status_comment,personal_spare.company_id,technician.employee_id,technician.technician_id,technician.first_name,technician.last_name,technician.image,technician.email_id,technician.skill_level,technician.contact_number,technician.alternate_number,technician.flat_no,technician.street,technician.city,technician.state,technician.region,technician.location,technician.area,technician.role,technician.product,technician.category,technician.current_location,technician.current_lat,technician.current_long,technician.availability,technician.today_task_count,technician.task_count,technician.companyname,technician.last_update,technician.town,technician.landmark,technician.country,technician.pincode,product_management.product_id,product_management.product_name,product_management.product_image,product_management.product_modal,product_management.product_desc,category_details.cat_id,category_details.cat_name,category_details.cat_image,category_details.prod_id,category_details.cat_modal,category_details.cat_desc');
        $this->db->from('personal_spare');
        $this->db->join("technician", "personal_spare.tech_id=technician.technician_id");
        $this->db->join('product_management', 'technician.product=product_management.product_id');
        $this->db->join('category_details', 'technician.category=category_details.cat_id');
        $this->db->where($where);
		$this->db->group_by("personal_spare.id");
        $query = $this->db->get();
        $data  = $query->result_array();
        foreach ($data as $row) {
                $spare_array = json_decode($row['spare_array'], true);
				if (!empty($spare_array)) {
                $row['spare_array']=$spare_array;
				}
				else
				{
				$row['spare_array']=array();
				}
				array_push($json,$row);
		}
        return ($json);
    }
	public function disply_accept_Spare($company_id)
    {
        $json = array();
        
        $company_id = $company_id;
        $today      = date("Y-m-d");
        $today1     = date("Y-m-d");
        $this->db->group_by('all_tickets.ticket_id');
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.current_status' => 11
        );
        $this->db->select('*,customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.current_status,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,all_tickets.prev_tech_id,all_tickets.location,all_tickets.requested_spare,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.location as tech_loc,technician.today_task_count');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
		foreach ($data as $row) {
                $spare_array = json_decode($row['requested_spare'], true);
                if (!empty($spare_array)) {
                    array_push($json, array(
                        'ticket_id' => $row['ticket_id'],
                        'customer_name' => $row['customer_name'],
                        'location' => $row['location'],
                        'product_name' => $row['product_name'],
                        'cat_name' => $row['cat_name'],
                        'prob_desc' => $row['prob_desc'],
                        'cust_contact'=>$row['cust_contact'],
                        'priority' => $row['priority'],
                        'current_status' => $row['current_status'],
                        'schedule_next' => $row['schedule_next'],
                        'lead_time' => $row['lead_time'],
                        'poa' => $row['poa'],
                        'reason' => $row['reason'],
                        'call_tag' => $row['call_tag'],
                        'call_type' => $row['call_type'],
                        'is_it_chargeable' => $row['is_it_chargeable'],
						'spare_cost' => $row['spare_cost'],
						'cust_acceptance' => $row['cust_acceptance'],
                        'tech_loc' => $row['tech_loc'],
                        'employee_id' => $row['employee_id'],
                        'tech_id' => $row['tech_id'],
                        'first_name' => $row['first_name'],
                        'email_id' => $row['email_id'],
                        'contact_number' => $row['contact_number'],
                        'skill_level' => $row['skill_level'],
                        'today_task_count' => $row['today_task_count'],
                        'spare_array' => $spare_array
                    ));
                }
		}
        return ($json);
    }
    public function disply_reject_Spare($company_id)
    {
        $json = array();
        $this->db->group_by('ticket_id');
        $company_id = $company_id;
        $today      = date("Y-m-d");
        $today1     = date("Y-m-d");
        $this->db->group_by('ticket_id');
        $where = array(
            'all_tickets.company_id' => $company_id,
            'all_tickets.current_status' => 17
        );
        $this->db->select('*,customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.current_status,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,all_tickets.prev_tech_id,all_tickets.location,all_tickets.requested_spare,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.location as tech_loc,technician.today_task_count');
        $this->db->from('all_tickets');
        $this->db->join("customer", "customer.customer_id=all_tickets.cust_id");
        $this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
        $this->db->join('product_management', 'all_tickets.product_id=product_management.product_id');
        $this->db->join('category_details', 'all_tickets.cat_id=category_details.cat_id');
        //$this->db->join('avaliblity', 'avaliblity.charge_id=all_tickets.is_it_chargeable');
        //$this->db->join("amc_price", "amc_price.contract_type=all_tickets.call_type");
        $this->db->where($where);
        $query = $this->db->get();
        $data  = $query->result_array();
		foreach ($data as $row) {
                $spare_array = json_decode($row['requested_spare'], true);
                if (!empty($spare_array)) {
                    array_push($json, array(
                        'ticket_id' => $row['ticket_id'],
                        'customer_name' => $row['customer_name'],
                        'location' => $row['location'],
                        'product_name' => $row['product_name'],
                        'cat_name' => $row['cat_name'],
                        'prob_desc' => $row['prob_desc'],
                        'cust_contact'=>$row['cust_contact'],
                        'priority' => $row['priority'],
                        'current_status' => $row['current_status'],
                        'schedule_next' => $row['schedule_next'],
                        'lead_time' => $row['lead_time'],
                        'poa' => $row['poa'],
                        'reason' => $row['reason'],
                        'call_tag' => $row['call_tag'],
                        'call_type' => $row['call_type'],
                        'is_it_chargeable' => $row['is_it_chargeable'],
						'spare_cost' => $row['spare_cost'],
						'cust_acceptance' => $row['cust_acceptance'],
                        'tech_loc' => $row['tech_loc'],
                        'employee_id' => $row['employee_id'],
                        'tech_id' => $row['tech_id'],
                        'first_name' => $row['first_name'],
                        'email_id' => $row['email_id'],
                        'contact_number' => $row['contact_number'],
                        'skill_level' => $row['skill_level'],
                        'today_task_count' => $row['today_task_count'],
                        'spare_array' => $spare_array
                    ));
                }
		}
        return ($json);
    }
    public function load_assigned($company_id)
    {
                $where  = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => 1
					);
            $this->db->select('technician.employee_id,customer.customer_name,customer.contact_number as cust_contact,all_tickets.priority,all_tickets.ticket_id,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,call_tag.call as call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.today_task_count');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ' );
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
            $this->db->where($where);
//$this->db->group_by('customer.customer_id');
            $query  = $this->db->get();
            $result = $query->result_array();
			$wher = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => '1',
                    'all_tickets.amc_id!=' => ""
                );
            $this->db->select('technician.employee_id,customer.customer_name,customer.contact_number as cust_contact,all_tickets.priority,all_tickets.ticket_id,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag as call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.last_update as assigned_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.today_task_count');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
$this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');             
			 $this->db->where($wher);
           
//$this->db->group_by('customer.customer_id');
            //$this->db->where('all_tickets.last_update>=',$today);
            //$this->db->where('all_tickets.last_update<=',$today1);;
            $query1  = $this->db->get();
            $result=array_merge($result,$query1->result_array());
        
        return $result;
        }
		 public function load_unassigned($company_id)
    {
                $where = array(
                    'all_tickets.company_id' => $company_id
                );
            $current_status=["3","0","15"];
           $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,call_tag.call as call_tag,all_tickets.call_type,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.raised_time');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->where($where);
			$this->db->where('amc_id','');
            $this->db->where_in('current_status',$current_status);
         //   $this->db->where('all_tickets.last_update>=', $today);
           $this->db->where('all_tickets.cust_preference_date<=', $today1);
$this->db->group_by('all_tickets.ticket_id');
            $query  = $this->db->get();
            $result = $query->result_array();//print_r($query);
			$wher=array('all_tickets.company_id' => $company_id,
					'all_tickets.amc_id!='=>'');
            $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag as call_tag,all_tickets.call_type,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.raised_time');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
              $this->db->where($wher);
            $this->db->where_in('current_status',$current_status);
           
//$this->db->group_by('customer.customer_id');
            //$this->db->where('all_tickets.last_update>=',$today);
            //$this->db->where('all_tickets.last_update<=',$today1);;
			
           $this->db->where('all_tickets.cust_preference_date<=', $today1);
$this->db->group_by('all_tickets.ticket_id');
            $query1  = $this->db->get();
            $result=array_merge($result,$query1->result_array());
        
        return $result;
        }
		public function load_accepted($company_id)
    {
        $where = array(
                    'all_tickets.company_id' => $company_id
                );
            $current_status=['2','4','5','6','7'];
            $where1 = ("(sla_mapping.prod_id = all_tickets.product_id OR all_tickets.previous_status=9)");
            $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.current_status,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,call_tag.call as call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as accepted_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.today_task_count');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
            $this->db->where_in('current_status',$current_status);
            $this->db->where($where);
           // $this->db->where('all_tickets.last_update>=', $today);
           // $this->db->where('all_tickets.last_update<=', $today1);
//$this->db->group_by('customer.customer_id');
            $query  = $this->db->get();
            $result = $query->result_array();
			$wher = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => '2',
                    'all_tickets.amc_id!=' => ""
                );
            $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,technician.employee_id,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.current_status,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.tech_id,technician.location as tech_loc,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.last_update as accepted_time,technician.first_name,technician.email_id,technician.skill_level,technician.contact_number,technician.today_task_count');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
$this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');             
			 $this->db->where($wher);
           
//$this->db->group_by('customer.customer_id');
            //$this->db->where('all_tickets.last_update>=',$today);
            //$this->db->where('all_tickets.last_update<=',$today1);;
            $query1  = $this->db->get();
            $result=array_merge($result,$query1->result_array());
        return $result;
    }
		public function load_deferred($company_id)
    {
        $tommorrow = date("Y-m-d 00:00:00", strtotime("+1 day"));
        $today = date("Y-m-d 00:00:00");
        $today1 = date("Y-m-d 23:59:59");
            $where = array(
                'all_tickets.company_id' => $company_id,
                'all_tickets.current_status' => 0,
                'all_tickets.cust_preference_date>=' => $today,
                'all_tickets.cust_preference_date>=' => $today1
            );
        $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,call_tag.call as call_tag,all_tickets.call_type,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.cust_preference_date');
        $this->db->from('all_tickets');
        $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
        $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
        $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
        $this->db->where($where);
//$this->db->group_by('customer.customer_id');
        $query  = $this->db->get();
		//print_r($query);
        $result = $query->result_array();
		$wher = array(
                    'all_tickets.company_id' => $company_id,
                    'all_tickets.current_status' => '0',
                    'all_tickets.amc_id!=' => "",
                'all_tickets.cust_preference_date>=' => $tommorrow
                );
        $this->db->select('distinct(all_tickets.ticket_id),customer.contact_number as cust_contact,customer.customer_name,all_tickets.priority,all_tickets.schedule_next,all_tickets.lead_time,all_tickets.poa,all_tickets.reason,all_tickets.call_tag,all_tickets.call_type,all_tickets.location,product_management.product_name,category_details.cat_name,product_management.product_id,category_details.cat_id,all_tickets.prob_desc,all_tickets.priority,all_tickets.response_time,all_tickets.cust_preference_date');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
			 $this->db->where($wher);
           
//$this->db->group_by('customer.customer_id');
            //$this->db->where('all_tickets.last_update>=',$today);
            //$this->db->where('all_tickets.last_update<=',$today1);;
            $query1  = $this->db->get();
            $result=array_merge($result,$query1->result_array());
        return $result;
    }
	public function get_tech($company_id, $latitude, $longitude, $product_id, $cat_id, $tech_id)
    {
        $where = array(
            'technician.today_task_count<' => 4,
            'technician.product' => $product_id,
            'technician.company_id' => $company_id,
            'technician.category' => $cat_id,
            'technician.availability' => 1,
            'technician.technician_id!=' => $tech_id
        );
        $this->db->select('product_management.product_name,category_details.cat_name,technician.employee_id,technician.technician_id,technician.first_name,technician.last_name,technician.skill_level,
		technician.current_location,technician.today_task_count,technician.product,technician.category,( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( technician.current_lat ) ) *	cos( radians( technician.current_long ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * 	sin( radians( technician.current_lat ) ) ) ) AS distance');
        $this->db->from('technician');
        $this->db->join('product_management', 'product_management.product_id=technician.product');
        $this->db->join('category_details', 'category_details.cat_id=technician.category');
        $this->db->order_by('distance');
        $this->db->where($where);
        $query  = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	public function tickets_count($company_id)
    {
	$res=array();
	$res1=array();
		$where4 = array(
            'all_tickets.company_id' => $company_id
        );
            $sub_where1 = array(
                'current_status' => 1
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
          //  $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where1);
            $this->db->group_by('all_tickets.ticket_id');
            $query2  = $this->db->get();
            $result2 = $query2->num_rows();
            array_push($res, array(
                'assigned' => $result2
            ));
            $sub_where2 = array(
                'current_status' => 2
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
         //   $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where2);
            $this->db->group_by('all_tickets.ticket_id');
            $query3  = $this->db->get();
            $result3 = $query3->num_rows();
            array_push($res, array(
                'accepted' => $result3
            ));
            $sub_where4 = array(
                'current_status' => 9
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
     //       $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where4);
            $this->db->group_by('all_tickets.ticket_id');
            $query5  = $this->db->get();
            $result5 = $query5->num_rows();
            array_push($res1, array(
                'escalated' => $result5
            ));
            $sub_where5 = array(
                'current_status' => 11,'requested_spare!='=>""
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
     //       $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where5);
            $this->db->group_by('all_tickets.ticket_id');
            $query6  = $this->db->get();
            $result6 = $query6->num_rows();
            array_push($res1, array(
                'spare_requested' => $result6
            ));
            $sub_where6 = array(
                'current_status' => 10
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
        //    $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where6);
            $this->db->group_by('all_tickets.ticket_id');
            $query7  = $this->db->get();
            $result7 = $query7->num_rows();
            array_push($res1, array(
                'work_in_progress' => $result7
            ));
            $sub_where8 = array(
                'current_status' => 0
            );
            $today6     = date("Y-m-d 23:59:59");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
       //     $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->where($where4);
            $this->db->where($sub_where8);
            $this->db->where('all_tickets.cust_preference_date<=', $today6);
            $this->db->group_by('all_tickets.ticket_id');
            $query8  = $this->db->get();
            $result8 = $query8->num_rows();
            array_push($res, array(
                'unassigned' => $result8
            ));
             $tommorrow  = date("Y-m-d 00:00:00", strtotime("+1 day"));
            $sub_where9 = array(
                'current_status' => 0,
                'cust_preference_date>=' => $tommorrow
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
      //      $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->where($sub_where9);
            $this->db->where($where4);
            $this->db->group_by('all_tickets.ticket_id');
            $query9  = $this->db->get();
            $result9 = $query9->num_rows();
            array_push($res, array(
                'deferred' => $result9
            ));
			
			$reslt=array("fresh"=>$res,"carry"=>$res1);
			return $reslt;
    }
	public function tickets_status($company_id)
    {
	$res=array();
	$res1=array();
		$where4 = array(
            'all_tickets.company_id' => $company_id
        );
            $sub_where1 = array(
                'current_status' => 1
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
          //  $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where1);
            $this->db->group_by('all_tickets.ticket_id');
            $query2  = $this->db->get();
            $result2 = $query2->num_rows();
            array_push($res, array(
                'assigned' => $result2
            ));
            $sub_where5 = array(
                'current_status' => 14,'requested_spare!='=>""
            );
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->join('technician', 'technician.technician_id=all_tickets.tech_id');
     //       $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->where($where4);
            $this->db->where($sub_where5);
            $this->db->group_by('all_tickets.ticket_id');
            $query6  = $this->db->get();
            $result6 = $query6->num_rows();
            array_push($res, array(
                'spare_requested' => $result6
            ));
            $sub_where8 = array(
                'current_status' => 0
            );
            $today6     = date("Y-m-d 23:59:59");
            $this->db->select('*');
            $this->db->from('all_tickets');
            $this->db->join('product_management', 'product_management.product_id=all_tickets.product_id');
            $this->db->join('category_details', 'category_details.cat_id=all_tickets.cat_id');
       //     $this->db->join('call_tag', 'call_tag.id=all_tickets.call_tag');
            $this->db->join('customer', 'customer.customer_id=all_tickets.cust_id and customer.component_serial_no=all_tickets.cat_id and customer.product_serial_no=all_tickets.product_id ');
            $this->db->where($where4);
            $this->db->where($sub_where8);
            $this->db->where('all_tickets.cust_preference_date<=', $today6);
            $this->db->group_by('all_tickets.ticket_id');
            $query8  = $this->db->get();
            $result8 = $query8->num_rows();
            array_push($res, array(
                'unassigned' => $result8
            ));
			$sub_where9      = array(
				'personal_spare.company_id' => $company_id,
				'personal_spare.status' => 0
			);
			$this->db->select('*,personal_spare.id as p_id');
			$this->db->from('personal_spare');
			$this->db->join("technician", "personal_spare.tech_id=technician.technician_id");
			$this->db->join('product_management', 'technician.product=product_management.product_id');
			$this->db->join('category_details', 'technician.category=category_details.cat_id');
			$this->db->where($sub_where9);
			$query9 = $this->db->get();
			$data9  = $query9->num_rows();
			array_push($res, array(
                'imprest_spare_request' => $data9
            ));
			$sub_where10 = array(
            "all_tickets.amc_id!=" => ''
			);
			$group = array(
				"customer.customer_id",
				"customer.serial_no",
				"customer.product_serial_no"
			);
			$this->db->select('*,customer.customer_name,product_management.product_name,category_details.cat_name');
			$this->db->from('all_tickets');
			$this->db->join("customer", "all_tickets.cust_id=customer.customer_id");
			$this->db->join("product_management", "all_tickets.product_id=product_management.product_id");
			$this->db->join("technician", "all_tickets.tech_id=technician.technician_id");
			$this->db->join("category_details", "all_tickets.cat_id=category_details.cat_id");
			$this->db->order_by('all_tickets.last_update', 'desc');
			$this->db->group_by('all_tickets.ticket_id');
			$this->db->group_start();
			$this->db->where('all_tickets.current_status', 8);
			$this->db->or_where('all_tickets.current_status', 12);
			$this->db->group_end();
			$this->db->where($sub_where10);
			$this->db->where($where4);
            $this->db->group_by('all_tickets.ticket_id');
			$query10 = $this->db->get();
			$data10  = $query10->num_rows();
			array_push($res, array(
                'contract_approval' => $data10
            ));
			$sub_where11 = array(
            "reimbursement_request.action" => 0,
            "reimbursement_request.status" => 17,
            "reimbursement_request.company_id" => $company_id
			);
			$this->db->select('*');
			$this->db->from('reimbursement_request');
			$this->db->join("technician", "reimbursement_request.technician_id=technician.technician_id");
			$this->db->join("product_management", "technician.product=product_management.product_id");
			$this->db->join("category_details", "technician.category=category_details.cat_id");
			$this->db->where($sub_where11);
			$this->db->order_by('reimbursement_request.id','desc');
			$this->db->group_by(array(
				"reimbursement_request.start_date",
				"reimbursement_request.end_date",
				"reimbursement_request.technician_id"
			));
			$query11 = $this->db->get();
			$total11=$query11->num_rows();
			array_push($res, array(
                'reimbursement_approval' => $total11
            ));
			return $res;
    }

}