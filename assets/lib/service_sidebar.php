<div class="page-sidebar navbar-collapse collapse">
                            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
							<li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
								<li class="nav-item start " id="dashboard">
                                    <a href="<?php echo site_url('controller_service');?>" class="nav-link nav-toggle">
                                        <i class="icon-home"></i>
										<span class="title"> Dashboard</span>
										 </a>
										
                                </li>
								<li id="new" class="nav-item">
                                    <a href="<?php echo site_url('controller_service/assigned_tickets');?>" class="nav-link nav-toggle">
                                        <i class="fa fa-ticket"></i>
										<span class="title">New Tickets</span>
										 </a>
                                </li>	
				<li class="nav-item"  id="ongoing">
                  <a href="<?php echo site_url('controller_service/ongoing');?>" class="nav-link nav-toggle">
                  <i class="icon-login"></i>
				  <span class="title">Ongoing Tickets</span>
				  </a>
               </li>
               <li class="nav-item" id="completed">
                  <a href="<?php echo site_url('controller_service/completed');?>" class="nav-link nav-toggle">
                  <i class="fa fa-th-list"></i>
				  <span class="title">Completed Tickets </span>
				  </a>
               </li>
			   <li class="nav-item" id="knowledge_b">
                  <a href="<?php echo site_url('controller_service/knowledge_base');?>" class="nav-link nav-toggle">
                  <i class="icon-book-open"></i>
				  <span class="title">Knowledge-Base</span> 
				  </a>
               </li>
               <li class="nav-item" id="spare">
                  <a href="<?php echo site_url('controller_service/spare_form');?>" class="nav-link nav-toggle">
                  <i class="icon-notebook"></i>
				  <span class="title">Spare Request</span>
				  </a>
               </li>
			   <li class="nav-item" id="service_raise_tkt">
                  <a href="<?php echo site_url('controller_service/service_raise_tkt');?>" class="nav-link nav-toggle">
                  <i class="icon-wrench"></i>
				  <span class="title">Raise Ticket</span>
				  </a>
			  </li>
               <li class="nav-item" id="service_amc">
                  <a href="<?php echo site_url('controller_service/service_amc');?>" class="nav-link nav-toggle">
                  <i class="icon-wrench"></i>
				  <span class="title">AMC Contract</span>
				  </a>
			   </li>
				<li class="nav-item" id="customer_req">
                  <a href="<?php echo site_url('controller_service/customer_req');?>" class="nav-link nav-toggle">
                  <i class="icon-wrench"></i>
				  <span class="title">New Registration</span>
				  </a>
			   </li>							
                                        
                            </ul>
                        </div>