
<div class="page-sidebar navbar-collapse collapse">
                            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" >
							<li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
								<li id="admin_dashboard" class="nav-item start">
                                    <a href="<?php echo site_url('controller_admin');?>" class="nav-link nav-toggle">
                                        <i class="icon-home"></i>
										<span class="title"> Dashboard</span>
									 </a>	
                                </li>
								<li id="user_management" class="nav-item">
                                    <a href="<?php echo site_url('controller_admin/user');?>" class="nav-link nav-toggle">
                                        <i class="icon-user"></i>
										<span class="title">User Management</span>
										 </a>
                                </li>
								<li id="product_management" class="nav-item">
                                    <a href="<?php echo site_url('controller_admin/product');?>" class="nav-link nav-toggle">
                                        <i class="icon-layers"></i>
										<span class="title">Product Management</span>
										 </a>
                                </li>
								<li id="customer_management" class="nav-item">
                                    <a href="<?php echo site_url('controller_admin/customer');?>" class="nav-link nav-toggle">
                                        <i class="icon-user"></i>
										<span class="title">Customer Management</span>
										 </a>
                                </li>
								<li id="training_management" class="nav-item">
								<a href="<?php echo site_url('controller_admin/certificate');?>" class="nav-link nav-toggle">
								<i class="icon-book-open"></i> 
								<span class="title">Content Management </span>
								</a>
								</li>
								<li id="sla_management" class="nav-item">
								<a href="<?php echo site_url('controller_admin/sla');?>" class="nav-link nav-toggle">
								<i class="icon-directions"></i> 
								<span class="title">SLA Mapping </span>
								</a>
								</li>
								<li id="spare_management" class="nav-item">
								<a href="<?php echo site_url('controller_admin/spare');?>" class="nav-link nav-toggle">
								<i class="icon-wrench"></i>
								<span class="title"> Inventory Management</span>
								</a>
								</li>	

								<li id="service_group" class="nav-item">
								<a href="<?php echo site_url('controller_admin/service_group');?>" class="nav-link nav-toggle">
								<i class="icon-user"></i>
								<span class="title"> Service Group</span>
								</a>
								</li>	
							</ul>
</div>
