<div class="page-sidebar navbar-collapse collapse">
                            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
							<li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
				<li class="nav-item start" id="focus_dasboard">
                                    <a href="<?php echo site_url('Controller_superad');?>" class="nav-link nav-toggle"> 
                                        <i class="icon-home"></i>
										 <span class="title"> Dashboard</span>
								   </a>
               </li>
               <li class="nav-item" id="focus_newsubs">
                                    <a href="<?php echo site_url('Controller_superad/new_subscription');?>" class="nav-link nav-toggle">
                                        <i class="icon-plus"></i>
										<span class="title"> New Subscription </span>
									</a>
                                </li> 
			    <li class="nav-item" id="focus_managesubs">
                                    <a href="<?php echo site_url('Controller_superad/manage_subscription');?>" class="nav-link nav-toggle">
                                        <i class="icon-puzzle"></i>
										<span class="title"> Manage Subscriptions </span>
									</a>
                                </li>
                                <li class="nav-item" id="product_m">
                                    <a href="<?php echo site_url('Controller_superad/product_master');?>" class="nav-link nav-toggle">
                                        <i class="icon-layers"></i>
										<span class="title"> Product Master </span>
									</a>
                                </li>
                                <li id="sla_page" class="nav-item">
                                    <a href="<?php echo site_url('Controller_superad/sla_mapping');?>" class="nav-link nav-toggle">
									<i class="fa fa-map-signs" aria-hidden="true"></i>
									<span class="title"> SLA Mapping </span>
									</a>
                                </li>
								<li class="nav-item" id="focus_billing">
                                    <a href="<?php echo site_url('Controller_superad/billing_details');?>" class="nav-link nav-toggle">
							<i class="fa fa-money" aria-hidden="true"></i>
							<span class="title"> Billing Details </span>
							 </a>
                                </li>
						
                                        
                            </ul>
                        </div>